//
//  hvachistview.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "hvachistview.h"
#import "timedataview.h"
#import "hvachistdataview.h"
#import "timebar.h"
#import "LegendItem.h"
#import "CustomPage.h"



@implementation CHVACHistoryView
/*============================================================================*/

- (id)initWithFrame:(CGRect)rFrame TStat:(CTStat*)pTStat

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame]) 
    {
        // Initialization code
        m_pTStat = pTStat;
        m_bLockYScroll = TRUE;

        CGRect rData = CGRectMake(-rFrame.size.width, 0, 3 * rFrame.size.width, [self DataViewDY]);
        m_pTimeDataView = [self CreateTimeDataView:rData LastView:NULL];
        [self insertSubview:m_pTimeDataView belowSubview:m_pTimeBar];

        [self InitScaleView];

        self.autoresizingMask = 0xFFFFFFFF;
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(UIView*)CreateLegendView

/*============================================================================*/
{
    CGRect rLegend = CGRectMake(4, 5, self.frame.size.width-8, 22);
    UIView *pView = [[UIView alloc] initWithFrame:rLegend];
    [pView setOpaque:NO];
    for (int i = 0; i < 4; i++)
    {
        int iDXItem = rLegend.size.width / 4;
        CGRect rItem = CGRectMake(iDXItem * i, 0, iDXItem, rLegend.size.height);
        NSString *pText = NULL;
        UIColor *pColor = NULL;
        UIColor *pColorFill = NULL;
        switch (i)
        {
        case 0: { pText = @"Cool Setpoint"; pColorFill = [UIColor colorWithRed:0 green:0 blue:.25 alpha:1];     pColor = [UIColor blueColor];     } break;
        case 1: { pText = @"Heat Setpoint"; pColorFill = [UIColor colorWithRed:.25 green:0 blue:0 alpha:1];     pColor = [UIColor redColor];     } break;
        case 2: { pText = @"Room Temp";     pColorFill = [UIColor colorWithRed:0 green:.25 blue:0 alpha:1];     pColor = [UIColor greenColor];   } break;
        case 3: { pText = @"Outside Temp";  pColorFill = [UIColor colorWithRed:.1 green:.1 blue:.1 alpha:1];    pColor = [UIColor whiteColor];   } break;
        }

        CLegendItem *pItem = [[CLegendItem alloc] initWithFrame:rItem Color:pColor FillColor:pColorFill Text:pText Round:NO];
        [pView addSubview:pItem];
        [pItem release];
    }

    return pView;
}
/*============================================================================*/

-(CTimeDataView*)CreateTimeDataView:(CGRect)rFrame LastView:(CTimeDataView*)pLastView

/*============================================================================*/
{
    CHVACHistoryDataView *pLastHVACView = (CHVACHistoryDataView*)pLastView;
    CHVACHistoryDataView *pView = [[CHVACHistoryDataView alloc] initWithFrame:rFrame TimeView:self TStat:m_pTStat LastView:pLastHVACView];
    return pView;
}
/*============================================================================*/

-(int)DataViewDY

/*============================================================================*/
{
    return self.frame.size.height;
}
/*============================================================================*/

-(void)NotifyRotation

/*============================================================================*/
{
    [m_pTimeBar removeFromSuperview];
    [m_pTimeBar release];

    CGRect rFrame = self.frame;
    CGRect rTimeBar = CGRectMake(-rFrame.size.width, rFrame.size.height-[CMainView DY_TIMEBAR], 3*rFrame.size.width, [CMainView DY_TIMEBAR]);
    m_pTimeBar = [[CTimeBar alloc] initWithFrame:rTimeBar TimeView:self];
    [self addSubview:m_pTimeBar];

    CGRect rData = m_pTimeDataView.frame;
    rData.origin.x = -self.frame.size.width;
    rData.size.width = 3 * self.frame.size.width;
    rData.size.height = self.frame.size.height;
    int iDYDataOld = m_pTimeDataView.frame.size.height;
    int iDYDataNew = rData.size.height;
    [m_pTimeDataView setFrame:rData];

    CHVACHistoryDataView *pHistData = (CHVACHistoryDataView*)m_pTimeDataView;
    [pHistData SetXOrigin];
    [pHistData UpdateRTEViews];
    [pHistData UpdateTimeBar];
    
    CGRect rLegend = m_pLegendView.frame;
    rLegend.origin.x = self.frame.size.width - rLegend.size.width - 4;
    rLegend.origin.y = 2;
    [m_pLegendView setFrame:rLegend];
    [pHistData SetFrameResizeOld:iDYDataOld New:iDYDataNew];
    [m_pTimeDataView setNeedsDisplay];
}
/*============================================================================*/

-(void)InitScaleView

/*============================================================================*/
{
    [m_pScaleView removeFromSuperview];
    [m_pScaleView release];

    CGRect rScale = CGRectMake(0, 0, 60, [self DataViewDY] - [CMainView DY_TIMEBAR]);

    m_pScaleView = [[UIView alloc] initWithFrame:rScale];
    m_pScaleView.clipsToBounds = YES;
    m_pScaleView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.75];

    if (m_pLegendView != NULL)
        [self insertSubview:m_pScaleView belowSubview:m_pLegendView];
    else
        [self insertSubview:m_pScaleView belowSubview:m_pTimeBar];

    CHVACHistoryDataView *pHVAC = (CHVACHistoryDataView*)m_pTimeDataView;
    int iStep = 2;
    int iDTemp = [pHVAC MaxTemp] - [pHVAC MinTemp];
    if (iDTemp > 30)
        iStep = 5;
    else if (iDTemp > 50)
        iStep = 10;

    for (int i = [pHVAC MinTemp]; i <= [pHVAC MaxTemp]; i+=iStep)
    {
        int iY = [pHVAC YLogical:i];
        CGRect rTemp = CGRectMake(0, iY-10, 60, 20);
        if (iY > 60 && iY < rScale.size.height - 20)
        {
            UILabel *pLabel = [[UILabel alloc] initWithFrame:rTemp];
            [CCustomPage InitStandardLabel:pLabel Size:18];
            
            pLabel.textAlignment = UITextAlignmentCenter;
            NSString *pText = [NSString stringWithFormat:@"%d°", i];
            [pLabel setText:pText];

            [m_pScaleView addSubview:pLabel];
            [pLabel release];
        }
    }
}

@end
