/*
--------------------------------------------------------------------------------

    STRINGLIB.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#ifdef GATEWAY
#include "hlfile.h"
#include "schema.h"
#endif

#include "hlmsg.h"
#include "hlquery.h"
#include "platform.h"
#include "StringLib.h"

#ifdef GATEWAY
    #include "..\lcore\tcp_servclient.h"
    #include "..\lcore\servhost.h"
    #include "..\lcore\system.h"
    #include "..\lcore\aserv.h"
#endif

    #define FILE_DATA           HL_FOLDER(_T("LANGUAGE.DAT"))
    #define FILE_XSTRINGS       HL_FOLDER(_T("XSTRINGS.DAT"))

    CStringLib                  *CStringLib::g_pInstance = NULL;


/*============================================================================*/

    XStringPair::XStringPair(
    char                            *pKeyString,
    CHLString                       *pString )

/*============================================================================*/
{
    int iSize = (int)strlen(pKeyString);
    m_pKeyString = new char[iSize+1];
    strcpy(m_pKeyString, pKeyString);
    m_pContextString = NULL;
    m_pXString = pString;

#ifdef HLCONFIG
    m_iFlags = 0;
#endif
};
/*============================================================================*/

    XStringPair::XStringPair(
    char                            *pKeyString,
    char                            *pContextString,
    CHLString                       *pString)

/*============================================================================*/
{
    int iSize1 = (int)strlen(pKeyString);
    int iSize2 = 0;
    if (pContextString != NULL)
        iSize2 = (int)strlen(pContextString);

    m_pKeyString = new char[iSize1+1];
    strcpy(m_pKeyString, pKeyString);

    m_pContextString = NULL;
    if (iSize2 > 0)
    {
        m_pContextString = new char[iSize2+1];
        strcpy(m_pContextString, pContextString);
    }

    m_pXString = pString;

#ifdef HLCONFIG
    m_iFlags = 0;
#endif
};
/*============================================================================*/

    XStringPair::~XStringPair()

/*============================================================================*/
{
    delete [] m_pContextString;
    delete [] m_pKeyString;
    delete m_pXString;
};

/*============================================================================*/

    CStringLib                  *CStringLib::Instance()

/*============================================================================*/
{
    return g_pInstance;
}
/*============================================================================*/

    void                        CStringLib::InitInstance()

/*============================================================================*/
{
    if(g_pInstance == NULL)
    {
        g_pInstance = new CStringLib();
    }
}
/*============================================================================*/

    void                     CStringLib::ReleaseInstance()

/*============================================================================*/
{
    delete g_pInstance;
    g_pInstance = NULL;
}
/*============================================================================*/

    CStringLib::CStringLib()

/*============================================================================*/
{
    m_iCodePage             = CODE_PAGE_ANSI_LATIN_1;
    m_ppXStringPairs        = NULL;
    m_nXStringPairs         = 0;
    m_bSorted               = FALSE;
}
/*============================================================================*/

    CStringLib::~CStringLib(void)

/*============================================================================*/
{
    for(int i = 0; i < m_nXStringPairs; i++)
    {
        delete m_ppXStringPairs[i];
    }
    m_nXStringPairs = 0;
    delete [] m_ppXStringPairs;
}
/*============================================================================*/

    int                             CStringLib::GetNumXStrings()

/*============================================================================*/
{
    return m_nXStringPairs;
}
/*============================================================================*/

    XStringPair                     *CStringLib::GetXStringPair(int iIndex)

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_nXStringPairs)
        return NULL;

    XStringPair *pPair = m_ppXStringPairs[iIndex];
    return pPair;
}
/*============================================================================*/

    XStringPair                     *CStringLib::FindXStringPair(
    const char                      *pKeyString )

/*============================================================================*/
{
    XStringPair *pPair = NULL;
    int iLo = 0;
    int iHi = m_nXStringPairs - 1;
    int i = 0;
    if (m_bSorted)
    {
        // Binary search, gets within 2 possibilities
        i = (iHi + iLo) >> 1;
        while ((iHi - iLo) > 1)
        {
            int icmp = strcmp(pKeyString, m_ppXStringPairs[i]->Key());
            if (icmp < 0)
                iHi = i;
            else if (icmp > 0)
                iLo = i;
            else  // (icmp == 0)
            {
                // Found a match for KeyString
                pPair = m_ppXStringPairs[i];
                break;
            }
            i = (iHi + iLo) >> 1;
        }
    }


    if (pPair == NULL)
    {
        for(i = iLo; i <= iHi; i++)
        {
            if (strcmp(pKeyString, m_ppXStringPairs[i]->Key()) == 0)
            {
                pPair = m_ppXStringPairs[i];
                break;
            }
        }
    }

    if (pPair)
    {
        // Possibly duplicate KeyStrings with different Contexts, find one with no Context
        if ((pPair->Context() == NULL) || strlen(pPair->Context()) <= 0)
            return pPair;
        else
        {
            iLo = i-1;
            iHi = i+1;
            while ((iLo >= 0) && strcmp(pKeyString, m_ppXStringPairs[iLo]->Key()) == 0)
            {
                if ((m_ppXStringPairs[iLo]->Context() == NULL) || strlen(m_ppXStringPairs[iLo]->Context()) <= 0)
                    return m_ppXStringPairs[i];
                iLo--;
            }
            while ((iHi < m_nXStringPairs) && strcmp(pKeyString, m_ppXStringPairs[iHi]->Key()) == 0)
            {
                if ((m_ppXStringPairs[iHi]->Context() == NULL) || strlen(m_ppXStringPairs[iHi]->Context()) <= 0)
                    return m_ppXStringPairs[iHi];
                iHi++;
            }
            return NULL;
        }
    }

    return pPair;
}
/*============================================================================*/

    XStringPair                     *CStringLib::FindXStringPair(
    const char                      *pKeyString,
    const char                      *pContextString )

/*============================================================================*/
{
    if (pContextString == NULL)
        return FindXStringPair(pKeyString);
    if (strlen(pContextString) < 1)
        return FindXStringPair(pKeyString);

    XStringPair *pPair = NULL;
    int iLo = 0;
    int iHi = m_nXStringPairs - 1;
    int i = 0;
    if (m_bSorted)
    {
        // Binary search, gets within 2 possibilities
        i = (iHi + iLo) >> 1;
        while ((iHi - iLo) > 1)
        {
            int icmp = strcmp(pKeyString, m_ppXStringPairs[i]->Key());
            if (icmp < 0)
                iHi = i;
            else if (icmp > 0)
                iLo = i;
            else  // (icmp == 0)
            {
                // Found a match for KeyString
                pPair = m_ppXStringPairs[i];
                break;
            }
            i = (iHi + iLo) >> 1;
        }
    }
    else
    {
        for (int i = 0; i < m_nXStringPairs; i++)
        {
            XStringPair *pX = m_ppXStringPairs[i];
            if (pX->Context() != NULL)
            {
                if (strcmp(pKeyString, pX->Key()) == 0 && strcmp(pContextString, pX->Context()) == 0)
                    return pX;
            }
        }

        return NULL;
    }

    if (pPair == NULL)
    {
        for(i = iLo; i <= iHi; i++)
        {
            if (strcmp(pKeyString, m_ppXStringPairs[i]->Key()) == 0)
            {
                pPair = m_ppXStringPairs[i];
                break;
            }
        }
    }

    if (pPair)
    {
        // Possibly duplicate KeyStrings with different Contexts, find the one we want
        if ((pPair->Context()!= NULL) && (strcmp(pPair->Context(), pContextString) == 0))
            return pPair;
        else
        {
            iLo = i-1;
            iHi = i+1;
            while ((iLo >= 0) && strcmp(pKeyString, m_ppXStringPairs[iLo]->Key()) == 0)
            {
                if ((m_ppXStringPairs[iLo]->Context() != NULL) && (strcmp(m_ppXStringPairs[iLo]->Context(), pContextString) == 0))
                    return m_ppXStringPairs[iLo];
                iLo--;
            }
            while ((iHi < m_nXStringPairs) && strcmp(pKeyString, m_ppXStringPairs[iHi]->Key()) == 0)
            {
                if ((m_ppXStringPairs[iHi]->Context() != NULL) && (strcmp(m_ppXStringPairs[iHi]->Context(), pContextString) == 0))
                    return m_ppXStringPairs[iHi];
                iHi++;
            }
            return NULL;
        }
    }
    return NULL;
}
/*============================================================================*/

    int                             CStringLib::LookupXString(
    const char                      *pKeyString,
    CHLString                       *pXString)

/*============================================================================*/
{
    XStringPair *pPair = FindXStringPair(pKeyString);
    if (pPair)
    {
        int iLength = pPair->m_pXString->GetLength();
        memcpy(pXString->GetBufferWCHAR(iLength), pPair->m_pXString->GetBufferWCHAR(iLength), iLength * 2);
        pXString->ReleaseBuffer(iLength);
        return iLength;
    }
//  ASSERT(0);   // need to add to table
    return 0;
}
/*============================================================================*/

    int                             CStringLib::LookupXString(
    const char                      *pKeyString,
    const char                      *pContextString,
    CHLString                       *pXString)

/*============================================================================*/
{
    XStringPair *pPair = FindXStringPair(pKeyString, pContextString);
    if (pPair)
    {
        int iLength = pPair->m_pXString->GetLength();
        memcpy(pXString->GetBufferWCHAR(iLength), pPair->m_pXString->GetBufferWCHAR(iLength), iLength * 2);
        pXString->ReleaseBuffer(iLength);
        return iLength;
    }
//  ASSERT(0);   // need to add to table
    return 0;
}
/*============================================================================*/

    void                            CStringLib::SetXString(
    const char                      *pKeyString,
    CHLString                       *pXString)

/*============================================================================*/
{
    XStringPair *pPair = FindXStringPair(pKeyString);
    if (pPair)
    {
#ifdef HLCONFIG
        if (pPair->m_pXString == NULL)
            pPair->m_pXString = new CHLString();
#else
        if (pPair->m_pXString == NULL)
            pPair->m_pXString = new CHLString(CHLString::HLSTRING_UNICODE);
#endif
        *pPair->m_pXString = *pXString;
    }
}
#ifdef GATEWAY
/*============================================================================*/

    BOOL                            CStringLib::Read()

/*============================================================================*/
{
    CHLFile file = (FILE_DATA);
    if (!file.Open(CHLFile::MODE_READ))
        return FALSE;

    RemoveLangStrings();
    int iSchema = 0;
    if (!file.ReadInt(&iSchema))
        return FALSE;

    if (!file.ReadInt(&m_iCodePage))
        return FALSE;

    int iNXStrings = 0;
    if (!file.ReadInt(&iNXStrings))
        return FALSE;

    if (iNXStrings < 0 || iNXStrings > 10000)
        return FALSE;

    m_ppXStringPairs = new XStringPair*[iNXStrings];

    for (int i = 0; i < iNXStrings; i++)
    {
        if (iSchema >= SCHEMA_334)
        {
            CHLString sKey;
            CHLString sContext;
            CHLString sVal;

            if (!file.ReadString(&sKey))
                return FALSE;
            if (!file.ReadString(&sContext))
                return FALSE;
            CHLString *pVal = new CHLString();
            if (!file.ReadString(pVal))
                return FALSE;
            m_ppXStringPairs[i] = new XStringPair(sKey, sContext, pVal);
            m_nXStringPairs++;
        }
        else
        {
            CHLString sKey;
            CHLString sVal;
            if (!file.ReadString(&sKey))
                return FALSE;
            CHLString *pVal = new CHLString();
            if (!file.ReadString(pVal))
                return FALSE;
            m_ppXStringPairs[i] = new XStringPair(sKey, pVal);
            m_nXStringPairs++;
        }
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                            CStringLib::Write()

/*============================================================================*/
{
    CHLFile file = (FILE_DATA);
    if (!file.Open(CHLFile::MODE_WRITE))
        return FALSE;

    if (!file.WriteInt(SCHEMA_LATEST))
        return FALSE;

    if (!file.WriteInt(m_iCodePage))
        return FALSE;

    if (!file.WriteInt(m_nXStringPairs))
        return FALSE;

    for (int i = 0; i < m_nXStringPairs; i++)
    {
        XStringPair *pX = m_ppXStringPairs[i];
        CHLString sKey    = pX->Key();
        CHLString *psVal  = pX->Value();
        CHLString sContext;
        if (pX->Context())
            sContext = pX->Context();

        if (!file.WriteString(sKey))
            return FALSE;
        if (!file.WriteString(sContext))
            return FALSE;
        if (!file.WriteString(*psVal))
            return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    void                        CStringLib::ProcessHLMessage(
    CServerClient               *pClient,
    short                       shSenderID,
    short                       shMessageID )

/*============================================================================*/
{
    HLT_HEADER *pHeader = (HLT_HEADER *) pClient->Message();
    CHLMSG msgQ(pHeader);
    CHLMSG msgA(shSenderID, shMessageID+1);

    switch(shMessageID)
    {
    case HLM_STRINGLIB_GETALLSTRINGSQ:
    case HLM_STRINGLIB_GETUISTRINGSQ:
        {
            SaveTo(&msgA, shMessageID == HLM_STRINGLIB_GETALLSTRINGSQ);
        }
        break;

    case HLM_STRINGLIB_SETALLSTRINGSQ:
        {
            LoadFrom(&msgQ);
        }
        break;

    case HLM_STRINGLIB_GETDEFAULTSTRINGSQ:
        {
            msgA.PutInt(CODE_PAGE_ANSI_LATIN_1);

            CHLObList List;
            CHLFile file(FILE_XSTRINGS);
            if (file.Open(CHLFile::MODE_READ))
            {
                int iN = 0;
                file.ReadInt(&iN);

                for (int i = 0; i < iN; i++)
                {
                    CHLString sKey;
                    CHLString sContext;
                    file.ReadString(&sKey);
                    file.ReadString(&sContext);
                    CHLString sKeyThis = sKey;
                    XStringPair *pX = new XStringPair((char*)sKeyThis, (char*)sContext, new CHLString(sKey));
                    List.AddTail(pX);
                }
            }

            CAudioServer::Instance()->LoadTemplateStrings(&List);

            msgA.PutInt(List.GetCount());

            while (!List.IsEmpty())
            {
                XStringPair *pX = (XStringPair*)List.RemoveHead();  
                CHLString sKey;
                CHLString sContext;
                if (pX->Key() != NULL)
                    sKey = pX->Key();
                if (pX->Context() != NULL)
                    sContext = pX->Context();

                msgA.PutString(sKey);
                msgA.PutString(sContext);
                msgA.PutString(sKey);
                delete pX;
            }
        }
        break;

    default:
        goto ABORT;
    }

    CServerHost::Instance()->SendClientMSG(pClient, &msgA);
    return;

ABORT:
    CServerHost::Instance()->SendClient(pClient, shSenderID, HLM_BAD_PARAMETERS);
}
#endif
/*============================================================================*/

    BOOL                            CStringLib::SupportsAlphaScroll()

/*============================================================================*/
{
    switch (m_iCodePage)
    {
    case CODE_PAGE_THAI:
    case CODE_PAGE_HEBREW:
    case CODE_PAGE_ARABIC:
    case CODE_PAGE_SIMP_CHINESE:
        return FALSE;
    default:
        return TRUE;
    }

    return TRUE;
}
/*============================================================================*/

    void                            CStringLib::LoadFrom(CHLMSG *pMSG)

/*============================================================================*/
{
    int iN = 0;

#ifdef GATEWAY
    int iLastCodePage = m_iCodePage;
#endif

    if (!pMSG->GetInt(&m_iCodePage))
        return;
    if (!pMSG->GetInt(&iN))
        return;

    RemoveLangStrings();
    m_bSorted = FALSE;

    m_ppXStringPairs = new XStringPair*[iN];

    for (int i = 0; i < iN; i++)
    {
        CHLString sKey;
        CHLString sContext;

        if (!pMSG->GetString(&sKey))
            return;
        if (!pMSG->GetString(&sContext))
            return;

        CHLString *psVal = new CHLString();
        if (!pMSG->GetString(psVal))
            return;

#ifdef GATEWAY
        // Don't save if key is = value
        if (psVal->Compare(sKey) == 0)
        {
            delete psVal;
        }
        else
        {
            m_ppXStringPairs[m_nXStringPairs] = new XStringPair(sKey, sContext, psVal);
            m_nXStringPairs++;
        }

#else
    #ifdef CRYSTALPAD
        FixEscapes(&sKey);
        FixEscapes(psVal);
    #endif
        m_ppXStringPairs[m_nXStringPairs] = new XStringPair(sKey, sContext, psVal);
        ValidateFormat(m_ppXStringPairs[m_nXStringPairs]);
        m_nXStringPairs++;
#endif
    }
#ifdef CRYSTALPAD
    SortByKey();
    m_bSorted = TRUE;
#endif

#ifdef GATEWAY
    if (m_iCodePage != iLastCodePage)
    {
        CSystem::Instance()->OnCodePageChange();
    }

    Write();
#endif
}
/*============================================================================*/

    void                            CStringLib::SaveTo(
    CHLMSG                          *pMSG,
    BOOL                            bIncludeDefaultStrings )

/*============================================================================*/
{
    pMSG->PutInt(m_iCodePage);

#ifdef GATEWAY
    CHLObList MissingStrings;

    if (bIncludeDefaultStrings)
    {
        CHLFile file(FILE_XSTRINGS);

        CHLObList List;
        CAudioServer::Instance()->LoadTemplateStrings(&List);

        while (!List.IsEmpty())
        {
            XStringPair *pX = (XStringPair*)List.RemoveHead();
            CHLString sKey;
            CHLString sContext;
            if (pX->Key() != NULL)
                sKey = pX->Key();
            if (pX->Context() != NULL)
                sContext = pX->Context();

            if (!FindXStringPair(sKey, sContext))
            {
                MissingStrings.AddTail(new CHLString(sKey));
                MissingStrings.AddTail(new CHLString(sContext));
            }

            delete pX;            
        }

        if (file.Open(CHLFile::MODE_READ))
        {
            int iN = 0;
            file.ReadInt(&iN);
            for (int i = 0; i < iN; i++)
            {
                CHLString sKey, sContext;
                file.ReadString(&sKey);
                file.ReadString(&sContext);
                if (!FindXStringPair(sKey, sContext))
                {
                    MissingStrings.AddTail(new CHLString(sKey));
                    MissingStrings.AddTail(new CHLString(sContext));
                }
            }
        }
    }

    pMSG->PutInt(m_nXStringPairs + MissingStrings.GetCount()/2);
    for (int i = 0; i < m_nXStringPairs; i++)
    {
        XStringPair *pX = m_ppXStringPairs[i];
        pMSG->PutString(pX->Key());
        CHLString sContext;
        if (pX->Context() != NULL)
            sContext = pX->Context();
        pMSG->PutString(sContext);
        pMSG->PutString(*pX->Value());
    }
    while (!MissingStrings.IsEmpty())
    {
        CHLString *pKey     = (CHLString*)MissingStrings.RemoveHead();
        CHLString *pContext = (CHLString*)MissingStrings.RemoveHead();
        pMSG->PutString(*pKey);
        pMSG->PutString(*pContext);
        pMSG->PutString(*pKey);
        delete pKey;
        delete pContext;
    }

#else
    pMSG->PutInt(m_nXStringPairs);
    for (int i = 0; i < m_nXStringPairs; i++)
    {
        XStringPair *pX = m_ppXStringPairs[i];
        pMSG->PutString(pX->Key());
        pMSG->PutString(pX->Context());
        pMSG->PutString(*pX->Value());
    }
#endif
}
/*============================================================================*/

    void                            CStringLib::SortByKey()

/*============================================================================*/
{
    qsort(m_ppXStringPairs, m_nXStringPairs, sizeof(XStringPair*), CStringLib::CompareKeyStrings);
}
/*============================================================================*/

    void                            CStringLib::SortByContext()

/*============================================================================*/
{
    qsort(m_ppXStringPairs, m_nXStringPairs, sizeof(XStringPair*), CStringLib::CompareContextStrings);
}
/*============================================================================*/

    int                             CStringLib::Compare(
    const void                      *arg1,
    const void                      *arg2 )

/*============================================================================*/
{
    XStringPair *pItem1 = *(XStringPair**)arg1;
    XStringPair *pItem2 = *(XStringPair**)arg2;

    CHLString sText1 = pItem1->Key();
    CHLString sText2 = pItem2->Key();

//  return sText1.CompareNoCase(sText2);
    return sText1.Compare(sText2);
}
/*============================================================================*/

    int                             CStringLib::CompareKeyStrings(
    const void                      *arg1,
    const void                      *arg2 )

/*============================================================================*/
{
    XStringPair *pItem1 = *(XStringPair**)arg1;
    XStringPair *pItem2 = *(XStringPair**)arg2;

    return strcmp(pItem1->Key(), pItem2->Key());
}
/*============================================================================*/

    int                             CStringLib::CompareContextStrings(
    const void                      *arg1,
    const void                      *arg2 )

/*============================================================================*/
{
    XStringPair *pItem1 = *(XStringPair**)arg1;
    XStringPair *pItem2 = *(XStringPair**)arg2;
    char sNull = 0;
    char *s1 = pItem1->Context();
    char *s2 = pItem2->Context();
    if (s1 == NULL)  s1 = &sNull;
    if (s2 == NULL)  s2 = &sNull;

    int iCmp = strcmp(s1, s2);
    if (iCmp == 0)
        // Context is the same, compare the KeyStrings
        return strcmp(pItem1->Key(), pItem2->Key());
    else
        return iCmp;
}
/*============================================================================*/

    void                            CStringLib::RemoveLangStrings()

/*============================================================================*/
{
    // new table
    for(int i = 0; i < m_nXStringPairs; i++)
    {
        delete m_ppXStringPairs[i];
    }
    delete [] m_ppXStringPairs;
    m_ppXStringPairs = NULL;
    m_nXStringPairs = 0;
}
/*============================================================================*/

    void                            CStringLib::FixEscapes(
    CHLString                       *pString )

/*============================================================================*/
{
    int iLen = pString->GetLength();
    int i = 0;
    int ii = 0;
    if (pString->Type() == CHLString::HLSTRING_UNICODE)
    {
        wchar_t* pBuffer = pString->GetBufferWCHAR(iLen);
        while (i < iLen)
        {
            if (pBuffer[i] == '\\')
            {
                // convert literal "\n" chars to <LF> char
                if (pBuffer[i+1] == 'n')
                {
                    pBuffer[ii] = 0x0A;
                    i += 2;
                    ii++;
                    continue;
                }
            }
            if (i > ii)
                pBuffer[ii] = pBuffer[i];

            i++;
            ii++;
        }
        if (ii < iLen)
            pString->ReleaseBuffer(ii);
    }
    else
    {
        char* pBuffer = pString->GetBufferCHAR(iLen);
        while (i < iLen)
        {
            if (pBuffer[i] == '\\')
            {
                // convert literal "\n" chars to <LF> char
                if (pBuffer[i+1] == 'n')
                {
                    pBuffer[ii] = 0x0A;
                    i += 2;
                    ii++;
                    continue;
                }
            }
            if (i > ii)
                pBuffer[ii] = pBuffer[i];

            i++;
            ii++;
        }
        if (ii < iLen)
            pString->ReleaseBuffer(ii);
    }
}
/*============================================================================*/

    BOOL                            CStringLib::ValidateFormat(
    XStringPair                     *pPair )

/*============================================================================*/
{
    if (pPair->Context() != NULL)
    {
        if (strstr(pPair->Context(), "KYBD"))
            return TRUE;
    }

    char*  pKey = pPair->Key();
    TCHAR* pXString = pPair->Value()->GetBufferTCHAR(0);
    int iLenK = (int)strlen(pPair->Key());
    int iLenX = pPair->Value()->GetLength();
    BOOL bError = FALSE;
    int ix = 0;
    for (int i = 0; i < iLenK-1; i++)
    {
        if (pKey[i] == '%')
        {
            i++;
            while ((ix < iLenX-1) && (pXString[ix] != '%'))
                ix++;
            if (pXString[ix] == '%')
            {
                ix++;
                if ((int)pKey[i] != (int)pXString[ix])
                {
                    bError = TRUE;
                    break;
                }
            }
            else
            {
                bError = TRUE;
                break;
            }
        }
    }
    if (bError)
    {
        LOGASSERT(0);
        CHLString *pXString = pPair->Value();
        int iType = pXString->Type();
        delete pXString;
        pPair->m_pXString = new CHLString(pPair->Key());
#ifndef MFC_CSTRING
        if (iType == CHLString::HLSTRING_UNICODE)
            pPair->m_pXString->ConvertTowchar_t();
#endif
    }

    return bError;
}
