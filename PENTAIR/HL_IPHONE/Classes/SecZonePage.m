//
//  SecZonePage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecZonePage.h"
#import "SecPartition.h"
#import "SecZone.h"
#import "SecZoneCell.h"
#import "MainView.h"

@implementation CSecZonePage
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame]) 
    {
        m_pPartition = pPartition;

        //
        // Table View
        //
        CGRect rTable = self.bounds;
        m_pTableView = [[UITableView alloc] initWithFrame:rTable style:UITableViewStylePlain];
        
        // set the autoresizing mask so that the table will always fill the view
        m_pTableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
        
        // set the tableview delegate to this object and the datasource to the datasource which has already been set
        m_pTableView.delegate = self;
        m_pTableView.dataSource = self;
        m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        m_pTableView.rowHeight = [CSecZonePage RowHeight];
        UIEdgeInsets insets = m_pTableView.contentInset;
        insets.top = 10;
        insets.bottom = 10;
        [m_pTableView setContentInset:insets];

        m_pTableView.backgroundColor = [UIColor clearColor];
        
        // set the tableview as the controller view
        [self addSubview:m_pTableView];
        m_pTableView.autoresizingMask = 0xFFFFFFFF;
        self.autoresizingMask = 0xFFFFFFFF;
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTableView release];
    [super dealloc];
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return 1;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    return [m_pPartition NZones];
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    static NSString *CellIdentifier = @"Cell";

    if (indexPath == NULL)
        return NULL;

    CSecZoneCell *cell = (CSecZoneCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[CSecZoneCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier Partition:m_pPartition] autorelease];
    }

    CSecZone *pZone = [m_pPartition Zone:indexPath.row];

    [cell SetZone:pZone];

    // Configure the cell
    return cell;
}
/*============================================================================*/

+(int)RowHeight

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return 40;
    return 26;
}
/*============================================================================*/

+(int)TextHeight

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return 32;
    return 18;
}

@end
