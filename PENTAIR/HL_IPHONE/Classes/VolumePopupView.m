//
//  VolumePopupView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/2/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "VolumePopupView.h"
#import "AudioZone.h"
#import "hlm.h"
#import "sink.h"
#import "MainView.h"
#import "hlprogress.h"
#import "crect.h"
#import "HLToolBar.h"

@implementation CVolumePopupView

    #define PROGRESS_SIZE_IPAD          30
    #define PROGRESS_SIZE_IPOD          20

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone Direction:(int)iDir

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        // Initialization code
        m_pZone = pZone;
        [m_pZone AddSink:self];
        
        self.opaque = NO;

        rect.size.height = rect.size.height / 2;
        
        if (m_pZone.m_iVolumeType != VOLUME_TYPE_RAMP)
        {
            // 2-Way Show Volume Bar
            CGRect rProgress = self.bounds;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                [self setBackgroundColor:[UIColor blackColor]];
                [CRect Inset:&rProgress DX:20 DY:10];
            }
            else 
            {
                [CRect Inset:&rProgress DX:5 DY:2];
                [CRect CenterDownToY:&rProgress DY:20];
            }


            m_pProgress = [[CHLProgress alloc] initWithFrame:rProgress];

            [self addSubview:m_pProgress];
            [self Notify:0 Data:SINK_VOLCHANGED];

            CGRect rHL = self.bounds;
            rHL = [CRect BreakOffTop:&rHL DY:rHL.size.height/2];
            CToolBarHighlight *pHL = [[CToolBarHighlight alloc] initWithFrame:rHL];
            [pHL setOpaque:NO];
            [pHL setBackgroundColor:[UIColor clearColor]];
            [self addSubview:pHL];
            [pHL release];
        }
        else
        {
            // 1-Way Show Animation Thing
        }

		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

        CGFloat colors[] =
        {
              0.0,        0.0,          0.0,     0.90,
              0.0,        0.0,          0.0,     0.90,
        };

        m_Gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
		CGColorSpaceRelease(rgb);
    }


    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return;
    }

    CGContextRef context = UIGraphicsGetCurrentContext();
    int iRad = 0;

    CGPoint ptStart = CGPointMake(rect.origin.x + iRad, rect.origin.y);
    CGPoint ptEnd = ptStart;

    ptEnd.y += rect.size.height;


    int iX = rect.origin.x;
    int iY = rect.origin.y;
    int iDX = rect.size.width;
    int iDY = rect.size.height;

    CGContextSaveGState(context);

    CGContextBeginPath (context);
    CGContextMoveToPoint(context, iX+iRad, iY);
    CGContextAddArcToPoint(context, iX+iDX, iY, iX+iDX, iY+iRad, iRad);
    CGContextAddArcToPoint(context, iX+iDX, iY+iDY, iX+iDX-iRad, iY+iDY, iRad);
    CGContextAddArcToPoint(context, iX, iY+iDY, iX, iY+iDY-iRad, iRad);
    CGContextAddArcToPoint(context, iX, iY, iX+iRad, iY, iRad);

	CGContextClip(context);
    CGContextDrawLinearGradient(context, m_Gradient, ptStart, ptEnd, 0);

    CGContextRestoreGState(context);
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [m_pProgress release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_UIVOLUMEEND)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [self setAlpha:0.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
        [UIView commitAnimations];

    }

    if ((iData & SINK_VOLCHANGED) == 0)
        return;
     if (m_pZone.m_iVolumeType == VOLUME_TYPE_RAMP)
        return;

    [m_pProgress SetProgress:[m_pZone Volume]];
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [self removeFromSuperview];
}

@end
