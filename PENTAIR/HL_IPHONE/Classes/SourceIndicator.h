//
//  SourceIndicator.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"

@class CSourceIconView;
@class CAudioZone;

/*============================================================================*/

@interface CSourceIndicator : UIView < CSink >

/*============================================================================*/
{
    UILabel                             *m_pLabel;
    CSourceIconView                     *m_pIcon;
    CAudioZone                          *m_pZone;

}

-(id)initWithFrame:(CGRect) rFrame Zone:(CAudioZone*)pZone;
-(void)Notify:(int)iID Data:(int)iData;

@end
