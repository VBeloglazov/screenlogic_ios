//
//  SubTab.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

@class CSubTabCell;
@class CSubTabViewOverlay;

/*============================================================================*/

@interface CSubTab : NSObject 

/*============================================================================*/
{
    NSString                            *m_pName;
    int                                 m_iID;
    int                                 m_iData1;
    int                                 m_iData2;
    CSubTabCell                         *m_pSubTabCell;
    //UIViewController                    *m_pMainViewController;
}

@property (nonatomic, retain) NSString *m_pName;
@property (nonatomic) int       m_iID;
@property (nonatomic) int       m_iData1;
@property (nonatomic) int       m_iData2;

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;

-(CSubTabCell*)GetSubTabCell;
-(void)SetSubTabCell:(CSubTabCell*)pSubTabCell;
-(NSString*)Name;
//-(void)SetMainViewController:(UIViewController*)mainViewController;

@end
