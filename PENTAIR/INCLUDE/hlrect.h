/*
--------------------------------------------------------------------------------

    HLRECT.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLRECT_H
#define HLRECT_H

#include "hlapi.h"

#undef RECT_INTERFACE
#if defined (CRYSTALPAD) | defined(HLCONFIG) | defined(HLOSD)
    #define RECT_INTERFACE
    class CHLWindow;
#endif

    #define MAX_RECT_LIST               256

/*============================================================================*/

    class HL_API CHLRect

/*============================================================================*/
{
protected:
    int                         m_iXLeft;
    int                         m_iYTop;
    int                         m_iDX;          // Width, inclusive (>=1)
    int                         m_iDY;          // Height, inclusive (>=1)


public:
    CHLRect()
    {
        m_iXLeft = 0;
        m_iYTop  = 0;
        m_iDX    = 0;
        m_iDY    = 0;
    }

    CHLRect(
    CHLRect                     *pSrc )
    {
        LOGASSERT(pSrc != NULL);
        m_iXLeft    = pSrc->m_iXLeft;
        m_iYTop     = pSrc->m_iYTop;
        m_iDX       = pSrc->m_iDX;
        m_iDY       = pSrc->m_iDY;
    }

    virtual ~CHLRect();

protected:


public:
        
	CHLRect(
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY )
    {
        m_iXLeft = iXLeft;
        m_iYTop  = iYTop;
        m_iDX    = iDX;
        m_iDY    = iDY;
    }
		
    CHLRect                     AtOrigin()          { return CHLRect(0, 0, m_iDX, m_iDY); }

    BOOL                        IsEqualTo(
    CHLRect                     *pOther );

    int                         XLeft()             { return m_iXLeft; }
    void                        XLeft(int iNew)     { int iDX = iNew - m_iXLeft; m_iXLeft = iNew; m_iDX -= iDX; }
    int                         YTop()              { return m_iYTop; }
    void                        YTop(int iNew)      { int iDY = iNew - m_iYTop; m_iYTop = iNew; m_iDY -= iDY; }
    int                         DX()                { return m_iDX; }
    void                        DX(int iNew)        { m_iDX = iNew; }
    int                         DY()                { return m_iDY; }
    void                        DY(int iNew)        { m_iDY = iNew; }

    int                         XRight()            { return m_iXLeft + m_iDX - 1; }
    void                        XRight(int iNew)    { m_iDX = iNew - m_iXLeft + 1; }
    int                         YBottom()           { return m_iYTop + m_iDY - 1; }
    void                        YBottom(int iNew)   { m_iDY = iNew - m_iYTop + 1; }

    int                         XMid()              { return m_iXLeft + (m_iDX / 2); }
    int                         YMid()              { return m_iYTop + (m_iDY / 2); }

    const CHLRect               &Inset(
    int                         iDX,
    int                         iDY );

    const CHLRect               &Offset(
    int                         iDX,
    int                         iDY );

    const CHLRect               &Translate(
    int                         iDX,
    int                         iDY );

    #ifdef RECT_INTERFACE
        const CHLRect           &ClientToScreen(
        CHLWindow               *pWnd );

        const CHLRect           &ScreenToClient(
        CHLWindow               *pWnd );
    #endif

    void                        CenterDownTo(
    int                         iDXMax,
    int                         iDYMax );

    void                        CenterUpTo(
    int                         iDXMin,
    int                         iDYMin );

    RECT                        GetRECT();

    CHLRect                     CreateSplitLeft( int iDX );
    CHLRect                     CreateSplitRight( int iDX );
    CHLRect                     CreateSplitTop( int iDY );
    CHLRect                     CreateSplitBottom( int iDY );

    CHLRect                     ConfineToAspect(
    int                         iDX,
    int                         iDY );

    CHLRect                     CreateXSection(
    int                         iIndex,
    int                         iNSections,
    int                         iBorder );

    CHLRect                     CreateXSectionWeighted(
    int                         iIndex,
	int							iIndexWeight,
    int                         iNSections,
	int							iPrevRectRight,
    int                         iBorder );

	CHLRect                     BreakOffLeft(
    int                         iDX,
    int                         iBuffer );

    CHLRect                     BreakOffRight(
    int                         iDX,
    int                         iBuffer );

    CHLRect                     BreakOffTop(
    int                         iDY,
    int                         iBuffer );

    CHLRect                     BreakOffBottom(
    int                         iDY,
    int                         iBuffer );

    CHLRect                     *CreateBreakOffLeft(
    int                         iDX,
    int                         iBuffer );

    CHLRect                     *CreateBreakOffRight(
    int                         iDX,
    int                         iBuffer );

    CHLRect                     *CreateBreakOffTop(
    int                         iDY,
    int                         iBuffer );

    CHLRect                     *CreateBreakOffBottom(
    int                         iDY,
    int                         iBuffer );

    CHLRect                     CreateYSection(
    int                         iIndex,
    int                         iNSections,
    int                         iBorder );

    CHLRect                     CreateVerticalSlice(
    int                         iX1,
    int                         iX2 );

    CHLRect                     BlendWith(
    CHLRect                     r,  
    int                         i1000 );

    CHLRect                     CreateInset(
    int                         iDXEdge,
    int                         iDYEdge );

    BOOL                        IsPointInside(
    POINT                       p );

    void                        Flip(
    BOOL                        bHz,
    BOOL                        bVt,
    int                         iDXBounds,
    int                         iDYBounds );

    CHLRect                     IntersectWith(
    CHLRect                     rSubRect );

    BOOL                        IntersectsWith(
    CHLRect                     *pOther );

    BOOL                        IsInside(
    CHLRect                     *pOther );

    BOOL                        ContainsX(
    int                         iX );

    BOOL                        ContainsY(
    int                         iY );

    BOOL                        CombineWith(
    CHLRect                     *pOther );

    BOOL                        SameTopBottom(
    CHLRect                     *pOther );

    BOOL                        SameLeftRight(
    CHLRect                     *pOther );

    BOOL                        IsWithinXBoundsOf(
    CHLRect                     *pOther );

    BOOL                        IsWithinYBoundsOf(
    CHLRect                     *pOther );
};

/*============================================================================*/

    class CDXYRect :            public CHLRect

/*============================================================================*/
{
public:
    CDXYRect(
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY )

    : CHLRect(iXLeft, iYTop, iDX, iDY)
    {
    }
};

/*============================================================================*/

    class CX12Rect :            public CHLRect

/*============================================================================*/
{
public:
    CX12Rect(
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 )

    : CHLRect(iX1, iY1, iX2 - iX1 + 1, iY2 - iY1 + 1)
    {
    }
};
/*============================================================================*/

    class HL_API CRectList

/*============================================================================*/
{
protected:
    int                         m_iX0[MAX_RECT_LIST];
    int                         m_iY0[MAX_RECT_LIST];
    int                         m_iDX[MAX_RECT_LIST];
    int                         m_iDY[MAX_RECT_LIST];

    int                         m_iNRects;

public:
    CRectList();
    virtual ~CRectList();

    CRectList                   *CreateCopy();
    
    void                        CopyFrom(
    CRectList                   *pOther );

//    void                        AddRect(
//    CHLRect                     rect );    

    void                        AddRectRef(
    CHLRect                     *pRect );    

    void                        AddRectDirectRef(
    CHLRect                     *pRect );

    void                        AddRectDirectDXDY(
    int                         iX0,
    int                         iY0,
    int                         iDX,
    int                         iDY );

    void                        AppendTo(
    CRectList                   *pOther );

    void                        SubtractFrom(
    CRectList                   *pOther );

    void                        Subtract(
    CHLRect                     *pRect );

    void                        Clear();

    void                        Translate(
    int                         iDX,
    int                         iDY );

    inline int                  NRects()        { return m_iNRects;     }
    CHLRect                     RectAtIndex(int iIndex);

    inline int                  X(int iIndex)   { return m_iX0[iIndex]; }
    inline int                  Y(int iIndex)   { return m_iY0[iIndex]; }
    inline int                  DX(int iIndex)  { return m_iDX[iIndex]; }
    inline int                  DY(int iIndex)  { return m_iDY[iIndex]; }

    inline void                 XLeft(int iIndex, int iNew)     { int iDX = iNew - m_iX0[iIndex]; m_iX0[iIndex] = iNew; m_iDX[iIndex] -= iDX; }
    inline void                 YTop(int iIndex, int iNew)      { int iDY = iNew - m_iY0[iIndex]; m_iY0[iIndex] = iNew; m_iDY[iIndex] -= iDY; }
    inline void                 XRight(int iIndex, int iNew)    { m_iDX[iIndex] = iNew - m_iX0[iIndex] + 1; }
    inline void                 YBottom(int iIndex, int iNew)   { m_iDY[iIndex] = iNew - m_iY0[iIndex] + 1; }

    inline void                 DX(int iIndex, int iNew)        { m_iDX[iIndex] = iNew; }
    inline void                 DY(int iIndex, int iNew)        { m_iDY[iIndex] = iNew; }

    inline int                  XRight(int iIndex)  { return m_iX0[iIndex]+m_iDX[iIndex]-1; }
    inline int                  YBottom(int iIndex) { return m_iY0[iIndex]+m_iDY[iIndex]-1; }

    BOOL                        RectAtIndexContains(
    int                         iIndex,
    CHLRect                     *pTest );

    BOOL                        RectAtIndexIsInside(
    int                         iIndex,
    CHLRect                     *pTest );

    BOOL                        RectAtIndexIsWithinXBoundsOf(
    int                         iIndex,
    CHLRect                     *pTest );

    BOOL                        RectAtIndexIsWithinYBoundsOf(
    int                         iIndex,
    CHLRect                     *pTest );

    BOOL                        RectAtIndexIntersectsWith(
    int                         iIndex,
    CHLRect                     *pTest );

    void                        Consolidate();

    void                        SortForTranslate(
    int                         iDX,
    int                         iDY );

    BOOL                        CombineAtIndex(
    int                         iIndex,
    CHLRect                     *pTest );

    void                        CombineHz(int iDstIndex, int iSrcIndex);
    void                        CombineVt(int iDstIndex, int iSrcIndex);

    inline void                 Swap(int i1, int i2)  { int iX = m_iX0[i1];   
                                                        int iY = m_iY0[i1];   
                                                        int iDX = m_iDX[i1]; 
                                                        int iDY = m_iDY[i1]; 
                                                        m_iX0[i1] = m_iX0[i2];
                                                        m_iY0[i1] = m_iY0[i2];
                                                        m_iDX[i1] = m_iDX[i2];
                                                        m_iDY[i1] = m_iDY[i2]; 
                                                        m_iX0[i2] = iX;
                                                        m_iY0[i2] = iY;
                                                        m_iDX[i2] = iDX;
                                                        m_iDY[i2] = iDY; };


    inline BOOL                 SameTopBottom(int iIndex, CHLRect *pTest)   { return (Y(iIndex) == pTest->YTop() && YBottom(iIndex) == pTest->YBottom());   }
    inline BOOL                 SameLeftRight(int iIndex, CHLRect *pTest)   { return (X(iIndex) == pTest->XLeft() && XRight(iIndex) == pTest->XRight());    }

    BOOL                        RectAtIndexContainsX(int iIndex, int iX);
    BOOL                        RectAtIndexContainsY(int iIndex, int iX);

    void                        SplitBreakOffLeft(int iIndex, int iDX);
    void                        SplitBreakOffRight(int iIndex, int iDX);
    void                        SplitBreakOffTop(int iIndex, int iDY);
    void                        SplitBreakOffBottom(int iIndex, int iDY);

    void                        DeleteRect(
    int                         iIndex );

    DWORD                       Area();

    CHLRect                     GetBounds();
};

#endif

