//
//  tconnect.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "hlcomm.h"
#import "NSStatusSink.h"
#import "VERSION.H"

#import "tconnect.h"
#import "ControllerDescriptor.h"
#import "DLSegmentedControl.h"
#import "ConnectionEditor.h"

NSString *const pentair00 = @"Pentair: 00-00-00";
NSString *const pentairDemoServer = @"Demo Server";

@implementation CConnectPage
/*============================================================================*/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Orientation:(UIInterfaceOrientation)orientation;

/*============================================================================*/
{
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) 
    {
		// Initialization code
        m_Orientation = orientation;
	}

	return self;
}


 //Implement loadView if you want to create a view hierarchy programmatically
//- (void)loadView
//{
    //this is just for the test
//    m_LabelPreviousConnection.textColor = [UIColor blueColor];
//    m_ConnectionTableScroller.backgroundColor = [UIColor blueColor];
    //add some stuff to connections scroller
    
    //UIButton* aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //[aButton setTag:1];
    //[m_ConnectionTableScroller addSubview:aButton];
    
    //self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//}

/*============================================================================*/

- (void)viewDidLoad 

/*============================================================================*/
{
    m_iYConnectLabel    = m_pConnectLabel.frame.origin.y;
    m_iYMode            = m_pMode.frame.origin.y;
    m_iYActivity        = m_pActivity.frame.origin.y;
    m_iYStatus          = m_pStatus.frame.origin.y;

    
    m_pEnableIOTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(EnableIOAfterTimeout) userInfo:nil repeats:NO] retain];

    [self LoadSettings];

    if (m_pWiFiTimer == NULL)
        m_pWiFiTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(WiFiTimer) userInfo:nil repeats:YES] retain];

    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [m_pVersion setText:version];
    //[m_pVersion setText:@HL_VERSION];
            
    [CHLComm SuspendIO:TRUE];

    #ifdef PENTAIR
        [m_pLogo setImage:[UIImage imageNamed:@"PWPS_fish.png"]];
    #endif
    
    //customize connect button
    m_pConnectBtn.layer.cornerRadius = 6;
    m_pConnectBtn.layer.borderWidth = 1;
    m_pConnectBtn.layer.borderColor = [UIColor colorWithRed:0.0 green:122.0/255 blue:1.0 alpha:1.0].CGColor;
    m_pConnectBtn.layer.backgroundColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1.0].CGColor;
    
    [m_pName addTarget:self action:@selector(GoEvent:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [m_pPass addTarget:self action:@selector(GoEvent:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [m_pActivity setHidden:TRUE];
    [m_pStatus setHidden:TRUE];
    
    //CGSize s = CGSizeMake(220.0, 200.0);
    //[m_ConnectionTableScroller setContentSize:s];
    [m_ConnectionTableScroller setAlwaysBounceVertical:YES];
    //[m_ConnectionTableScroller setFrame:rect];
    //[m_ConnectionTableScroller setShowsVerticalScrollIndicator:YES];
    
    NSArray *subViews = [m_ConnectionTableScroller subviews];
    for(UIView *v in subViews)[v removeFromSuperview];
    
    //CGRect frLogo = m_pLogo.frame;
    //frLogo.origin.y = m_pConnectBtn.frame.origin.y;
    //[m_pLogo setFrame:frLogo];
    
    
    [self refreshControllerList];
    //select appropriate connection view
    NSString *psNIB = NULL;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        psNIB = @"connectionEditor-iPad";
        switch (self.interfaceOrientation)
        {
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
                psNIB = @"connectionEditor-iPad_ls";
                break;
                
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
                break;
        }
    }
    else
    {
        psNIB = @"connectionEditor";
        switch (self.interfaceOrientation)
        {
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
                psNIB = @"tconnect_ls";
                break;
                
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
                break;
        }
        
        //and finally adjust bottom controls
        if([[UIScreen mainScreen] bounds].size.height > 480)
        {
            int shiftDown = 30;
            int scrollerHeightDelta = 50;
            
            CGRect deltaRect = m_LabelPreviousConnection.frame;
            deltaRect.origin.y += shiftDown;
            [m_LabelPreviousConnection setFrame:deltaRect];

            
            deltaRect = m_ConnectionTableScroller.frame;
            deltaRect.origin.y += shiftDown;
            deltaRect.size.height += scrollerHeightDelta;
            [m_ConnectionTableScroller setFrame:deltaRect];
            
            deltaRect = m_pStatus.frame;
            deltaRect.origin.y += (shiftDown + scrollerHeightDelta);
            [m_pStatus setFrame:deltaRect];
            
            deltaRect = m_pVersion.frame;
            deltaRect.origin.y += (shiftDown + scrollerHeightDelta);
            [m_pVersion setFrame:deltaRect];
        }
    }
    

    
    
    editConnectionView = [[ConnectionEditor alloc] initWithNibName:psNIB bundle:[NSBundle mainBundle] Orientation:self.interfaceOrientation];
    //[self.view addSubview:editConnectionView.view];
    
    //DL this makes status bar be visile (iPhone)
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        UIView *addStausbar = [[UIView alloc] init];
        addStausbar.frame = CGRectMake(0,0,self.view.frame.size.width,20);
        addStausbar.backgroundColor = [UIColor colorWithWhite:200.0 alpha:0.5];
        [self.view addSubview:addStausbar];
    }
    
    //float ver = [[[UIDevice currentDevice] systemVersion] floatValue];

}

-(void)refreshControllerList
{
    float dx = [m_ConnectionTableScroller frame].size.width;
    float dy =  120;//[m_ConnectionTableScroller frame].size.height;
    DBManager *dbManager = [DBManager getSharedInstance];
    
    controllerList = nil;
    controllerList = [dbManager getAllControllers];
    [m_ConnectionTableScroller.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    int scrollerHeight = 0;
    int count = 0;
    
    if([controllerList count] == 0)
    {
        ControllerDescriptor *cd = [[ControllerDescriptor alloc] init];
        cd.systemName = pentair00;
        cd.nickName = pentairDemoServer;
        cd.password = [[NSString alloc]initWithUTF8String:("guest")];

        [self addController:cd coounter:&count scrollerHeight:&scrollerHeight width:&dx height:&dy];
        //also add to the database
        DBManager *dbManager = [DBManager getSharedInstance];
        if(![dbManager ifExists:(NSString*)[cd systemName]])
        {
            [dbManager addController:cd];
        }
    }
    else
    {
        for(ControllerDescriptor *cd in controllerList)
        {
            [self addController:cd coounter:&count scrollerHeight:&scrollerHeight width:&dx height:&dy];
        }
    }
    scrollerHeight += dy;//44;
    CGSize s = CGSizeMake(220.0, scrollerHeight);
    [m_ConnectionTableScroller setContentSize:s];
    m_ConnectionTableScroller.layer.cornerRadius = 6;
    m_ConnectionTableScroller.layer.borderWidth = 1;
    //m_ConnectionTableScroller.layer.borderColor = [UIColor colorWithRed:0.0 green:122.0/255 blue:1.0 alpha:1.0].CGColor;
    m_ConnectionTableScroller.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    //DL this makes sure that scroller won't run away from the label afet return from connection editing
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
       CGRect frLabel = m_LabelPreviousConnection.frame;
        CGRect frScroll = m_ConnectionTableScroller.frame;
        frScroll.origin.y = frLabel.origin.y + frLabel.size.height * 1.5;
        [m_ConnectionTableScroller setFrame:frScroll];
        
        //CGRect frLogo = m_pLogo.frame;
        //frLogo.origin.y = m_pConnectBtn.frame.origin.y;
        //[m_pLogo setFrame:frLogo];
    }
    
}

-(void)addController:(ControllerDescriptor *)cd coounter:(int*)count scrollerHeight:(int*)scrollerHeight width:(float*)dx height:(float*)dy
{
    NSArray *itemArray = [NSArray arrayWithObjects: cd.nickName, @"Two", nil];
    DLSegmentedControl *segmentedControl = [[DLSegmentedControl alloc] initWithItems:itemArray];
    segmentedControl.frame = CGRectMake((((*dx)/100) * 20)/2 , (*count) * (*dy)/2.8 + 10, ((*dx)/100) * 80, 32.0);
    segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    [segmentedControl setObjectTag:(NSObject*)cd];
    [segmentedControl addTarget:self action:@selector(segmentButtonHandler:) forControlEvents:UIControlEventValueChanged];
    
    [segmentedControl setWidth:(((*dx)/100) * 80)/3 forSegmentAtIndex:1];
    [segmentedControl setImage:[self scaledImage:[UIImage imageNamed:@"edit_icon_mono.png"] scaledToSize:CGSizeMake(32.0, 24.0)]forSegmentAtIndex:1];
    
    [m_ConnectionTableScroller addSubview:segmentedControl];
    (*count)++;
    (*scrollerHeight) += segmentedControl.frame.size.height;
}

-(void)checkEditStatus:(ConnectionEditor *)controller isFinished:(int)status
{
    //BOOL st = status;
    switch (status)
    {
        case DELETE:
        {
            ControllerDescriptor* cd = editConnectionView.controllerDescriptor;
            DBManager *dbManager = [DBManager getSharedInstance];
            [dbManager deleteControllerByName:cd];
        }
            break;
        case SAVE:
        {
            ControllerDescriptor* cd = editConnectionView.controllerDescriptor;
            DBManager *dbManager = [DBManager getSharedInstance];
            [dbManager updateNickname:cd];
        }
            break;
        case CANCEL:
        default:
        {
            //do nothing
        }
            break;
    }
    [self refreshControllerList];
}

/*============================================================================*/
-(void)segmentButtonHandler:(DLSegmentedControl*)segment
/*============================================================================*/
{
    if(segment.selectedSegmentIndex == 0)
    {
        ControllerDescriptor* cd = (ControllerDescriptor*)segment.objectTag;
        [m_pName setText:cd.systemName];
        [m_pPass setText:cd.password];
        //[m_pName setText:(NSString*)((ControllerDescriptor*)segment.objectTag)];
    }
    else
    {
        //set it with data
        [editConnectionView setControllerDescriptor:(ControllerDescriptor*)segment.objectTag];
        //show
        //[self.view addSubview:editConnectionView.view];
        editConnectionView.checkStatusDelegate = self;
        editConnectionView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentModalViewController:editConnectionView animated:YES];
    }
    segment.selectedSegmentIndex = -1;
}
/*============================================================================*/

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation

/*============================================================================*/
{
    return [self shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

/*============================================================================*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 

/*============================================================================*/
{
	// Return YES for supported orientations
    switch(interfaceOrientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        case UIInterfaceOrientationLandscapeRight:
        case UIInterfaceOrientationLandscapeLeft:
        {
            return YES;
        }
            break;
        case UIInterfaceOrientationUnknown:
            return NO;
            break;
    }

    /*
    switch (interfaceOrientation)
    {
    case UIInterfaceOrientationPortrait:
    case UIInterfaceOrientationPortraitUpsideDown:
        {
            //switch (m_Orientation)
            //{
            //case UIInterfaceOrientationPortrait:
            //case UIInterfaceOrientationPortraitUpsideDown:
                return TRUE;

            //default:
            //    break;
            //}
        }
        break;

    case UIInterfaceOrientationLandscapeLeft:
    case UIInterfaceOrientationLandscapeRight:
        {
            //switch (m_Orientation)
            //{
            //case UIInterfaceOrientationLandscapeLeft:
            //case UIInterfaceOrientationLandscapeRight:
                return TRUE;

            //default:
            //    break;
            //}
        }
        break;
    }
    return NO;
     */
}

/*============================================================================*/

- (void)didReceiveMemoryWarning 

/*============================================================================*/
{
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}
/*============================================================================*/

-(void)SetStatusString:(NSString*)sStatus

/*============================================================================*/
{
    [m_pStatus performSelectorOnMainThread:@selector(setText:) withObject:sStatus waitUntilDone:YES];
    printf("%s", [sStatus UTF8String]);
    printf("\n");
}
/*============================================================================*/

-(IBAction)RemoteConnect:(id)sender

/*============================================================================*/
{
    [m_pActivity setAlpha:1];
    [m_pStatus setAlpha:1];
    [m_pActivity setHidden:FALSE];
    [m_pStatus setHidden:FALSE];
    [m_pActivity startAnimating];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [m_pName setAlpha:.5];
    [m_pPass setAlpha:.5];
    [m_pConnectBtn setAlpha:.5];
    [m_pMode setAlpha:.5];
    [UIView commitAnimations];

    [m_pName setEnabled:NO];
    [m_pPass setEnabled:NO];
    [m_pConnectBtn setEnabled :NO];
    [m_pMode setEnabled :NO];
    m_iConnectionState = CONNECTION_STATE_WORKING;

    m_pConnectTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(CheckConnectTimer) userInfo:nil repeats:YES] retain];

    [NSThread detachNewThreadSelector:@selector(ConnectThreadProc)  toTarget:self withObject:nil];
}
/*============================================================================*/

-(IBAction)ToggleAuto:(id)sender

/*============================================================================*/
{
    if (m_pMode.selectedSegmentIndex != 0)
    {
        [m_pStatus setHidden:TRUE];
        [m_pActivity stopAnimating];
        [CHLComm SuspendIO:TRUE];


        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [m_pNameLabel setAlpha:1];
        [m_pPassLabel setAlpha:1];
        [m_pName setAlpha:1];
        [m_pPass setAlpha:1];
        [m_pConnectBtn setAlpha:1];
        [m_ConnectionTableScroller setAlpha:1];
        [m_LabelPreviousConnection setAlpha:1];
        [self OffsetView:m_pConnectLabel Pos:m_iYConnectLabel];
        [self OffsetView:m_pMode Pos:m_iYMode];

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            [self OffsetView:m_pConnectBtn Pos:m_iYMode];

//        [self OffsetView:m_pActivity Pos:m_iYActivity];
//        [self OffsetView:m_pStatus Pos:m_iYStatus];
        [UIView commitAnimations];
        return;
    }

    [m_pStatus setHidden:FALSE];
    if (m_pEnableIOTimer == NULL)
        [m_pActivity startAnimating];


	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];

    [self OffsetView:m_pConnectLabel Pos:m_iYConnectLabel+125];
    [self OffsetView:m_pMode Pos:m_iYMode+125];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        [self OffsetView:m_pConnectBtn Pos:m_iYConnectLabel+125];

//    [self OffsetView:m_pActivity Pos:m_iYActivity-50];
//    [self OffsetView:m_pStatus Pos:m_iYStatus-50];
    
    //DL here we make "Remote" stuff invisible
    [m_pNameLabel setAlpha:0];
    [m_pPassLabel setAlpha:0];
    [m_pName setAlpha:0];
    [m_pPass setAlpha:0];
    [m_pConnectBtn setAlpha:0];
    [m_ConnectionTableScroller setAlpha:0];
    [m_LabelPreviousConnection setAlpha:0];

	[UIView commitAnimations];

    [CHLComm SuspendIO:FALSE];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [self SaveSettings];
    [m_pConnectTimer invalidate];
    [m_pConnectTimer release];
    [m_pWiFiTimer invalidate];
    [m_pWiFiTimer release];
    [m_LabelPreviousConnection release];
    [m_ConnectionTableScroller release];
	[super dealloc];
}
/*============================================================================*/

- (void)ConnectThreadProc

/*============================================================================*/
{
    NSAutoreleasePool *pool = [NSAutoreleasePool new];

    [CHLComm SuspendIO:TRUE];
    NSString *pName = m_pName.text;
    NSString *pPass = m_pPass.text;
    const char *pNameChar = [pName UTF8String];
    const char *pPassChar = [pPass UTF8String];

    // This is a bit HOKEY but here goes
    [self SetStatusString:@"Checking Connection..."];
    /*
    CFWriteStreamRef pWriter = NULL;
    CFStreamCreatePairWithSocketToHost (NULL, CFSTR("homelogic.com"), 80, NULL,&pWriter );
    
    if (!CFWriteStreamOpen(pWriter))
    {
        [self SetStatusString:@"Can't Connect to HomeLogic.com"];
        [CHLComm SuspendIO:FALSE];
        m_iConnectionState = CONNECTION_STATE_FAILED;
        if (pWriter != NULL)
            CFRelease(pWriter);
        [pool release];
        [NSThread exit];
        return;
    }

    unsigned char pZero[8];
    memset(pZero, 0, 8);
    CFWriteStreamWrite(pWriter, pZero, 8);


    CFWriteStreamClose(pWriter);
    CFRelease(pWriter);
     */
    
    if (![CHLComm CreateRemoteConnection:self Name:pNameChar Password:pPassChar])
    {
        m_iConnectionState = CONNECTION_STATE_FAILED;
        [pool release];
        [NSThread exit];
        return;
    }
    
    [CHLComm SuspendIO:FALSE];
    m_iConnectionState = CONNECTION_STATE_IDLE;
    [pool release];
    [NSThread exit];
}
/*============================================================================*/

-(void)CheckConnectTimer

/*============================================================================*/
{
    if (![CHLComm IsConnecting] && (m_iConnectionState == CONNECTION_STATE_IDLE))
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [m_pConnectBtn setAlpha:1];
        [m_pMode setAlpha:1];
        [m_pName setAlpha:1];
        [m_pPass setAlpha:1];
        [m_LabelPreviousConnection setAlpha:1];
        [UIView commitAnimations];

        [m_pName setEnabled:YES];
        [m_pPass setEnabled:YES];
        [m_pConnectBtn setEnabled:YES];
        [m_pMode setEnabled:YES];
        [m_pStatus setText:@"Searching for System Controller"];
    }

    [m_pConnectTimer invalidate];
    [m_pConnectTimer release];
    m_pConnectTimer = nil;
}
/*============================================================================*/

-(void)WiFiTimer

/*============================================================================*/
{
    if (m_iConnectionState == CONNECTION_STATE_FAILED)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:2.5];

        [m_pActivity setAlpha:0];
        [m_pStatus setAlpha:0];

        [UIView commitAnimations];

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [m_pConnectBtn setAlpha:1];
        [m_pMode setAlpha:1];
        [m_pPass setAlpha:1];
        [m_pName setAlpha:1];
        [m_LabelPreviousConnection setAlpha:1];
        [UIView commitAnimations];

        [m_pName setEnabled:TRUE];
        [m_pPass setEnabled:TRUE];
        [m_pMode setEnabled:TRUE];
        [m_pConnectBtn setEnabled:TRUE];
        m_iConnectionState = CONNECTION_STATE_IDLE;
    }

    if (m_pMode.selectedSegmentIndex != 0)
    {
        return;
    }

    if ([CHLComm IsConnecting] || m_iConnectionState == CONNECTION_STATE_WORKING)
        return;

    if ([CHLComm Connected])
    {
        [m_pWiFiTimer invalidate];
        [m_pWiFiTimer release];
        m_pWiFiTimer = NULL;
        return;
    }

    if (m_pEnableIOTimer != NULL)
    {
        [m_pActivity setHidden:TRUE];
        [m_pStatus setHidden:TRUE];
    }
    else
    {
        [m_pActivity setAlpha:1];
        [m_pStatus setAlpha:1];
        [m_pStatus setHidden:FALSE];
        [m_pActivity startAnimating];

        if ([CHLComm WirelessOK])
        {
            [m_pStatus setText:@"Searching for System Controller"];
            return;
        }
        else
        {
            [m_pStatus setText:@"Waiting for WiFi"];
        }
    }
    
    return;
}
/*============================================================================*/

-(void)EnableIOAfterTimeout;

/*============================================================================*/
{
    [m_pEnableIOTimer invalidate];
    [m_pEnableIOTimer release];
    m_pEnableIOTimer = NULL;

    if (m_pMode.selectedSegmentIndex != 0)
        return;
        
    if ([CHLComm IsConnecting]) 
        return;

    [CHLComm SuspendIO:FALSE];
}
/*============================================================================*/

-(void)LoadSettings

/*============================================================================*/
{
    NSString *pDefName  = [[NSUserDefaults standardUserDefaults] stringForKey:@"HLUserName"];
    NSString *pDefPass  = [[NSUserDefaults standardUserDefaults] stringForKey:@"HLUserPass"];

    #ifdef PENTAIR
        if (pDefName != nil)
            [m_pName setText:pDefName];
        else
        {
            //[m_pName setText:@"Pentair: 00-00-00"];
            [m_pName setText:@""];
        }
        if (pDefPass != nil)
            [m_pPass setText:pDefPass];
        else
        {
            //[m_pPass setText:@"guest"];
            [m_pPass setText:@""];
        }
    #else
        if (pDefName != nil)
            [m_pName setText:pDefName];
        else
            [m_pName setText:@"g!"];

        if (pDefPass != nil)
            [m_pPass setText:pDefPass];
        else
            [m_pPass setText:@"iphone"];
    #endif

    NSString *pMode = [[NSUserDefaults standardUserDefaults] stringForKey:@"HLMode"];

    //just for test DL
    //pMode = @"REMOTE";
    
    // NULL == Remote (default)
    if ([pMode compare:@"REMOTE"] == 0)
        pMode = NULL;

    if (pMode == NULL)
        [m_pMode setSelectedSegmentIndex:1];
    else
        [m_pMode setSelectedSegmentIndex:0];
    [self ToggleAuto:m_pMode];
}
/*============================================================================*/

-(void)SaveSettings

/*============================================================================*/
{
    NSString *pName = m_pName.text;
    NSString *pPass = m_pPass.text;
    NSUserDefaults *pDefaults = [NSUserDefaults standardUserDefaults];
    [pDefaults setObject:pName forKey:@"HLUserName"];
    [pDefaults setObject:pPass forKey:@"HLUserPass"];
    NSString *pMode = @"AUTO";
    if (m_pMode.selectedSegmentIndex != 0)
    {
        pMode = @"REMOTE";
        //add this system/pwd to db
        ControllerDescriptor *cd = [ControllerDescriptor new];
        [cd setSystemName:pName];
        [cd setNickName:pName];//for the first time it will be matching system name
        [cd setPassword:pPass];
        DBManager *dbManager = [DBManager getSharedInstance];
        if(![dbManager ifExists:(NSString*)[cd systemName]])
        {
            [dbManager addController:cd];
        }
    }
    [pDefaults setObject:pMode forKey:@"HLMode"];
}
/*============================================================================*/

-(IBAction)GoEvent:(id)sender

/*============================================================================*/
{
    [m_pName resignFirstResponder];
    [m_pPass resignFirstResponder];
    [self RemoteConnect:m_pConnectBtn];
}
/*============================================================================*/

-(void)ReleaseAll

/*============================================================================*/
{
    [m_pConnectTimer invalidate];
    [m_pConnectTimer release];
    [m_pWiFiTimer invalidate];
    [m_pWiFiTimer release];
    [m_pEnableIOTimer invalidate];
    [m_pEnableIOTimer release];
    m_pConnectTimer = NULL;
    m_pWiFiTimer = NULL;
    m_pEnableIOTimer = NULL;
}
/*============================================================================*/

-(void)OffsetView:(UIView*)pView Pos:(int)iY

/*============================================================================*/
{
    CGRect rFrame = pView.frame;
    rFrame.origin.y = iY;
    [pView setFrame:rFrame];
}
- (void)viewDidUnload {
    [m_LabelPreviousConnection release];
    m_LabelPreviousConnection = nil;
    [m_ConnectionTableScroller release];
    m_ConnectionTableScroller = nil;
    [self setDidRotate2:nil];
    [super viewDidUnload];
}

-(UIImage*)scaledImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return  newImage;
}

-(void)orientationChanged:(NSNotification*)object
{
    [UIView commitAnimations];
    [self dismissModalViewControllerAnimated:NO];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    int y = 0;
}

////DL trying to delay
//- (void)viewDidAppear: (BOOL)animated
//{
//    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeLeft;
//}

@end
