//
//  Weather.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/28/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "Weather.h"
#import "HLTableFrame.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "crect.h"
#import "IPAD_ViewController.h"
#import "RoundRegion.h"
#import "AppDelegate.h"
#import "CustomPage.h"

#define     IMAGE_SIZE                  64

#define     DAY_OF_WEEK_SIZE_VT         28
#define     TEMP_SIZE_VT                22
#define     DESCRIPTION_SIZE_VT         16

#define     DAY_OF_WEEK_SIZE_HZ         18
#define     TEMP_SIZE_HZ                18
#define     DESCRIPTION_SIZE_HZ         12


#define     DX_CELL                     150
#define     DY_CELL                     100
#define     DY_CELL_HZ                  175

@implementation CDayForecast
/*============================================================================*/

-(id) initWithMSG:(CNSMSG*)pMSG Index:(int)iIndex Comp:(NSDateComponents*)pComps Calendar:(NSCalendar*)pCal Helper:(CDateHelper*)pHelper;

/*============================================================================*/
{
   
    [pMSG GetTimeWith:pComps Calendar:pCal DateHelper:pHelper];
    
    //DL - was this
    //int iDay = pComps.weekday;
    
    //DL - become this
    NSDate *date = [pCal dateFromComponents:pComps];
    NSDateComponents *weekComps = [pCal components:NSWeekdayCalendarUnit fromDate:date];
    int iDay = (int)[weekComps weekday];
    
    switch (iDay)
    {
    case 2:   m_psDate = @"Monday";   break;
    case 3:   m_psDate = @"Tuesday";   break;
    case 4:   m_psDate = @"Wednesday";   break;
    case 5:   m_psDate = @"Thursday";   break;
    case 6:   m_psDate = @"Friday";   break;
    case 7:   m_psDate = @"Saturday";   break;
    case 1:   m_psDate = @"Sunday";   break;
    }


    m_iHigh     = [pMSG GetInt];
    m_iLow      = [pMSG GetInt];
    m_psText    = [[pMSG GetString] retain];

    m_iIndex = iIndex;
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_psText release];
    [m_psImageRef release];
    [super dealloc];
}
/*============================================================================*/

-(int)Index;                    { return m_iIndex;                                      }
-(NSString*)DayOfWeekText;      { return m_psDate;                                      }
-(NSString*)HighText;           { if (m_iHigh == FC_VALUE_UNKNOWN) return @""; return [NSString stringWithFormat:@"%d°", m_iHigh];  }
-(NSString*)LowText;            { if (m_iLow  == FC_VALUE_UNKNOWN) return @""; return [NSString stringWithFormat:@"%d°", m_iLow];   }
-(NSString*)Description         { return m_psText;                                      }

/*============================================================================*/

@end


@implementation CForecast
/*============================================================================*/

-(id)init

/*============================================================================*/
{
    m_pDays = [[NSMutableArray alloc] init];
    m_pSinkServer = [[CSinkServer alloc] init];
    [CHLComm AddSocketSink:self];
    [self Post];

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pSinkServer release];
    [CHLComm RemoveSocketSink:self];
    [m_pDays release];
    [m_psText release];
    [m_psWind release];
    [super dealloc];
}
/*============================================================================*/

-(int)CurrentTemp           { return m_iTemp;   }
-(NSString*)Conditions      { return m_psText;  }

/*============================================================================*/

-(void)ReadFrom:(CNSMSG*)pMSG

/*============================================================================*/
{
    [m_psText release];
    [m_psWind release];
    m_psText = NULL;
    m_psWind = NULL;
    [m_pDays removeAllObjects];

    NSCalendar *pCal = [NSCalendar currentCalendar];
    //NSCalendar *pCal = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    //[pCal setFirstWeekday:1];
    //NSDateComponents *pComps = [pCal components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *pComps = [[NSDateComponents alloc] init];
    CDateHelper *pHelper = [[CDateHelper alloc] init];

    [pMSG GetInt];                                              // Version
    [pMSG GetString];                                           // ZIP
    [pMSG GetTimeWith:pComps Calendar:pCal DateHelper:pHelper]; // Last Update
    [pMSG GetTimeWith:pComps Calendar:pCal DateHelper:pHelper]; // Last Request
    [pMSG GetString];                                           // Date Text
    m_psText        = [pMSG GetString];
    m_iTemp         = [pMSG GetInt];
    m_iHumidity     = [pMSG GetInt];
    m_psWind        = [pMSG GetString];
    m_iBaro         = [pMSG GetInt];
    m_iDewPoint     = [pMSG GetInt];
    m_iWindChill    = [pMSG GetInt];
    m_iVisibility   = [pMSG GetInt];
    
    [m_psText retain];
    [m_psWind retain];

    [m_pDays removeAllObjects];
    int iNDays      = [pMSG GetInt];
    for (int i = 0; i < iNDays; i++)
    {
        CDayForecast *pDay = [[CDayForecast alloc] initWithMSG:pMSG Index:i Comp:pComps Calendar:pCal Helper:pHelper];
        [m_pDays addObject:pDay];
        [pDay release];
    }

    [pComps release];
    [pHelper release];
}
/*============================================================================*/

-(int)NDays

/*============================================================================*/
{
    return [m_pDays count];
}
/*============================================================================*/

-(CDayForecast*)Day:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pDays count])
        return NULL;

    CDayForecast *pDay = (CDayForecast*)[m_pDays objectAtIndex:iIndex];
    return pDay;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        [self Post];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iMSG = [pMSG MessageID];
    switch (iMSG)
    {
    case HLM_WEATHER_FORECASTCHANGED:
        {
            [self Post];
        }
        break;

    case HLM_WEATHER_GETFORECASTA:
        {
            [self ReadFrom:pMSG];
            [m_pSinkServer NotifySinks:0 Data:0];
        }
        break;
    }
}
/*============================================================================*/

-(void)Post

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_WEATHER_GETFORECASTQ] autorelease];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
}


@end





@implementation CWeatherForecastView
/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
    self = [super initWithFrame:frame Title:@"Forecast"];
    if (self)
    {
        CWeatherScrollView *pScrollView = [[CWeatherScrollView alloc] initWithFrame:CGRectZero];
        CHLTableFrame *pFrame = [[CHLTableFrame alloc] initWithFrame:CGRectZero View:pScrollView];
        [m_pRgn2 SetClientView:pFrame];
        [pScrollView release];
        [pFrame release];
    }
    return self;
}
@end

@implementation CWeatherScrollView
/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
        m_pScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pScrollView];
        [m_pScrollView release];
        m_pForecast = [pDelegate Forecast];
        [m_pForecast AddSink:self];
        m_pDayViews = [[NSMutableArray alloc] init];

        [self Notify:0 Data:0];
    }
    return self;
}

//DL
///*============================================================================*/
//
//-(void)ConnectionChanged:(BOOL)bNowConnected
//
///*============================================================================*/
//{
//    [m_pForecast dealloc];
//    m_pForecast = nil;
//}

/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pDayViews release];
    [m_pForecast RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    for (int i = 0; i < [m_pDayViews count]; i++)
    {
        CDayForecastCell *pCell = (CDayForecastCell*)[m_pDayViews objectAtIndex:i];
        [pCell removeFromSuperview];
    }

    [m_pDayViews removeAllObjects];

    for (int i = 0; i < [m_pForecast NDays]; i++)
    {
        CDayForecast *pDayFC = [m_pForecast Day:i];
        CDayForecastCell *pCell = [[CDayForecastCell alloc] initWithFrame:(CGRectZero) Day:pDayFC];
        [m_pScrollView addSubview:pCell];
        [m_pDayViews addObject:pCell];
        [pCell release];
    }
   [self layoutSubviews];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iAspect = rBounds.size.width * 100 / rBounds.size.height;
    int iNDays = [m_pDayViews count];

    if (iAspect > 200)
    {
        //
        // Horizontal Mode
        //
        [CRect CenterDownToX:&rBounds DX:DX_CELL * iNDays];
        [m_pScrollView setFrame:rBounds];
        [m_pScrollView setContentSize:CGSizeMake(DX_CELL * iNDays, rBounds.size.height)];
        rBounds = m_pScrollView.bounds;
        for (int i = 0; i < iNDays; i++)
        {
            CDayForecastCell *pCell = (CDayForecastCell*)[m_pDayViews objectAtIndex:i];
            [pCell Orientation:ORIENTATION_HZ];
            CGRect rDay = [CRect CreateXSection:rBounds Index:i Of:iNDays];
            [CRect CenterDownToY:&rDay DY:DY_CELL_HZ];
            //DL this makes weather view to show hi and lo temp not clipped in landscape
            rDay.size.height -= 20;
            
            [pCell setFrame:rDay];
        }
    }
    else
    {
        //
        // Vertical Mode
        //
        [CRect CenterDownToY:&rBounds DY:DY_CELL * iNDays];
        [m_pScrollView setFrame:rBounds];
        [m_pScrollView setContentSize:CGSizeMake(rBounds.size.width, DY_CELL * iNDays)];
        rBounds = CGRectMake(0, 0,self.bounds.size.width, DY_CELL*iNDays);
        for (int i = 0; i < iNDays; i++)
        {
            CDayForecastCell *pCell = (CDayForecastCell*)[m_pDayViews objectAtIndex:i];
            CGRect rDay = [CRect CreateYSection:rBounds Index:i Of:iNDays];
            [pCell Orientation:ORIENTATION_VT];
            [pCell setFrame:rDay];
        }
    }
}

@end

@implementation CDayForecastCell
/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Day:(CDayForecast*)pDay

/*============================================================================*/
{
	self = [super initWithFrame:CGRectZero];
    if (self)
    {
        [self SetDay:pDay];
        [self setOpaque:NO];
        [self setBackgroundColor:[CHLComm GetRGB:RGB_LIST_AREA]];
    }

    return self;
}
/*============================================================================*/

-(void)SetDay:(CDayForecast*)pDay

/*============================================================================*/
{
    if (pDay == m_pDay)
        return;

    m_pDay = pDay;
    [m_pDayOfWeek removeFromSuperview];
    [m_pHi removeFromSuperview];
    [m_pLo removeFromSuperview];
    [m_pDesc removeFromSuperview];
    [m_pIconView removeFromSuperview];

    m_pDayOfWeek = [self AddText:[pDay DayOfWeekText] Size:DAY_OF_WEEK_SIZE_VT Color:[UIColor whiteColor]];
    m_pHi = [self AddText:[pDay HighText] Size:TEMP_SIZE_VT Color:[UIColor redColor]];
    m_pLo = [self AddText:[pDay LowText] Size:TEMP_SIZE_VT Color:[UIColor blueColor]];
    m_pDesc = [self AddText:[pDay Description] Size:DESCRIPTION_SIZE_VT Color:[UIColor whiteColor]];
    
    m_pIconView = [[CDayIconView alloc] initWithFrame:CGRectZero DayIndex:[pDay Index] Day:pDay];
    [self addSubview:m_pIconView];
    [m_pIconView release];

    m_pDayOfWeek.textAlignment = UITextAlignmentLeft;
    m_pDesc.textAlignment = UITextAlignmentLeft;
}
/*============================================================================*/

-(void)Orientation:(int)iOrientation

/*============================================================================*/
{
    if (m_iOrientation == iOrientation)
        return;

    m_iOrientation = iOrientation;

    int iSizeDayOfWeek = DAY_OF_WEEK_SIZE_HZ;
    int iSizeTemp      = TEMP_SIZE_HZ;
    int iSizeDesc      = DESCRIPTION_SIZE_HZ;

    if (iOrientation == ORIENTATION_VT)
    {
        iSizeDayOfWeek = DAY_OF_WEEK_SIZE_VT;
        iSizeTemp      = TEMP_SIZE_VT;
        iSizeDesc      = DESCRIPTION_SIZE_VT;
    }

    [CCustomPage InitStandardLabel:m_pDayOfWeek Size:iSizeDayOfWeek];
    [CCustomPage InitStandardLabel:m_pHi        Size:iSizeTemp];
    [CCustomPage InitStandardLabel:m_pLo        Size:iSizeTemp];
    [CCustomPage InitStandardLabel:m_pDesc      Size:iSizeDesc];
}
/*============================================================================*/

-(UILabel*)AddText:(NSString*)psText Size:(int)iSize Color:(UIColor*)pColor

/*============================================================================*/
{
    UILabel *pLabel  = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
    [CCustomPage InitStandardLabel:pLabel Size:iSize];
    pLabel.textColor = pColor;
    [pLabel setText:psText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];
    CGRect rBounds = self.bounds;

    if (m_iOrientation == ORIENTATION_VT)
    {
        CGRect rTemps = [CRect BreakOffRight:&rBounds DX:100];
        [CRect CenterDownToY:&rTemps DY:TEMP_SIZE_VT * 250 / 100];
        CGRect rHi = [CRect BreakOffTop:&rTemps DY:rTemps.size.height/2];
        CGRect rLo = rTemps;

        rBounds = self.bounds;
        [CRect BreakOffLeft:&rBounds DX:16];
        CGRect rImage  = [CRect BreakOffLeft:&rBounds DX:IMAGE_SIZE*150/100];

        [CRect CenterDownToY:&rBounds DY:(DESCRIPTION_SIZE_VT+DAY_OF_WEEK_SIZE_VT) * 125 / 100];
        CGRect rDesc = [CRect BreakOffBottom:&rBounds DY:DESCRIPTION_SIZE_VT*125/100];

        int iDim = MIN(rImage.size.width, rImage.size.height);
        [CRect CenterDownToX:&rImage DX:iDim];
        [CRect CenterDownToY:&rImage DY:iDim];
        [m_pIconView setFrame:rImage];
        [m_pHi setFrame:rHi];
        [m_pLo setFrame:rLo];
        [m_pDesc setFrame:rDesc];
        [m_pDayOfWeek setFrame:rBounds];
    }
    else
    {
        CGRect rTemps = [CRect BreakOffBottom:&rBounds DY:TEMP_SIZE_HZ];
        [CRect CenterDownToX:&rTemps DX:150];
        CGRect rHi = [CRect BreakOffRight:&rTemps DX:rTemps.size.width/2];
        CGRect rLo = rTemps;

        CGRect rDayOfWeek = [CRect BreakOffTop:&rBounds DY:DAY_OF_WEEK_SIZE_HZ];
        CGRect rDesc      = [CRect BreakOffTop:&rBounds DY:DESCRIPTION_SIZE_HZ];
        
        //DL this makes day of week and weather description not clipped
        rDayOfWeek.origin.y -= 4;
        rDayOfWeek.size.height += 4;
        rDesc.origin.y -= 4;
        rDesc.size.height += 4;
        rBounds.origin.y -= 1;
        //////////////////////

        int iDim = MIN(rBounds.size.width, rBounds.size.height);
        [CRect CenterDownToX:&rBounds DX:iDim];
        [CRect CenterDownToY:&rBounds DY:iDim];
        [m_pIconView setFrame:rBounds];
        [m_pHi setFrame:rHi];
        [m_pLo setFrame:rLo];
        [m_pDesc setFrame:rDesc];
        [m_pDayOfWeek setFrame:rDayOfWeek];
    }
}
@end

@implementation CDayIconView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rect DayIndex:(int)iIndex Day:(CDayForecast*)pDay

/*============================================================================*/
{
    if (self == [super initWithFrame:rect])
    {
        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_WEATHER_GETIMAGEBYDAYINDEXQ] autorelease];
        [pQ PutInt:iIndex];
        [pQ PutInt:IMAGE_SIZE];
        [pQ PutInt:IMAGE_SIZE];
        [pQ SendMessageWithMyID:self];
        [self setBackgroundColor:[CHLComm GetRGB:RGB_LIST_AREA]];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_pImage != NULL)
        CGImageRelease(m_pImage);
    if (m_pData != NULL)
        free(m_pData);

    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    printf("DAYICONVIEW DRARECT1\n");

    CGContextRef context = UIGraphicsGetCurrentContext();
    if (m_pImage == NULL)
        return;

    int iDYThis = rect.size.height;
    int iDX = CGImageGetWidth(m_pImage);
    int iDY = CGImageGetHeight(m_pImage);

    float fDXDY = (float)iDX / (float)iDY;
    [CRect ConfineToAspect:&rect DXDY:fDXDY];

    CGContextScaleCTM(context, 1, -1);
    rect.origin.y -= iDYThis;
    CGContextDrawImage(context, rect, m_pImage);
    CGContextScaleCTM(context, 1, -1);

    printf("DAYICONVIEW DRARECT2\n");
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID]) 
    {
    case HLM_WEATHER_GETIMAGEBYDAYINDEXA:
        {
            int iEncodeType     = [pMSG GetInt];
            [pMSG GetString];   // Aux Data eg 20%,30% etc.
            int iSize           = [pMSG GetInt];
            void *pData         = [pMSG GetDataAtReadIndex];
            m_pData = malloc(iSize);
            memcpy(m_pData, pData, iSize);

            CGDataProviderRef pSrc = CGDataProviderCreateWithData(NULL, m_pData, iSize, NULL);
            switch (iEncodeType)
            {
            case ENCODE_JPEG:
                m_pImage = CGImageCreateWithJPEGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
                break;

            case ENCODE_GIF:
                {
                    NSData *pNSData = [NSData dataWithBytes:pData length:iSize];
                    UIImage *pImage = [UIImage imageWithData:pNSData];
                    m_pImage = [pImage CGImage];
                    CGImageRetain(m_pImage);
                }
                break;
            
            case ENCODE_PNG:
                m_pImage = CGImageCreateWithPNGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
                break;
            }

            CGDataProviderRelease(pSrc);
            [self setNeedsDisplay];
        }
        break;

    default:
        break;
    }
}
@end