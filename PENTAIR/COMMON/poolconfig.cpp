/*
--------------------------------------------------------------------------------

    POOLCONFIG.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlquery.h"
#include "hlmsg.h"
#include "hlserver.h"
#include "hlm.h"
#include "helpers.h"
#include "hlstring.h"
#include "stringlib.h"

#ifdef HLCONFIG
    #include "main.h"
#endif

#include "poolconfig.h"

#define NAME_CHUNK_SIZE 25
#define CTLR_I5             0
#define CTLR_I7_3           1
#define CTLR_I9_3           2
#define CTLR_I5S            3
#define CTLR_I9_3S          4
#define CTLR_I10_3D         5
#define CTLR_I10X           6
#define CTLR_SUNTOUCH       0x0A
#define CTLR_EASYTOUCH      0x0E
#define CTLR_EASYTOUCH2     0x0D

#ifdef MULTI_SERVER
    #define QUERY(x) CHLQuery x(g_pHLServer)
#else
    #define QUERY(x) CHLQuery x
#endif

#ifdef CRYSTALPAD
/*========================================================================*/

    CPoolCircuit::CPoolCircuit(
    CPoolConfig                 *pConfig,
    CHLMSG                      *pMSG )

/*========================================================================*/
{
    m_pConfig = pConfig;

    pMSG->GetInt(&m_iCircuitID);

    // Crystalpad mode, friendly but large
    pMSG->GetString(&m_sName);
    pMSG->GetBYTE(&m_iNameIndex);
    pMSG->GetBYTE(&m_iFunction);
    pMSG->GetBYTE(&m_iInterface);
    pMSG->GetBYTE(&m_iFlags);
    pMSG->GetBYTE(&m_iColorSet);
    pMSG->GetBYTE(&m_iColorPos);
    pMSG->GetBYTE(&m_iColorStagger);
    pMSG->GetBYTE(&m_iDeviceID);
    pMSG->GetWORD(&m_iDefaultRT);

    BYTE byPad;
    pMSG->GetBYTE(&byPad);   // Padding
    pMSG->GetBYTE(&byPad);   // Padding

    m_iState        = -1;
    m_pButton       = NULL;
}
#endif

#ifndef CRYSTALPAD
/*========================================================================*/

    CPoolCircuit::CPoolCircuit(
    CPoolConfig                 *pConfig,
    int                         iCtlrIndex,
    CHLMSG                      *pMSG )

/*========================================================================*/
{
    m_pConfig = pConfig;

    m_iCtlrIndex = iCtlrIndex;
    pMSG->GetInt(&m_iCircuitID);

    BYTE byIntFlags = 0;
    BYTE byColorData = 0;

    // Total packet size/circuit should be 8 bytes
    pMSG->GetBYTE(&m_iNameIndex);  
    pMSG->GetBYTE(&m_iFunction);   
    pMSG->GetBYTE(&byIntFlags);    
    pMSG->GetBYTE(&byColorData);   
    pMSG->GetBYTE(&m_iColorStagger);
    pMSG->GetBYTE(&m_iDeviceID);  
    pMSG->GetWORD(&m_iDefaultRT);

    m_iColorSet = ((byColorData & 0xF0) >> 4);
    m_iColorPos = (byColorData & 0x0F);

    m_iFlags = (byIntFlags >> 6);
    m_iInterface = (byIntFlags & 0x3F);

    m_iState        = -1;
    m_pButton       = NULL;
}
#endif

/*========================================================================*/

    void                        CPoolCircuit::DefaultRunTime(
    int                         iCtlrIndex,
    WORD                        wNew )

/*========================================================================*/
{
    if (wNew == m_iDefaultRT)
        return;
    m_iDefaultRT = wNew;

    if (iCtlrIndex == -1)
        return;

    // Send off to gateway too
    CHLMSG msgQ(0, HLM_POOL_SETCIRCUITRUNTIMEBYIDQ);
    msgQ.PutInt(iCtlrIndex);
    msgQ.PutInt(m_iCircuitID);
    msgQ.PutInt(wNew);
    #ifdef MULTI_SERVER
        m_pConfig->Server()->SendMSG(&msgQ);
    #else
        g_pHLServer->SendMSG(&msgQ);
    #endif
}
/*========================================================================*/

    BOOL                        CPoolCircuit::HeatCmdValid()

/*========================================================================*/
{
    if (m_iFunction == POOLCIRCUIT_POOL)
        return TRUE;
    if (m_iFunction == POOLCIRCUIT_SPA)
        return TRUE;
    return FALSE;
}
#ifdef ANYCONFIG
    #ifdef MULTI_SERVER
/*========================================================================*/

        CPoolConfig::CPoolConfig(
        CHLServer                   *pServer,
        int                         iControllerIndex,
        WORD                        wMajorVersion,
        WORD                        wMinorVersion )

/*========================================================================*/
    #else
/*========================================================================*/

        CPoolConfig::CPoolConfig(
        int                         iControllerIndex,
        WORD                        wMajorVersion,
        WORD                        wMinorVersion )

/*========================================================================*/
    #endif
#else
#ifdef IPHONE
/*========================================================================*/

    CPoolConfig::CPoolConfig(
    int                         iControllerIndex,
    CHLMSG                      *pMSGA,
    WORD                        wMajorVersion,
    WORD                        wMinorVersion )

/*========================================================================*/
#else
/*========================================================================*/

    CPoolConfig::CPoolConfig(
    int                         iControllerIndex,
    CHLMSG                      *pMSGA  )

/*========================================================================*/
#endif
#endif
{
    m_wMajorVersion = 0xFFFF;
    m_wMinorVersion = 0xFFFF;

    #ifdef ANYCONFIG
        #ifdef MULTI_SERVER
            g_pHLServer = pServer;
        #endif

        m_wMajorVersion     = wMajorVersion;
        m_wMinorVersion     = wMinorVersion;
        CHLMSG *pMSGA       = NULL;
    #endif

    #ifdef IPHONE
        m_wMajorVersion     = wMajorVersion;
        m_wMinorVersion     = wMinorVersion;
    #endif

    m_bDataOK           = FALSE;
    m_iControllerIndex  = iControllerIndex;
    m_iControllerID     = -1;
    m_dwInterfaceTabFlags = POOL_TAB_ALL;
    m_bDegC             = FALSE;
    m_iAirTemp          = 0;
    m_iPH               = 0;
    m_iORP              = 0;
    m_iSaturation       = 0xFF;
    m_iSaltPPM          = 0;
    m_iPHTank           = 0;
    m_iORPTank          = 0;
    m_iAlarms           = 0;
    m_bShowAlarms       = FALSE;

    memset(m_byIChemData, 0, 42);

    ZeroMemory(m_iMinSetPoint,  sizeof(int) * 2);
    ZeroMemory(m_iMaxSetPoint,  sizeof(int) * 2);
    ZeroMemory(m_iCurrentTemp,  sizeof(int) * 2);
    ZeroMemory(m_iHeatStatus,   sizeof(int) * 2);
    ZeroMemory(m_iSetPoint,     sizeof(int) * 2);
    ZeroMemory(m_iCoolSetPoint, sizeof(int) * 2);
    ZeroMemory(m_iHeatMode,     sizeof(int) * 2);
    ZeroMemory(m_byPumpCir,     sizeof(BYTE) * 8);

    m_sGenCircuitName   = CHLXString("General", "Pool");

    m_byDeviceType      = 0;
    m_bControllerType   = 0;
    m_bHWType           = 0;
    m_bControllerData   = 0;

    #ifndef CRYSTALPAD
        m_iNVersionInfo     = 0;
        m_iN2SpeedConfig    = 0;
        m_iNValveConfig     = 0;
        m_iNRemoteConfig    = 0;
        m_iNSensorConfig    = 0;
        m_iNDelayConfig     = 0;
        m_iNMacroConfig     = 0;
        m_iNMiscData        = 0;
        m_iNLightData       = 0;
        m_iNIFlowData       = 0;
        m_iNSCGData         = 0;
        m_iNSpaIFlowData    = 0;

        m_pVersionInfo      = NULL;
        m_p2SpeedConfig     = NULL;
        m_pValveConfig      = NULL;
        m_pRemoteConfig     = NULL;
        m_pSensorConfig     = NULL;
        m_pDelayConfig      = NULL;
        m_pMacroConfig      = NULL;
        m_pMiscData         = NULL;
        m_pLightData        = NULL;
        m_pIFlowData        = NULL;
        m_pSCGData          = NULL;
        m_pSpaIFlowData     = NULL;
    #endif 

    QUERY(q);
    if (pMSGA == NULL)
    {
        CHLMSG msgQ(0, HLM_POOL_GETCTLRCONFIGQ);
        msgQ.PutInt(m_iControllerIndex);

        #ifdef CRYSTALPAD
            msgQ.PutInt(0); // Std
        #else 
            msgQ.PutInt(1); // Config
        #endif

        #ifdef CRYSTALPAD
            q.SetTimeout(30000);
        #endif

        if (!q.AskQuestion(&pMSGA, &msgQ))
            return;
    }

    // Our ID
    pMSGA->GetInt(&m_iControllerID);

    // General controller settings
    pMSGA->GetBYTE(&(m_iMinSetPoint[BODYTYPE_POOL]));
    pMSGA->GetBYTE(&(m_iMaxSetPoint[BODYTYPE_POOL]));
    pMSGA->GetBYTE(&(m_iMinSetPoint[BODYTYPE_SPA]));
    pMSGA->GetBYTE(&(m_iMaxSetPoint[BODYTYPE_SPA]));
    pMSGA->GetBYTE(&m_bDegC);
    pMSGA->GetBYTE(&m_bControllerType);
    m_byDeviceType = m_bControllerType;
    pMSGA->GetBYTE(&m_bHWType);
    pMSGA->GetBYTE(&m_bControllerData);

    #ifdef ANYCONFIG
        if (IsMinimumVersion(4, 0))
        {
            pMSGA->GetInt((int*)&m_byEquipFlags);
        }
        else
        {
            // Old
            m_byEquipFlags = 0;
            if (m_byDeviceType)
                m_byEquipFlags |= POOL_SOLARPRESENT;
            m_byDeviceType = 0;
        }
            
    #else
        pMSGA->GetInt((int*)&m_byEquipFlags);
    #endif

    pMSGA->GetString(&m_sGenCircuitName);

    int iNCircuits;
    pMSGA->GetInt(&iNCircuits);
    int i = 0;
    for (i = 0; i < iNCircuits; i++)
    {
        #ifdef CRYSTALPAD
            CPoolCircuit *pNew = new CPoolCircuit(this, pMSGA);
        #else
            CPoolCircuit *pNew = new CPoolCircuit(this, i, pMSGA);
        #endif

        m_AllCircuits.AddTail(pNew);
    }

    #ifndef CRYSTALPAD
        pMSGA->GetString(&m_sControllerString);
    #endif

    int iNColors;
    pMSGA->GetInt(&iNColors);
    for (i = 0; i < iNColors; i++)
    {
        CHLString sName;
        COLORREF rgb;
        pMSGA->GetString(&sName);
        pMSGA->GetColor(&rgb);
        CLightColor *pColor = new CLightColor(sName, rgb);
        m_Colors.AddTail(pColor);
    }

    #ifdef ANYCONFIG
        if (IsMinimumVersion(4, 0))
        {    
            for (i = 0; i < 8; i++)
            {
                pMSGA->GetBYTE(m_byPumpCir + i);
            }
        }
    #else
        for (i = 0; i < 8; i++)
        {
            pMSGA->GetBYTE(m_byPumpCir + i);
        }
    #endif

    pMSGA->GetInt((int*)&m_dwInterfaceTabFlags);
    pMSGA->GetInt(&m_bShowAlarms);

    #ifndef CRYSTALPAD
        // Extra stuff crystalpad doesn't need to know about
        LoadConfigData();
        UpdateCircuitNames();
    #endif

    m_byDeviceStatus = 1;
    m_bDataOK = TRUE;
}
/*========================================================================*/

    CPoolConfig::~CPoolConfig()

/*========================================================================*/
{
    while (!m_AllCircuits.IsEmpty())
    {
        CPoolCircuit *pC = (CPoolCircuit*)m_AllCircuits.RemoveHead();
        delete pC;
    }

    while (!m_Colors.IsEmpty())
    {
        CLightColor *pColor = (CLightColor*) m_Colors.RemoveHead();
        delete pColor;
    }

    #ifndef CRYSTALPAD
        while (!m_ControllerNames.IsEmpty())
        {
            CHLString *pName = (CHLString*) m_ControllerNames.RemoveHead();
            delete pName;
        }

        while (!m_CustomNames.IsEmpty())
        {
            CHLString *pName = (CHLString*) m_CustomNames.RemoveHead();
            delete pName;
        }

        while (!m_CircuitTypes.IsEmpty())
        {
            CCircuitType *pType = (CCircuitType*) m_CircuitTypes.RemoveHead();
            delete pType;
        }

        DeleteConfigData();
    #endif
}
/*========================================================================*/

    CHLString                   CPoolConfig::HeatModeText(
    int                         iMode )

/*========================================================================*/
{
    BOOL bHPump = EquipPresent(POOL_SOLARHEATPUMP);
    switch (iMode)
    {
    case POOLHEAT_OFF:          
        return CHLXString("Off", "Pool");
    case POOLHEAT_HEAT:         
        return CHLXString("Heater", "Pool");
    case POOLHEAT_SOLAR:        
        {
            if (bHPump)
                return CHLXString("H.Pump Only", "Pool");
            else
                return CHLXString("Solar Only", "Pool");
        }
        break;
    
    case POOLHEAT_SOLARPREF:        
        {
            if (bHPump)
                return CHLXString("H.Pump Pref", "Pool");
            else
                return CHLXString("Solar Pref", "Pool");
        }
        break;
    }

    return CHLXString("Don't Change", "");
}
/*========================================================================*/

    CHLString                   CPoolConfig::HeatStatusText(
    int                         iStatus )

/*========================================================================*/
{
    BOOL bHPump = EquipPresent(POOL_SOLARHEATPUMP);
    #ifdef HL_PLATFORM_SMALL
        if (bHPump)
        {
            switch (iStatus)
            {
            case HEAT_HEATER_ACTIVE:    return CHLXString("Heater On", "Pool");
            case HEAT_SOLAR_ACTIVE:     return CHLXString("H.Pump On", "Pool");
            case HEAT_BOTH_ACTIVE:      return _T("Htr+HP On");
            }
        }
        else
        {
            switch (iStatus)
            {
            case HEAT_HEATER_ACTIVE:    return CHLXString("Heater On", "Pool");
            case HEAT_SOLAR_ACTIVE:     return CHLXString("Solar On", "Pool");
            case HEAT_BOTH_ACTIVE:      return _T("Htr+Sol On");
            }
        }
    #else
        if (bHPump)
        {
            switch (iStatus)
            {
            case HEAT_HEATER_ACTIVE:    return CHLXString("Heater On", "Pool");
            case HEAT_SOLAR_ACTIVE:     return CHLXString("H.Pump On", "Pool");
            case HEAT_BOTH_ACTIVE:      return CHLXString("Heater/H.P. On", "Pool");            
            }
        }
        else
        {
            switch (iStatus)
            {
            case HEAT_HEATER_ACTIVE:    return CHLXString("Heater On", "Pool");
            case HEAT_SOLAR_ACTIVE:     return CHLXString("Solar On", "Pool");
            case HEAT_BOTH_ACTIVE:      return CHLXString("Heater/Solar On", "Pool");
            }
        }
    #endif
   
    return CHLXString("Off", "Pool");
}
/*========================================================================*/

    CHLString                   CPoolConfig::EnableText(
    BOOL                        bEnable )

/*========================================================================*/
{
    if (bEnable)
        return CHLXString("Enabled", "Pool");
    return CHLXString("Disabled", "Pool");
}
/*========================================================================*/

    void                        CPoolConfig::ProcessMSG(
    CHLMSG                      *pMSG )

/*========================================================================*/
{
    if ((pMSG->MessageID() != HLM_POOL_STATUSCHANGED) && (pMSG->MessageID() != HLM_POOL_GETSTATUSA))
    {
        // Not one we want
        return;
    }

    int iOK;
    if (!pMSG->GetInt(&iOK))
    {
        m_byDeviceStatus = 0;
        return;
    }

    #ifndef CRYSTALPAD
        if (IsMinimumVersion(3, 378))
            pMSG->GetChar((char*)&m_bFreezeMode);
    #else
        pMSG->GetChar((char*)&m_bFreezeMode);
    #endif

    pMSG->GetChar((char*)&m_bRemotes);
    pMSG->GetChar((char*)&m_bPoolDelay);
    pMSG->GetChar((char*)&m_bSpaDelay);
    pMSG->GetChar((char*)&m_bCleanerDelay);

    #ifndef CRYSTALPAD
        if (IsMinimumVersion(3, 378))
        {
            char cPadding;
            pMSG->GetChar((char*)&cPadding);
            pMSG->GetChar((char*)&cPadding);
            pMSG->GetChar((char*)&cPadding);
        }
    #else
        char cPadding;
        pMSG->GetChar((char*)&cPadding);
        pMSG->GetChar((char*)&cPadding);
        pMSG->GetChar((char*)&cPadding);
    #endif

    m_byDeviceStatus = iOK;

    // Air Temp
    if (!pMSG->GetInt(&m_iAirTemp))
        return;

    // Num Bodies coming back    
    int iNBodies = 0;
    if (!pMSG->GetInt(&iNBodies))
        return;
    if (iNBodies > MAX_BODIES)
    {
        LOGASSERT(FALSE);
        iNBodies = MAX_BODIES;
    }

    for (int i = 0; i < iNBodies; i++)
    {
        int iBodyType = 0;
        if (!pMSG->GetInt(&iBodyType))
            return;
        // patch, currently always 2 bodies, 1st= BODYTYPE_POOL=0, 2nd= BODYTYPE_SPA=1
        // however some versions of v4 brick return type=80 for second body
        iBodyType = i;

        // Basic Body Attribs
        pMSG->GetInt(&(m_iCurrentTemp[iBodyType]));
        pMSG->GetInt(&(m_iHeatStatus[iBodyType]));
        pMSG->GetInt(&(m_iSetPoint[iBodyType]));
        if (IsMinimumVersion(5, 1))
            pMSG->GetInt(&(m_iCoolSetPoint[iBodyType]));
        pMSG->GetInt(&(m_iHeatMode[iBodyType]));
    }

    // Circuits
    int iNCircuits = 0;
    pMSG->GetInt(&iNCircuits);

    for (int iCircuit = 0; iCircuit < iNCircuits; iCircuit++)
    {
        int iID, iState;
        char cColorSet;
        char cColorPos;
        char cColorStagger;
        char cDelay;

        pMSG->GetInt(&iID);
        pMSG->GetInt(&iState);
        pMSG->GetChar(&cColorSet);
        pMSG->GetChar(&cColorPos);
        pMSG->GetChar(&cColorStagger);
        pMSG->GetChar(&cDelay);

        CPoolCircuit *pC = CircuitByID(iID);
        if (pC != NULL)
        {
            pC->State(iState);
            pC->ColorSet(cColorSet);
            pC->ColorPos(cColorPos);
            pC->ColorStagger(cColorStagger);
            pC->Delay(cDelay);
        }
    }

    if (IsMinimumVersion(5, 1))
    {
        pMSG->GetInt(&m_iPH);
        pMSG->GetInt(&m_iORP);
        pMSG->GetInt(&m_iSaturation);
        pMSG->GetInt(&m_iSaltPPM);
        pMSG->GetInt(&m_iPHTank);
        pMSG->GetInt(&m_iORPTank);
        pMSG->GetInt(&m_iAlarms);
    }
}
/*========================================================================*/

    void                        CPoolConfig::ProcessChemMSG(
    CHLMSG                      *pMSG )

/*========================================================================*/
{
    switch (pMSG->MessageID())
    {
    case HLM_POOL_CHEMDATACHANGED:
    case HLM_POOL_GETCHEMDATAA:
        break;
    default:
        return;

    }

    pMSG->ResetRead();

    int nBytes = 0;
    pMSG->GetInt(&nBytes);
    if (nBytes != 42)
        return;

    for (int i=0; i < 42; i++)
    {
        BYTE byte = 0;
        pMSG->GetBYTE(&byte);
        m_byIChemData[i] = byte;
    }
}
#ifndef CRYSTALPAD
/*========================================================================*/

    CCircuitType                *CPoolConfig::CircuitType(
    int                         iIndex)

/*========================================================================*/
{
    if (iIndex < 0 || iIndex >= m_CircuitTypes.GetCount())
        return NULL;
    CHLPos *pPos = m_CircuitTypes.FindIndex(iIndex);
    if (pPos == NULL)
        return NULL;
    return (CCircuitType*) pPos->Object();
}
/*========================================================================*/

    CHLString                   CPoolConfig::TypeNameByIndex(
    int                         iIndex)

/*========================================================================*/
{
    CCircuitType *pType = CircuitType(iIndex);
    if (pType == NULL)
        return CHLXString("ERROR", "");
    return pType->Name();
}
/*========================================================================*/

    CHLString                   CPoolConfig::TypeNameByType(
    int                         iType )

/*========================================================================*/
{
    CHLPos *pPos = m_CircuitTypes.GetHeadPosition();
    while (pPos != NULL)
    {
        CCircuitType *pType = (CCircuitType*)pPos->Object();
        pPos = pPos->Next();
        if (pType->Type() == iType)
            return pType->Name();
    }

    return _T("");
}
/*========================================================================*/

    int                         CPoolConfig::TypeByIndex(
    int                         iIndex)

/*========================================================================*/
{
    CCircuitType *pType = CircuitType(iIndex);
    if (pType == NULL)
        return -1;
    return pType->Type();
}
#endif
/*========================================================================*/

    BOOL                        CPoolConfig::DeviceReady()

/*========================================================================*/
{
    return (m_byDeviceStatus == 1);
}
/*========================================================================*/

    BOOL                        CPoolConfig::DeviceSync()

/*========================================================================*/
{
    return (m_byDeviceStatus == 2);
}
/*========================================================================*/

    BOOL                        CPoolConfig::DeviceServiceMode()

/*========================================================================*/
{
    return (m_byDeviceStatus == 3);
}
/*========================================================================*/

    void                        CPoolConfig::SetEquipFlag(DWORD dwMask, BOOL bTF )

/*========================================================================*/
{
    if (bTF)
        m_byEquipFlags |= dwMask;
    else
        m_byEquipFlags &= ~dwMask;

}

/*========================================================================*/

    int                         CPoolConfig::GetPH()

/*========================================================================*/
{
    int iPH      = (m_byIChemData[1] << 8) + m_byIChemData[2];
    return iPH;
}
/*========================================================================*/

    int                         CPoolConfig::GetORP()

/*========================================================================*/
{
    int iORP     = (m_byIChemData[3] << 8) + m_byIChemData[4];
    return iORP;
}
/*========================================================================*/

    int                         CPoolConfig::GetSaturation()

/*========================================================================*/
{
    int iSat = 0;
    if (m_byIChemData[23] & 0x80)
        iSat = -(0x100 - m_byIChemData[23]);
    else
        iSat = m_byIChemData[23];

    if (m_byIChemData[34] & 0x80)
    {
        // Comm Error
        return Saturation();
    }


    return iSat;
}
/*========================================================================*/

    int                         CPoolConfig::GetSaltPPM()

/*========================================================================*/
{
    int iSaltPPM     = m_byIChemData[30] * 50;
    return iSaltPPM;
}
/*========================================================================*/

    int                         CPoolConfig::GetPHTankLevel()

/*========================================================================*/
{
    return m_byIChemData[21];
}
/*========================================================================*/

    int                         CPoolConfig::GetORPTankLevel()

/*========================================================================*/
{
    return m_byIChemData[22];
}
/*========================================================================*/

    CHLString                   CPoolConfig::SolarText()

/*========================================================================*/
{
    if (EquipPresent(POOL_SOLARHEATPUMP))
        return CHLXString("H.Pump", "Pool");
    return CHLXString("Solar", "Pool");
}
/*========================================================================*/

    void                        CPoolConfig::LoadCircuitsByInterface(
    CHLObList                   *pList,
    int                         iIFace )

/*========================================================================*/
{
    CHLPos *pPos = m_AllCircuits.GetHeadPosition();
    while (pPos != NULL)
    {
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();
        if (pC->Interface() == iIFace)
            pList->AddTail(pC);
    }
}
/*========================================================================*/

    void                        CPoolConfig::SetCircuitFirstByFunction(
    CHLObList                   *pList, 
    int                         iFunction )

/*========================================================================*/
{
    CHLPos *pPos = pList->GetHeadPosition();
    while (pPos != NULL)
    {
        CHLPos *pPosThis = pPos;
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();
        if (pC->Function() == iFunction)
        {
            pList->RemoveAt(pPosThis);
            pList->AddHead(pC);
            return;
        }
    }
}
/*========================================================================*/

    void                        CPoolConfig::SortByColorPosition(
    CHLObList                   *pList )

/*========================================================================*/
{
    CHLObList Local;
    while (!pList->IsEmpty())
    {
        CPoolCircuit *pC = (CPoolCircuit*) pList->RemoveHead();
        
        BOOL bInsert = FALSE;
        CHLPos *pPosLocal = Local.GetHeadPosition();
        while (pPosLocal != NULL && !bInsert)
        {
            CPoolCircuit *pLocal = (CPoolCircuit*) pPosLocal->Object();
            if (pC->ColorPos() <= pLocal->ColorPos())
            {
                Local.InsertBefore(pPosLocal, pC);
                bInsert = TRUE;
            }

            pPosLocal = pPosLocal->Next();
        }

        if (!bInsert)
            Local.AddTail(pC);
    }

    pList->RemoveAll();
    while (!Local.IsEmpty())
    {
        pList->AddTail(Local.RemoveHead());
    }
}
/*========================================================================*/

    CPoolCircuit                *CPoolConfig::CircuitByID(
    int                         iID )

/*========================================================================*/
{
    CHLPos *pPos = m_AllCircuits.GetHeadPosition();
    while (pPos != NULL)
    {
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();
        if (pC->CircuitID() == iID)
            return pC;
    }

    return NULL;
}
/*========================================================================*/

    CPoolCircuit                *CPoolConfig::CircuitByDeviceID(
    int                         iID )

/*========================================================================*/
{
    CHLPos *pPos = m_AllCircuits.GetHeadPosition();
    while (pPos != NULL)
    {
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();
        if (pC->DeviceID() == iID)
            return pC;
    }

    return NULL;
}
/*========================================================================*/

    CPoolCircuit                *CPoolConfig::CircuitByFunction(
    int                         iFunction )

/*========================================================================*/
{
    CHLPos *pPos = m_AllCircuits.GetHeadPosition();
    while (pPos != NULL)
    {
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();
        if (pC->Function() == iFunction)
            return pC;
    }

    return NULL;
}
/*========================================================================*/

    CPoolCircuit                *CPoolConfig::NextCircuit(
    CPoolCircuit                *pCircuit )

/*========================================================================*/
{
    CHLPos *pPos = m_AllCircuits.Find(pCircuit);
    if (pPos == NULL)
        return NULL;
    CHLPos *pPos0 = pPos;
    do 
    {
        pPos = pPos->Next();
        if (pPos == NULL)
            pPos = m_AllCircuits.GetHeadPosition();
        CPoolCircuit* pC = (CPoolCircuit*) pPos->Object();
        if (CircuitInstalled(pC))
            return pC;
    } while (pPos != pPos0); 

    return NULL;
}
/*========================================================================*/

    CPoolCircuit                *CPoolConfig::PrevCircuit(
    CPoolCircuit                *pCircuit )

/*========================================================================*/
{
    CHLPos *pPos = m_AllCircuits.Find(pCircuit);
    if (pPos == NULL)
        return NULL;
    CHLPos *pPos0 = pPos;
    do 
    {
        pPos = pPos->Prev();
        if (pPos == NULL)
            pPos = m_AllCircuits.GetTailPosition();
        CPoolCircuit* pC = (CPoolCircuit*) pPos->Object();
        if (CircuitInstalled(pC))
            return pC;
    } while (pPos != pPos0); 

    return NULL;
}
/*============================================================================*/

    BOOL                    CPoolConfig::CircuitInstalled(
    CPoolCircuit            *pC )

/*============================================================================*/
{
    if (pC->Interface() == POOLINT_INVALID)
        return FALSE;
//  if (pC->Interface() != POOLINT_DONTSHOW)
//      return TRUE;

    int iID = pC->DeviceID();  // 1-based
    if (iID < 1)
        return FALSE;
    if (iID <= 10)
    {
        // One of the on-board ones
        switch (m_bControllerType)
        {
        case CTLR_EASYTOUCH:
        case CTLR_EASYTOUCH2:
            if (m_bHWType & 0x02)
                return (iID <= 6);   // ET 4
            else
                return (iID <= 9);   // ET 8

        case CTLR_I5:
        case CTLR_I5S:
            return (iID <= 6);

        case CTLR_I7_3:
            return (iID <= 8);

        case CTLR_I9_3:
        case CTLR_I9_3S:
        case CTLR_I10_3D:
        case CTLR_I10X:
            return TRUE;
        }
    }

    if (iID >= 41 && iID <= 50)
    {
        // Intellitouch Feature Circuit
        switch (m_bControllerType)
        {
        case CTLR_I5:
        case CTLR_I5S:
        case CTLR_I7_3:
        case CTLR_I9_3:
        case CTLR_I9_3S:
        case CTLR_I10_3D:
        case CTLR_I10X:
            return TRUE;

        case CTLR_EASYTOUCH:
        case CTLR_EASYTOUCH2:
            return FALSE;
        }
    }
    if (iID >= 11 && iID <= 18)
    {
        // EasyTouch Feature Circuit, only some have these
        switch (m_bControllerType)
        {
        case CTLR_EASYTOUCH2:
#ifndef CRYSTALPAD
            if (Firmware() >= 2040)
                return TRUE;
            else
                return FALSE;
#else
            // !***! CrystalPad does not currently receive the Firmware version data.
            //       This CircuitInstalled() function is not currently used by Crystalpad,
            //       needs to be fixed up if it is used.
            ASSERT(0);
            return TRUE;
#endif
        case CTLR_EASYTOUCH:
            return FALSE;
        }
    }
    if (IsEasyTouch())
    {
        if ((iID == 20) && (EquipPresent(POOL_SOLARPRESENT) == FALSE))
            return TRUE;    // AuxExtra
        else
            return FALSE;   // EasyTouch has no other circuits
    }

    // IntelliTouch expansion units
    BYTE bySlaves = ((m_bControllerData & 0xC0) >> 6);   // 0..3 slaves

    int iMax = (bySlaves * 10) + 10;
    int iIndex = iID - 1;  // zero based
    if (iIndex >= iMax)
        return FALSE;

    int iSlave = (iIndex - 10) / 10;
    int iCircuitInSlave = iIndex % 10;

    if (iCircuitInSlave >= 5)
    {
        if (!Slave10X(iSlave))
            return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                    CPoolConfig::Slave10X(
    int                     iSlaveIndex )        

/*============================================================================*/
{
    int iCount = 0;
    int iStart = 11 + iSlaveIndex * 10;
    for (int iC = 0; iC < 10; iC++)
    {
        CPoolCircuit *pC = CircuitByDeviceID(iStart + iC);
        if (pC != NULL)
        {
            if ((pC->NameIndex() != 0 ) && (pC->NameIndex() != 255 ))
                iCount++;
        }
    }

    return (iCount == 10);
}
/*========================================================================*/

    void                        CPoolConfig::SetCircuitOnOff(
    CPoolCircuit                *pC,
    BOOL                        bOn )

/*========================================================================*/
{
    if (pC == NULL)
        return;

    CHLMSG msgQ(0, HLM_POOL_BUTTONPRESSQ);
    msgQ.PutInt(m_iControllerIndex);
    msgQ.PutInt(pC->CircuitID());

    if (bOn)
        msgQ.PutInt(1);
    else
        msgQ.PutInt(0);

    pC->State(bOn);
    g_pHLServer->SendMSG(&msgQ);
}
/*========================================================================*/

    void                        CPoolConfig::SetCircuitColor(
    CPoolCircuit                *pC,
    int                         iIndex )

/*========================================================================*/
{
    if (pC == NULL)
        return;

    pC->ColorSet(iIndex);
    SendCircuitColorInfo(pC);
}
/*========================================================================*/

    int                         CPoolConfig::NColors()

/*========================================================================*/
{
    return m_Colors.GetCount();
}
/*========================================================================*/

    CLightColor                 *CPoolConfig::Color(
    int                         iIndex)

/*========================================================================*/
{
    if (iIndex < 0 || iIndex >= m_Colors.GetCount())
        return NULL;
    CHLPos *pPos = m_Colors.FindIndex(iIndex);
    return (CLightColor*) pPos->Object();
}
/*========================================================================*/

    void                        CPoolConfig::SendCircuitColorInfo(
    CPoolCircuit                *pC )

/*========================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_CONFIGLIGHTQ);
    msgQ.PutInt(m_iControllerIndex);
    msgQ.PutInt(pC->CircuitID());
    msgQ.PutInt(pC->ColorSet());
    msgQ.PutInt(pC->ColorPos());
    msgQ.PutInt(pC->ColorStagger());
    g_pHLServer->SendMSG(&msgQ);
}
/*========================================================================*/

    CHLString                   CPoolConfig::FormatDegText(
    int                         iTemp )

/*========================================================================*/
{
    if (iTemp == 0xFF)
        return _T("--");

    CHLString sRet;

    if (m_bDegC)
        sRet.Format(_T("%d C"), iTemp);
    else
        sRet.Format(_T("%d F"), iTemp);
    return sRet;
}
/*========================================================================*/

    BOOL                        CPoolConfig::FreezeMode()

/*========================================================================*/
{
    #ifndef CRYSTALPAD
        if (IsMinimumVersion(3, 378))
            return m_bFreezeMode;

        if (m_iAirTemp <= 36)
            return TRUE;

        return FALSE;
    #else
        return m_bFreezeMode;
    #endif
}
/*========================================================================*/

    BOOL                        CPoolConfig::Alarms()

/*========================================================================*/
{
    return (m_iAlarms != 0);
}
/*========================================================================*/

    void                        CPoolConfig::ShowAlarms(
    BOOL                        bShow )

/*========================================================================*/
{
    m_bShowAlarms = bShow;
}
/*========================================================================*/

    BOOL                        CPoolConfig::CancelDelays()

/*========================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_CANCELDELAYSQ);
    msgQ.PutInt(m_iControllerIndex);
    QUERY(q);
    return q.AskQuestion(NULL, &msgQ);
}
/*========================================================================*/

    BOOL                        CPoolConfig::EnableRemotes(
    BOOL                        bEnable)

/*========================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_ENABLEREMOTESQ);
    msgQ.PutInt(m_iControllerIndex);
    msgQ.PutInt(bEnable);    
    QUERY(q);
    return q.AskQuestion(NULL, &msgQ);
}
/*============================================================================*/

    CHLString                   CPoolConfig::PoolText()

/*============================================================================*/
{
    CPoolCircuit *pC = CircuitByFunction(POOLCIRCUIT_POOL);
    if (pC != NULL)
    {
        if (pC->Name().CompareNoCase(_T("Low Temp")) == 0)
            return CHLXString("Low", "Pool Temperature");
        return pC->Name();
    }
    return CHLXString("Pool", "Pool");
}
/*============================================================================*/

    CHLString                   CPoolConfig::SpaText()

/*============================================================================*/
{
    CPoolCircuit *pC = CircuitByFunction(POOLCIRCUIT_SPA);
    if (pC != NULL)
    {
        if (pC->Name().CompareNoCase(_T("High Temp")) == 0)
            return CHLXString("High", "Pool Temperature");
        return pC->Name();
    }

    return CHLXString("Spa", "Pool");
}
/*========================================================================*/

    CHLString                   CPoolConfig::PHORPText()

/*========================================================================*/
{
    CHLString sText;
    if (EquipPresent(POOL_ICHEMPRESENT))
        sText.Format(_T("%.2f / %d"), (float)(.01 * GetPH()), GetORP());
    else
        sText = _T("--");

    return sText;
}
/*============================================================================*/

    CHLString                   CPoolConfig::FreezeModeStatusText()

/*============================================================================*/
{
    if (m_bFreezeMode)
        return CHLXString("Yes", "");
    return CHLXString("No", "");
}
/*============================================================================*/

    CHLString                   CPoolConfig::DeviceIDToString(
    BYTE                        byID )

/*============================================================================*/
{
    switch (byID)
    {
    case POOL_2SPDTRIG_SOLAR:       return CHLXString("Solar Active", "Pool");
    case POOL_2SPDTRIG_HEATER:      return CHLXString("Pool or Spa Heater Active", "Pool");
    case POOL_2SPDTRIG_POOLHEATER:  return CHLXString("Pool Heater Active", "Pool");
    case POOL_2SPDTRIG_SPAHEATER:   return CHLXString("Spa Heater Active", "Pool");
    case POOL_2SPDTRIG_FREEZE:      return CHLXString("Freeze Mode Active", "Pool");
    case POOL_REMOTE_HEATBOOST:     return CHLXString("Heat Boost", "Pool");
    case POOL_REMOTE_HEATENABLE:    return CHLXString("Heat Enable", "Pool");
    case POOL_REMOTE_INC_PUMPSPD:   return CHLXString("Increment Pump Speed", "Pool");
    case POOL_REMOTE_DEC_PUMPSPD:   return CHLXString("Decrement Pump Speed", "Pool");
    case POOL_POOL_HEATER:          return CHLXString("Pool Heater", "Pool");
    case POOL_SPA_HEATER:           return CHLXString("Spa Heater", "Pool");
    case POOL_EITHER_HEATER:        return CHLXString("Either Heater", "Pool");
    case POOL_SOLAR:                return CHLXString("Solar", "Pool");
    case POOL_FREEZE:               return CHLXString("Freeze", "Pool");
    }

    CPoolCircuit *pC = CircuitByDeviceID(byID);
    if (pC != NULL)
        return pC->Name();

    return CHLXString("None", "");
}

#ifndef CRYSTALPAD
/*============================================================================*/

    BOOL                        CPoolConfig::SetCircuitConfig(
    CPoolCircuit                *pC )

/*============================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_SETCIRCUITINFOBYIDQ);
    msgQ.PutInt(m_iControllerID);    
    msgQ.PutInt(pC->CircuitID());
    msgQ.PutInt(pC->NameIndex());
    msgQ.PutInt(pC->Function());
    msgQ.PutInt(pC->Interface());
    msgQ.PutInt(pC->Flags());
    msgQ.PutInt(pC->ColorPos());
    QUERY(q);
    if (!q.AskQuestion(NULL, &msgQ))
        return FALSE;
    
    // Need to fix up name here locally
    if (pC->NameIndex() < m_ControllerNames.GetCount())
    {
        CHLPos *pPos = m_ControllerNames.FindIndex(pC->NameIndex());
        CHLString *pName = (CHLString*) pPos->Object();
        pC->Name(*pName);
    }
    else 
    {
        int iIndex = pC->NameIndex() - m_ControllerNames.GetCount();
        if (iIndex < m_CustomNames.GetCount())
        {
            CHLPos *pPos = m_CustomNames.FindIndex(iIndex);
            CHLString *pName = (CHLString*) pPos->Object();
            pC->Name(*pName);
        }
        else
        {
            pC->Name(CHLXString("None", ""));
        }
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CPoolConfig::SetCustomName(
    int                         iIndex,
    CHLString                   sNewName )

/*============================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_SETCUSTOMNAMEQ);
    msgQ.PutInt(m_iControllerIndex);
    msgQ.PutInt(iIndex);
    msgQ.PutString(sNewName);
    QUERY(q);
    if (!q.AskQuestion(NULL, &msgQ))
        return FALSE;

    CHLPos *pPos = m_CustomNames.FindIndex(iIndex);
    if (pPos != NULL)
    {
        CHLString *pName = (CHLString*) pPos->Object();
        *pName = sNewName;
    }

    UpdateCircuitNames();

    return TRUE;
}
/*============================================================================*/

    BOOL                    CPoolConfig::Set2SpeedConfig(
    int                     iIndex,
    int                     iVal )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iN2SpeedConfig)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_p2SpeedConfig[iIndex] = iVal;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                    CPoolConfig::SetValveConfig(
    int                     iIndex,
    int                     iVal )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNValveConfig)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pValveConfig[iIndex] = iVal;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                    CPoolConfig::SetRemoteConfig(
    int                     iIndex,
    int                     iVal,
    BOOL                    bSendToGateway )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNRemoteConfig)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pRemoteConfig[iIndex] = iVal;

    if (!bSendToGateway)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                    CPoolConfig::SetSensorConfig(
    int                     iIndex,
    int                     iVal,
    BOOL                    bSend )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNSensorConfig)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }


    m_pSensorConfig[iIndex] = iVal;

    #ifndef CRYSTALPAD
        BYTE by3 = GetSensorConfig(2);
        m_bDegC = (by3 & 0x01) != 0;
    #endif

    if (!bSend)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                    CPoolConfig::SetDelayConfig(
    int                     iIndex,
    int                     iVal,
    BOOL                    bSend )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNDelayConfig)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pDelayConfig[iIndex] = iVal;

    if (!bSend)
        return TRUE;

    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                        CPoolConfig::SetMacroConfig(
    int                         iIndex,
    int                         iVal,
    BOOL                        bSendToGateway )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNMacroConfig)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pMacroConfig[iIndex] = iVal;

    if (!bSendToGateway)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                        CPoolConfig::SetMiscData(
    int                         iIndex,
    BYTE                        bVal,
    BOOL                        bSend )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNMiscData)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pMiscData[iIndex] = bVal;

    if (!bSend)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                        CPoolConfig::SetLightData(
    int                         iIndex,
    BYTE                        bVal,
    BOOL                        bSend )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNLightData)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pLightData[iIndex] = bVal;

    if (!bSend)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                    CPoolConfig::SetIFlowData(
    int                     iIndex,
    BYTE                    bVal,
    BOOL                    bSend )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNIFlowData)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pIFlowData[iIndex] = bVal;

    if (!bSend)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    void                    CPoolConfig::ZeroIFlowData(
    int                     iIndex )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNIFlowData)
    {
        LOGASSERT(FALSE);
        return;
    }

    BYTE* pData = m_pIFlowData + (iIndex * IFLOWINFO_SIZE);
    ZeroMemory(pData, IFLOWINFO_SIZE*sizeof(BYTE));
}
/*============================================================================*/

    BOOL                    CPoolConfig::SetSpaIFlowCtrlData(
    int                     iIndex,
    BYTE                    bVal,
    BOOL                    bSend )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNSpaIFlowData)
    {
        LOGASSERT(FALSE);
        return FALSE;
    }

    m_pSpaIFlowData[iIndex] = bVal;

    if (!bSend)
        return TRUE;
    return SendEquipConfig();
}
/*============================================================================*/

    BOOL                        CPoolConfig::SetCircuitColorPos(
    CPoolCircuit                *pC,
    int                         iIndex )

/*============================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_CONFIGLIGHTQ);
    msgQ.PutInt(m_iControllerIndex);
    msgQ.PutInt(pC->CircuitID());
    msgQ.PutInt(pC->ColorSet());
    msgQ.PutInt(iIndex);
    msgQ.PutInt(pC->ColorStagger());
    QUERY(q);
    if (!q.AskQuestion(NULL, &msgQ))
        return FALSE;
    pC->ColorPos(iIndex);
    return TRUE;
}
/*============================================================================*/

    BOOL                        CPoolConfig::SetCalibration(
    int                         iTempType,
    int                         iDiff )

/*============================================================================*/
{
    switch (iTempType)
    {
    case POOLINT_AIRTEMP:
        m_iAirTemp += iDiff;
        break;

    case POOLINT_POOLTEMP:
        m_iCurrentTemp[BODYTYPE_POOL] += iDiff;
        break;

    case POOLINT_SPATEMP:
        m_iCurrentTemp[BODYTYPE_SPA] += iDiff;
        break;
    }

    CHLMSG msgQ(0, HLM_POOL_SETCALQ);
    msgQ.PutInt(0);
    msgQ.PutInt(m_iAirTemp);
    msgQ.PutInt(m_iCurrentTemp[BODYTYPE_POOL]);
    msgQ.PutInt(m_iCurrentTemp[BODYTYPE_SPA]);
    QUERY(q);
    return q.AskQuestion(NULL, &msgQ);
}
/*============================================================================*/

    BYTE                    CPoolConfig::GetByte(
    BYTE                    *pArray,
    int                     iArraySize,
    int                     iIndex )

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= iArraySize)
    {
        LOGASSERT(FALSE);
        return 0;
    }
    return pArray[iIndex];
}
/*============================================================================*/

    BOOL                        CPoolConfig::LoadConfigData()

/*============================================================================*/
{
    // Controller names, need to do in chunks because brick has little brain
    CHLMSG msgQNNames(0, HLM_POOL_GETNCIRCUITNAMESQ);
    msgQNNames.PutInt(0); // Controller Index
    CHLMSG *pMSGA = NULL;
    QUERY(q1);
    if (!q1.AskQuestion(&pMSGA, &msgQNNames))
        return FALSE;
    int iNNames = 0;
    if (!pMSGA->GetInt(&iNNames))
        return FALSE;

    BOOL bOK = TRUE;
    int iNDone = 0;
    while (bOK && iNDone < iNNames)
    {
        int iNGet = NAME_CHUNK_SIZE;
        iNGet = __min(iNNames - iNDone, iNGet);

        CHLMSG msgChunk(0, HLM_POOL_GETCIRCUITNAMESQ);
        msgChunk.PutInt(0); // Controller Index;
        msgChunk.PutInt(iNDone);
        msgChunk.PutInt(iNGet);

        QUERY(q);
        if (!q.AskQuestion(&pMSGA, &msgChunk))
            return FALSE;

        int iN;
        pMSGA->GetInt(&iN);
        if (iN == iNGet)
        {
            for (int i = 0; i < iN; i++)
            {
                CHLString *pName = new CHLString;
                pMSGA->GetString(pName);
                m_ControllerNames.AddTail(pName);
            }
        }
        else
        {
            bOK = FALSE;
        }

        iNDone += iN;
    }

    if (!LoadNames(&m_CustomNames, HLM_POOL_GETALLCUSTOMNAMESQ))
        return FALSE;
    
    CHLMSG msgQTypes(0, HLM_POOL_GETCIRCUITDEFSQ);
    msgQTypes.PutInt(0); // Controller Index
    pMSGA = NULL;
    QUERY(qTypes);
    if (qTypes.AskQuestion(&pMSGA, &msgQTypes))
    {
        int iN;
        pMSGA->GetInt(&iN);
        for (int i = 0; i < iN; i++)
        {
            CHLString sName;
            int iType;
            pMSGA->GetInt(&iType);
            pMSGA->GetString(&sName);
            m_CircuitTypes.AddTail(new CCircuitType(iType, sName));
        }
    }

    DeleteConfigData();

    CHLMSG msgQ(0, HLM_POOL_GETEQUIPCONFIGQ);
    msgQ.PutInt(m_iControllerIndex);
    QUERY(q);
    pMSGA = NULL;
    if (!q.AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    int iData;
    pMSGA->GetInt(&iData);  m_bControllerType = (iData & 0xFF);
                            m_bHWType = ((iData >> 8) & 0xFF);
    pMSGA->GetInt(&iData);  m_bControllerData = iData;

    // The Version info does NOT get packed back, we obviously can't SET it
    if (!UnpackConfig(&m_iNVersionInfo,     &m_pVersionInfo,        pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iN2SpeedConfig,    &m_p2SpeedConfig,       pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNValveConfig,     &m_pValveConfig,        pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNRemoteConfig,    &m_pRemoteConfig,       pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNSensorConfig,    &m_pSensorConfig,       pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNDelayConfig,     &m_pDelayConfig,        pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNMacroConfig,     &m_pMacroConfig,        pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNMiscData,        &m_pMiscData,           pMSGA)) return FALSE;
    if (!UnpackConfig(&m_iNLightData,       &m_pLightData,          pMSGA)) return FALSE;
    #ifdef ANYCONFIG
        if (IsMinimumVersion(4,0))
        {
            if (!UnpackConfig(&m_iNIFlowData,  &m_pIFlowData,       pMSGA)) return FALSE;
        }

//      if (IsMinimumVersion(4, 519))
        if (IsMinimumVersion(4, 0))
        {
            if (!UnpackConfig(&m_iNSCGData,  &m_pSCGData,           pMSGA)) return FALSE;
        }

        if (IsMinimumVersion(4,0))
        {
            if (!UnpackConfig(&m_iNSpaIFlowData, &m_pSpaIFlowData,  pMSGA)) return FALSE;
        }
    #else
        if (!UnpackConfig(&m_iNIFlowData,       &m_pIFlowData,          pMSGA)) return FALSE;
    #endif
    

    return TRUE;
}
/*============================================================================*/

    BOOL                        CPoolConfig::SendEquipConfig()

/*============================================================================*/
{
    CHLMSG msgQ(0, HLM_POOL_SETEQUIPCONFIGQ);
    msgQ.PutInt(m_iControllerIndex);

    if (!PackConfig(m_iN2SpeedConfig,    m_p2SpeedConfig,       &msgQ)) return FALSE;
    if (!PackConfig(m_iNValveConfig,     m_pValveConfig,        &msgQ)) return FALSE;
    if (!PackConfig(m_iNRemoteConfig,    m_pRemoteConfig,       &msgQ)) return FALSE;
    if (!PackConfig(m_iNSensorConfig,    m_pSensorConfig,       &msgQ)) return FALSE;
    if (!PackConfig(m_iNDelayConfig,     m_pDelayConfig,        &msgQ)) return FALSE;
    if (!PackConfig(m_iNMacroConfig,     m_pMacroConfig,        &msgQ)) return FALSE;
    if (!PackConfig(m_iNMiscData,        m_pMiscData,           &msgQ)) return FALSE;
    if (!PackConfig(m_iNLightData,       m_pLightData,          &msgQ)) return FALSE;

    #ifdef ANYCONFIG
        if (IsMinimumVersion(4, 0))
        {
            if (!PackConfig(m_iNIFlowData,     m_pIFlowData,    &msgQ)) return FALSE;
            if (!PackConfig(m_iNSpaIFlowData,  m_pSpaIFlowData, &msgQ)) return FALSE;
        }
    #else
        if (!PackConfig(m_iNIFlowData,       m_pIFlowData,          &msgQ)) return FALSE;
    #endif

    msgQ.PutInt(m_bShowAlarms);

    QUERY(q);
    return q.AskQuestion(NULL, &msgQ);
}
/*============================================================================*/

    BOOL                        CPoolConfig::LoadNames(
    CHLObList                   *pList,
    short                       shQ )

/*============================================================================*/
{
    CHLMSG msgQ(0, shQ);
    msgQ.PutInt(0); // Controller Index
    CHLMSG *pMSGA = NULL;  
    QUERY(q);
    if (!q.AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    int iN;
    pMSGA->GetInt(&iN);
    for (int i = 0; i < iN; i++)
    {
        CHLString *pName = new CHLString;
        pMSGA->GetString(pName);
        pList->AddTail(pName);
    }

    return TRUE;
}
/*============================================================================*/

    void                        CPoolConfig::DeleteConfigData()

/*============================================================================*/
{
    delete [] m_pVersionInfo;
    delete [] m_p2SpeedConfig;
    delete [] m_pValveConfig;
    delete [] m_pRemoteConfig;
    delete [] m_pSensorConfig;
    delete [] m_pDelayConfig;
    delete [] m_pMacroConfig;
    delete [] m_pMiscData;
    delete [] m_pLightData;
    delete [] m_pIFlowData;
    delete [] m_pSCGData;
    delete [] m_pSpaIFlowData;

    m_iN2SpeedConfig    = 0;
    m_iNValveConfig     = 0;
    m_iNRemoteConfig    = 0;
    m_iNSensorConfig    = 0;
    m_iNDelayConfig     = 0;
    m_iNMacroConfig     = 0;
    m_iNMiscData        = 0;
    m_iNLightData       = 0;
    m_iNIFlowData       = 0;
    m_iNSCGData         = 0;
    m_iNSpaIFlowData    = 0;

    m_pVersionInfo      = NULL;
    m_p2SpeedConfig     = NULL;
    m_pValveConfig      = NULL;
    m_pRemoteConfig     = NULL;
    m_pSensorConfig     = NULL;
    m_pDelayConfig      = NULL;
    m_pMacroConfig      = NULL;
    m_pMiscData         = NULL;
    m_pLightData        = NULL;
    m_pIFlowData        = NULL;
    m_pSCGData          = NULL;
    m_pSpaIFlowData     = NULL;
}
/*============================================================================*/

    BOOL                    CPoolConfig::UnpackConfig(
    int                     *piCount,
    BYTE                    **ppData,
    CHLMSG                  *pMSG )

/*============================================================================*/
{
    int iCount = 0;
    int iExtra = 0;
    if (!pMSG->GetInt(&iCount))
        return FALSE;
    
    if (iCount < 0 || iCount > 1000)
    {
        ASSERT(FALSE);
        return FALSE;
    }

    iExtra = DWORDAlign(iCount) - iCount;
    BYTE *pData = new BYTE[iCount];
    *ppData = pData;
    int i = 0;
    for (i = 0; i < iCount; i++)
    {
        if (!pMSG->GetBYTE(pData + i))
            return FALSE;
    }
    
    BYTE byPad;
    for (i = 0; i < iExtra; i++)
    {
        if (!pMSG->GetBYTE(&byPad))
            return FALSE;
    }

    *piCount = iCount;
    return TRUE;
}
/*============================================================================*/

    BOOL                    CPoolConfig::PackConfig(
    int                     iCount,
    BYTE                    *pData,
    CHLMSG                  *pMSG )

/*============================================================================*/
{
    int iExtra = 0;
    if (!pMSG->PutInt(iCount))
        return FALSE;
    
    if (iCount < 0 || iCount > 1000)
    {
        ASSERT(FALSE);
        return FALSE;
    }

    iExtra = DWORDAlign(iCount) - iCount;
    int i = 0;
    for (i = 0; i < iCount; i++)
    {
        if (!pMSG->PutBYTE(pData[i]))
            return FALSE;
    }
    
    for (i = 0; i < iExtra; i++)
    {
        if (!pMSG->PutBYTE(0))
            return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    void                        CPoolConfig::UpdateCircuitNames()

/*============================================================================*/
{
    // Assign names
    CHLPos *pPos = m_AllCircuits.GetHeadPosition();
    while (pPos != NULL)
    {
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();

        BYTE byIndex = pC->NameIndex();
        if (byIndex < m_ControllerNames.GetCount())
        {
            CHLPos *pPosName = m_ControllerNames.FindIndex(byIndex);
            CHLString *pName = (CHLString*) pPosName->Object();
            pC->Name(*pName);
        }
        else
        {
            byIndex -= m_ControllerNames.GetCount();
            if (byIndex < m_CustomNames.GetCount())
            {
                CHLPos *pPosName = m_CustomNames.FindIndex(byIndex);
                CHLString *pName = (CHLString*) pPosName->Object();
                pC->Name(*pName);
            }
            else
            {
                pC->Name(CHLXString("None", ""));
            }
        }
    }
}
#endif
/*============================================================================*/

    BOOL                    CPoolConfig::IsMinimumVersion(
    DWORD                   dwMajor,
    DWORD                   dwMinor )

/*============================================================================*/
{
    if (m_wMajorVersion > dwMajor)
        return TRUE;
    if (m_wMajorVersion >= dwMajor && m_wMinorVersion >= dwMinor)
        return TRUE;
    return FALSE;
}
#ifdef CRYSTALPAD
/*============================================================================*/

    CHLString               CPoolConfig::PumpName(
    int                     iPumpIndex )

/*============================================================================*/
{
    BYTE byCir = m_byPumpCir[iPumpIndex];
    CPoolCircuit *pC = CircuitByDeviceID(byCir & 0x3F);
    if (pC && !pC->Name().IsEmpty())
        return pC->Name();

    if (byCir & 0x80)
        return _T("VS");
    if (byCir & 0x40)
        return _T("VSF");
    return _T("VF");
}
/*============================================================================*/

    BOOL                    CPoolConfig::PumpIs4Speed(
    int                     iPumpIndex )

/*============================================================================*/
{
    BYTE byCir = m_byPumpCir[iPumpIndex];
    if ((byCir & 0x80) || (byCir & 0x40))   // VS or VSF
        return TRUE;
    return FALSE;
}
/*============================================================================*/

    BOOL                    CPoolConfig::SchedulesFull(
    int                     iNSchedItems )

/*============================================================================*/
{
    int iNEggTimer = 0;
    CHLPos *pPos = m_AllCircuits.GetHeadPosition();
    while (pPos != NULL)
    {
        CPoolCircuit *pC = (CPoolCircuit*) pPos->Object();
        pPos = pPos->Next();
        if (pC->DefaultRunTime() != 720)
            iNEggTimer++;
    }


    int iMaxSchedules = 99;
    // EasyTouch
    if (m_byDeviceType == 0x0D || m_byDeviceType == 0x0E)
        iMaxSchedules = 12;

    return ((iNSchedItems + iNEggTimer) >= iMaxSchedules);
}
#endif




