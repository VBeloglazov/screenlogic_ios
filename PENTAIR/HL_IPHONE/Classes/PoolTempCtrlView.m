//
//  PoolTempCtrlView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PoolTempCtrlView.h"
#import "NSPoolConfig.h"
#import "PoolTempView.h"
#import "hlradio.h"
#import "hlm.h"
#import "hlbutton.h"
#import "crect.h"

@implementation CPoolTempCtrlView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Type:(int)iType Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_iHeatMode = -1;

        m_pConfig = pConfig;
        m_iType = iType;

        [m_pConfig AddSink:self];

        m_pHeat = [[CPoolTempView alloc] initWithFrame:CGRectZero Config:m_pConfig BodyType:m_iType Mode:TEMP_HEAT]; 
        m_pAct = [[CPoolTempView alloc] initWithFrame:CGRectZero Config:m_pConfig BodyType:m_iType Mode:TEMP_ACT]; 
        m_pHeat.alpha = 0;


        [self addSubview:m_pAct];
        [self addSubview:m_pHeat];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pConfig RemoveSink:self];
    [m_pAct release];
    [m_pHeat release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    int iHeatMode = [m_pConfig HeatMode:m_iType];
    if (iHeatMode != m_iHeatMode)
    {
        [self UpdatePositions];
        m_iHeatMode = iHeatMode;
    }
    [UIView commitAnimations];
}   
/*============================================================================*/

-(void)UpdatePositions

/*============================================================================*/
{    
    int iHeatMode = [m_pConfig HeatMode:m_iType];
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;
    BOOL bHeat = FALSE;

    CGRect rHeat = m_pHeat.frame;
    CGRect rRoom = m_pAct.frame;
    rHeat.size.width = iDXThis * 75 / 100;
    rHeat.size.height = iDYThis * 50 / 100;
    rRoom.size.width = iDXThis / 2;
    rRoom.size.height = iDYThis * 50 / 100;

    BOOL bLandscape = FALSE;
    if (iDXThis > iDYThis * 125 / 100)
        bLandscape = TRUE;

    if (bLandscape)
    {
        rHeat.size.width = iDXThis * 45 / 100;
        rRoom.size.width = iDXThis * 45 / 100;
        rHeat.size.height = iDYThis * 75 / 100;
        rRoom.size.height = iDYThis * 75 / 100;

        rHeat.size.height = MIN(rHeat.size.height, rHeat.size.width);
        rRoom.size.height = MIN(rRoom.size.height, rRoom.size.width);
    }


    switch (iHeatMode)
    {
    case POOLHEAT_OFF:
        {
            rRoom.origin.x = iDXThis / 4;
            rRoom.origin.y = iDYThis / 2 - rRoom.size.height / 2;
        }
        break;

    default:
        {
            bHeat = TRUE;
            if (bLandscape)
            {
                rRoom.origin.x = 0;
                rRoom.origin.y = iDYThis / 2 - rRoom.size.height / 2;
                rHeat.origin.x = iDXThis - rHeat.size.width;
                rHeat.origin.y = iDYThis / 2 - rHeat.size.height / 2;
            }
            else
            {
                rRoom.origin.x = iDXThis / 2 - rRoom.size.width / 2;
                rRoom.origin.y = 0;
                rHeat.origin.x = iDXThis / 2 - rHeat.size.width / 2;
                rHeat.origin.y = iDYThis - rRoom.size.height;
            }

        }
        break;

    }
    
    if (bHeat)
        [m_pHeat setAlpha:1.0];
    else
        [m_pHeat setAlpha:0.0];

    [m_pHeat setUserInteractionEnabled:bHeat];

    [m_pHeat setFrame:rHeat];
    [m_pAct setFrame:rRoom];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;
    if (iDXThis == m_iDXLast && iDYThis == m_iDYLast)
        return;
    m_iDXLast = iDXThis;
    m_iDYLast = iDYThis;
    [self UpdatePositions];
}

@end
