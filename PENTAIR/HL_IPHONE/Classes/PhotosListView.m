//
//  PhotosListView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 8/30/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PhotosListView.h"
#import "PhotoAlbum.h"
#import "CustomPage.h"
#import "MainView.h"
#import "crect.h"
#import "hlm.h"
#import "hlcomm.h"
#import "iconview.h"
#import "PhotosPage.h"

#define ITEM_HEIGHT             48

@implementation CAlbumView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Parent:(CAlbumView*)pParent View:(CPhotosListView*)pView Album:(CPhotoAlbum*)pAlbum

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pParent = pParent;
        m_pMainView = pView;
        m_pAlbum = pAlbum;
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CHLComm GetInt:INT_LIST_TEXTHEIGHT]];
        [m_pLabel setText:[pAlbum Name]];
        [m_pLabel setTextAlignment:UITextAlignmentLeft];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
        m_pSubViews = [[NSMutableArray alloc] init];
        
        [self addTarget:pView action:@selector(SelectAlbum:) forControlEvents:UIControlEventTouchDown];

        if ([m_pAlbum NAlbums] > 0)
        {
            m_pIconView = [[CUserTabIconView alloc] initWithFrame:CGRectZero];
            [m_pIconView SetEmbeddedIcon:@"RIGHT.png"];
            [self addSubview:m_pIconView];
            [m_pIconView release];
        }
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pSubViews release];
    [super dealloc];
}
/*============================================================================*/

-(int)TotalHeight

/*============================================================================*/
{
    int iSum = ITEM_HEIGHT;

    if (m_bExpanded)
    {
        for (int i = 0; i < [m_pSubViews count]; i++)
        {
            CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
            iSum += [pSub TotalHeight];
        }
    }

    return iSum;
}
/*============================================================================*/

-(void)SetSelected:(BOOL)bSelected

/*============================================================================*/
{
    if (bSelected == m_bSelected)
        return;
    m_bSelected = bSelected;
    if (bSelected)
        [m_pLabel setBackgroundColor:[UIColor blueColor]];
    else
        [m_pLabel setBackgroundColor:[CHLComm GetRGB:RGB_APP_BACKGROUND]];

    [self setNeedsDisplay];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [self DoLayout];
}
/*============================================================================*/

-(void)DoLayout

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rLabel = [CRect BreakOffTop:&rThis DY:ITEM_HEIGHT];
    [m_pLabel setFrame:rLabel];
    
    if (m_pIconView != NULL)
    {
        CGRect rIcon = [CRect BreakOffRight:&rLabel DX:ITEM_HEIGHT];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            [CRect CenterDownToX:&rIcon DX:32];
            [CRect CenterDownToY:&rIcon DY:32];
        }

        [m_pIconView setFrame:rIcon];
    }

    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        int iDYSub = [pSub TotalHeight];
        CGRect rSub = [CRect BreakOffTop:&rThis DY:iDYSub];
        [pSub setFrame:rSub];
    }
}
/*============================================================================*/

-(BOOL)FindExpandedViewWithoutChild:(CAlbumView*)pSubView

/*============================================================================*/
{
    if (!m_bExpanded)
        return FALSE;
    if (![self FindSubView:pSubView])
        return TRUE;
        
    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        if ([pSub FindExpandedViewWithoutChild:pSubView])
            return TRUE;
    }
    return FALSE;
}
/*============================================================================*/

-(void)BeginCollapseWithoutChild:(CAlbumView*)pSubView;

/*============================================================================*/
{
    if (!m_bExpanded)
        return;

    // We are expanded
    if (![self FindSubView:pSubView])
    {
        [self BeginCollapse];
    }
        
    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        [pSub BeginCollapseWithoutChild:pSubView];
    }
}
/*============================================================================*/

-(CAlbumView*)Parent

/*============================================================================*/
{
    return m_pParent;
}
/*============================================================================*/

-(CPhotoAlbum*)Album

/*============================================================================*/
{
    return m_pAlbum;
}
/*============================================================================*/

-(BOOL)IsExpanded

/*============================================================================*/
{
    return m_bExpanded;
}
/*============================================================================*/

-(int)NSubAlbums

/*============================================================================*/
{
    return [m_pAlbum NAlbums];
}
/*============================================================================*/

-(void)BeginCollapse

/*============================================================================*/
{
    [m_pIconView SetEmbeddedIcon:@"RIGHT.png"];

    m_bExpanded = FALSE;
    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        [pSub setAlpha:0];
        [pSub BeginCollapse];
    }
}
/*============================================================================*/

-(void)EndCollapse

/*============================================================================*/
{
    if (!m_bExpanded)
    {
        for (int i = 0; i < [m_pSubViews count]; i++)
        {
            CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
            [pSub removeFromSuperview];
        }

        [m_pSubViews removeAllObjects];
    }

    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        [pSub EndCollapse];
    }
}
/*============================================================================*/

-(void)BeginExpand

/*============================================================================*/
{
    if ([m_pAlbum NAlbums] < 1)
        return;

    [self EndCollapse];

    [m_pIconView SetEmbeddedIcon:@"DOWN.png"];

    m_bExpanded = TRUE;
    for (int i = 0; i < [m_pAlbum NAlbums]; i++)
    {
        CPhotoAlbum *pSubAlbum = [m_pAlbum Album:i];
        CAlbumView *pSubView = [[CAlbumView alloc] initWithFrame:CGRectZero Parent:self View:m_pMainView Album:pSubAlbum];
        [self addSubview:pSubView];
        [m_pSubViews addObject:pSubView];
        [pSubView setAlpha:0];    
        [pSubView release];
    }
}
/*============================================================================*/

-(void)CommitExpand

/*============================================================================*/
{
    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pSub = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        [pSub setAlpha:1];
    }
}
/*============================================================================*/

-(BOOL)FindSubView:(CAlbumView*)pSubView

/*============================================================================*/
{
    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pTest = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        if (pTest == pSubView)
            return TRUE;
        if ([pTest FindSubView:pSubView])
            return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(BOOL)FindExpandedView

/*============================================================================*/
{
    if (m_bExpanded)
        return TRUE;

    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pTest = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        if ([pTest FindExpandedView])
            return TRUE;
    }
    return FALSE;
}
/*============================================================================*/

-(void)UpdateState:(CAlbumView*)pExpandedView

/*============================================================================*/
{

    double dA = 1.0;
    if (pExpandedView != NULL)
    {
        dA = 0.5;
        if (pExpandedView == self)
        {
            // Its us
            dA = 1.0;
            pExpandedView = NULL;
        }
    }
    [m_pLabel setAlpha:dA];
    [m_pIconView setAlpha:dA];

    if (!m_bExpanded)
        return;

    for (int i = 0; i < [m_pSubViews count]; i++)
    {
        CAlbumView *pTest = (CAlbumView*)[m_pSubViews objectAtIndex:i];
        [pTest UpdateState:pExpandedView];
    }
}
@end


@implementation CPhotosListView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Root:(CPhotoAlbumRoot*)pRoot PhotosPage:(CPhotosPage *)pPage

/*============================================================================*/
{
    self = [super initWithFrame:frame];

    if (self) 
    {
        m_pMainPage = pPage;
        m_pScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pScrollView];
        [m_pScrollView release];
        m_pItems = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < [pRoot NAlbums]; i++)
        {
            CPhotoAlbum *pAlbum = [pRoot Album:i];
            CAlbumView *pView = [[CAlbumView alloc] initWithFrame:CGRectZero Parent:NULL View:self Album:pAlbum];
            [m_pScrollView addSubview:pView];
            [m_pItems addObject:pView];
            [pView release];
        }

    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pItems release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [self DoLayoutAnimated:NO];
}
/*============================================================================*/

-(void)DoLayoutAnimated:(BOOL)bAnimated

/*============================================================================*/
{
    CGRect rThis = self.bounds;


    int iDYAll = 0;
    for (int i = 0; i < [m_pItems count]; i++)
    {
        CAlbumView *pView = (CAlbumView*)[m_pItems objectAtIndex:i];
        iDYAll += [pView TotalHeight];
    }

    if (iDYAll < rThis.size.height)
    {
        [CRect CenterDownToY:&rThis DY:iDYAll];
    }
    [m_pScrollView setFrame:rThis];
    [m_pScrollView setContentSize:CGSizeMake(rThis.size.width, iDYAll)];

    CGRect rViews = CGRectMake(0, 0, rThis.size.width, iDYAll);

    for (int i = 0; i < [m_pItems count]; i++)
    {
        CAlbumView *pView = (CAlbumView*)[m_pItems objectAtIndex:i];
        int iDYView = [pView TotalHeight];
        CGRect rSub = [CRect BreakOffTop:&rViews DY:iDYView];
        [pView setFrame:rSub];
        if (bAnimated)
        {
            [pView DoLayout];
        }
    }
}
/*============================================================================*/

-(void)SelectAlbum:(id)sender

/*============================================================================*/
{
    CAlbumView *pView = (CAlbumView*)sender;


    if (pView != m_pSelectedItem)
    {
        [m_pSelectedItem SetSelected:NO];
        m_pSelectedItem = pView;
        [m_pSelectedItem SetSelected:YES];
        m_pFocusItem = pView;
        
        if (![m_pFocusItem IsExpanded])
        {
            CAlbumView *pParent = [m_pFocusItem Parent];
            if (pParent != NULL)
            {
                if ([pParent IsExpanded])
                    m_pFocusItem = pParent;
            }
        }

        BOOL bNeedCollapse = FALSE;
        for (int i = 0; i < [m_pItems count] && !bNeedCollapse; i++)
        {
            CAlbumView *pThisView = (CAlbumView*)[m_pItems objectAtIndex:i];
            if ([pThisView FindExpandedViewWithoutChild:pView])
            {
                bNeedCollapse = TRUE;
            }
        }

        if (bNeedCollapse)
        {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];

            for (int i = 0; i < [m_pItems count]; i++)
            {
                CAlbumView *pThisView = (CAlbumView*)[m_pItems objectAtIndex:i];
                [pThisView BeginCollapseWithoutChild:pView];
            }

            [self DoLayoutAnimated:YES];
            [self UpdateItemStates];

            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
            [UIView commitAnimations];
            
        }

        [m_pMainPage SetActiveAlbum:[pView Album]];
    }
    else
    {
        if ([pView NSubAlbums] > 0)
        {
            if ([pView IsExpanded])
            {
                CAlbumView *pParent = [pView Parent];
                if (pParent != NULL)
                {
                    if ([pParent IsExpanded])
                        m_pFocusItem = pParent;
                }

                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.5];

                [m_pSelectedItem BeginCollapse];
                [self DoLayoutAnimated:YES];
                [self UpdateItemStates];

                [UIView setAnimationDelegate:self];
                [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
                [UIView commitAnimations];
            }
            else
            {
                m_pFocusItem = pView;

                [pView BeginExpand];

                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.5];

                [m_pSelectedItem CommitExpand];
                [self DoLayoutAnimated:YES];
                [self UpdateItemStates];

                [UIView setAnimationDelegate:self];
                [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
                [UIView commitAnimations];
            }
        }
    }
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    for (int i = 0; i < [m_pItems count]; i++)
    {
        CAlbumView *pView = (CAlbumView*)[m_pItems objectAtIndex:i];
        [pView EndCollapse];
    }
}
/*============================================================================*/

-(void)UpdateItemStates

/*============================================================================*/
{
    CAlbumView *pExpandedView = NULL;
    if (m_pFocusItem != NULL)
    {
        if ([m_pFocusItem IsExpanded])
            pExpandedView = m_pFocusItem;
    }


    for (int i = 0; i < [m_pItems count]; i++)
    {
        CAlbumView *pView = (CAlbumView*)[m_pItems objectAtIndex:i];
        [pView UpdateState:pExpandedView];
    }

}

@end
