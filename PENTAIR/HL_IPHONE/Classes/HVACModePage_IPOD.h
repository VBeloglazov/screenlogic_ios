//
//  HVACModePage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/29/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CTStat;
@class CHVACTempView;
@class CHLRadio;
@class CHLButton;
@class CHLSegment;

#import "hlview.h"
#import "Sink.h"

/*============================================================================*/

@interface CHVACModePage_IPOD : UIView < CSink, 
                                    CHLCommandResponder,
                                    UIAlertViewDelegate >

/*============================================================================*/
{
    int                                 m_iHVACState;

    CTStat                              *m_pTStat;

    CHLRadio                            *m_pMode;
    CHLRadio                            *m_pFan;

    int                                 m_iModes[10];
}

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat;

-(void)InitModeControl;
-(void)AddLabel:(CGRect)rLabel Text:(NSString*)pText;

@end
