//
//  SplitRoundRegion.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/25/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRoundRegion;


#define SRREGION_DEFAULT                0
#define SRREGION_SQUARE_CORNERS         1
#define SRREGION_SMALL_HEADER           2
#define SRREGION_MAIN_WINDOW            3

#define LAYOUT_OVERUNDER                0
#define LAYOUT_LEFTRIGHT                1

#define SRREGION_FLAG_AUTOLAYOUT        0x00000001

/*============================================================================*/

@interface CSplitRoundRegion : UIView 

/*============================================================================*/
{
    int                             m_iLayout;
    int                             m_iStyle;
    int                             m_iFlags;
    CRoundRegion                    *m_pRgn1;
    CRoundRegion                    *m_pRgn2;
    UILabel                         *m_pTitle;
}

-(id)initWithFrame:(CGRect)frame Style:(int)iStyle;
-(id)initWithFrame:(CGRect)frame Title:(NSString*)pTitle;
-(void)SetTitle:(NSString*)pTitle;
-(CRoundRegion*)UpperRegion;
-(CRoundRegion*)LowerRegion;
-(void)SetFlags:(int)iFlags;

@end
