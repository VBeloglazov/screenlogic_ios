/*
--------------------------------------------------------------------------------

    HLDC.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLDC_H
#define HLDC_H


class CHLWindow;
class CHLFont;
class CHLMemDC;
class CHLDibDC;
class CHLRect;
class CHLDDB;
class CHLDC;
class CEdgeList;
class CHLDibDC;
class CRectList;
class CHL3DButton;
class CHiResTimer;
class CAlphaDIB;
class CGradient2D;

#ifndef HL_PIXEL
    #ifdef HL_PIXEL32
        #define     HL_PIXEL    DWORD
    #else
        #define     HL_PIXEL    WORD
    #endif
#endif

#if defined(CRYSTALPAD) | defined(DDTEST) | defined(HLOSD)
    #if defined(HL_ARMV4IC) | defined(HL_ARMV4B) | defined(HL_X86C)
        #define HL_DIRECTDRAWSURFACE        LPDIRECTDRAWSURFACE
        #define HL_SURFACEDESC              DDSURFACEDESC
        #define HL_IDIRECTDRAW              IDirectDraw
        #define HL_DDRAWBASIC
    #else
        #define HL_DIRECTDRAWSURFACE        LPDIRECTDRAWSURFACE4
        #define HL_SURFACEDESC              DDSURFACEDESC2
        #define HL_IDIRECTDRAW              IDirectDraw4
        #define BLTFAST_FLAGS               DDBLTFAST_NOCOLORKEY
    #endif
#else
    #define HL_DIRECTDRAWSURFACE            LPDIRECTDRAWSURFACE4
    #define HL_SURFACEDESC                  DDSURFACEDESC2
    #define HL_IDIRECTDRAW                  IDirectDraw4
    #define BLTFAST_FLAGS                   DDBLTFAST_NOCOLORKEY
#endif

#ifdef HLDDRAW
    #include "ddraw.h"
#endif

#include "hlrect.h"
#include "hlm.h"
#include "hloblist.h"

#define DIR_NONE                -1
#define DIR_UP                  0
#define DIR_DOWN                1
#define DIR_LEFT                2
#define DIR_RIGHT               3

#define RGB_TRANSPARENT         RGB(0,0,0)

#define RADIUS_ON               0x00000000
#define RADIUS_INSET            0x00000001
#define RADIUS_OUTSET           0x00000002
#define RADIUS_JOINHZ           0x00000004
#define RADIUS_JOINVT           0x00000008
#define RADIUS_AAON             0x00000010
#define RADIUS_OUTSET_REL       0x00000020

#define GEO_MASK                (RADIUS_ON|RADIUS_INSET|RADIUS_OUTSET|RADIUS_OUTSET_REL)

#define BLEND_STEPS             8

/*============================================================================*/

    class CTextOut

/*============================================================================*/
{
protected:
    CHLRect                     m_rRect;
    CHLString                   m_sText;
    int                         m_iFormat;
    int                         m_iTextSize;
    COLORREF                    m_rgbColor;
    int                         m_iXOrigin;
    int                         m_iYOrigin;

public:
    CTextOut(
    CHLRect                     rect,
    CHLString                   sText,
    int                         iFormat,
    int                         iTextSize,
    COLORREF                    rgbColor,
    int                         iXOrg,
    int                         iYOrg );

    inline CHLRect              Rect()          { return m_rRect;       }
    inline CHLString            Text()          { return m_sText;       }
    inline int                  Format()        { return m_iFormat;     }
    inline int                  TextSize()      { return m_iTextSize;   }
    inline COLORREF             Color()         { return m_rgbColor;    }
    inline int                  XOrigin()       { return m_iXOrigin;    }
    inline int                  YOrigin()       { return m_iYOrigin;    }
};
/*============================================================================*/

    class CHLDC

/*============================================================================*/
{
public:

protected:
    HDC                         m_hDC;
    HWND                        m_hWnd;

#ifndef IPHONE
    CHLWindow                   *m_pWnd;
#endif

    COLORREF                    m_rgbColor;
    BOOL                        m_bOwnDC;
    int                         m_iXLeft;
    int                         m_iYTop;
    int                         m_iFontHeight;

    CHLObList                   m_ClipStack;
    CHLRect                     *m_pClipGlobal;
    
    CHLObList                   m_OriginStack;

    CHLObList                   m_TextOut;

    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE    m_pSurface;
    #endif

    int                         m_iAttribs;
    BOOL                        m_bOutEnabled;

public:
    CHLDC();

#ifndef IPHONE
    CHLDC(
    CHLWindow                   *pWnd );
    inline CHLWindow            *Wnd()          { return m_pWnd;            }
#endif

    #ifndef MAC_OS
    CHLDC(
    CHLWindow                   *pWnd,
    HDC                         hDC );
    #endif

    #ifdef HLDDRAW
        void                    Attach(
        HL_DIRECTDRAWSURFACE    pSurface );
    #endif

    #ifdef HLDDRAW
        virtual HL_DIRECTDRAWSURFACE                        GetSurface();
    #endif

    inline int                  FontSize()                  { return m_iFontHeight; }
    inline COLORREF             Color()                     { return m_rgbColor;    }


    inline CHLRect              *GlobalClip()               { return m_pClipGlobal; }

    void                        GlobalClip(
    CHLRect                     *pNew );

    inline int                  Attribs()                   { return m_iAttribs;    }
    void                        Attribs(int iNew);

    // This is NOT a virtual destructor, and will NOT be called when a derived
    // class is deleted.  Be careful in derived classes to clean up any CHLDC
    // variables that you use.
    ~CHLDC();

    static void                 InitBlend();
    static void                 DeallocBlend();

    virtual POINT               GetOrigin();
    virtual POINT               GetOriginAbs();

    void                        SetOrigin(
    POINT                       ptNew );

#ifndef IPHONE
    void                        SetOrigin(
    CHLWindow                   *pWnd );
#endif

    virtual void                SetOrigin(
    int                         iXLeft,
    int                         iYTop );

    void                        TranslateOrigin(
    int                         iDX,
    int                         iDY );
    
    virtual CHLDibDC            *DibDC();

#ifndef IPHONE
    virtual void                SetQuickFont(
    int                         iHeight );

    CHLFont                     *Font();

    static UINT                 GDIFormat(
    int                         iHLFormat );
#endif

#ifndef IPHONE
    virtual void                DrawString(
    CHLString                   sText,
    CHLRect                     rect,
    int                         iHLFormat );
#endif

    //
    //
    //
    void                        DrawRect(
    COLORREF                    rgb,
    CHLRect                     rect );

    void                        DrawRect(
    COLORREF                    rgb,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY);

#ifndef IPHONE
    virtual void                FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pRectList );

    virtual void                FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pRectList,
    CHLDibDC                    *pDIB,
    int                         iXSrcOffset,
    int                         iYSrcOffset );

    virtual void                FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pList,
    CGradient2D                 *pGradient,
    int                         iXSrcOffset,
    int                         iYSrcOffset );
#endif

    virtual void                FillRect(
    COLORREF                    rgbFill,
    CHLRect                     rect );

    virtual HDC                 GetHDC();

    virtual void                FillRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY );

    virtual void                FillRoundRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iGeoType = RADIUS_ON );

    void                        Draw3DFrame(
    CHLRect                     rOuter,
    int                         iThickness,
    COLORREF                    rgb,
    BOOL                        bRaised = FALSE );

    void                        DrawFilled3DBorder(
    COLORREF                    rgbFace,
    BOOL                        bRaised,
    int                         iXUL,
    int                         iYUL,
    int                         iXLR,
    int                         iYLR,
    int                         iBorder );

    void                        DrawFilled3DBorder(
    COLORREF                    rgbFace,
    COLORREF                    rgbEdge,
    BOOL                        bRaised,
    int                         iXUL,
    int                         iYUL,
    int                         iXLR,
    int                         iYLR,
    int                         iBorder );

    void                        DrawFilled3DBorder(
    COLORREF                    rgbFace,
    COLORREF                    rgbEdge,
    BOOL                        bRaised,
    int                         iXUL,
    int                         iYUL,
    int                         iXLR,
    int                         iYLR,
    int                         iBorder,
    float                       fSpotFactor );

    virtual void                SetColor(
    COLORREF                    rgbNew );

    void                        SetBkColor(
    COLORREF                    rgbNew );

    virtual void                DrawLine(
    COLORREF                    rgb,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iPenSize = 1 );

    void                        DrawPolygon(
    COLORREF                    rgb,
    int                         *pX,
    int                         *pY,
    int                         iNPoints );

    void                        DrawPolygon(
    COLORREF                    rgb,
    POINT                       *pPoints,
    int                         iNPoints );

    void                        DrawArrow(
    BOOL                        bRaised,
    CHLRect                     rBounds,
    int                         iDir,
    COLORREF                    rgbFace,
    BOOL                        bDim );

    CHLRect                     GetBltRect(
    CHLDibDC                    *pSrc, 
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

    CHLRect                     GetBltRect(
    CGradient2D                 *pSrc, 
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

    BOOL                        DDrawScale(
    CHLDibDC                    *pSrc,
    CHLRect                     rDst,
    CHLRect                     rSrc );

#ifdef HL_ARMV4IC
    BOOL                        DDrawBlendBlt(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    BYTE                        byAlpha );

    BOOL                        DDrawBltFromAlphaDIB(
    CAlphaDIB                   *pAlpha,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY );
#endif

    void                        Blt(
    int                         iXDest,
    int                         iYDest,
    CHLDC                       *pSrcDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    CHLDibDC                    *pHelperDC = NULL );

    virtual void                BltFromGradient(
    CGradient2D                 *pGradient,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iAlpha = 100 );

    virtual void                BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    CRectList                   *pList );

    virtual void                BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest );

    virtual void                BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    CEdgeList                   *pEdgeList );

    virtual void                BltFromDibDC(
    int                         iXDest,
    int                         iYDest,
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iFlags = 0);

    void                        BltToDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc );

    void                        BltToDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY );

    void                        MoveBlt(
    int                         iXSrc,
    int                         iYSrc,
    int                         iDXRect,
    int                         iDYRect,
    int                         iDXMove,
    int                         iDYMove );

    CHLRect                     IntersectClip(
    CHLRect                     rIn );

    virtual BOOL                TestClip(
    CHLRect                     rIn );

#ifndef IPHONE
    void                        PushOrigin(CHLWindow *pWnd);
#endif

    void                        PopOrigin();

    void                        PushClipDXDY(int iX0, int iY0, int iDX, int iDY);
    void                        PushClip12(int iX0, int iY0, int iX1, int iY1);
    virtual void                PushClip(CHLRect *pClip);
    virtual void                PopClip();

    static COLORREF             ScaleColor(
    double                      dScale,
    COLORREF                    rgbIn );

    static COLORREF             ScaleColorPCT(
    int                         iPCT,
    COLORREF                    rgbIn );


    static void                 CreateGradient(
    COLORREF                    rgbBase,
    int                         iD,
    COLORREF                    *pT,
    COLORREF                    *pB );

    static COLORREF             Blend(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    double                      dD2D1 );

    static COLORREF             BlendPCT(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iPCT2 );

    static COLORREF             Add(
    COLORREF                    rgb,
    COLORREF                    rgbAdd );

#ifndef IPHONE
    static CHLString            TruncateText(
    int                         iFontSize,
    CHLString                   sText,
    int                         iDXMax );

    static void                 TruncateText(
    int                         iFontSize, 
    TCHAR                       *pText,
    int                         *piNChars,
    int                         iDXMax );

    static CHLString            SplitText(
    int                         iTextSize,
    CHLString                   sIn,
    int                         iDXMax,
    int                         iSearchSpace,
    int                         iMaxLines);

    static CHLString            SplitText(
    int                         iTextSize,
    CHLString                   sIn,
    int                         iDXMax,
    int                         iSearchSpace,
    int                         *piMaxLines = NULL );

    static int                  Split(
    int                         iTextSize,
    CHLString                   sIn,
    int                         iDXMax,
    int                         iSearchSpace );    
#endif

    BOOL                        CheckRenderFont();

    void                        AntiAliasEdge(
    CHLObList                   *pEdgeList,
    COLORREF                    rgbFace,
    COLORREF                    rgbBackground );

    void                        FlushText();


protected:
    virtual void                SetActiveClip(
    CHLRect                     *pClip );

    CHLRect                     *CreateActiveClip();

    void                        Clip(
    int                         *piXDest,
    int                         *piYDest,
    int                         *piXOther,
    int                         *piYOther,
    int                         *piDX,
    int                         *piDY );
};



#endif

