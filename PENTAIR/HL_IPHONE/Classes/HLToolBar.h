//
//  HLToolBar.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 3/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainView.h"


@class IPOD_ViewController;
@class CIPADMainView;
@class CAAnimationGroup;

#define TOOLBAR_NONE        0
#define TOOLBAR_TOSYSMENU   1
#define TOOLBAR_FROMSYSMENU 2
#define TOOLBAR_RIGHTTOLEFT 3
#define TOOLBAR_LEFTTORIGHT 4

/*============================================================================*/

@interface CHLSegmentHighlight : UIView

/*============================================================================*/
{
    int                     m_iRUL;
    int                     m_iRUR;
    int                     m_iRLL;
    int                     m_iRLR;
    float                   m_fR;
    float                   m_fG;
    float                   m_fB;

    int                     m_iColor1;
    int                     m_iColor2;
}

-(id)InitWithFrame:(CGRect)frame Color1:(int)iIndex1 Color2:(int)iIndex2;
-(void)SetRadiusLeft:(int)iRL Right:(int)iRR;
-(void)SetRadiusUL:(int)iRUL UR:(int)iRUR LL:(int)iRLL LR:(int)iRLR;
-(void)SetColorR:(float)fR G:(float)fG B:(float)fB;

@end
/*============================================================================*/

@interface CToolBarHighlight : UIView

/*============================================================================*/
{
    int                     m_iR1;
    int                     m_iR2;
}
-(id)initWithFrame:(CGRect)rFrame UpperRad:(int)iR1 LowerRad:(int)iR2;

@end

/*============================================================================*/

@interface CToolBarBackground : UIView

/*============================================================================*/
{
    int m_iOrientation;
}

-(id)initWithFrame:(CGRect)rFrame Orientation:(int)iOrientation;
-(void)Invalidate;

@end

/*============================================================================*/

@interface CToolBarClientView : UIView

/*============================================================================*/
{
}

-(UIView*)InsetLeft;
-(UIView*)InsetRight;
-(BOOL)IsSystemMenu;

@end

/*============================================================================*/

@interface CHLToolBar : UIView

/*============================================================================*/
{
    CToolBarBackground                  *m_pBG;
    CToolBarHighlight                   *m_pHL;
    CToolBarClientView                  *m_pClientView;
    CToolBarClientView                  *_m_pClientView;
    int                                 m_iAnimationType;
}


-(id)initWithFrame:(CGRect)rFrame Orientation:(int)iOrientation;
-(void)SetBackgroundAlpha:(double)dAlpha;
-(void)SetClientView:(CToolBarClientView*)pView AnimationType:(int)iType;
-(void)CommitAnimations;
-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;
-(void)Invalidate;
-(UIView*)Background;
-(void)HLAddSubView:(UIView*)pSubView;
-(CToolBarClientView*)ClientView;
-(CToolBarClientView*)ActiveClientView;
-(void)AnimationComplete;

@end
