//
//  TunerPage_XM.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "TunerPage_XM.h"
#import "TunerFavsPage.h"
#import "AudioZone.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "HLComm.h"
#import "MediaZonePage.h"
#import "TableTitleHeaderView.h"
#import "crect.h"
#import "HLTableFrame.h"
#import "CustomPage.h"

#define TABLE_ROW_HEIGHT            28
#define MAX_CHARS_TITLE             12

@implementation CXMGenre

/*============================================================================*/

-(id)initWithID:(int)iID Name:(NSString*)pName View:(CTunerPageXM*)pView

/*============================================================================*/
{
    m_pView = pView;
    m_iID = iID;

    m_pName = pName;
    [m_pName retain];
    m_pStations = [[NSMutableArray alloc] init];

    [CHLComm AddSocketSink:self];

    if (iID != -1)
    {
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETXMSTATIONSBYCATIDQ];
        [pQ autorelease];
        [pQ PutInt:m_iID];
        [pQ SendMessageWithMyID:self];
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pName release];
    [m_pStations release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_AUDIO_GETXMSTATIONSBYCATIDA)
        return;

    [m_pStations removeAllObjects];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        int iID = [pMSG GetInt];
        NSString *pName = [pMSG GetString];
        CStation *pS = [[CStation alloc] initWithID:iID Icon:@"" Name:pName];
        [m_pStations addObject:pS];
        [pS release];
    }

        
    [CHLComm RemoveSocketSink:self];

    [m_pView GroupLoaded];
}
/*============================================================================*/

-(int)ID                { return m_iID;                 }
-(NSString*)Name        { return m_pName;               }
-(int)NStations         { return [m_pStations count];   }

/*============================================================================*/

-(CStation*)Station:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pStations count])
        return NULL;
    CStation *pS = (CStation*)[m_pStations objectAtIndex:iIndex];
    return pS;
}

@end

@implementation CTunerPageXM

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone MediaZonePage:(CMediaZonePage*)pZonePage

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        [self setBackgroundColor:[UIColor clearColor]];

        m_pMediaZonePage = pZonePage;
        m_pGenres = [[NSMutableArray alloc] init];
        m_pSectionTitles = [[NSMutableArray alloc] init];
        
        CXMGenre *pZero = [[CXMGenre alloc] initWithID:-1 Name:@"Working..." View:self];
        [m_pGenres addObject:pZero];
        [pZero release];
        
        m_pZone = pZone;
        [CHLComm AddSocketSink:self];

        [m_pZone InitData];

        m_pMainView = [[UIView alloc] initWithFrame:self.bounds];
        [m_pMainView setBackgroundColor:[UIColor clearColor]];
        m_pMainView.autoresizingMask = 0xFFFFFFFF;
        [self addSubview:m_pMainView];

        
        m_pTableView = [[CHLTableFrame alloc] initWithFrame:self.bounds DataSource:self Delegate:self];
    
        // set the autoresizing mask so that the table will always fill the view
        m_pTableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
        
        // set the tableview delegate to this object and the datasource to the datasource which has already been set
        [m_pMediaZonePage LockHorizontalScroll:TRUE];
        [m_pTableView setTag:999];
        
        int iTunerType = ([m_pZone ActiveSubSource] & 0x0000FFFF);
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETALLXMCATSQ];
        [pQ autorelease];
        [pQ PutInt:iTunerType];
        [pQ SendMessageWithMyID:self];
        
        [m_pMainView addSubview:m_pTableView];


//        m_pFavsView = [[CTunerFavsPage alloc] initWithFrame:[self FavsRect] Zone:m_pZone LowerBorder:FALSE];
//        m_pFavsView.autoresizingMask = 0xFFFFFFFF;
//        [m_pMainView addSubview:m_pFavsView];
    }

    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    // Drawing code
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pMainView release];
    [m_pTableView release];
    [m_pSectionTitles release];
    [m_pGenres release];

    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView 

/*============================================================================*/
{
	// returns the array of section titles. There is one entry for each unique character that an element begins with
	// [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    return m_pSectionTitles;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index 

/*============================================================================*/
{
	return index;
}
/*============================================================================*/

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 

/*============================================================================*/
{
    if (section >= [m_pSectionTitles count])
        return @"";

    NSString *pS = (NSString*)[m_pSectionTitles objectAtIndex:section];
    return pS;
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return [m_pGenres count];
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    CXMGenre *pGenre = (CXMGenre*)[m_pGenres objectAtIndex:indexPath.section];
    CStation *pS = [pGenre Station:indexPath.row];
    
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_TUNEZONETOXMBYIDQ];
    [pQ autorelease];
    [pQ PutInt:m_pZone.m_iID];
    [pQ PutInt:[pS ID]];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    CXMGenre *pGenre = (CXMGenre*)[m_pGenres objectAtIndex:section];
    return [pGenre NStations];
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    static NSString *CellIdentifier = @"Cell";
    CXMGenre *pGenre = (CXMGenre*)[m_pGenres objectAtIndex:indexPath.section];
    CStation *pStation = [pGenre Station:indexPath.row];
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }

    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setText:[pStation Name]];
    [CCustomPage InitStandardLabel:cell.textLabel Size:18];
    [cell.textLabel setTextAlignment:UITextAlignmentLeft];

    // Configure the cell
    return cell;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iMSGID = [pMSG MessageID];
    switch (iMSGID)
    {
    case HLM_AUDIO_GETALLXMCATSA:
        {
            [m_pGenres removeAllObjects];
            [m_pSectionTitles removeAllObjects];

            int iN = [pMSG GetInt];
            for (int i = 0; i < iN; i++)
            {
                int iID = [pMSG GetInt];
                NSString *pName = [pMSG GetString];
                CXMGenre *pGenre = [[CXMGenre alloc] initWithID:iID Name:pName View:self];

                [m_pSectionTitles addObject:[self TruncateGenreTitle:pName]];

                [m_pGenres addObject:pGenre];
                [pGenre release];
            }

            [m_pTableView ReloadData];
        }
        break;
    }
}
/*============================================================================*/

-(void)DeselectAll

/*============================================================================*/
{
    for (int i = 0; i < [m_pGenres count] ; i++)
    {
        CXMGenre *pGenre = (CXMGenre*)[m_pGenres objectAtIndex:i];
        for (int iStation = 0; iStation < [pGenre NStations]; iStation++)
        {
            NSIndexPath *pPath = [NSIndexPath indexPathForRow:iStation inSection:i];
            [[m_pTableView TableView] deselectRowAtIndexPath:pPath animated:YES];
        }
    } 
}
/*============================================================================*/

-(void)GroupLoaded

/*============================================================================*/
{
    [m_pTableView ReloadData];
}
/*============================================================================*/

-(NSString*)TruncateGenreTitle:(NSString*)pName

/*============================================================================*/
{
    int iLen = [pName length];
    if (iLen < MAX_CHARS_TITLE)
        return pName;
    const char *pChars = [pName UTF8String];
    int iChopIndex = MAX_CHARS_TITLE;
    for (int iChop = MAX_CHARS_TITLE-1; iChop > 5 && (iChopIndex == MAX_CHARS_TITLE); iChop--)
    {
        if (pChars[iChop] == ' ')
            iChopIndex = iChop;
    }
    NSString *pNew = [pName substringToIndex:iChopIndex-1];
    NSString *pRet = [NSString stringWithFormat:@"%s...", [pNew UTF8String]];
    return pRet;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pMainView setFrame:self.bounds];
    [m_pTableView setFrame:m_pMainView.bounds];
}


@end
