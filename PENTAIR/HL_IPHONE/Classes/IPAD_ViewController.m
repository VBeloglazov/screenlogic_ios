    //
//  IPAD_ViewController.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/20/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//


#import "IPAD_ViewController.h"
#import "AppDelegate.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "MainTab.h"
#import "tconnect.h"
#import "hlcomm.h"
#import "SinkServer.h"
#import "crect.h"
#import "HLToolBar.h"
#import "systembar.h"
#import "SubSystemPage.h"
#import "zoneselector.h"
#import "SubTab.h"
#import "Weather.h"
#import "shader.h"


@implementation CIPADMainView
/*============================================================================*/

-(void)SetController:(IPAD_ViewController*)pController

/*============================================================================*/
{
    m_pController = pController;
}
/*============================================================================*/

-(void)SetToolBar:(UIView*)pView

/*============================================================================*/
{
    m_pToolBar = pView;
}
/*============================================================================*/

-(void)SetContent:(CSubSystemPage*)pView

/*============================================================================*/
{
    m_pContent = pView;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [self DoLayout];
}
/*============================================================================*/

-(void)DoLayout

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rContent = rThis;
    CGRect rToolBar;
    CGRect rSideBar;
    
    int iDXToolBar = [CMainView DX_SIDEBAR];
    rSideBar = CGRectMake(-iDXToolBar, 0, iDXToolBar, rThis.size.height);

    int iLayout = [m_pController Layout];

    switch (iLayout) 
    {
    case LAYOUT_DEFAULT:
        {
            printf("LAYOUT DEFAULT\n");
            int iDYToolBar = [CMainView DY_TOOLBAR];
            rToolBar = [CRect BreakOffTop:&rContent DY:iDYToolBar];
        }
        break;

    case LAYOUT_LARGESYSBAR_TOP:
        {
            printf("LAYOUT LARGE SYSBAR TOP\n");
            int iDYToolBar = [CMainView DY_TOOLBAR_LARGE];
            rToolBar = [CRect BreakOffTop:&rContent DY:iDYToolBar];
        }
        break;
    case LAYOUT_LARGESYSBAR_TOP_OFFSET:
        {
            printf("LAYOUT LARGE SYSBAR TOP OFFSET\n");
            int iDYToolBar1 = [CMainView DY_TOOLBAR];
            int iDYToolBar2 = [CMainView DY_TOOLBAR_LARGE];
            rToolBar = [CRect BreakOffTop:&rContent DY:iDYToolBar2];
            rContent.size.height = rThis.size.height - iDYToolBar1;
        }
        break;

    case LAYOUT_SIDESYSBAR:
        {
            printf("LAYOUT SIDESYSBAR\n");
            int iDYToolBar = [CMainView DY_TOOLBAR];
            rToolBar = CGRectMake(0, -iDYToolBar, rThis.size.width, iDYToolBar);
            rSideBar = [CRect BreakOffLeft:&rContent DX:iDXToolBar];
        }
        break;

    case LAYOUT_SIDESYSBAR_OFFSET:
        {
            printf("LAYOUT SIDESYSBAR OFFSET\n");
            int iDYToolBar = [CMainView DY_TOOLBAR];
            rToolBar = [CRect BreakOffTop:&rContent DY:iDYToolBar];
            rContent.origin.y -= iDYToolBar/2;
            rToolBar.origin.y -= iDYToolBar;
            rContent.origin.x += iDXToolBar;
            rToolBar.origin.x += iDXToolBar;
            rSideBar.origin.x = 0;
        }
        break;
    }

    if ([m_pController HideNavBar])
    {
        switch (iLayout) 
        {
        case LAYOUT_DEFAULT:
            {
                int iDYToolBar = [CMainView DY_TOOLBAR];
                rToolBar = CGRectMake(0, -iDYToolBar, rThis.size.width, iDYToolBar);
            }
            break;

        case LAYOUT_LARGESYSBAR_TOP:
        case LAYOUT_LARGESYSBAR_TOP_OFFSET:
            {
                int iDYToolBar = [CMainView DY_TOOLBAR_LARGE];
                rToolBar = CGRectMake(0, -iDYToolBar, rThis.size.width, iDYToolBar);
            }
            break;

        case LAYOUT_SIDESYSBAR:
        case LAYOUT_SIDESYSBAR_OFFSET:
            {
                int iDXToolBar = [CMainView DY_TOOLBAR];
                rToolBar = CGRectMake(-iDXToolBar, 0, iDXToolBar, rThis.size.height);
            }
            break;
        }
    }

    [m_pToolBar setFrame:rToolBar];
    [m_pContent setFrame:rContent];
}
@end

@implementation IPAD_ViewController

    static IPAD_ViewController          *g_pController = NULL;

/*============================================================================*/

- (void)viewDidLoad 

/*============================================================================*/
{
    [super viewDidLoad];
    
    CIPADMainView *pMain = [[CIPADMainView alloc]init];
    [self setView:pMain];
    [pMain SetController:self];

    m_bInit = TRUE;
    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(CheckConnectTimer) userInfo:nil repeats:YES] retain];
	m_pMainTabs = [[NSMutableArray alloc] init];

    CGRect rBounds = self.view.bounds;
    CGRect rToolBar = [CRect BreakOffTop:&rBounds DY:[CMainView DY_TOOLBAR]];
    
    m_pToolBar = [[CHLToolBar alloc] initWithFrame:rToolBar Orientation:TOOLBAR_TOP];
    [self.view addSubview:m_pToolBar];
    [m_pToolBar release];
    
    [pMain SetToolBar:m_pToolBar];
    
    g_pController = self;
}
/*============================================================================*/

-(NSUInteger)supportedInterfaceOrientations

/*============================================================================*/
{

    int iRet = 0;
    if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait])
        iRet |= UIInterfaceOrientationMaskPortrait;
    if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortraitUpsideDown])
        iRet |= UIInterfaceOrientationMaskPortraitUpsideDown;
    //DL added
    if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationLandscapeRight])
        iRet |= UIInterfaceOrientationMaskLandscapeRight;
    if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationMaskLandscapeLeft])
        iRet |= UIInterfaceOrientationMaskLandscapeLeft;
    
    return iRet;

}
/*============================================================================*/

- (BOOL)shouldAutorotate

/*============================================================================*/
{
    return YES;
}
/*============================================================================*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

/*============================================================================*/

{
    // Overriden to allow any orientation.
    BOOL bOK = TRUE;
    
    if (m_pConnectPage != NULL)
        bOK = FALSE;

    if (!bOK)
    {
        if (m_pConnectPage != NULL)
        {
           return [m_pConnectPage shouldAutorotateToInterfaceOrientation:interfaceOrientation];
        }

        switch(interfaceOrientation)
        {
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
            case UIInterfaceOrientationLandscapeRight:
            case UIInterfaceOrientationLandscapeLeft:
            return TRUE;
        default:
            break;
        }
        return FALSE;
    }
    return YES;
}

//-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
    //[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:<#duration#>];
//    CGRect bounds = self.view.bounds;
//    int yy = 0;
//}

/*============================================================================*/

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
    //DL it is not getting there in iOS8, apparently for 5.1
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIApplication *pApp = [UIApplication sharedApplication];
        switch (toInterfaceOrientation)
        {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            pApp.statusBarHidden = TRUE;
            break;

        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            pApp.statusBarHidden = FALSE;
            break;
        }
        //DL - comment this out to prevent Connection page on the background
        //m_pConnectPage = nil;
        //[self ShowConnectPage];
        
        [self HideConnectPage];//looks like the best solution - removes Connection page paroperly after roation
    }
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    //DL this is for 6.0 and higher
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIApplication *pApp = [UIApplication sharedApplication];
        switch ([[UIApplication sharedApplication] statusBarOrientation])
        {
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
                pApp.statusBarHidden = TRUE;
                break;
                
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
                pApp.statusBarHidden = FALSE;
                break;
        }
        //DL - comment this out to prevent Connection page on the background
        //m_pConnectPage = nil;//this will refresh the page after rotation but will prevent from showing it in background
        //[self ShowConnectPage];
        
        [self HideConnectPage];//looks like the best solution - removes Connection page paroperly after roation
    }
}

/*============================================================================*/

- (void)didReceiveMemoryWarning 

/*============================================================================*/
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
/*============================================================================*/

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation

/*============================================================================*/
{
    //CGRect r = self.view.bounds;
    [m_pContentPage NotifyRotationComplete:self.interfaceOrientation];
}
/*============================================================================*/

- (void)viewDidUnload 

/*============================================================================*/
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
/*============================================================================*/

- (void)loadView 

/*============================================================================*/
{
    CIPADMainView *pView = [[CIPADMainView alloc] initWithFrame:CGRectZero];
    pView.backgroundColor = [UIColor clearColor];
    [self setView:pView];
    [pView release];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pZoomDelegate release];
    [m_pTimer invalidate];

    [m_pConnectPage release];
    [m_pTimer release];
	[m_pMainTabs release];
    [m_pLockView release];
	[super dealloc];    
}


/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        [self HideConnectPage];

        [CHLComm LoadSettings];
        
        [m_pToolBar Invalidate];

        CGRect rBounds = self.view.bounds;
        CGRect rToolBar = [CRect BreakOffTop:&rBounds DY:[CMainView DY_TOOLBAR]];
        [m_pToolBar setFrame:rToolBar];
        

        CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETUSERINFOQ];
        if ([pQ1 AskQuestion])
        {
            [pQ1 GetInt]; // Base Port
            [pQ1 GetInt]; // External Port
            NSString *pSysName = [pQ1 GetString];
    
            // More stuff here we don't care about now
            if ([pSysName isEqual:m_pSysName])
            {
                //DL - investigate if it is really needed here!
                //[pQ1 release];
                //return;
            }

            [m_pSysName release];
            m_pSysName = pSysName;
            [m_pSysName retain];

            // Remove any pages that may be pushed up 
            [[self navigationController] popToRootViewControllerAnimated:NO];
        }


        CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
        CSystemModeList *pList =  [pDelegate SysModeList];
        [pList RemoveSink:self];
        pList = [pDelegate InitSysModeList];
        [pList AddSink:self];
        
        [pQ1 release];

 
        [m_pMainTabs removeAllObjects];

        [pDelegate InitSounds];

#ifdef PENTAIR
        //
        // Pentair Mode
        //
        //[self SetBackgroundImage:2000];

        CNSQuery *pQVer = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETVERSIONQ];
        if ([pQVer AskQuestion])
        {
            [pQVer GetString];
            [pQVer GetInt];
            int iVer1 = [pQVer GetInt];
            int iVer2 = [pQVer GetInt];
            [CHLComm SetVersion:iVer1 Sub:iVer2];
        }

        [pQVer release];
        [m_pMainTabs removeAllObjects];

        //DL - trying to avoid recreation, keep old one
        //it is not good since it remembers the very first configuration
        if(NULL == [pDelegate PoolConfig])
        {
            [pDelegate InitPoolConfig];
        }
        else
        {
            //DL this is to prevent stop polling after new Home page was loaded
            [m_pContentPage removeFromSuperview];
            //DL new stuff
            m_pContentPage = NULL;
            [pDelegate RestartPoolConfig];
        }

 #endif


        CNSQuery *pQ2 = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETTABCONFIGQ];
        CMainTab *pHomeTab = NULL;
        if ([pQ2 AskQuestion])
        {
            int iBGMode = [pQ2 GetInt];    // Enable VTabs
            int iNTabs = [pQ2 GetInt];
    
            [self SetBackgroundImage:iBGMode];

            for (int iTab = 0; iTab < iNTabs; iTab++)
            {
                int iTabType = [pQ2 GetInt];
                NSString *pTabName = [pQ2 GetString];

#ifdef PENTAIR
                BOOL bOK = FALSE;
                switch (iTabType)
                {
                case TAB_HOME:
                case TAB_POOL:
                case TAB_LIGHTING:
                case TAB_VIDEO:
                    bOK = TRUE;
                    break;
                }
#else
                BOOL bOK = TRUE;
                switch (iTabType)
                {
                case TAB_WEB:
                case TAB_TELEPHONE:
                case TAB_IRRIGATION:
                case 13:    // DVR from 4.0
                    bOK = FALSE;
                    break;
                }
#endif

                if (bOK)
                {
                    //DL - try to get nickName from respective controller descriptor (if found)
                    //[m_pNickName initWithCString:"" encoding:NSUTF8StringEncoding];
                    if(nil != m_pSysName)
                    {
                        m_pNickName = m_pSysName;
                    }
                    else
                    {
                        m_pNickName = @"";
                    }
                    DBManager *dbManager = [DBManager getSharedInstance];
                    ControllerDescriptor *cd = [[ControllerDescriptor alloc] init];
                    cd.systemName = m_pSysName;
                    if([dbManager getControllerByName:cd])
                    {
                        m_pNickName = cd.nickName;
                    }
                    [cd release];

                    
                    CMainTab *pTab = [[CMainTab alloc] initWithName:pTabName TabID:iTabType SystemName:m_pNickName];
                    [pTab SetMainController:(UIViewController *)self];
                    [m_pMainTabs addObject:pTab];
                    [pTab release];
                    if (pHomeTab == NULL)
                        pHomeTab = pTab;
                }
            }
        }
        [pQ2 release];

        CNSQuery *pQLock = [[[CNSQuery alloc] initWithQ:HLM_TABLETLOCKOUTQ] autorelease];
        [pQLock SendMessageWithMyID:self];

        CNSQuery *pQVersion = [[[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETVERSIONQ] autorelease];
        [pQVersion SendMessageWithMyID:self];
        
        [self LoadSystemToolBarForAnimation:NO];
        [pHomeTab LoadConfig];
        //DL - brings up the upper bar
        [self SetActiveMainTab:pHomeTab CursorDirection:-1];

        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDLIBRARYCLIENTQ];
        [pQ autorelease];
        [pQ SendMessage];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_TABLETLOCKOUTA:
        {
            int iLock = [pMSG GetInt];
            if (iLock)
            {
                if (m_pLockView == NULL)
                {
                        m_pLockView = [[UIAlertView alloc]      initWithTitle:@"HomeLogic" 
                                                                message:@"Connection Not Supported by Lock Configuration" 
                                                                delegate:NULL
                                                                cancelButtonTitle:@"Cancel" otherButtonTitles:NULL];
                }
                [m_pLockView show];

                [CHLComm Disconnect];
            }
            else
            {
                if (m_pLockView != NULL)
                {
                    [m_pLockView dismissWithClickedButtonIndex:0 animated:TRUE];
                    [m_pLockView release];
                    m_pLockView = NULL;
                }
            }
        }
        break;

    case HLM_SYSCONFIG_GETVERSIONA:
        {
            [pMSG GetString];
            [pMSG GetInt];
            int iVer1 = [pMSG GetInt];
            int iVer2 = [pMSG GetInt];

            [CHLComm SetVersion:iVer1 Sub:iVer2];
        }
    }
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
}
/*============================================================================*/

-(void)CheckConnectTimer

/*============================================================================*/
{
    if (![CHLComm IsInit])
        return;

    if (m_bInit)
    {
        [CHLComm AddSocketSink:self];
        m_bInit = FALSE;
    }

    if (m_pConnectPage == NULL)
    {
        if (![CHLComm Connected] && ![CHLComm IsConnecting])
        {
            [self ShowConnectPage];
        }
    }
    else
    {
        if ([CHLComm Connected] && ![CHLComm IsConnecting])
        {
            [self HideConnectPage];
        }
    }
}

-(void)ShowConnectPage2
{
    //DL make sure Connection page will go into Remote mode
    NSUserDefaults *pDefaults = [NSUserDefaults standardUserDefaults];
    [pDefaults setObject:@"REMOTE" forKey:@"HLMode"];
    
    [CHLComm Disconnect];
    [CHLComm RemoveAllSocketSinks];

    //this refreshes pages right but sticks with Portrait orientation
    
    //dispatch_async(dispatch_get_main_queue(),^{
    //    /*[self performSegueWithIdentifier: @"firstRun" sender: self];*/
    //    [self viewWillAppear: true];
    //});
    
    //[self viewWillAppear: true];
    //check orientation and xib before call?
    //[self viewDidLoad];
    
    
    //[super viewDidLoad];
    
    CIPADMainView *pMain = [[CIPADMainView alloc]init];
    
    [self setView:pMain];
    [pMain SetController:self];
    
    m_bInit = TRUE;
    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(CheckConnectTimer) userInfo:nil repeats:YES] retain];
    CGRect rBounds = self.view.bounds;
    CGRect rToolBar = [CRect BreakOffTop:&rBounds DY:[CMainView DY_TOOLBAR]];

    //m_pToolBar = NULL;
    //DL - new stuff comment out and see... this makes it show up the right weather but old stays either
    m_pToolBar = [[CHLToolBar alloc] initWithFrame:rToolBar Orientation:TOOLBAR_TOP];
    
    

    [self.view addSubview:m_pToolBar];
    [m_pToolBar release];
    
    [pMain SetToolBar:m_pToolBar];

    //////////////////////////////////////////////////////////////////
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    if(ver_float >= 6.0)
    {
        [self ShowConnectPage];
    }

}
/*============================================================================*/

- (void)ShowConnectPage

/*============================================================================*/
{
    if (m_pConnectPage != NULL)
        return;

//    [[self navigationController] popToRootViewControllerAnimated:NO];

    NSString *psNIB = NULL;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIDeviceOrientation interfaceOrient = self.interfaceOrientation;
        
        UIDeviceOrientation devOrient = [[UIDevice currentDevice] orientation];
        
        //NSLog(@"interfaceOrient = %d,  devOrient = %d", interfaceOrient, devOrient);
        
        psNIB = @"tconnect-iPad";
        switch (devOrient)
        {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            psNIB = @"tconnect-iPad_ls";
            break;

        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            psNIB = @"tconnect-iPad";
            break;
        }
        //m_pConnectPage = [[CConnectPage alloc] initWithNibName:psNIB bundle:nil Orientation:devOrient];
    }
    else
    {
        psNIB = @"tconnect";
        switch (self.interfaceOrientation)
        {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            psNIB = @"tconnect_ls";
            break;

        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            break;
        }
        //m_pConnectPage = [[CConnectPage alloc] initWithNibName:psNIB bundle:nil Orientation:self.interfaceOrientation];
    }
    
    //m_pConnectPage = [[CConnectPage alloc] initWithNibName:psNIB bundle:nil Orientation:self.interfaceOrientation];
    m_pConnectPage = [[CConnectPage alloc] initWithNibName:psNIB bundle:nil Orientation:[[UIDevice currentDevice] orientation]];

    m_pConnectPage.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];

    UIView *pView = self.navigationController.topViewController.view;

    /*
    switch([[UIDevice currentDevice] orientation])//[[UIApplication sharedApplication] statusBarOrientation])
    {
        case UIInterfaceOrientationUnknown:
            NSLog(@"*** UNKNOWN");
            break;
        case UIInterfaceOrientationLandscapeLeft:
            NSLog(@"*** LandLeft");
            break;
        case UIInterfaceOrientationLandscapeRight:
            NSLog(@"*** LandRight");
            //[self.navigationController.view setTransform:CGAffineTransformMakeRotation(M_PI)];
            break;
        case UIInterfaceOrientationPortrait:
            NSLog(@"*** Portrait");
            //[self.navigationController.view setTransform:CGAffineTransformMakeRotation(0)];
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            NSLog(@"*** PortraitUpsidedown");
            //[self.navigationController.view setTransform:CGAffineTransformMakeRotation(M_PI)];
            break;
            
    }
    */



	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];

    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:pView cache:NO];
    [pView addSubview:m_pConnectPage.view];
    [m_pConnectPage.view setAlpha:1.0];

	[UIView commitAnimations];
 
}
/*============================================================================*/

- (void)HideConnectPage

/*============================================================================*/
{
    if (m_pConnectPage == NULL)
        return;
    
    [m_pConnectPage ReleaseAll];

	// present page six as a modal child or overlay view
    UIView *pView = self.navigationController.topViewController.view;

	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:pView cache:NO];
    [m_pConnectPage.view removeFromSuperview];
	[UIView commitAnimations];
	[[self navigationController] dismissModalViewControllerAnimated:YES];
    
    [m_pConnectPage release];
    m_pConnectPage = nil;
    
}
/*============================================================================*/

-(void)SetBackgroundImage:(int)iImage

/*============================================================================*/
{
    [CAppDelegate BackgroundStyle:iImage];


    if (m_pBackground != NULL)
    {
        [m_pBackground removeFromSuperview];
        [m_pBackground release];
        m_pBackground = NULL;
    }

    UIImage *pImage = NULL;

    switch (iImage)
    {
    case 1000: pImage = [UIImage imageNamed:@"BACKGROUND1.jpg"]; break;
    case 2000: pImage = [UIImage imageNamed:@"BACKGROUND2.png"]; break;
    case 3000:
    case 4000: 
        pImage = [UIImage imageNamed:@"IPAD_GRADIENT1.png"]; break;
    }

    UIViewController *pParent = self.parentViewController;

    if (pImage == NULL)
    {
        [pParent.view setBackgroundColor:[CHLComm GetRGB:RGB_APP_BACKGROUND]];
        return;
    }

    m_pBackground = [[UIImageView alloc ]initWithImage:pImage];
    [pParent.view addSubview:m_pBackground];
    [pParent.view sendSubviewToBack:m_pBackground];

    CGRect rMain = [UIScreen mainScreen].bounds;


    [m_pBackground setFrame:rMain];
    [m_pBackground release];
}
/*============================================================================*/

-(void)LoadSystemToolBarForAnimation:(BOOL)bAnimate

/*============================================================================*/
{
    CGRect rFrame = m_pToolBar.bounds;
    CSystemToolBarClient *pToolBarClient = [[CSystemToolBarClient alloc] initWithFrame:rFrame MainTabs:m_pMainTabs ViewController:self SelectedIndex:m_iMainTabIndex];
    int iType = TOOLBAR_NONE;
    if (bAnimate)
        iType = TOOLBAR_TOSYSMENU;

    [m_pToolBar SetClientView:pToolBarClient AnimationType:iType];
}
/*============================================================================*/

-(void)SetActiveMainTab:(CMainTab*)pMainTab CursorDirection:(int)iCursorDirection

/*============================================================================*/
{
    printf("SET ACTIVE MAIN TAB BEGINS\n");
    
    for (int i = 0; i < [m_pMainTabs count]; i++)
    {
        CMainTab *pTab = (CMainTab*)[m_pMainTabs objectAtIndex:i];
        if (pTab == pMainTab)
            m_iMainTabIndex = i;
    }

    CGRect rNewPage = self.view.bounds;
    

    if ([pMainTab NSubTabs] <= 1)
    {
        [CRect BreakOffTop:&rNewPage DY:[CMainView DY_TOOLBAR]];
        rNewPage.origin.y += [CMainView DY_TOOLBAR_LARGE]-[CMainView DY_TOOLBAR];
    }
    else
    {
        [CRect BreakOffTop:&rNewPage DY:[CMainView DY_TOOLBAR_LARGE]];
    }

    // Delete old page if any
    if (_m_pContentPage != NULL)
    {
        [_m_pContentPage removeFromSuperview];
        _m_pContentPage = NULL;
    }


    _m_pContentPage = m_pContentPage;
    m_pContentPage = NULL;

    CIPADMainView *pMain = (CIPADMainView*)self.view;
    [pMain SetContent:NULL];


    if (iCursorDirection == CURSOR_LEFTTORIGHT)
        rNewPage.origin.x += self.view.bounds.size.width;
    else 
        rNewPage.origin.x -= self.view.bounds.size.width;

    if ([pMainTab NSubTabs] == 1)
    {
        CSubSystemPage *pPage = [pMainTab CreateUserPage:rNewPage SubTab:[pMainTab SubTab:0] MainTabPage:NULL];
        [pPage UIInit];
        [pPage SetVisible:TRUE];
        m_pContentPage = pPage;
        [pMain SetContent:m_pContentPage];
        [self.view addSubview:m_pContentPage];
        [self SetFlipTimer:3000];
    }
    else 
    {
        CZoneSelector *pPage = [[CZoneSelector alloc] initWithFrame:rNewPage MainTab:pMainTab SubPageIndex:-1 ViewController:self];
        m_pContentPage = pPage;
        [pMain SetContent:m_pContentPage];
        [self.view addSubview:m_pContentPage];
    }


    [m_pContentPage release];

    @try
    {
        [m_pContentPage.layer setShouldRasterize:TRUE];
        [_m_pContentPage.layer setShouldRasterize:TRUE];
    }
    @catch (NSException *ex)
    {
    }

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];

    CGRect rEnd = self.view.bounds;
    if ([m_pContentPage WantSystemMenu])
    {
        [CRect BreakOffTop:&rEnd DY:[CMainView DY_TOOLBAR_LARGE]];
    }
    else
    {
        [CRect BreakOffTop:&rEnd DY:[CMainView DY_TOOLBAR]];
        rEnd.origin.y += [CMainView DY_TOOLBAR_LARGE]-[CMainView DY_TOOLBAR];
    }

    [m_pContentPage setFrame:rEnd];

    if (_m_pContentPage != NULL)
    {
        rEnd = _m_pContentPage.frame;
        if (iCursorDirection == CURSOR_LEFTTORIGHT)
            rEnd.origin.x -= self.view.bounds.size.width;
        else 
            rEnd.origin.x += self.view.bounds.size.width;
        
        [_m_pContentPage setFrame:rEnd];
    }

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];

    printf("SET ACTIVE MAIN TAB ENDS\n");
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    CSubTab *pSub = [_m_pContentPage SubTab];
    NSString *psName = pSub.m_pName;
    printf("Releasing Page 0x%08X %s\n", (unsigned int)_m_pContentPage, [psName UTF8String]);


    @try
    {
        [m_pContentPage.layer setShouldRasterize:NO];
    }
    @catch (NSException *ex)
    {
    }


    [_m_pContentPage removeFromSuperview];
    _m_pContentPage = NULL;
    
    [_m_pOverlay removeFromSuperview];
    _m_pOverlay = NULL;

    [m_pToolBar AnimationComplete];

    printf("Content Page retain Count = %d Should be 1\n", [m_pContentPage retainCount]);
}
/*============================================================================*/

-(void)SetFlipTimer:(int)iMSEC

/*============================================================================*/
{
    [m_pFlipTimer invalidate];
    [m_pFlipTimer release];
    double dSec = iMSEC / 1000.0;
    m_pFlipTimer = [[NSTimer scheduledTimerWithTimeInterval:dSec target:self selector:@selector(RestoreSubSysMenu) userInfo:nil repeats:NO] retain];
}
/*============================================================================*/

-(void)CancelFlipTimer

/*============================================================================*/
{
    [m_pFlipTimer invalidate];
    [m_pFlipTimer release];
    m_pFlipTimer = NULL;
}
/*============================================================================*/

-(void)RestoreSubSysMenu

/*============================================================================*/
{
    CGRect rFrame = m_pToolBar.bounds;
    
    CToolBarClientView *pView = [m_pContentPage CreateMenuView:rFrame ViewController:self Flags:[self GetMenuFlags]];

    if (pView == NULL)
        return;
    //DL - set main view controller for the Fish button (1)
    [((CSubSystemHeaderBarView*)pView) SetMainViewController:self];
    
    [m_pToolBar SetClientView:pView AnimationType:TOOLBAR_FROMSYSMENU];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    CIPADMainView *pMain = (CIPADMainView*)self.view;
    [pMain DoLayout];
    [m_pToolBar CommitAnimations];
    
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(FlipComplete:finished:context:)];
    [UIView commitAnimations];


    [pView release];
}
/*============================================================================*/

-(void)ShowMainMenu:(id)sender

/*============================================================================*/
{
    //DL - iPad - Fish button has been pushed
    if ([m_pContentPage retainCount] != 1)
    {
        printf("Content Page retain Count = %d Should be 1\n", [m_pContentPage retainCount]);
    }
    
#ifdef PENTAIR
    if ([m_pMainTabs count] == 2)
    {
        int iNewIndex = 0;;
        if (m_iMainTabIndex == 0)
            iNewIndex = 1;
        
        [self ShiftMainTabTo:iNewIndex];
        return;
    }
#endif

    [self LoadSystemToolBarForAnimation:YES];
    CIPADMainView *pMain = (CIPADMainView*)self.view;

    CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:m_iMainTabIndex];

    if ([pMainTab NSubTabs] <= 1)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        [pMain DoLayout];


        [m_pToolBar CommitAnimations];
        
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(FlipComplete:finished:context:)];
        [UIView commitAnimations];

        // Single SubSystem Page just flip, set timer, and done
        [self SetFlipTimer:3000];
        return;
    }

    // Need to show zone page
    [m_pContentPage PrepareLoseModal];
    CGRect rZonePage   = self.view.bounds;
    CGRect rSrcPage    = self.view.bounds;

    [CRect BreakOffTop:&rZonePage DY:[CMainView DY_TOOLBAR_LARGE]];
    [CRect BreakOffTop:&rSrcPage  DY:[CMainView DY_TOOLBAR]];

    int iSubPageIndex = [pMainTab IndexOf:[m_pContentPage SubTab]];
    CZoneSelector *pZonePage = [[CZoneSelector alloc] initWithFrame:rZonePage MainTab:pMainTab SubPageIndex:iSubPageIndex ViewController:self];
    [self.view insertSubview:pZonePage belowSubview: m_pContentPage];
    [pZonePage release];

    // Force layout
    [pZonePage layoutIfNeeded];
    [pZonePage setAlpha:0];
    
    CSubTabView *pView = [pZonePage SubTabView:iSubPageIndex];
    [m_pContentPage SetSubTabView:pView];

    CGPoint pt = CGPointMake(0, 0);
    pt = [self.view convertPoint:pt fromView:pView];
    CGRect rPage = [pView SubSystemRect];
    rPage.origin.x += pt.x;
    rPage.origin.y += pt.y;

    CGRect rOverlay = [pView OverlayRect];
    rOverlay.origin.x += pt.x;
    rOverlay.origin.y += pt.y;

    CGAffineTransform Shift = CGAffineTransformMakeTranslation(0, [CMainView DY_TOOLBAR]-[CMainView DY_TOOLBAR_LARGE]);
    [m_pContentPage setTransform:Shift];

    CSubTabViewOverlay *pOverlay = NULL;
    if ([m_pContentPage WantOverviewOverlay])
    {
        pOverlay = [pView CreateOverlay];
        [m_pContentPage InitSubTabViewOverlay];

        [pOverlay retain];
        [pOverlay removeFromSuperview];
        [self.view addSubview:pOverlay];
        [pOverlay release];
        [pOverlay setAlpha:0];

        [pOverlay setFrame:rOverlay];

        double dScaleX = rSrcPage.size.width / rOverlay.size.width;
        double dScaleY = rSrcPage.size.height / rOverlay.size.height;
        CGAffineTransform MyScaler2 = CGAffineTransformMakeScale(dScaleX, dScaleY);
        CGAffineTransform MyTrans2  = CGAffineTransformMakeTranslation([CRect XMid:rSrcPage]-[CRect XMid:rOverlay], [CRect YMid:rSrcPage]-[CRect YMid:rOverlay]);
        CGAffineTransform MyConcat2 = CGAffineTransformConcat(MyScaler2, MyTrans2);
        [pOverlay setTransform:MyConcat2];
    }

    [pZonePage SetReflectionAlpha:0];

    if ([m_pConnectPage retainCount] != 1)
    {
        printf("Content Page retain Count = %d Should be 1\n", [m_pContentPage retainCount]);
    }
    [m_pZoomDelegate release];
    m_pZoomDelegate = NULL;
    m_pZoomDelegate = [[CZonePageZoomDelegate alloc] initWithSubTabView:pView SubSystemPage:m_pContentPage ZoneSelector:pZonePage ToolBar:m_pToolBar];

    CGAffineTransform ZoomOut = CGAffineTransformMakeScale(0.5, 0.5);
    [pZonePage setTransform:ZoomOut];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    [m_pContentPage PrepareScaleView:FALSE];
    [pZonePage setAlpha:1.0];

    double dScaleX = rPage.size.width / rSrcPage.size.width;
    double dScaleY = rPage.size.height / rSrcPage.size.height;
    CGAffineTransform MyScaler = CGAffineTransformMakeScale(dScaleX, dScaleY);
    CGAffineTransform MyTrans  = CGAffineTransformMakeTranslation([CRect XMid:rPage]-[CRect XMid:rSrcPage], [CRect YMid:rPage]-[CRect YMid:rSrcPage]+rSrcPage.origin.y-rZonePage.origin.y);
    CGAffineTransform MyConcat = CGAffineTransformConcat(MyScaler, MyTrans);
    [m_pContentPage setTransform:MyConcat];

    if (pOverlay != NULL)
    {
        [pOverlay setTransform:CGAffineTransformIdentity];
        [pOverlay setAlpha:1.0];
        [m_pContentPage setAlpha:0];
    }

    CGAffineTransform ZoomIdent = CGAffineTransformIdentity;
    [pZonePage setTransform:ZoomIdent];

    [UIView setAnimationDelegate:m_pZoomDelegate];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [m_pToolBar CommitAnimations];

//    [m_pContentPage.layer setShouldRasterize:TRUE];
//    [_m_pContentPage.layer setShouldRasterize:TRUE];

    m_pContentPage = pZonePage;
    [pMain SetContent:pZonePage];

    [pMain DoLayout];

    [UIView commitAnimations];

}
/*============================================================================*/

-(void)ZoomToSubPageView:(id)sender

/*============================================================================*/
{
    [self CancelFlipTimer];

    CGRect rEnd = self.view.bounds;
    CGRect rStart = self.view.bounds;

    [CRect BreakOffTop:&rStart DY:[CMainView DY_TOOLBAR]];
    [CRect BreakOffTop:&rEnd DY:[CMainView DY_TOOLBAR]];

    rStart.origin.y += [CMainView DY_TOOLBAR_LARGE];
    rStart.origin.y -= [CMainView DY_TOOLBAR];
    

    CSubTabView *pView = (CSubTabView*)sender;
    CGPoint pt = CGPointMake(0, 0);
    pt = [self.view convertPoint:pt fromView:pView];
    CSubSystemPage *pPage = [pView SubSystemPage];
    [pPage setHidden:NO];
    [pPage UIInit];
    
    CGRect rPage = [pView SubSystemRect];
    rPage.origin.x += pt.x;
    rPage.origin.y += pt.y;

    CGRect rOverlay = [pView OverlayRect];
    rOverlay.origin.x += pt.x;
    rOverlay.origin.y += pt.y;

    int iRetainCount = [pPage retainCount];
    printf("ZOOM TO SUBPAGE RETAIN1 = %d\n", iRetainCount);

    [pPage removeFromSuperview];

    iRetainCount = [pPage retainCount];
    printf("ZOOM TO SUBPAGE RETAIN2 = %d\n", iRetainCount);

    [pPage setTransform:CGAffineTransformIdentity];
    [self.view addSubview:pPage];
    [pView DetachSystemPage];
    

    iRetainCount = [pPage retainCount];
    printf("ZOOM TO SUBPAGE RETAIN3 = %d\n", iRetainCount);

    CIPADMainView *pMain = (CIPADMainView*)self.view;
    if (_m_pContentPage != NULL)
    {
        [m_pContentPage release];
        m_pContentPage = NULL;
        [pMain SetContent:NULL];
    }

    _m_pContentPage = m_pContentPage;
    m_pContentPage = pPage;
    [pMain SetContent:m_pContentPage];

    [pPage setFrame:rEnd];
//    [pPage setCenter:CGPointMake([CRect XMid:rEnd], [CRect YMid:rEnd])];

    _m_pOverlay = [pPage Overlay];
    if (_m_pOverlay != NULL)
    {
        [_m_pOverlay retain];
        [_m_pOverlay removeFromSuperview];
        [self.view addSubview:_m_pOverlay];
        [_m_pOverlay release];

        [_m_pOverlay setFrame:rOverlay];
    }


    int iDYShift = [CMainView DY_TOOLBAR_LARGE]-[CMainView DY_TOOLBAR];
    double dScaleX = rPage.size.width / rStart.size.width;
    double dScaleY = rPage.size.height / rStart.size.height;
    CGAffineTransform MyScaler = CGAffineTransformMakeScale(dScaleX, dScaleY);
    CGAffineTransform MyTrans  = CGAffineTransformMakeTranslation([CRect XMid:rPage]-[CRect XMid:rStart], [CRect YMid:rPage]-[CRect YMid:rStart]+iDYShift);
    CGAffineTransform MyConcat = CGAffineTransformConcat(MyScaler, MyTrans);
    [pPage setTransform:MyConcat];

    [pPage PrepareForZoom:TRUE];
    

    CGRect rFrame = m_pToolBar.bounds;
    CToolBarClientView *pToolBarView = [m_pContentPage CreateMenuView:rFrame ViewController:self Flags:[self GetMenuFlags]];

    if (pToolBarView == NULL)
        return;

    [m_pToolBar SetClientView:pToolBarView AnimationType:TOOLBAR_FROMSYSMENU];
    [pToolBarView release];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    [pPage PrepareScaleView:TRUE];
    [pPage SetVisible:YES];

    [_m_pContentPage setAlpha:0.0];
    CGAffineTransform ZoomOut = CGAffineTransformMakeScale(0.5, 0.5);
    [_m_pContentPage setTransform:ZoomOut];

    if (_m_pOverlay != NULL)
    {
        double dScaleX = rEnd.size.width / rOverlay.size.width;
        double dScaleY = rEnd.size.height / rOverlay.size.height;
        CGAffineTransform MyScaler2 = CGAffineTransformMakeScale(dScaleX, dScaleY);
        CGAffineTransform MyTrans2  = CGAffineTransformMakeTranslation([CRect XMid:rEnd]-[CRect XMid:rOverlay], [CRect YMid:rEnd]-[CRect YMid:rOverlay]);
        CGAffineTransform MyConcat2 = CGAffineTransformConcat(MyScaler2, MyTrans2);
        [_m_pOverlay setTransform:MyConcat2];
    }
    [m_pContentPage setTransform:CGAffineTransformIdentity];
    [[pPage Overlay] setAlpha:0.0];
    [pPage setAlpha:1.0];
    [m_pToolBar CommitAnimations];

    @try
    {
        [m_pContentPage.layer setShouldRasterize:TRUE];
        [_m_pContentPage.layer setShouldRasterize:TRUE];
    }
    @catch (NSException *ex)
    {
    }


    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];
    
    [pPage setUserInteractionEnabled:YES];
    
}
/*============================================================================*/

-(void)PageLeft:(id)sender

/*============================================================================*/
{
    [self ShiftPage:-1];
}
/*============================================================================*/

-(void)PageRight:(id)sender

/*============================================================================*/
{
    [self ShiftPage:+1];
}
/*============================================================================*/

-(void)ShiftPage:(int)iDir

/*============================================================================*/
{
    if (m_iMainTabIndex < 0 || m_iMainTabIndex >= [m_pMainTabs count])
        return;
    CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:m_iMainTabIndex];
    int iSubTabIndex = [pMainTab IndexOf:[m_pContentPage SubTab]];
    int iNewIndex = iSubTabIndex + iDir;
    CSubTab *pSubTab = [pMainTab SubTab:iNewIndex];

    CIPADMainView *pMain = (CIPADMainView*)self.view;
    
    if (_m_pContentPage != NULL)
    {
        [m_pContentPage removeFromSuperview];
        m_pContentPage = NULL;
        [pMain SetContent:NULL];
    }
    
    CGRect rNewPage = self.view.bounds;
    
    [CRect BreakOffTop:&rNewPage DY:m_pToolBar.bounds.size.height];
    rNewPage.origin.x += (rNewPage.size.width * iDir);

    CSubSystemPage *pNewPage = [pMainTab CreateUserPage:rNewPage SubTab:pSubTab MainTabPage:NULL];
    [self.view addSubview:pNewPage];
    
    _m_pContentPage = m_pContentPage;
    m_pContentPage = pNewPage;
    [pMain SetContent:m_pContentPage];
    [m_pContentPage release];
    [pNewPage UIInit];
    [pNewPage SetVisible:TRUE];
    [pNewPage setAlpha:0];

    if (pNewPage != NULL)
    {
        @try
        {
            [pNewPage.layer setShouldRasterize:YES];
        }
        @catch (NSException *ex)
        {
        }
    }


    if (_m_pContentPage != NULL)
    {
        @try
        {
            [_m_pContentPage.layer setShouldRasterize:YES];
        }
        @catch (NSException *ex)
        {
        }
    }

    CGRect rFrame = m_pToolBar.bounds;
    CToolBarClientView *pToolBarView = [m_pContentPage CreateMenuView:rFrame ViewController:self Flags:[self GetMenuFlags]];

    if (pToolBarView == NULL)
        return;

    int iType = TOOLBAR_LEFTTORIGHT;
    if (iNewIndex > iSubTabIndex)
        iType = TOOLBAR_RIGHTTOLEFT;

    [m_pToolBar SetClientView:pToolBarView AnimationType:iType];
    [pToolBarView release];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    rNewPage.origin.x -= (rNewPage.size.width * iDir);
    [pNewPage setFrame:rNewPage];
    [pNewPage setAlpha:1.0];

    if (_m_pContentPage != NULL)
    {
        CGRect rPrev = _m_pContentPage.frame;
        rPrev.origin.x -= (rNewPage.size.width * iDir)/2;
        [_m_pContentPage setFrame:rPrev];
        [_m_pContentPage setAlpha:0];
    }

    [m_pToolBar CommitAnimations];

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)ShiftMainTabTo:(int)iNew

/*============================================================================*/
{
    if (m_iMainTabIndex < 0 || m_iMainTabIndex >= [m_pMainTabs count])
        return;

    int iDir = -1;
    if (iNew > m_iMainTabIndex)
        iDir = 1;

    CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iNew];
    [pMainTab LoadConfig];
    CSubTab *pSubTab = [pMainTab SubTab:0];

    CIPADMainView *pMain = (CIPADMainView*)self.view;
    
    if (_m_pContentPage != NULL)
    {
        [_m_pContentPage removeFromSuperview];
        _m_pContentPage = NULL;
        [pMain SetContent:NULL];
    }
    
    CGRect rNewPage = self.view.bounds;
    [CRect BreakOffTop:&rNewPage DY:m_pToolBar.bounds.size.height];
    rNewPage.origin.x += (rNewPage.size.width * iDir);

    CSubSystemPage *pNewPage = [pMainTab CreateUserPage:rNewPage SubTab:pSubTab MainTabPage:NULL];
    [self.view addSubview:pNewPage];

    _m_pContentPage = m_pContentPage;
    m_pContentPage = pNewPage;
    [pMain SetContent:m_pContentPage];
    [m_pContentPage release];
    [pNewPage UIInit];
    [pNewPage SetVisible:TRUE];
    [pNewPage setAlpha:0];

    CGRect rFrame = m_pToolBar.bounds;
    m_iMainTabIndex = iNew;
    CToolBarClientView *pToolBarView = [m_pContentPage CreateMenuView:rFrame ViewController:self Flags:[self GetMenuFlags]];
 
    if (pToolBarView == NULL)
        return;
    //DL - set main view controller for the Fish button (2)
    [((CSubSystemHeaderBarView*)pToolBarView) SetMainViewController:self];
    
    int iAnimationType = TOOLBAR_LEFTTORIGHT;
    if (iDir == 1)
        iAnimationType = TOOLBAR_RIGHTTOLEFT;

    [m_pToolBar SetClientView:pToolBarView AnimationType:iAnimationType];
    [pToolBarView release];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    rNewPage.origin.x -= (rNewPage.size.width * iDir);
    [pNewPage setFrame:rNewPage];
    [pNewPage setAlpha:1.0];

    if (_m_pContentPage != NULL)
    {
        CGRect rPrev = _m_pContentPage.frame;
        rPrev.origin.x -= (rNewPage.size.width * iDir)/2;
        [_m_pContentPage setFrame:rPrev];
        [_m_pContentPage setAlpha:0];
    }

    [m_pToolBar CommitAnimations];

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];
}
/*============================================================================*/

+(IPAD_ViewController*)MainViewController

/*============================================================================*/
{
    return g_pController;
}
/*============================================================================*/

-(int)GetMenuFlags

/*============================================================================*/
{
    int iFlags = 0;
    if (m_iMainTabIndex >= 0 && m_iMainTabIndex < [m_pMainTabs count])
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:m_iMainTabIndex];
        int iSubTabIndex = [pMainTab IndexOf:[m_pContentPage SubTab]];

        if (iSubTabIndex > 0)
            iFlags |= SUBTAB_HASTABSLEFT;
        if (iSubTabIndex < ([pMainTab NSubTabs]-1))
            iFlags |= SUBTAB_HASTABSRIGHT;
    }
    return iFlags;
}
/*============================================================================*/

-(int)Layout

/*============================================================================*/
{
    CToolBarClientView *pToolBarClient = [m_pToolBar ClientView];
    CGSize size = self.view.bounds.size;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if ([m_pContentPage WantSystemMenu])
            return LAYOUT_LARGESYSBAR_TOP;
        if ([pToolBarClient IsSystemMenu])
            return LAYOUT_LARGESYSBAR_TOP_OFFSET;

        return LAYOUT_DEFAULT;
    }


    if (size.width > size.height)
    {
        if ([m_pContentPage WantSystemMenu])
            return LAYOUT_LARGESYSBAR_TOP;
        if ([pToolBarClient IsSystemMenu])
            return LAYOUT_LARGESYSBAR_TOP_OFFSET;
    }
    else
    {
        if ([m_pContentPage WantSystemMenu])
            return LAYOUT_SIDESYSBAR;
        if ([pToolBarClient IsSystemMenu])
            return LAYOUT_SIDESYSBAR_OFFSET;
    }
       

    return LAYOUT_DEFAULT;
}
/*============================================================================*/

-(void)FlipComplete:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [m_pToolBar AnimationComplete];
}
/*============================================================================*/

-(void)HideNavBar:(BOOL)bHide

/*============================================================================*/
{
    m_bHideNavBar = bHide;
    CIPADMainView *pMain = (CIPADMainView*)self.view;
    [pMain DoLayout];
}
/*============================================================================*/

-(BOOL)HideNavBar

/*============================================================================*/
{
    return m_bHideNavBar;
}
@end


