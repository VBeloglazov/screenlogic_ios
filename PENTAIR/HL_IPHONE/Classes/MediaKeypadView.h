//
//  MediaKeypadView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NSSocketSink.h"
#import "CustomPage.h"

/*============================================================================*/

@interface CMediaKeypadView : CCustomPage < CNSSocketSink >

/*============================================================================*/
{
    int                             m_iType;
    int                             m_iTextSize;
}

-(id)initWithFrame: (CGRect)rView ID:(int)iID ZoneID:(int)iZoneID Flags:(int)iFlags TextSize:(int)iTextSize Type:(int)iType;
-(void)InitComm;

@end
