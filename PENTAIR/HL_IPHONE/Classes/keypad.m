//
//  keypad.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "keypad.h"
#import "nsmsg.h"
#import "nsquery.h"
#import "hlcomm.h"
#import "hlm.h"
#import "hlbutton.h"
#import "shader.h"

#define CMD_MOM_DOWN_START              1000
#define CMD_MOM_DOWN_END                1999
#define CMD_MOM_UP_START                2000
#define CMD_MOM_UP_END                  2999
#define CMD_SCENES_START                3000
#define CMD_SCENES_END                  3999

@implementation CButtonDef
/*============================================================================*/

-(id)initWithName:(NSString*)pName Type:(int)iType X1:(int)iX1 Y1:(int)iY1 DX:(int)iDX DY:(int)iDY Face:(UIColor*)pFace Text:(UIColor*)pText

/*============================================================================*/
{
    m_pName     = pName;
    m_iType = iType;
    m_iX1       = iX1;
    m_iY1       = iY1;
    m_iDX       = iDX;
    m_iDY       = iDY;
    m_pRGBFace  = pFace;
    m_pRGBText  = pText;

    [m_pName retain];
    [m_pRGBFace retain];
    [m_pRGBText retain];

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [m_pRGBFace release];
    [m_pRGBText release];
    [super dealloc];
}
/*============================================================================*/

-(NSString*)Text            { return m_pName;       }
-(UIColor*)FaceColor        { return m_pRGBFace;    }
-(UIColor*)TextColor        { return m_pRGBText;    }
-(int)Type                  { return m_iType;   }
-(int)X1                    { return m_iX1;     }
-(int)Y1                    { return m_iY1;     }
-(int)DX                    { return m_iDX;     }
-(int)DY                    { return m_iDY;     }

/*============================================================================*/
@end

@implementation CLightingKeypad
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ID:(int)iID

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];

    if (self)
    {
        m_pButtons = [[NSMutableArray alloc] init];
        m_iID = iID;

        [CHLComm AddSocketSink:self];
        
        // Get Layout
        CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_GETKEYPADLAYOUTBYIDQ] autorelease];
        [pQ1 PutInt:m_iID];
        [pQ1 SendMessageWithMyID:self];

        // Add Client
        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_ADDCLIENTBYIDQ ] autorelease];
        [pQ2 PutInt:m_iID];
        [pQ2 PutInt:[CHLComm SenderID:self]];
        [pQ2 SendMessageWithMyID:self];

        // Get States
        CNSQuery *pQ3 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_GETSTATESBYIDQ] autorelease];
        [pQ3 PutInt:m_iID];
        [pQ3 SendMessageWithMyID:self];

        self.autoresizingMask = 0xFFFFFFFF;
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_REMOVECLIENTBYIDQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];


    [CHLComm RemoveSocketSink:self];
    [m_pButtons release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        // Add Client
        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_ADDCLIENTBYIDQ ] autorelease];
        [pQ2 PutInt:m_iID];
        [pQ2 PutInt:[CHLComm SenderID:self]];
        [pQ2 SendMessageWithMyID:self];

        // Get States
        CNSQuery *pQ3 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_GETSTATESBYIDQ] autorelease];
        [pQ3 PutInt:m_iID];
        [pQ3 SendMessageWithMyID:self];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_LIGHTING_GETKEYPADLAYOUTBYIDA:
        {
            [self LoadKeypad:pMSG];
        }
        break;

    case HLM_LIGHTING_KEYPADSCENECHANGE:
    case HLM_LIGHTING_KEYPAD_GETSTATESBYIDA:
        {
            int iNScenes = [pMSG GetInt];
            for (int i = 0; i < iNScenes; i++)
            {
                CHLButton *pBtn = (CHLButton*)[m_pButtons objectAtIndex:i];
                int iState = [pMSG GetInt];
                [pBtn SetChecked:iState != 0];
            }
        }
        break;
    }
}
/*========================================================================*/

-(void)OnCommand:(int)iCommandID

/*========================================================================*/
{
    if (iCommandID >= CMD_SCENES_START && iCommandID <= CMD_SCENES_END)
    {
        // Hit a scene button
        [self Execute:HLM_LIGHTING_EXECUTESCENEBYIDQ Index:(iCommandID-CMD_SCENES_START)];
    }
    else if (iCommandID >= CMD_MOM_DOWN_START && iCommandID <= CMD_MOM_DOWN_END)
    {
        [self Execute:HLM_LIGHTING_EXECUTESCENEBYIDMDQ Index:(iCommandID-CMD_MOM_DOWN_START)];
    }
    else if (iCommandID >= CMD_MOM_UP_START && iCommandID <= CMD_MOM_UP_END)
    {
        [self Execute:HLM_LIGHTING_EXECUTESCENEBYIDMUQ Index:(iCommandID-CMD_MOM_UP_START)];
    }
}
/*============================================================================*/

-(void)LoadKeypad:(CNSMSG*)pMSG

/*============================================================================*/
{

    for (int i = 0; i < [m_pButtons count]; i++)
    {
        UIView *pView = (UIView*)[m_pButtons objectAtIndex:i];
        [pView removeFromSuperview];
    }

    [m_pButtons removeAllObjects];
    int iX1B = [pMSG GetInt];
    int iX2B = [pMSG GetInt];
    int iY1B = [pMSG GetInt];
    int iY2B = [pMSG GetInt];
    int iNButtons = [pMSG GetInt];
    
    NSMutableArray *pBtnList = [[[NSMutableArray alloc] init] autorelease];

    // Load up basic buttons
    for (int i=0; i<iNButtons; i++)
    {
        [pMSG GetInt];  // Scene ID
        NSString *pText = [pMSG GetString];
        int iType       = [pMSG GetInt];
        int iX1         = [pMSG GetInt];
        int iY1         = [pMSG GetInt];
        int iDX         = [pMSG GetInt];
        int iDY         = [pMSG GetInt];

        [pMSG GetInt];  // Rad

        UIColor *pRGBFace   = [pMSG GetColor];
        UIColor *pRGBText   = [pMSG GetColor];
        CButtonDef *pDef = [[CButtonDef alloc] initWithName:pText Type:iType X1:iX1 Y1:iY1 DX:iDX DY:iDY Face:pRGBFace Text:pRGBText];
        [pBtnList addObject:pDef];
        [pDef release];


        if (i == 0)
        {
            iX1B = iX1;
            iX2B = iX1 + iDX;
            iY1B = iY1;
            iY2B = iY1 + iDY;
        }

        iX1B = MIN(iX1B, iX1);
        iX2B = MAX(iX2B, iX1+iDX);
        iY1B = MIN(iY1B, iY1);
        iY2B = MAX(iY2B, iY1+iDY);
    }

    //
    // Got all buttons, bound them
    //

    // Figure out the rect for the Round Region
    int iDXBtns = iX2B - iX1B;
    int iDYBtns = iY2B - iY1B;

    int iDXScrn = self.frame.size.width;
    int iDYScrn = self.frame.size.height;

    float fDXDYBtns = (float) iDXBtns / (float) iDYBtns;
    float fDXDYScrn = (float) iDXScrn / (float) iDYScrn;

    int iDXRect = 0;
    int iDYRect = 0;

    BOOL bAspectLock = TRUE;

    if (bAspectLock)
    {
        if (fDXDYBtns > fDXDYScrn)
        {
            // Button aspect is WIDER than screen, so limit by screen width
            iDXRect = iDXScrn;
            iDYRect = (int)((float)iDXRect / fDXDYBtns);
        }
        else
        {
            // Button aspect is TALLER than screen so limit by screen height
            iDYRect = iDYScrn;
            iDXRect = (int)((float)iDYRect * fDXDYBtns);
        }
    }
    else
    {
        iDXRect = iDXScrn;
        iDYRect = iDYScrn;
    }

    int iXUL = (iDXScrn-iDXRect)/2;
    int iYUL = (iDYScrn-iDYRect)/2;

    float fXScale = (float)100.0 / (float)(iX2B - iX1B);
    float fYScale = (float)100.0 / (float)(iY2B - iY1B);


    //
    // Make the real buttons
    //
    for (int iIndex = 0; iIndex < [pBtnList count]; iIndex++)
    {
        CButtonDef *pBtn = (CButtonDef*) [pBtnList objectAtIndex:iIndex];

        float fX1 = (float)(([pBtn X1] - iX1B) * fXScale / 100.0);
        float fY1 = (float)(([pBtn Y1] - iY1B) * fYScale / 100.0); 
        float fDX = (float)([pBtn DX] * fXScale / 100.0);
        float fDY = (float)([pBtn DY] * fYScale / 100.0);

        CGRect rBtn = CGRectMake((int)(fX1*iDXRect)+iXUL, (int)(fY1*iDYRect)+iYUL, (int)(fDX*iDXRect), (int)(fDY*iDYRect));


        int iIcon = AUDIO_ICON_TEXT;
        NSString *pText = [pBtn Text];
        
        int iLen = MIN(5, [pText length]);
        NSString *pSubString = [pText substringToIndex:iLen];
    
        if ([pSubString caseInsensitiveCompare:@"RAISE"] == NSOrderedSame)
            iIcon = AUDIO_ICON_UP;
        if ([pSubString caseInsensitiveCompare:@"LOWER"] == NSOrderedSame)
            iIcon = AUDIO_ICON_DOWN;

        if (iIcon != AUDIO_ICON_TEXT)
            pText = @"";

        UIColor *pRGBText = [pBtn TextColor];
        UIColor *pRGBFace = [pBtn FaceColor];
        
        CHLButton *pSceneBtn = [[CHLButton alloc] initWithFrame:rBtn IconFormat:HL_ICONCENTER Style:iIcon Text:pText TextSize:0 TextColor:pRGBText Color:pRGBFace Icon:@""];

#ifdef PENTAIR
        if (iIcon != AUDIO_ICON_TEXT)
        {
            [pSceneBtn IconFormat:HL_ICON75PCTHT];
        }
#endif

        if ([pBtn Type] == SCENETYPE_MOMENTARY)
        {
            int iDnCmd = CMD_MOM_DOWN_START + iIndex;
            int iUpCmd = CMD_MOM_UP_START + iIndex;
            [pSceneBtn SetCommandID:iDnCmd ReleaseID:iUpCmd Responder:self];
        }
        else
        {
            if ([pBtn Type] == SCENETYPE_SCENE_MOMENTARY)
            {
                int iDnCmd = CMD_MOM_DOWN_START + iIndex;
                int iUpCmd = CMD_MOM_UP_START + iIndex;
                [pSceneBtn SetCommandID:iDnCmd ReleaseID:iUpCmd Responder:self];
            }
            else
            {
                int iCmdID = CMD_SCENES_START + iIndex;
                [pSceneBtn SetCommandID:iCmdID Responder:self];
            }
        }

        pSceneBtn.autoresizingMask = 0xFFFFFFFF;
        [self addSubview:pSceneBtn];
        [m_pButtons addObject:pSceneBtn];
    }
}
/*============================================================================*/

-(void)Execute:(int)iCmd Index:(int)iIndex

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:iCmd] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iIndex];
    [pQ SendMessage];

    // Get States
    CNSQuery *pQStates = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_GETSTATESBYIDQ] autorelease];
    [pQStates PutInt:m_iID];
    [pQStates SendMessageWithMyID:self];
}

@end
