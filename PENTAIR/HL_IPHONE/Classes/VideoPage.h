//
//  VideoPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "SubSystemPage.h"
#import "hlview.h"
#import "sink.h"

@class CSubTab;
@class CVideoStreamView;
@class CCustomPage;
@class CHLRadio;
@class CHLButton;

/*============================================================================*/

@interface CVideoPage : CSubSystemPage < CHLCommandResponder, CSink >

/*============================================================================*/
{
    CCustomPage                         *m_pCustomPage;
    CVideoStreamView                    *m_pVideoStream;

    CHLRadio                            *m_pResControl;
    CHLRadio                            *m_pPresetControl;
    CHLButton                           *m_pZoomIn;
    CHLButton                           *m_pZoomOut;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)Init:(CSubTab*)pSubTab;
-(void)InitSubView:(UIView*)pSubView;
-(void)AddVideoControls;


@end
