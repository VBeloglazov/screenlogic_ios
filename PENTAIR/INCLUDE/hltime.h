/*
--------------------------------------------------------------------------------

    HLTIME.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

These classes can be used to get the time and time events in a program.


Notes:

    CHLTimes uses GetTickCount(), which wraps after 49 days.  The class can
    handle one wrap: use CHLTime for events that would last longer. Units for
    the timer class are in tick counts which are supposed to be about a
    millisecond.

    CHLTime uses the OS system time functions, which do not record the number
    of milliseconds accurately: depending on the system the millisecond value
    will increment in steps of about 15 milliseconds. Use CHLTimer to time
    events where precise millisecond tracking is needed.

    !***! CHLTime can be extended to accurately get the milliseconds.  Add a
    CHLTimer member, and then hook in an idle function to periodically reset
    the HLTimer around midnight.

--------------------------------------------------------------------------------
*/
#ifndef HLTIME_H
#define HLTIME_H

#include "hlapi.h"
#include "hlmsgs.h"

class CHLTimeSpan;
class CHLObList;

#ifdef MAC_OS
    #define SYSTEMTIME          time_t
#endif


    //
    // Global functions for text.
    //
#ifdef STRINGLIB
    extern CHLString            DayOfWeekText(int iDay);
    extern CHLString            DayOfWeekTextShort(int iDay );
    extern CHLString            MonthText(int iMonth );
    extern CHLString            MonthTextShort(int iMonth);
#endif
    extern CHLString            DayTimeText(int iMinutesAfterMidnight );
    extern CHLString            DayTimeTextSmall(int iMinutesAfterMidnight );

#ifdef MAC_OS
#include <sys/time.h>
/*============================================================================*/

    inline DWORD                GetTickCount()

/*============================================================================*/
{
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    DWORD dwTicks = (DWORD)(tv.tv_sec * 1000);
    dwTicks += (DWORD)(tv.tv_usec / 1e+3);
    return dwTicks;
}
#endif
#ifdef GATEWAY
/*============================================================================*/

    class CTimeZoneInfo

/*============================================================================*/
{
protected:
    TCHAR                       m_szDisplay[128]; 
    TIME_ZONE_INFORMATION       m_Info;

public:
    CTimeZoneInfo(
    HKEY                        hKey );

    inline TCHAR                    *DisplayName()  { return m_szDisplay;   }
    inline TIME_ZONE_INFORMATION    *Info()         { return &m_Info;       }
};
#endif
/*============================================================================*/

    class HL_API CTimeServer

/*============================================================================*/
{
protected:
    static CTimeServer          *g_pInstance;

#ifndef MAC_OS
    TIME_ZONE_INFORMATION       m_TimeZone;         // Time zone data
#endif

    DWORD                       m_dwZoneID;         // Zone ID: daylight or not
    BOOL                        m_bDSTAuto;         // True for DST auto adjust

public:
    CTimeServer();

    virtual ~CTimeServer();

    static void                 InitInstance();
    static void                 ReleaseInstance();
    static CTimeServer          *Instance();

#ifndef MAC_OS
    double                      TimeZone();
#ifdef GATEWAY
    void                        LoadAllTimeZones(CHLObList *pList);
    int                         IndexOfCurrentTimeZone(CHLObList *pList);
    void                        SetTimeZone(CHLObList *pList, int iIndex);
#endif
#endif

    inline BOOL                 DSTAuto()           { return m_bDSTAuto;    }
    inline void                 DSTAuto(BOOL bNew)  { m_bDSTAuto = bNew;    }

#ifdef UNDER_CE
    #ifdef GATEWAY
        void                        FixCETimeZone(
        CHLString                   sTimeZone,
        char                        *pData );
    #endif
#endif

    static int                  CompareTimeZones( 
    const void                  *arg1, 
    const void                  *arg2 );
};
/*============================================================================*/

    class HL_API CHLTimer

/*============================================================================*/
{
public:
    #ifdef MAC_OS
        clock_t                 m_dwCount;
    #else
        DWORD                   m_dwCount;
    #endif


public:
    inline CHLTimer()
    {
        m_dwCount = GetTickCount();
    }

    inline void                 Restart()
    {
        m_dwCount = GetTickCount();
    }

    inline void                 SetElapsed(DWORD dwOffset)
    {
        m_dwCount = GetTickCount() - dwOffset;
    }

    inline DWORD                HLTickCount()
    {
        DWORD dwRet = 0;
        DWORD dwStop = GetTickCount();

        if (dwStop < m_dwCount)
            dwRet = (0xFFFFFFFF - m_dwCount) + dwStop;
        else
            dwRet = dwStop - m_dwCount;

        return dwRet;
    }
};
#ifndef MAC_OS
/*============================================================================*/

    class HL_API CHiResTimer

/*============================================================================*/
{
private:
    static __int64              g_iHZ;

public:
    DWORD                       m_dwOffset;
    DWORD                       m_dwOffsetNS;
    LARGE_INTEGER               m_liTimer;

public:
    inline CHiResTimer()
    {
        m_dwOffset   = 0;
        m_dwOffsetNS = 0;
        QueryPerformanceCounter(&m_liTimer);
    }

    static inline __int64       HZ()        { return g_iHZ; }

    inline void                 Restart()
    {
        m_dwOffset = 0;
        QueryPerformanceCounter(&m_liTimer);
    }

    inline void                 SetElapsed(DWORD dwOffset)
    {
        m_dwOffset = dwOffset;
        QueryPerformanceCounter(&m_liTimer);
    }

    inline void                 SetElapsedNS(DWORD dwOffset)
    {
        m_dwOffsetNS = dwOffset;
        QueryPerformanceCounter(&m_liTimer);
    }

    inline DWORD                HLTickCount()
    {
        LARGE_INTEGER           liNow;
        QueryPerformanceCounter(&liNow);

        int iNow  = liNow.LowPart;
        int iLast = m_liTimer.LowPart;
        __int64 iDiff = iNow - iLast;
        __int64 iMSEC = iDiff * 1000 / g_iHZ;

        DWORD dwMSEC = (DWORD)iMSEC;

        return (dwMSEC + m_dwOffset + m_dwOffsetNS / 1000);
    }

    inline DWORD                HLnsecTickCount()
    {
        LARGE_INTEGER           liNow;
        QueryPerformanceCounter(&liNow);

        __int64 iNow  = liNow.HighPart;
        __int64 iLast = m_liTimer.HighPart;
        iNow = iNow << 32;
        iLast = iLast << 32;
        iNow |= liNow.LowPart;
        iLast |= m_liTimer.LowPart;

        __int64 iDiff = iNow - iLast;
        __int64 iMSEC = iDiff * 1000000 / g_iHZ;
        DWORD dwMSEC = (DWORD)iMSEC;
        return (dwMSEC + m_dwOffset * 1000 + m_dwOffsetNS);
    }

    static inline void          Init()
    {
        LARGE_INTEGER liHZ;
        QueryPerformanceFrequency(&liHZ);
        g_iHZ = (((__int64) liHZ.HighPart) << 32) | (liHZ.LowPart);
    }
};
#endif
/*============================================================================*/

    class HL_API CHLTime

/*============================================================================*/
{
protected:
    #ifdef MAC_OS
        time_t                  m_Time;             // Always stored as UTC
    #else
        SYSTEMTIME              m_Time;             // Always stored as UTC
    #endif

    static int                  m_iGlobalOffset;

public:
    CHLTime();                                      // Sets to current time

    CHLTime(
    SYSTEMTIME                  tm);                // Sets to current time

    CHLTime(
    const CHLTime               &rhs );

    virtual ~CHLTime();

    void                        Set(
    int                         iYear,
    int                         iMonth,
    int                         iDayOfWeek,
    int                         iDay,
    int                         iHour,
    int                         iMinute,
    int                         iSecond,
    int                         iMSecond );


    //
    // CHLTime operators
    //
#ifndef MAC_OS
    operator SYSTEMTIME();                          // Returns a copy

    CHLTime                     &operator=(         // Copy system time
    const SYSTEMTIME            &rhs );
#endif

    CHLTime                     &operator=(         // Exact copy
    const CHLTime               &rhs );

    CHLTime                     &operator+(         // This ASSERTS: don't add
    const CHLTime               &rhs );

    CHLTimeSpan                 operator-(          // Normally do (End - Start)
    const CHLTime               &rhs );

    BOOL                        operator==(         // Exact comparison
    const CHLTime               &rhs );

    BOOL                        operator>=(         // Is after rhs time
    const CHLTime               &rhs );

    BOOL                        operator<=(         // Is before rhs time
    const CHLTime               &rhs );

    BOOL                        operator>(          // Is after rhs time
    const CHLTime               &rhs );

    BOOL                        operator<(          // Is before rhs time
    const CHLTime               &rhs );

    // Functions to change the time.
    void                        AddDays(
    int                         iDays );

    void                        AddHours(
    int                         iHours );

    void                        AddMinutes(
    int                         iMinutes );

    void                        AddSeconds(
    int                         iSeconds );

    void                        AddMSEC(
    int                         iSeconds );

    //
    // Member access
    //
    BOOL                        DaylightSavings();

    void                        GetPresentTime(
    BOOL                        bCorrected = TRUE); // Reset to current time

    int                         Year();             // 1601 - 3000
    void                        Year( int iNew );
    int                         Month();            // 1-12 (Jan=1, etc)
    void                        Month( int iNew );
    int                         DayOfWeek();        // 0-6 (Mon=0, ... Sun=6)
    void                        DayOfWeek( int iNew );
    int                         Day();              // 1-31 (not like day of week...)
    void                        Day( int iNew );
    int                         Hour();             // 0-23
    int                         Hour12();           // 12, 1, 2, ...
    void                        Hour( int iNew );
    int                         Minute();           // 0-59
    void                        Minute( int iNew );
    int                         Second();           // 0-59
    void                        Second( int iNew );
    int                         MSecond();          // 0-999
    void                        MSecond( int iNew );

#ifdef STRINGLIB
    CHLString                   DayOfWeekText();    // Monday, etc.
    CHLString                   MonthText();        // January, etc.
    CHLString                   TimeText();         // 1:00PM etc.
    CHLString                   TimeTextShort();    // 1:00a etc.
    CHLString                   TimeTextSeconds();  // 1:00:21 etc
    CHLString                   AMPMText();         // AM or PM

    CHLString                   DayOfWeekTextShort();   // Mon, Tue, etc
    CHLString                   MonthTextShort();       // Jan, Feb, etc.
#endif

    static CHLString            FormatDuration(
    int                         iSeconds );

    static CHLString            FormatDurationMSEC(
    int                         iMSEC );

    inline static int           GlobalOffsetMinutes()           { return m_iGlobalOffset;   }
    inline static void          GlobalOffsetMinutes(int iNew)   { m_iGlobalOffset = iNew;   }

protected:
    void                        UpdateDayOfWeek();
};


/*============================================================================*/

    class HL_API CHLTimeSpan

/*============================================================================*/
{
    friend class CHLTime;

protected:
    BOOL                        m_bNegative;        // End was before start
    __int64                     m_iTimeSpan;        // UTC delta


public:
    CHLTimeSpan();

    CHLTimeSpan(
    int                         iDays,
    int                         iHours,
    int                         iMinutes,
    int                         iSeconds,
    int                         iMSeconds );

    virtual ~CHLTimeSpan();

    void                        Set(
    int                         iDays,
    int                         iHours,
    int                         iMinutes,
    int                         iSeconds,
    int                         iMSeconds );

    //
    // Note that the values returned below are always positive, even
    // though they return an "int".
    //
    // Use Negative() to determine if the time span is running backwards.
    //
    BOOL                        Negative();
    void                        Negative( BOOL bNew );


    //
    // CHLTimeSpan operators
    //
    operator __int64();                             // Returns a copy

    CHLTimeSpan                 &operator=(         // Exact copy
    const CHLTimeSpan           &rhs );

    CHLTimeSpan                 &operator=(         // Copy from __int64
    const __int64               &rhs );


    //
    // Elapsed time broken down into its components. Note that we don't try to break
    // down the time into months or years since different months / years have different
    // numbers of days, and we don't bother with weeks.
    //
    // Minutes returns 12 for a time span of 1 hr 12 mins.
    //
    int                         Days();
    int                         Hours();
    int                         Minutes();
    int                         Seconds();
    int                         MSeconds();


    //
    // Total elapsed time, not broken down: this is the total time represented by
    // the time span.
    //
    // TotalMinutes returns 72 for a time span of 1 hr 12 mins.
    //
    int                         TotalDays();
    int                         TotalHours();
    int                         TotalMinutes();
    int                         TotalSeconds();
    __int64                     TotalMSeconds();

    CHLString                   MinSecText();
    CHLString                   HourMinText();
    CHLString                   HourMinSecText();
};


/*============================================================================*/

        void                        HLSleep(
        DWORD                       dwMSEC );

/*============================================================================*/

#endif

