//
//  VideoPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "VideoPage.h"
#import "VideoStreamView.h"
#import "SubTab.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "MainTabVC.h"
#import "CustomPage.h"
#import "hlradio.h"
#import "hlbutton.h"
#import "crect.h"
#import "MainView.h"


#define CMD_PRESET                  1000
#define CMD_RES                     1001
#define CMD_ZOOM_OUT                1002
#define CMD_ZOOM_IN                 1003

#define DY_LABEL                    25
#define BORDER                      20

@implementation CVideoPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:pSubTab];
    if (self)
    {
        [self Init:pSubTab];
    }
    return self;
}
/*============================================================================*/

-(void)Init:(CSubTab*)pSubTab

/*============================================================================*/
{
    [m_pZoomIn removeFromSuperview];
    [m_pZoomOut removeFromSuperview];
    
    [m_pResControl release];
    [m_pPresetControl release];
    [m_pVideoStream release];
    [m_pCustomPage release];

    m_pResControl = NULL;
    m_pPresetControl = NULL;
    m_pVideoStream = NULL;
    m_pCustomPage = NULL;

    // Remove Old Views
    NSArray *pViews = self.subviews;
    while([pViews count] > 0)
    {
        UIView *pView = (UIView*)[pViews objectAtIndex:0];
        [pView removeFromSuperview];
        pViews = self.subviews;
    }

    CGRect rectView;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rectView = self.bounds;
    }
    else 
    {
        rectView = CGRectMake(0, 0, 320, 240);

        [super Init:pSubTab];

        switch (m_pMainPage.interfaceOrientation)
        {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            break;
        default:
            rectView = [CRect ScreenRect:m_pMainPage.interfaceOrientation];
            break;
                
        }
    }

    if (m_pSubTab.m_iData1 & VIDEO_CUSTOMPAGE)
    {
        UIInterfaceOrientation ioNow = m_pMainPage.interfaceOrientation;
        m_pCustomPage = [[CCustomPage alloc] initWithFrame:self.bounds SysFam:FAMILY_VIDEO ID:m_pSubTab.m_iID Orientation:ioNow];
        [self addSubview:m_pCustomPage];
        return;
    }

    m_pVideoStream = [[CVideoStreamView alloc] initWithFrame:rectView CameraID:m_pSubTab.m_iID Options:m_pSubTab.m_iData2];
    [m_pVideoStream AddSink:self];

    [self addSubview:m_pVideoStream];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [self AddVideoControls];
    }
    else
    {
        switch (m_pMainPage.interfaceOrientation)
        {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            [self AddVideoControls];
            break;
        default:
            break;
                
        }
    }
}
/*============================================================================*/

-(void)AddVideoControls

/*============================================================================*/
{
    if (m_pSubTab.m_iData1 & VIDEO_CUSTOMPAGE)
        return;

    int iDYThis = self.frame.size.height;
    int iDYRes      = (iDYThis - 240) * 2 / 5 - 8;
    int iDYPreset   = (iDYThis - 240) * 3 / 5 - 8;
    int iYPresetTop = 244;
    int iYResTop = iYPresetTop + iDYPreset+8;
        
    if ((m_pSubTab.m_iData1 & VIDEO_HASPRESETS8) && m_pPresetControl == NULL)
    {
        NSArray *PresetList = [NSArray arrayWithObjects: @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", nil];

        m_pPresetControl = [[CHLRadio alloc] initWithFrame:CGRectZero StringList:PresetList NCols:4];
        [m_pPresetControl SetCommandID:CMD_PRESET Responder:self];
        [self addSubview:m_pPresetControl];
        [self InitSubView:m_pPresetControl];
        [m_pPresetControl release];
    }

    BOOL bResCtrl = TRUE;
    if (m_pSubTab.m_iData2 & VIDEO_OPTION_NOSHOW_RES)
        bResCtrl = FALSE;
    
    if ((m_pSubTab.m_iData1 & VIDEO_HASZOOM) && m_pZoomIn == NULL)
    {
        m_pZoomOut = [[CHLButton alloc] initWithFrame:CGRectZero Text:@"Zoom Out"];
        m_pZoomIn  = [[CHLButton alloc] initWithFrame:CGRectZero Text:@"Zoom In"];
        [self addSubview:m_pZoomIn];
        [self addSubview:m_pZoomOut];
        [m_pZoomIn release];
        [m_pZoomOut release];
        [m_pZoomOut SetCommandID:CMD_ZOOM_OUT Responder:self];
        [m_pZoomIn SetCommandID:CMD_ZOOM_IN Responder:self];

        iYResTop -= (iDYRes+10);
    }

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        // For iPod, not iPad: If we have zoom + presets, omit the res control to save space 
        if (m_pZoomIn != NULL && m_pPresetControl != NULL)
            bResCtrl = FALSE;
    }
    
    if (m_pResControl == NULL && bResCtrl)
    {
        NSArray *PresetList = [NSArray arrayWithObjects: @"Low", @"Medium", @"High", nil];

        m_pResControl = [[CHLRadio alloc] initWithFrame:CGRectZero StringList:PresetList NCols:3];
        [m_pResControl SetCommandID:CMD_RES Responder:self];
        [self addSubview:m_pResControl];
        [self InitSubView:m_pResControl];
        [m_pResControl release];
    }
    

    // Custom initialization
    m_pVideoStream.m_pPresetControl = m_pPresetControl;
    m_pVideoStream.m_pResControl = m_pResControl;
}
/*============================================================================*/

-(BOOL)ShouldRotate:(UIInterfaceOrientation)interfaceOrientation;

/*============================================================================*/
{
    return TRUE;
}
/*============================================================================*/

-(void)PrepareRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo

/*============================================================================*/
{
    // Assume going into landscale
    UIView *pView = m_pVideoStream;
    if (m_pCustomPage != NULL)
        pView = m_pCustomPage;

    double dScaleX = 480.f/320.f;
    double dScaleY = 320.f/240.f;
    int iOffsetX = (480-320)/2;
    int iOffsetY = (320-240)/2;

    if (m_pCustomPage != NULL)
    {
        dScaleY = 320.f / 420.f;
        iOffsetY = (320-420)/2;
    }

    switch (ioTo)
    {
    case UIDeviceOrientationLandscapeLeft:
    case UIDeviceOrientationLandscapeRight:
        break;
        
    case UIDeviceOrientationPortrait:
    case UIDeviceOrientationPortraitUpsideDown:
        {
            dScaleX = 320.f/480.f;
            dScaleY = 240.f/320.f;
            if (m_pCustomPage != NULL)
                dScaleY = 420.f / 320.f;
            iOffsetX = -iOffsetX;
            iOffsetY = -iOffsetY;
        }
        break;
    }

    CGAffineTransform transform1 = CGAffineTransformMakeScale(dScaleX, dScaleY);

    if (m_pVideoStream != NULL)
    {
        if ([m_pVideoStream Rotate180])
        {
            transform1 = CGAffineTransformMakeScale(-dScaleX, -dScaleY);
//            CATransform3D transform = CATransform3DMakeScale(-1, -1, 1);
//            [m_pVideoStream.layer setTransform:transform];
        }
    }

    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(iOffsetX, iOffsetY);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    pView.transform = transform;
    self.clipsToBounds = NO;
}
/*============================================================================*/

-(void)NotifyRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo

/*============================================================================*/
{
    int iDXView = 0;
    int iDYView = 0;
    BOOL bHideNavBar = FALSE;
    int iDXScreen = 320;
    int iDYScreen = 480;
    int iDefaultRes = 0;

    switch(ioTo)
    {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            {
                iDXView = 320;
                iDYView = 240;
                iDefaultRes = 1;
            }
            break;

        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            {
                bHideNavBar = TRUE;
                iDXScreen = 480;
                iDYScreen = 320;
                iDXView = 480;
                iDYView = 320;
                iDefaultRes = 2;
            }
            break;
    }

    CGRect rView = CGRectMake(0, 0, iDXView, iDYView);
    [m_pVideoStream setBounds:rView];
    [m_pVideoStream setCenter:CGPointMake(iDXView/2,iDYView/2)];

    if (m_pVideoStream != NULL)
    {
        if (m_pResControl == NULL && (m_pSubTab.m_iData2 & VIDEO_OPTION_DEFAULTRES_AUTO))
        {
            if ([m_pVideoStream IsStreaming])
            {
                CNSQuery *pQRes = [[[CNSQuery alloc] initWithQ:HLM_VIDEO_SETRESMODEBYIDQ] autorelease];
                [pQRes PutInt:m_pSubTab.m_iID];
                [pQRes PutInt:iDefaultRes];
                [pQRes SendMessage];
            }
        }
    }

    NSArray *pArray = self.subviews;
    for (int i = 0; i < [pArray count]; i++)
    {
        UIView *pView = (UIView*)[pArray objectAtIndex:i];
        if (pView != m_pVideoStream && pView != m_pCustomPage)
            pView.hidden = bHideNavBar;
    }

    [m_pCustomPage NotifyRotationFrom:ioFrom To:ioTo];


    UIView *pView = m_pVideoStream;
    if (m_pCustomPage != NULL)
        pView = m_pCustomPage;

    CGAffineTransform transform1 = CGAffineTransformMakeScale(1, 1);

    if (m_pVideoStream != NULL)
    {
        if ([m_pVideoStream Rotate180])
        {
            transform1 = CGAffineTransformMakeScale(-1, -1);
//            CATransform3D transform = CATransform3DMakeScale(-1, -1, 1);
//            [m_pVideoStream.layer setTransform:transform];
        }
    }

    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, 0);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    pView.transform = transform;


}
/*============================================================================*/

-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    switch(ioNow)
    {
    case UIDeviceOrientationPortrait:
    case UIDeviceOrientationPortraitUpsideDown:
        [self AddVideoControls];    
        break;
    default:
        break;
    }

    [m_pCustomPage setFrame:self.bounds];
    [m_pCustomPage NotifyRotationComplete:ioNow];
    self.clipsToBounds = YES;
}
/*============================================================================*/

-(void)PrepareScaleView:(BOOL)bFullScale

/*============================================================================*/
{
    if (bFullScale == m_bFullScaleView)
        return;

    [super PrepareScaleView:bFullScale];

    if (m_pCustomPage != NULL)
        return;

    [self layoutSubviews];

    [m_pVideoStream SetFullScaleView:m_bFullScaleView];

    if (bFullScale)
    {
        [m_pResControl setAlpha:1.0];
        [m_pPresetControl setAlpha:1.0];
        [m_pZoomIn setAlpha:1.0];
        [m_pZoomOut setAlpha:1.0];
    }
    else
    {
        [m_pResControl setAlpha:0.0];
        [m_pPresetControl setAlpha:0.0];
        [m_pZoomIn setAlpha:0.0];
        [m_pZoomOut setAlpha:0.0];
    }
}
/*============================================================================*/

-(BOOL)WantOverviewOverlay

/*============================================================================*/
{
    if (m_pCustomPage != NULL)
        return TRUE;

    return FALSE;
}
/*============================================================================*/

-(BOOL)VisibleInOverview

/*============================================================================*/
{
    if (m_pCustomPage != NULL)
        return FALSE;

    return TRUE;
}
/*============================================================================*/

-(void)viewWillAppear:(BOOL)animated
 
/*============================================================================*/
{
    [m_pVideoStream StartStream];
}
/*============================================================================*/

-(void)viewWillDisappear:(BOOL)animated
 
/*============================================================================*/
{
    [m_pVideoStream StopStream];
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pResControl release];
    [m_pPresetControl release];
    [m_pVideoStream release];
    [m_pCustomPage release];
    [super dealloc];
}
/*============================================================================*/

-(void)InitSubView:(UIView*)pSubView

/*============================================================================*/
{
    pSubView.backgroundColor = [UIColor clearColor];
	pSubView.opaque = NO;

}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_PRESET:
        {
            int iIndex = [m_pPresetControl GetSelectedIndex];
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_VIDEO_PRESETCONTROLQ];
            [pQ autorelease];
            [pQ PutInt:m_pSubTab.m_iID];
            [pQ PutInt:FALSE]; // TRUE to Set
            [pQ PutInt:iIndex]; // TRUE to Set
            [pQ SendMessage];
            
        }
        break;

    case CMD_RES:
        {
            int iIndex = [m_pResControl GetSelectedIndex];
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_VIDEO_SETRESMODEBYIDQ];
            [pQ autorelease];
            [pQ PutInt:m_pSubTab.m_iID];
            [pQ PutInt:iIndex]; // TRUE to Set
            [pQ SendMessage];
        }
        break;

    case CMD_ZOOM_IN:
    case CMD_ZOOM_OUT:
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_VIDEO_ZOOMCONTROLQ];
            [pQ autorelease];
            [pQ PutInt:m_pSubTab.m_iID];
            [pQ PutInt:(iCommandID == CMD_ZOOM_OUT)]; // TRUE to Set
            [pQ SendMessage];
        }
        break;
    
    default:
        break;
    }
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [m_pVideoStream SetVisible:bVisible];
    [m_pCustomPage SetVisible:bVisible];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (m_pCustomPage == NULL && !m_bFullScaleView)
        {
            CGRect rVideo = self.bounds;
            [m_pVideoStream setFrame:rVideo];
            return;
        }
    }

    CGRect rThis = self.bounds;
    CGRect rVideo = m_pVideoStream.frame;
    int iDYCtrls = 0;
    int iDYCtrl = 50;
    int iNPad = 0;
    int iDYRes = iDYCtrl;

    if (m_pResControl != NULL)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && self.bounds.size.width > self.bounds.size.height)
            iDYRes = iDYCtrl*4;

        iDYCtrls += iDYRes;
        iNPad++;
    }
    if (m_pPresetControl != NULL)
    {
        iDYCtrls += 2*iDYCtrl;
        iNPad++;
    }
    if (m_pZoomIn != NULL)
    {
        iDYCtrls += iDYCtrl;
        iNPad++;
    }

    iDYCtrls += (10 * (iNPad-1));
    
    

    if (rThis.size.width > rThis.size.height)
    {
        // Wide
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            if (iDYCtrls <= 0)
            {
                rVideo = rThis;
                [CRect ConfineToAspect:&rVideo DXDY:4.0/3.0];
            }
            else
            {
                int iDYVideo = rThis.size.height - 2 * BORDER;
                int iDXVideo = iDYVideo * 4 / 3;
                if (m_pPresetControl != NULL)
                {
                    iDYCtrls += 2*iDYCtrl;
                    [m_pPresetControl NColumns:2];
                }

                [CRect BreakOffLeft:&rThis DX:BORDER];
                rVideo = [CRect BreakOffLeft:&rThis DX:iDXVideo];
                [CRect Inset:&rVideo DX:0 DY:BORDER];
                [CRect Inset:&rThis DX:10 DY:0];
                [m_pResControl NColumns:1];
            }
        }
        else 
        {
            rVideo = rThis;
            rThis.origin.y  += rThis.size.height;
        }

    }
    else
    {
        // Tall
        [m_pResControl NColumns:3];
        int iDXVideo = rThis.size.width - 2 * BORDER;
        int iDYVideo = iDXVideo * 3 / 4;
        [CRect BreakOffTop:&rThis DY:BORDER];
        rVideo = [CRect BreakOffTop:&rThis DY:iDYVideo];

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            [CRect Inset:&rVideo DX:BORDER DY:0];

        [m_pPresetControl NColumns:4];
        [CRect Inset:&rThis DX:40 DY:0];

        if (iDYCtrls <= 0)
        {
            rVideo = self.bounds;
            [CRect ConfineToAspect:&rVideo DXDY:4.0/3.0];
        }
    }
    
    iDYCtrls = MIN(iDYCtrls, rThis.size.height);
    CGRect rCtrls = CGRectMake(rThis.origin.x, [CRect YMid:rThis]-iDYCtrls/2, rThis.size.width, iDYCtrls);

    if (m_pResControl != NULL)
    {
        [m_pResControl setFrame:[CRect BreakOffTop:&rCtrls DY:iDYRes]];
        [CRect BreakOffTop:&rCtrls DY:10];
    }
    if (m_pPresetControl != NULL)
    {
        [m_pPresetControl setFrame:[CRect BreakOffTop:&rCtrls DY:iDYCtrl*[m_pPresetControl NRows]]];
        [CRect BreakOffTop:&rCtrls DY:10];
    }
    if (m_pZoomIn != NULL)
    {
        CGRect rZoom = [CRect BreakOffTop:&rCtrls DY:iDYCtrl];

        int iDX = rZoom.size.width / 2 - 2;
        CGRect rIn = [CRect BreakOffLeft:&rZoom DX:iDX];
        CGRect rOut = [CRect BreakOffRight:&rZoom DX:iDX];
        [m_pZoomIn setFrame:rIn];
        [m_pZoomOut setFrame:rOut];
    }

    [m_pVideoStream setFrame:rVideo];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData
    
/*============================================================================*/
{
    [self UpdateOverlay];
}
@end
