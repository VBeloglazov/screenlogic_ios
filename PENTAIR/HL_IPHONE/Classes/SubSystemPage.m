//
//  SubSystemPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/29/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SubSystemPage.h"
#import "IPOD_ViewController.h"
#import "IPAD_ViewController.h"
#import "MainTab.h"
#import "SubTab.h"
#import "hlcomm.h"
#import "hlm.h"
#import "MainTabVC.h"
#import "SubTabheader.h"
#import "systembar.h"
#import "crect.h"
#import "CustomPage.h"
#import "zoneselector.h"
#import "hlsegment.h"
#import "hlbutton.h"
#import "shader.h"

@implementation CSubSystemPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];

    if (self) 
    {
        m_pMainPage = pMainTabPage;
        m_pSubTab = pSubTab;
        self.clipsToBounds = YES;
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        m_bFullScaleView = TRUE;
    }
    return self;
}
/*============================================================================*/

-(void)Init:(CSubTab*)pSubTab

/*============================================================================*/
{
    m_pSubTab = pSubTab;
}
/*============================================================================*/

-(void)viewDidAppear:(BOOL)animated
 
 
/*============================================================================*/
{
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
}
/*============================================================================*/

-(BOOL)ShouldRotate:(UIInterfaceOrientation)interfaceOrientation;

/*============================================================================*/
{
    return TRUE;
}
/*============================================================================*/

-(void)PrepareRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifyRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
}
/*============================================================================*/

-(void)LockHorizontalScroll:(BOOL)bNew

/*============================================================================*/
{
    [m_pMainPage LockHorizontalScroll:bNew];
}
/*============================================================================*/

-(void)PrepareScaleView:(BOOL)bFullScale

/*============================================================================*/
{
    m_bFullScaleView = bFullScale;
}
/*============================================================================*/

-(void)PrepareLoseModal

/*============================================================================*/
{
}
/*============================================================================*/

-(void)PrepareForZoom:(BOOL)bIn

/*============================================================================*/
{
/*
    if (m_pOverlay != NULL)
    {
        if (m_pOverlay.superview != self)
        {
            [m_pOverlay retain];
            [m_pOverlay removeFromSuperview];
            [self addSubview:m_pOverlay];
            [m_pOverlay setFrame:self.bounds];
            [m_pOverlay release];
        }

        if (bIn)
            m_pOverlay.alpha = 1.0;
        else
            m_pOverlay.alpha = 0;
    }
*/
}
/*============================================================================*/

-(BOOL)WantOverviewOverlay

/*============================================================================*/
{
    return TRUE;
}
/*============================================================================*/

-(BOOL)VisibleInOverview

/*============================================================================*/
{
    return FALSE;
}
/*============================================================================*/

-(BOOL)IsVisible

/*============================================================================*/
{
    if (m_pMainPage == NULL)
        return TRUE;

    return [m_pMainPage IsVisible:self];
}
/*============================================================================*/

-(UIViewController*)ViewController

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return [IPAD_ViewController MainViewController];
    return m_pMainPage;
}
/*============================================================================*/

-(CMainTabViewController*)MainTabPage

/*============================================================================*/
{
    return m_pMainPage;
}
/*============================================================================*/

-(CSubTabViewOverlay*)Overlay

/*============================================================================*/
{
    if (m_pSubTabView == NULL)
        return NULL;
    return [m_pSubTabView GetOverlay];
}
/*============================================================================*/

-(void)SetSubTabView:(CSubTabView*)pSubTabView

/*============================================================================*/
{
    m_pSubTabView = pSubTabView;
}
/*============================================================================*/

-(void)InitSubTabViewOverlay

/*============================================================================*/
{
}
/*============================================================================*/

-(void)BeginUpdateSubTabViewOverlay:(int)iDuration Every:(int)iRate

/*============================================================================*/
{
    [m_pSubTabView BeginUpdateReflection:(int)iDuration Every:(int)iRate];
}
/*============================================================================*/

-(void)UpdateOverlay

/*============================================================================*/
{
    [m_pSubTabView InvalidateReflection];
}
/*============================================================================*/

-(CHLSegment*)GetPageCtrl

/*============================================================================*/
{
    return NULL;
}
/*============================================================================*/

-(void)UIInit

/*============================================================================*/
{
}
/*============================================================================*/

-(void)TranslateNewPage:(UIView*)pNewPage OldPage:(UIView*)pOldPage ThisIndex:(int)iThisIndex LastIndex:(int)iLastIndex

/*============================================================================*/
{
    BOOL bReturningPage = FALSE;
    if (_m_pActivePage != NULL)
    {
        if (_m_pActivePage == pNewPage)
        {
            bReturningPage = TRUE;
        }
        else
        {
            [_m_pActivePage removeFromSuperview];
        }
    }

    @try
    {
        [pNewPage.layer setShouldRasterize:YES];
        [pOldPage.layer setShouldRasterize:YES];
    }
    @catch (NSException *ex)
    {
    }


    CGRect rNewPage = pNewPage.frame;
    CGRect rOldPage = pOldPage.frame;
    int iDX = self.bounds.size.width;
    if (iThisIndex > iLastIndex)
        iDX = -iDX;
    rNewPage.origin.x -= iDX;

    if (!bReturningPage)
    {
        [pNewPage setFrame:rNewPage];
        [pNewPage setAlpha:0];
    }

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView setAnimationDuration:0.25];

    rNewPage.origin.x += iDX;
    [pNewPage setFrame:rNewPage];
    rOldPage.origin.x += iDX / 4;
    [pOldPage setFrame:rOldPage];
    [pOldPage setAlpha:0];
    [pNewPage setAlpha:1];
    
    _m_pActivePage = pOldPage;
    
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [_m_pActivePage removeFromSuperview];
    _m_pActivePage = NULL;
}
/*============================================================================*/

-(CToolBarClientView*)CreateMenuView:(CGRect)rView ViewController:(IPAD_ViewController*)pCtlr Flags:(int)iFlags

/*============================================================================*/
{
    CHLSegment *pSeg = [self GetPageCtrl];
    CSubSystemHeaderBarView *pView = [[CSubSystemHeaderBarView alloc] initWithFrame:rView Label:[m_pSubTab Name] Flags:iFlags PageCtrl:pSeg];
    return pView;
}
/*============================================================================*/

-(CSubTab*)SubTab

/*============================================================================*/
{
    return m_pSubTab;
}
/*============================================================================*/

-(BOOL)WantSystemMenu

/*============================================================================*/
{
    return FALSE;
}
@end

@implementation CSubSystemHeaderBarView

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame Label:(NSString*)pLabel Flags:(int)iFlags PageCtrl:(CHLSegment*)pCtrl

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
              
        IPAD_ViewController *pCtlr = [IPAD_ViewController MainViewController];
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        self.autoresizingMask = 0xFFFFFFFF;

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [m_pLabel setText:pLabel];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView TEXT_SIZE_TITLE]];

        [self addSubview:m_pLabel];
        [m_pLabel release];

        #ifdef PENTAIR
            m_pHomeButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:NULL Icon:@"PWPS_fish.png"];
        #else
            m_pHomeButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:NULL Icon:@"G.PNG"];
        #endif

        [m_pHomeButton IconFormat:HL_ICONCENTERMAX];
        [m_pHomeButton Orientation:TOOLBAR_TOP];

        [self addSubview:m_pHomeButton];
        
        //DL - was like this on touch down
        //[m_pHomeButton addTarget:pCtlr action:@selector(ShowMainMenu:) forControlEvents:UIControlEventTouchDown];
        [m_pHomeButton addTarget:pCtlr action:@selector(ShowMainMenu:) forControlEvents:UIControlEventTouchUpInside];

        //DL - long press addition
        
        UILongPressGestureRecognizer *longPressGesture = [[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)] autorelease];
        [longPressGesture setMinimumPressDuration:2];
        [m_pHomeButton addGestureRecognizer:longPressGesture];
        //-------------------------

        
        [m_pHomeButton release];

        int iArrowSize = 48;

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            iArrowSize = 32;
        }
        

        if (iFlags & SUBTAB_HASTABSLEFT)
        {
            m_pArrowL = [[CSystemBarButton alloc] initWithFrame:CGRectZero IconName:@"LEFT.png" Framed:FALSE ImageHeight:iArrowSize];
            [self addSubview:m_pArrowL];
            [m_pArrowL release];
            [m_pArrowL setAlpha:0.4];
            [m_pArrowL addTarget:pCtlr action:@selector(PageLeft:) forControlEvents:UIControlEventTouchDown];
        } 
        if (iFlags & SUBTAB_HASTABSRIGHT)
        {
            m_pArrowR = [[CSystemBarButton alloc] initWithFrame:CGRectZero IconName:@"RIGHT.png" Framed:FALSE ImageHeight:iArrowSize];
            [self addSubview:m_pArrowR];
            [m_pArrowR release];
            [m_pArrowR setAlpha:0.4];
            [m_pArrowR addTarget:pCtlr action:@selector(PageRight:) forControlEvents:UIControlEventTouchDown];
        } 
        
        if (pCtrl != NULL)
        {
            [self addSubview:pCtrl];
            m_pSegment = pCtrl;
        }
    }

    return self;
}
/*============================================================================*/
-(void)longPress:(UILongPressGestureRecognizer*)gesture
{
    switch(gesture.state)
    {
        case UIGestureRecognizerStateFailed:
            printf("Long Falied...");
            break;
        case UIGestureRecognizerStateBegan:
            printf("Long Began");
            
            //DL - do block animation
            [UIView animateWithDuration:0.5 delay:0 options: UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                             animations:^(void)
             {
                 //here is the animation (pulse from 1 to 0.3 and back)
                 [self setAlpha:0.3];
             }
             completion:^(BOOL finished)
             {
                 //restore full alpha when finished
                 [self setAlpha:1.0];
                 return;
             }];

            break;
        case UIGestureRecognizerStateEnded:
            //printf("Long Ended or Recognized");
            //go back to login screen?
            [self.layer removeAllAnimations];
            [((IPAD_ViewController*)m_pMainViewController) ShowConnectPage2];
            break;
        case UIGestureRecognizerStateCancelled:
            printf("Long Cancelled");
            break;
        case UIGestureRecognizerStateChanged:
            printf("Long Changed");
            break;
        case UIGestureRecognizerStatePossible:
            printf("Long Possible");
            break;
    }
}
/*============================================================================*/

/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    NSString *pText = [m_pLabel text];
    CGSize labelSize = [pText sizeWithFont:m_pLabel.font];

    CGRect rThis = self.bounds;
    int iDXThis = rThis.size.width;
    //DL
    rThis.origin.y += 20;

    CGRect rHome = [CRect BreakOffLeft:&rThis DX:iDXThis/8];
    [m_pHomeButton setFrame:rHome];
    

    CGRect rLabel = CGRectMake(iDXThis/2-labelSize.width/2, 0, labelSize.width, self.bounds.size.height);
    //DL
    rLabel.origin.y += 20;
    if (m_pSegment != NULL)
    {
        int iDXSeg = [m_pSegment NItems] * iDXThis / 10;
        CGRect rSeg = [CRect BreakOffRight:&rThis DX:iDXSeg];
        [m_pSegment setFrame:rSeg];
        rLabel.origin.x = [CRect XMid:rThis] - rLabel.size.width / 2;
    }

    [m_pLabel setFrame:rLabel];

    int iDXArrow = self.frame.size.height;
    if (m_pArrowL != NULL)
    {
        CGRect rL = [CRect BreakOffLeft:&rLabel DX:iDXArrow];
        rL.origin.x -= iDXArrow;
        [m_pArrowL setFrame:rL];
    }
    if (m_pArrowR != NULL)
    {
        CGRect rR = [CRect BreakOffRight:&rLabel DX:iDXArrow];
        rR.origin.x += iDXArrow;
        [m_pArrowR setFrame:rR];
    }

    if (m_pAccessoryView != NULL)
    {
        CGRect rThis = self.bounds;
        CGRect rAcc = [CRect BreakOffRight:&rThis DX:m_iAccessoryDX];
        //DL - controls top margin for weather text and bitmap on the main toolbar
        rAcc.origin.y += 20;
        
        [m_pAccessoryView setFrame:rAcc];
    }
}
//DL this one sets combined weather text/picture wiew in the right side of HOME bar
/*============================================================================*/

-(void)SetAccessoryView:(UIView*)pView Width:(int)iDX

/*============================================================================*/
{
    if (m_pAccessoryView != NULL)
    {
        [m_pAccessoryView removeFromSuperview];
        m_pAccessoryView = NULL;
    }

    [self addSubview:pView];
    m_pAccessoryView = pView;
    m_iAccessoryDX = iDX;
}
/*============================================================================*/

-(UIView*)InsetLeft

/*============================================================================*/
{
    return m_pHomeButton;
}
/*=============================================================================*/

-(void)SetMainViewController:(UIViewController*)mainViewController

/*=============================================================================*/
{
    m_pMainViewController = mainViewController;
}
@end

@implementation CSubSystemContainerPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ClientView:(UIView*)pClientView

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        [self addSubview:pClientView];
        m_pClientView = pClientView;
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    //DL - this controls top margin for concrete (pool, spa) single page iPad!
    rThis.origin.y += 20;
    rThis.size.height -= 20;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInset = [CHLComm GetInt:INT_PAGE_INSET];
        [CRect Inset:&rThis DX:iInset DY:iInset];
    }

    [m_pClientView setFrame:rThis];
}

@end

