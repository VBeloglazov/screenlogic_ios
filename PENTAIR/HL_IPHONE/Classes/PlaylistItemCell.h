//
//  PlaylistItemCell.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/2/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


/*============================================================================*/

@interface CPlaylistItemCell : UITableViewCell 

/*============================================================================*/
{
    int                                 m_iPosition;
	CGGradientRef                       m_Gradient1;
	CGGradientRef                       m_Gradient2;
}

-(void)SetPosition:(int)iNew;


@end
