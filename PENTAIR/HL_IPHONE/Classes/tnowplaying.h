//
//  tnowplaying.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/26/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"

@class CAudioZone;
@class CMediaArtView;
@class CMediaArtReflectionView;
@class CPlaylistView;
@class CAlbumInfoView;
@class CButtonBar;
@class CButtonBarButton;
@class CTransportBar;

/*============================================================================*/

@interface CNowPlayingPage : UIView

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    UIScrollView                        *m_pScrollView;

    CAlbumInfoView                      *m_pAlbumView;
    CPlaylistView                       *m_pPlaylistView;
    CTransportBar                       *m_pTransportBar;
    
    CGSize                              m_Size;
}

-(void)DeleteItem;
-(void)PlayItem;
-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone;

@end
/*============================================================================*/

@interface CTransportBar : UIView < CSink >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    CNowPlayingPage                     *m_pNowPlayingPage;
    
    CButtonBar                          *m_pDelete;
    CButtonBar                          *m_pRepeatRandom;
    CButtonBar                          *m_pTransport;
    
    CButtonBarButton                    *m_pRepeat;
    CButtonBarButton                    *m_pRandom;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone NowPlayingPage:(CNowPlayingPage*)pNowPlayingPage;
-(CButtonBarButton*)InitButton:(CButtonBar*)pBar Icon:(NSString*)pIcon Data:(int)iData;

@end
/*============================================================================*/

@interface CAlbumInfoView : UIView     < CSink >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    CMediaArtView                       *m_pImageView;
    CMediaArtReflectionView             *m_pReflectView;
    UIProgressView                      *m_pProgress;
    UILabel                             *m_pArtist;
    UILabel                             *m_pAlbum;
    UILabel                             *m_pTrack;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone;

@end
