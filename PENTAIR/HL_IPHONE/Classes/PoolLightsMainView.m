//
//  PoolLightsMainView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolLightsMainView.h"
#import "PoolLightsView.h"
#import "HLTableFrame.h"
#import "crect.h"
#import "PoolLightsColorMainView.h"
#import "hlcomm.h"
#import "hlm.h"

@implementation CPoolLightsMainView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        CPoolLightsView *pSubView = [[CPoolLightsView alloc] initWithFrame:CGRectZero Config:pConfig];
        m_pLightsView = [[CHLTableFrame alloc] initWithFrame:CGRectZero View:pSubView];
        [pSubView release];
        [self addSubview:m_pLightsView];
        [m_pLightsView release];

        m_pColorsView = [[CPoolLightsColorMainView alloc] initWithFrame:CGRectZero Config:pConfig];
        [self addSubview:m_pColorsView];
        [m_pColorsView release];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rBtm = [CRect BreakOffBottom:&rThis DY:300];
    [CRect BreakOffBottom:&rThis DY:[CHLComm GetInt:INT_PAGE_INSET]];
    [m_pColorsView setFrame:rBtm];
    [m_pLightsView setFrame:rThis];
    
}

@end
