//
//  HVACModePage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HVACPgmPage.h"
#import "HVACScheduleView.h"
#import "tstat.h"
#import "hlm.h"

@implementation CHVACProgramPage

#define PAGE_INSET                      10


/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat

/*============================================================================*/
{
    if (self = [super initWithFrame:rect])
    {
        m_pTStat = pTStat;
        [m_pTStat AddSink:self];
        int iDXThis = self.bounds.size.width;
        int iDYThis = self.bounds.size.height;
        int iInset = PAGE_INSET;
        CGRect rSched = CGRectMake(iInset, iInset, iDXThis - 2 * iInset, iDYThis * 3 / 4);
        m_pScheduleView = [[CHVACScheduleView alloc] initWithFrame:rSched TStat:m_pTStat];
        [self addSubview:m_pScheduleView];
        [self Notify:0 Data:0];
        m_bInit = TRUE;

        self.autoresizingMask = 0xFFFFFFFF;
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTStat RemoveSink:self];
    [m_pScheduleView release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    BOOL bSched = ([m_pTStat HVACMode] != HVAC_STATE_OFF);
    CGRect rSched = m_pScheduleView.frame;

    int iDYThis = self.bounds.size.height - 40;
    
    if (m_bInit)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.75];
    }
    
    if (bSched)
        rSched.origin.y = iDYThis / 2 - rSched.size.height / 2;
    [m_pScheduleView setFrame:rSched];
    
    if (bSched)
        [m_pScheduleView setAlpha:1.0];
    else
        [m_pScheduleView setAlpha:0.0];

    [m_pScheduleView setUserInteractionEnabled:bSched];
    
    if (m_bInit)
        [UIView commitAnimations];
}

@end
