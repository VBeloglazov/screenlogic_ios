//
//  hlprogress.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "hlprogress.h"

@implementation CHLProgressRect

/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *pColor = [UIColor colorWithRed:0.5 green:0.5 blue:1.0 alpha:1.0];
    [pColor set];
    CGContextFillRect(context, rect);
}

@end

@implementation CHLProgress


/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
    if ((self = [super initWithFrame:frame])) 
    {
        m_pPoints = [[NSMutableArray alloc] init];

        // Initialization code
        int iDYDot = frame.size.height;
        int iDXDot = iDYDot + 4;
        iDXDot = MIN(iDXDot, 10);
        int iNDots = frame.size.width / iDXDot;
        for (int i = 0; i < iNDots; i++)
        {
            CGRect rDot = CGRectMake(iDXDot*i, 0, iDXDot-4, frame.size.height);
            CHLProgressRect *pDot = [[CHLProgressRect alloc] initWithFrame:rDot];
            [self addSubview:pDot];
            [m_pPoints addObject:pDot];
            [pDot setAlpha:.25];
        }

    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pPoints release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetProgress:(int)iPCT

/*============================================================================*/
{
    int iSize = [m_pPoints count];
    int iMax = iSize * iPCT / 100;
    for (int i = 0; i < iSize; i++)
    {
        CHLProgressRect *pRect = (CHLProgressRect*)[m_pPoints objectAtIndex:i];
        if (i >= iMax)
            [pRect setAlpha:.25];
        else
            [pRect setAlpha:1.0];
    }
}


@end
