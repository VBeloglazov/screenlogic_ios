//
//  HVACMainTabViewController.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/29/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "MainTabVC.h"
#import "NSSocketSink.h"

#import <UIKit/UIKit.h>

/*============================================================================*/

@interface CHVACMainTabViewController : CMainTabViewController < CNSSocketSink >

/*============================================================================*/
{
    UILabel                         *m_pTempText;
}

-(void)PostWeather;


@end
