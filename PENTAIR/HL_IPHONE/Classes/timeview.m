//
//  timeview.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "timeview.h"
#import "timedataview.h"
#import "timebar.h"
#import "timedataview.h"

@implementation CTimeView



/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame]) 
    {
        m_bStaticScale = FALSE;
        self.backgroundColor = [UIColor clearColor];

        // Initialization code
        m_pCalendar = [[NSCalendar currentCalendar] retain];
        m_iViewSeconds = 3600*24;   // 1 day
        NSDate *pNow = [NSDate date];
        m_pViewCenterDate = [[pNow dateByAddingTimeInterval:-m_iViewSeconds / 2] retain];
        [self setMultipleTouchEnabled:YES];

        CGRect rTimeBar = CGRectMake(-rFrame.size.width, rFrame.size.height-[CMainView DY_TIMEBAR], 3*rFrame.size.width, [CMainView DY_TIMEBAR]);
        m_pTimeBar = [[CTimeBar alloc] initWithFrame:rTimeBar TimeView:self];
        [self addSubview:m_pTimeBar];

        m_pLegendView = [self CreateLegendView];
        if (m_pLegendView)
            [self addSubview:m_pLegendView];
        [self setClipsToBounds:YES];
        self.autoresizingMask = 0xFFFFFFFF;
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTimeDataView StopQuery];
    [m_pActivityView release];
    [m_pLegendView release];
    [m_pCalendar release];
    [m_pScaleView release];
    [m_pTimeDataView release];
    [m_pTimeBar release];
    [m_pViewCenterDate release];
    [super dealloc];
}
/*============================================================================*/

-(int)IntervalDX:(int)iType

/*============================================================================*/
{
    float iDXThis = self.bounds.size.width;
    float iNSec = [self IntervalSeconds:iType];

    return (int)(iNSec * iDXThis / (float)m_iViewSeconds);
}
/*============================================================================*/

-(int)XLogical:(NSDate*)pDate

/*============================================================================*/
{
    // dDiff Should be in seconds
    NSTimeInterval dDiff = [pDate timeIntervalSinceDate:m_pViewCenterDate];
    float fDXThis = self.bounds.size.width;
    float fPixels = dDiff * fDXThis /  (float)m_iViewSeconds;
    return (int)fDXThis / 2.f + fPixels;
    
}
/*============================================================================*/

-(NSDate*)GetTimeAtX:(int)iX

/*============================================================================*/
{
    float fDXThis = self.bounds.size.width;
    float fDX = iX - (fDXThis / 2.f);
    NSTimeInterval dDiff = fDX * (float)m_iViewSeconds / fDXThis;
    NSDate *pDate = [NSDate alloc];
    pDate = [pDate initWithTimeInterval:dDiff sinceDate:m_pViewCenterDate];
    return pDate;
}
/*============================================================================*/

-(NSDate*)RoundUp:(NSDate*)pDate Type:(int)iType

/*============================================================================*/
{
    // Round up past
    int iInt = [self IntervalSeconds:iType];
    NSDate *pTemp = [[[NSDate alloc] initWithTimeInterval:iInt sinceDate:pDate] autorelease];

    int iFlags = kCFCalendarUnitYear|kCFCalendarUnitMonth|kCFCalendarUnitDay|kCFCalendarUnitHour|kCFCalendarUnitMinute|kCFCalendarUnitSecond;
    NSDateComponents *pComps = [m_pCalendar components:iFlags fromDate:pTemp];

    switch (iType)
    {
    case TYPE_MONTH:  
        {
            [pComps setDay:1];
            [pComps setHour:0];
            [pComps setMinute:0];
            [pComps setSecond:0];
        }
        break;
    case TYPE_MONTH_DAY:  
    case TYPE_DAY:  
        {
            [pComps setHour:0];
            [pComps setMinute:0];
            [pComps setSecond:0];
        }
        break;

    case TYPE_HOUR_12:
    case TYPE_HOUR_6:
    case TYPE_HOUR_2:
    case TYPE_HOUR_1:
        {
            switch (iType)
            {
            case TYPE_HOUR_12:  [pComps setHour:[pComps hour] / 12 * 12]; break;
            case TYPE_HOUR_6:   [pComps setHour:[pComps hour] / 6 * 6]; break;
            case TYPE_HOUR_2:   [pComps setHour:[pComps hour] / 2 * 2]; break;
            }

            [pComps setMinute:0];
            [pComps setSecond:0];
        }
        break;

    case TYPE_MIN_30:
    case TYPE_MIN_15:
    case TYPE_MIN_10:
    case TYPE_MIN_5: 
    case TYPE_MIN_1: 
        {
            switch (iType)
            {
            case TYPE_MIN_30:  [pComps setMinute:[pComps minute] / 30 * 30]; break;
            case TYPE_MIN_15:  [pComps setMinute:[pComps minute] / 15 * 15]; break;
            case TYPE_MIN_10:  [pComps setMinute:[pComps minute] / 10 * 10]; break;
            case TYPE_MIN_5:  [pComps setMinute:[pComps minute] / 5 * 5]; break;
            }
        }
        break;
    }
    

    NSDate *pRet = [[m_pCalendar dateFromComponents:pComps] retain];
    return pRet;
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    NSArray *pTouches = [touches allObjects];
    for (int i = 0; i < [pTouches count]; i++)
    {
        UITouch *pTouch = [pTouches objectAtIndex:i];
        if (m_pTouch1 == NULL)
        {
            m_iViewSecondsTouch = m_iViewSeconds;
            m_pViewCenterTouch = [[NSDate alloc] initWithTimeInterval:0 sinceDate:m_pViewCenterDate];
            m_iYDataTouch = m_pTimeDataView.frame.origin.y;

            // First Touch
            m_pTouch1 = pTouch;
            m_ptTouch1 = [pTouch locationInView:self];
            m_ptTouch1Last = m_ptTouch1;
            printf("Touches Begin 1 Down\n");
        }
        else
        {
            m_ptTouch2 = [pTouch locationInView:self];
            if ([self DistanceFrom:m_ptTouch1 To:m_ptTouch2] < 50)
            {
                return;
            }

            int iXMid = (m_ptTouch2.x + m_ptTouch1.x) / 2;
            m_pViewCenterZoom = [self GetTimeAtX:iXMid];

            // Second finger down
            m_pTouch2 = pTouch;
            m_ptTouch2Last = m_ptTouch2;
            [m_pTimeBar setHidden:YES];
            printf("Touches Begin 2 Down\n");

        }
    }
}
/*============================================================================*/

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    NSArray *pTouches = [touches allObjects];
    for (int i = 0; i < [pTouches count]; i++)
    {
        UITouch *pTouch = [pTouches objectAtIndex:i];
        // Second touch or move on 1
        if (pTouch == m_pTouch1)
        {
            m_ptTouch1Last = [pTouch locationInView:self];

            if (m_pTouch2 != NULL)
            {
                // Update Zooming
                [self UpdateZoom];
            }
            else
            {
                // Update Scrolling
                [self UpdateScroll];
            }
            printf("Touch 1 Moved\n");
        }
        else if (pTouch == m_pTouch2)
        {
            m_ptTouch2Last = [pTouch locationInView:self];
            // Update Zooming
            [self UpdateZoom];
            printf("Touch 2 Moved\n");
        }
    }
}
/*============================================================================*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    if (m_pTouch1 == NULL && m_pTouch2 == NULL)
        return;
    
    if (m_pTouch2 != NULL)
    {
        // Normally our center is lined up with the time data view
        // But this can shift due to zooming, correct here

        //  Centerline of ourview in time data view
        CGRect rData = m_pTimeDataView.frame;
        int iDXThis = self.frame.size.width;
        int iXCView = iDXThis / 2- rData.origin.x;
        int iXCAll = rData.size.width / 2;

        // Center offset for data view
        int iDXC = iXCView - iXCAll;
        double dSec = (double) m_iViewSeconds * (double) iDXC / iDXThis;
      //  dSec = dSec * (double) iDXThis / (double)rData.size.width;
        NSDate *pDate = [NSDate alloc];
        pDate = [pDate initWithTimeInterval:dSec sinceDate:m_pViewCenterDate];
        [m_pViewCenterDate release];
        m_pViewCenterDate = pDate;
        printf("Touches Ended 2 Down\n");
    }
    else
    {
        int iDXPix = m_ptTouch1Last.x - m_ptTouch1.x;

        NSDate *pNow = [NSDate date];
        int iXNow = [self XLogical:pNow];
        int iDXThis = self.frame.size.width;

        if ((iXNow - (iDXThis / 2 - iDXPix)) < 0)
            iDXPix = -(iXNow - iDXThis / 2);


        NSDate *pDate = [self GetTimeAtX:self.bounds.size.width / 2 - iDXPix];
        [m_pViewCenterDate release];
        m_pViewCenterDate = pDate;
        printf("Touches Ended 1 Down\n");
    }

    if (m_pTimeDataView != NULL)
    {
        m_iYDataTouch = m_pTimeDataView.frame.origin.y;
    }

    [m_pTimeDataView StopQuery];
    [m_pTimeDataView removeFromSuperview];

    [m_pTimeBar removeFromSuperview];
    [m_pTimeBar release];

    CGRect rFrame = self.frame;

    CGRect rData = CGRectMake(-rFrame.size.width, m_iYDataTouch, 3*rFrame.size.width, [self DataViewDY]);
    CTimeDataView *pNewView = [self CreateTimeDataView:rData LastView:m_pTimeDataView];
    [m_pTimeDataView release];
    m_pTimeDataView = pNewView;
    
    if (m_pLegendView)
    {
        [self bringSubviewToFront:m_pLegendView];
    }

//    [self addSubview:m_pTimeDataView];
    [self insertSubview:m_pTimeDataView belowSubview:m_pScaleView];

    CGRect rTimeBar = CGRectMake(-rFrame.size.width, rFrame.size.height-[CMainView DY_TIMEBAR], 3*rFrame.size.width, [CMainView DY_TIMEBAR]);
    m_pTimeBar = [[CTimeBar alloc] initWithFrame:rTimeBar TimeView:self];
    [self addSubview:m_pTimeBar];
    [m_pTimeDataView UpdateTimeBar];

    [m_pViewCenterTouch release];
    [m_pViewCenterZoom release];
    m_pViewCenterTouch = NULL;
    m_pViewCenterZoom = NULL;

    m_pTouch1 = NULL;
    m_pTouch2 = NULL;
}
/*============================================================================*/

-(void)UpdateZoom

/*============================================================================*/
{
    int iDStart = [self DistanceFrom:m_ptTouch1 To:m_ptTouch2];
    int iDNow = [self DistanceFrom:m_ptTouch1Last To:m_ptTouch2Last];
    m_iViewSeconds = m_iViewSecondsTouch * iDStart / iDNow;
    m_iViewSeconds = MAX(m_iViewSeconds, 5*60);
    m_iViewSeconds = MIN(m_iViewSeconds, 3600 * 24 * 30);

    double dScale = (double)m_iViewSecondsTouch / (double)m_iViewSeconds;

    int iDXShift = 0;

    // Get the center point between fingers now
    int iXMid = (m_ptTouch1Last.x + m_ptTouch2Last.x) / 2;
    NSDate *pTimeCenterNow = [[self GetTimeAtX:iXMid] autorelease];
    NSTimeInterval diff = [pTimeCenterNow timeIntervalSinceDate:m_pViewCenterZoom];
    iDXShift = self.frame.size.width * diff  / m_iViewSeconds;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.10];
    CGAffineTransform transform1 = CGAffineTransformMakeScale(dScale, 1.0);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(iDXShift, 0);

    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    m_pTimeDataView.transform = transform;
    [UIView commitAnimations];

//    [m_pTimeDataView setNeedsDisplay];
}
/*============================================================================*/

-(void)UpdateScroll

/*============================================================================*/
{
    NSDate *pNow = [NSDate date];
    // Figure out where on the screen "Now Is"
    int iXNow = [self XLogical:pNow];

    int iDXPix = m_ptTouch1Last.x - m_ptTouch1.x;
    int iDYPix = m_ptTouch1Last.y - m_ptTouch1.y;

    int iDXThis = self.frame.size.width;

    if ((iXNow - (iDXThis / 2 - iDXPix)) < 0)
    {
        iDXPix = -(iXNow - iDXThis / 2);
    }


    CGRect rTimeBar = m_pTimeBar.frame;
    rTimeBar.origin.x = -iDXThis + iDXPix;
    [m_pTimeBar setFrame:rTimeBar];
    [m_pTimeBar UpdateAlignments];
    CGRect rData = m_pTimeDataView.frame;
    rData.origin.x = -iDXThis + iDXPix;

    int iDYMax = m_pTimeDataView.bounds.size.height - self.frame.size.height + [CMainView DY_TIMEBAR];
    iDYMax = MAX(0, iDYMax);
    int iYData = m_iYDataTouch+iDYPix;
    iYData = MIN(0, iYData);
    iYData = MAX(iYData, -iDYMax);
    if (m_bLockYScroll)
        iYData = 0;

    rData.origin.y = iYData;

    if (m_pScaleView != NULL)
    {
        CGRect rScale = m_pScaleView.frame;
        rScale.origin.y = iYData;
        [m_pScaleView setFrame:rScale];
    }

    [m_pTimeDataView setFrame:rData];
    [m_pTimeDataView NotifyPosition];
}
/*============================================================================*/

-(int)IntervalSeconds:(int)iType

/*============================================================================*/
{
    switch (iType)
    {
    case TYPE_MONTH:        return 24*3600*31;

    case TYPE_MONTH_DAY:    
    case TYPE_DAY:      
        return 24*3600;

    case TYPE_HOUR_12:  return 12*3600;
    case TYPE_HOUR_6:   return 6*3600;
    case TYPE_HOUR_2:   return 7200;
    case TYPE_HOUR_1:   return 3600;
    case TYPE_MIN_30:   return 30*60;
    case TYPE_MIN_15:   return 15*60;
    case TYPE_MIN_10:   return 10*60;
    case TYPE_MIN_5:    return 5*60;
    case TYPE_MIN_1:    return 60;
    }
    return 1;
}
/*============================================================================*/

-(CTimeBar*)GetTimeBar

/*============================================================================*/
{
    return m_pTimeBar;
}
/*============================================================================*/
    
-(void)InitScaleView

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifyRotationComplete

/*============================================================================*/
{
    if (!m_bStaticScale)
        [self InitScaleView];

    if (m_pScaleView == NULL)
        return;

    m_pScaleView.alpha = 0;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.50];
    [m_pScaleView setAlpha:1.0];
    [UIView commitAnimations];
}
/*============================================================================*/

-(UIView*)CreateLegendView

/*============================================================================*/
{
    return NULL;
}
/*============================================================================*/

-(CTimeDataView*)CreateTimeDataView:(CGRect)rFrame LastView:(CTimeDataView*)pLastView

/*============================================================================*/
{
    CTimeDataView *pView = [[CTimeDataView alloc] initWithFrame:rFrame TimeView:self];
    return pView;
}
/*============================================================================*/

-(UIView*)GetScaleView

/*============================================================================*/
{
    return m_pScaleView;
}
/*============================================================================*/

-(int)DistanceFrom:(CGPoint)pPt1 To:(CGPoint)pPt2

/*============================================================================*/
{
    int iDX = pPt1.x - pPt2.x;
    int iDY = pPt1.y - pPt2.y;
    return sqrt(iDX*iDX + iDY*iDY);
}
/*============================================================================*/

-(int)DataViewDY

/*============================================================================*/
{
    return self.frame.size.height;
}
/*============================================================================*/

-(void)SetWorking:(BOOL)bWorking

/*============================================================================*/
{
    if (bWorking)
    {
        if (m_pActivityView != NULL)
            return;
        m_pActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        int iDim = 24;
        CGRect rFrame = CGRectMake(self.frame.size.width - iDim-5, 32, iDim, iDim);
        [m_pActivityView setFrame:rFrame];
        [self addSubview:m_pActivityView];
        [m_pActivityView startAnimating];
        return;
    }
    
    if (m_pActivityView == NULL)
        return;
    [m_pActivityView removeFromSuperview];
    [m_pActivityView release];
    m_pActivityView = NULL;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rFrame = self.bounds;
    int iDXTime = 3*rFrame.size.width;
    if (iDXTime == m_iDXLast)
        return;
    m_iDXLast = iDXTime;

    if (m_pTimeBar != NULL)
    {
        CGRect rTimeBar = CGRectMake(-rFrame.size.width, rFrame.size.height-[CMainView DY_TIMEBAR], iDXTime, [CMainView DY_TIMEBAR]);
        [m_pTimeBar setFrame:rTimeBar];

        if (m_pTimeBar != NULL)
        {
            
            // Time Bar exists, OK to keep it?
            [m_pTimeBar removeFromSuperview];
            [m_pTimeBar release];
            m_pTimeBar = NULL;
        }

        if (m_pTimeBar == NULL)
        {
            m_pTimeBar = [[CTimeBar alloc] initWithFrame:rTimeBar TimeView:self];
            [self addSubview:m_pTimeBar];
            [m_pTimeDataView UpdateTimeBar];
        }
    }

    CGRect rData = CGRectMake(-rFrame.size.width, m_pTimeDataView.frame.origin.y, iDXTime, [self DataViewDY]);
    [m_pTimeDataView setFrame:rData];
    [m_pTimeDataView SetXOrigin];
    [m_pTimeDataView setNeedsDisplay];
    [m_pTimeDataView NotifyPosition];

    if (!m_bStaticScale)
        [m_pScaleView setAlpha:0];
}
@end
