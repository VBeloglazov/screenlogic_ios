/*
--------------------------------------------------------------------------------

    NSMSG.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifdef __cplusplus
    class CHLMSG;
    class CHLString;
#else
    @class CHLMSG;
    @class CHLString;
#endif

#include "hlm.h"

@class CDateHelper;

/*============================================================================*/

    @interface CNSMSG : NSObject

/*============================================================================*/
{
    CHLMSG                      *m_pMSG;
}

-(CHLMSG*)Message;

-(id)initWithMSG:(CHLMSG*) pMSG;

// Response Examination
-(void)ResetRead;
-(int)MessageID;
-(int)SenderID;
-(int)MessageSize;
-(int)DataSize;
-(int)GetInt;
-(int)GetShort;
-(int)GetBYTE;
-(UIColor*)GetColor;
-(NSString*)GetString;
-(NSString*)GetStringNoFilter;
-(NSDate*)GetTimeCompressedWith:(NSDateComponents*)pComponents Calendar:(NSCalendar*)pCalendar DateHelper:(CDateHelper*)pHelper;
-(NSDate*)GetTimeWith:(NSDateComponents*)pComponents Calendar:(NSCalendar*)pCalendar DateHelper:(CDateHelper*)pHelper;
-(unsigned char*)GetDataAtReadIndex;

+(void)MakeCHLString:(CHLString*)pOut From:(NSString*)pIn;

@end


/*============================================================================*/

@interface CDateHelper : NSObject

/*============================================================================*/
{
    NSDate                          *m_pLastDate;
    NSDate                          *m_pLastDateRet;
    int                             m_iYear;
    int                             m_iMonth;
    int                             m_iDay;
}

-(id)init;

-(NSDate*)GetDateFromComponents:(NSDateComponents*)pComponents Calendar:(NSCalendar*)pCalendar;

@end