//
//  HLView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

/*============================================================================*/

@protocol CHLView

/*============================================================================*/

-(void)SetVisible:(BOOL)bNew;
-(void)NotifySubViewChanged;

@end

/*============================================================================*/

@protocol CHLCommandResponder

/*============================================================================*/

-(void)OnCommand:(int)iCommandID;

@end

/*============================================================================*/

@protocol CHLTouchReceiver

/*============================================================================*/

-(void)NotifyTouch:(BOOL)bDown Sender:(id)Sender;

@end

