//
//  timebar.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "timebar.h"
#import "timeview.h"
#import "CustomPage.h"

@implementation CTimeBarLabel

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Text:(NSString*)pText Type:(int)iType StartDate:(NSDate*)pStartDate EndDate:(NSDate*)pEndDate

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
//        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.9]];
        [self setBackgroundColor:[UIColor clearColor]];
        m_iType = iType;
        m_pStartDate = pStartDate;
        m_pEndDate = pEndDate;
        [m_pStartDate retain];
        [m_pEndDate retain];

        m_pLabel = [[UILabel alloc] initWithFrame:self.bounds];
        [CCustomPage InitStandardLabel:m_pLabel Size:10];
        [m_pLabel setText:pText];
        [self addSubview:m_pLabel];
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pAuxView release];
    [m_pStartDate release];
    [m_pEndDate release];
    [m_pLabel release];
    [super dealloc];
}
/*============================================================================*/

- (void)UpdateAlignmentFrom:(int)iXMin To:(int)iXMax

/*============================================================================*/
{
    int iX0This = self.frame.origin.x;
    int iX1This = iX0This + self.frame.size.width;
    
    int iX0Label = 0;
    int iDXLabel = self.frame.size.width;

    if (iX0This < iXMin && iX1This >= iXMin)
    {
        // Straddle left edge of visible
        iX0Label = iXMin - iX0This;
        iDXLabel -= iX0Label;
    }
    if (iX1This > iXMax && iX0This < iXMax)
    {
        // Straddle right edge of visible
        iDXLabel = iXMax - iX0Label - iX0This;
    }

    int iY0 = 0;
    int iDY = self.frame.size.height;

    CGRect rLabelFrame = m_pLabel.frame;
    if (iX0Label != rLabelFrame.origin.x || iDXLabel != rLabelFrame.size.width)
    {
        [m_pLabel setFrame:CGRectMake(iX0Label, iY0,iDXLabel, iDY)];
        if (m_pAuxView != NULL)
            [m_pAuxView setFrame:CGRectMake(iX0Label, iY0,iDXLabel, iDY)];
    }
}
/*============================================================================*/

-(void)SetAuxView:(UIView*)pAuxView

/*============================================================================*/
{
    if (m_pAuxView != NULL)
    {
        [m_pAuxView removeFromSuperview];
        [m_pAuxView release];
    }

    m_pAuxView = pAuxView;

    if (pAuxView == NULL)
        return;

    [m_pAuxView retain];

    [pAuxView setFrame:m_pLabel.frame];
    [self addSubview:pAuxView];
    
}
/*============================================================================*/

- (int)Type

/*============================================================================*/
{
    return m_iType;
}
/*============================================================================*/

-(NSDate*)StartDate         { return m_pStartDate;  }
-(NSDate*)EndDate           { return m_pEndDate;    }

/*============================================================================*/
@end


@implementation CTimeBar


/*============================================================================*/

- (id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView

/*============================================================================*/
{
    if (self = [super initWithFrame:frame]) 
    {
        [self setBackgroundColor:[UIColor clearColor]];

        m_pTimeView = pTimeView;
        m_pLabels = [[NSMutableArray alloc] init];

        // Initialization code
        int iDYThis = self.frame.size.height;
        int iTypes[2];
        int iType0 = TYPE_DAY;
        int iType1 = TYPE_MIN_1;

        while (iType0 >= TYPE_MONTH && [pTimeView IntervalDX:iType0] < [self MinIntervalSize:iType0])
            iType0--;
        while (iType1 > iType0 && [pTimeView IntervalDX:iType1] < [self MinIntervalSize:iType1])
            iType1--;

        if (iType1 == iType0 && iType0 == TYPE_MONTH_DAY)
            iType0 = TYPE_MONTH;
        if (iType1 == iType0)
            iType1 = TYPE_HOUR_1;

        iTypes[0] = iType0;
        iTypes[1] = iType1;

        if (iType1 < 0 || [pTimeView IntervalDX:iType1] < [self MinIntervalSize:iType1])
        {
            CGRect rBar = CGRectMake(0, 0, frame.size.width, iDYThis);
            [self InitBar:rBar Type:iType0 TimeView:pTimeView];
        }
        else
        {
            int iDYBar = iDYThis / 2;
            for (int i = 1; i >= 0; i--)
            {
                CGRect rBar = CGRectMake(0, i * iDYBar, frame.size.width, iDYBar);
                [self InitBar:rBar Type:iTypes[1-i] TimeView:pTimeView];
            }
        }
    }

    [self UpdateAlignments];

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pLabels release];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rDraw

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();

    UIColor *pBG = [UIColor colorWithRed:0 green:0 blue:0 alpha:.75];
    [pBG set];
    CGContextFillRect(pRef, rDraw);

    UIColor *pRGB = [UIColor darkGrayColor];
    [pRGB set];
    
    for (int i = 0; i < [m_pLabels count]; i++)
    {
        CTimeBarLabel *pLabel = (CTimeBarLabel*)[m_pLabels objectAtIndex:i];
        CGContextStrokeRect(pRef, pLabel.frame);
    }
}
/*============================================================================*/

-(BOOL)InitBar:(CGRect)rBar Type:(int)iType TimeView:(CTimeView*)pTimeView

/*============================================================================*/
{
    if (iType < 0)
        return FALSE;

    int iDXThis = self.bounds.size.width;
    int iDXInterval = [pTimeView IntervalDX:iType];
    if (iDXInterval < [self MinIntervalSize:iType])
        return FALSE;

    int iXLeft = self.frame.origin.x;
    
    NSDate *pDayStart = [pTimeView GetTimeAtX:iXLeft];
    NSDate *pDayEnd   = [[pTimeView RoundUp:pDayStart Type:iType] autorelease];

    NSTimeInterval interval = [pTimeView IntervalSeconds:iType];

    int iXL = 0;
    int iXR = 0;
    BOOL bEntry = TRUE;
    while (iXR < iDXThis)
    {
        

        NSDateComponents *pComps = [[NSCalendar currentCalendar] components:0xFFFFFFFF fromDate:pDayStart];
        NSString *pText = NULL;

        switch (iType)
        {
        case TYPE_MONTH_DAY:
        case TYPE_DAY:  
            {
                // Check for DST
                NSDateComponents *pCompsEnd = [[NSCalendar currentCalendar] components:0xFFFFFFFF fromDate:pDayEnd];
                if ([pCompsEnd day] == [pComps day])
                {
                    pDayEnd = [pDayEnd dateByAddingTimeInterval:3600];
                }
            }
            break;
        }


        iXL = [pTimeView XLogical:pDayStart] - iXLeft;
        iXR = [pTimeView XLogical:pDayEnd] - iXLeft;
        CGRect rDay = CGRectMake(iXL, rBar.origin.y, iXR-iXL, rBar.size.height);

        switch (iType)
        {
        case TYPE_MONTH:
            {
                switch ([pComps month])
                {
                case 1: pText = @"January";  break;
                case 2: pText = @"February";  break;
                case 3: pText = @"March";  break;
                case 4: pText = @"April";  break;
                case 5: pText = @"May";  break;
                case 6: pText = @"June";  break;
                case 7: pText = @"July";  break;
                case 8: pText = @"August";  break;
                case 9: pText = @"September";  break;
                case 10: pText = @"October";  break;
                case 11: pText = @"November";  break;
                case 12: pText = @"December";  break;
                }
            }
            break;

        case TYPE_MONTH_DAY:
            pText = [NSString stringWithFormat:@"%d", [pComps day]];
            break;

        case TYPE_DAY:  
            pText = [NSString stringWithFormat:@"%d/%d", [pComps month], [pComps day]];   break;
            break;

        case TYPE_HOUR_12:
        case TYPE_HOUR_6:
        case TYPE_HOUR_2:
        case TYPE_HOUR_1:
            {
                int iHour = [pComps hour];
                NSString *pAMPM = @"AM";
                

                if (iHour >= 12)
                {
                    iHour -= 12;
                    pAMPM = @"PM";
                }

                if (iHour == 0)
                    iHour = 12;
                
                pText = [NSString stringWithFormat:@"%d %s",  iHour, [pAMPM UTF8String]];   
            }
            break;

        case TYPE_MIN_30:
        case TYPE_MIN_15:
        case TYPE_MIN_10:
        case TYPE_MIN_5: 
        case TYPE_MIN_1: 
            {
                int iHour = [pComps hour];
                NSString *pAMPM = @"AM";
                if (iHour >= 12)
                {
                    iHour -= 12;
                    pAMPM = @"PM";
                }

                if (iHour == 0)
                    iHour = 12;
                
                pText = [NSString stringWithFormat:@"%d:%02d %s", iHour, [pComps minute], [pAMPM UTF8String]];   
            }
            break;
        }

        NSDate *pStartDate = pDayStart;
        NSDate *pEndDate = pDayEnd;
        if (bEntry)
            pStartDate = NULL;
        if (iXR >= self.frame.size.width)
            pEndDate = NULL;

        CTimeBarLabel *pLabel = [[CTimeBarLabel alloc] initWithFrame:rDay Text:pText Type:iType StartDate:pStartDate EndDate:pEndDate]; 
        [self addSubview:pLabel];
        [m_pLabels addObject:pLabel];
        [pLabel release];

        [pDayStart release];
        pDayStart = [[NSDate alloc] initWithTimeInterval:0 sinceDate:pDayEnd];
        pDayEnd = [pDayEnd dateByAddingTimeInterval:interval];
        bEntry = FALSE;
    }

    [pDayStart release];

    return TRUE;
}
/*============================================================================*/

-(int)MinIntervalSize:(int)iType

/*============================================================================*/
{
    switch (iType)
    {
    case TYPE_MONTH:        return 120;
    case TYPE_MONTH_DAY:    return 20;
    case TYPE_DAY:          return 40;
    case TYPE_HOUR_12:
    case TYPE_HOUR_6: 
    case TYPE_HOUR_2: 
    case TYPE_HOUR_1: 
        return 30;
    case TYPE_MIN_30: 
    case TYPE_MIN_15: 
    case TYPE_MIN_10: 
    case TYPE_MIN_5:  
    case TYPE_MIN_1:  
        return 50;
    }
    return 100;
}
/*============================================================================*/

-(void)UpdateAlignments

/*============================================================================*/
{
    int iXMin = -self.frame.origin.x;
    int iXMax = iXMin + m_pTimeView.frame.size.width;
    for (int i = 0; i < [m_pLabels count]; i++)
    {
        CTimeBarLabel *pLabel = (CTimeBarLabel*)[m_pLabels objectAtIndex:i];
        [pLabel UpdateAlignmentFrom:iXMin To:iXMax];
    }
}
/*============================================================================*/

-(NSMutableArray*)LabelList

/*============================================================================*/
{
    return m_pLabels;
}
@end
