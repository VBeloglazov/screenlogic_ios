//
//  CustomPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "hlview.h"

@class CHLMSG;
@class CControlDef;

/*============================================================================*/

@interface CCustomPage : UIView   < CNSSocketSink,
                                    CHLView >

/*============================================================================*/
{
    BOOL                            m_bWaitData;
    int                             m_iSysFam;
    int                             m_iID;

    NSMutableArray                  *m_pControls;
    BOOL                            m_bVisible;
    int                             m_iZoneID;
    
    NSString                        *m_pName;
}

-(id)initWithFrame:(CGRect)rFrame SysFam:(int)iSysFam ID:(int)iID Orientation:(UIInterfaceOrientation)ioNow;
-(id)initWithFrame:(CGRect)rFrame MSG:(CNSMSG*)pMSG ZoneID:(int)iZoneID;
-(id)initWithFrame:(CGRect)rFrame ControlList:(NSMutableArray*)pList;

-(void)LoadControlsFromList:(NSMutableArray*)pList ZoneID:(int)iZoneID;
-(void)LoadControls:(CNSMSG*)pMSG ZoneID:(int)iZoneID Flags:(int)iFlags;
-(UIView*)AddControl:(CControlDef*)pDef;

-(void)NotifyRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo;
-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow;

+(void)SetVisible:(UIView*)pView Visible:(BOOL)bVisible;

-(void)SetName:(NSString*)pName;
-(NSString*)GetName;

-(void)PostGetControls:(UIInterfaceOrientation)ioNow;

+(UIImage*)ScaleImage:(UIImage*)pImage Height:(int)iDY;
+(void)AddScaledImage:(NSMutableArray*)pArray Named:(NSString*)pName Height:(int)iDY;
+(void)InitStandardLabel:(UILabel*)pLabel Size:(int)iTextSize;
+(void)InitStandardLabelSize:(UILabel*)pLabel Size:(int)iTextSize;

@end
