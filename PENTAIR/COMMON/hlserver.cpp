/*
--------------------------------------------------------------------------------

    HLSERVER.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlquery.h"
#include "hlsocket.h"
#include "hlsock.h"
#include "hlmsg.h"
#include "hlpos.h"
#include "hlcode.h"
#include "socksink.h"
#include "lock.h"
#include "hlinet.h"
#include "schema.h"
#include "platform.h"

#ifdef GATEWAY
    #include "..\LCORE\LCORE.h"
    #include "..\LCORE\system.h"
#endif

#ifdef CRYSTALPAD
    #include "main.h"
#endif

#include "hlserver.h"

#ifndef GATEWAY
unsigned short              g_ushBasePort = HL_BASE_PORT;
#endif

#ifdef IPHONE
    CHLServer               *g_pHLServer                = NULL;
    int g_iConnectionType           = 0;
#endif

#define INET_BUF_SIZE           2000
#define TIMEOUT_DDX_HOMELOGIC   30000

#define RELAYSERVER_PORT        7576

#ifdef MAC_OS




/*============================================================================*/

    CStatusSink::CStatusSink(
    void                        *pSink )

/*============================================================================*/
{
    m_pNSSink = pSink;
}
#endif
/*============================================================================*/

    void                        CStatusSink::SetStatus(
    CHLString                   sStatus )

/*============================================================================*/
{
#ifdef MAC_OS
    if (m_pNSSink == NULL)
        return;
    NSStatusSinkSetStatus(m_pNSSink, sStatus);
#endif
}
/*============================================================================*/

    BOOL                        CStatusSink::DoWaitProc()

/*============================================================================*/
{
    return TRUE;
}

/*============================================================================*/

    CHLServer::CHLServer()

    #ifdef MULTI_SERVER
    : CSocketSink(NULL)
    #else
    : CSocketSink((short)0)
    #endif

/*============================================================================*/
{
    #ifdef MULTI_SERVER
        m_pTheSocket        = new CHLSocket(this);
        m_shSenderID        = 0;
        g_pHLServer         = this;
    #else
        m_pTheSocket        = new CHLSocket;
    #endif
    m_shNextSenderID    = 1;

    m_bEnableKeepAlive  = TRUE;
    m_bRemoteMode       = FALSE;
    m_bPasswordFail     = FALSE;
    m_bLoggingIn        = FALSE;

    ZeroMemory(m_pGatewayName, 32);

    ZeroMemory(&m_Address, sizeof(SOCKADDR_IN));

    m_bConnect          = FALSE;
    m_iEntry            = 0;

    #ifdef IPHONE
        m_bConnect = TRUE;
        m_bSuspendIO = FALSE;
    #endif

    m_bNotifyConnection = FALSE;

    // Add ourselves as a sink: we are always at index 0.
    AddSocketSink(this);
}
/*============================================================================*/

    CHLServer::~CHLServer()

/*============================================================================*/
{
    // Remove socket.
    delete m_pTheSocket;

    // Remove ourselves from the sink list.
    RemoveSocketSink(this);

    // Shouldn't have any more sinks...
    while (!m_PendingMSG.IsEmpty())
    {
        CHLMSG *pMSG = (CHLMSG*) m_PendingMSG.RemoveHead();
        delete pMSG;
    }
}
/*============================================================================*/

    void                        CHLServer::SetGatewayName(
    char                        *pName )

/*============================================================================*/
{
    int iLen = strlen(pName);
    iLen = __min(31, iLen);
    ZeroMemory(m_pGatewayName, 32);
    memcpy(m_pGatewayName, pName, iLen);
}
/*============================================================================*/

    char                        *CHLServer::GetGatewayName()

/*============================================================================*/
{
    return m_pGatewayName;
}
/*============================================================================*/

    CHLSocket                   *CHLServer::Socket()

/*============================================================================*/
{
    return m_pTheSocket;
}
/*============================================================================*/

    BOOL                        CHLServer::CreateConnection()

/*============================================================================*/
{
    m_bPasswordFail = FALSE;
    m_bConnect = TRUE;

    // Should not be connected.
    if (m_pTheSocket->IsConnected())
    {
        m_pTheSocket->Disconnect();
    }

    return m_pTheSocket->Connect(m_pGatewayName, NULL);
}
/*============================================================================*/

    BOOL                        CHLServer::CreateConnection(
    DWORD                       dwIPAddress,
    WORD                        wPort,
    int                         iConType )

/*============================================================================*/
{
    m_bPasswordFail = FALSE;
    m_bConnect = TRUE;

    // Should not be connected.
    if (m_pTheSocket->IsConnected())
    {
        m_pTheSocket->Disconnect();
    }

    #ifdef MAC_OS
        m_Address.sin_addr.s_addr = dwIPAddress;
    #else
        m_Address.sin_addr.S_un.S_addr = dwIPAddress;
    #endif
    m_Address.sin_port = htons(wPort);
    m_Address.sin_family = PF_INET;

    if (!m_pTheSocket->ConnectAndLogin(&m_Address, FALSE, INVALID_SOCKET, iConType))
        return FALSE;

    CHLMSG msgQ(0, HLM_CHALLENGESTRINGQ);
    CHLMSG *pMSGA = NULL;

    #ifdef MULTI_SERVER
        CHLQuery q(g_pHLServer);
    #else
        CHLQuery q;
    #endif

    if (!q.AskQuestion(&pMSGA, &msgQ))
    {
        m_pTheSocket->Disconnect();
        return FALSE;
    }

    CHLString sChallenge;
#if !defined(HLCONFIG) && defined(UNICODE)
	sChallenge.Type(CHLString::HLSTRING_CHAR);
#endif
    pMSGA->GetString(&sChallenge);

    return DoLogin(sChallenge, m_sRemotePass, iConType, _T("Local Config"));
}
/*============================================================================*/

    void                        CHLServer::StopConnection()

/*============================================================================*/
{
    m_pTheSocket->Disconnect();
    m_bConnect = FALSE;
}
/*============================================================================*/

    BOOL                        CHLServer::CreateCrystalpadConnection(
    CHLString                   sUserName,
    CHLString                   sPassword,
    CStatusSink                 *pStatus,
    int                         iConnType )

/*============================================================================*/
{
    // Remember we've tried at least once
    m_bPasswordFail = FALSE;
    m_bConnect = TRUE;

    // Should not be connected.
    if (m_pTheSocket->IsConnected())
    {
        m_pTheSocket->Disconnect();
    }

    // Do the internet lookup, try regular way first
    CHLString sChallenge;
    SOCKET hRS = INVALID_SOCKET;
    if (!FindGateway(pStatus, sUserName, FALSE, &m_Address, &sChallenge))
    {
        // No Luck in the dump, try the relay server
        if (pStatus != NULL)
        {
            if (!pStatus->DoWaitProc())
                return FALSE;
        }

        hRS = FindRelayServerGateway(pStatus, sUserName, &sChallenge);
        if (hRS == INVALID_SOCKET)
            return FALSE;
    }

    if (iConnType == -1)
    {
        iConnType = CONNECTION_INT();
    }

    CHLString sConnectionType = _T("Service");
    switch (iConnType)
    {
    case CONNECTION_TYPE_CONFIG:        sConnectionType = _T("Configurator");               break;
    case CONNECTION_TYPE_CP_800X480:    sConnectionType = _T("Remote Crystalpad 800x480");  break;
    case CONNECTION_TYPE_CP_800X600:    sConnectionType = _T("Remote Crystalpad 800x600");  break;
    case CONNECTION_TYPE_CP_240X320:    sConnectionType = _T("Remote Crystalpad 240x320");  break;
    case CONNECTION_TYPE_CP_240X240:    sConnectionType = _T("Remote Crystalpad 240x240");  break;

    case CONNECTION_TYPE_IOS:        
    case CONNECTION_TYPE_IOS_3G:
        sConnectionType = _T("iOS");
        break;
    }

    if (CreateConnection(sChallenge, sPassword, iConnType, sConnectionType, pStatus, hRS))
        return TRUE;

    // Failed on connection, if we already tried the relay Server, we're done
    if (hRS != INVALID_SOCKET)
    {
        m_pTheSocket->Disconnect();
        closesocket(hRS);
        return FALSE;
    }

    // Bail from user?
    if (pStatus != NULL)
    {
        if (!pStatus->DoWaitProc())
            return FALSE;
    }

    // No Luck in the dump, try the relay server
    hRS = FindRelayServerGateway(pStatus, sUserName, &sChallenge);
    if (hRS == INVALID_SOCKET)
        return FALSE;

    // Try to create connection through relay server now
    if (CreateConnection(sChallenge, sPassword, iConnType, sConnectionType, pStatus, hRS)) 
        return TRUE;
    
    // Looks like we got a relay server connection but failed somewhere else
    closesocket(hRS);
    return FALSE;
}
#ifdef HLSURVEY
/*============================================================================*/

    BOOL                        CHLServer::CreateCrystalpadConnection(
    BYTE                        byAd1,
    BYTE                        byAd2,
    BYTE                        byAd3,
    BYTE                        byAd4,
    WORD                        wPort,
    CHLString                   sPassword,
    CStatusSink                 *pStatus,
    int                         iConType )

/*============================================================================*/
{
    m_bConnect = TRUE;
    SetAddress(&m_Address, byAd1, byAd2, byAd3, byAd4, wPort);
    return CreateConnection(_T(""), sPassword, iConType, _T("SERVICE"), pStatus, INVALID_SOCKET);
}
#endif
/*============================================================================*/

    BOOL                        CHLServer::CreateServiceConnection(
    CHLString                   sUserName,
    BOOL                        bInternal,
    CStatusSink                 *pStatus )

/*============================================================================*/
{
    // Remember we've tried at least once
    m_bPasswordFail = FALSE;
    m_bConnect = TRUE;

    // Should not be connected.
    if (m_pTheSocket->IsConnected())
    {
        m_pTheSocket->Disconnect();
    }

    // Do the internet lookup
    CHLString sChallenge;
    SOCKET hRS = INVALID_SOCKET;
    if (!FindGateway(pStatus, sUserName, bInternal, &m_Address, &sChallenge))
    {
        hRS = FindRelayServerGateway(pStatus, sUserName, &sChallenge);
        if (hRS == INVALID_SOCKET)
            return FALSE;
    }

    // Try to connect
    if (CreateConnection(sChallenge, SERVICE_KEY, CONNECTION_TYPE_SERVICE, SERVICE_LOGIN, pStatus, hRS))
        return TRUE;

    // Failed
    if (hRS != INVALID_SOCKET)
    {
        closesocket(hRS);
        return FALSE;
    }

    // Haven't try relay server yet, try it first
    hRS = FindRelayServerGateway(pStatus, sUserName, &sChallenge);
    if (hRS == INVALID_SOCKET)
        return FALSE;

    if (CreateConnection(sChallenge, SERVICE_KEY, CONNECTION_TYPE_SERVICE, SERVICE_LOGIN, pStatus, hRS))
        return TRUE;

    // Failed
    closesocket(hRS);
    return FALSE;
}

/*============================================================================*/

    BOOL                        CHLServer::CreateServiceConnection(
    BYTE                        byAd1,
    BYTE                        byAd2,
    BYTE                        byAd3,
    BYTE                        byAd4,
    WORD                        wPort,
    CStatusSink                 *pStatus,
    SOCKET                      hRelayServer )

/*============================================================================*/
{
    m_bConnect = TRUE;
    SetAddress(&m_Address, byAd1, byAd2, byAd3, byAd4, wPort);
    return CreateConnection(_T(""), SERVICE_KEY, CONNECTION_TYPE_SERVICE, SERVICE_LOGIN, pStatus, hRelayServer);
}
/*============================================================================*/

    BOOL                        CHLServer::CreateConnection(
    CHLString                   sChallenge,
    CHLString                   sPassword,
    int                         iConnectionType,
    CHLString                   sLoginName,
    CStatusSink                 *pStatus,
    SOCKET                      hRelayServer )

/*============================================================================*/
{
    m_sPassword = sPassword;
    m_sLoginName = sLoginName;
    m_iConnectionType = iConnectionType;

    CHLString sStatus;

    if (hRelayServer == INVALID_SOCKET)
    {
        BYTE by1, by2, by3, by4;
        WORD wPort;
        GetAddress(&m_Address, &by1, &by2, &by3, &by4, &wPort);

        sStatus.Format(_T("Connecting to System at (%d.%d.%d.%d:%d)"), by1, by2, by3, by4, wPort);
        pStatus->SetStatus(sStatus);

        // Try to make the socket connection to the gateway
        if (!m_pTheSocket->ConnectAndLogin(&m_Address, FALSE, INVALID_SOCKET, iConnectionType, pStatus))
        {
            CHLString sError;
            sError.Format(_T("ERROR: Cannot connect to system at public address (%ld)"), GetLastError());
            pStatus->SetStatus(sError);
            return FALSE;
        }
    }
    else
    {
        if (!m_pTheSocket->ConnectAndLogin(&m_Address, FALSE, hRelayServer, iConnectionType))
        {
            pStatus->SetStatus(_T("ERROR: Logging in through Relay Server"));
            return FALSE;
        }
    }


    pStatus->SetStatus(_T("Connected to system, logging in..."));

    pStatus->SetStatus(_T("Getting Challenge String"));
    CHLMSG msgQ(0, HLM_CHALLENGESTRINGQ);
    CHLMSG *pMSGA = NULL;

    #ifdef MULTI_SERVER
        CHLQuery q(g_pHLServer);
    #else
        CHLQuery q;
        q.SetTimeout(10000);
    #endif

    if (!q.AskQuestion(&pMSGA, &msgQ))
    {
        pStatus->SetStatus(_T("ERROR: Communication error while logging in."));
        m_pTheSocket->Disconnect();
        return FALSE;
    }

    pMSGA->GetString(&sChallenge);

    if (DoLogin(sChallenge, sPassword, iConnectionType, sLoginName))
    {
        pStatus->SetStatus(_T("Connection to system OK"));
        SetConnectionChanged();
        return TRUE;
    }

    pStatus->SetStatus(_T("Connection Actively Refused by Gateway"));
    m_pTheSocket->Disconnect();
    return FALSE;
}
/*============================================================================*/

    BOOL                        CHLServer::DoLogin(
    CHLString                   sChallenge,
    CHLString                   sPassword,
    int                         iConnectionType,
    CHLString                   sLoginName )

/*============================================================================*/
{
    // Make password the key to the encoder/decoder
    #ifndef MFC_CSTRING
        sChallenge.Type(CHLString::HLSTRING_CHAR);
        sPassword.Type(CHLString::HLSTRING_CHAR);
        sLoginName.Type(CHLString::HLSTRING_CHAR);
    #endif

    int iLen = 0;
    char *pPass = CHLCode::MakeBlock(sPassword, &iLen, (BYTE)0);
    CHLCode encoder;
    encoder.MakeKey(pPass, CHLCode::sm_chain0);

    // Encrypt the challenge string with the password
    char *pChallenge = CHLCode::MakeBlock(sChallenge, &iLen, 0);
    char pEncrypted[16];
    encoder.EncryptBlock(pChallenge, pEncrypted);

    // Got a login key, keep going
    CHLMSG msgQ((short)0, HLM_CLIENTLOGINQ, 256);

    msgQ.PutInt(SCHEMA_LATEST);
    msgQ.PutInt(iConnectionType);
    msgQ.PutString(sLoginName);
    msgQ.PutInt(16);
    msgQ.PutData((BYTE*)pEncrypted, 16);
    msgQ.PutInt(PROC_INT);

    #ifdef GATEWAY
        msgQ.PutString(CSystem::Instance()->SystemName());
    #endif

    #if defined(CRYSTALPAD) | defined(HLOSD) | defined(MINIPAD) | defined(HLSTART)
        msgQ.PutString(m_sMachineName);
        msgQ.PutString(m_sUUID);
    #endif

    delete [] pPass;
    delete [] pChallenge;

    #ifdef MULTI_SERVER
        CHLQuery q(g_pHLServer);
    #else
        CHLQuery q;
    #endif

    q.SetTimeout(10000);
    CHLMSG *pMSGA = NULL;

    if (!q.AskQuestion(&pMSGA, &msgQ))
    {
        m_bPasswordFail = TRUE;
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLServer::IsConnected()

/*============================================================================*/
{
    return m_pTheSocket->IsConnected();
}
/*============================================================================*/

    BOOL                        CHLServer::CheckConnection()

/*============================================================================*/
{
    // Try to ping the gateway, should normally return here
    if (m_pTheSocket->IsConnected())
        return TRUE;

    // Try to reconnect
    return m_pTheSocket->Connect(m_pGatewayName, NULL);
}
/*============================================================================*/

    BOOL                        CHLServer::SendMSG(
    CHLMSG                      *pMSG )

/*===========================b=================================================*/
{
    #if defined(CRYSTALPAD) & !defined(MAC_OS)
        START_TIMER(GDI_SOCKET);
    #endif

    // Check.
    if (m_pTheSocket == NULL)
    {
        // Should be checking IsConnected before sending!
        LOGASSERT(FALSE);
        return FALSE;
    }

    if (pMSG == NULL)
        return FALSE;

    //
    // Check the question: this is the one place we centralize checking the
    // questions going out.
    //
    if (!m_pTheSocket->Send(pMSG->MSGSize(), pMSG->MSG()))
        return FALSE;

    #if defined(CRYSTALPAD) & !defined(MAC_OS)
        END_TIMER(GDI_SOCKET);
    #endif

    return TRUE;
}
/*============================================================================*/

    short                       CHLServer::GenerateSenderID()

/*============================================================================*/
{
    if (m_shNextSenderID == HLM_ALL_SENDERS_ID)
        m_shNextSenderID = 1;
    else
        m_shNextSenderID++;

    return m_shNextSenderID;
}
/*============================================================================*/

    BOOL                        CHLServer::WaitingResponse()

/*============================================================================*/
{
    CHLPos *pPos = m_Sinks.GetHeadPosition();
    while (pPos != NULL)
    {
        CSocketSink *pSink = (CSocketSink*)pPos->Object();
        pPos = pPos->Next();

        if (pSink->WaitingResponse())
            return TRUE;
    }


    return FALSE;
}
/*============================================================================*/

    CSocketSink                 *CHLServer::FindSender(
    short                       shSenderID )

/*============================================================================*/
{
    CHLPos *pPos = m_Sinks.GetHeadPosition();
    while (pPos != NULL)
    {
        CSocketSink *pSink = (CSocketSink*)pPos->Object();
        pPos = pPos->Next();

        if (pSink->SenderID() == shSenderID)
            return pSink;
    }


    return NULL;
}
#ifdef MAC_OS
/*============================================================================*/

    CSocketSink                 *CHLServer::FindSink(
    void                        *pNSSink )

/*============================================================================*/
{
    CHLPos *pPos = m_Sinks.GetHeadPosition();
    while (pPos != NULL)
    {
        CSocketSink *pSink = (CSocketSink*)pPos->Object();
        pPos = pPos->Next();

        if (pSink->NSSink() == pNSSink)
            return pSink;
    }


    return NULL;
}
#endif
/*============================================================================*/

    BOOL                        CHLServer::AddSocketSink(
    CSocketSink                 *pSink  )

/*
NOTES:  This adds someone to our list of sinks.  If someone is already in
        the list it ASSERTS.
*/
/*============================================================================*/
{
    // Already in the list?
    CHLPos *pPos = m_Sinks.GetHeadPosition();
    while (pPos != NULL)
    {
        CSocketSink *pTest = (CSocketSink*)pPos->Object();
        pPos = pPos->Next();

        if (pTest == pSink)
        {
            #ifndef GATEWAY
                LOGASSERT(FALSE);
            #endif
            return FALSE;
        }
    }

    m_Sinks.AddTail(pSink);
    return TRUE;
}
/*============================================================================*/

    void                        CHLServer::RemoveSocketSink(
    CSocketSink                  *pSink )

/*============================================================================*/
{
    // Already in the list?
    m_Sinks.Remove(pSink);
}
/*============================================================================*/

    void                        CHLServer::RemoveAllSocketSinks()

/*============================================================================*/
{
    m_Sinks.RemoveAll();
}
/*============================================================================*/

    BOOL                        CHLServer::ReadyToSend()

/*============================================================================*/
{
    if (m_pTheSocket == NULL)
        return FALSE;
    return m_pTheSocket->IsReadyToSend();
}
/*============================================================================*/

    BOOL                        CHLServer::PumpSocketMessages(
    DWORD                       dwMaxWaitForData )

/*============================================================================*/
{
    return PumpSocketMessages(NULL, HLM_ALL_SENDERS_ID, dwMaxWaitForData );
}
/*============================================================================*/

    int                         CHLServer::PumpSocketMessages(
    CStatusSink                 *pSink,
    short                       shSenderID,
    DWORD                       dwMaxWaitForData )

/*
NOTES:  Return TRUE to indicate that there is activity on the sockets.  This
        can be used to prevent sleeping on this thread, and thereby improve
        the speed with which we read messages.
*/
/*============================================================================*/
{
    dwMaxWaitForData = 100;
    
    #if defined(CRYSTALPAD) & !defined(MAC_OS)
        START_TIMER(GDI_SOCKET);
    #endif

    #ifdef DEBUG_SOCKET
    CMessageTracker *pTracker = m_pTheSocket->MessageTracker();
    #endif

    // Delayed processing
    CHLPos *pPos = m_PendingMSG.GetHeadPosition();
    while (pPos != NULL)
    {
        #ifdef DEBUG_SOCKET
        if (pTracker != NULL)
            pTracker->ProcessMessage(_T("Process Pending..."));
        #endif

        CHLPos *pPosThis = pPos;
        CHLMSG *pMSG = (CHLMSG*) pPos->Object();
        pPos = pPos->Next();

        BOOL bOKToProc = FALSE;
        if (shSenderID == HLM_ALL_SENDERS_ID)
            bOKToProc = TRUE;
        if (pMSG->SenderID() == shSenderID)
            bOKToProc = TRUE;

        if (bOKToProc)
        {
            // OK to process this one
            m_PendingMSG.RemoveAt(pPosThis);
            ProcessMessage(pMSG);
            delete pMSG;
        }
    }

    //
    // See if the socket is connected.
    //
    if (!m_pTheSocket->IsConnected())
    {
        while (!m_PendingMSG.IsEmpty())
        {
            CHLMSG *pPending = (CHLMSG*)m_PendingMSG.RemoveHead();
            delete pPending;
        }

        if (m_bNotifyConnection)
        {
            if ((shSenderID == 0) || (shSenderID == HLM_ALL_SENDERS_ID))
            {
                SendConnectionChanged(FALSE);
                m_bNotifyConnection = FALSE;
            }
        }

        // Not Connected
        if ((shSenderID != 0) && (shSenderID != HLM_ALL_SENDERS_ID))
        {
            // Don't connect in a query, its not clean
            return RECEIVE_ERROR;
        }

#ifdef MAC_OS
        if (m_Address.sin_addr.s_addr != 0)
#else
        if (m_Address.sin_addr.S_un.S_addr != 0)
#endif
        {
            if (m_bRemoteMode && !m_bConnect)
            {
                return RECEIVE_ERROR;
            }

            if (m_pTheSocket->ConnectAndLogin(&m_Address, !m_bRemoteMode))
            {
                if (m_bRemoteMode)
                {
                    CHLMSG msgQ(0, HLM_CHALLENGESTRINGQ);
                    CHLMSG *pMSGA = NULL;

                    #ifdef MULTI_SERVER
                        CHLQuery q(g_pHLServer);
                    #else
                        CHLQuery q;
                    #endif
                    if (!q.AskQuestion(&pMSGA, &msgQ))
                        return FALSE;

                    CHLString sChallenge;
                    if (!pMSGA->GetString(&sChallenge))
                        return FALSE;

                    if (!DoLogin(sChallenge, m_sPassword, m_iConnectionType, m_sLoginName))
                        return FALSE;
                }

                SetConnectionChanged();
            }
        }
        else
        {
            // Try to connect: if OK, then continue.
            if (!m_bConnect)
                return RECEIVE_IDLE;

            if (!m_pTheSocket->Connect(m_pGatewayName, pSink))
                return RECEIVE_ERROR;
        }
    }
    else
    {
        m_pTheSocket->CheckTX();

        if (m_bEnableKeepAlive && shSenderID == HLM_ALL_SENDERS_ID)
        {
            // The socket thinks its connected, check how long its been since a ping
            if (m_pTheSocket->KeepAliveTimeout())
            {
                // Too long, kill the connection
                m_pTheSocket->Disconnect();
                return RECEIVE_ERROR;
            }
        }
    }

    if (m_bNotifyConnection)
    {
        if ((shSenderID == 0) || (shSenderID == HLM_ALL_SENDERS_ID))
        {
            SendConnectionChanged(m_pTheSocket->IsConnected());
            m_bNotifyConnection = FALSE;
        }
    }

    //
    // See if there is data there.
    //
    // CHLSocket has intelligence to return one complete message at a time,
    // so we should not have to worry about messages getting here that are
    // incomplete or otherwise split.
    //
    CHLMSG *pRX = m_pTheSocket->WaitMessage(dwMaxWaitForData);

    if (pRX != NULL)
    {
        BOOL bProcOK = TRUE;
        if (shSenderID != HLM_ALL_SENDERS_ID)
        {
            // Check if we're waiting on specific message
            if (pRX->SenderID() != shSenderID)
                bProcOK = FALSE;
        }

        if (bProcOK)
        {
            // OK to process now
            ProcessMessage(pRX);
            delete pRX;
        }
        else
        {
            if (m_PendingMSG.GetCount() < 512)
            {
                // Do it later
                m_PendingMSG.AddTail(pRX);
            }
            else
            {
                LOGASSERT(FALSE);
                while (!m_PendingMSG.IsEmpty())
                {
                    CHLMSG *pPending = (CHLMSG*)m_PendingMSG.RemoveHead();
                    delete pPending;
                }
            }
        }

        #if defined(CRYSTALPAD) & !defined(MAC_OS)
            END_TIMER(GDI_SOCKET);
        #endif
        return RECEIVE_BUSY;
    }

    #if defined(CRYSTALPAD) & !defined(MAC_OS)
        END_TIMER(GDI_SOCKET);
    #endif
    return RECEIVE_IDLE;
}

/*============================================================================*/

    void                        CHLServer::ProcessMessage(
    CHLMSG                      *pMSG )

/*============================================================================*/
{
    // See who this is for.
    short shSenderID = pMSG->SenderID();

    if ((shSenderID < 0))
    {
        // Message has come in for a sink that is invalid, or no longer around.
        LOGASSERT(FALSE);
    }
    else if (HLM_ALL_SENDERS_ID == shSenderID)
    {
        // Send to all sinks...     
        SendSinkMessage(pMSG);
    }
    else
    {
        // Pass it on. It is possible that the receiver has gone away: just ignore.
        CSocketSink *pSink = FindSender(shSenderID);
        if (pSink != NULL)
        {
            pSink->SinkMessage(pMSG);
        }
    }
}

/*============================================================================*/

    void                        CHLServer::SetConnectionChanged()

/*============================================================================*/
{
    m_bNotifyConnection = TRUE;
}
/*============================================================================*/

    void                        CHLServer::SendSinkMessage(
    CHLMSG                      *pMSG )

/*============================================================================*/
{
    // Check recursion, bad because we only have one CHLMSG input buffer
    static int iBusy = 0;
    iBusy++;
    LOGASSERT(iBusy == 1);

    // Pass on to clients.
    CHLPos *pPos = m_Sinks.GetHeadPosition();
    int iCount = 0;
    while (pPos != NULL)
    {
        CSocketSink *pSink = (CSocketSink*) m_Sinks.GetNext(pPos);
        LOGASSERT(pSink != NULL);
        pSink->SinkMessage(pMSG);

        // Check, make sure pPos is still in the list
        iCount++;
        if (m_Sinks.FindIndex(iCount) != pPos)
        {
            // pPos has been removed, get a new one
            pPos = m_Sinks.FindIndex(iCount);
        }
    }

    iBusy--;
}
/*============================================================================*/

    void                        CHLServer::SendConnectionChanged(
    BOOL                        bNowConnected )

/*============================================================================*/
{
    // Pass on to clients.
    CHLPos *pPos = m_Sinks.GetHeadPosition();
    int iCount = 0;
    while (pPos != NULL)
    {
        CSocketSink *pSink = (CSocketSink*) m_Sinks.GetNext(pPos);
        LOGASSERT(pSink != NULL);
        
        if(pSink != NULL)
            pSink->ConnectionChanged(bNowConnected);

        // Check, make sure pPos is still in the list
        iCount++;
        if (m_Sinks.FindIndex(iCount) != pPos)
        {
            // pPos has been removed, get a new one
            pPos = m_Sinks.FindIndex(iCount);
        }
    }
}
/*============================================================================*/

    BOOL                        CHLServer::FindGateway(
    CStatusSink                 *pStatus,
    CHLString                   sUserName,
    BOOL                        bInternal,
    SOCKADDR_IN                 *pAddress,
    CHLString                   *pChallenge )

/*============================================================================*/
{
    pStatus->SetStatus(_T("Connecting to Address Server..."));

    CHLString sIP;
    CHLString sPort;

    BOOL bLookupOK = FALSE;

#ifdef GATEWAY
    if (DoLookupServerSoft(sUserName, &sIP, &sPort, pStatus))
        bLookupOK = TRUE;
#endif

    if (!bLookupOK)
    {
        SOCKET hSocket = ConnectHomeLogic();
        if (hSocket == INVALID_SOCKET)
        {
            pStatus->SetStatus(_T("ERROR: Cannot connect to Address Server"));
            return FALSE;
        }

        pStatus->SetStatus(_T("Connected to Address Server OK"));

        if (!DoLookupPost(hSocket, sUserName, bInternal, &sIP, &sPort, pChallenge, pStatus))
        {
            pStatus->SetStatus(_T("Done"));
            CloseSocket(hSocket);
            return FALSE;
        }

        CloseSocket(hSocket);
    }

    CHLString sStatus;
    sStatus.Format(_T("Found system at: %s:%s Challenge: %s"), (const TCHAR*)sIP, (const TCHAR*)sPort, (const TCHAR*)*pChallenge);
    pStatus->SetStatus(sStatus);

    #ifdef UNDER_CE
        BYTE by1 = (BYTE)_wtoi(GetParam('.', &sIP));
        BYTE by2 = (BYTE)_wtoi(GetParam('.', &sIP));
        BYTE by3 = (BYTE)_wtoi(GetParam('.', &sIP));
        BYTE by4 = (BYTE)_wtoi(GetParam('.', &sIP));
        WORD uPort = (WORD)_wtol(sPort);
    #else
#if defined(HLCONFIG) && defined(UNICODE)
        BYTE by1 = _wtoi(GetParam('.', &sIP));
        BYTE by2 = _wtoi(GetParam('.', &sIP));
        BYTE by3 = _wtoi(GetParam('.', &sIP));
        BYTE by4 = _wtoi(GetParam('.', &sIP));
        WORD uPort = (WORD)_wtol(sPort);
#else
        BYTE by1 = atoi(GetParam('.', &sIP));
        BYTE by2 = atoi(GetParam('.', &sIP));
        BYTE by3 = atoi(GetParam('.', &sIP));
        BYTE by4 = atoi(GetParam('.', &sIP));
        WORD uPort = (WORD)atol(sPort);
#endif
    #endif


    SetAddress(pAddress, by1, by2, by3, by4, uPort);

    pStatus->SetStatus(_T("Found system OK"));

    return TRUE;
}
#ifdef GATEWAY
/*============================================================================*/

    BOOL                        CHLServer::DoLookupServerSoft(
    CHLString                   sUserName,
    CHLString                   *pIP,
    CHLString                   *pPort,
    CStatusSink                 *pStatus )

/*============================================================================*/
{
    CGatewayLookupServerSoft LookUp(sUserName, pIP, pPort, pStatus);
    return LookUp.WaitResponse();
}
#endif
/*============================================================================*/

    SOCKET                      CHLServer::FindRelayServerGateway(
    CStatusSink                 *pStatus,
    CHLString                   sUserName,
    CHLString                   *pChallenge )

/*============================================================================*/
{
    #if defined(UNDER_CE) && defined(HLUTIL)
        return INVALID_SOCKET;
    #else
        pStatus->SetStatus(_T("Connecting to Relay Server..."));
        SOCKET hSocket = ConnectHomeLogic(RELAYSERVER_PORT);

        if (hSocket == INVALID_SOCKET)
        {
            pStatus->SetStatus(_T("ERROR: Cannot connect to RelayServer"));
            return FALSE;

        }

        pStatus->SetStatus(_T("Connected to RelayServer, Getting Challenge"));
        char cBuf[38];
        memset(cBuf, 0, 38);  
        int iLen = __min(31, sUserName.GetLength());

#if defined(MFC_CSTRING)
        #ifdef UNICODE
            TCHAR *pNameTCHAR = sUserName.GetBuffer(iLen);
		    char pName[64];
            wcstombs((char*) pName, (wchar_t*)pNameTCHAR, iLen);
        #else
            char *pName = sUserName.GetBuffer(iLen);
        #endif
#else
        char *pName = (char*)sUserName.GetBufferCHAR(iLen);
#endif

        memcpy(cBuf, pName, iLen);
        int iPort = 443;
        memcpy(cBuf+32, &iPort, sizeof(int));
        if (send(hSocket, cBuf, 38, 0) != 38)
        {
            pStatus->SetStatus(_T("ERROR: Can't send connection message"));
            closesocket(hSocket);
            return INVALID_SOCKET;
        }

        pStatus->SetStatus(_T("Waiting for response from relay server..."));

        // OK, now were waiting for 32 bytes (Challenge string)
        CHLTimer tmWait;
        BOOL bError = FALSE;
        while (tmWait.HLTickCount() < 10000 && !bError)
        {
            if (pStatus != NULL)
            {
                if (!pStatus->DoWaitProc())
                    bError = TRUE;
            }

            ULONG lQue = 0;
            if (ioctlsocket(hSocket, FIONREAD, &lQue) == SOCKET_ERROR)
            {
                bError = TRUE;
            }
            else
            {
                // Did we get something?
                if (lQue >= 32)
                {
                    memset(cBuf, 0, 32);
                    if (recv(hSocket, cBuf, 32, 0) != 32)
                        bError = TRUE;
                    else
                    {
                        cBuf[31] = 0;
                        int iLen = strlen(cBuf);
#if defined(MFC_CSTRING)
    #ifdef UNICODE
                        TCHAR *pOut = pChallenge->GetBuffer(iLen);
		                mbstowcs((wchar_t*) pOut, (char*)cBuf, iLen);
                        pChallenge->ReleaseBuffer(iLen);
    #else
                        char *pOut = (char*)pChallenge->GetBufferCHAR(iLen);
                        memcpy(pOut, cBuf, iLen);
                        pChallenge->ReleaseBuffer(iLen);
    #endif
#else
                        char *pOut = (char*)pChallenge->GetBufferCHAR(iLen);
                        memcpy(pOut, cBuf, iLen);
                        pChallenge->ReleaseBuffer(iLen);
#endif
                
                        // All good
                        return hSocket;
                    }    
                }
            }

            HLSleep(25);
        }

        pStatus->SetStatus(_T("ERROR: Could not connect to system via Relay Server"));
        closesocket(hSocket);
        return INVALID_SOCKET;
    #endif
}

/*============================================================================*/

    BOOL                        CHLServer::DoLookupPost(
    SOCKET                      hSocket,
    CHLString                   sUserName,
    BOOL                        bInternal,
    CHLString                   *pIP,
    CHLString                   *pPort,
    CHLString                   *pChallenge,
    CStatusSink                 *pStatus )

/*============================================================================*/
{
    CHLString sParams;
    sParams.Format(_T("USERNAME=%s"), (const TCHAR*)sUserName);
    CHLString sPost;
    int iParamSize = sParams.GetLength();
    sPost.Format(_T("POST /cgi-bin/getaddr.pl HTTP/1.0\r\nUser-Agent: HomeLogic\r\nHost: homelogic.com\r\nContent-Length: %d\r\nCache-Control: no-cache\r\n\r\n"), iParamSize);

	//sParams.ConvertTochar();
	//sPost.ConvertTochar();

    // Send the post
    #ifdef HLUTIL
        if (!SendString(hSocket, sPost))
            return FALSE;
        if (!SendString(hSocket, sParams))
            return FALSE;
    #else
        if (!SendString(hSocket, (char*)sPost))
            return FALSE;
        if (!SendString(hSocket, (char*)sParams))
            return FALSE;
    #endif

    pStatus->SetStatus(_T("Waiting from response from Address Server..."));

    // Make socket async
    SetNonBlocking(hSocket);

    // Loop till the host shuts down or timeout
    BOOL bDone      = FALSE;
    DWORD lQue      = 0;
    int iRead       = 0;

    char pReadBuf[INET_BUF_SIZE];
    ZeroMemory(pReadBuf, INET_BUF_SIZE);

    CHLTimer hltWait;
    while ((hltWait.HLTickCount() < TIMEOUT_DDX_HOMELOGIC) && !bDone)
    {
        if (pStatus != NULL)
        {
            if (!pStatus->DoWaitProc())
                return FALSE;
        }

        if (WaitSocketReadState(hSocket, 25))
        {
            if (lQue > (UINT)(INET_BUF_SIZE- iRead))
            {
                // Too much to read, will cause buf overflow, so bail on this one
                pStatus->SetStatus(_T("Error: Overflow"));
                return FALSE;
            }

            // Try to read
            int iToRead = INET_BUF_SIZE-iRead;
            int iReadNow = recv(hSocket, (char*)(pReadBuf+iRead), iToRead, 0);
            if (iReadNow <= 0)
            {
                pStatus->SetStatus(_T("Error: while recv()"));
                return FALSE;
            }

            iRead += iReadNow;

            if (strstr(pReadBuf, "\r\n\r\n"))
                bDone = TRUE;
        }
        else
        {
            // Nothing to read, see if they shut down
            int iTest = recv(hSocket, (pReadBuf+iRead), 1, MSG_PEEK);
            if (iTest == 0)
            {
                // Graceful shutdown, normal case
                bDone = TRUE;
            }
            else if (iTest > 0)
                iRead += iTest;
        }

        HLSleep(10);
    }

    CHLString sDebug;

    if (!bDone)
    {
        sDebug.Format(_T("TimeOut Got %d Bytes"), iRead);
        pStatus->SetStatus(sDebug);
    }

    if (!strstr(pReadBuf, "200 OK"))
    {
        pStatus->SetStatus(_T("Error, Returned:"));
        int iLen = __min(strlen(pReadBuf), 40);

        #ifdef HLUTIL
            TCHAR *pBuf = sDebug.GetBuffer(iLen);
        #else
            char *pBuf = sDebug.GetBufferCHAR(iLen);
        #endif

        memcpy(pBuf, pReadBuf, iLen);
        sDebug.ReleaseBuffer(iLen);
        pStatus->SetStatus(sDebug);
        return FALSE;
    }

    char *pStart = pReadBuf + iRead - 10;
    BOOL bStart = FALSE;
    while (pStart > pReadBuf && !bStart)
    {
        if (pStart[0] == '\r' && pStart[1] == '\n')
            bStart = TRUE;
        pStart--;
    }

    if (!bStart)
    {
        pStatus->SetStatus(_T("Error: Cant find data"));
        return FALSE;
    }

    pStart += 3;

    int iLen = (int)(iRead - (pStart - pReadBuf));
    CHLString sLine;

    #ifndef MFC_CSTRING
        // Force to char
        sLine.Type(CHLString::HLSTRING_CHAR);
    #endif
#if defined(HLCONFIG) && defined(UNICODE)
    wchar_t *pBuf = (wchar_t*)sLine.GetBuffer(iLen);
	for(int i = 0; i < iLen; i++)
	{
		pBuf[i] = (wchar_t)pStart[i];
	}
    //wmemcpy(pBuf, (const wchar_t*)pStart, iLen);
    wchar_t *pEnd = wcsstr(pBuf, _T("\r\n"));
#else
    char *pBuf = (char*)sLine.GetBufferCHAR(iLen);
    memcpy(pBuf, pStart, iLen);
    char *pEnd = strstr(pBuf, "\r\n");
#endif
    if (pEnd != NULL)
    {
        iLen = (int)(pEnd - pBuf);
    }
    sLine.ReleaseBuffer(iLen);

    #ifdef CRYSTALPAD
        sLine.TrimRight("\n");
        sLine.TrimRight("\n");
        sLine.TrimLeft("\n");
        sLine.TrimLeft("\n");
    #else
        sLine.TrimRight(_T("\n"));
        sLine.TrimRight(_T("\n"));
        sLine.TrimLeft(_T("\n"));
        sLine.TrimLeft(_T("\n"));
    #endif

    CHLString sTimeRel    = GetParam(_T('|'), &sLine);
    CHLString sName       = GetParam(_T('|'), &sLine);
    CHLString sTimeAbs    = GetParam(_T('|'), &sLine);
    CHLString sAddressExt = GetParam(_T('|'), &sLine);
    CHLString sPortExt    = GetParam(_T('|'), &sLine);
    CHLString sAddressInt = GetParam(_T('|'), &sLine);
    CHLString sPortInt    = GetParam(_T('|'), &sLine);
    CHLString sChallenge  = GetParam(_T('|'), &sLine);

    #ifdef HLUTIL
        CString sNameT = sName;
    #else
        CHLString sNameT = (TCHAR*)sName;
    #endif

    if (sNameT.Compare(sUserName))
    {
        pStatus->SetStatus(_T("Public Port Closed"));
        return FALSE;
    }

    if (bInternal)
    {
        *pIP    = sAddressInt;
        *pPort  = sPortInt;
    }
    else
    {
        *pIP    = sAddressExt;
        *pPort  = sPortExt;
    }

    #ifndef UNICODE
        long lTimeRel = atol(sTimeRel);

        if (lTimeRel > 1000)
        {
            // Challenge is too old
            sChallenge = _T("");
        }
    #endif
        

    *pChallenge = sChallenge;

    return TRUE;
}
/*============================================================================*/

    CHLString                     CHLServer::GetParam(
    TCHAR                         cDelimiter,
    CHLString                     *pLine )

/*============================================================================*/
{
    CHLString sTest = *pLine;


    #ifndef MFC_CSTRING
        sTest.Type(CHLString::HLSTRING_CHAR);
    #endif

    int iPos = sTest.Find(cDelimiter, 0);
    if (iPos == -1)
        return *pLine;


    CHLString sRet = pLine->Left(iPos);
    CHLString sRight = pLine->Right(pLine->GetLength() - iPos - 1);

    *pLine = sRight;
    return sRet;
}
#ifdef GATEWAY
/*============================================================================*/

    CGatewayLookupServerSoft::CGatewayLookupServerSoft(
    CHLString                   sUserName,
    CHLString                   *pIP,
    CHLString                   *pPort,
    CStatusSink                 *pSink )

/*============================================================================*/
{
    m_pIP       = pIP;
    m_pPort     = pPort;
    m_pSink     = pSink;
    m_iStatus   = -1;

    CHLMSG  *pMsgQ = new CHLMSG( 0, HLM_SERVICE_GDISCOVERY_GETGATEWAYDATA_Q );
    pMsgQ->PutString( sUserName             );  // Gateway Name
    pMsgQ->PutString( _HLTEXT("Service")    );  // Login Name
    CallServersoft(pMsgQ);
}
/*============================================================================*/

    void                            CGatewayLookupServerSoft::ProcessServersoftResponse(
    CHLMSG                          *pMsgA )

/*============================================================================*/
{
    short shMessageID = pMsgA->MessageID();

    switch( shMessageID )
    {
    case HLM_SERVICE_GDISCOVERY_GETGATEWAYDATA_A:
        {
            BOOLEAN   bGatewayFound; pMsgA->GetBYTE  ( &bGatewayFound );
            BOOLEAN   bLicenseOK;    pMsgA->GetBYTE  ( &bLicenseOK    );
            CHLString sAddress;      pMsgA->GetString( m_pIP          );
            WORD      wPort;         pMsgA->GetWORD  ( &wPort         );
            BOOLEAN   bPortOpen;     pMsgA->GetBYTE  ( &bPortOpen     );
            BOOLEAN   bRelayOn;      pMsgA->GetBYTE  ( &bRelayOn      );

            m_pPort->Format(_T("%d"), wPort);

            if ( !bGatewayFound )
            {
                m_iStatus = 0;
                return;
            }

            m_iStatus = 1;
        }
        break;

    case HLM_SERVERSOFT_ERROR:
        {
            CHLString sError;   pMsgA->GetString( &sError );
            m_iStatus = 0;
            return;
        }
        break;

    default:
        break;
    }
}
/*============================================================================*/

    void                            CGatewayLookupServerSoft::ProcessServersoftNoResponse(
    WORD                            shSenderID )

/*============================================================================*/
{
    m_iStatus = 0;
}
/*============================================================================*/

    BOOL                            CGatewayLookupServerSoft::WaitResponse()

/*============================================================================*/
{
    CHLTimer tm;
    while (tm.HLTickCount() < 120000)
    {
        if (m_iStatus == 0)
            return FALSE;
        if (m_iStatus == 1)
            return TRUE;
        Sleep(100);
    }

    return FALSE;
}
#endif