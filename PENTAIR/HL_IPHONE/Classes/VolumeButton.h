//
//  VolumeButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlbutton.h"

@class CAudioZone;

/*============================================================================*/

@interface CVolumeButton : CToolBarButton

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
}

-(id)initWithZone:(CAudioZone*)pZone ImageName:(NSString*)pImageName;

@end
