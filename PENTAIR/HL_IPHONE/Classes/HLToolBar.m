//
//  HLToolBar.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 3/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HLToolBar.h"
#import "IPOD_ViewController.h"
#import "crect.h"
#import "hlcomm.h"
#import "hlm.h"
#import "shader.h"
#import "AppDelegate.h"
#import "IPAD_ViewController.h"
#import <QuartzCore/QuartzCore.h>


@implementation CHLSegmentHighlight

/*============================================================================*/

-(id)InitWithFrame:(CGRect)frame Color1:(int)iIndex1 Color2:(int)iIndex2

/*============================================================================*/
{
    if (self == [super initWithFrame:frame])
    {
        m_iColor1 = iIndex1;
        m_iColor2 = iIndex2;
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rect

/*============================================================================*/
{
    if (self == [super initWithFrame:rect])
    {
        m_iColor1 = -1;
        m_iColor2 = -1;
        m_fR = 0.2;
        m_fG = 0.2;
        m_fB = 1.0;
    }

    return self;
}
/*============================================================================*/

-(void)SetRadiusLeft:(int)iRL Right:(int)iRR

/*============================================================================*/
{
    if (iRL != m_iRUL || iRR != m_iRUR)
    {
        [self setNeedsDisplay];
    }

    m_iRUL = iRL;
    m_iRLL = iRL;
    m_iRUR = iRR;
    m_iRLR = iRR;
}
/*============================================================================*/

-(void)SetRadiusUL:(int)iRUL UR:(int)iRUR LL:(int)iRLL LR:(int)iRLR

/*============================================================================*/
{
    if (iRUL != m_iRUL || iRUR != m_iRUR)
    {
        [self setNeedsDisplay];
    }

    m_iRUL = iRUL;
    m_iRLL = iRLL;
    m_iRUR = iRUR;
    m_iRLR = iRLR;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{

    CGContextRef context = UIGraphicsGetCurrentContext();
    int iRad = 10;
    CGPoint ptStart = CGPointMake(rect.origin.x + iRad, rect.origin.y);
    CGPoint ptEnd = ptStart;

    ptEnd.y += rect.size.height;


	CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient;

    if (m_iColor1 < 0 || m_iColor2 < 0)
    {
        CGFloat colors[] =
        {
              m_fR,       m_fG,     m_fB,     0.55,
              m_fR,       m_fG,     m_fB,     0.25,
        };

        gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    }
    else 
    {
        CGFloat colors[8];
        [CHLComm LoadGradientTo:colors G1:m_iColor1 G2:m_iColor2];
        gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    }

    CGColorSpaceRelease(rgb);


    int iX = rect.origin.x;
    int iY = rect.origin.y;
    int iDX = rect.size.width;
    int iDY = rect.size.height;

    CGContextBeginPath (context);
    CGContextMoveToPoint(context, iX+m_iRUL, iY);                                         // Start Point
    CGContextAddArcToPoint(context, iX+iDX, iY, iX+iDX, iY+iDY, m_iRUR);                  // Top Line-UR
    CGContextAddArcToPoint(context, iX+iDX, iY+iDY, iX+iDX-m_iRLR, iY+iDY, m_iRLR);         // Right Line-LR
    CGContextAddArcToPoint(context, iX,     iY+iDY, iX, iY+iDY-m_iRLL, m_iRLL);             // Bottom Line-LL
    CGContextAddArcToPoint(context, iX, iY, iX+m_iRUL, iY, m_iRUL);                         // Bottom Line-LL

	CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, ptStart, ptEnd, 0);
    CGGradientRelease(gradient);
}
/*============================================================================*/

-(void)SetColorR:(float)fR G:(float)fG B:(float)fB

/*============================================================================*/
{
    m_fR = fR;
    m_fG = fG;
    m_fB = fB;
}
@end

@implementation CToolBarHighlight

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame UpperRad:(int)iR1 LowerRad:(int)iR2

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        self.contentMode = UIViewContentModeRedraw;
        [self setOpaque:NO];
        [self setClearsContextBeforeDrawing:YES];
        m_iR1 = iR1;
        m_iR2 = iR2;
        [self setUserInteractionEnabled:NO];
        
    }
    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        self.contentMode = UIViewContentModeRedraw;
        [self setOpaque:NO];
        [self setClearsContextBeforeDrawing:YES];
        m_iR1 = 0;
        m_iR2 = 10;
        [self setUserInteractionEnabled:NO];
        
    }
    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint ptStart = CGPointMake(rect.origin.x, rect.origin.y);
    CGPoint ptEnd = ptStart;

    ptEnd.y += rect.size.height;


    int iX = rect.origin.x;
    int iY = rect.origin.y;
    int iDX = rect.size.width;
    int iDY = rect.size.height;

    CGContextBeginPath (context);
    CGContextMoveToPoint(context, iX+m_iR1, iY);                                         // Start Point
    CGContextAddArcToPoint(context, iX+iDX, iY, iX+iDX, iY+iDY, m_iR1);                  // Top Line-UR
    CGContextAddArcToPoint(context, iX+iDX, iY+iDY, iX+iDX-m_iR2, iY+iDY, m_iR2);         // Right Line-LR
    CGContextAddArcToPoint(context, iX,     iY+iDY, iX, iY+iDY-m_iR2, m_iR2);             // Bottom Line-LL
    CGContextAddArcToPoint(context, iX, iY, iX+m_iR1, iY, m_iR1);                         // Bottom Line-LL

    int iA1 = [CHLComm GetInt:INT_ALPHAMAX];
    int iA2 = iA1 * [CHLComm GetInt:INT_ALPHA_UPPER_RATIO] / 100;
    float fA1 = (float)iA1 / 255.0f;
    float fA2 = (float)iA2 / 255.0f;


	CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    CGFloat colors[] =
    {
          1.0,       1.0,     1.0,     fA1,
          1.0,       1.0,     1.0,     fA2,
    };

    CGGradientRef gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    CGColorSpaceRelease(rgb);

	CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, ptStart, ptEnd, 0);
    CGGradientRelease(gradient);
}
@end


@implementation CToolBarBackground
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Orientation:(int)iOrientation

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        self.contentMode = UIViewContentModeScaleToFill;
        m_iOrientation = iOrientation;
        if (rFrame.size.width > 0 && rFrame.size.height > 0)
        {
            [self displayLayer:self.layer];
        }
        else
        {
            self.contentMode = UIViewContentModeRedraw;
        }
    }
    return self;
}
/*============================================================================*/

-(void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CGImageRef pRef = [pDelegate GetToolBarImage:self.bounds.size.height Orientation:m_iOrientation State:ELEMENT_STD FrameStyle:TOOLBAR_FRAME_FULL];
    [layer setContents:(id)pRef];
    double d1PixelX = 5.0 / 50.0;
    double d1PixelY = 1.0 / self.bounds.size.height;
    CGRect rStretch = CGRectMake(d1PixelX, d1PixelY, 1.0 - 2 * d1PixelX, 1.0 - 2 * d1PixelY);
    [self setContentStretch:rStretch];
}
/*============================================================================*/

-(void)Invalidate

/*============================================================================*/
{
    if (self.bounds.size.width > 0 && self.bounds.size.height > 0)
    {
        [self displayLayer:self.layer];
    }
}
@end

@implementation CToolBarClientView
/*============================================================================*/

-(UIView*)InsetLeft     { return NULL;  }
-(UIView*)InsetRight    { return NULL;  }
-(BOOL)IsSystemMenu     { return FALSE; }

/*============================================================================*/

@end

@implementation CHLToolBar

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Orientation:(int)iOrientation

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        CGRect rThis = self.bounds;
        m_pBG = [[CToolBarBackground alloc] initWithFrame:rThis Orientation:iOrientation];
        [self addSubview:m_pBG];
        [m_pBG release];
        
        int iA1 = [CHLComm GetInt:INT_ALPHAMAX];
        if (iA1 > 0)
        {
            m_pHL = [[CToolBarHighlight alloc] initWithFrame:CGRectZero];
            [m_pHL setOpaque:NO];
            [m_pHL setBackgroundColor:[UIColor clearColor]];
            [self addSubview:m_pHL];
            [m_pHL release];
        }
    }

    return self;
}
/*============================================================================*/

-(void)SetBackgroundAlpha:(double)dAlpha

/*============================================================================*/
{
    [m_pBG setAlpha:dAlpha];
    [m_pHL setAlpha:dAlpha];
}
/*============================================================================*/

-(void)SetClientView:(CToolBarClientView*)pView AnimationType:(int)iType

/*============================================================================*/
{
    [pView setAutoresizingMask:0];

    m_iAnimationType = iType;

    if (iType != TOOLBAR_NONE)
    {
        [pView setFrame:self.bounds];

        _m_pClientView = m_pClientView;
        if (m_pHL == NULL)
            [self addSubview:pView];
        else
            [self insertSubview:pView belowSubview:m_pHL];
        m_pClientView = pView;
        [pView setAlpha:0];

        double dDY1 = [CMainView DY_TOOLBAR];
        double dDY2 = [CMainView DY_TOOLBAR_LARGE];

        switch (iType)
        {
        case TOOLBAR_FROMSYSMENU:
            {
                double dTemp = dDY1;
                dDY1 = dDY2;
                dDY2 = dTemp;
                CGAffineTransform MyScaler = CGAffineTransformMakeScale(1.0, dDY1 / dDY2);
                CGAffineTransform MyTrans  = CGAffineTransformMakeTranslation(0, (dDY1-dDY2)/2);
                CGAffineTransform MyConcat = CGAffineTransformConcat(MyScaler, MyTrans);
                [self setTransform:MyConcat];
                [self setFrame:CGRectMake(0, 0, self.bounds.size.width, dDY2)];
                MyScaler = CGAffineTransformMakeScale(1.0, dDY2 / dDY1);
                MyTrans  = CGAffineTransformMakeTranslation(0, (dDY2-dDY1)/2);
                MyConcat = CGAffineTransformConcat(MyScaler, MyTrans);
                [_m_pClientView setTransform:MyConcat];
            }
            break;
            
        case TOOLBAR_TOSYSMENU:
            {
                CGAffineTransform MyScaler = CGAffineTransformMakeScale(1.0, dDY1 / dDY2);
                CGAffineTransform MyTrans  = CGAffineTransformMakeTranslation(0, (dDY1-dDY2)/2);
                CGAffineTransform MyConcat = CGAffineTransformConcat(MyScaler, MyTrans);
                [self setTransform:MyConcat];
                [self setFrame:CGRectMake(0, 0, self.bounds.size.width, dDY2)];
                MyScaler = CGAffineTransformMakeScale(1.0, dDY2 / dDY1);
                MyTrans  = CGAffineTransformMakeTranslation(0, (dDY2-dDY1)/2);
                MyConcat = CGAffineTransformConcat(MyScaler, MyTrans);
                [_m_pClientView setTransform:MyConcat];
            }
            break;

        case TOOLBAR_LEFTTORIGHT:
            {
                CGRect rBounds = self.bounds;
                rBounds.origin.x -= rBounds.size.width / 4;
                [pView setFrame:rBounds];
            }
            break;
        case TOOLBAR_RIGHTTOLEFT:
            {
                CGRect rBounds = self.bounds;
                rBounds.origin.x += rBounds.size.width / 4;
                [pView setFrame:rBounds];
            }
            break;
        }


        return;
    }

    m_pClientView = pView;
    if (pView == NULL)
        return;
    if (m_pHL == NULL)
        [self addSubview:pView];
    else
        [self insertSubview:pView belowSubview:m_pHL];
    m_pClientView = pView;
}
/*============================================================================*/

-(void)CommitAnimations

/*============================================================================*/
{
    [_m_pClientView setAlpha:0];
    [m_pClientView setAlpha:1];
    [self setTransform:CGAffineTransformIdentity];
    
    CGRect rBounds = self.bounds;
    
    switch (m_iAnimationType) 
    {
    case TOOLBAR_LEFTTORIGHT:
        {
            [m_pClientView setFrame:rBounds];
            rBounds.origin.x += rBounds.size.width / 4;
            [_m_pClientView setFrame:rBounds];
        }
        break;

    case TOOLBAR_RIGHTTOLEFT:
        {
            [m_pClientView setFrame:rBounds];
            rBounds.origin.x -= rBounds.size.width / 4;
            [_m_pClientView setFrame:rBounds];
        }
        break;
    }
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGRect rHL = self.bounds;
    
        //DL
        //rHL.origin.y += 20;
    
        [m_pClientView setFrame:rHL];
        [m_pClientView layoutSubviews];
    
        //DL - background for the toolbar
        //rHL.origin.y += 20;

        CGRect rBackground = rHL;
        rBackground.origin.y += 20;
    
        CToolBarClientView *pClientView = m_pClientView;
        if (_m_pClientView != NULL)
            pClientView = NULL;

        UIView *pInsetL = [pClientView InsetLeft];
        UIView *pInsetR = [pClientView InsetRight];

        if (pInsetL != NULL)
        {
            int iDX = pInsetL.frame.origin.x + pInsetL.frame.size.width + 2;
            [CRect BreakOffLeft:&rBackground DX:iDX];
        }
        if (pInsetR != NULL)
        {
            rBackground.size.width = MIN(rBackground.size.width, pInsetR.frame.origin.x - rBackground.origin.x - 2);
        }

        [m_pBG setFrame:rBackground];
        [CRect BreakOffBottom:&rHL DY:rHL.size.height/2];
        [m_pHL setFrame:rHL];
    }
    else//DL - controls backgrounds for the tool bar - iPhpne
    {
        CGRect rHL = self.bounds;
        
        [m_pClientView setFrame:rHL];
        [m_pClientView layoutSubviews];
        
        CGRect rBackground = rHL;
        
        CToolBarClientView *pClientView = m_pClientView;
        if (_m_pClientView != NULL)
            pClientView = NULL;
        
        UIView *pInsetL = [pClientView InsetLeft];
        UIView *pInsetR = [pClientView InsetRight];
        
        if (pInsetL != NULL)
        {
            int iDX = pInsetL.frame.origin.x + pInsetL.frame.size.width + 2;
            [CRect BreakOffLeft:&rBackground DX:iDX];
        }
        if (pInsetR != NULL)
        {
            rBackground.size.width = MIN(rBackground.size.width, pInsetR.frame.origin.x - rBackground.origin.x - 2);
        }
        //DL - this deals with gray and gradient background for Main menu bar (home screen) iPhone
        //[m_pBG setFrame:rBackground];
        [CRect BreakOffBottom:&rHL DY:rHL.size.height/2];
        [m_pHL setFrame:rHL];

    }
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    [_m_pClientView removeFromSuperview];
    _m_pClientView = NULL;
}
/*============================================================================*/

-(void)Invalidate

/*============================================================================*/
{
    int iA1 = [CHLComm GetInt:INT_ALPHAMAX];
    if (iA1 > 0)
    {
        if (m_pHL == NULL)
        {
            m_pHL = [[CToolBarHighlight alloc] initWithFrame:CGRectZero];
            [m_pHL setOpaque:NO];
            [m_pHL setBackgroundColor:[UIColor clearColor]];
            [self addSubview:m_pHL];
            [m_pHL release];
        }
    }
    else
    {
        if (m_pHL != NULL)
        {
            [m_pHL removeFromSuperview];
            m_pHL= NULL;
        }
    }


    [m_pBG Invalidate];
    [self setNeedsDisplay];
    [m_pHL setNeedsDisplay];
}
/*============================================================================*/

-(UIView*)Background

/*============================================================================*/
{
    return m_pBG;
}
/*============================================================================*/

-(void)HLAddSubView:(UIView*)pSubView

/*============================================================================*/
{
    if (m_pHL == NULL)
        [self addSubview:pSubView];
    else
        [self insertSubview:pSubView belowSubview:m_pHL];
}
/*============================================================================*/

-(CToolBarClientView*)ClientView

/*============================================================================*/
{
    return m_pClientView;
}
/*============================================================================*/

-(CToolBarClientView*)ActiveClientView

/*============================================================================*/
{
    if (_m_pClientView != NULL)
        return _m_pClientView;
    return m_pClientView;
}
/*============================================================================*/

-(void)AnimationComplete

/*============================================================================*/
{
    [_m_pClientView removeFromSuperview];
    _m_pClientView = NULL;
}
@end
