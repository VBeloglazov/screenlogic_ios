/*
--------------------------------------------------------------------------------

    HLDC.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"

#ifdef IPHONE
    #include "dibdc.h"
    #include "colormap.h"
#else
    #include "helpers.h"
    #include "hlwindow.h"
    #include "fontserver.h"
    #include "dibdc.h"
    #include "gdiserv.h"
    #include "speed.h"
    #include "shadepoly.h"
    #include "shadeassem.h"
    #include "main.h"
    #include "colormap.h"
#endif

#include "hldc.h"

#ifdef XSCALE
    #include "..\include\ipp_arm\ipp.h"
#endif

    #define ENHANCE_STEPS       1
    #define ENHANCE_BLEND       4


/*============================================================================*/

    CTextOut::CTextOut(
    CHLRect                     rect,
    CHLString                   sText,
    int                         iFormat,
    int                         iTextSize,
    COLORREF                    rgbColor,
    int                         iXOrigin,
    int                         iYOrigin )

/*============================================================================*/
{
    m_rRect     = rect;
    m_sText     = sText;
    m_iFormat   = iFormat;  
    m_iTextSize = iTextSize;
    m_rgbColor  = rgbColor;
    m_iXOrigin  = iXOrigin;
    m_iYOrigin  = iYOrigin;
}
/*============================================================================*/

    CHLDC::CHLDC()

/*============================================================================*/
{
#ifndef IPHONE
    m_pWnd          = NULL;
#endif
    m_hWnd          = NULL;
    m_hDC           = NULL;
    m_bOwnDC        = FALSE;
    m_iXLeft        = 0;
    m_iYTop         = 0;
    m_iFontHeight   = 0;
    m_rgbColor      = RGB(0,0,0);
    m_pClipGlobal   = NULL;
    m_iAttribs      = 0;
    m_bOutEnabled   = TRUE;

#ifndef IPHONE
    #ifdef CRYSTALPAD
        if (g_pMainWindow != NULL)
            m_bOutEnabled = g_pMainWindow->EnableDisplay();
    #endif
#endif
    #ifdef HLDDRAW
        m_pSurface      = NULL;
    #endif
}
#ifndef IPHONE
/*============================================================================*/

    CHLDC::CHLDC(
    CHLWindow                   *pWnd )

/*============================================================================*/
{
    if (pWnd != g_pMainWindow)
    {
        int iTest = 1;
    }

    START_TIMER(GDI_DCCONST);
    m_pWnd          = pWnd;
    m_hWnd          = g_pMainWindow->HWnd();
    m_pClipGlobal   = NULL;
    m_iAttribs      = 0;
    m_hDC           = NULL;
    m_bOwnDC        = TRUE;
    m_iXLeft        = pWnd->XScreen(0);
    m_iYTop         = pWnd->YScreen(0);
    m_rgbColor      = RGB(0,0,0);
    m_bOutEnabled   = TRUE;

    #ifdef CRYSTALPAD
        if (g_pMainWindow != NULL)
            m_bOutEnabled = g_pMainWindow->EnableDisplay();
    #endif

    #ifdef HLDDRAW
        m_pSurface      = NULL;
    #endif    

    CHLRect rClip;
    if (pWnd->GetScreenClip(pWnd, &rClip))
    {
        rClip.Translate(-pWnd->XScreen(0), -pWnd->YScreen(0));
        PushClip(&rClip);
    }

    m_iFontHeight   = 0;

    END_TIMER(GDI_DCCONST);
}
#endif
#ifndef MAC_OS
/*============================================================================*/

    CHLDC::CHLDC(
    CHLWindow                   *pWnd,
    HDC                         hDC )

/*============================================================================*/
{
    m_pWnd          = pWnd;
    m_hWnd          = g_pMainWindow->HWnd();
    m_hDC           = hDC;
    m_bOwnDC        = FALSE;
    m_iXLeft        = pWnd->XScreen(0);
    m_iYTop         = pWnd->YScreen(0);
    m_pClipGlobal   = NULL;
    m_iAttribs      = 0;
    m_rgbColor      = RGB(0,0,0);
    m_bOutEnabled   = TRUE;

    #ifdef CRYSTALPAD
        m_bOutEnabled = g_pMainWindow->EnableDisplay();
    #endif

    SetBkMode(hDC, TRANSPARENT);

    #ifdef HLDDRAW
        m_pSurface      = NULL;
    #endif
}
#endif
/*============================================================================*/

    CHLDC::~CHLDC()

/*============================================================================*/
{
    FlushText();

    if (m_hDC != NULL)
    {
        if (m_bOwnDC)
        {
            LOGASSERT(m_hDC != NULL);
            ReleaseDC(m_hWnd, m_hDC);
        }
    }


    while (!m_ClipStack.IsEmpty())
    {
        CHLRect *pRect = (CHLRect*)m_ClipStack.RemoveHead();
        delete pRect;
    }

    while (!m_OriginStack.IsEmpty())
    {
        POINT *pPt = (POINT*)m_OriginStack.RemoveHead();
        delete pPt;
    }

    delete m_pClipGlobal;
    m_pClipGlobal = NULL;
}
#ifdef HLDDRAW
/*============================================================================*/

    HL_DIRECTDRAWSURFACE        CHLDC::GetSurface()

/*============================================================================*/
{
    return m_pSurface;
}
/*============================================================================*/

    void                        CHLDC::Attach(
    HL_DIRECTDRAWSURFACE        pSurface )

/*============================================================================*/
{
    if (m_pSurface != NULL)
        return;
    m_pSurface = pSurface;
}
#endif
/*============================================================================*/

    void                        CHLDC::GlobalClip(
    CHLRect                     *pNew )

/*============================================================================*/
{
    if (m_pClipGlobal != NULL)
    {
        delete m_pClipGlobal;
        m_pClipGlobal = NULL;
    }

    if (pNew == NULL)
        return;
    m_pClipGlobal = new CDXYRect(pNew->XLeft(), pNew->YTop(), pNew->DX(), pNew->DY());
}
/*============================================================================*/

    void                        CHLDC::Attribs(int iNew)           

/*============================================================================*/
{ 
    if (m_iAttribs & DDFLAG_DISCARDABLE)
        iNew |= DDFLAG_DISCARDABLE;
    else
        iNew &= ~DDFLAG_DISCARDABLE;

    m_iAttribs = iNew;    
}
/*============================================================================*/

    void                        CHLDC::InitBlend()

/*============================================================================*/
{
}
/*============================================================================*/

    void                        CHLDC::DeallocBlend()

/*============================================================================*/
{
}
/*============================================================================*/

    POINT                       CHLDC::GetOrigin()

/*============================================================================*/
{
    POINT pt;
    pt.x = m_iXLeft;
    pt.y = m_iYTop;
    return pt;
}
/*============================================================================*/

    void                        CHLDC::SetOrigin(
    POINT                       ptNew )

/*============================================================================*/
{
    SetOrigin(ptNew.x, ptNew.y);
}
#ifndef IPHONE
/*============================================================================*/

    void                        CHLDC::SetOrigin(
    CHLWindow                   *pWnd )

/*============================================================================*/
{
    SetOrigin(pWnd->XScreen(0), pWnd->YScreen(0));
}
#endif
/*============================================================================*/

    void                        CHLDC::SetOrigin(
    int                         iXLeft,
    int                         iYTop )

/*============================================================================*/
{
    m_iXLeft = iXLeft;
    m_iYTop = iYTop;
}
/*============================================================================*/

    POINT                       CHLDC::GetOriginAbs()

/*============================================================================*/
{
    return GetOrigin();
}
/*============================================================================*/

    void                        CHLDC::TranslateOrigin(
    int                         iDX,
    int                         iDY )
    
/*============================================================================*/
{ 
    m_iXLeft += iDX;
    m_iYTop += iDY;  
}
/*============================================================================*/

    CHLDibDC                    *CHLDC::DibDC()

/*============================================================================*/
{
    return NULL;
}
#ifndef IPHONE
/*============================================================================*/

    void                        CHLDC::SetQuickFont(
    int                         iHeight )

/*============================================================================*/
{
    // Quick Check
    if (iHeight == m_iFontHeight)
        return;

    m_iFontHeight = g_pFontServer->ClosestHeight(iHeight);
}
/*============================================================================*/

    UINT                        CHLDC::GDIFormat(
    int                         iHLFormat )

/*============================================================================*/
{
    // Figure out left/right
    UINT nFormat = 0;
    switch (iHLFormat & 0x07)
    {
    case HL_HLEFT:
        nFormat |= DT_LEFT;
        break;
    case HL_HCENTER:
        nFormat |= DT_CENTER;
        break;
    case HL_HRIGHT:
        nFormat |= DT_RIGHT;
        break;
    }

    // Now up/down
    switch (iHLFormat & 0x38)
    {
    case HL_VBOTTOM:
        nFormat |= DT_BOTTOM;
        break;
    case HL_VTOP:
        nFormat |= DT_TOP;
        break;
    }

    return nFormat;
}
#endif
#ifndef IPHONE
/*============================================================================*/

    void                        CHLDC::DrawString(
    CHLString                   sText,
    CHLRect                     rect,
    int                         iHLFormat  )

/*============================================================================*/
{
    if (iHLFormat & HL_TRUNCATE)
    {
        if (g_pFontServer->GetTextExtent(m_iFontHeight, sText).cx > rect.DX())
        {
            int iSizeDots = g_pFontServer->GetTextExtent(m_iFontHeight, _T("...")).cx;
            int iDXMax = rect.DX()-iSizeDots;
            int iLen = sText.GetLength();
            TCHAR *pText = sText.GetBufferTCHAR(iLen);
            int iMaxChars = g_pFontServer->MaxCharsInLine(m_iFontHeight, pText, iLen, iDXMax, 0);
            sText.ReleaseBuffer(iMaxChars);
            CHLString sTemp = sText;
            sText.Format(_T("%s..."), (const TCHAR*)sTemp);
        }
    }

#ifndef CRYSTALPAD
    #ifdef HLDDRAW
        if (m_pSurface != NULL && m_hDC == NULL)
        {
            m_TextOut.AddTail(new CTextOut(rect, sText, iHLFormat, m_iFontHeight, m_rgbColor, m_iXLeft, m_iYTop));
            return;
        }
    #else
        if (m_hDC == NULL)
        {
            m_TextOut.AddTail(new CTextOut(rect, sText, iHLFormat, m_iFontHeight, m_rgbColor, m_iXLeft, m_iYTop));
            return;
        }
    #endif
#endif


#ifdef RASTERIZE_FONT
    CHLFont *pFont = g_pFontServer->GetFont(m_iFontHeight);
    if (pFont == NULL)
        return;

    START_TIMER(GDI_TEXT);
    rect.Translate(m_iXLeft, m_iYTop);
    pFont->RenderText(this, &sText, rect, iHLFormat, m_rgbColor);
    END_TIMER(GDI_TEXT);
#else
    START_TIMER(GDI_TEXT);
    CHLRect rIn = rect;

    rect.Translate(m_iXLeft, m_iYTop);
    RECT rTemp = rect.GetRECT();
    rTemp.right++;
    rTemp.bottom++;

    HDC hDC = GetHDC();
    SetTextColor(hDC, m_rgbColor);
    HFONT hFont = g_pFontServer->GetFont(m_iFontHeight);
    HANDLE hOldFont = SelectObject(hDC, hFont);

    if (iHLFormat & HL_VCENTER)
    {
        //
        // We have to go through this deal becuase GDI can't handle vertically centering mult-line
        // text. so do it here.  Also, DT_CALCRECT seems to come up short for clear-type fonts by about 10%
        //
        CHLString sTemp = sText;
        BOOL bSingleLine = FALSE;
        #ifdef UNICODE
            TCHAR *pBuf = sTemp.GetBufferTCHAR(sText.GetLength());
            if (wcsstr(pBuf, _T("\n")) == NULL)
                bSingleLine = TRUE;
        #else
            char *pBuf = sTemp.GetBufferCHAR(sText.GetLength());
            if (strstr(pBuf, "\n") == NULL)
                bSingleLine = TRUE;
        #endif

        if (bSingleLine)
        {
            int iLen = sText.GetLength();
            END_TIMER(GDI_TEXT);
            START_TIMER(GDI_TEXTOUT);
            DrawText(GetHDC(), sText, iLen, &rTemp, GDIFormat(iHLFormat)|DT_NOPREFIX|DT_VCENTER|DT_SINGLELINE);
            END_TIMER(GDI_TEXTOUT);            
            SelectObject(hDC, hOldFont);
            return;
        }


        RECT rSize;
        memset(&rSize, 0, sizeof(RECT));
        END_TIMER(GDI_TEXT);
        START_TIMER(GDI_TEXTOUT);
        DrawText(GetHDC(), sText, -1, &rSize, DT_CALCRECT);
        END_TIMER(GDI_TEXTOUT);

        int iDY2 = (int)((rSize.bottom - rSize.top) / 2);
        int iYMid = (rTemp.top + rTemp.bottom) / 2;
       
        rTemp.top = iYMid - iDY2;
        rTemp.bottom = iYMid + iDY2;
    }



    int iLen = sText.GetLength();
    END_TIMER(GDI_TEXT);
    START_TIMER(GDI_TEXTOUT);
    DrawText(GetHDC(), sText, iLen, &rTemp, DT_NOPREFIX|GDIFormat(iHLFormat));
    END_TIMER(GDI_TEXTOUT);
    SelectObject(hDC, hOldFont);
#endif
}
#endif
/*============================================================================*/

    void                        CHLDC::DrawRect(
    COLORREF                    rgb, 
    CHLRect                     rect )

/*============================================================================*/
{
    DrawRect(rgb, rect.XLeft(), rect.YTop(), rect.DX(), rect.DY());
}
/*============================================================================*/

    void                        CHLDC::DrawRect(
    COLORREF                    rgb,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY)

/*============================================================================*/
{
    //
    // Remember that XLR = XUL + DX - 1.
    //

    #ifdef HLDDRAW
        if (m_pSurface != NULL)
        {
            FillRect(rgb, iXLeft, iYTop, 1, iDY);
            FillRect(rgb, iXLeft, iYTop, iDX, 1);
            FillRect(rgb, iXLeft+iDX-1, iYTop, 1, iDY);
            FillRect(rgb, iXLeft, iYTop+iDY-1, iDX, 1);
            return;
        }
    #endif

    int iXRight = iXLeft + iDX - 1;
    int iYBottom = iYTop + iDY - 1;
    DrawLine(rgb, iXLeft,  iYTop,    iXRight, iYTop);
    DrawLine(rgb, iXRight, iYTop,    iXRight, iYBottom);
    DrawLine(rgb, iXRight, iYBottom, iXLeft,  iYBottom);
    DrawLine(rgb, iXLeft,  iYBottom, iXLeft,  iYTop);
}
#ifndef IPHONE
/*============================================================================*/

    void                        CHLDC::FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pList )

/*============================================================================*/
{
    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE pSurf = m_pSurface;
        if (pSurf == NULL)
            pSurf = g_pGDIServer->GetPrimary();
        if (pSurf != NULL || DibDC() != NULL)
        {
            int iNRects = pList->NRects();
            for (int i = 0; i < iNRects; i++)
            {
                FillRect(rgbFill, pList->X(i), pList->Y(i), pList->DX(i), pList->DY(i));
            }

            return;
        }
    #endif

    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);

    START_TIMER(GDI_FILLRECT);

    HBRUSH hBrush = CreateSolidBrush(rgbFill);

    int iNRects = pList->NRects();
    for (int i = 0; i < iNRects; i++)
    {
        CHLRect rect = pList->RectAtIndex(i);
        rect.Translate(m_iXLeft, m_iYTop);
        RECT r = rect.GetRECT();
        if (m_bOutEnabled)
        {
            ::FillRect(hDC, &r, hBrush);
        }
    }

    END_TIMER(GDI_FILLRECT);

    START_TIMER(GDI_GDI);
    DeleteObject(hBrush);
    END_TIMER(GDI_GDI);
}
/*============================================================================*/

    void                        CHLDC::FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pList,
    CHLDibDC                    *pDIBDC,
    int                         iXSrcOffset,
    int                         iYSrcOffset )

/*============================================================================*/
{
    if (pDIBDC == NULL)
    {
        FillRectList(rgbFill, pList);
        return;
    }

    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE pSurface = m_pSurface;
        if (pSurface == NULL)
            pSurface = g_pGDIServer->GetPrimary();

        if (pSurface != NULL || DibDC() != NULL)
        {
            int iNRects = pList->NRects();
            for (int i = 0; i < iNRects; i++)
            {
                int iDX = pList->DX(i);
                int iDY = pList->DY(i);
                int iXLeft      = pList->X(i) + m_iXLeft;
                int iXRight     = iXLeft + iDX;
                int iYTop       = pList->Y(i) + m_iYTop;
                int iYBottom    = iYTop + iDY;

                int iXSrc = iXLeft + iXSrcOffset;
                int iYSrc = iYTop  + iYSrcOffset;
                CHLRect rBlt = GetBltRect(pDIBDC, iXLeft, iYTop, iXSrc, iYSrc, iDX, iDY);
                if (rBlt.DX() > 0 || rBlt.DY() > 0)
                {
                    CRectList TempList;
                    CDXYRect rRef(iXLeft, iYTop, iDX, iDY);
                    TempList.AddRectRef(&rRef);
                    TempList.Subtract(&rBlt);
                    BltFromDibDC(iXLeft, iYTop, pDIBDC, iXSrc, iYSrc, iDX, iDY);
                    for (int iFill = 0; iFill < TempList.NRects(); iFill++) 
                    {
                        CHLRect rTempThis = TempList.RectAtIndex(iFill);
                        FillRect(rgbFill, rTempThis);
                    }
                }
                else
                {
                    FillRect(rgbFill, iXLeft, iYTop, iDX, iDY);
                }
            }

            return;
        }
    #endif

    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);
    HBRUSH hBrush = CreateSolidBrush(rgbFill);

    START_TIMER(GDI_FILLRECT);

    int iNRects = pList->NRects();
    for (int i = 0; i < iNRects; i++)
    {
        int iDX = pList->DX(i);
        int iDY = pList->DY(i);
        int iXLeft      = pList->X(i) + m_iXLeft;
        int iXRight     = iXLeft + iDX;
        int iYTop       = pList->Y(i) + m_iYTop;
        int iYBottom    = iYTop + iDY;

        int iXSrc = iXLeft + iXSrcOffset;
        int iYSrc = iYTop  + iYSrcOffset;
        CHLRect rBlt = GetBltRect(pDIBDC, iXLeft, iYTop, iXSrc, iYSrc, iDX, iDY);

        if (rBlt.DX() > 0 && rBlt.DY() > 0)
        {
            CRectList TempList;
        
            CDXYRect rRef(iXLeft, iYTop, iDX, iDY);
            TempList.AddRectRef(&rRef);
            TempList.Subtract(&rBlt);
            BltFromDibDC(iXLeft, iYTop, pDIBDC, iXSrc, iYSrc, iDX, iDY);
            for (int iFill = 0; iFill < TempList.NRects(); iFill++) 
            {
                RECT r = TempList.RectAtIndex(iFill).GetRECT();
                if (m_bOutEnabled)
                {
                    ::FillRect(hDC, &r, hBrush);
                }
            }
        }
        else
        {
            RECT r = CDXYRect(iXLeft, iYTop, iDX, iDY).GetRECT();
            if (m_bOutEnabled)
            {
                ::FillRect(hDC, &r, hBrush);
            }
        }
    }

    END_TIMER(GDI_FILLRECT);

    START_TIMER(GDI_GDI);
    DeleteObject(hBrush);
    END_TIMER(GDI_GDI);
}
#endif
#ifndef IPHONE
/*============================================================================*/

    void                        CHLDC::FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pList,
    CGradient2D                 *pGradient,
    int                         iXSrcOffset,
    int                         iYSrcOffset )

/*============================================================================*/
{
    if (pGradient == NULL)
    {
        FillRectList(rgbFill, pList);
        return;
    }

    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE pSurface = m_pSurface;
        if (pSurface == NULL)
            pSurface = g_pGDIServer->GetPrimary();

        if (pSurface != NULL || DibDC() != NULL)
        {
            int iNRects = pList->NRects();
            for (int i = 0; i < iNRects; i++)
            {
                int iDX = pList->DX(i);
                int iDY = pList->DY(i);
                int iXLeft      = pList->X(i) + m_iXLeft;
                int iXRight     = iXLeft + iDX;
                int iYTop       = pList->Y(i) + m_iYTop;
                int iYBottom    = iYTop + iDY;

                int iXSrc = iXLeft + iXSrcOffset;
                int iYSrc = iYTop  + iYSrcOffset;
                CHLRect rBlt = GetBltRect(pGradient, iXLeft, iYTop, iXSrc, iYSrc, iDX, iDY);
                if (rBlt.DX() > 0 || rBlt.DY() > 0)
                {
                    CRectList TempList;
                    CDXYRect rRef(iXLeft, iYTop, iDX, iDY);
                    TempList.AddRectRef(&rRef);
                    TempList.Subtract(&rBlt);
                    BltFromGradient(pGradient, iXLeft, iYTop, iXSrc, iYSrc, iDX, iDY);
                    for (int iFill = 0; iFill < TempList.NRects(); iFill++) 
                    {
                        CHLRect rTempThis = TempList.RectAtIndex(iFill);
                        FillRect(rgbFill, rTempThis);
                    }
                }
                else
                {
                    FillRect(rgbFill, iXLeft, iYTop, iDX, iDY);
                }
            }

            return;
        }
    #endif

    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);
    HBRUSH hBrush = CreateSolidBrush(rgbFill);

    START_TIMER(GDI_FILLRECT);

    int iNRects = pList->NRects();
    for (int i = 0; i < iNRects; i++)
    {
        int iDX = pList->DX(i);
        int iDY = pList->DY(i);
        int iXLeft      = pList->X(i) + m_iXLeft;
        int iXRight     = iXLeft + iDX;
        int iYTop       = pList->Y(i) + m_iYTop;
        int iYBottom    = iYTop + iDY;

        int iXSrc = iXLeft + iXSrcOffset;
        int iYSrc = iYTop  + iYSrcOffset;
        CHLRect rBlt = GetBltRect(pGradient, iXLeft, iYTop, iXSrc, iYSrc, iDX, iDY);

        if (rBlt.DX() > 0 && rBlt.DY() > 0)
        {
            CRectList TempList;
        
            CDXYRect rRef(iXLeft, iYTop, iDX, iDY);
            TempList.AddRectRef(&rRef);
            TempList.Subtract(&rBlt);
            BltFromGradient(pGradient, iXLeft, iYTop, iXSrc, iYSrc, iDX, iDY);
            for (int iFill = 0; iFill < TempList.NRects(); iFill++) 
            {
                RECT r = TempList.RectAtIndex(iFill).GetRECT();
                if (m_bOutEnabled)
                {
                    ::FillRect(hDC, &r, hBrush);
                }
            }
        }
        else
        {
            RECT r = CDXYRect(iXLeft, iYTop, iDX, iDY).GetRECT();
            if (m_bOutEnabled)
            {       
                ::FillRect(hDC, &r, hBrush);
            }
        }
    }

    END_TIMER(GDI_FILLRECT);

    START_TIMER(GDI_GDI);
    DeleteObject(hBrush);
    END_TIMER(GDI_GDI);
}
#endif
/*============================================================================*/

    void                        CHLDC::FillRect(
    COLORREF                    rgbFill,
    CHLRect                     rect  )

/*============================================================================*/
{
    FillRect(rgbFill, rect.XLeft(), rect.YTop(), rect.DX(), rect.DY());
}
/*============================================================================*/

    HDC                         CHLDC::GetHDC()

/*============================================================================*/
{
#ifdef IPHONE
    return 0;
#else
    #ifdef HLDDRAW
        LOGASSERT(m_pSurface == NULL);

        if (m_hDC != NULL)
            return m_hDC;

        if (m_pSurface != NULL)
        {
            m_pSurface->GetDC(&m_hDC);
            return m_hDC;
        }
    #endif

    if (m_hDC == NULL)
    {
        START_TIMER(GDI_GDI);
        m_hDC           = GetDC(m_hWnd);
        m_bOwnDC        = TRUE;
        SetBkMode(m_hDC, TRANSPARENT);
        END_TIMER(GDI_GDI);
    }

    return m_hDC;
#endif
}
/*============================================================================*/

    void                        CHLDC::FillRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
#ifndef IPHONE
    iXLeft += m_iXLeft;
    iYTop  += m_iYTop;
    Clip(&iXLeft, &iYTop, NULL, NULL, &iDX, &iDY);

    if (iDX < 1 || iDY < 1)
        return;

    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE pSurface = m_pSurface;
        if (pSurface == NULL)
            pSurface = g_pGDIServer->GetPrimary();
        if (pSurface != NULL)
        {
            // Use a color fill blit to fill the primary surface with 
            // the color key. 
            DDBLTFX ddbltfx;
            ZeroMemory( &ddbltfx, sizeof( ddbltfx ) ); 
            ddbltfx.dwSize = sizeof( ddbltfx ); 
            ddbltfx.dwFillColor = RGBTOHLPIXEL(rgbFill); 
            ddbltfx.dwROP       = PATCOPY;
            RECT rect;

            rect.left   = iXLeft;
            rect.top    = iYTop;            
            rect.right  = rect.left + iDX;
            rect.bottom = rect.top + iDY;

            rect.left = __max(0, rect.left);
            rect.top  = __max(0, rect.top);
            rect.right  = __min(rect.right, g_pMainWindow->DX());    
            rect.bottom = __min(rect.bottom, g_pMainWindow->DY());    

            if (rect.right <= rect.left)
                return;
            if (rect.bottom <= rect.top)
                return;

            #ifdef HL_DDRAWBASIC
                HRESULT res = pSurface->Blt( &rect, NULL, NULL, DDBLT_COLORFILL, &ddbltfx );
            #else
                HRESULT res = pSurface->Blt( &rect, NULL, &rect, DDBLT_COLORFILL | DDBLT_WAIT, &ddbltfx );
            #endif

            if (res != S_OK)
            {
                CHLString sTrace;
                sTrace.Format(_T("ERROR Line = %d RES = %08X"), __LINE__, res);
                TraceWrap(sTrace);
            }

            
            END_TIMER(GDI_FILLRECT);
            return;
        }
    #endif

    RECT r = CDXYRect(iXLeft, iYTop, iDX, iDY).GetRECT();

    HDC hDC = GetHDC();
    HBRUSH hBrush = CreateSolidBrush(rgbFill);

    //
    // Convert to screen coords. Also, recall that Rectangle does not include the
    // last pixel, whereas our FillRect does. Normally you'd expect to see -1
    // after XUL + DX and YUL + DY, but here we need that extra 1.
    //
    if (m_bOutEnabled)
    {
        ::FillRect(hDC, &r, hBrush);
    }

    END_TIMER(GDI_FILLRECT);

    START_TIMER(GDI_GDI);
    DeleteObject(hBrush);
    END_TIMER(GDI_GDI);
#endif
}
/*============================================================================*/

    void                        CHLDC::Draw3DFrame(
    CHLRect                     rOuter,
    int                         iThickness,
    COLORREF                    rgb,
    BOOL                        bRaised )

/*============================================================================*/
{
    int iX1 = rOuter.XLeft();
    int iY1 = rOuter.YTop();
    int iX2 = rOuter.XRight();
    int iY2 = rOuter.YBottom();
    int i;

    float fBright   = 1.207f;
    float fDark     = 0.50f;

    if (bRaised)
    {
        float fTemp = fDark;
        fDark = fBright;
        fBright = fTemp;
    }

    COLORREF rgbB = ScaleColor(fBright, rgb);
    COLORREF rgbD = ScaleColor(fDark, rgb);

    for (i = 0; i < iThickness; i++)
    {
        DrawLine(rgbD, iX1, iY1, iX2, iY1);
        DrawLine(rgbD, iX1, iY1, iX1, iY2);
        DrawLine(rgbB, iX1, iY2, iX2, iY2);
        DrawLine(rgbB, iX2, iY2, iX2, iY1);
        iX1++;
        iY1++;
        iX2--;
        iY2--;
    }
}
/*============================================================================*/

    void                        CHLDC::FillRoundRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iGeoType )

/*============================================================================*/
{
#ifndef IPHONE
    if (iDX < 1 || iDY < 1)
        return;

    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    BltToDibDC(&dib, iXLeft, iYTop);
    dib.FillRoundRect(rgbFill, 0, 0, iDX, iDY, iRad, iGeoType);
    BltFromDibDC(&dib, iXLeft, iYTop);
#endif
}
/*============================================================================*/

    void                        CHLDC::DrawFilled3DBorder(
    COLORREF                    rgbFace,
    BOOL                        bRaised,
    int                         iXUL,
    int                         iYUL,
    int                         iXLR,
    int                         iYLR,
    int                         iBorder )

/*============================================================================*/
{
    DrawFilled3DBorder(rgbFace, rgbFace, bRaised, iXUL, iYUL, iXLR, iYLR, iBorder);
}
/*============================================================================*/

    void                        CHLDC::DrawFilled3DBorder(
    COLORREF                    rgbFace,
    COLORREF                    rgbEdge,
    BOOL                        bRaised,
    int                         iXUL,
    int                         iYUL,
    int                         iXLR,
    int                         iYLR,
    int                         iBorder  )

/*============================================================================*/
{
    DrawFilled3DBorder(rgbFace, rgbEdge, bRaised, iXUL, iYUL, iXLR, iYLR, iBorder, 1.0);
}
/*============================================================================*/

    void                        CHLDC::DrawFilled3DBorder(
    COLORREF                    rgbFace,
    COLORREF                    rgbEdge,
    BOOL                        bRaised,
    int                         iXUL,
    int                         iYUL,
    int                         iXLR,
    int                         iYLR,
    int                         iBorder,
    float                       fSpotFactor )

/*============================================================================*/
{
    if (iBorder == 0)
    {
        FillRect(rgbFace, iXUL, iYUL, iXLR - iXUL+1, iYLR - iYUL + 1);
        return;
    }

    // Check border: iBorder is number of pixels that will get colored.
    iBorder = MAX(1, iBorder);

    FillRect(rgbFace, iXUL, iYUL, iXLR-iXUL+1, iYLR-iYUL+1);

    float fBright   = 0.207f;
    float fDark     = -0.50f;
    fBright *= fSpotFactor;
    fDark   *= fSpotFactor;
    fBright = 1.0f + fBright;
    fDark   = 1.0f + fDark;
    COLORREF rgbBright  = ScaleColor(fBright, rgbEdge);
    COLORREF rgbDark    = ScaleColor(fDark,   rgbEdge);
    if (rgbEdge == 0x00000000)
        rgbBright = RGB(128,128,128);

    // Check for white on white
    if (rgbBright == rgbEdge)
    {
        rgbBright = Blend(rgbFace, rgbEdge, 1.0);
    }

    if (bRaised)
    {
        COLORREF rgbTemp = rgbDark;
        rgbDark = rgbBright;
        rgbBright = rgbTemp;
    }

    int i;
    for (i = 0; i < iBorder; i++)
    {
        DrawLine(rgbDark, iXUL+i, iYUL+i, iXUL+i, iYLR-i);   // Left vert
        DrawLine(rgbDark, iXUL+i, iYUL+i, iXLR-i, iYUL+i);   // Top Hz
    }

    for (i = 0; i < iBorder; i++)
    {
        DrawLine(rgbBright, iXLR-i, iYUL+i, iXLR-i, iYLR-i);   // Right ver
        DrawLine(rgbBright, iXUL+i, iYLR-i, iXLR-i, iYLR-i);   // Bottom Hz
    }
}
/*============================================================================*/

    void                        CHLDC::SetColor(
    COLORREF                    rgbNew )

/*============================================================================*/
{
    if (rgbNew == m_rgbColor)
        return;

    m_rgbColor = rgbNew;
}
/*============================================================================*/

    void                        CHLDC::DrawLine(
    COLORREF                    rgb,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iPenSize )

/*============================================================================*/
{
#ifndef IPHONE
    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE pSurface = m_pSurface;
        if (pSurface == NULL)
            pSurface = g_pGDIServer->GetPrimary();
        if (pSurface != NULL)
        {
            if (iX1 == iX2)
            {
                int iDY = abs(iY2-iY1);
                iX1 -= iPenSize/2;
                FillRect(rgb, iX1, __min(iY1,iY2), iPenSize, iDY);
                return;
            }
            if (iY1 == iY2)
            {
                int iDX = abs(iX2-iX1);
                iY1 -= iPenSize/2;
                FillRect(rgb, __min(iX1,iX2), iY1, iDX, iPenSize);
                return;
            }
        }
    #endif

    HDC hDC = GetHDC();
    SetColor(rgb);

    HPEN hOldPen = NULL;
    HPEN hTempPen = NULL;

    hTempPen = CreatePen(PS_SOLID, iPenSize, rgb);
    hOldPen = (HPEN)SelectObject(hDC, hTempPen);

    // Shift.
    iX1 += (m_iXLeft);
    iY1 += m_iYTop;
    iX2 += (m_iXLeft);
    iY2 += m_iYTop;

    #if ((_WIN32_WCE == 211) || (_WIN32_WCE == 300))
        static POINT pts[2];
        pts[0].x = iX1;
        pts[0].y = iY1;
        pts[1].x = iX2;
        pts[1].y = iY2;
        Polyline(hDC, pts, 2);
    #else
        MoveToEx(hDC, iX1, iY1, NULL);
        LineTo(hDC, iX2, iY2);
    #endif
    SetPixel(hDC, iX2, iY2, m_rgbColor);

    SelectObject(hDC, hOldPen);
    DeleteObject(hTempPen);
#endif
}
/*============================================================================*/

    void                        CHLDC::DrawPolygon(
    COLORREF                    rgb,
    int                         *pX,
    int                         *pY,
    int                         iNPoints )

/*============================================================================*/
{
    for (int i = 0; i < (iNPoints-1); i++)
    {
        int iX1 = pX[i];
        int iY1 = pY[i];
        int iX2 = pX[i+1];
        int iY2 = pY[i+1];
        DrawLine(rgb, iX1, iY1, iX2, iY2);
    }
}
/*============================================================================*/

    void                        CHLDC::DrawPolygon(
    COLORREF                    rgb,
    POINT                       *pPoints,
    int                         iNPoints )

/*============================================================================*/
{
    for (int i = 0; i < (iNPoints-1); i++)
    {
        POINT *p1 = &(pPoints[i]);
        POINT *p2 = &(pPoints[i+1]);
        DrawLine(rgb, p1->x, p1->y, p2->x, p2->y);
    }
}
/*============================================================================*/

    CHLRect                     CHLDC::GetBltRect(
    CHLDibDC                    *pSrc, 
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    // Start out assuming the full thing will go
    CHLRect rRes = CDXYRect(iXDst, iYDst, iDX, iDY);
    if (iXSrc < 0)
        rRes.BreakOffLeft(-iXSrc, 0);
    if (iYSrc < 0)
        rRes.BreakOffTop(-iYSrc, 0);
    
    int iDXBlt = pSrc->ImageDX()-iXSrc;
    int iDYBlt = pSrc->DY()-iYSrc;
    if (iDXBlt < iDX)
        rRes.BreakOffRight(iDX-iDXBlt, 0);
    if (iDYBlt < iDY)
        rRes.BreakOffBottom(iDY-iDYBlt, 0);
    return rRes;
}
/*============================================================================*/

    CHLRect                     CHLDC::GetBltRect(
    CGradient2D                 *pSrc, 
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    // Start out assuming the full thing will go
    CHLRect rRes = CDXYRect(iXDst, iYDst, iDX, iDY);
    if (iXSrc < 0)
        rRes.BreakOffLeft(-iXSrc, 0);
    if (iYSrc < 0)
        rRes.BreakOffTop(-iYSrc, 0);
    
//    int iDXBlt = pSrc->ImageDX()-iXSrc;
    int iDYBlt = pSrc->Dimension()-iYSrc;
//    if (iDXBlt < iDX)
//        rRes.BreakOffRight(iDX-iDXBlt, 0);
    if (iDYBlt < iDY)
        rRes.BreakOffBottom(iDY-iDYBlt, 0);
    return rRes;
}
/*============================================================================*/

    BOOL                        CHLDC::DDrawScale(
    CHLDibDC                    *pSrc,
    CHLRect                     rDst,
    CHLRect                     rSrc )


/*============================================================================*/
{
    if (rSrc.DX() <= 0 || rSrc.DY() <= 0)
        return TRUE;
    if (rDst.DX() <= 0 || rDst.DY() <= 0)
        return TRUE;

    #ifdef HLDDRAW
        if (rDst.DX() == rSrc.DX() && rDst.DY() == rSrc.DY())
        {
            BltFromDibDC(rDst.XLeft(), rDst.YTop(), pSrc, rSrc.XLeft(), rSrc.YTop(), rSrc.DX(), rSrc.DY());
            return TRUE;
        }

        rDst.Translate(m_iXLeft, m_iYTop);
        CHLDibDC *pThis = DibDC();
        if (pThis != NULL)
            CHLDibDC::CorrectScaleRects(pSrc, pThis, &rSrc, &rDst);

        #ifdef HL_ARMV4IC
            rSrc.XLeft(__max(0, rSrc.XLeft()));
            rSrc.YTop(__max(0, rSrc.YTop()));
            rSrc.XRight(__min(rSrc.XRight(), pSrc->DX()-2));    // Inclusive
            rSrc.YBottom(__min(rSrc.YBottom(), pSrc->DY()-2));  // Inclusive
        #endif

        HL_DIRECTDRAWSURFACE pSource = pSrc->GetSurface();
        if (pSource == NULL)
            return FALSE;

        // GCU can't do this, only video->video scales
        if (pSrc->GetSurface() == NULL)
            return FALSE;

        HL_DIRECTDRAWSURFACE pDst = m_pSurface;
        if (pDst == NULL)
            pDst = g_pGDIServer->GetPrimary();

        if (pDst == NULL)
            return FALSE;

        #ifdef HL_ARMV4IC
            if (!g_pGDIServer->EnableGCUScale())
            {
                RECT rDstRECT = rDst.GetRECT();
                RECT rSrcRECT = rSrc.GetRECT();

                HRESULT hRes = pDst->Blt(&rDstRECT, pSource, &rSrcRECT, 0, NULL);
                return (hRes == S_OK);
            }

            int iDXSrcIn = rSrc.DX();
            int iDXDstIn = rDst.DX();
            while (rSrc.DX() > 0)
            {
                int iDXSrc = __min(249, rSrc.DX());
                if (rSrc.DX() < 497 && rSrc.DX() > 249)    
                    iDXSrc = rSrc.DX()/2;

                CHLRect rSrcThis = rSrc.BreakOffLeft(iDXSrc, 0);
                int iDXDst = rDst.DX();
                if (rSrc.DX() > 0)
                    iDXDst = iDXSrc * iDXDstIn / iDXSrcIn;
                CHLRect rDstThis = rDst.BreakOffLeft(iDXDst, 0);            
        
                RECT rDstRECT = rDstThis.GetRECT();
                RECT rSrcRECT = rSrcThis.GetRECT();

//                CHiResTimer tm;
                HRESULT hRes = pDst->Blt(&rDstRECT, pSource, &rSrcRECT, 0, NULL);
                if (hRes != S_OK)
                    return FALSE;

/*
                DWORD dwTime = tm.HLTickCount();
                if (dwTime > 10)
                {
                    CHLString sTrace;
                    sTrace.Format(_T("BLT: %d,%d %d\r\n"), rSrcThis.DX(), rSrcThis.DY(), tm.HLTickCount());
                    TraceWrap(sTrace);                
                }
*/
            }
        #else
            RECT rDstRECT = rDst.GetRECT();
            RECT rSrcRECT = rSrc.GetRECT();

            HRESULT hRes = pDst->Blt(&rDstRECT, pSource, &rSrcRECT, 0, NULL);
            if (hRes != S_OK)
                return FALSE;
        #endif

        return TRUE;
    #endif

    return FALSE;
}
#ifdef HL_ARMV4IC
/*============================================================================*/

    BOOL                        CHLDC::DDrawBlendBlt(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    BYTE                        byAlpha )

/*============================================================================*/
{
#ifdef HL_WIN32
    return FALSE;
#else
    if (byAlpha >= 0xFF)
    {
        iXDst -= m_iXLeft;
        iYDst -= m_iYTop;
        BltFromDibDC(iXDst, iYDst, pSrcDib, iXSrc, iYSrc, iDX, iDY);
        return TRUE;
    }

    if (byAlpha <= 0)
        return TRUE;

    if (!pSrcDib->GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, NULL))
        return TRUE;

    CDXYRect rSrc(iXSrc, iYSrc, iDX, iDY);
    CDXYRect rDst(iXDst, iYDst, iDX, iDY);
    RECT rSrcRECT = rSrc.GetRECT();
    RECT rDstRECT = rDst.GetRECT();
    HL_DIRECTDRAWSURFACE pDst = GetSurface();
    HL_DIRECTDRAWSURFACE pSrc = pSrcDib->GetSurface();
    if (pDst != NULL && pSrc != NULL)
    {
        DDALPHABLTFX fx;
        memset(&fx, 0, sizeof(DDALPHABLTFX));
        fx.dwSize = sizeof(DDALPHABLTFX);
        BYTE by8Bit = (BYTE)byAlpha;
        fx.ddargbScaleFactors.alpha = by8Bit;
        fx.ddargbScaleFactors.red   = 0xFF;
        fx.ddargbScaleFactors.green = 0xFF;
        fx.ddargbScaleFactors.blue  = 0xFF;
        HRESULT hRes = pDst->AlphaBlt(&rDstRECT, pSrc, &rSrcRECT, 0, &fx); 
        if (hRes == S_OK)
            return TRUE;
    }

    return FALSE;
#endif
}
/*============================================================================*/

    BOOL                        CHLDC::DDrawBltFromAlphaDIB(
    CAlphaDIB                   *pAlpha,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
#ifdef HL_WIN32
    return FALSE;
#else
    if (!pAlpha->HasDDAlphaSurface())
        return FALSE;

    iXDst += m_iXLeft;
    iYDst += m_iYTop;

    if (!pAlpha->GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, NULL))
        return TRUE;

    RECT rDst = CDXYRect(iXDst, iYDst, iDX, iDY).GetRECT();
    RECT rSrc = CDXYRect(iXSrc, iYSrc, iDX, iDY).GetRECT();

    HL_DIRECTDRAWSURFACE pThis = GetSurface();
    HL_DIRECTDRAWSURFACE pSrc  = pAlpha->GetDDAlphaSurface();

    BOOL bHandled = TRUE;
    #ifdef HL_DDRAWBASIC
        pThis->AlphaBlt(&rDst, pSrc, &rSrc, 0, NULL);
        bHandled = TRUE;
    #else
        LPDIRECTDRAWSURFACE5 pSurf5D = NULL;
        LPDIRECTDRAWSURFACE5 pSurf5S = NULL;
        pThis->QueryInterface(IID_IDirectDrawSurface5, (LPVOID*)&pSurf5D);
        pSrc->QueryInterface(IID_IDirectDrawSurface5, (LPVOID*)&pSurf5S);
        if (pSurf5D != NULL && pSurf5S != NULL)
        {
            HRESULT hRes = pSurf5D->AlphaBlt(&rDst, pSurf5S, &rSrc, 0, NULL);
            if (hRes == S_OK)
                bHandled = TRUE;
        }
        if (pSurf5D != NULL) pSurf5D->Release();
        if (pSurf5S != NULL) pSurf5S->Release();
    #endif
    
    if (bHandled)
        return TRUE;

    return FALSE;
#endif
}
#endif
/*============================================================================*/

    void                        CHLDC::Blt(
    int                         iXDest,
    int                         iYDest,
    CHLDC                       *pSrcDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    CHLDibDC                    *pHelperDC )

/*============================================================================*/
{

    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;
    iXSrc += pSrcDC->GetOrigin().x;
    iYSrc += pSrcDC->GetOrigin().y;


    if (iXSrc < 0)
    {
        iXDest -= iXSrc;
        iDX += iXSrc;
        iXSrc = 0;
    }
    if (iYSrc < 0)
    {
        iYDest -= iYSrc;
        iDY += iYSrc;
        iYSrc = 0;
    }
    if (iXDest < 0)
    {
        iXSrc -= iXDest;
        iDX += iXDest;
        iXDest = 0;
    }
    if (iYDest < 0)
    {
        iYSrc -= iYDest;
        iDY += iYDest;
        iYDest = 0;
    }

    if (iXSrc < 0)
    {
        iXDest -= iXSrc;
        iDX += iXSrc;
        iXSrc = 0;
    }
    if (iYSrc < 0)
    {
        iYDest -= iYSrc;
        iDY += iYSrc;
        iYSrc = 0;
    }

    #ifdef HL_X86A
        if (m_pSurface != NULL && pSrcDC == this)
        {
            HDC hDC = NULL;
            m_pSurface->GetDC(&hDC);
            ::BitBlt(hDC, iXDest, iYDest, iDX, iDY, hDC, iXSrc, iYSrc, SRCCOPY);
            m_pSurface->ReleaseDC(hDC);
            return;
        }

    #endif

    #ifdef HLDDRAW
        int iDXMain = g_pMainWindow->DX();
        int iDYMain = g_pMainWindow->DY();
        iDX = __min(iDX, iDXMain - iXSrc);
        iDX = __min(iDX, iDXMain - iXDest);
        iDY = __min(iDY, iDYMain - iYSrc);
        iDY = __min(iDY, iDYMain - iYDest);
        if (iDX <= 0 || iDY <= 0)
            return;
    #endif

    #ifdef HLDDRAW
    HL_DIRECTDRAWSURFACE pSurface = m_pSurface;
//    if (pSurface == NULL)
//        pSurface = g_pGDIServer->GetPrimary();

    if (pSurface != NULL)
    {
        if (pSurface->IsLost())
            pSurface->Restore();
        RECT rSrc;
        rSrc.left = iXSrc;
        rSrc.top  = iYSrc;
        rSrc.right  = iXSrc + iDX;
        rSrc.bottom = iYSrc + iDY;

        RECT rDst;
        rDst.left = iXDest;
        rDst.top  = iYDest;
        rDst.right  = iXDest + iDX;
        rDst.bottom = iYDest + iDY;

        if (pHelperDC != NULL)
        {
            HL_DIRECTDRAWSURFACE pHelperSurf = pHelperDC->GetSurface();
            if (pHelperDC->ImageDX() >= iDX && pHelperDC->DY() >= iDY && pHelperSurf != NULL)
            {
                RECT rHelper = CDXYRect(0, 0, iDX, iDY).GetRECT();
                pHelperSurf->Blt(&rHelper, pSurface, &rSrc, 0, NULL);
                pSurface->Blt(&rDst, pHelperSurf, &rHelper, 0, NULL);
                return;
            }
        }

        #ifdef HL_DDRAWBASIC
            HRESULT hr = pSurface->Blt(&rDst, pSurface, &rSrc, 0, NULL);
        #else
            HRESULT hr = pSurface->Blt(&rDst, pSurface, &rSrc, DDBLT_WAIT, NULL);
        #endif

        if (hr != S_OK)
        {
            CHLString sTrace;
            sTrace.Format(_T("ERROR Line = %d RES = %08X"), __LINE__, hr);
            TraceWrap(sTrace);
        }


        END_TIMER(GDI_BITBLT_GDI);
        return;
    }
    #endif

    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);

    START_TIMER(GDI_BITBLT_GDI);

#ifndef IPHONE
    if (m_bOutEnabled)
    {
        ::BitBlt(hDC, iXDest, iYDest, iDX, iDY, pSrcDC->GetHDC(), iXSrc, iYSrc, SRCCOPY);
    }
#endif

    END_TIMER(GDI_BITBLT_GDI);
}
/*============================================================================*/

    void                        CHLDC::BltFromGradient(
    CGradient2D                 *pGradient,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iAlpha )

/*============================================================================*/
{
    if (pGradient->IsSolidColor())
    {
        FillRect(pGradient->SolidColor(), iXDst, iYDst, iDX, iDY);
        return;
    }

    CHLRect *pClip = CreateActiveClip();

    iXDst += m_iXLeft;
    iYDst += m_iYTop;

    if (pClip != NULL)
    {
        pClip->Translate(m_iXLeft, m_iYTop);

        if (iXDst < pClip->XLeft())
        {
            int iXTrim = pClip->XLeft()-iXDst;
            iDX -= iXTrim;
            iXSrc += iXTrim;
            iXDst += iXTrim;
        }
        if (iYDst < pClip->YTop())
        {
            int iYTrim = pClip->YTop()-iYDst;
            iDY -= iYTrim;
            iYSrc += iYTrim;
            iYDst += iYTrim;
        }

        iDX = MIN(iDX, pClip->XRight()+1-iXDst);
        iDY = MIN(iDY, pClip->YBottom()+1-iYDst);

        delete pClip;
    }

    if (iDX <= 0 || iDY <= 0)
        return;


    if (iYSrc < 0)
    {
        iYDst -= iYSrc;
        iDY += iYSrc;
        iYSrc = 0;
    }

#ifdef HLDDRAW
	if (m_pSurface != NULL)
    {
        CHLDibDC *pRenderDIB = pGradient->GetGradientRenderDIB();
        if (pRenderDIB != NULL)
        {
            while (iDX > 0)
            {
                int iDXThis = __min(iDX, pRenderDIB->ImageDX());
                BltFromDibDC(iXDst, iYDst, pRenderDIB, 0, iYSrc, iDXThis, iDY);
                iXDst += iDXThis;
                iDX -= iDXThis;
            }
            return;
        }
    }
#endif

    iDY = __min(iDY, pGradient->Dimension()-iYSrc);
    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    if (iAlpha < 100)
    {
        BltToDibDC(&dib, iXDst-m_iXLeft, iYDst-m_iYTop);
    }
    dib.BltFromGradient(pGradient, 0, 0, iXSrc, iYSrc, iDX, iDY, iAlpha);
    BltFromDibDC(iXDst-m_iXLeft, iYDst-m_iYTop, &dib, 0, 0, iDX, iDY); 
}
/*============================================================================*/

    void                        CHLDC::BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    CRectList                   *pList )

/*============================================================================*/
{
    for (int i = 0; i < pList->NRects(); i++)
    {
        int iX = pList->X(i);
        int iY = pList->Y(i);
        int iDX = pList->DX(i);
        int iDY = pList->DY(i);
        BltFromDibDC(iXDest+iX, iYDest+iY, pDibDC, iX, iY, iDX, iDY);
    }
}
/*============================================================================*/

    void                        CHLDC::BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest )

/*============================================================================*/
{
    BltFromDibDC(iXDest, iYDest, pDibDC, 0, 0, pDibDC->ImageDX(), pDibDC->DY());
}
/*============================================================================*/

    void                        CHLDC::BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    CEdgeList                   *pEdgeList )

/*============================================================================*/
{
    CHLDibDC dib(pDibDC->ImageDX(), pDibDC->DY(), 0, 0, MEMORY_SYSTEM);
    BltToDibDC(&dib, iXDest, iYDest, 0, 0, pDibDC->ImageDX(), pDibDC->DY());
    dib.BltFromDibDC(pDibDC, 0, 0, iXSrc, iYSrc, pEdgeList);
    BltFromDibDC(&dib, iXDest, iYDest);
}
/*============================================================================*/

    void                        CHLDC::BltFromDibDC( 
    int                         iXDest,
    int                         iYDest,
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iFlags )

/*============================================================================*/
{
    #ifdef DEBUG_DRAW
        Sleep(100);
    #endif

    if (pDibDC->ParentDIB() != NULL)
    {
        iXSrc += pDibDC->GetOrigin().x;
        iYSrc += pDibDC->GetOrigin().y;
        BltFromDibDC(iXDest, iYDest, pDibDC->ParentDIB(), iXSrc, iYSrc, iDX, iDY, iFlags);
        return;
    }

    pDibDC->FlushText();

    if (iDX <= 0 || iDY <= 0)
        return;

    iXDest += m_iXLeft;
    iYDest += m_iYTop;

    START_TIMER(GDI_BITBLT_GDI);

    Clip(&iXDest, &iYDest, &iXSrc, &iYSrc, &iDX, &iDY);

    iDX = __min(iDX, pDibDC->ImageDX()-iXSrc);
    iDY = __min(iDY, pDibDC->DY()-iYSrc);

    if (iDX <= 0 || iDY <= 0)
        return;

    #ifdef HLDDRAW
        pDibDC->FlushText();


        HL_DIRECTDRAWSURFACE pThisSurface = m_pSurface;
        if (pDibDC->GetSurface() != NULL)
        {
            if (pThisSurface == NULL)
                pThisSurface = g_pGDIServer->GetPrimary();
        }

        if (pThisSurface != NULL)
        {
            RECT rDst = CDXYRect(iXDest, iYDest, iDX, iDY).GetRECT();
            RECT rSrc = CDXYRect(iXSrc,  iYSrc,  iDX, iDY).GetRECT();

            HL_DIRECTDRAWSURFACE pSource = pDibDC->GetSurface();

            if (pSource != NULL)
            {
                if (pSource->IsLost())
                    pSource->Restore();
                HRESULT hRes = 0;
                #ifdef HL_DDRAWBASIC
                    int iDDFlags = 0;
#ifndef HL_WIN32
                    if (iFlags & DIBATTRIB_WAITVSYNC)
                        iDDFlags = DDBLT_WAITNOTBUSY;
#endif
                    
                    hRes = pThisSurface->Blt(&rDst, pSource, &rSrc, iDDFlags, NULL);
                #else
                    if (!m_ClipStack.IsEmpty())
                        hRes = pThisSurface->Blt(&rDst, pSource, &rSrc, 0, NULL);
                    else
                        hRes = pThisSurface->BltFast(iXDest, iYDest, pSource, &rSrc, BLTFAST_FLAGS);
                #endif
                if (hRes != S_OK)
                {
                    CHLString sTrace;
                    sTrace.Format(_T("ERROR Line = %d RES = %08X"), __LINE__, hRes);
                    TraceWrap(sTrace);
                }

                return;
            }

            CHLDC dc(g_pMainWindow);
            dc.BltFromDibDC(iXDest, iYDest, pDibDC, iXSrc, iYSrc, iDX, iDY);
            return;
        }
    #endif


#ifndef IPHONE
    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);

    BOOL bHandled = FALSE;

    #if !defined(HL_X86A) & !defined(HL_X86B) & !defined(HL_ARMV4B)
        BYTE byInfo[sizeof(BITMAPINFOHEADER)+3*(sizeof(DWORD))];
        BITMAPINFO *pInfo = (BITMAPINFO*) byInfo;
        pInfo->bmiHeader.biSize           = sizeof(BITMAPINFOHEADER);// + 3 * sizeof(DWORD);
        pInfo->bmiHeader.biWidth          = pDibDC->DX();
        pInfo->bmiHeader.biHeight         = -pDibDC->DY();
        pInfo->bmiHeader.biPlanes         = 1;
        pInfo->bmiHeader.biBitCount       = 16;
        pInfo->bmiHeader.biCompression    = BI_BITFIELDS;
        pInfo->bmiHeader.biSizeImage      = 2 * pDibDC->DX() * pDibDC->DY();
        pInfo->bmiHeader.biXPelsPerMeter  = 10;
        pInfo->bmiHeader.biYPelsPerMeter  = 10;
        pInfo->bmiHeader.biClrUsed        = 3;
        pInfo->bmiHeader.biClrImportant   = 0;
        BYTE *pColors = byInfo + sizeof(BITMAPINFOHEADER);
        DWORD *pQ = (DWORD*) pColors;
        pQ[0] = (DWORD) 0xF800;     // 1111100000000000
        pQ[1] = (DWORD) 0x07E0;     // 0000011111100000
        pQ[2] = (DWORD) 0x001F;     // 0000000000011111
        if (m_bOutEnabled)
        {
            bHandled = ::SetDIBitsToDevice(hDC, iXDest, iYDest, iDX, iDY, iXSrc, pDibDC->DY()-iYSrc-iDY, 0, pDibDC->DY(), pDibDC->Bits(), pInfo, DIB_RGB_COLORS);
        }
    #endif

    if (!bHandled && m_bOutEnabled)
    {
        ::BitBlt(hDC, iXDest, iYDest, iDX, iDY, pDibDC->GetHDC(), iXSrc, iYSrc, SRCCOPY);
    }
#endif

#ifdef HLDDRAW
#endif

    END_TIMER(GDI_BITBLT_GDI);
}
/*============================================================================*/

    void                        CHLDC::BltToDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc )

/*============================================================================*/
{
    int iX = m_iXLeft + iXSrc;
    int iY = m_iYTop + iYSrc;

    CHLDibDC *pDibThis =DibDC();
    if (pDibThis != NULL)
    {
        pDibDC->BltFromDibDC(0, 0, (CHLDibDC*)pDibThis, iX, iY, pDibDC->DX(), pDibDC->DY());
        return;
    }

    #ifdef HLDDRAW
        HL_DIRECTDRAWSURFACE pSrc = m_pSurface;
        HL_DIRECTDRAWSURFACE pDst = pDibDC->GetSurface();
        if (pSrc != NULL && pDst != NULL)
        {
            BltToDibDC(pDibDC, iXSrc, iYSrc, 0, 0, pDibDC->ImageDX(), pDibDC->DY());
            return;
        }
    #endif

    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);

//    TRACE(_T("FIXME!!!\n"));
//    return;

    START_TIMER(GDI_BITBLT_GDI);
#ifndef IPHONE    
    ::BitBlt(pDibDC->GetHDC(), 0, 0, pDibDC->DX(), pDibDC->DY(), hDC, iX, iY, SRCCOPY);
#endif
    END_TIMER(GDI_BITBLT_GDI);
}
/*============================================================================*/

    void                        CHLDC::BltToDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    iXSrc += (m_iXLeft);
    iYSrc += (m_iYTop);

    CHLDibDC *pDibDCThis = DibDC();
    if (pDibDCThis != NULL)
    {
        pDibDC->BltFromDibDC(iXDest, iYDest, pDibDCThis, iXSrc, iYSrc, iDX, iDY);
        return;
    }

    #ifdef HLDDRAW
        if (iXSrc < 0)  {   iXDest -= iXSrc;    iXSrc = 0;  }
        if (iYSrc < 0)  {   iYDest -= iYSrc;    iYSrc = 0;  }
        if (iXDest < 0) {   iXSrc -= iXDest;    iXDest = 0; }
        if (iYDest < 0) {   iYSrc -= iYDest;    iYDest = 0; }

        if (iXSrc < 0)
        {
            iXDest -= iXSrc;
            iDX += iXSrc;
            iXSrc = 0;
        }
        if (iYSrc < 0)
        {
            iYDest -= iYSrc;
            iDY += iYSrc;
            iYSrc = 0;
        }

        int iDXMain = g_pMainWindow->DX();
        int iDYMain = g_pMainWindow->DY();
        iDX = __min(iDX, iDXMain - iXSrc);
        iDX = __min(iDX, pDibDC->ImageDX() - iXDest);
        iDY = __min(iDY, iDYMain - iYSrc);
        iDY = __min(iDY, pDibDC->DY() - iYDest);

        if (iDX <= 0 || iDY <= 0)
            return;

        HL_DIRECTDRAWSURFACE pSrc = m_pSurface;
        if (pSrc == NULL)        
            pSrc = g_pGDIServer->GetPrimary();
        HL_DIRECTDRAWSURFACE pDst = pDibDC->GetSurface();
        if (pSrc != NULL && pDst != NULL)
        {
            RECT rDst = CDXYRect(iXDest, iYDest, iDX, iDY).GetRECT();
            RECT rSrc = CDXYRect(iXSrc,  iYSrc,  iDX, iDY).GetRECT();
            HRESULT hRes = pDst->Blt(&rDst, pSrc, &rSrc, 0, NULL);
            return;
        }
    #endif

    HDC hDC = GetHDC();
    LOGASSERT(hDC != NULL);

    START_TIMER(GDI_BITBLT_GDI);
#ifndef IPHONE
    ::BitBlt(pDibDC->GetHDC(), iXDest, iYDest, iDX, iDY, hDC, iXSrc, iYSrc, SRCCOPY);
#endif
    END_TIMER(GDI_BITBLT_GDI);
}
/*========================================================================*/

    void                        CHLDC::MoveBlt(
    int                         iXSrc,
    int                         iYSrc,
    int                         iDXRect,
    int                         iDYRect,
    int                         iDXMove,
    int                         iDYMove )

/*========================================================================*/
{
#ifndef IPHONE
    Blt(iXSrc+iDXMove, iYSrc+iDYMove, this, iXSrc, iYSrc, iDXRect, iDYRect);
#endif
}
/*========================================================================*/

    COLORREF                    CHLDC::ScaleColor(
    double                      dScale,
    COLORREF                    rgbIn )

/*========================================================================*/
{
    //
    // Create a brightened or dimmed copy of our background color,
    // used for gradient based borders etc.
    //

    int iR = GetRValue(rgbIn) ;
    int iG = GetGValue(rgbIn) ;
    int iB = GetBValue(rgbIn) ;

    iR = (int) ((double) iR * dScale);
    iG = (int) ((double) iG * dScale);
    iB = (int) ((double) iB * dScale);

    iR = __min(iR, 255);
    iG = __min(iG, 255);
    iB = __min(iB, 255);
    iR = __max(iR, 0);
    iG = __max(iG, 0);
    iB = __max(iB, 0);

    return RGB(iR, iG, iB);
}
/*========================================================================*/

    COLORREF                    CHLDC::ScaleColorPCT(
    int                         iPCT,
    COLORREF                    rgbIn )

/*========================================================================*/
{
    //
    // Create a brightened or dimmed copy of our background color,
    // used for gradient based borders etc.
    //

    int iR = GetRValue(rgbIn) ;
    int iG = GetGValue(rgbIn) ;
    int iB = GetBValue(rgbIn) ;

    iR = iR * iPCT / 100;
    iG = iG * iPCT / 100;
    iB = iB * iPCT / 100;

    iR = __min(iR, 255);
    iG = __min(iG, 255);
    iB = __min(iB, 255);
    iR = __max(iR, 0);
    iG = __max(iG, 0);
    iB = __max(iB, 0);

    return RGB(iR, iG, iB);
}
/*========================================================================*/

    void                        CHLDC::CreateGradient(
    COLORREF                    rgbBase,
    int                         iD,
    COLORREF                    *pT,
    COLORREF                    *pB )

/*========================================================================*/
{
    int iR = GetRValue(rgbBase);
    int iG = GetGValue(rgbBase);
    int iB = GetBValue(rgbBase);
    int iRT = iR;
    int iGT = iG;
    int iBT = iB;
    int iRB = iR;
    int iGB = iG;
    int iBB = iB;
    
    if (iD > 0)
    {
        iRB = iR+iD;
        iGB = iG+iD;
        iBB = iB+iD;
        if (iRB > 255)  { iRT -= (iRB-255); iRB = 255;  }
        if (iGB > 255)  { iGT -= (iGB-255); iGB = 255;  }
        if (iBB > 255)  { iBT -= (iBB-255); iBB = 255;  }
    }
    else
    {   
        iRB = iR+iD;
        iGB = iG+iD;
        iBB = iB+iD;
        if (iRB < 0)  { iRT -= iRB; iRB = 0;  }
        if (iGB < 0)  { iGT -= iGB; iGB = 0;  }
        if (iBB < 0)  { iBT -= iBB; iBB = 0;  }
    }

    *pT = RGB(iRT, iGT, iBT);
    *pB = RGB(iRB, iGB, iBB);
}
/*========================================================================*/

    COLORREF                    CHLDC::Blend(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    double                      dD2D1 )

/*========================================================================*/
{
    float fR1 = (float)GetRValue(rgb1);
    float fG1 = (float)GetGValue(rgb1);
    float fB1 = (float)GetBValue(rgb1);
    float fR2 = (float)GetRValue(rgb2);
    float fG2 = (float)GetGValue(rgb2);
    float fB2 = (float)GetBValue(rgb2);

    double dR1 = (1.0 - dD2D1);
    double dR2 = dD2D1;

    float fRNew = (float) (fR1 * dR1 + fR2 * dR2);
    float fGNew = (float) (fG1 * dR1 + fG2 * dR2);
    float fBNew = (float) (fB1 * dR1 + fB2 * dR2);

    return RGB((int) fRNew, (int) fGNew, (int) fBNew);
}
/*========================================================================*/

    COLORREF                    CHLDC::BlendPCT(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iPCT2 )

/*========================================================================*/
{
    int iR1 = GetRValue(rgb1);
    int iG1 = GetGValue(rgb1);
    int iB1 = GetBValue(rgb1);
    int iR2 = GetRValue(rgb2);
    int iG2 = GetGValue(rgb2);
    int iB2 = GetBValue(rgb2);
    
    int iPCT1 = 100-iPCT2;
    int iR = (iR1 * iPCT1 + iR2 * iPCT2) / 100;
    int iG = (iG1 * iPCT1 + iG2 * iPCT2) / 100;
    int iB = (iB1 * iPCT1 + iB2 * iPCT2) / 100;

    return RGB(iR, iG, iB);
}
/*========================================================================*/

    COLORREF                    CHLDC::Add(
    COLORREF                    rgb,
    COLORREF                    rgbAdd )

/*========================================================================*/
{
    int iR = __min(255, GetRValue(rgb)+GetRValue(rgbAdd));
    int iG = __min(255, GetGValue(rgb)+GetGValue(rgbAdd));
    int iB = __min(255, GetBValue(rgb)+GetBValue(rgbAdd));
    return RGB(iR, iG, iB);
}
/*========================================================================*/

    CHLRect                     CHLDC::IntersectClip(
    CHLRect                     rIn )

/*========================================================================*/
{
    CHLRect *pClip = CreateActiveClip();
    if (pClip == NULL)
        return rIn;
    CHLRect rInt = rIn.IntersectWith(*pClip);
    delete pClip;
    return rInt;
}
/*========================================================================*/

    BOOL                        CHLDC::TestClip(
    CHLRect                     rIn )

/*========================================================================*/
{
    if (m_pClipGlobal != NULL)
    {
        rIn.Translate(m_iXLeft, m_iYTop);
        return m_pClipGlobal->IntersectsWith(&rIn);
    }

    return TRUE;
}
#ifndef IPHONE
/*========================================================================*/

    void                        CHLDC::PushOrigin(
    CHLWindow                   *pWnd )

/*========================================================================*/
{
    POINT *pNew = new POINT;
    pNew->x = pWnd->XScreen(0);
    pNew->y = pWnd->YScreen(0);
    m_OriginStack.AddTail(pNew);        
    SetOrigin(pWnd);
}
#endif
/*========================================================================*/

    void                        CHLDC::PopOrigin()

/*========================================================================*/
{
    LOGASSERT(!m_OriginStack.IsEmpty());
    POINT *pPt = (POINT*)m_OriginStack.RemoveTail();
    delete pPt;

    if (m_OriginStack.IsEmpty())
        return;
    pPt = (POINT*)m_OriginStack.GetTail();
    SetOrigin(pPt->x, pPt->y);
}
/*========================================================================*/

    void                        CHLDC::PushClipDXDY(
    int                         iX0, 
    int                         iY0, 
    int                         iDX, 
    int                         iDY)

/*========================================================================*/
{
    CDXYRect rClip(iX0, iY0, iDX, iDY);
    PushClip(&rClip);
}
/*========================================================================*/

    void                        CHLDC::PushClip12(
    int                         iX0, 
    int                         iY0, 
    int                         iX1, 
    int                         iY1)

/*========================================================================*/
{
    CX12Rect rClip(iX0, iY0, iX1, iY1);
    PushClip(&rClip);
}
/*========================================================================*/

    void                        CHLDC::PushClip(
    CHLRect                     *pClip )

/*========================================================================*/
{
    FlushText();

    CHLRect *pNew = NULL;
    if (pClip != NULL)
    {
        pNew = new CHLRect(*pClip);
        pNew->Translate(m_iXLeft, m_iYTop);
    }
    m_ClipStack.AddHead(pNew);
    SetActiveClip(pNew);
}
/*========================================================================*/

    void                        CHLDC::PopClip()

/*========================================================================*/
{
    LOGASSERT(!m_ClipStack.IsEmpty());
    if (!m_ClipStack.IsEmpty())
    {
        CHLRect *pRect = (CHLRect*)m_ClipStack.RemoveHead();
        delete pRect;
    }

    if (m_ClipStack.IsEmpty())
        SetActiveClip(NULL);
    else
        SetActiveClip((CHLRect*)m_ClipStack.GetHead());
}
#ifndef IPHONE
/*====================================================================*/

    CHLString                   CHLDC::TruncateText(
    int                         iTextSize, 
    CHLString                   sText,
    int                         iMaxWidth )

/*====================================================================*/
{
    START_TIMER(GDI_TEXT);
    TCHAR pDots [] = { '.', '.', '.', NULL };
    SIZE size = g_pFontServer->GetTextExtent(iTextSize, pDots, 3);

    int iSizeDots = size.cx;

    int iLen = sText.GetLength();
    if (iLen == 0)
    {
        END_TIMER(GDI_TEXT);
        return _T("");
    }

    TCHAR *pTarget = NULL;
    CHLString sReturn;

    TCHAR *pBuf = (TCHAR*) sText.GetBufferTCHAR(iLen);
    size = g_pFontServer->GetTextExtent(iTextSize, pBuf, iLen);

    if (size.cx <= iMaxWidth)
    {
        sText.ReleaseBuffer(iLen);
        return sText;
    }

    while (size.cx > (iMaxWidth-iSizeDots) && iLen > 0)
    {
        iLen--;
        size = g_pFontServer->GetTextExtent(iTextSize, pBuf, iLen);
    }

    pTarget = (TCHAR*) sReturn.GetBufferTCHAR(iLen+4);
    memcpy(pTarget, pBuf, iLen * sizeof(TCHAR));
    pTarget[iLen] = _T('.');
    pTarget[iLen+1] = _T('.');
    pTarget[iLen+2] = _T('.');
    pTarget[iLen+3] = 0;
    sReturn.ReleaseBuffer(iLen+3);
    END_TIMER(GDI_TEXT);
    return sReturn;
}
/*====================================================================*/

    void                        CHLDC::TruncateText(
    int                         iTextSize, 
    TCHAR                       *pText,
    int                         *piNChars,
    int                         iMaxWidth )

/*====================================================================*/
{
    TCHAR *pTarget = NULL;
    TCHAR pDots [] = { '.', '.', '.', NULL };
    START_TIMER(GDI_TEXT);
    SIZE size = g_pFontServer->GetTextExtent(iTextSize, pDots, 3);
    int iSizeDots = size.cx;

    int iLen = *piNChars;
    if (iLen < 4)
    {
        END_TIMER(GDI_TEXT);
        return;
    }

    size = g_pFontServer->GetTextExtent(iTextSize, pText, iLen);

    if (size.cx< iMaxWidth)
    {
        END_TIMER(GDI_TEXT);
        return;
    }

    while (size.cx > (iMaxWidth-iSizeDots) && iLen > 0)
    {
        iLen--;
        size = g_pFontServer->GetTextExtent(iTextSize, pText, iLen);
    }

    pTarget = pText+iLen+3;
    *pTarget = 0;   pTarget--;
    *pTarget = '.';   pTarget--;
    *pTarget = '.';   pTarget--;
    *pTarget = '.';
    *piNChars = iLen+3;
    END_TIMER(GDI_TEXT);
}
/*====================================================================*/

    CHLString                   CHLDC::SplitText(
    int                         iTextSize, 
    CHLString                   sIn,
    int                         iDXMax,
    int                         iSearchSpace,
    int                         iMaxLines )

/*====================================================================*/
{
    int iNLines = 0;

/*
    #ifdef HLOSD
        CHLObList Lines;
        int iLen = sIn.GetLength();
        char *pBuf = sIn.GetBufferCHAR(iLen);
        while (iLen > 0 && iNLines < iMaxLines)
        {
            BOOL bSplit = FALSE;
            for (int i = 0; i < iLen && !bSplit; i++)
            {
                if (pBuf[i] == '\n' || i == iLen-1)
                {
                    int iLenThisLine = i;
                    if (i == (iLen-1))
                        iLenThisLine = iLen;

                    CHLString sThisLine;
                    char *pThisBuf = sThisLine.GetBufferCHAR(iLenThisLine);
                    memcpy(pThisBuf, pBuf, iLenThisLine);
                    sThisLine.ReleaseBuffer(iLenThisLine);                
                    int iNLinesThis = iMaxLines - iNLines;
                    sThisLine = SplitText(iTextSize, sThisLine, iDXMax, iSearchSpace, &iNLinesThis);
                    iNLines += iNLinesThis;
                    Lines.AddTail(new CHLString(sThisLine));
                    iLenThisLine++;
                    iLenThisLine = __min(iLen, iLenThisLine);
                    iLen -= iLenThisLine;
                    memmove(pBuf, pBuf+iLenThisLine, iLen);
                    bSplit = TRUE;
                }
            }
        }

        CHLString sRet;
        CHLString *pLine1 = (CHLString*)Lines.RemoveHead();
        sRet.Format(_T("%s"), (const TCHAR*)*pLine1);
        delete pLine1;
        while (!Lines.IsEmpty())
        {
            CHLString *pLine = (CHLString*)Lines.RemoveHead();
            CHLString sTemp = sRet;
            sRet.Format(_T("%s\n%s"), (const TCHAR*) sTemp, (const TCHAR*)*pLine);
            delete pLine;
        }

        return sRet;
    #endif
*/

    return SplitText(iTextSize, sIn, iDXMax, iSearchSpace, &iMaxLines);
}
/*====================================================================*/

    CHLString                   CHLDC::SplitText(
    int                         iTextSize,
    CHLString                   sIn,
    int                         iDXMax,
    int                         iSearchSpace,
    int                         *piMaxLines )

/*====================================================================*/
{
    CHLString sRight = sIn;
    CHLString sLeft  = _T("");
    BOOL bFirst = TRUE;
    int iNLines = 1;
    int iMaxLines = 0;
    if (piMaxLines != NULL)
        iMaxLines = *piMaxLines;

    while (sRight.GetLength() > 0)
    {
        int iLeft = Split(iTextSize, sRight, iDXMax, iSearchSpace);
        if (iLeft == 0)
            return sLeft;

        CHLString sLast = sLeft;
        if (!bFirst)
            sLeft.Format(_T("%s\n%s"), (const TCHAR*)sLast, (const TCHAR*)sRight.Left(iLeft));
        else
            sLeft.Format(_T("%s%s"), (const TCHAR*)sLast, (const TCHAR*)sRight.Left(iLeft));

        bFirst = FALSE;

        int iRight = sRight.GetLength() - iLeft;
        if (iRight > 0)
            iNLines++;

        if (iMaxLines > 0)
        {
            if (iNLines > iMaxLines)
            {
                iNLines = iMaxLines;
                iRight = 0;
            }
        }
        
        sRight = sRight.Right(iRight);
        int iLenRight = sRight.GetLength();
        TCHAR *pBuf = sRight.GetBufferTCHAR(iLenRight);
        while (pBuf[0] == ' ' && iLenRight > 0)
        {
            iLenRight--;
            memmove(pBuf, pBuf+1, iLenRight * sizeof(TCHAR));
        }
        sRight.ReleaseBuffer(iLenRight);
    }

    if (piMaxLines != NULL)
        *piMaxLines = iNLines;

    return sLeft;
}
/*============================================================================*/

    int                         CHLDC::Split(
    int                         iTextSize, 
    CHLString                   sIn,
    int                         iDXMax,
    int                         iSearchSpace )

/*============================================================================*/
{
    int iLen = sIn.GetLength();
    if (iLen == 0)
    {
        LOGASSERT(FALSE);
        return 0;
    }

    TCHAR *pTarget = NULL;
    CHLString sReturn;

    TCHAR *pBuf = (TCHAR*) sIn.GetBufferTCHAR(iLen);

    START_TIMER(GDI_TEXT);

    SIZE size = g_pFontServer->GetTextExtent(iTextSize, pBuf, iLen);

    if (size.cx< iDXMax)
    {
        // Whole thing fits
        sIn.ReleaseBuffer(iLen);
        return iLen;
    }

    int iLenRet = g_pFontServer->MaxCharsInLine(iTextSize, pBuf, iLen, iDXMax, iSearchSpace);

    sIn.ReleaseBuffer(iLen);
    END_TIMER(GDI_TEXT);
    return iLenRet;
}
#endif
/*========================================================================*/

    void                        CHLDC::FlushText()

/*========================================================================*/
{
    if (m_TextOut.IsEmpty())
        return;

    POINT ptOrg;
    ptOrg.x = m_iXLeft;
    ptOrg.y = m_iYTop;

#ifndef IPHONE
    int iTextSize = m_iFontHeight;
    COLORREF rgb     = m_rgbColor;
#endif

    CHLRect *pClip = NULL;
    if (m_hDC == NULL && !m_ClipStack.IsEmpty())
    {
        pClip = (CHLRect*)m_ClipStack.GetHead();
    }

#ifndef IPHONE
    HDC hDC = GetHDC();
#endif

    if (pClip != NULL)
    {
        SetActiveClip(pClip);
    }

    while (!m_TextOut.IsEmpty())
    {
        CTextOut *pText = (CTextOut*)m_TextOut.RemoveHead();
        m_iXLeft = pText->XOrigin();
        m_iYTop = pText->YOrigin();
        SetColor(pText->Color());
#ifndef IPHONE
        SetQuickFont(pText->TextSize());
        DrawString(pText->Text(), pText->Rect(), pText->Format());
#endif

        delete pText;
    }

#ifndef IPHONE
    SetQuickFont(iTextSize);
    SetColor(rgb);
#endif

    m_iXLeft = ptOrg.x;
    m_iYTop = ptOrg.y;
}
/*========================================================================*/

    void                        CHLDC::SetActiveClip(
    CHLRect                     *pClip )

/*========================================================================*/
{
    #ifdef HLDDRAW
    if (m_pSurface != NULL && m_hDC == NULL)
    {
        return;
    }
    #endif

    #ifdef HLDDRAW
        return;
    #endif

#ifndef IPHONE
    // Remove old if any
    HDC hDC = GetHDC();
    
    // Convert to screen coords (or local coords for dibdc)
    BOOL bClip = FALSE;
    CHLRect rClip;

    if (pClip == NULL)
    {
        if (m_pClipGlobal != NULL)
        {
            CHLRect rGlobal = *m_pClipGlobal;
            if (DibDC() != NULL)
            {
                // Make rGlobal into our coords
                POINT ptAbs = GetOriginAbs();
                ptAbs.x -= m_iXLeft;
                ptAbs.y -= m_iYTop;
                rGlobal.Translate(-ptAbs.x, -ptAbs.y);
            }
            rClip = rGlobal;
            bClip = TRUE;
        }
    }
    else
    {
        rClip = *pClip;

        if (m_pClipGlobal != NULL)
        {
            // m_rGlobalClip are in screen coords
            CHLRect rGlobal = *m_pClipGlobal;

            // Make rGlobal into client coords
            if (DibDC() != NULL)
            {
                // Make rGlobal into our coords
                POINT ptAbs = GetOriginAbs();
                ptAbs.x -= m_iXLeft;
                ptAbs.y -= m_iYTop;
                rGlobal.Translate(-ptAbs.x, -ptAbs.y);
            }

            rClip.XLeft(    __max(rClip.XLeft(),    rGlobal.XLeft()));
            rClip.XRight(   __min(rClip.XRight(),   rGlobal.XRight()));
            rClip.YTop(     __max(rClip.YTop(),     rGlobal.YTop()));
            rClip.YBottom(  __min(rClip.YBottom(),  rGlobal.YBottom()));
        }

        bClip = TRUE;
    }

    HRGN hClip = NULL;
    if (bClip)
    {
        hClip = CreateRectRgn(rClip.XLeft(), rClip.YTop(), rClip.XRight()+1, rClip.YBottom()+1);
    }

    SelectClipRgn(hDC, hClip);

    if (hClip != NULL)
    {
        DeleteObject(hClip);
    }
#endif
}
/*========================================================================*/

    CHLRect                     *CHLDC::CreateActiveClip()

/*========================================================================*/
{
    if (m_ClipStack.IsEmpty())
        return NULL;
    CHLRect *pRect = (CHLRect*)m_ClipStack.GetHead();
    if (pRect != NULL)
    {
        CHLRect *pNew = new CHLRect(pRect);

        if (m_pClipGlobal != NULL)
        {
            // m_rGlobalClip are in screen coords
            CHLRect rGlobal = *m_pClipGlobal;

            // Make rGlobal into client coords
            if (DibDC() != NULL)
            {
                // Make rGlobal into our coords
                POINT ptAbs = GetOriginAbs();
                ptAbs.x -= m_iXLeft;
                ptAbs.y -= m_iYTop;
                rGlobal.Translate(-ptAbs.x, -ptAbs.y);
            }

            pNew->XLeft(    __max(pNew->XLeft(),    rGlobal.XLeft()));
            pNew->XRight(   __min(pNew->XRight(),   rGlobal.XRight()));
            pNew->YTop(     __max(pNew->YTop(),     rGlobal.YTop()));
            pNew->YBottom(  __min(pNew->YBottom(),  rGlobal.YBottom()));
        }

        pNew->Translate(-m_iXLeft, -m_iYTop);

        return pNew;
    }

    return NULL;
}
/*========================================================================*/

    void                        CHLDC::Clip(
    int                         *piXDest,
    int                         *piYDest,
    int                         *piXOther,
    int                         *piYOther,
    int                         *piDX,
    int                         *piDY )

/*========================================================================*/
{
#ifndef IPHONE
    int iXMin = 0;
    int iYMin = 0;
    int iDXMax = g_pMainWindow->DX();
    int iDYMax = g_pMainWindow->DY();
    int iXDest  = 0;
    int iYDest  = 0;
    int iDX     = 0;
    int iDY     = 0;
    int iXOther = 0;
    int iYOther = 0;

    if (piXDest != NULL)    iXDest  = *piXDest;
    if (piYDest != NULL)    iYDest  = *piYDest;
    if (piXOther != NULL)   iXOther = *piXOther;
    if (piYOther != NULL)   iYOther = *piYOther;
    if (piDX != NULL)       iDX = *piDX;
    if (piDY != NULL)       iDY = *piDY;

    if (!m_ClipStack.IsEmpty())
    {
        CHLRect *pClip = (CHLRect*)m_ClipStack.GetHead();
        if (pClip != NULL)
        {
            iXMin = __max(iXMin, pClip->XLeft());
            iYMin = __max(iYMin, pClip->YTop());
            iDXMax = __min(iDXMax, pClip->XRight()+1);    
            iDYMax = __min(iDYMax, pClip->YBottom()+1);    
        }
    }

    if (iXOther < 0)
    {
        iDX    += iXOther;
        iXDest -= iXOther;
        iXOther = 0;
    }
    if (iYOther < 0)
    {
        iDY    += iYOther;
        iYDest -= iYOther;
        iYOther = 0;
    }

    if (iXDest < iXMin)
    {
        iDX     -= (iXMin-iXDest);
        iXOther += (iXMin-iXDest);
        iXDest   = iXMin;
    }   
    if (iYDest < iYMin)
    {
        iDY     -= (iYMin-iYDest);
        iYOther += (iYMin-iYDest);
        iYDest   = iYMin;
    }
    iDX = __min(iDX, iDXMax-iXDest);
    iDY = __min(iDY, iDYMax-iYDest);

    if (piXDest != NULL)    *piXDest  = iXDest;
    if (piYDest != NULL)    *piYDest  = iYDest;
    if (piXOther != NULL)   *piXOther = iXOther;
    if (piYOther != NULL)   *piYOther = iYOther;
    if (piDX != NULL)       *piDX = iDX;
    if (piDY != NULL)       *piDY = iDY;
#endif
}