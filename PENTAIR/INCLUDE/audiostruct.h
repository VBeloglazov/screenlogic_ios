/*
--------------------------------------------------------------------------------

    AUDIOSTRUCT.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifndef AUDIOSTRUCT_H
#define AUDIOSTRUCT_H

/*============================================================================*/

// Audio Database structures

/*============================================================================*/

#define NBITMASK_DWORD              4

/*============================================================================*/

typedef struct
{
    int                         m_iNameID;
    int                         m_iPathID;
    int                         m_iNSeconds;
    int                         m_iKBitrate;
    int                         m_iTrackNo;
    int                         m_iType;            // track format= AUDIO_TRACK_MP3, _WAV, _WMA, etc.
    int                         m_iShareID;         // share this track resides on, CAudioSystems can check if it can play tracks from that share
    int                         m_iDBTagID;         // tag that is meaniful in the database that the track is stored in, e.g, Imerge needs "MediaID" to access track
    DWORD                       m_dwGenres[NBITMASK_DWORD];

} HLT_TRACK;

/*============================================================================*/

typedef struct
{
    int                         m_iNameID;
    int                         m_iDBTagID;
    int                         m_iTrackTypes;
    int                         m_iArtworkStatus;
    int                         m_iCoverState;

    HLT_TRACK                   *m_pTracks;
    int                         m_iNTracks;
    int                         m_iNTracksAlloc;
} HLT_ALBUM;

/*============================================================================*/

typedef struct
{
    int                         m_iNameID;
    int                         m_iTrackTypes;

    HLT_ALBUM                   *m_pAlbums;
    int                         m_iNAlbums;
    int                         m_iNAlbumsAlloc;
} HLT_ARTIST;

/*============================================================================*/

typedef struct
{
    int                         m_iArtist;
    int                         m_iAlbum;
    int                         m_iTrack;

} HLT_PLAYLIST_ITEM;

/*============================================================================*/

/*============================================================================*/

typedef struct
{
    int                         m_iPathID;
    int                         m_iArtist;
    int                         m_iAlbum;
    int                         m_iTrack;
    int                         m_iFlags;
    int                         m_iTextID1;
    int                         m_iTextID2;
    int                         m_iTextID3;

    DWORD                       m_dwGenres[NBITMASK_DWORD];
    BYTE                        m_byData;

    int                         m_iTrackTypes;

    HLT_PLAYLIST_ITEM           *m_pSubItems;
    int                         m_iNSubItems;
    int                         m_iNSubItemsAlloc;
} HLT_SORT_ITEM;

/*============================================================================*/

#endif


