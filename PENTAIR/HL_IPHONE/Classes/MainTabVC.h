//
//  MainTabVC.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#define DY_PAGE_CONTROL                 35
#define DY_PAGE_SPACING                 5

#import <UIKit/UIKit.h>
#import "IPOD_ViewController.h"

@class CMainTab;
@class CSubTabHeader;
@class CSubSystemPage;
@class CHLPageControl;
@class CToolBarButton;
@class CHLToolBar;

/*============================================================================*/

@interface CMainTabViewController : UIViewController < UIScrollViewDelegate >

/*============================================================================*/
{
    CToolBarButton                      *m_pHomeButton;
    BOOL                                m_bHideHomeButton;

    UIScrollView                        *m_pScrollView;
    UIView                              *m_pModalView;

    UIPageControl                       *m_pPageControl;
    CHLPageControl                      *m_pHLPageControl;
    UILabel                             *m_pZoneLabel;

    CMainTab                            *m_pMainTab;
    BOOL                                m_bInit;
    NSMutableArray                      *m_pPages;
    CGRect                              m_rOrig;
    CHLToolBar                          *m_pToolBar;
    BOOL                                m_bHideNavBar;

    BOOL                                m_bPageControlBusy;
    int                                 m_iTargetPage;

    UIImageView                         *m_pBackground;
    BOOL                                m_bInRotation;
    BOOL                                m_bEnableScroll;
    
    UIView                              *m_pLabelsOverlay;
    BOOL                                m_bLabels;
    IPOD_ViewController                 *m_pMainViewController;
}

-(id)initWithMainTab:(CMainTab*)pMainTab Background:(UIImageView*)pBackground;

-(CMainTab*)MainTab;

-(void)PopSelf;
-(void)PageLeftRight:(int)iDir;
-(void)InitMain;
-(IBAction)ChangePage:(id)sender;
-(void)LabelsFadeOut:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;

-(void)UpdateVisiblePages;
-(void)InitZoneLabel;
-(void)LoadTab:(int)iIndex;
-(void)LockHorizontalScroll:(BOOL)bNew;
-(BOOL)IsVisible:(CSubSystemPage*)pPage;
-(BOOL)EnableScroll;
-(void)EnableHomeButton:(BOOL)bNew;
-(CHLToolBar*)ToolBar;
-(UIView*)ContentView;
-(void)UpdateScrollSubViews;
-(void)HideNavBar:(BOOL)bNew;
-(BOOL)HideNavBar;
-(void)longPress:(UILongPressGestureRecognizer*)gesture;
-(void)SetMainViewController:(IPOD_ViewController*)mainViewController;

@end

/*============================================================================*/

@interface CSubSystemView : UIView

/*============================================================================*/
{
    CMainTabViewController                *m_pPage;
    int                         m_iDXLast;
    int                         m_iDYLast;
}

-(id)initWithFrame:(CGRect)rFrame MainTabPage:(CMainTabViewController*)pPage;

@end
