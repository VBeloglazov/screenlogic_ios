//
//  HLTableFrame.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "HLTableFrame.h"
#import "TableTitleHeaderView.h"
#import "MainView.h"
#import "crect.h"
#import "hlm.h"
#import "hlcomm.h"
#import "shader.h"
#import "CustomPage.h"

#define HEADER_SIZE                 40

@implementation CHLTableFrame

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame DataSource:(id)pSrc Delegate:(id)pDelegate

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        self.contentMode = UIViewContentModeRedraw;

        m_pTableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        // set the autoresizing mask so that the table will always fill the view
        m_pTableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
        
        // set the tableview delegate to this object and the datasource to the datasource which has already been set
        m_pTableView.delegate = pDelegate;
        m_pTableView.dataSource = pSrc;
        m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        m_pTableView.rowHeight = 40;
        m_pTableView.exclusiveTouch = FALSE;
        m_pTableView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:m_pTableView];
        
        [m_pTableView release];

        m_iRadius = [CHLComm GetInt:INT_TAB_ROUT];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            m_iRadius = 0;
        }

        self.layer.cornerRadius = MAX(m_iRadius-1, 0);
        self.clipsToBounds = YES;


        if (rFrame.size.height > 0 && rFrame.size.width > 0)
        {
            [self displayLayer:self.layer];
        }
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame View:(UIView*)pView

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        self.contentMode = UIViewContentModeRedraw;
        [self addSubview:pView];
        m_pView = pView;

        m_iRadius = [CHLComm GetInt:INT_TAB_ROUT];
        self.layer.cornerRadius = MAX(0, m_iRadius-1);
        self.clipsToBounds = YES;

        if (rFrame.size.height > 0 && rFrame.size.width > 0)
        {
            [self displayLayer:self.layer];
        }
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    //DL
    rThis.origin.y += 20;
    
    [CRect Inset:&rThis DX:1 DY:1];
    [m_pTableView setFrame:rThis];
    [m_pEmptyLabel setFrame:rThis];
    [CRect Inset:&rThis DX:m_iRadius DY:0];
    [m_pView setFrame:rThis];
}
/*============================================================================*/

-(void)ReloadData

/*============================================================================*/
{
    [m_pTableView reloadData];
}
/*============================================================================*/

-(void)ClearEmpty

/*============================================================================*/
{
    [m_pEmptyLabel removeFromSuperview];
    m_pEmptyLabel = NULL;
}
/*============================================================================*/

-(void)SetEmpty:(NSString*)pText

/*============================================================================*/
{
    if (m_pEmptyLabel != NULL)
    {
        [m_pEmptyLabel setText:pText];
        return;
    }
    
    m_pEmptyLabel = [[UILabel alloc] initWithFrame:self.bounds];
    m_pEmptyLabel.textAlignment = UITextAlignmentCenter;
    [CCustomPage InitStandardLabel:m_pEmptyLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
    [self addSubview:m_pEmptyLabel];
    [m_pEmptyLabel release];
}
/*============================================================================*/

-(UITableView*)TableView

/*============================================================================*/
{
    return m_pTableView;
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    int iEdges = RREGION_EDGE_TOP|RREGION_EDGE_BOTTOM;
    unsigned int rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_TOP]];
    unsigned int rgbB = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_BTM]];
    unsigned int rgbM = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_MID]];
    unsigned int rgbBG = [CShader MakeRGB:[CHLComm GetRGB:RGB_LIST_AREA]];


    unsigned int *prgbT = &rgbT;
    unsigned int *prgbM = &rgbM;
    
    int iFlags =[CHLComm GetInt:INT_TOPBARFLAGS];
    if ((iFlags & TOPBAR_FLAG_ENABLE_PAGETAB_RGB3) == 0)
        prgbM = NULL;
    if ((iFlags & TOPBAR_FLAG_ENABLE_PAGETAB_RGB2) == 0)
        prgbT = NULL;

    CGImageRef pRef = [CShader CreateRegionImageDX:iDX DY:iDY Rad:m_iRadius RoundedEdges:iEdges ColorT:prgbT ColorM:prgbM ColorB:rgbB FillCenter:YES FillColor:rgbBG Modifier:NULL SolidBG:NULL];
    [layer setContents:(id)pRef];
    CGImageRelease(pRef);
}
/*============================================================================*/

-(int)Radius

/*============================================================================*/
{
    return m_iRadius;
}
/*============================================================================*/

-(void)Radius:(int)iNew

/*============================================================================*/
{
    m_iRadius = iNew;
    self.layer.cornerRadius = MAX(m_iRadius-1, 0);
}

@end
