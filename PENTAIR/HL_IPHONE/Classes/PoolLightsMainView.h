//
//  PoolLightsMainView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CNSPoolConfig;
@class CHLTableFrame;
@class CPoolLightsColorMainView;

/*============================================================================*/

@interface CPoolLightsMainView : UIView 

/*============================================================================*/
{
    CHLTableFrame               *m_pLightsView;
    CPoolLightsColorMainView    *m_pColorsView;
}

-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig;

@end
