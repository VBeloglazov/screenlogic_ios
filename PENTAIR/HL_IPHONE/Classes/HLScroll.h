//
//  HLScroll.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/3/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMainTabViewController;

/*============================================================================*/

@protocol CHLExclusiveTouchReceiver

/*============================================================================*/

-(BOOL)WantExclusiveTouch;

@end

/*============================================================================*/

@interface CHLScrollView : UIScrollView

/*============================================================================*/
{
    CMainTabViewController                    *m_pMainTabPage;
}

-(id)initWithFrame:(CGRect)frame MainTabPage:(CMainTabViewController*)pMainTabPage;

- (UIView *)FindExclusiveView:(NSArray*)pSubViews WithPoint:(CGPoint)point Event:(UIEvent*)event;


@end
