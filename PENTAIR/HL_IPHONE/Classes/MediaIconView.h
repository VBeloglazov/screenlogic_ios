//
//  MediaIconView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/14/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CChannelCell;

/*============================================================================*/

    @interface CMediaIconView : UIControl

/*============================================================================*/
{
    UIImage                         *m_pImage;
    NSString                        *m_pIconName;
    CChannelCell                    *m_pCell;
    int                             m_iIndex;
    UIView                          *m_pOverlay;
}

-(void)InitImageX:(int)iDimX Y:(int)iDimY;
-(void)SetUserIcon:(NSString*) pNew;
-(void)SetView:(CChannelCell*)pView Index:(int)iIndex;

@end
