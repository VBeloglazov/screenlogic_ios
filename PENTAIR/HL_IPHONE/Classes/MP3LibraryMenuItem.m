//
//  MP3LibraryMenuItem.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MP3LibraryMenuItem.h"
#import "MP3ItemCell.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "AudioZone.h"
#import "hlcomm.h"

@implementation CMP3LibraryMenuItem

/*============================================================================*/

-(id)initWithData:(NSString*)pName Data:(int)iData Section:(int)iSection Row:(int)iRow Index:(int)iIndex Menu:(CMP3Menu*)pMenu

/*============================================================================*/
{
    if (self == [super initWithName:pName Data:iData Service:NULL])
    {
        m_pMenu = pMenu;
        m_iIndex = iIndex;
        m_iSection = iSection;
        m_iRow = iRow;
        m_iDataState = DATASTATE_INIT;
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_iDataState == DATASTATE_WAIT)
    {
        [CHLComm RemoveSocketSink:self];
    }

    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_AUDIO_GETQUICKSORTITEMA:
        {
            [m_pText release];
            [m_pAltText release];
            m_pAltText = NULL;

            m_pText = [[pMSG GetString] retain];
        }
        break;

    case HLM_AUDIO_GETSORTITEMA:
        {
            m_iArtistIndex = [pMSG GetInt];
            m_iAlbumIndex = [pMSG GetInt];
            m_iTrackIndex = [pMSG GetInt];
            [m_pText release];
            [m_pAltText release];
            m_pAltText = NULL;

            m_pText = [[pMSG GetString] retain];
            int iLen = [m_pText length];
            BOOL bTabFound = FALSE;
            for (int i = 0; i < iLen && !bTabFound; i++)
            {
                if ([m_pText characterAtIndex:i] == '\t')
                {
                    bTabFound = TRUE;
                    m_pAltText = [m_pText substringFromIndex:i+1];
                    [m_pAltText retain];
                    NSString *pTemp = m_pText;
                    m_pText = [pTemp substringToIndex:i];
                    [m_pText retain];
                    [pTemp release];
                }
            }
        }
        break;
        
    default:
        return;
    }

    [m_pMenu SetPacketsInFlight:[m_pMenu GetPacketsInFlight]-1];
    m_iDataState = DATASTATE_OK;
    [CHLComm RemoveSocketSink:self];

    CMP3ItemCell *pCell = [m_pMenu MP3CellForSection:m_iSection Index:m_iRow];
    if (pCell != NULL)
    {
        [pCell.m_pLabel setText:m_pText];
        [pCell SetAltText:m_pAltText];
        [m_pMenu setNeedsDisplay];
    }
    else
    {
    }
}
/*============================================================================*/

-(BOOL)OnIdleVisible

/*============================================================================*/
{
    if (m_iDataState == DATASTATE_INIT)
    {
        [m_pMenu SetPacketsInFlight:[m_pMenu GetPacketsInFlight]+1];
        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETSORTITEMQ];
        [pQ PutInt:[m_pMenu SortType]];
        [pQ PutInt:[m_pMenu SortSubType1]];
        [pQ PutInt:[m_pMenu SortSubType2]];
        [pQ PutInt:m_iIndex];
        [pQ PutInt:[[m_pMenu Zone] ActiveSourceID]];
        [pQ SendMessageWithMyID:self];
        [pQ release];
        m_iDataState = DATASTATE_WAIT;
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(NSString*)Text

/*============================================================================*/
{
    if ([m_pMenu GetPacketsInFlight] < 10 && m_iDataState == DATASTATE_INIT)
    {
        [self OnIdleVisible];
    }
    
    return [super Text];
}
/*============================================================================*/

-(int)Index                     { return m_iIndex;          }
-(int)ArtistIndex               { return m_iArtistIndex;    }
-(int)AlbumIndex                { return m_iAlbumIndex;     }
-(int)TrackIndex                { return m_iTrackIndex;     }

/*============================================================================*/

@end
