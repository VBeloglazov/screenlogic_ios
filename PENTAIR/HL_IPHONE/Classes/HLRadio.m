//
//  HLRadio.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/21/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HLRadio.h"
#import "hlbutton.h"

@implementation CHLRadio


/*============================================================================*/

-(id)initWithFrame: (CGRect)rView StringList:(NSArray*)pStrings NCols:(int)iNCols

/*============================================================================*/
{
    if (self = [super initWithFrame:rView])
    {
        m_iSelectedIndex = -1;
        m_iNCols = iNCols;
        int iNRows = 1;
        while ((iNRows * iNCols) < [pStrings count])
        {
            iNRows++;
        }

        int iInset = 4;
        m_pButtons = [[NSMutableArray alloc] init];
        int iDXBtn = (rView.size.width + 2 * iInset) / iNCols;
        int iDYBtn = (rView.size.height + 2 * iInset) / iNRows;

        int iCol = 0;
        int iRow = 0;
        for (int i = 0; i < [pStrings count]; i++)
        {
            CGRect rBtn = CGRectMake(iDXBtn * iCol, iDYBtn * iRow, iDXBtn - 2 * iInset, iDYBtn - 2 * iInset);
            NSString *pText = (NSString*)[pStrings objectAtIndex:i];
            CHLButton *pBtn = [[CHLButton alloc] initWithFrame:rBtn Text:pText];
            [pBtn SetCommandID:i Responder:self];
            [m_pButtons addObject:pBtn];
            [self addSubview:pBtn];

            iCol++;
            if (iCol >= iNCols)
            {
                iRow++;
                iCol = 0;
            }
        }
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iNRows = 1;
    while ((iNRows * m_iNCols) < [m_pButtons count])
    {
        iNRows++;
    }

    CGRect rView = self.bounds;
    int iInset = 4;

    if (rView.size.height <= 8 || rView.size.width <= 8)
    {
        iInset = 0;
    }

    int iDXBtn = (rView.size.width + 2 * iInset) / m_iNCols;
    int iDYBtn = (rView.size.height + 2 * iInset) / iNRows;

    int iCol = 0;
    int iRow = 0;
    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CGRect rBtn = CGRectMake(iDXBtn * iCol, iDYBtn * iRow, iDXBtn - 2 * iInset, iDYBtn - 2 * iInset);
        CHLButton *pBtn = (CHLButton*)[m_pButtons objectAtIndex:i];
        [pBtn setFrame:rBtn];

        iCol++;
        if (iCol >= m_iNCols)
        {
            iRow++;
            iCol = 0;
        }
    }
}
/*============================================================================*/

-(void)SetCommandID:(int)iCommandID Responder:(id)pResponder;

/*============================================================================*/
{
    m_iCommandID = iCommandID;
    m_pCommandResponder = pResponder;
}
/*============================================================================*/

-(void)SetSelectedIndex:(int)iIndex

/*============================================================================*/
{
    m_iSelectedIndex = iIndex;
    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CHLButton *pBtn = (CHLButton*)[m_pButtons objectAtIndex:i];
        [pBtn SetChecked:(i == iIndex)];
    }
}
/*============================================================================*/

-(int)GetSelectedIndex

/*============================================================================*/
{
    return m_iSelectedIndex;
}
/*============================================================================*/

-(CHLButton*)GetSelectedButton

/*============================================================================*/
{
    if (m_iSelectedIndex < 0 || m_iSelectedIndex >= [m_pButtons count])
        return NULL;
    CHLButton *pSel = (CHLButton*)[m_pButtons objectAtIndex:m_iSelectedIndex];
    return pSel;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pButtons release];
    [super dealloc];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    m_iSelectedIndex = iCommandID;
    [self SetSelectedIndex:iCommandID];
    [m_pCommandResponder OnCommand:m_iCommandID];
}
/*============================================================================*/

-(int)NButtons

/*============================================================================*/
{
    return [m_pButtons count];
}
/*============================================================================*/

-(CHLButton*)Button:(int)iIndex

/*============================================================================*/
{
    CHLButton *pBtn = (CHLButton*)[m_pButtons objectAtIndex:iIndex];
    return pBtn;
}
/*============================================================================*/

-(void)NColumns:(int)iNew

/*============================================================================*/
{
    m_iNCols = iNew;
}
/*============================================================================*/

-(int)NRows

/*============================================================================*/
{
    int iNRows = 1;
    while ((iNRows * m_iNCols) < [m_pButtons count])
    {
        iNRows++;
    }
    return iNRows;
}
@end
