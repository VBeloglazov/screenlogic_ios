/*
--------------------------------------------------------------------------------

    HLAPI.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifdef GATEWAY
    #include "audiostruct.h"

    class CHLString;
    class CHLObList;
    class CSystemComponent;
    class CSystemComponentDef;
    class CAudioService;
    class CAudioSystem;
    class CDeviceInterface;
    class CDeviceInterfaceDef;
    class CAudioShare;
    class CAudioFile;

    __declspec(dllexport) void              LoadSystemComponentDefs(int iFamily, CHLObList *pList);
    __declspec(dllexport) CSystemComponent  *CreateSystemComponent(CSystemComponentDef *pDef);
    __declspec(dllexport) void              LoadAudioServices(CHLObList *pList);
    __declspec(dllexport) CAudioService     *CreateAudioService(CSystemComponentDef *pDef, CAudioSystem *pSystem);
    __declspec(dllexport) void              LoadDeviceInterfaces(CHLObList *pList);
    __declspec(dllexport) CDeviceInterface  *CreateDeviceInterface(CDeviceInterfaceDef *pDef);
    __declspec(dllexport) CAudioShare       *CreateAudioShare(int iType, CHLString *psData, CDeviceInterface *pData);
    __declspec(dllexport) void              LoadAudioFileTypes(CHLObList *pList);
    __declspec(dllexport) CAudioFile        *CreateAudioFile(int iType, HLT_TRACK *pTrack);

    #ifdef HL_DLL
        // DRIVER DLL

        #define HL_API __declspec(dllimport)

    #else
        // GATEWAY APP
        #define HL_API __declspec(dllexport)


        typedef void                (*LOADSYSTEMCOMPONENTDEFS) (int iFamily, CHLObList *pList);
        typedef CSystemComponent*   (*CREATESYSTEMCOMPONENT) (CSystemComponentDef *pDef);
        typedef void                (*LOADAUDIOSERVICES) (CHLObList *pList);
        typedef CAudioService*      (*CREATEAUDIOSERVICE) (CSystemComponentDef *pDef, CAudioSystem *pSystem);
        typedef void                (*LOADDEVICEINTERFACES) (CHLObList *pList);
        typedef CDeviceInterface*   (*CREATEDEVICEINTERFACE) (CDeviceInterfaceDef *pDef);
        typedef CAudioShare*        (*CREATEAUDIOSHARE) (int iType, CHLString *psData, CDeviceInterface *pData);
        typedef void                (*LOADAUDIOFILETYPES) (CHLObList *pList);
        typedef CAudioFile*         (*CREATEAUDIOFILE) (int iType, HLT_TRACK *pTrack);

        #ifdef UNDER_CE
            #define                 S_LOADSYSTEMCOMPONENTDEFS   _T("LoadSystemComponentDefs")
            #define                 S_CREATESYSTEMCOMPONENT     _T("CreateSystemComponent")
            #define                 S_LOADAUDIOSERVICES         _T("LoadAudioServices")
            #define                 S_CREATEAUDIOSERVICE        _T("CreateAudioService")
            #define                 S_LOADDEVICEINTERFACES      _T("LoadDeviceInterfaces")
            #define                 S_CREATEDEVICEINTERFACE     _T("CreateDeviceInterface")
            #define                 S_CREATEAUDIOSHARE          _T("CreateAudioShare")
            #define                 S_LOADAUDIOFILETYPES        _T("LoadAudioFileTypes")
            #define                 S_CREATEAUDIOFILE           _T("CreateAudioFile")
        #else
            #define                 S_LOADSYSTEMCOMPONENTDEFS   "LoadSystemComponentDefs"
            #define                 S_CREATESYSTEMCOMPONENT     "CreateSystemComponent"
            #define                 S_LOADAUDIOSERVICES         "LoadAudioServices"
            #define                 S_CREATEAUDIOSERVICE        "CreateAudioService"
            #define                 S_LOADDEVICEINTERFACES      "LoadDeviceInterfaces"
            #define                 S_CREATEDEVICEINTERFACE     "CreateDeviceInterface"
            #define                 S_CREATEAUDIOSHARE          "CreateAudioShare"
            #define                 S_LOADAUDIOFILETYPES        "LoadAudioFileTypes"
            #define                 S_CREATEAUDIOFILE           "CreateAudioFile"
        #endif
    #endif
#else
    #define HL_API
#endif
