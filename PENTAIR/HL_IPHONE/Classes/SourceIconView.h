//
//  SourceIconView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


/*============================================================================*/

@interface CSourceIconView : UIView 

/*============================================================================*/
{
    NSString                            *m_pIconName;
    NSString                            *m_pSourceName;
    UILabel                             *m_pLabel;
    CGSize                              m_SizeLast;
}

- (id)initWithFrame:(CGRect)frame IconName:(NSString*)pIconName SourceName:(NSString*)pSourceName;

@end
