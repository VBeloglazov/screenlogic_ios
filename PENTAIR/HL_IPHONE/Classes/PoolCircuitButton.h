//
//  PoolCircuitButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "hlbutton.h"
#import "hlview.h"

@class CNSPoolCircuit;
@class CNSPoolConfig;

/*============================================================================*/

@interface CPoolCircuitButton : CHLButton <CSink>

/*============================================================================*/
{
    CNSPoolCircuit                      *m_pCircuit;
    CNSPoolConfig                       *m_pConfig;
}

-(id)initWithFrame:(CGRect) rFrame Circuit:(CNSPoolCircuit*)pCircuit Config:(CNSPoolConfig*)pConfig;

@end
