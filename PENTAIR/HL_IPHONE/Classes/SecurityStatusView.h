//
//  SecurityStatusView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "hlview.h"

@class CSecPartition;
@class CToolBarBackground;
@class CHLSegment;
@class CSecurityListBox;
@class CTableTitleHeaderView;
@class CHLTableFrame;
@class CKeypadView;

#define LISTBOX_ZONES           0
#define LISTBOX_FAULTS          1

/*============================================================================*/

@interface CSecurityStatusView : UIView <CSink>


/*============================================================================*/
{
    CSecPartition               *m_pPartition;
    CHLSegment                  *m_pModes;
    UILabel                     *m_pStatusText;
    CSecurityListBox            *m_pZones;
    CSecurityListBox            *m_pFaults;
    CKeypadView                 *m_pKeypad;
    CKeypadView                 *_m_pKeypad;
    int                         m_iDXLast;
    int                         m_iDYLast;
}

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition;
-(void)DismissKeypad;
-(CGRect)KeypadRect;

@end

/*============================================================================*/

@interface CSecurityListBox : UIView <CSink, UITableViewDataSource, UITableViewDelegate>

/*============================================================================*/
{
    int                         m_iType;
    CSecPartition               *m_pPartition;
    CHLTableFrame               *m_pTableView;
}

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition Type:(int)iType;

@end


/*============================================================================*/

@interface CKeypadView : UIView < CHLCommandResponder >

/*============================================================================*/
{
    CSecurityStatusView         *m_pView;
    CSecPartition               *m_pPartition;
    int                         m_iModeIndex;
    int                         m_nKeysEntered;
    int                         m_nKeysWanted;
    int                         m_Keys[10];
    NSMutableArray              *m_pBtns;
}

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition View:(CSecurityStatusView*)pView Mode:(int)iIndex;

@end

