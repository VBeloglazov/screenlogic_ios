/*
--------------------------------------------------------------------------------

    HLSOCKET.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlmsgs.h"
#include "hlstructs.h"
#include "hlserver.h"
#include "hlmsg.h"
#include "schema.h"
#include "hlsock.h"
#include "hlserver.h"
#include "hltime.h"
#include "helpers.h"
#include "main.h"
#include "platform.h"
#include "version.h"

#include "hlsocket.h"

#ifdef CRYSTALPAD
    #include "main.h"
#endif

#ifdef HLCONFIG
    #include "..\hlconfig\statwnd.h"
#endif


    //
    // Debug constants
    //
    //#define DEBUG_DISCONNECT

/*============================================================================*/

    void                        CMessageTracker::ProcessMessageStatus(
    short                       shCurrentMessageID,
    short                       shCurrentSenderID,
    DWORD                       dwNBytesIn,
    DWORD                       dwNBytesMessage )

/*============================================================================*/
{
}
/*============================================================================*/

    void                        CMessageTracker::ProcessMessage(
    CHLString                   sMSG )

/*============================================================================*/
{
}
#ifdef MULTI_SERVER
/*============================================================================*/

    CHLSocket::CHLSocket(
    CHLServer               *pServer )

/*============================================================================*/
#else
/*============================================================================*/

    CHLSocket::CHLSocket()

/*============================================================================*/
#endif
{
    #ifdef MULTI_SERVER
        g_pHLServer = pServer;
    #endif

    m_hPing                 = INVALID_SOCKET;
    m_hSocket               = INVALID_SOCKET;
    m_bAltPing              = FALSE;

    m_bDisconnectOnTimeout  = FALSE;
    m_bTCPNoDelay           = TRUE;
    m_bEnableKeepAlive      = TRUE;

    memset(&m_RXHeader, 0, sizeof(HLT_HEADER));
    m_iNRXHeader            = 0;
    m_iNRXMSG               = 0;
    m_pRXMSG                = NULL;

    m_iNTotalBytesIn        = 0;
    m_iNTotalBytesOut       = 0;

    m_iNMessagesIn          = 0;
    m_iNMessagesOut         = 0;

    m_iNPartialHeaderRX     = 0;
    m_iNPartialRX           = 0;
    m_iNMultipleRX          = 0;
    m_iNMultiPacketRX       = 0;
    m_iNQATickCount         = 0;

    m_iNDrops               = 0;

    m_dwKeepAliveTimeout    = HL_TIMEOUT_KEEPALIVE_INIT;
    m_dwLastCounter         = 0;

    m_bFirstKeepAlive       = TRUE;
    m_bServerNotify         = TRUE;

    m_pMessageTracker       = NULL;

    m_iNTXBuffer            = 0;

    #ifdef IPHONE
        m_bWirelessOK       = FALSE;
    #endif
}
/*============================================================================*/

    CHLSocket::~CHLSocket()

/*============================================================================*/
{
    Disconnect();
}
/*============================================================================*/

    void                        CHLSocket::Socket(
    SOCKET                      hSocket)

/*============================================================================*/
{
    if (m_hPing != INVALID_SOCKET)
    {
        shutdown(m_hPing, 0);
        CloseSocket(m_hPing);
        m_hPing = INVALID_SOCKET;
    }

    m_hSocket = hSocket;

    SetBlocking(m_hSocket);
    SetTCPNoDelay(m_bTCPNoDelay);

    m_hltLastReceive.Restart();
}
/*============================================================================*/

    BOOL                        CHLSocket::IsConnected()

/*============================================================================*/
{
#ifdef NO_DISCONNECT
    if (m_hltDebug.HLTickCount() > 30000)
        return TRUE;
#endif
    return (m_hSocket != INVALID_SOCKET);
}
/*============================================================================*/

    BOOL                        CHLSocket::Connect(
    char                        *pGatewayName,
    CStatusSink                 *pSink )

/*============================================================================*/
{
    // Disconnect if we are currently connected.
    if (IsConnected())
        Disconnect();

    // m_hltDisconnect is reset on disconnect, and again on any connection
    // attempt.  Restart it here in case we return FALSE below.

    SOCKADDR_IN hostAddress;

    if (!g_pHLServer->RemoteMode())
    {
        // Not Remote mode, need to find gateway first
        if (!FindGateway(&hostAddress, pGatewayName, pSink))
        {
            return FALSE;
        }
    }

    // Try to make the socket connection
    if (!ConnectAndLogin(&hostAddress, TRUE))
        return FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLSocket::ConnectAndLogin(
    SOCKADDR_IN                 *pAddress,
    BOOL                        bLocalLogin,
    SOCKET                      hRelayServer,
    int                         iConType,
    CStatusSink                 *pSink )

/*============================================================================*/
{
    //
    // If we get here then we got a hit.
    //
    // This is the one place we want to set m_hSocket to a valid value.
    // See Disconnect for the only place where it should be getting set to
    // invalid.
    //
    LOGASSERT(m_hSocket == INVALID_SOCKET);
    if (hRelayServer != INVALID_SOCKET)
    {
        m_hSocket = hRelayServer;
        if (m_hPing != INVALID_SOCKET)
        {
            shutdown(m_hPing, 0);
            CloseSocket(m_hPing);
            m_hPing = INVALID_SOCKET;
        }
    }
    else
    {
        m_hSocket = socket(PF_INET, SOCK_STREAM, 0);
        if (m_hSocket == INVALID_SOCKET)
        {
            return FALSE;
        }

        #ifdef HLUTIL
            //
            // ASYNC
            //

            // Make non-blocking
            DWORD uTrue = TRUE;
            ioctlsocket(m_hSocket, FIONBIO, &uTrue);

            // Start Connect
            int iResult = ::connect(m_hSocket, (SOCKADDR*)pAddress, sizeof(SOCKADDR_IN));

            // Loop till we connect or timeout
            CHLTimer hltStart;
            BOOL bReady  = FALSE;
            while (!bReady)
            {
                if (hltStart.HLTickCount() > 10000)
                {
                    // Timed out waiting to connect
                    closesocket(m_hSocket);
                    m_hSocket = INVALID_SOCKET;
                    return FALSE;
                }

                fd_set set;
                fd_set setZero;
                setZero.fd_count    = 0;
                set.fd_count        = 1;
                set.fd_array[0]     = m_hSocket;

                timeval tv;
                tv.tv_sec   = 0;
                tv.tv_usec  = 1000;

                int iSel = select(0, &setZero, &set, &setZero, &tv);

                // Ready?
                if (iSel > 0)
                    bReady = TRUE;
                else
                    Sleep(50);

                // Check on caller abort
                if (pSink != NULL)
                {
                    if (!pSink->DoWaitProc())
                        return FALSE;
                }
            }
        #else

            if (::connect(m_hSocket, (SOCKADDR *) pAddress, sizeof(SOCKADDR)) == SOCKET_ERROR)
            {
                CloseSocket(m_hSocket);
                m_hSocket = INVALID_SOCKET;
                return FALSE;
            }
        #endif
    }

    m_hltLastReceive.Restart();

    if (m_hPing != INVALID_SOCKET)
    {
        shutdown(m_hPing, 0);
        CloseSocket(m_hPing);
        m_hPing = INVALID_SOCKET;
    }

    SetBlocking(m_hSocket);

    if (!m_bServerNotify)
    {
        return TRUE;
    }

    // Tell the gateway to switch us from the CFileServer to CServerHost
    char byOut [] = {"CONNECTSERVERHOST\r\n\r\n"};
    Send(21, (BYTE*)byOut);

    // We're connect over internet, so this is all we do here
    if (!bLocalLogin)
    {
        // At this point we should be connected to the gateway serverhost, but we are not validated
        return TRUE;
    }

    // Default: no blocking.
    SetBlocking(m_hSocket);
    SetTCPNoDelay(m_bTCPNoDelay);

    if (iConType == -1)
    {
        iConType = CONNECTION_INT();
    }

    // Next, login
    CHLMSG msgLogIn(0, HLM_CLIENTLOGINQ);
    msgLogIn.PutInt(SCHEMA_LATEST);
    msgLogIn.PutInt(iConType);
    msgLogIn.PutString(_T( HL_VERSION ));
    msgLogIn.PutInt(16);

    BYTE byZero[16];
    memset(byZero, 0, 16);
    msgLogIn.PutData(byZero, 16);
    msgLogIn.PutInt(PROC_INT);

    #if defined(CRYSTALPAD) | defined(HLOSD) | defined(MINIPAD)
        msgLogIn.PutString(g_pHLServer->MachineName());
        msgLogIn.PutString(g_pHLServer->UUID());
        msgLogIn.PutString(g_pHLServer->OSVersion());
    #endif


    Send(msgLogIn.MSGSize(), msgLogIn.MSG());
    FlushTXBuffer();

    //
    // Pass on the news: since this is the only place connections are made,
    // this is the only place we send out this message.
    //
    if( g_pHLServer != NULL && m_bServerNotify )
        g_pHLServer->SetConnectionChanged();


    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLSocket::FindGateway(
    SOCKADDR_IN                 *pAddress,
    char                        *pName,
    CStatusSink                 *pSink )

/*============================================================================*/
{
    //
    // Create the broadcast socket
    //
    BOOL bTrue  = TRUE;

    if (m_hPing != INVALID_SOCKET && m_hltPingCreate.HLTickCount() > 5000)
    {
        CloseSocket(m_hPing);
        m_hPing = INVALID_SOCKET;
    }

    if (m_hPing == INVALID_SOCKET)
    {
        m_hltPingCreate.Restart();
        m_hPing = socket(PF_INET, SOCK_DGRAM, 0);
        int iError = setsockopt(m_hPing, SOL_SOCKET, SO_BROADCAST, (char*) &bTrue, sizeof(BOOL));
        if (iError)
        {
            if (pSink != NULL)
                pSink->SetStatus(_T("Error in setsockopt()"));

            CloseSocket(m_hPing);
            m_hPing = INVALID_SOCKET;

            return FALSE;
        }

        #if defined (IPHONE) & defined(IPHONE_DEBUG)
            printf("CreatePing OK\n");
        #endif
    }

    //
    // Send the request message
    //
    if (m_hltPing.HLTickCount() > 250)
    {

        SOCKADDR_IN sockAddr;
   //
        if (m_bAltPing)
            SetAddress(&sockAddr, 255, 255, 255, 255, 1444);
        else
            SetAddress(&sockAddr, 255, 255, 255, 255, 444);
        
#ifdef PENTAIR
        m_bAltPing = !m_bAltPing;
#endif

        // Default, new connection to the server
        int iReq[2];
        iReq[0] = HLM_ADD_CLIENTQ;
        iReq[1] = 0;

        int iSend = sendto(m_hPing, (char *) iReq, HL_SIZE_PINGPACKETQ, 0, (SOCKADDR *) &sockAddr, sizeof(sockAddr));
        if (iSend !=  HL_SIZE_PINGPACKETQ)
        {

            #ifdef IPHONE
                m_bWirelessOK = FALSE;
                printf("Error Sending Ping\n");
            #else
                if (pSink != NULL)
                    pSink->SetStatus(_T("Error sending ping packet"));
                shutdown(m_hPing, 0);
                CloseSocket(m_hPing);
                m_hPing = INVALID_SOCKET;

                // No response, try identity
                SetAddress(pAddress, 127, 0, 0, 1, g_ushBasePort);

                return TRUE;
            #endif
        }
        else
        {
            #if defined (IPHONE) 
                #ifdef IPHONE_DEBUG
                    printf("Send Ping OK\n");
                #endif
                
                m_bWirelessOK = TRUE;
            #endif
        }

        m_hltPing.Restart();
    }

    int iNameLen = strlen(pName);

    if (pSink != NULL)
        pSink->SetStatus(_T("Waiting for ping response."));

    //
    // Message is out there, just hang out and wait for connections
    //
    CHLTimer hltTime;

    memset(pAddress, 0, sizeof(SOCKADDR_IN));

    int iSizePing = (int)sizeof(HLT_PINGA);
    int iNPackets = 0;

    BYTE byBuffer[4096];
    while (TRUE && iNPackets < 100)
    {
        if (!WaitSocketReadState(m_hPing, 20))
        {
            return FALSE;
        }

        HLT_PINGA PingA;

        // Got something: do a read.
        //int iNRead = recv(m_hPing, (char*)&byBuffer, 4096, 0);
#ifdef IPHONE
        memset(byBuffer, 0, 4096);
        int iTest = 1;
        int iNRead = HLRecvFromUpTo(m_hPing, (char*)byBuffer, 4096, pAddress);
#else
        int iHostSize = sizeof(SOCKADDR_IN);
        int iNRead = recvfrom(m_hPing, (char*)byBuffer, 4096, 0, (SOCKADDR*)pAddress, &iHostSize);
#endif

        if (iNRead <= 0)
        {
            CloseSocket(m_hPing);
            m_hPing = INVALID_SOCKET;
            return FALSE;
        }

        iNPackets++;

        memset(&PingA, 0, iSizePing);
        if (iNRead == iSizePing)
        {
            memcpy(&PingA, byBuffer, iSizePing);
        }
            // Check response.
        if (PingA.m_iMessageID == HLM_ADD_CLIENTA)
        {
            #ifdef MAC_OS
                pAddress->sin_addr.s_addr  = PingA.m_dwIP;
            #else
                pAddress->sin_addr.S_un.S_addr  = PingA.m_dwIP;
            #endif
            
            pAddress->sin_port              = htons(PingA.m_wPort);

            // Don't connect to slave bricks
            BOOL bOK = FALSE;

            #ifdef MINIPAD
                // Minipad REQUIRES a full gateway
                if (PingA.m_cGatewayType == HLM_GATEWAY_FULL)
                    bOK = TRUE;
                if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_X86C)
                    bOK = TRUE;
                if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_ARMV4IA)
                    bOK = TRUE;
                if (PingA.m_cGatewayType == HLM_GATEWAY_BRICK_MASTER)
                    bOK = TRUE;
            #endif

            #if defined(CRYSTALPAD) | defined(HLOSD)
                // Crystalpad can hook up to either a full gateway or a master brick
                #ifdef MAC_OS
                    if (PingA.m_cGatewayType == HLM_GATEWAY_FULL)
                        bOK = TRUE;
                    if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_X86C)
                        bOK = TRUE;
                    if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_ARMV4IA)
                        bOK = TRUE;

                    #ifdef PENTAIR
                        if (PingA.m_cGatewayType == HLM_GATEWAY_BRICK_MASTER)
                            bOK = TRUE;
                    #endif
                #else
                    if (PingA.m_cGatewayType == HLM_GATEWAY_FULL)
                        bOK = TRUE;
                    if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_X86C)
                        bOK = TRUE;
                    if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_ARMV4IA)
                        bOK = TRUE;
                    if (PingA.m_cGatewayType == HLM_GATEWAY_BRICK_MASTER)
                        bOK = TRUE;
/*
                    if (PingA.m_cGatewayType == HLM_GATEWAY_BRICK_APRILAIRE)
                        bOK = TRUE;
*/
                #endif
            #endif

            #ifdef POOLCONFIG
                // Crystalpad can hook up to either a full gateway or a master brick
                if (PingA.m_cGatewayType == HLM_GATEWAY_FULL)
                    bOK = TRUE;
                if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_X86C)
                    bOK = TRUE;
                if (PingA.m_cGatewayType == HLM_GATEWAY_FULL_ARMV4IA)
                    bOK = TRUE;
                if (PingA.m_cGatewayType == HLM_GATEWAY_BRICK_MASTER && PingA.m_cGatewaySubType == FAMILY_POOL_CTLR)
                    bOK = TRUE;
            #endif

                #if defined(IPHONE) & defined(PENTAIR)
                    if (strncmp(PingA.m_cGatewayName, "Pentair", 7) != 0)
                        bOK = FALSE;
                #endif

            if (bOK)
            {
                CHLString sAddress = HLFormatAddress(pAddress);
                CHLString sName;
                PingA.m_cGatewayName[27] = 0;
                int iLen = strlen(PingA.m_cGatewayName);
                char *pBuf = (char*)sName.GetBufferCHAR(iLen);
                memcpy(pBuf, PingA.m_cGatewayName, iLen);
                sName.ReleaseBuffer(iLen);

                CHLString sTrace;
                sTrace.Format(_T("Ping From %s %s"), (const TCHAR*)sAddress, (const TCHAR*)sName);
                #ifdef IPHONE_DEBUG
                    printf("%s\n", (const char*)sTrace);
                #endif

                if (pSink != NULL)
                    pSink->SetStatus(sTrace);


                if (iNameLen > 0)
                {

                    PingA.m_cGatewayName[27] = 0;

                    if (strcmp(pName, (char*) PingA.m_cGatewayName) ==  0)
                        return TRUE;
                    if (pSink != NULL)  
                    {
                        sTrace.Format(_T("Wrong System, Looking For: %s"), pName);
                        pSink->SetStatus(sTrace);
                    }
                }
                else
                {
                    return TRUE;
                }
            }
        }
    }

    return FALSE;
}
/*============================================================================*/

    void                        CHLSocket::Disconnect()

/*
NOTES:  This closes the socket, and resets all internal data, which also will
        clear out any existing messages or partial messages.

        Note too that we send the message to all the sinks when the connection
        goes down from here. All events that kill the socket need to come in
        through here to ensure this is always true.
*/
/*============================================================================*/
{
    // Shut down socket.
    if (m_hSocket != INVALID_SOCKET)
    {
        //
        // We're using m_hSocket to control the sending of the ConnectionChanged
        // messages, so we set it invalid here ONLY.
        // See up in Connect to where it should be set to a valid value.
        //
        shutdown(m_hSocket, 0);
        CloseSocket(m_hSocket);
        m_hSocket = INVALID_SOCKET;

        // Pass on the news.
        if( g_pHLServer != NULL && m_bServerNotify)
            g_pHLServer->SetConnectionChanged();

        // Update status vars.
        m_iNDrops++;
    }

    // Cleanup buffers.
    m_iNRXHeader = 0;
    m_iNRXMSG    = 0;
    memset(&m_RXHeader, 0, sizeof(HLT_HEADER));
    delete m_pRXMSG;
    m_pRXMSG = NULL;
}
/*============================================================================*/

    BOOL                        CHLSocket::DisconnectOnTimeout()

/*============================================================================*/
{
    return m_bDisconnectOnTimeout;
}
/*============================================================================*/

    void                        CHLSocket::DisconnectOnTimeout(
    BOOL                        bNew )

/*============================================================================*/
{
    m_bDisconnectOnTimeout = bNew;
}
/*============================================================================*/

    BOOL                        CHLSocket::TCPNoDelay()

/*============================================================================*/
{
    return m_bTCPNoDelay;
}
/*============================================================================*/

    void                        CHLSocket::SetTCPNoDelay(
    BOOL                        bNew )

/*============================================================================*/
{
    //
    // non-zero means no delay: this disables the Nagle algorithm and sends
    // small packets as they arrive instead of grouping them.
    //
    #ifndef MAC_OS
        BOOL bParam = bNew ? 1 : 0;
        int iReturn = setsockopt(m_hSocket, IPPROTO_TCP, TCP_NODELAY, (const char *) &bParam, sizeof(BOOL));
        LOGASSERT(iReturn == 0);
    #endif

    m_bTCPNoDelay = bNew;
}
/*============================================================================*/

    int                         CHLSocket::NDrops()

/*============================================================================*/
{
    return m_iNDrops;
}
/*============================================================================*/

    void                         CHLSocket::ZeroCounts()

/*============================================================================*/
{
    // Reset just the params tracking our messages: we could be in the
    // middle of processing things, so don't mess with the buffers.
    m_iNTotalBytesIn    = 0;
    m_iNTotalBytesOut   = 0;
    m_iNMessagesIn      = 0;
    m_iNMessagesOut     = 0;

    m_iNPartialHeaderRX = 0;
    m_iNPartialRX       = 0;
    m_iNMultipleRX      = 0;
    m_iNMultiPacketRX   = 0;
    m_iNQATickCount     = 0;

    m_iNDrops           = 0;
}
/*============================================================================*/

    BOOL                        CHLSocket::IsReadyToSend()

/*============================================================================*/
{
    if (!IsConnected())
        return FALSE;
    if (ReadyToReceive(m_hSocket))
        return FALSE;
    return ReadyToSend(m_hSocket);
}
/*============================================================================*/

    BOOL                        CHLSocket::Send(
    int                         iNBytes,
    BYTE                        *pBytes )

/*============================================================================*/
{
    if (!IsConnected())
        return FALSE;

    FlushTXBuffer();

    #ifdef HLCONFIG
        if (g_pMainWindow != NULL)
            g_pMainWindow->DrawStatus(STATUS_SENDING      );
    #endif
    
    if (!WaitSocketWriteState(m_hSocket, 500))
    {
        Disconnect();
        return FALSE;
    }

    // Get length and send.
    #ifdef MAC_OS
        errno = 0;
    #else
        SetLastError(0);
    #endif

    int iSent = send(m_hSocket, (char *) pBytes, iNBytes, 0);
    if (iSent != iNBytes)
    {
        Disconnect();
        return FALSE;
    }

    #ifdef HLCONFIG
        if (g_pMainWindow != NULL)
            g_pMainWindow->DrawStatus(STATUS_IDLE);
    #endif

    m_hltTX.Restart();
    m_iNTotalBytesOut += iSent;

    return TRUE;
}
/*============================================================================*/

    CHLMSG                      *CHLSocket::WaitMessage(    
    DWORD                       dwMaxWaitForData )


// Returns one of the following
// RECEIVE_ERROR    Something bad happened,
// RECEIVE_IDLE     Nothing to read and nothing inbound
// RECEIVE_BUSY     Either we've read more data or we expect some soon
// RECEIVE_MSGREADY There's a message ready to be processed
/*============================================================================*/
{
    if (m_iNRXHeader < 8)
    {
        if (!WaitSocketReadState(m_hSocket, dwMaxWaitForData))
            return NULL;

        char *pBuf = (char*)&m_RXHeader;
        int iNToRead = 8 - m_iNRXHeader;
        int iNBytesRead = recv(m_hSocket, (char*)(pBuf+m_iNRXHeader), iNToRead, 0);
        if (iNBytesRead <= 0)
        {
            Disconnect();
            return NULL;
        }

        m_hltLastReceive.Restart();
        m_iNRXHeader += iNBytesRead;
    }

    if (m_iNRXHeader < 8)
        return NULL;

    int iNBytesData = m_RXHeader.m_iNBytes;

    if (iNBytesData < 0 || iNBytesData > HL_MAX_MESSAGE_SIZE)
    {
        Disconnect();
        return NULL;
    }

    if (m_pRXMSG == NULL)
    {
        m_pRXMSG = new CHLMSG(m_RXHeader.m_shSenderID, m_RXHeader.m_shMessageID, iNBytesData, TRUE);
        m_iNRXMSG = 0;
    }

    int iNToRead = iNBytesData - m_iNRXMSG;

    if (iNToRead > 0)
    {
        // Need more to fill the msg
        if (!WaitSocketReadState(m_hSocket, dwMaxWaitForData))
            return NULL;

        char *pBuf = (char*)m_pRXMSG->Data();
        int iNBytesRead = recv(m_hSocket, (char*)(pBuf+m_iNRXMSG), iNToRead, 0);
        if (iNBytesRead <= 0)
        {
            Disconnect();
            return NULL;
        }

        m_hltLastReceive.Restart();
        m_iNRXMSG += iNBytesRead;
    }

    if (m_iNRXMSG >= iNBytesData)
    {
        //
        // Message is full. return it
        //
        CHLMSG *pRet    = m_pRXMSG;

        // Reset our vars
        m_pRXMSG        = NULL;
        m_iNRXHeader    = 0;
        m_iNRXMSG       = 0;

        return pRet;
    }

    return NULL;
}
/*============================================================================*/

    BOOL                        CHLSocket::CheckTX()

/*============================================================================*/
{
    if (!IsConnected())
        return FALSE;

    if (m_iNTXBuffer < 1)
        return TRUE;
    if (m_hltTXBuffer.HLTickCount() < TIME_TX_BUFFER)
        return TRUE;
    if (!ReadyToSend(m_hSocket))
        return TRUE;

    return FlushTXBuffer();
}
/*============================================================================*/

    BOOL                        CHLSocket::FlushTXBuffer()

/*============================================================================*/
{
    if (!WaitSocketWriteState(m_hSocket, 500))
    {
        Disconnect();
        return FALSE;
    }

    // Get length and send.
    #ifdef MAC_OS
        errno = 0;
    #else
        SetLastError(0);
    #endif

    int iSent = send(m_hSocket, (char *) m_TXBuffer, m_iNTXBuffer, 0);
    if (iSent != m_iNTXBuffer)
    {
        Disconnect();
        return FALSE;
    }

    #ifdef HLCONFIG
        if (g_pMainWindow != NULL)
            g_pMainWindow->DrawStatus(STATUS_IDLE);
    #endif

    m_hltTX.Restart();
    m_iNTotalBytesOut += iSent;
    m_iNTXBuffer = 0;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLSocket::KeepAliveTimeout()

/*============================================================================*/
{
    //
    // For debugging: this will disconnect the tablet if you break in the
    // debugger for more than the timeout, so by default in debug builds we
    // don't disconnect.
    //
    // If you want to debug the disconnect mechanism itself, uncomment
    // the define at the top, and the test will be included in debug builds.
    //
#ifdef HLCONFIG
    DWORD dwMaxTime = 30000;
#else
    DWORD dwMaxTime = 10000;
    if (g_pHLServer->RemoteMode())
        dwMaxTime = 20000;
#endif

    if (LastReceive() > 5000 && m_hltTX.HLTickCount() > 2000)
    {
        CHLMSG msgQ(0, HLM_PING_SERVERQ);
        if (!Send(msgQ.MSGSize(), msgQ.MSG()))
            return TRUE;
    }


    if (LastReceive() > dwMaxTime)
    {
        // Been a long time, check to see if there's stuff sitting on the wire before
        // we make our descision
        if (!WaitSocketReadState(m_hSocket, 25))
            return TRUE;

        m_hltLastReceive.Restart();
        return FALSE;
    }

    return FALSE;
}
