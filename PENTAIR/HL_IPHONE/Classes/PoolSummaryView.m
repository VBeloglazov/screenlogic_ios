//
//  PoolSummaryView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/13/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolSummaryView.h"
#import "NSPoolConfig.h"
#import "AppDelegate.h"
#import "hlm.h"
#import "crect.h"
#import "hlcomm.h"
#import "MainView.h"
#import "CustomPage.h"
#import "NSQuery.h"

#define DY_ITEM         26
#define DY_ITEM_MAX     40
#define ITEM_TEXT_SIZE  16

@implementation CPoolSummaryItem

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Type:(int)iType Label:(NSString*)pLabel

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_iType = iType;
        m_pLabel = [self AddLabel:pLabel];
        m_pValue = [self AddLabel:@""];
    }

    return self;
}
/*============================================================================*/

-(int)Type

/*============================================================================*/
{
    return m_iType;
}
/*============================================================================*/

-(void)SetText:(NSString*)pText

/*============================================================================*/
{
    [m_pValue setText:pText];
}
/*============================================================================*/

-(UILabel*)AddLabel:(NSString*)pText

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [CCustomPage InitStandardLabel:pLabel Size:ITEM_TEXT_SIZE];
    pLabel.textAlignment = UITextAlignmentLeft;
    [pLabel setText:pText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(void)SetLabel:(NSString *)pText

/*============================================================================*/
{
    [m_pLabel setText:pText];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    CGRect rLabel = [CRect BreakOffLeft:&rBounds DX:rBounds.size.width * 65 / 100];
    [m_pLabel setFrame:rLabel];
    [m_pValue setFrame:rBounds];
}
@end


@implementation CPoolSummaryView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame

/*============================================================================*/
{
    self = [super initWithFrame:frame];

    if (self) 
    {
        #ifdef PENTAIR

            CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
            m_pConfig = [pDelegate PoolConfig];

        #else
            m_pConfig = [[CNSPoolConfig alloc] init ];
        #endif


        m_pControls = [[NSMutableArray alloc] init];

        NSString *sPool = [m_pConfig PoolText];
        NSString *sSpa  = [m_pConfig SpaText];
        BOOL bSpa = [m_pConfig HasSpa];

        // Temps
        [self AddItem:ITEM_AIRTEMP          Label:S_AIRTEMP];
        //DL - Last temp
        [self AddItem:ITEM_POOLTEMP         Label:sPool Suffix:S_POOLTEMP];
        //[self AddItem:ITEM_POOLTEMP         Label:@"HREN" Suffix:S_POOLTEMP];
        if (bSpa)
            [self AddItem:ITEM_SPATEMP      Label:sSpa  Suffix:S_SPATEMP];

        // Setpoints
        [self AddItem:ITEM_POOLSP           Label:sPool Suffix:S_POOLSP];
        if (bSpa)
            [self AddItem:ITEM_SPASP        Label:sSpa Suffix:S_SPASP];

        [self AddItem:ITEM_POOLHEATMODE     Label:sPool Suffix:S_POOLHEATMODE];
        [self AddItem:ITEM_POOLHEATSTATUS   Label:sPool Suffix:S_POOLHEATSTATUS];
        // Maybe these
        if (bSpa)
        {
            [self AddItem:ITEM_SPAHEATMODE      Label:sSpa Suffix:S_SPAHEATMODE];
            [self AddItem:ITEM_SPAHEATSTATUS    Label:sSpa Suffix:S_SPAHEATSTATUS];
        }

        if ([m_pConfig EquipPresent:POOL_ICHEMPRESENT])
            [self AddItem:ITEM_PHORP Label:S_PHORP];

        [self AddItem:ITEM_REMOTES Label:S_REMOTES];

        [self AddItem:ITEM_FREEZE Label:S_FREEZE];

        
        [m_pConfig BeginPoll];
        [m_pConfig AddSink:self];
        [self Notify:0 Data:0];
        
        CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETSTATUSQ] autorelease];
        [pQ1 PutInt:0];
        [pQ1 SendMessageWithMyID:self];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pConfig EndPoll];
    [m_pControls release];
    [m_pConfig RemoveSink:self];
    
    #ifndef PENTAIR
        [m_pConfig release];
    #endif

    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    //NSLog([m_pConfig FormatDegText:[m_pConfig WaterTemp:BODYTYPE_POOL]]);
    if([m_pConfig PoolIsOn])
    {
        [self SetItemText:ITEM_POOLTEMP          Text:[m_pConfig FormatDegText:[m_pConfig WaterTemp:BODYTYPE_POOL]]];
    }
    else
    {
        NSString *lastTemp = [m_pConfig FormatDegText:[m_pConfig WaterTemp:BODYTYPE_POOL]];
        lastTemp = [lastTemp stringByAppendingString:@" (Last)"];
        [self SetItemText:ITEM_POOLTEMP Text:lastTemp];
    }
    [self SetItemText:ITEM_POOLSP            Text:[m_pConfig FormatDegText:[m_pConfig HeatSetPoint:BODYTYPE_POOL]]];
    [self SetItemText:ITEM_POOLHEATMODE      Text:[m_pConfig HeatModeText:[m_pConfig HeatMode:BODYTYPE_POOL]]];
    [self SetItemText:ITEM_POOLHEATSTATUS    Text:[m_pConfig HeatStatusText:[m_pConfig HeatStatus:BODYTYPE_POOL]]];
    if([m_pConfig SpaIsOn])
    {
        [self SetItemText:ITEM_SPATEMP           Text:[m_pConfig FormatDegText:[m_pConfig WaterTemp:BODYTYPE_SPA]]];
    }
    else
    {
        NSString *lastTemp = [m_pConfig FormatDegText:[m_pConfig WaterTemp:BODYTYPE_SPA]];
        lastTemp = [lastTemp stringByAppendingString:@" (Last)"];
        [self SetItemText:ITEM_SPATEMP Text:lastTemp];
    }
    [self SetItemText:ITEM_SPASP             Text:[m_pConfig FormatDegText:[m_pConfig HeatSetPoint:BODYTYPE_SPA]]];
    [self SetItemText:ITEM_SPAHEATMODE       Text:[m_pConfig HeatModeText:[m_pConfig HeatMode:BODYTYPE_SPA]]];
    [self SetItemText:ITEM_SPAHEATSTATUS     Text:[m_pConfig HeatStatusText:[m_pConfig HeatStatus:BODYTYPE_SPA]]];
    [self SetItemText:ITEM_AIRTEMP           Text:[m_pConfig FormatDegText:[m_pConfig AirTemp]]];
    [self SetItemText:ITEM_PHORP             Text:[m_pConfig PHORPText]];
    [self SetItemText:ITEM_REMOTES           Text:[m_pConfig EnableText:[m_pConfig RemotesEnabled]]];
    [self SetItemText:ITEM_FREEZE            Text:[m_pConfig FreezeModeStatusText]];
}
/*============================================================================*/

-(void)AddItem:(int)iType Label:(NSString*)pLabel

/*============================================================================*/
{
    CPoolSummaryItem *pItem = [[CPoolSummaryItem alloc] initWithFrame:CGRectZero Type:iType Label:pLabel];
    [self addSubview:pItem];
    [m_pControls addObject:pItem];
    [pItem release];
}
/*============================================================================*/

-(void)AddItem:(int)iType Label:(NSString*)pLabel Suffix:(NSString*)pSuffix

/*============================================================================*/
{
    NSString *psLabel = [NSString stringWithFormat:@"%s %s", [pLabel UTF8String], [pSuffix UTF8String]];
    [self AddItem:iType Label:psLabel];
}
/*============================================================================*/

-(void)SetItemText:(int)iType Text:(NSString*)pText

/*============================================================================*/
{
    for (int i = 0; i < [m_pControls count]; i++)
    {
        CPoolSummaryItem *pItem = (CPoolSummaryItem*)[m_pControls objectAtIndex:i];
        if ([pItem Type] == iType)
        {
            [pItem SetText:pText];
            return;
        }
    }
}
/*============================================================================*/

-(void)SetLabelText:(int)iType Text:(NSString*)pText

/*============================================================================*/
{
    for (int i = 0; i < [m_pControls count]; i++)
    {
        CPoolSummaryItem *pItem = (CPoolSummaryItem*)[m_pControls objectAtIndex:i];
        if ([pItem Type] == iType)
        {
            [pItem SetLabel:pText];
            return;
        }
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iDYAll = [m_pControls count] * DY_ITEM;

    int iNCols = 1;
    int iNRows = [m_pControls count];
    
    if (iDYAll > rBounds.size.height)
    {
        iNCols = 2;
        iNRows = rBounds.size.height / DY_ITEM;
    }

    while (iNRows * iNCols < [m_pControls count])
        iNRows++;

    int iCol = 0;
    int iRow = 0;
    
    [CRect CenterDownToY:&rBounds DY:iNRows * DY_ITEM_MAX];

    for (int i = 0; i < [m_pControls count]; i++)
    {
        CGRect rCol = [CRect CreateXSection:rBounds Index:iCol Of:iNCols];
        CGRect rItem = [CRect CreateYSection:rCol Index:iRow Of:iNRows];
        CPoolSummaryItem *pItem = (CPoolSummaryItem*)[m_pControls objectAtIndex:i];
        [pItem setFrame:rItem];
        iRow++;
        if (iRow >= iNRows)
        {
            iCol++;
            iRow = 0;
        }
    }
    
    //CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETSTATUSQ] autorelease];
    //[pQ1 PutInt:0];
    //[pQ1 SendMessageWithMyID:self];
}
@end
