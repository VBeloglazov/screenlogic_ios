/*
--------------------------------------------------------------------------------

    HLOBLIST.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLOBLIST_H
#define HLOBLIST_H

#include "hlpos.h"
#include "hlapi.h"

/*============================================================================*/

    class HL_API CHLObList

/*============================================================================*/
{
protected:
    CHLPos                      *m_pStart;
    CHLPos                      *m_pEnd;
    int                         m_iCount;


public:
    CHLObList();

    // CHLObList overrides: all
    virtual ~CHLObList();

    virtual BOOL                IsEmpty();

    virtual int                 GetCount();

    virtual void                *GetHead();
    virtual void                *GetTail();

    virtual CHLPos              *GetHeadPosition();
    virtual CHLPos              *GetTailPosition();

    virtual void                *GetAt(
    CHLPos                      *pOP );

    virtual void                *GetNext(
    CHLPos                      *&pOP );

    virtual void                *GetPrev(
    CHLPos                      *&pOP );

    virtual CHLPos              *Find(
    void                        *pObj );

    virtual CHLPos              *FindIndex(
    int                         iIndex );

    virtual void                AddHead(
    void                        *pObj );

    virtual void                AddTail(
    void                        *pObj );

    virtual void                AddTail(
    void                        *pObj,
    int                         iSortIndex );

    virtual void                InsertBefore(
    CHLPos                      *pOP,
    void                        *pObj );

    virtual void                InsertAfterObject(
    CHLObList                   *pNewItems,
    void                        *pObj );

    virtual BOOL                Contains(
    void                        *pObj );

    virtual void                Remove(
    void                        *pObj );

    virtual void                *RemoveHead();

    virtual void                *RemoveTail();

    virtual void                RemoveAt(
    CHLPos                      *pPos );

    virtual void                RemoveAll();

    virtual CHLObList           *CreateSortedList();
};



#endif

