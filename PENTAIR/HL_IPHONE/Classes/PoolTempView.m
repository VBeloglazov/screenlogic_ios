//
//  CPoolTempView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PoolTempView.h"
#import "NSPoolConfig.h"
#import "hlbutton.h"
#import "hlm.h"
#import "CustomPage.h"

@implementation CPoolTempView

#define CMD_TEMPUP                      1000
#define CMD_TEMPDN                      1001

//DL - 6/30/2015 - font issue (clipping) - was 38
#define DY_LABEL_IPAD                      32
#define DY_LABEL_IPOD                      25

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Config:(CNSPoolConfig*)pConfig BodyType:(int)iBodyType Mode:(int)iMode;

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_pConfig = pConfig;
        [m_pConfig AddSink:self];
        m_iBodyType = iBodyType;
        m_iMode = iMode;

        m_pTempText = [[UILabel alloc] initWithFrame:CGRectZero];
        m_pTempText.backgroundColor = [UIColor clearColor];
        m_pTempText.opaque = NO;
        m_pTempText.textColor = [UIColor whiteColor];
        [m_pTempText setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [m_pTempText setAdjustsFontSizeToFitWidth:YES];
        [m_pTempText setMinimumFontSize:12];
        m_pTempText.textAlignment = UITextAlignmentCenter;
        [m_pTempText setText:@"-"];
        [self addSubview:m_pTempText];

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:18];
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        m_pLabel.numberOfLines = 2;
        m_pLabel.textAlignment = UITextAlignmentCenter;
        [m_pLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];

        if (m_iMode != TEMP_ACT)
        {
            UIColor *pRGBTint = [UIColor colorWithRed:.1 green:0.0 blue:0.0 alpha:0.0];

            m_pUp = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYUP Text:NULL TextSize:20 TextColor:NULL Color:pRGBTint Icon:NULL];
            m_pDn = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYDN Text:NULL TextSize:20 TextColor:NULL Color:pRGBTint Icon:NULL];
            [m_pUp SetCommandID:CMD_TEMPUP Responder:self];
            [m_pDn SetCommandID:CMD_TEMPDN Responder:self];
            [m_pUp SetRepeatDwell:300 Rate:4];
            [m_pDn SetRepeatDwell:300 Rate:4];
        }

        [self addSubview:m_pUp];
        [self addSubview:m_pDn];

        switch (m_iMode)
        {
        case TEMP_ACT: 
            {
                [self setLabels];
            }
            break;
        case TEMP_HEAT: { [m_pLabel setText:@"Set Point"];   m_pTempText.textColor = [UIColor colorWithRed:1.0 green:.5 blue:.5 alpha:1.0];    } break;
        }

        [self addSubview:m_pLabel];
        [self Notify:0 Data:0];
        
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pUp release];
    [m_pDn release];
    [m_pConfig RemoveSink:self];
    [m_pTempText release];
    [m_pLabel release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iVal = 0;
    switch (m_iMode)
    {
    case TEMP_ACT: { iVal = [m_pConfig WaterTemp:m_iBodyType];   }   break;
    case TEMP_HEAT: { iVal = [m_pConfig HeatSetPoint:m_iBodyType];     }   break;
    }

    if (m_iMode != TEMP_ACT)
    {
        if ([m_pUp HasCapture] || [m_pDn HasCapture])
            iVal = m_iLocalTemp;
    }
    else
    {
        [self setLabels];
    }

    NSString *pText = [NSString stringWithFormat:@"%d°", iVal];
    if (iVal == 0 || iVal == 0xFF)
        pText = @"-";

    m_pLabel.hidden = FALSE;
    [m_pTempText setText:pText];
}
/*============================================================================*/

-(void)setLabels

/*============================================================================*/
{
    NSString *pLabel = NULL;
    if (m_iBodyType == BODYTYPE_POOL)
    {
        if([m_pConfig PoolIsOn])
        {
            pLabel = [NSString stringWithFormat:@"%s Temperature", [[m_pConfig PoolText] UTF8String]];
        }
        else
        {
            pLabel = [NSString stringWithFormat:@"Last %s Temperature", [[m_pConfig PoolText] UTF8String]];
            
        }
    }
    if (m_iBodyType == BODYTYPE_SPA)
    {
        if([m_pConfig SpaIsOn])
        {
            pLabel = [NSString stringWithFormat:@"%s Temperature", [[m_pConfig SpaText] UTF8String]];
        }
        else
        {
            pLabel = [NSString stringWithFormat:@"Last %s Temperature", [[m_pConfig SpaText] UTF8String]];
        }
    }
    [m_pLabel setText:pLabel];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (![m_pUp HasCapture] && ![m_pDn HasCapture])
    {
        m_iLocalTemp = [m_pConfig HeatSetPoint:m_iBodyType];
    }


    int iDir = 1;
    switch (iCommandID)
    {
    case CMD_TEMPUP:
        {
            if (m_iLocalTemp >= [m_pConfig MaxSetPoint:m_iBodyType])
                return;
        }
        break;
    case CMD_TEMPDN:
        {
            if (m_iLocalTemp <= [m_pConfig MinSetPoint:m_iBodyType])
                return;
            iDir = -1;
        }
        break;
    default:
        return;
    }

    m_iLocalTemp += iDir;

    [m_pConfig SetHeatSetPoint:m_iBodyType Temp:m_iLocalTemp];
    [self Notify:0 Data:0];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    if (iDXThis == m_iDXLast && iDYThis == m_iDYLast)
    {
        return;
    }

    m_iDXLast = iDXThis;
    m_iDYLast = iDYThis;


    int iDXLabel = 300;
    int iDYLabel = DY_LABEL_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iDXLabel = 350;
        iDYLabel = DY_LABEL_IPAD;
    }

    CGRect rLabel   = CGRectMake(0, 0, iDXThis/2, iDYLabel);

    int iBtnInset = iDXThis / 20;
    CGRect rUp      = CGRectMake(iDXThis/2+iBtnInset, iBtnInset,  iDXThis/2-2*iBtnInset, iDYThis/2-2*iBtnInset);
    CGRect rDn      = CGRectMake(iDXThis/2+iBtnInset, iDYThis/2+iBtnInset,  iDXThis/2-2*iBtnInset, iDYThis/2-2*iBtnInset);
    CGRect rTemp    = CGRectMake(0, 0, iDXThis/2, iDYThis);

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        rTemp.origin.y = iDYLabel;

    if (m_iMode == TEMP_ACT)
    {
        int iXMid = self.bounds.origin.x + self.bounds.size.width / 2 ;
        rLabel = CGRectMake(iXMid - iDXLabel/2, 0, iDXLabel, iDYLabel);
        rTemp.size.width = iDXThis;
    }

    int iFontSize = rTemp.size.height * 40 / 100;
    [CCustomPage InitStandardLabel:m_pTempText Size:iFontSize];
    CGSize sz = [m_pTempText.text sizeWithFont:m_pTempText.font];
    //DL - 6/30/2015 - font issue (clipping)
    while ((sz.width > (rTemp.size.width - 10)|| sz.height > rTemp.size.height) && iFontSize > 12)
    {
        iFontSize--;
        [CCustomPage InitStandardLabel:m_pTempText Size:iFontSize];
        sz = [m_pTempText.text sizeWithFont:m_pTempText.font];
    }
    
    [CCustomPage InitStandardLabel:m_pLabel Size:iDYLabel * 75 / 100];

    [m_pTempText setFrame:rTemp];
    [m_pLabel setFrame:rLabel];
    [m_pUp setFrame:rUp];
    [m_pDn setFrame:rDn];
}
@end

