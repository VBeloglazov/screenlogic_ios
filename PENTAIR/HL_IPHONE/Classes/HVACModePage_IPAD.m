//
//  HVACModePage_IPAD.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/29/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HVACModePage_IPAD.h"
#import "HVACTempView.h"
#import "tstat.h"
#import "hlm.h"
#import "hlradio.h"
#import "MainView.h"
#import "crect.h"
#import "hlbutton.h"
#import "hlsegment.h"

@implementation CHVACModePage_IPAD

#define DX_INSET_IPAD                   32
#define DY_MODES_IPAD                   70
#define DY_BUTTON_IPAD                  60
#define DX_LABEL_IPAD                   175
#define DY_LABEL_IPAD                   100

#define CMD_MODE                        1000
#define CMD_FAN                         1001
#define CMD_RUN_PROG                    1002

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_iHVACState = -1;
        m_pTStat = pTStat;
        [m_pTStat AddSink:self];
        [self InitModeControl];

        self.autoresizingMask = 0xFFFFFFFF;

        if ([m_pTStat StateOK])
        {
            [self Notify:0 Data:0];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTStat RemoveSink:self];
    [m_pMode release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_CONFIGCHANGED)
        [self InitModeControl];
        
    int iHVACMode = [m_pTStat HVACMode];
    if (iHVACMode == HVAC_STATE_NOTOK)
    {
        [m_pMode setUserInteractionEnabled:NO];
        [m_pMode setAlpha:0.0];
    }
    else 
    {
        [m_pMode setUserInteractionEnabled:YES];
        [m_pMode setAlpha:1.0];
    }

    int iIndex = -1;
    for (int i = 0; i < [m_pMode NItems] && iIndex == -1; i++)
    {
        if (m_iModes[i] == iHVACMode)
            iIndex = i;
    }

    [m_pMode SetSelectedIndex:iIndex];

    if (m_pFan == NULL)
        return;
    iIndex = 0;
    int iFanState = [m_pTStat FanMode];
    if (iFanState != FAN_STATE_AUTO)
    {
        if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
        {
            switch (iFanState)
            {
            case FAN_STATE_LOW   : iIndex = 1;  break;
            case FAN_STATE_CONT  : iIndex = 2;  break;
            case FAN_STATE_HI    : iIndex = 3;  break;
            }
        }
        else
        {
            iIndex = 1;
        }

    }
    [m_pFan SetSelectedIndex:iIndex];

    int iProgramState = [m_pTStat ProgramState];
    if (iProgramState == PRG_STATE_RUN)
    {
        [m_pRunProg SetSelectedIndex:0];
    }
    else 
    {
        [m_pRunProg SetSelectedIndex:-1];
    }
}
/*============================================================================*/

-(void)InitModeControl

/*============================================================================*/
{
    if (m_pMode != NULL)
    {
        [m_pMode removeFromSuperview];
        [m_pMode release];
        m_pMode = NULL;
    }

    if (m_pRunProg != NULL)
    {
        [m_pRunProg removeFromSuperview];
        [m_pRunProg release];
        m_pRunProg = NULL;
    }

    int iNModeButtons = 1;
    int iNFanButtons = 0;

    if ([m_pTStat HasFan])
    {
        iNFanButtons = 2;
        if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
            iNFanButtons = 4;
    }

    NSMutableArray *pModes = [[NSMutableArray alloc] init];
    [pModes addObject:@"Off"];
    BOOL bHeat = FALSE;
    BOOL bCool = FALSE;
    if ([m_pTStat HasHeat])
    {
        bHeat = TRUE;
        [pModes addObject:@"Heat"];
        m_iModes[iNModeButtons] = HVAC_STATE_HEAT;
        iNModeButtons++;
    }
    if ([m_pTStat HasCool])
    {
        bCool = TRUE;
        [pModes addObject:@"Cool"];
        m_iModes[iNModeButtons] = HVAC_STATE_COOL;
        iNModeButtons++;
    }

    if (([m_pTStat HVACFlags] & HVAC_HASAUTO) && bHeat && bCool)
    {
        [pModes addObject:@"Auto"];
        m_iModes[iNModeButtons] = HVAC_STATE_AUTO;
        iNModeButtons++;
    }

    //
    // iPad left to right
    //
    m_pMode = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Items:pModes Type:HLSEGMENT_BUTTON Data:NO];
    [pModes release];
    [self addSubview:m_pMode];
    [m_pMode addTarget:self action:@selector(SetMode:) forControlEvents:UIControlEventValueChanged];

    if ([m_pTStat HasSchedule])
    {
        pModes = [[NSMutableArray alloc] init];
        [pModes addObject:@"Run Pgm"];
        m_pRunProg = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Items:pModes Type:HLSEGMENT_BUTTON Data:NO];
        [self addSubview:m_pRunProg];
        [m_pRunProg addTarget:self action:@selector(SetRunProgram:) forControlEvents:UIControlEventValueChanged];
        [pModes release];
    }

    if (![m_pTStat HasFan])
        return;
    
    pModes = [[NSMutableArray alloc] init];
    [pModes addObject:@"Fan Auto"];
    if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
    {
        [pModes addObject:@"Low"];
        [pModes addObject:@"Medium"];
        [pModes addObject:@"High"];
    }
    else
    {
        [pModes addObject:@"Fan On"];
    }

    m_pFan = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Items:pModes Type:HLSEGMENT_BUTTON Data:NO];
    [pModes release];
    [m_pFan addTarget:self action:@selector(SetFan:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:m_pFan];
        
}
/*============================================================================*/

-(void)SetMode:(id)sender

/*============================================================================*/
{
    int iIndex = [m_pMode SelectedIndex];

    NSString *pMode = NULL;
    switch (m_iModes[iIndex])
    {
    case HVAC_STATE_OFF:  pMode = @"Off"; break;
    case HVAC_STATE_HEAT: pMode = @"Heat"; break;
    case HVAC_STATE_COOL: pMode = @"Cool"; break;
    case HVAC_STATE_AUTO: pMode = @"Auto"; break;
    }

    NSString *psMSG = [NSString stringWithFormat:@"Are you sure you change to %s mode?", [pMode UTF8String]];

    UIAlertView *pAlert = [[UIAlertView alloc] initWithTitle:@"Change Mode" message:psMSG
                            delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"OK", nil];

    [pAlert show];	
    [pAlert release];
}
/*============================================================================*/

-(void)SetFan:(id)sender

/*============================================================================*/
{
    int iFanMode = FAN_STATE_AUTO;
    
    int iIndex = [m_pFan SelectedIndex];

    if (iIndex > 0)
    {
        if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
        {
            switch (iIndex)
            {
            case 1: iFanMode = FAN_STATE_LOW;   break;
            case 2: iFanMode = FAN_STATE_CONT;  break;
            case 3: iFanMode = FAN_STATE_HI;    break;
            }
        }
        else
        {
            iFanMode = FAN_STATE_CONT;
        }
    }

    [m_pTStat SetFanMode:iFanMode];
}
/*============================================================================*/

-(void)SetRunProgram:(id)sender

/*============================================================================*/
{
    [m_pTStat SetProgramState:PRG_STATE_RUN]; 
}
/*============================================================================*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

/*============================================================================*/
{
    if (buttonIndex == 1)
    {
        int iIndex = [m_pMode SelectedIndex];
        [m_pTStat SetHVACMode:m_iModes[iIndex]];
    }
    else 
    {
        [self Notify:0 Data:0];
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iNModeButtons = 0;
    int iNFanButtons = 0;
    int iNSchedButtons = 0;

    iNModeButtons = [m_pMode NItems];
    iNFanButtons  = [m_pFan NItems];
    
    if (m_pRunProg != NULL)
        iNSchedButtons = 1;

    int iDXButton = 100;
    int iDXModeSection  = iNModeButtons * iDXButton;
    int iDXFanSection   = iNFanButtons  * iDXButton;
    int iDXSchedSection = iNSchedButtons * iDXButton;
    int iDXAll          = iDXModeSection + iDXFanSection + iDXSchedSection;
    
    CGRect rThis = self.bounds;
    int iDXMax = rThis.size.width;

    CGRect rMode = [CRect BreakOffLeft:&rThis DX:iDXModeSection * iDXMax / (iDXAll)];
    CGRect rFan  = [CRect BreakOffLeft:&rThis DX:iDXFanSection * iDXMax / (iDXAll)];
    CGRect rProg = rThis;

    [CRect CenterDownToX:&rMode DX:iDXModeSection];
    [CRect CenterDownToX:&rFan  DX:iDXFanSection];
    [CRect CenterDownToX:&rProg DX:iDXButton];
    
    int iDXSpaces = iDXMax - rProg.size.width - rMode.size.width - rFan.size.width;
    int iNSpaces = 2;
    if (iNSchedButtons > 0)
        iNSpaces++;
    
    if (iNFanButtons > 0)
        iNSpaces = 4;
        
    int iPad = iDXSpaces / iNSpaces;
    rMode.origin.x = iPad;
    rFan.origin.x = rMode.origin.x + rMode.size.width + iPad;
    rProg.origin.x = rFan.origin.x + rFan.size.width + iPad;

    if (iNFanButtons < 1)
        rProg.origin.x -= iPad;

    [m_pMode setFrame:rMode];
    [m_pFan setFrame:rFan];
    [m_pRunProg setFrame:rProg];
}
@end
