//
//  HLPageControl.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/11/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMainTabViewController;
@class CDotView;

/*============================================================================*/

@interface CHLPageControl : UIView

/*============================================================================*/
{
    CMainTabViewController                *m_pMainTabPage;
    int                         m_iNPages;
    CDotView                    *m_pDotView;

}

- (id)initWithFrame:(CGRect)frame MainTabPage:(CMainTabViewController*)pPage;

-(void)SetNumberOfPages:(int)iN;
-(void)SetCurrentPage:(int)iPage;

@end

/*============================================================================*/

@interface CDotView : UIView

/*============================================================================*/
{
}

@end
