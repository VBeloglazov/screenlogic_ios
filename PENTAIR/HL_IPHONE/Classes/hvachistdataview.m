//
//  hvachistdataview.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "hvachistdataview.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "timeview.h"
#import "tstat.h"
#import "nsmsg.H"
#import "RTEView.h"
#import "TimeBar.h"
#import "hvachistview.h"
#import "CustomPage.h"



@implementation CHVACHistoryDataView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView TStat:(CTStat*)pTStat LastView:(CHVACHistoryDataView*)pLastView

/*============================================================================*/
{
    if (self = [super initWithFrame:frame TimeView:pTimeView]) 
    {
        m_iTempMin = MIN_TEMP;
        m_iTempMax = MAX_TEMP;
        m_iXOrigin = frame.origin.x;
        int iXLeft = frame.origin.x;
        int iXRight = iXLeft + frame.size.width;
        NSDate *pStart = [[pTimeView GetTimeAtX:iXLeft] autorelease];
        NSDate *pEnd   = [[pTimeView GetTimeAtX:iXRight] autorelease];
        BOOL bNeedData = TRUE;

        // Initialization code
        if (pLastView != NULL)
        {
            m_iTempMin = [pLastView MinTemp];
            m_iTempMax = [pLastView MaxTemp];

            if (([pStart laterDate:[pLastView StartDataDate]] == pStart) && ([pEnd earlierDate:[pLastView EndDataDate]] == pEnd))
            {
                [m_pDateDataStart release];
                [m_pDateDataEnd release];
                m_pDateDataStart = [pLastView StartDataDate];
                m_pDateDataEnd = [pLastView EndDataDate];
                [m_pDateDataStart retain];
                [m_pDateDataEnd retain];
                bNeedData = FALSE;
                m_bDataOK = TRUE;
            }

            m_pTempSeries = [pLastView TempSeries];
            m_pRTESeries = [pLastView RTESeries];
            [self AddRTEViews];


            [m_pTempSeries retain];
            [m_pRTESeries retain];

            if (![pLastView DataOK])
            {
                m_pDateDataStart = [pTimeView GetTimeAtX:frame.origin.x];
                m_pDateDataEnd = [pTimeView GetTimeAtX:frame.origin.x + frame.size.width];
                bNeedData = TRUE;
            }
         }
        m_pTStat = pTStat;

        if (bNeedData)
        {
            [m_pTimeView SetWorking:YES];

            if (pLastView == NULL)
                [self PostQuery];
            else
                m_pQueryTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(PostQuery) userInfo:nil repeats:NO] retain];
        }
        else
        {
            [m_pTimeView SetWorking:NO];
            m_bCheckScale = TRUE;
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    m_pTimeView = NULL;
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    [m_pRTEHeatView release];
    [m_pRTECoolView release];
    [m_pQueryTimer release];
    [m_pTempSeries release];
    [m_pRTESeries release];
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();

    printf("BeginDraw\n");

    [self DrawDayRects:pRef];

    UIColor *pTick = [UIColor colorWithRed:.25 green:.25 blue:0 alpha:1.0 ];
    [pTick set];
    CGPoint pPts[2];
    pPts[0].x = 0;
    pPts[1].x = self.frame.size.width;
    
    for (int iTick = m_iTempMin; iTick <= m_iTempMax; iTick += 10)
    {
        int iY = [self YLogical:iTick];
        pPts[0].y = iY;
        pPts[1].y = iY;
        CGContextStrokeLineSegments(pRef, pPts, 2);
    }

//    CGContextSetAllowsAntialiasing(pRef, FALSE);
    CGContextSetLineWidth(pRef, 2);

    UIColor *pRGBHeat = [UIColor colorWithRed:1.0 green:0 blue:0 alpha:.25];
    UIColor *pRGBCool = [UIColor colorWithRed:0 green:0 blue:1.0 alpha:.25];
    CTempSeries *pHeatSP = (CTempSeries*)[m_pTempSeries objectAtIndex:2];
    CTempSeries *pCoolSP = (CTempSeries*)[m_pTempSeries objectAtIndex:3];
    [self DrawSetPoints:pRef Color:pRGBHeat Series:pHeatSP Dir:FALSE];
    [self DrawSetPoints:pRef Color:pRGBCool Series:pCoolSP Dir:TRUE];

    NSDate *pDate0 = [[m_pTimeView GetTimeAtX:m_iXOrigin] autorelease];
    for (int i = 0; i < [m_pTempSeries count]; i++)
    {
        CTempSeries *pSeries = (CTempSeries*)[m_pTempSeries objectAtIndex:i];
        UIColor *pColor = [pSeries Color];
        [pColor setStroke];
        NSMutableArray *pPoints = [pSeries PointList];

        CGContextBeginPath (pRef);

        BOOL bPoints = FALSE;
        BOOL bDone = FALSE;
        CDataPoint *pStartPoint = NULL;
        
        for (int iPoint = 0; iPoint < [pPoints count] && !bDone; iPoint++)
        {
            CDataPoint *pDataPoint = (CDataPoint*)[pPoints objectAtIndex:iPoint];
            NSDate *pPointDate = [pDataPoint Date];
            if ([pDate0 earlierDate:pPointDate] == pDate0)
            {

                if (!bPoints)
                {
                    if (pStartPoint == NULL)
                        pStartPoint = pDataPoint;
                    int iX0 = [m_pTimeView XLogical:[pStartPoint Date]] - m_iXOrigin;
                    int iY0 = [self YLogical:[pStartPoint Val]];
                    CGContextMoveToPoint(pRef, iX0, iY0);
                    bPoints = TRUE;
                }

                if (pStartPoint != pDataPoint)
                {
                    int iX = [m_pTimeView XLogical:pPointDate] - m_iXOrigin;
                    int iY = [self YLogical:[pDataPoint Val]];
                    CGContextAddLineToPoint(pRef, iX, iY);

                    if (iX > self.frame.size.width)
                        bDone= TRUE;
                }


            }
            else
            {
                pStartPoint = pDataPoint;
            }
            
        }

        CGContextDrawPath(pRef, kCGPathStroke);
    }

    if (m_bDataOK)
        [m_pTimeView SetWorking:NO];
        
    if (m_bCheckScale)
    {
        int iTempMin = -100;
        int iTempMax = -100;
        m_bCheckScale = FALSE;
        NSDate *pStart = [[m_pTimeView GetTimeAtX:0] autorelease];
        NSDate *pEnd   = [[m_pTimeView GetTimeAtX:m_pTimeView.frame.size.width] autorelease];
        for (int i = 0; i < [m_pTempSeries count]; i++)
        {
            CTempSeries *pSeries = (CTempSeries*)[m_pTempSeries objectAtIndex:i];
            int iMin = [pSeries MinTempFrom:pStart To:pEnd];
            int iMax = [pSeries MaxTempFrom:pStart To:pEnd];
        
            if (iMin > HVAC_MIN_TEMP && iMin < HVAC_MAX_TEMP)
            {
                if (iTempMin == -100)
                    iTempMin = iMin;
                else
                    iTempMin = MIN(iMin, iTempMin);
            }

            if (iMax > HVAC_MIN_TEMP && iMax < HVAC_MAX_TEMP)
            {
                if (iTempMax == -100)
                    iTempMax = iMax;
                else
                    iTempMax = MAX(iMax, iTempMax);
            }
        }

        printf("1: Min %d Max %d\n", iTempMin, iTempMax);

        if (iTempMin == -100)
            iTempMin = MIN_TEMP;
        if (iTempMax == -100)
            iTempMax = MAX_TEMP;
        
        iTempMax = [self RoundTempUp:iTempMax];
        iTempMin = [self RoundTempDown:iTempMin];
        
        [self SetScaleMin:iTempMin Max:iTempMax];
    }
    
    if (m_iFrameResizeNew != m_iFrameResizeOld)
    {
        [self ResizeFrameFrom:m_iFrameResizeOld To:m_iFrameResizeNew];
        m_iFrameResizeOld = m_iFrameResizeNew;
    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_HVAC_HISTORYDATA)
        return;

    [m_pRTEHeatView removeFromSuperview];
    [m_pRTECoolView removeFromSuperview];
    [m_pRTEHeatView release];
    [m_pRTECoolView release];
    m_pRTEHeatView = NULL;
    m_pRTECoolView = NULL;

    [m_pTempSeries release];
    [m_pRTESeries release];

    m_pTempSeries = [[NSMutableArray alloc]init];
    m_pRTESeries = [[NSMutableArray alloc]init];
    
    //
    // Temperature data
    // 

    int iTempMin = -100;
    int iTempMax = -100;
    NSDate *pStart = [[m_pTimeView GetTimeAtX:0] autorelease];
    NSDate *pEnd   = [[m_pTimeView GetTimeAtX:m_pTimeView.frame.size.width] autorelease];

    for (int i = 0; i < 4; i++)
    {
        UIColor *pRGB = NULL;
        switch (i)
        {
        case 0: pRGB = [UIColor  greenColor];   break;
        case 1: pRGB = [UIColor  whiteColor];   break;
        case 2: pRGB = [UIColor  redColor];     break;
        case 3: pRGB = [UIColor  blueColor];    break;
        }
    
        CTempSeries *pSeries = [[CTempSeries alloc] initWithColor:pRGB];
        [m_pTempSeries addObject:pSeries];
        [pSeries ReadFrom:pMSG];
        [pSeries release];


        int iMin = [pSeries MinTempFrom:pStart To:pEnd];
        int iMax = [pSeries MaxTempFrom:pStart To:pEnd];
    
        if (iMin > HVAC_MIN_TEMP && iMin < HVAC_MAX_TEMP)
        {
            if (iTempMin == -100)
                iTempMin = iMin;
            else
                iTempMin = MIN(iMin, iTempMin);
        }
        if (iMax > HVAC_MIN_TEMP && iMax < HVAC_MAX_TEMP)
        {
            if (iTempMax == -100)
                iTempMax = iMax;
            else
                iTempMax = MAX(iMax, iTempMax);
        }
    }

    if (iTempMin == -100)
        iTempMin = MIN_TEMP;
    if (iTempMax == -100)
        iTempMax = MAX_TEMP;

    printf("2: Min %d Max %d\n", iTempMin, iTempMax);

    iTempMax = [self RoundTempUp:iTempMax];
    iTempMin = [self RoundTempDown:iTempMin];

    //
    // RTE Data
    //
    for (int i = 0; i < 3; i++)
    {
        UIColor *pRGB = NULL;
        switch (i)
        {
        case 0: pRGB = [UIColor  colorWithRed:.5 green:1.0 blue:.5 alpha:1.0];   break;
        case 1: pRGB = [UIColor  colorWithRed:1.0 green:.5 blue:.5 alpha:1.0];   break;
        case 2: pRGB = [UIColor  colorWithRed:.5 green:.5 blue:1.0 alpha:1.0];   break;
        }
    
        CRTESeries *pSeries = [[CRTESeries alloc] initWithColor:pRGB];
        [m_pRTESeries addObject:pSeries];
        [pSeries ReadFrom:pMSG];
        [pSeries release];
    }

    [self AddRTEViews];
    [self UpdateTimeBar];

    printf("End Read MSG\n");
    m_bDataOK = TRUE;
    if (![self SetScaleMin:iTempMin Max:iTempMax])
        [self setNeedsDisplay];
}
/*============================================================================*/

-(int)YLogical:(int)iTemp

/*============================================================================*/
{
    int iDYThis = self.frame.size.height;
    int iDY = (iTemp - m_iTempMin) * iDYThis / (m_iTempMax-m_iTempMin);
    return iDYThis - iDY;
}
/*============================================================================*/

-(void)DrawSetPoints:(CGContextRef)pRef Color:(UIColor*)pRGB Series:(CTempSeries*)pSeries Dir:(BOOL)bTopDown

/*============================================================================*/
{
    [pRGB set];
    int iX0 = 0;
    NSMutableArray *pPoints = [pSeries PointList];
    for (int iPoint = 0; iPoint < [pPoints count]; iPoint++)
    {
        CDataPoint *pDataPoint = (CDataPoint*)[pPoints objectAtIndex:iPoint];
        int iX1 = [m_pTimeView XLogical:[pDataPoint Date]] - m_iXOrigin;
        int iY = [self YLogical:[pDataPoint Val]];
        int iY0 = 0;
        int iY1 = iY;
        if (!bTopDown)
        {
            iY0 = iY;
            iY1 = self.frame.size.height;
        }

        if ([pDataPoint Val] > -100)
            CGContextFillRect(pRef, CGRectMake(iX0, iY0, iX1 - iX0, iY1-iY0));


        iX0 = iX1;
        if (iX0 >= self.frame.size.width)
            return;
    }
}
/*============================================================================*/

-(NSMutableArray*)TempSeries

/*============================================================================*/
{
    return m_pTempSeries;
}
/*============================================================================*/

-(NSMutableArray*)RTESeries

/*============================================================================*/
{
    return m_pRTESeries;
}
/*============================================================================*/

-(void)PostQuery

/*============================================================================*/
{
    int iXLeft = m_iXOrigin;
    int iXRight = iXLeft + self.frame.size.width;
    NSDate *pStart = [[m_pTimeView GetTimeAtX:iXLeft] autorelease];
    NSDate *pEnd   = [[m_pTimeView GetTimeAtX:iXRight] autorelease];
    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_HVAC_GETHISTORYBYTSTATQ] autorelease];
    [pQ PutInt:m_pTStat.m_iID];
    [pQ PutTime:pStart];
    [pQ PutTime:pEnd];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
}
/*============================================================================*/

-(void)StopQuery

/*============================================================================*/
{
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
}
/*============================================================================*/

-(void)NotifyPosition

/*============================================================================*/
{
    int iDYRTE = 5;
    int iDYSuper = m_pTimeView.frame.size.height;
    int iYCool = iDYSuper - [CMainView DY_TIMEBAR] - 1 - self.frame.origin.y - 2*iDYRTE;
    int iYHeat = iYCool - iDYRTE;

    if (m_pRTEHeatView != NULL)
    {
        CGRect rHeat = m_pRTEHeatView.frame;
        rHeat.origin.y = iYHeat;
        rHeat.size.width = self.frame.size.width;
        [m_pRTEHeatView setFrame:rHeat];
    }

    if (m_pRTECoolView != NULL)
    {
        CGRect rCool = m_pRTECoolView.frame;
        rCool.origin.y = iYCool;
        rCool.size.width = self.frame.size.width;
        [m_pRTECoolView setFrame:rCool];
    }
}
/*============================================================================*/

-(void)AddRTEViews

/*============================================================================*/
{
    [m_pRTEHeatView removeFromSuperview];
    [m_pRTECoolView removeFromSuperview];
    [m_pRTEHeatView release];
    [m_pRTECoolView release];
    m_pRTEHeatView = NULL;
    m_pRTECoolView = NULL;

    int iDYRTE = 5;
    int iDYSuper = m_pTimeView.frame.size.height;
    int iYCool = iDYSuper - [CMainView DY_TIMEBAR] - 1 - self.frame.origin.y - 2*iDYRTE;
    int iYHeat = iYCool - iDYRTE;
    
    
    if ([m_pRTESeries count] >= 2)
    {
        CRTESeries *pHeatRTE = (CRTESeries*)[m_pRTESeries objectAtIndex:1];
        if ([[pHeatRTE PointList] count] > 0)
        {
            m_pRTEHeatView = [[CRTEView alloc] initWithFrame:CGRectMake(0, iYHeat, self.frame.size.width, iDYRTE) XOrigin:m_iXOrigin Series:pHeatRTE TimeView:m_pTimeView];
            [self addSubview:m_pRTEHeatView];
        }
    }

    if ([m_pRTESeries count] >= 3)
    {
        CRTESeries *pCoolRTE = (CRTESeries*)[m_pRTESeries objectAtIndex:2];
        if ([[pCoolRTE PointList] count] > 0)
        {
            m_pRTECoolView = [[CRTEView alloc] initWithFrame:CGRectMake(0, iYCool, self.frame.size.width, iDYRTE) XOrigin:m_iXOrigin Series:pCoolRTE TimeView:m_pTimeView];
            [self addSubview:m_pRTECoolView];
        }
    }
}
/*============================================================================*/

-(void)UpdateRTEViews

/*============================================================================*/
{
    [m_pRTEHeatView SetXOrigin:m_iXOrigin];
    [m_pRTECoolView SetXOrigin:m_iXOrigin];
    if (m_pRTEHeatView != NULL)
    {
        CGRect rHeat = m_pRTEHeatView.frame;
        rHeat.size.width = self.frame.size.width;
        [m_pRTEHeatView setFrame:rHeat];
    }

    if (m_pRTECoolView != NULL)
    {
        CGRect rCool = m_pRTECoolView.frame;
        rCool.size.width = self.frame.size.width;
        [m_pRTECoolView setFrame:rCool];
    }

    [self NotifyPosition];
}
/*============================================================================*/

-(void)UpdateTimeBar

/*============================================================================*/
{
    CTimeBar *pTimeBar = [m_pTimeView GetTimeBar];
    NSMutableArray *pLabels = [pTimeBar LabelList];
    for (int iLabel = 0; iLabel < [pLabels count]; iLabel++)
    {
        CTimeBarLabel *pLabel = (CTimeBarLabel*)[pLabels objectAtIndex:iLabel];
        [self UpdateLabel:pLabel];
    }
}
/*============================================================================*/

-(void)UpdateLabel:(CTimeBarLabel*)pLabel

/*============================================================================*/
{
    switch ([pLabel Type])
    {
    case TYPE_DAY:
    case TYPE_MONTH:
        break;

    default:
        {
            [pLabel SetAuxView:NULL];
            return;
        }
        break;
    }

    NSDate *pStart = [pLabel StartDate];
    NSDate *pEnd = [pLabel EndDate];
    if (pStart == NULL || pEnd == NULL)
    {
        [pLabel SetAuxView:NULL];
        return;
    }

    int iHeatPCT = 0;
    int iCoolPCT = 0;

    if ([m_pRTESeries count] >= 2)
    {
        CRTESeries *pHeatRTE = (CRTESeries*)[m_pRTESeries objectAtIndex:1];
        iHeatPCT = [self CalcUsage:pHeatRTE StartDate:pStart EndDate:pEnd];
    }

    if ([m_pRTESeries count] >= 3)
    {
        CRTESeries *pCoolRTE = (CRTESeries*)[m_pRTESeries objectAtIndex:2];
        iCoolPCT = [self CalcUsage:pCoolRTE StartDate:pStart EndDate:pEnd];
    }

    if (iHeatPCT <= 0 && iCoolPCT <= 0)
    {
        [pLabel SetAuxView:NULL];
        return;
    }

    UIView *pView = [[UIView alloc] initWithFrame:CGRectZero];
    [pView setClipsToBounds:YES];
    CGRect rInfo = CGRectMake(5, 0, 25, 12);

    if (iHeatPCT > 0)
    {
        [self AddInfoTextToView:pView Rect:rInfo PCT:iHeatPCT Color:[UIColor colorWithRed:1.0 green:0.5 blue:0.5 alpha:1.0]];
        rInfo.origin.y += rInfo.size.height;
    }
    if (iCoolPCT > 0)
    {
        [self AddInfoTextToView:pView Rect:rInfo PCT:iCoolPCT Color:[UIColor colorWithRed:0.5 green:0.5 blue:1.0 alpha:1.0]];
    }

    [pLabel SetAuxView:pView];
    [pView release];
}
/*============================================================================*/

-(void)AddInfoTextToView:(UIView*)pView Rect:(CGRect)rect PCT:(int)iPCT Color:(UIColor*)pColor

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:rect];
    NSString *pText = [NSString stringWithFormat:@"%d%%", iPCT];
    [pLabel setText:pText];
    [pLabel setTextColor:pColor];
    [CCustomPage InitStandardLabel:pLabel Size:10];
    [pView addSubview:pLabel];
    [pLabel release];
}
/*============================================================================*/

-(int)CalcUsage:(CRTESeries*)pSeries StartDate:(NSDate*)pStartDate EndDate:(NSDate*)pEndDate

/*============================================================================*/
{
    NSMutableArray *pPoints = [pSeries PointList];
    int iSum = 0;
    for (int i = 0; i < [pPoints count]; i++)
    {
        CRunTimeEvent *pRTE = (CRunTimeEvent*)[pPoints objectAtIndex:i];
        iSum += [pRTE OverlapWith:pStartDate End:pEndDate];
    }

    int iTotal = [pEndDate timeIntervalSinceDate:pStartDate];
    return iSum * 100 / iTotal;
}
/*============================================================================*/

-(int)MinTemp       { return m_iTempMin;    }
-(int)MaxTemp       { return m_iTempMax;    }

/*============================================================================*/

-(BOOL)SetScaleMin:(int)iMin Max:(int)iMax

/*============================================================================*/
{
    int iDiffNew = iMax-iMin;
    int iAdder = iDiffNew / 4;
    iAdder = iAdder / 10 * 10;
    iAdder = MAX(iAdder, 10);
    iMin -= iAdder;
    iMax += iAdder;

    if (iMin == m_iTempMin && iMax == m_iTempMax)
        return FALSE;

    int iScaleOld = m_iTempMax - m_iTempMin;
    int iScaleNew = iMax - iMin;
    double dScaleY = (double)iScaleOld / (double)iScaleNew;
    int iCenterF = (iMax + iMin ) / 2;
    int iCenterOld = [self YLogical:iCenterF];

    m_iTempMin = iMin;
    m_iTempMax = iMax;

    int iDYShift = (double)(self.frame.size.height / 2 - iCenterOld) * dScaleY;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    CGAffineTransform transform1 = CGAffineTransformMakeScale(1.0, dScaleY);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, iDYShift);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    self.transform = transform;


    UIView *pScaleView = [m_pTimeView GetScaleView];
    if (pScaleView != NULL)
      pScaleView.transform = transform;

    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];

    [m_pRTEHeatView setHidden:YES];
    [m_pRTECoolView setHidden:YES];

    return TRUE;
}
/*============================================================================*/

-(BOOL)ResizeFrameFrom:(int)iFrom To:(int)iTo

/*============================================================================*/
{
    double dScaleY = (double)iTo / (double)iFrom;
    int iDYShift = (iTo - iFrom) / 2;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    CGAffineTransform transform1 = CGAffineTransformMakeScale(1.0, dScaleY);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, iDYShift);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);

    UIView *pScaleView = [m_pTimeView GetScaleView];
    if (pScaleView != NULL)
      pScaleView.transform = transform;

    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];

    [m_pRTEHeatView setHidden:YES];
    [m_pRTECoolView setHidden:YES];

    return TRUE;
}
/*============================================================================*/

-(void)SetCheckScale

/*============================================================================*/
{
    m_bCheckScale = TRUE;
}
/*============================================================================*/

-(void)SetFrameResizeOld:(int)iDYDataOld New:(int)iDYDataNew

/*============================================================================*/
{
    m_iFrameResizeOld = iDYDataOld;
    m_iFrameResizeNew = iDYDataNew;
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    CGRect rFrame = self.frame;
    if (m_iFrameResizeNew != 0)
        rFrame.size.height = m_iFrameResizeNew;
    self.frame = rFrame;

    CGAffineTransform transform1 = CGAffineTransformMakeScale(1.0, 1.0);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, 0);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    self.transform = transform;
    [self setNeedsDisplay];
    CHVACHistoryView *pHVACView = (CHVACHistoryView*)m_pTimeView;
    [m_pRTEHeatView setHidden:NO];
    [m_pRTECoolView setHidden:NO];
    [pHVACView InitScaleView];
}
@end
