/*
--------------------------------------------------------------------------------

    STRINGLIB.H

    Copyright HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once

class CHLQuery;
class CHLMSG;
class CHLObList;
#ifndef CHLString
    class CHLString;
#endif

//
// Supported code pages.
//
#define CODE_PAGE_THAI          874
#define CODE_PAGE_EUROPE_LATIN  1250
#define CODE_PAGE_CYRILLIC      1251
#define CODE_PAGE_ANSI_LATIN_1  1252
#define CODE_PAGE_GREEK         1253
#define CODE_PAGE_TURKISH       1254
#define CODE_PAGE_HEBREW        1255
#define CODE_PAGE_ARABIC        1256
#define CODE_PAGE_BALTIC        1257
#define CODE_PAGE_VIETNAMESE    1258
#define CODE_PAGE_SIMP_CHINESE  936

#ifdef GATEWAY
    class CServerClient;
#endif


/*============================================================================*/

    class   XStringPair

/*============================================================================*/
{
friend class CStringLib;

private:
    char                        *m_pKeyString;
    char                        *m_pContextString;
    CHLString                   *m_pXString;
#ifdef HLCONFIG
    int                         m_iFlags;
#endif


    public:
    XStringPair(
    char                        *pKeyString,
    CHLString                   *pString);


    XStringPair(
    char                        *pKeyString,
    char                        *pContextString,
    CHLString                   *pString);

    virtual ~XStringPair();

    inline char                 *Key()                  { return m_pKeyString;      }
    inline char                 *Context()              { return m_pContextString;  }
    inline CHLString            *Value()                { return m_pXString;        }

#ifdef HLCONFIG
    inline int                  Flags()                 { return m_iFlags;          }
    inline void                 Flags(int iNew)         { m_iFlags = iNew;          }

    inline void                 Value(CHLString sNew)   { *m_pXString = sNew;       }
#endif
};
/*============================================================================*/

    class CStringLib

/*============================================================================*/
{
protected:
    static CStringLib               *g_pInstance;
    XStringPair**                   m_ppXStringPairs;
    int                             m_nXStringPairs;
    BOOL                            m_bSorted;
    int                             m_iCodePage;            // For TS2


public:
    CStringLib();
    virtual ~CStringLib();

    static HL_API CStringLib        *Instance();
    static void                     InitInstance();
    static void                     ReleaseInstance();

    inline BOOL                     IsEmpty()           { return (m_nXStringPairs < 1);     }
    inline HL_API int               CodePage()          { return m_iCodePage;               }
    inline HL_API void              CodePage(int iNew)  { m_iCodePage = iNew;               }

#ifdef GATEWAY
    BOOL                            Read();
    BOOL                            Write();

    void                            ProcessHLMessage(
    CServerClient                   *pClient,
    short                           shSenderID,
    short                           shMessageID );
#endif

    BOOL                            SupportsAlphaScroll();

    void                            LoadFrom(
    CHLMSG                          *pMSG);

    void                            SaveTo(
    CHLMSG                          *pMSG,
    BOOL                            bIncludeDefaultStrings );

    int                             GetNumXStrings();

    XStringPair                     *GetXStringPair(int iIndex);

    XStringPair                     *FindXStringPair(
    const char                      *pKeyString );

    XStringPair                     *FindXStringPair(
    const char                      *pKeyString,
    const char                      *pContextString );

    HL_API int                      LookupXString(
    const char                      *pKeyString,
    CHLString                       *pXString);

    HL_API int                      LookupXString(
    const char                      *pKeyString,
    const char                      *pContextString,
    CHLString                       *pXString);

    void                            SetXString(
    const char                      *pKeyString,
    CHLString                       *pXString);

    void                            SortByKey();
    void                            SortByContext();

    static int                      Compare(
    const void                      *arg1,
    const void                      *arg2 );

    static int                      CompareKeyStrings(
    const void                      *arg1,
    const void                      *arg2 );

    static int                      CompareContextStrings(
    const void                      *arg1,
    const void                      *arg2 );

    void                            RemoveLangStrings();

protected:
    static void                     FixEscapes(
    CHLString                       *pString );

    static BOOL                     ValidateFormat(
    XStringPair                     *pPair );
};
/*============================================================================*/

    class   CHLXString  : public CHLString

/*============================================================================*/
{
public:

#ifdef IPHONE
    CHLXString( const char* pKeyString, const char *pContextString )
#else
    CHLXString( char* pKeyString, char *pContextString )
#endif
    {
#ifdef STRINGLIB
        CStringLib* pLib = CStringLib::Instance();
        if (!pLib || !pLib->LookupXString(pKeyString, pContextString, this))
#endif
        {
            Empty();

#ifdef MFC_CSTRING
        #ifdef UNICODE
            int iLen = strlen(pKeyString);
            TCHAR *pThisBuf = GetBuffer(iLen);
            mbstowcs(pThisBuf, pKeyString, iLen);
            ReleaseBuffer(iLen);
        #else
            int iLen = strlen(pKeyString);
            TCHAR *pThisBuf = GetBuffer(iLen);
            memcpy(pThisBuf, pKeyString, iLen);
            ReleaseBuffer(iLen);
        #endif
#else
            int iLen = (int)strlen(pKeyString);
            Init(HLSTRING_CHAR, iLen+1, pKeyString);
#endif
        }
    };


    CHLXString( char* pKeyString, char *pContextString, BOOL bMakeUpper )
    {
        CStringLib* pLib = CStringLib::Instance();
        if (!pLib || !pLib->LookupXString(pKeyString, pContextString, this))
        {
            Empty();

#ifdef MFC_CSTRING
        #ifdef UNICODE
            int iLen = strlen(pKeyString);
            TCHAR *pThisBuf = GetBuffer(iLen);
            mbstowcs(pThisBuf, pKeyString, iLen);
            ReleaseBuffer(iLen);
        #else
            int iLen = strlen(pKeyString);
            TCHAR *pThisBuf = GetBuffer(iLen);
            memcpy(pThisBuf, pKeyString, iLen);
            ReleaseBuffer(iLen);
        #endif
#else
            int iLen = (int)strlen(pKeyString);
            Init(HLSTRING_CHAR, iLen+1, pKeyString);
#endif
        }

        if (bMakeUpper)
            MakeUpper();
    };


#if defined(MFC_CSTRING) & !defined(HLCONFIG)
    operator                    char*()
    {
        return GetBuffer(GetLength());
    }
#endif
};
