//
//  MediaZoneBar.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/28/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLToolBar.h"
#import "Sink.h"

@class CMediaZonePage;
@class CAudioZone;
@class CHLButton;
@class CToolBarButton;
@class CVolumeControl;

#define MEDIABAR_DEFAULT                0
#define MEDIABAR_PREVNEXT               1

/*============================================================================*/

@interface CMediaZoneToolBarClientView : CToolBarClientView < CSink, UIAlertViewDelegate>

/*============================================================================*/
{
    CMediaZonePage                      *m_pZonePage;

    CAudioZone                          *m_pZone;
    CToolBarButton                      *m_pSourceButton;
    CToolBarButton                      *m_pOffButton;
    CToolBarButton                      *m_pSettingsButton;
    CToolBarButton                      *m_pMuteButton;
    CToolBarButton                      *m_pNextButton;
    CToolBarButton                      *m_pPrevButton;
    CVolumeControl                      *m_pVolumeCtrl;

    NSTimer                             *m_pPowerButtonTimer;
    int                                 m_iPowerButtonState;
    int                                 m_iConfig;
}

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Page:(CMediaZonePage*)pPage;
-(CHLButton*)SourceButton;
-(CHLButton*)SettingsButton;
-(void)MuteAction;
-(void)NextAction;
-(void)PrevAction;
-(void)SetConfig:(int)iConfig;
-(int)RequiredHeight:(int)iDX;

@end
