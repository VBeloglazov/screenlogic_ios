//
//  PoolMainPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PoolSubSystemPage.h"
#import "NSPoolConfig.h"
#import "hlm.h"
#import "PoolTempPage.h"
#import "hlsegment.h"
#import "crect.h"
#import "PoolHistoryView.h"
#import "PoolSpaPage.h"
#import "PoolMainPage.h"
#import "SubSystemPage.h"

#define DY_TABBAR                   40

@implementation CPoolSubSystemPage
/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig SubTab:(CSubTab*)pSub

/*============================================================================*/
{
    self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:pSub];
    if (self)
    {
        m_pConfig = pConfig;
        CGRect rThis = self.bounds;
        m_iPages[0] = PAGE_MAIN;

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            //
            // iPod/iPhone
            //
            m_iPages[0] = PAGE_POOL_SPA;
            NSMutableArray *pItems = [[NSMutableArray alloc] init];
            NSMutableArray *pPoolCircuits = [[[NSMutableArray alloc] init] autorelease];
            NSMutableArray *pSpaCircuits = [[[NSMutableArray alloc] init] autorelease];
            NSMutableArray *pGenCircuits = [[[NSMutableArray alloc] init] autorelease];
            [m_pConfig LoadCircuitsByInterface:pPoolCircuits Interface:POOLINT_POOL];
            [m_pConfig LoadCircuitsByInterface:pSpaCircuits Interface:POOLINT_SPA];
            [m_pConfig LoadCircuitsByInterface:pGenCircuits Interface:POOLINT_GENERAL];
            
            int iNTabs = 0;
            if ([pPoolCircuits count] > 0)
            {
                [pItems addObject:[m_pConfig PoolText]];
                m_iPages[iNTabs] = PAGE_POOL;
                iNTabs++;
            }
            if ([pSpaCircuits count] > 0)
            {
                [pItems addObject:[m_pConfig SpaText]];
                m_iPages[iNTabs] = PAGE_SPA;
                iNTabs++;
            }
            if ([pGenCircuits count] > 0)
            {
                [pItems addObject:@"Circuits"];
                m_iPages[iNTabs] = PAGE_CIRCUITS;
                iNTabs++;
            }

            [pItems addObject:@"History"];
            m_iPages[iNTabs] = PAGE_HISTORY;
            iNTabs++;
        
            CGRect rPageCtrl = [CRect BreakOffBottom:&rThis DY:[CMainView DY_TOOLBAR]];

            m_pPageCtrl = [[CHLSegment alloc] initWithFrame:rPageCtrl ImageProvider:NULL Items:pItems Type:HLSEGMENT_PAGE_CTRL Data:NO];
            [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
            [m_pPageCtrl SetSelectedIndex:0];
            [self addSubview:m_pPageCtrl];
            [m_pPageCtrl release];
            [pItems release];
        }

        m_pMainView = [[UIView alloc] initWithFrame:rThis];
        [m_pMainView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:m_pMainView];
        
        [self SelectPage:0];
        m_bInit = TRUE;

    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pPage0 release];
    [m_pPage1 release];
    [m_pPage2 release];
    [m_pPage3 release];
    [m_pPage4 release];
    [m_pPage5 release];
    [m_pMainView release];
    [m_pTabBar release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [super SetVisible:bVisible];
    if (bVisible)
        [self LockHorizontalScroll:NO];
}
/*============================================================================*/

-(CHLSegment*)GetPageCtrl

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        return NULL;

    if (m_pPageCtrl != NULL)
        return m_pPageCtrl;
    
    NSMutableArray *pGenCircuits = [[[NSMutableArray alloc] init] autorelease];
    [m_pConfig LoadCircuitsByInterface:pGenCircuits Interface:POOLINT_GENERAL];

    NSMutableArray *pItems = [[NSMutableArray alloc] init];

    int iNPages = 0;
    m_iPages[iNPages] = PAGE_MAIN; iNPages++;


    CHLSegmentIconView *p1 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"Pool_Spa_Main.png" Text:@"MAIN"];
    [pItems addObject:p1];

/*
    CHLSegmentIconView *p2 = NULL;
    if ([pGenCircuits count] > 0)
    {
        p2 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"SETTINGS.png" Text:@"Circuits"];
        m_iPages[iNPages] = PAGE_CIRCUITS; iNPages++;
        [pItems addObject:p2];
    }
*/

    CHLSegmentIconView *p3 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"HISTORY.png" Text:@"HISTORY"];
    m_iPages[iNPages] = PAGE_HISTORY; iNPages++;
    [pItems addObject:p3];


    [p1 release];
//    [p2 release];
    [p3 release];

    m_pPageCtrl = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Views:pItems  Type:HLSEGMENT_TOOLBAR];
    [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
    [pItems release];
    return m_pPageCtrl;
}
/*============================================================================*/

-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    [m_pPage4 NotifyRotationComplete];
}
/*============================================================================*/

-(void)SelectPage:(id)sender

/*============================================================================*/
{
    UIView *pNewView = NULL;
    int iIndex = 0;
    if (sender != NULL)
    {
        CHLSegment *pCtrl = (CHLSegment*)sender;
        iIndex = [pCtrl SelectedIndex];
    }

    CGRect rPage = m_pMainView.bounds;
    int iType = m_iPages[iIndex];

    switch (iType)
    {
    case PAGE_POOL_SPA: 
        {
            if (m_pPage0 == NULL)
                m_pPage0 = [[CPoolSpaPage alloc] initWithFrame:rPage Config:m_pConfig];
            pNewView = m_pPage0;
        }
        break;

    case PAGE_POOL: 
        {
            if (m_pPage1 == NULL)
                m_pPage1 = [[CPoolTempPage alloc] initWithFrame:rPage Type:PAGE_POOL Config:m_pConfig];
            pNewView = m_pPage1;
        }
        break;
    case PAGE_SPA: 
        {
            if (m_pPage2 == NULL)
                m_pPage2 = [[CPoolTempPage alloc] initWithFrame:rPage Type:PAGE_SPA Config:m_pConfig];
            pNewView = m_pPage2;
        }
        break;
    case PAGE_CIRCUITS: 
        {
            if (m_pPage3 == NULL)
                m_pPage3 = [[CPoolTempPage alloc] initWithFrame:rPage Type:PAGE_CIRCUITS Config:m_pConfig];
            pNewView = m_pPage3;
        }
        break;
    case PAGE_HISTORY:
        {
            if (m_pPage4 == NULL)
                m_pPage4 = [[CPoolHistoryView alloc] initWithFrame:rPage Config:m_pConfig];
            pNewView = m_pPage4;
        }
        break;
    case PAGE_MAIN: 
        {
            if (m_pPage5 == NULL)
            {
                CPoolMainPage *pPoolMain = [[CPoolMainPage alloc] initWithFrame:CGRectZero Config:m_pConfig];
                m_pPage5 = [[CSubSystemContainerPage alloc] initWithFrame:rPage ClientView:pPoolMain];
                [pPoolMain release];
            }
            pNewView = m_pPage5;
        }
        break;
            
    }

    if (m_bInit)
    {
        [pNewView setFrame:rPage];
        [m_pMainView addSubview:pNewView];
        [self TranslateNewPage:pNewView OldPage:m_pPage ThisIndex:iIndex LastIndex:m_iLastIndex];
        m_pPage = pNewView;
        m_iLastIndex = iIndex;
        [m_pConfig NotifySinks];
        return;
    }

    [m_pPage removeFromSuperview];
    m_pPage = pNewView;
    [m_pMainView addSubview:pNewView];

    [m_pConfig NotifySinks];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [m_pMainView setFrame:rBounds];
    }
    else 
    {
        int iDYTabs = [CMainView DY_TOOLBAR];
        if (rBounds.size.width > rBounds.size.height)
        {
            // Landscape Mode
            CGRect rTabBar = CGRectMake(0, rBounds.size.height, rBounds.size.width, iDYTabs);
            [m_pMainView setFrame:rBounds];
            [m_pPageCtrl setFrame:rTabBar];
        }
        else
        {
            // Portrait Mode
            CGRect rTabBar = [CRect BreakOffBottom:&rBounds DY:iDYTabs];
            [m_pMainView setFrame:rBounds];
            [m_pPageCtrl setFrame:rTabBar];
        }
    }

    [m_pPage setFrame:m_pMainView.bounds];
}
@end
