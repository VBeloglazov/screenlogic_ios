//
//  MediaZoneBar.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/28/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "MediaZoneBar.h"
#import "hlbutton.h"
#import "MediaZonePage.h"
#import "VolumeControl.h"
#import "crect.h"
#import "AudioZone.h"
#import "hlm.h"
#import "shader.h"


@implementation CMediaZoneToolBarClientView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Page:(CMediaZonePage*)pPage

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_pZone = pZone;
        m_pZonePage = pPage;
        [m_pZone AddSink:self];
        m_pOffButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:@"OFF" Icon:@"POWER.png"];
        [m_pOffButton SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
        [self addSubview:m_pOffButton];
        [m_pOffButton addTarget:self action:@selector(ZoneOffActionPress) forControlEvents:UIControlEventTouchDown];
        [m_pOffButton addTarget:self action:@selector(ZoneOffActionRelease) forControlEvents:UIControlEventTouchUpOutside];
        [m_pOffButton addTarget:self action:@selector(ZoneOffActionRelease) forControlEvents:UIControlEventTouchUpInside];

        m_pSourceButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:@"SOURCE" Icon:@"SOURCE.png"];
        [m_pSourceButton SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
        [self addSubview:m_pSourceButton];
        [m_pSourceButton addTarget:pPage action:@selector(SelectSource) forControlEvents:UIControlEventTouchDown];

        m_pVolumeCtrl = [[CVolumeControl alloc] initWithFrame:CGRectZero Zone:pZone];
        [self addSubview:m_pVolumeCtrl];

        if ([pZone HasMute])
        {
            m_pMuteButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:@"MUTE" Icon:@"VOL_MUTE.png"];
            [self addSubview:m_pMuteButton];
            [m_pMuteButton release];
            [m_pMuteButton addTarget:self action:@selector(MuteAction) forControlEvents:UIControlEventTouchDown];
            [m_pMuteButton SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
        }
        if ([pZone HasSettings])
        {
            m_pSettingsButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:@"SETTINGS" Icon:@"SETTINGS.png"];
            [self addSubview:m_pSettingsButton];
            [m_pSettingsButton release];
            [m_pSettingsButton addTarget:pPage action:@selector(ShowSettings) forControlEvents:UIControlEventTouchDown];
            [m_pSettingsButton SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
        }

        [m_pOffButton release];
        [m_pSourceButton release];
        [m_pVolumeCtrl release];
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

/*============================================================================*/
{
    if (buttonIndex == 1)
    {
        [m_pZone SetActiveSourceIndex:0];
    }
}
/*============================================================================*/

-(void)layoutSubviews    

/*============================================================================*/
{
    CGRect rBar = self.bounds;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        //
        // iPhone/touch
        //
        if (m_iConfig == MEDIABAR_PREVNEXT && rBar.size.width <= 320)
        {
            //
            // 2-Line Mode
            //
            int iDXBtn = rBar.size.width/4;
            CGRect rBar0 = [CRect BreakOffTop:&rBar DY:rBar.size.height / 2];
            CGRect rBar1 = rBar;
            CGRect rOff  = [CRect BreakOffLeft:&rBar0 DX:iDXBtn];
            CGRect rSrc  = [CRect BreakOffLeft:&rBar0 DX:iDXBtn];
            CGRect rPrev = [CRect BreakOffLeft:&rBar0 DX:iDXBtn];
            CGRect rNext = rBar0;

            CGRect rMute = CGRectZero;
            if (m_pMuteButton != NULL)
                rMute = [CRect BreakOffRight:&rBar1 DX:iDXBtn];
            CGRect rSet = CGRectZero;
            if (m_pSourceButton != NULL)
                rSet = [CRect BreakOffLeft:&rBar DX:iDXBtn];

            [m_pOffButton setFrame:rOff];
            [m_pSourceButton setFrame:rSrc];
            [m_pPrevButton setFrame:rPrev];
            [m_pNextButton setFrame:rNext];
            [m_pSettingsButton setFrame:rSet];
            [m_pMuteButton setFrame:rMute];
            [m_pVolumeCtrl setFrame:rBar1];
            return;
        }

        int iNBtns = 4;

        BOOL bShowSettings = TRUE;
        if (rBar.size.width < 400)
        {
            if (m_pMuteButton != NULL)
            {
                bShowSettings = FALSE;
                [m_pSettingsButton setAlpha:0];
            }
        }

        if (m_pSettingsButton != NULL && bShowSettings) iNBtns++;
        if (m_pMuteButton != NULL)                      iNBtns++;
        if (m_pPrevButton != NULL)                      iNBtns++;
        if (m_pNextButton != NULL)                      iNBtns++;
        
        int iDXButton = rBar.size.width / iNBtns;

        CGRect rOff     = [CRect BreakOffLeft:&rBar DX:iDXButton];
        CGRect rSources = [CRect BreakOffLeft:&rBar DX:iDXButton];
        CGRect rPrev    = CGRectZero;
        CGRect rNext    = CGRectZero;
        if (m_pPrevButton != NULL)
            rPrev = [CRect BreakOffLeft:&rBar DX:iDXButton];
        if (m_pNextButton != NULL)
            rNext = [CRect BreakOffLeft:&rBar DX:iDXButton];

        if (m_pSettingsButton != NULL && bShowSettings)
        {
            CGRect rSet = [CRect BreakOffLeft:&rBar DX:iDXButton];
            [m_pSettingsButton setFrame:rSet];
            [m_pSettingsButton setAlpha:1.0];
        }
    
        if (m_pMuteButton != NULL)
        {
            CGRect rMute = [CRect BreakOffRight:&rBar DX:iDXButton];
            [m_pMuteButton setFrame:rMute];
        }

        [m_pVolumeCtrl setFrame:rBar];
        [m_pSourceButton setFrame:rSources];
        [m_pOffButton setFrame:rOff];
        [m_pPrevButton setFrame:rPrev];
        [m_pNextButton setFrame:rNext];
    }
    else
    {
        //
        // iPad
        //

        int iDXButton = rBar.size.width / 8;
        int iNBtns = 4;
        if (m_pSettingsButton != NULL)                  iNBtns++;
        if (m_pMuteButton != NULL)                      iNBtns++;
        if (m_pPrevButton != NULL)                      iNBtns++;
        if (m_pNextButton != NULL)                      iNBtns++;

        if (iNBtns == 7)
            iDXButton = rBar.size.width / 7;
        

        CGRect rMute = CGRectZero;
        if (m_pMuteButton != NULL)
        {
            rMute = [CRect BreakOffRight:&rBar DX:iDXButton];
            [CRect BreakOffRight:&rBar DX:2];
            [m_pMuteButton setFrame:rMute];
        }

        CGRect rOff     = [CRect BreakOffLeft:&rBar DX:iDXButton];
        [CRect BreakOffLeft:&rBar DX:2];
        CGRect rSources = [CRect BreakOffLeft:&rBar DX:iDXButton];
        [CRect BreakOffLeft:&rBar DX:2];


        if (m_pPrevButton != NULL)
        {
            CGRect rPrev = [CRect BreakOffLeft:&rBar DX:iDXButton];
            [CRect BreakOffLeft:&rBar DX:2];
            [m_pPrevButton setFrame:rPrev];
        }
        
        if (m_pNextButton != NULL)
        {
            CGRect rNext = [CRect BreakOffLeft:&rBar DX:iDXButton];
            [CRect BreakOffLeft:&rBar DX:2];
            [m_pNextButton setFrame:rNext];
        }

        if (m_pSettingsButton != NULL)
        {
            CGRect rSet = [CRect BreakOffLeft:&rBar DX:iDXButton];
            [CRect BreakOffLeft:&rBar DX:2];
            [m_pSettingsButton setFrame:rSet];
        }

        CGRect rVolume = CGRectZero;
        if (rBar.size.width >= 2 * iDXButton)
            rVolume =  [CRect BreakOffRight:&rBar DX:2*iDXButton];
        else
            rVolume = rBar;

        [CRect BreakOffRight:&rBar DX:2];

        [m_pVolumeCtrl setFrame:rVolume];
        [m_pSourceButton setFrame:rSources];
        [m_pOffButton setFrame:rOff];
    }
    [m_pOffButton FrameStyle:TOOLBAR_FRAME_FULL];
    [m_pSourceButton FrameStyle:TOOLBAR_FRAME_FULL];
    [m_pPrevButton FrameStyle:TOOLBAR_FRAME_FULL];
    [m_pNextButton FrameStyle:TOOLBAR_FRAME_FULL];
    [m_pMuteButton FrameStyle:TOOLBAR_FRAME_FULL];
    [m_pSettingsButton FrameStyle:TOOLBAR_FRAME_FULL];
    [m_pVolumeCtrl FrameStyle:TOOLBAR_FRAME_FULL];

}
/*============================================================================*/

-(void)MuteAction

/*============================================================================*/
{
    BOOL bMute = ![m_pMuteButton IsChecked];
    [m_pZone SetMute:bMute];
}
/*============================================================================*/

-(void)NextAction

/*============================================================================*/
{
    [m_pZone ZoneCommand:HL_PLAYLIST_NEXT Data1:0 Data2:0];
}
/*============================================================================*/

-(void)PrevAction

/*============================================================================*/
{
    [m_pZone ZoneCommand:HL_PLAYLIST_PREVIOUS Data1:0 Data2:0];
}
/*============================================================================*/

-(void)ZoneOffActionPress

/*============================================================================*/
{
    [m_pPowerButtonTimer invalidate];
    [m_pPowerButtonTimer release];
    
    m_pPowerButtonTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(BeginAllOffWarning) userInfo:nil repeats:NO] retain];
    m_iPowerButtonState = POWER_BUTTON_STATE_ZONEOFF;
}
/*============================================================================*/

-(void)ZoneOffActionRelease

/*============================================================================*/
{
    [m_pPowerButtonTimer invalidate];
    [m_pPowerButtonTimer release];
    m_pPowerButtonTimer = NULL;
    
    switch (m_iPowerButtonState)
    {
    case POWER_BUTTON_STATE_NONE:
        return;

    case POWER_BUTTON_STATE_CANCELWARNING:
        {
            m_iPowerButtonState = POWER_BUTTON_STATE_NONE;
            [m_pZonePage CancelAllOffWarning];
            return;
        }
        break;
    
    default:
        break;
    }

    m_iPowerButtonState = POWER_BUTTON_STATE_NONE;

    UIAlertView *pAlert = [[UIAlertView alloc] initWithTitle:@"Power Zone" message:@"Turn Off This Zone?"
                            delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Power Off", nil];

    [pAlert show];	
    [pAlert release];
}
/*============================================================================*/

-(void)BeginAllOffWarning

/*============================================================================*/
{
    m_iPowerButtonState = POWER_BUTTON_STATE_CANCELWARNING;
    [m_pZonePage BeginAllOffWarning];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_VOLCHANGED)
    {
        [m_pMuteButton SetChecked:[m_pZone GetMute]];
    }
}
/*============================================================================*/

-(CHLButton*)SourceButton

/*============================================================================*/
{
    return m_pSourceButton;
}
/*============================================================================*/

-(CHLButton*)SettingsButton

/*============================================================================*/
{
    return m_pSettingsButton;
}
/*============================================================================*/

-(UIView*)InsetLeft     

/*============================================================================*/
{ 
    if (m_pSettingsButton != NULL) 
        return m_pSettingsButton; 
    if (m_pNextButton != NULL)
        return m_pNextButton;
    return m_pSourceButton;   }

/*============================================================================*/

-(UIView*)InsetRight    

/*============================================================================*/
{
    return m_pVolumeCtrl;
}
/*============================================================================*/

-(void)SetConfig:(int)iConfig

/*============================================================================*/
{
    m_iConfig = iConfig;
    switch (iConfig)
    {
    case MEDIABAR_DEFAULT:
        {
            [m_pNextButton removeFromSuperview];
            [m_pPrevButton removeFromSuperview];
            m_pNextButton = NULL;
            m_pPrevButton = NULL;
            
        }
        break;

    case MEDIABAR_PREVNEXT:
        {
            if (m_pNextButton == NULL)
            {
                m_pNextButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:@"NEXT" Icon:NULL];
                [self addSubview:m_pNextButton];
                [m_pNextButton release];
                [m_pNextButton addTarget:self action:@selector(NextAction) forControlEvents:UIControlEventTouchDown];
                [m_pNextButton SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
            }

            if (m_pPrevButton == NULL)
            {
                m_pPrevButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:@"PREV" Icon:NULL];
                [self addSubview:m_pPrevButton];
                [m_pPrevButton release];
                [m_pPrevButton addTarget:self action:@selector(PrevAction) forControlEvents:UIControlEventTouchDown];
                [m_pPrevButton SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
            }

        }   
        break;
    }
}
/*============================================================================*/

-(int)RequiredHeight:(int)iDX

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return [CMainView DY_MEDIA_CONTROLS];
    }

    if (iDX <= 320)
    {
        if (m_iConfig == MEDIABAR_PREVNEXT)
        {
            return 2*[CMainView DY_MEDIA_CONTROLS];
        }
    }

    return [CMainView DY_MEDIA_CONTROLS];
}
@end
