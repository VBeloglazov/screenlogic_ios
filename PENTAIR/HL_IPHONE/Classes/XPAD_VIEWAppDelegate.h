//
//  XPAD_VIEWAppDelegate.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XPAD_VIEWViewController;
@class CHLComm;

/*============================================================================*/

@interface XPAD_VIEWAppDelegate : NSObject <UIApplicationDelegate> 

/*============================================================================*/
{
    CHLComm                             *m_pComm;
	IBOutlet UIWindow                   *window;
	IBOutlet UINavigationController     *navigationController;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;

@end

