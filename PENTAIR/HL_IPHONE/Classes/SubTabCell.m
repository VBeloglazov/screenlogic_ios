//
//  SubTabCell.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#define INDENT                  44
#define DY_ICON_MAX             32
#define ACCESSORY_SIZE          16
#define BORDER                  4

#import "SubTabCell.h"
#import "SubTab.h"
#import "MainTab.h"
#import "iconview.h"
#import "MainView.h"
#import "crect.h"
#import "CustomPage.h"

@implementation CSubTabCell

@synthesize m_pLabel;

@synthesize m_iOptions;

/*============================================================================*/

- (id)initWithFrame:(CGRect)aRect reuseIdentifier:(NSString *)identifier

/*============================================================================*/
{
	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

	if (self)
	{
		// you can do this here specifically or at the table level for all cells
		self.accessoryType = UITableViewCellAccessoryNone;
        
		// Create label views to contain the various pieces of text that make up the cell.
		// Add these as subviews.
		m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
        int iTextSize = 18;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            iTextSize = 40;
        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];

        [m_pLabel setTextAlignment:UITextAlignmentLeft];
        [m_pLabel setLineBreakMode:UILineBreakModeWordWrap];
        [m_pLabel setNumberOfLines:2];

		m_pAuxLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
        [CCustomPage InitStandardLabel:m_pAuxLabel Size:18];
        [m_pAuxLabel setTextAlignment:UITextAlignmentRight];

		[self.contentView addSubview:m_pLabel];
		[self.contentView addSubview:m_pAuxLabel];
        self.contentView.backgroundColor = [UIColor clearColor];
		self.contentView.opaque = NO;
    
        CGRect frameIcon = CGRectMake(0, 0, 32, 32);
		m_pAccesory = [[CAccesoryView alloc] initWithFrame:frameIcon];	// layoutSubViews will decide the final frame
		m_pAccesory.backgroundColor = [UIColor clearColor];
        
        m_pDelegate = NULL;

		[self.contentView addSubview:m_pAccesory];
    }
	
	return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    //DL - don't touch it is sub button view
    //contentRect.origin.y += 20;
    
    int iDY = contentRect.size.height;
	
	// In this example we will never be editing, but this illustrates the appropriate pattern
    int iDYIcon = MIN(iDY, DY_ICON_MAX);
    int iDXIcon = 2 * iDYIcon;

    [m_pUserIconView setFrame:[CRect BreakOffLeft:&contentRect DX:iDXIcon]];
    [CRect BreakOffLeft:&contentRect DX:10];
    [m_pAccesory setFrame:[CRect BreakOffRight:&contentRect DX:ACCESSORY_SIZE]];
    
    if (m_pAuxLabel != NULL)
        m_pAuxLabel.frame = [CRect BreakOffRight:&contentRect DX:40];

	m_pLabel.frame = contentRect;
    [self.contentView setBackgroundColor:[UIColor blackColor]];
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pUserIconView release];
	[m_pLabel release];
    [m_pSubTab release];
    [m_pAccesory release];

    if (m_pSubTab != NULL)
    {
        [m_pSubTab SetSubTabCell:NULL];
    }

    [super dealloc];
}
/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated

/*============================================================================*/
{
    if (m_pDelegate != NULL && !selected)
    {
        if (![m_pDelegate CanDeselect:self])
            selected = TRUE;
    }


	[super setSelected:selected animated:animated];

	// when the selected state changes, set the highlighted state of the lables accordingly
	m_pLabel.highlighted = selected;
    
    if (m_iOptions & SUBTABCELL_ACCESSORYONLYWHENSELELECTED)
    {
        m_pAccesory.hidden = !selected;
    }
    if (m_iOptions & SUBTABCELL_ACCESSORYNEVER)
    {
        m_pAccesory.hidden = YES;
    }
}
/*============================================================================*/

-(void)SetDelegate:(id)pDelegate

/*============================================================================*/
{
    m_pDelegate = pDelegate;
}
/*============================================================================*/

-(void)SetSubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    if (m_pSubTab == pSubTab)
        return;
        
    if (m_pSubTab != NULL)
        [m_pSubTab SetSubTabCell:NULL];
        
    m_pSubTab = pSubTab;
}
/*============================================================================*/

-(void)SetUserIcon:(NSString*)pIconName;

/*============================================================================*/
{
    if (m_pUserIconView != NULL)
    {
        [m_pUserIconView removeFromSuperview];
        [m_pUserIconView release];
        m_pUserIconView = NULL;
    }
    
    if (pIconName == NULL)
        return;
    
    CGRect frameIcon = CGRectMake(0, 0, 32, 32);
	m_pUserIconView = [[CUserTabIconView alloc] initWithFrame:frameIcon];
    m_pUserIconView.backgroundColor = [UIColor clearColor];
    [m_pUserIconView SetUserIcon:pIconName];
    [self.contentView addSubview:m_pUserIconView];
    [self setNeedsLayout];
}
/*============================================================================*/

-(void)SetEmbeddedIcon:(NSString*)pIconName

/*============================================================================*/
{
    if (m_pUserIconView != NULL)
    {
        [m_pUserIconView removeFromSuperview];
        [m_pUserIconView release];
        m_pUserIconView = NULL;
    }
    
    if (pIconName == NULL)
        return;
    
    CGRect frameIcon = CGRectMake(0, 0, 32, 32);
	m_pUserIconView = [[CUserTabIconView alloc] initWithFrame:frameIcon];
    m_pUserIconView.backgroundColor = [UIColor clearColor];
    [m_pUserIconView SetEmbeddedIcon:pIconName];
    [self.contentView addSubview:m_pUserIconView];
    [self setNeedsLayout];
}
/*============================================================================*/

-(void)SetAuxText:(NSString*)pAuxText

/*============================================================================*/
{
    if (pAuxText == NULL)
    {
        [m_pAuxLabel setHidden:TRUE];
        return;
    }

    if ([pAuxText length] < 1)
    {
        [m_pAuxLabel setHidden:TRUE];
        return;
    }

    [m_pAuxLabel setText:pAuxText];
    [m_pAuxLabel setHidden:FALSE];
}
@end

