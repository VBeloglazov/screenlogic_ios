/*
--------------------------------------------------------------------------------

    SHADEPOLY.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "math.h"

#include "helpers.h"
#include "dibdc.h"
#include "speed.h"
#include "shadepoly.h"

    //
    // Borders, etc...
    //

/*============================================================================*/

    CEdgeList::CEdgeList(
    EDGE_TYPE                   Type,
    int                         iBufferSize )

/*============================================================================*/
{
    m_Type      = Type;
    m_piX       = NULL;
    m_piY       = NULL;
    m_piD       = NULL;
    m_iNPts     = 0;
    m_iNAlloc   = 0;

    if (iBufferSize > 0)
    {   
        m_iNAlloc = iBufferSize;
        m_piX = new int[m_iNAlloc];
        m_piY = new int[m_iNAlloc];
        m_piD = new int[m_iNAlloc];
    }
}
/*============================================================================*/

    CEdgeList::CEdgeList(
    CEdgeList                   *pCopyFrom )

/*============================================================================*/
{
    m_Type      = pCopyFrom->Type();
    m_iNPts     = pCopyFrom->NPoints();
    m_iNAlloc   = m_iNPts;
    m_piX       = new int[m_iNPts];
    m_piY       = new int[m_iNPts];
    m_piD       = new int[m_iNPts];
    memcpy(m_piX, pCopyFrom->m_piX, sizeof(int) * m_iNPts);
    memcpy(m_piY, pCopyFrom->m_piY, sizeof(int) * m_iNPts);
    memcpy(m_piD, pCopyFrom->m_piD, sizeof(int) * m_iNPts);
}
/*============================================================================*/

    CEdgeList::~CEdgeList()

/*============================================================================*/
{
    delete [] m_piX;
    delete [] m_piY;
    delete [] m_piD;
}
/*============================================================================*/

    CHLRect                     CEdgeList::GetBounds()

/*============================================================================*/
{
    if (m_iNPts < 1)
        return CDXYRect(0, 0, 0, 0);
    int iX0 = m_piX[0];
    int iX1 = iX0 + m_piD[0]-1;
    int iY0 = m_piY[0];
    int iY1 = iY0;
    for (int i = 1; i < m_iNPts; i++)
    {
        iX0 = __min(iX0, m_piX[i]);
        iX1 = __max(iX1, m_piX[i]+m_piD[i]-1);
        iY0 = __min(iY0, m_piY[i]);
        iY1 = __max(iY1, m_piY[i]);
    }

    return CX12Rect(iX0, iY0, iX1, iY1);
}
/*============================================================================*/

    void                        CEdgeList::AddEdge(
    int                         iX,
    int                         iY,
    int                         iD )

/*============================================================================*/
{
    if (iD <= 0)
        return;

    if (m_iNPts + 1 > m_iNAlloc)
    {
        int *piOldX = m_piX;
        int *piOldY = m_piY;
        int *piOldD = m_piD;
        m_iNAlloc += 128;
        m_piX = new int[m_iNAlloc];
        m_piY = new int[m_iNAlloc];
        m_piD = new int[m_iNAlloc];

        if (piOldX != NULL && piOldY != NULL && piOldD != NULL)
        {
            memcpy(m_piX, piOldX, sizeof(int) * m_iNPts);
            memcpy(m_piY, piOldY, sizeof(int) * m_iNPts);
            memcpy(m_piD, piOldD, sizeof(int) * m_iNPts);
            delete [] piOldX;
            delete [] piOldY;
            delete [] piOldD;
        }
    }
    
    m_piX[m_iNPts] = iX;
    m_piY[m_iNPts] = iY;
    m_piD[m_iNPts] = iD;
    m_iNPts++;
}
/*============================================================================*/

    void                        CEdgeList::CreateForCorner(
    int                         iCorner,
    int                         iRad,
    int                         iBoxDim,
    int                         iRadiusType,
    BOOL                        bFill )

/*============================================================================*/
{
    CPointList List;

    int iXR = iBoxDim-1;
    int iYR = iBoxDim-1;
    int iYFill = 0;

    // Remember, a iRad of 1 will go from Y=0 to Y=1 so a DY of 2
    int iDYFill = iBoxDim - iRad - 1;

    switch (iCorner)
    {
    case CORNER_LL_INSIDE:
    case CORNER_LR_INSIDE:
        {
            iYFill = iBoxDim - iDYFill;  
        }
        break;

    case CORNER_UR_INSIDE:
    case CORNER_UL_INSIDE:
        break;
    }

    switch (iCorner)
    {
    case CORNER_LL_OUTSIDE:
    case CORNER_UL_OUTSIDE:
    case CORNER_UR_OUTSIDE:
    case CORNER_LR_OUTSIDE:
        {
            // Calc Arc Points duplicate X's to the right
            CHLDibDC::CalcArcPoints(&List, iRad, TRUE, iRadiusType);
        }
        break;    

    case CORNER_LL_INSIDE:
    case CORNER_UL_INSIDE:
    case CORNER_UR_INSIDE:
    case CORNER_LR_INSIDE:
        {
            // Calc Arc Points duplicate X's to the LEFT
            CHLDibDC::CalcArcPoints(&List, iRad, FALSE, iRadiusType);

            int iNArc = List.NPoints();
            for (int i = iNArc-2; i >= 0; i--)
            {
                if (List.Y(i) == List.Y(i+1))
                {
                    List.RemoveAtIndex(i+1);
                    iNArc--;
                }
            }
        }
        break;    

    }

    int i = 0;
    if (bFill)
    {
        for (i = 0; i < List.NPoints(); i++)
        {
            int iX = List.X(i);
            int iY = List.Y(i);

            LOGASSERT(iY <= iRad);

            switch (iCorner)
            {
            case CORNER_LL_OUTSIDE: {   AddEdge(iXR-iX, iY,     iX+1);  }   break;
            case CORNER_UL_OUTSIDE: {   AddEdge(iXR-iX, iYR-iY, iX+1);  }   break;
            case CORNER_UR_OUTSIDE: {   AddEdge(0,      iYR-iY, iX+1);  }   break;
            case CORNER_LR_OUTSIDE: {   AddEdge(0,      iY,     iX+1);  }   break;

            case CORNER_LL_INSIDE:  {   AddEdge(0,      iY,     iBoxDim-iX);    }   break;
            case CORNER_UL_INSIDE:  {   AddEdge(0,      iYR-iY, iBoxDim-iX);    }   break;
            case CORNER_UR_INSIDE:  {   AddEdge(iX,     iYR-iY, iBoxDim-iX);    }   break;
            case CORNER_LR_INSIDE:  {   AddEdge(iX,     iY,     iBoxDim-iX);    }   break;
            }
        }
    }
    else
    {
        int iXLast = 0;
        for (i = 0; i < List.NPoints(); i++)
        {
            int iX = List.X(i);
            int iY = List.Y(i);

            LOGASSERT(iY <= iRad);

            int iD = __max(iX - iXLast, 1);
            switch (iCorner)
            {
            case CORNER_LL:  {   AddEdge(iXR-iX,      iY,     iD);    }   break;
            case CORNER_UL:  {   AddEdge(iXR-iX,      iYR-iY, iD);    }   break;
            case CORNER_UR:  {   AddEdge(iX,      iYR-iY, iD);    }   break;
            case CORNER_LR:  {   AddEdge(iX,      iY,     iD);    }   break;
            }

            iXLast = iX;
        }
    }

    for (i = 0; i < iDYFill; i++)
    {
        AddEdge(0, iYFill, iBoxDim);
        iYFill++;
    }
}
/*============================================================================*/

    void                        CEdgeList::CreateForCornerInt(
    int                         iCorner,
    int                         iRad,
    int                         iDX,
    int                         iDY,
    int                         iRadiusType )

/*============================================================================*/
{
    CPointList List;

    int iYFill = 0;

    // Remember, a iRad of 1 will go from Y=0 to Y=1 so a DY of 2
    int iDYFill = iDY- iRad - 1;

    switch (iCorner)
    {
    case CORNER_LL_INSIDE:
    case CORNER_LR_INSIDE:
        break;

    case CORNER_UR_INSIDE:
    case CORNER_UL_INSIDE:
        iYFill = iDY - iDYFill;  
        break;
    }

    // Calc Arc Points duplicate X's to the right
    CHLDibDC::CalcArcPoints(&List, iRad, TRUE, iRadiusType);

    int i = 0;
    for (i = 0; i < List.NPoints(); i++)
    {
        int iX = List.X(i);
        int iY = List.Y(i);

        int iXInset = iRad-iX;

        switch (iCorner)
        {
        case CORNER_LL_INSIDE:  {   AddEdge(iXInset,    iDY-iRad+iY-1,  iDX-iXInset);    }   break;
        case CORNER_UL_INSIDE:  {   AddEdge(iXInset,    iRad-iY,        iDX-iXInset);    }   break;
        case CORNER_UR_INSIDE:  {   AddEdge(0,          iRad-iY,        iDX-iXInset);    }   break;
        case CORNER_LR_INSIDE:  {   AddEdge(0,          iDY-iRad+iY-1,  iDX-iXInset);    }   break;
        }
    }

    for (i = 0; i < iDYFill; i++)
    {
        AddEdge(0, iYFill, iDX);
        iYFill++;
    }
}
/*============================================================================*/

    void                        CEdgeList::CreateForRoundRectExt(
    int                         iDX,
    int                         iDY,
    int                         iInset,
    int                         iRad,
    int                         iDirection )

/*============================================================================*/
{
    LOGASSERT(m_Type == EDGE_HORIZONTAL);

    switch (iDirection)
    {
    case DIR_LEFT:
    case DIR_RIGHT:
        iRad = __min(iRad, (iDY - 2 * iInset) / 2 - 1);
        break;

    case DIR_UP:
    case DIR_DOWN:
        iRad = __min(iRad, (iDX - 2 * iInset) / 2 - 1);
        break;

    default:
        iRad = __min(iRad, (__min(iDX, iDY) - 2 * iInset) / 2);
        break;
    }


    // We can do the top and bottom safely here
    int i = 0;
    for (i = 0; i < iInset; i++)
    {
        AddEdge(0, i, iDX);
        AddEdge(0, iDY-i-1, iDX);
    }

    // OK, now just the middle part
    int iRadType = RADIUS_ON;
    CPointList List;
    CHLDibDC::CalcArcPoints(&List, iRad, TRUE, iRadType);

    int iN = List.NPoints();
    int iXA = iInset + iRad;
    int iYA = iInset + iRad;
    int iYB = iDY - iInset - iRad - 1;
    int iYFill0 = iInset;
    int iYFill1 = iDY-iInset-1;

    for (i = 0; i < iN; i++)
    {
        int iDXR = List.X(i);
        int iDYR = List.Y(i);
        int iDXSide = iXA - iDXR;
        int iDXSideL = iDXSide;
        int iDXSideR = iDXSide;
    
        switch (iDirection)
        {
        case DIR_RIGHT: iDXSideL = 0;   break;
        case DIR_LEFT:  iDXSideR = 0;   break;
        }

        if (iDirection != DIR_DOWN)
        {
            AddEdge(0,              iYA - iDYR,     iDXSideL);
            AddEdge(iDX - iDXSideR, iYA - iDYR,     iDXSideR);
        }
        else
        {
            AddEdge(0, iYA - iDYR, iDX);
        }


        if (iDirection != DIR_UP)
        {
            if (iYB+iDYR != iYA-iDYR)
            {
                AddEdge(0,              iYB + iDYR,     iDXSideL);
                AddEdge(iDX - iDXSideR, iYB + iDYR,     iDXSideR);
            }
        }
        else
        {
            AddEdge(0, iYB + iDYR, iDX);
        }

        iYFill0 = __max(iYFill0, iYA-iDYR+1);
        iYFill1 = __min(iYFill1, iYB+iDYR-1);
    }
    
    // Now for the sides
    if (iInset <= 0)
        return;

    for (int iY = iYFill0; iY <= iYFill1; iY++)
    {
        AddEdge(0, iY, iInset);
        AddEdge(iDX-iInset, iY, iInset);
    }
}
/*============================================================================*/

    void                        CEdgeList::CreateForRoundRectInt(
    int                         iDX,
    int                         iDY,
    int                         iInset,
    int                         iRad,
    int                         iDir,
    int                         iRadiusType )

/*============================================================================*/
{
    LOGASSERT(m_Type == EDGE_HORIZONTAL);

    iRad = __min(iRad, (__min(iDX, iDY) - 2 * iInset) / 2 - 1);

    // OK, now just the middle part
    CPointList List;
    int iXA = iInset + iRad;
    int iYA = iInset + iRad;
    int iYB = iDY - iInset - iRad - 1;

    if (iDir >= CORNER_LL_INSIDE && iDir <= CORNER_LR_INSIDE)
    {
        CHLDibDC::CalcArcPoints(&List, iRad, TRUE, RADIUS_ON);
        int iN = List.NPoints();

        // Create for LR then flip
        for (int iFillY = iInset; iFillY < iYB; iFillY++)
        {
            AddEdge(iInset, iFillY, iDX);
        }
        
        for (int i = 0; i < iN; i++)
        {
            int iDXR = List.X(i);
            int iDYR = List.Y(i);
            int iDXSide = iXA-iDXR;
            AddEdge(iInset, iYB + iDYR, iDX - iInset - iDXSide);
        }

        switch (iDir)
        {
        case CORNER_LR_INSIDE:                                          break;
        case CORNER_LL_INSIDE:      { Flip(iDX, iDY, TRUE, FALSE);  }   break;
        case CORNER_UR_INSIDE:      { Flip(iDX, iDY, FALSE, TRUE);  }   break;
        case CORNER_UL_INSIDE:      { Flip(iDX, iDY, TRUE,  TRUE);  }   break;
        }

        return;
    }

    CHLDibDC::CalcArcPoints(&List, iRad, TRUE, RADIUS_ON);

    if (iRadiusType == RADIUS_INSET)
        CHLDibDC::InsetArcPoints(&List);

    int iN = List.NPoints();

    for (int i = 0; i < iN; i++)
    {
        int iDXR = List.X(i);
        int iDYR = List.Y(i);
        int iDXSide     = iXA - iDXR;
        int iDXLeft     = iDXSide;
        int iDXRight    = iDXSide;

        switch (iDir)
        {
        case DIR_RIGHT: iDXLeft     = iInset;   break;
        case DIR_LEFT:  iDXRight    = iInset;   break;
        }    

        if (iDir == DIR_DOWN)
            AddEdge(iInset,     iYA - iDYR,     iDX-2*iInset);
        else
            AddEdge(iDXLeft,    iYA - iDYR,     iDX-iDXLeft-iDXRight);

        if (iDir == DIR_UP)
            AddEdge(iInset,     iYB + iDYR,     iDX-2*iInset);
        else
            AddEdge(iDXLeft,    iYB + iDYR,     iDX-iDXLeft-iDXRight);
    }
    
    for (int iY = iYA + 1; iY < iYB; iY++)
    {
        AddEdge(iInset, iY, iDX-2*iInset);
    }
}
/*============================================================================*/

    void                        CEdgeList::CreateForArrow(
    int                         iDX,
    int                         iDY,
    int                         iRadMajor,
    int                         iRadMinor,
    int                         iDirection,
    int                         iNestSize )

/*============================================================================*/
{
    int iXR = iDX - iRadMajor;
    
    CPointList PointsMajor;
    CPointList PointsMinor;
    CHLDibDC::CalcArcPoints(&PointsMajor, iRadMajor, TRUE, RADIUS_ON);
    CHLDibDC::CalcArcPoints(&PointsMinor, iRadMinor, TRUE, RADIUS_ON);
    int iDY2 = iDY/2;

    int iXMinor = 0;
    int iYMinor = 0;
    for (int i = 0; i < PointsMinor.NPoints() && iXMinor == 0; i++)
    {
        if (PointsMinor.X(i) > PointsMinor.Y(i))
        {
            iXMinor = PointsMinor.X(i);
            iYMinor = iRadMinor-PointsMinor.Y(i);
        }
    }

    int iDXNUB = iRadMinor-iXMinor+2;

    for (int iY = 0; iY < iDY; iY++)
    {
        int iX0 = 0;
        int iX1 = iDX-1;
        int iDYMID = abs(iDY2-iY);

        if (iY < iDY2)
        {
            iX0 = iDY2-iY-iDXNUB;
            if (iY < iYMinor)
                iX0 += iXMinor - PointsMinor.XatY(iRadMinor-iY);
            


            if (iY < iRadMajor)
                iX1 = iXR+PointsMajor.X(iY);
        }
        else
        {
            int iDYB = iDY-iY;
            iX0 = iY-iDY2-iDXNUB;

            if (iDYB < iYMinor)
                iX0 += iXMinor - PointsMinor.XatY(iRadMinor-iDYB);

            if (iDYB < iRadMajor)
            {
                iX1 = iXR+PointsMajor.X(iDYB);
            }
        }                        
        if (iDYMID < iRadMinor-iYMinor)
        {
            iX0 = iRadMinor-PointsMinor.XatY(iDYMID);
        }


        iX1 = __min(iDX-1, iX1);
        iX0 = __max(iX0, 0);

        if (iNestSize > 0)
        {
            iX1 = iX0+iDX-iDY2-iNestSize;
            iX1 = __min(iDX-1, iX1);
        }

        int iD = iX1-iX0+1;
        if (iDirection == DIR_RIGHT)
            iX0 = iDX-iX0-iD;

        LOGASSERT(iX0 >= 0);
        AddEdge(iX0, iY, iD);

        if (iNestSize > 0)
        {
            int iDXR = iDY2-iX0;
            AddEdge(iDX-iDXR, iY, iDXR);
        }

    }    
}
/*============================================================================*/

    int                         CEdgeList::IndexOfY(int iY)

/*============================================================================*/
{
    for (int i = 0; i < m_iNPts; i++)
    {
        if (m_piY[i] == iY)
            return i;
    }

    return 0;
}
/*============================================================================*/

    void                        CEdgeList::Translate(
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    for (int i = 0; i < m_iNPts; i++)
    {
        m_piX[i] = m_piX[i] + iDX;
        m_piY[i] = m_piY[i] + iDY;
    }
}
/*============================================================================*/

    BOOL                        CEdgeList::ContainsPoint(
    int                         iX,
    int                         iY )

/*============================================================================*/
{
    for (int i = 0; i < m_iNPts; i++)
    {
        if (m_piY[i] == iY)
        {
            int iXMin = m_piX[i];
            int iXMax = iXMin + m_piD[i];
            if (iX >= iXMin && iX <= iXMax)
                return TRUE;
        }
    }
    return FALSE;
}
/*============================================================================*/

    void                        CEdgeList::Flip(
    int                         iDX,
    int                         iDY,
    BOOL                        bHz,
    BOOL                        bVt )

/*============================================================================*/
{
    if (bVt)
    {
        int *piX = new int[m_iNPts];
        int *piY = new int[m_iNPts];
        int *piD = new int[m_iNPts];
        memcpy(piX, m_piX, m_iNPts * sizeof(int));
        memcpy(piY, m_piY, m_iNPts * sizeof(int));
        memcpy(piD, m_piD, m_iNPts * sizeof(int));

        for (int i = 0; i < m_iNPts; i++)
        {
            m_piY[i] = iDY - piY[m_iNPts-i-1] - 1;
            m_piX[i] = piX[m_iNPts-i-1];
            m_piD[i] = piD[m_iNPts-i-1];
        }

        delete [] piX;
        delete [] piY;
        delete [] piD;
    }

    if (!bHz)
        return;

    for (int i = 0; i < m_iNPts; i++)
    {
        int iXR = m_piX[i] + m_piD[i];
        m_piX[i] = iDX - iXR;
    }
}
/*============================================================================*/

    CPointList::CPointList()

/*============================================================================*/
{
    m_piX = NULL;
    m_piY = NULL;
    m_iNPts = 0;
    m_iNAlloc = 0;
}
/*============================================================================*/

    CPointList::~CPointList()

/*============================================================================*/
{
    RemoveAll();
}
/*============================================================================*/

    int                         CPointList::X(
    int                         iIndex)

/*============================================================================*/
{
    LOGASSERT(iIndex >= 0 && iIndex < m_iNPts);
    return m_piX[iIndex];    
}
/*============================================================================*/

    void                        CPointList::X(
    int                         iIndex, 
    int                         iNew)

/*============================================================================*/
{
    LOGASSERT(iIndex >= 0 && iIndex < m_iNPts);
    m_piX[iIndex] = iNew;
}
/*============================================================================*/

    int                         CPointList::Y(
    int                         iIndex)

/*============================================================================*/
{
    LOGASSERT(iIndex >= 0 && iIndex < m_iNPts);
    return m_piY[iIndex];    
}
/*============================================================================*/

    void                        CPointList::Y(
    int                         iIndex, 
    int                         iNew)

/*============================================================================*/
{
    LOGASSERT(iIndex >= 0 && iIndex < m_iNPts);
    m_piY[iIndex] = iNew;
}
/*============================================================================*/

    int                         CPointList::XatY(int iY)

/*============================================================================*/
{
    static int iYLast = 0;
    static int iIndexLast = 0;
    
    if (iY == (iYLast + 1) && (iIndexLast < m_iNPts-1) && (iIndexLast >= 0))
    {
        if (m_piY[iIndexLast+1] == iY)
        {
            iYLast = iY;    
            iIndexLast = iIndexLast+1;
            return m_piX[iIndexLast];
        }
    }

    if (iY == (iYLast - 1) && (iIndexLast > 0) && (iIndexLast < m_iNPts))
    {
        if (m_piY[iIndexLast-1] == iY)
        {
            iYLast = iY;    
            iIndexLast = iIndexLast-1;
            return m_piX[iIndexLast];
        }
    }

    iYLast = iY;
    for (int i = 0; i < m_iNPts; i++)
    {
        if (m_piY[i] == iY)
        {
            iIndexLast = i;
            return m_piX[i];
        }
    }

    return 0;
}
/*============================================================================*/

    int                         CPointList::NPoints()

/*============================================================================*/
{
    return m_iNPts;
}
/*============================================================================*/

    void                        CPointList::RemoveAll()

/*============================================================================*/
{
    delete [] m_piX;
    delete [] m_piY;
    m_piX = NULL;
    m_piY = NULL;
    m_iNPts = 0;
    m_iNAlloc = 0;
}
/*============================================================================*/

    void                        CPointList::RemoveLast()

/*============================================================================*/
{
    if (m_iNPts < 1)
        return;
    m_iNPts--;
}
/*============================================================================*/

    void                        CPointList::AddPoint(
    int                         iX,
    int                         iY )

/*============================================================================*/
{
    if (m_iNPts + 1 > m_iNAlloc)
    {
        int *piOldX = m_piX;
        int *piOldY = m_piY;
        m_iNAlloc += 128;
        m_piX = new int[m_iNAlloc];
        m_piY = new int[m_iNAlloc];
        if (piOldX != NULL && piOldY != NULL)
        {
            memcpy(m_piX, piOldX, sizeof(int) * m_iNPts);
            memcpy(m_piY, piOldY, sizeof(int) * m_iNPts);
            delete [] piOldX;
            delete [] piOldY;
        }
    }
    
    m_piX[m_iNPts] = iX;
    m_piY[m_iNPts] = iY;
    m_iNPts++;
}
/*============================================================================*/

    void                        CPointList::InsertPointY(
    int                         iX,
    int                         iY )

/*============================================================================*/
{
    if (m_iNPts + 1 > m_iNAlloc)
    {
        int *piOldX = m_piX;
        int *piOldY = m_piY;
        m_iNAlloc += 128;
        m_piX = new int[m_iNAlloc];
        m_piY = new int[m_iNAlloc];
        if (piOldX != NULL && piOldY != NULL)
        {
            memcpy(m_piX, piOldX, sizeof(int) * m_iNPts);
            memcpy(m_piY, piOldY, sizeof(int) * m_iNPts);
            delete [] piOldX;
            delete [] piOldY;
        }
    }
    
    for (int i = 0; i < m_iNPts; i++)
    {
        if (m_piY[i] < iY)
        {
            int iNMove = m_iNPts-i;
            memmove(m_piX+i+1, m_piX+i, iNMove*sizeof(int));
            memmove(m_piY+i+1, m_piY+i, iNMove*sizeof(int));
            m_piX[i] = iX;
            m_piY[i] = iY;
            m_iNPts++;
            return;
        }
    }

    m_piX[m_iNPts] = iX;
    m_piY[m_iNPts] = iY;
    m_iNPts++;
}
/*============================================================================*/

    void                        CPointList::RemoveCommonPoints()

/*============================================================================*/
{
    if (NPoints() < 2)
        return;

    for (int i = 1; i < NPoints(); i++)
    {
        if (X(i) == X(i-1) && Y(i) == Y(i-1))
        {
            RemoveAtIndex(i);
            i--;
        }
    }
}
/*============================================================================*/

    void                        CPointList::RemoveAtIndex(
    int                         iIndex)

/*============================================================================*/
{
    if (iIndex >= m_iNPts || iIndex < 0)
    {
        LOGASSERT(FALSE);
        return;
    }

    m_iNPts--;
    int iNMove = m_iNPts - iIndex;
    memmove(m_piX + iIndex, m_piX + iIndex + 1, iNMove * sizeof(int));
    memmove(m_piY + iIndex, m_piY + iIndex + 1, iNMove * sizeof(int));
}
