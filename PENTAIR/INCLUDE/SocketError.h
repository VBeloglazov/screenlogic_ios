/*
--------------------------------------------------------------------------------

    SocketError.h

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once

/*============================================================================*/

	class SocketError

/*============================================================================*/
{
private:
	SocketError();

public:
	// Interface
	static CHLString LastSocketError( /*out*/ int* piErrorCode = NULL )
	{
#ifndef IPHONE
		int iError = WSAGetLastError();
#else
		int iError = errno;        
#endif

		if ( piErrorCode )
		{
			*piErrorCode = iError;
		}

		return SocketErrorFromCode( iError );
	}

	static CHLString SocketErrorDescription( int iErrorCode )
	{
		return SocketErrorFromCode( iErrorCode );
	}

private:
	static CHLString SocketErrorFromCode( int iErrorCode )
	{
		CHLString strError;

        switch( iErrorCode )
		{
#ifndef IPHONE

        // "http://msdn.microsoft.com/en-us/library/ms740668(v=vs.85).aspx";
		
        case WSAEINTR:                  // 10004
            strError = _HLTEXT("Interrupted function call.");
			break;
        case WSAEBADF:                  // 10009
            strError = _HLTEXT("File handle is not valid.");
            break;
		case WSAEACCES:                 // 10013
            strError = _HLTEXT("Permission denied.");
			break;
		case WSAEFAULT:                 // 10014
            strError = _HLTEXT("Bad address.");
			break;
		case WSAEINVAL:                 // 10022
            strError = _HLTEXT("Invalid argument.");
			break;
		case WSAEMFILE:                 // 10024
            strError = _HLTEXT("Too many open files.");
			break;
		case WSAEWOULDBLOCK:            // 10035
            strError = _HLTEXT("Resource temporarily unavailable.");
			break;
		case WSAEINPROGRESS:            // 10036
            strError = _HLTEXT("Operation now in progress.");
			break;
		case WSAEALREADY:               // 10037
            strError = _HLTEXT("Operation already in progress.");
			break;
		case WSAENOTSOCK:               // 10038
            strError = _HLTEXT("Socket operation on nonsocket.");
			break;
        case WSAEDESTADDRREQ:           // 10039
            strError = _HLTEXT("Destination address required.");
			break;
		case WSAEMSGSIZE:               // 10040
            strError = _HLTEXT("Message too long.");
			break;
		case WSAEPROTOTYPE:             // 10041
            strError = _HLTEXT("Protocol wrong type for socket.");
			break;
        case WSAENOPROTOOPT:            // 10042
            strError = _HLTEXT("Bad protocol option.");
			break;
		case WSAEPROTONOSUPPORT:        // 10043
            strError = _HLTEXT("Protocol not supported.");
			break;
		case WSAESOCKTNOSUPPORT:        // 10044
            strError = _HLTEXT("Socket type not supported.");
			break;
		case WSAEOPNOTSUPP:             // 10045
            strError = _HLTEXT("Operation not supported.");
			break;
        case WSAEPFNOSUPPORT:           // 10046
            strError = _HLTEXT("Protocol family not supported.");
			break;
		case WSAEAFNOSUPPORT:           // 10047
            strError = _HLTEXT("Address family not supported by protocol family.");
			break;
		case WSAEADDRINUSE:             // 10048
            strError = _HLTEXT("Address already in use.");
			break;
		case WSAEADDRNOTAVAIL:          // 10049
            strError = _HLTEXT("Cannot assign requested address.");
			break;
		case WSAENETDOWN:               // 10050
            strError = _HLTEXT("Network is down.");
			break;
		case WSAENETUNREACH:            // 10051
            strError = _HLTEXT("Network is unreachable.");
			break;
		case WSAENETRESET:              // 10052
            strError = _HLTEXT("Network dropped connection on reset.");
			break;
		case WSAECONNABORTED:           // 10053
            strError = _HLTEXT("Software caused connection abort.");
			break;
		case WSAECONNRESET:             // 10054
            strError = _HLTEXT("Connection reset by peer.");
			break;
		case WSAENOBUFS:                // 10055
            strError = _HLTEXT("No buffer space available.");
			break;
		case WSAEISCONN:                // 10056
            strError = _HLTEXT("Socket is already connected.");
			break;
		case WSAENOTCONN:               // 10057
            strError = _HLTEXT("Socket is not connected.");
			break;
		case WSAESHUTDOWN:              // 10058
            strError = _HLTEXT("Cannot send after socket shutdown.");
			break;
        case WSAETOOMANYREFS:           // 10059
            strError = _HLTEXT("Too many references.");
			break;
		case WSAETIMEDOUT:              // 10060
            strError = _HLTEXT("Connection timed out.");
			break;
		case WSAECONNREFUSED:           // 10061
            strError = _HLTEXT("Connection refused.");
			break;
        case WSAELOOP:                  // 10062
            strError = _HLTEXT("Cannot translate name.");
            break;
        case WSAENAMETOOLONG:           // 10063
            strError = _HLTEXT("Name too long.");
            break;
        case WSAEHOSTDOWN:              // 10064
            strError = _HLTEXT("Host is down.");
            break;
        case WSAEHOSTUNREACH:           // 10065
            strError = _HLTEXT("No route to host.");
            break;
        case WSAENOTEMPTY:              // 10066
            strError = _HLTEXT("Directory not empty.");
            break;
		case WSAEPROCLIM:               // 10067
			strError = _HLTEXT("Too many processes.");
			break;
		case WSAEUSERS:                 // 10068
			strError = _HLTEXT("User quota exceeded.");
			break;
		case WSAEDQUOT:                 // 10069
			strError = _HLTEXT("Disk quota exceeded.");
			break;
		case WSAESTALE:                 // 10070
			strError = _HLTEXT("Stale file handle reference.");
			break;
		case WSAEREMOTE:                // 10071
			strError = _HLTEXT("Item is remote.");
			break;
        case WSASYSNOTREADY:            // 10091
			strError = _HLTEXT("Network subsystem is unavailable.");
			break;
		case WSAVERNOTSUPPORTED:        // 10092
			strError = _HLTEXT("Winsock.dll version out of range.");
			break;
		case WSANOTINITIALISED:         // 10093
			strError = _HLTEXT("Successful WSAStartup not yet performed.");
			break;
		case WSAEDISCON:                // 10101
			strError = _HLTEXT("Graceful shutdown in progress.");
			break;
		case WSAENOMORE:                // 10102
			strError = _HLTEXT("No more results.");
			break;
		case WSAECANCELLED:             // 10103
			strError = _HLTEXT("Call has been canceled.");
			break;
		case WSAEINVALIDPROCTABLE:      // 10104
			strError = _HLTEXT("Procedure call table is invalid.");
			break;
		case WSAEINVALIDPROVIDER:       // 10105
			strError = _HLTEXT("Service provider is invalid.");
			break;
		case WSAEPROVIDERFAILEDINIT:    // 10106
			strError = _HLTEXT("Service provider failed to initialize.");
			break;
		case WSASYSCALLFAILURE:         // 10107
			strError = _HLTEXT("System call failure.");
			break;
		case WSASERVICE_NOT_FOUND:      // 10108
			strError = _HLTEXT("Service not found.");
			break;
		case WSATYPE_NOT_FOUND:         // 10109
			strError = _HLTEXT("Class type not found.");
			break;
		case WSA_E_NO_MORE:             // 10110
			strError = _HLTEXT("No more results.");
			break;
		case WSA_E_CANCELLED:           // 10111
			strError = _HLTEXT("Call was canceled.");
			break;
		case WSAEREFUSED:               // 10112
			strError = _HLTEXT("Database query was refused.");
			break;
		case WSAHOST_NOT_FOUND:         // 11001
			strError = _HLTEXT("Host not found.");
			break;
		case WSATRY_AGAIN:              // 11002
			strError = _HLTEXT("Nonauthoritative host not found.");
			break;
		case WSANO_RECOVERY:            // 11003
			strError = _HLTEXT("This is a nonrecoverable error.");
			break;
		case WSANO_DATA:                // 11004
			strError = _HLTEXT("Valid name, no data record of requested type.");
			break;
		case WSA_QOS_RECEIVERS:         // 11005
			strError = _HLTEXT("QOS receivers.");
			break;
		case WSA_QOS_SENDERS:           // 11006
			strError = _HLTEXT("QOS senders.");
			break;
		case WSA_QOS_NO_SENDERS:        // 11007
			strError = _HLTEXT("No QOS senders.");
			break;
		case WSA_QOS_NO_RECEIVERS:      // 11008
			strError = _HLTEXT("QOS no receivers.");
			break;
		case WSA_QOS_REQUEST_CONFIRMED: // 11009
			strError = _HLTEXT("QOS request confirmed.");
			break;
		case WSA_QOS_ADMISSION_FAILURE: // 11010
			strError = _HLTEXT("QOS admission error.");
			break;
		case WSA_QOS_POLICY_FAILURE:    // 11011
			strError = _HLTEXT("QOS policy failure.");
			break;
		case WSA_QOS_BAD_STYLE:         // 11012
			strError = _HLTEXT("QOS bad style.");
			break;
		case WSA_QOS_BAD_OBJECT:        // 11013
			strError = _HLTEXT("QOS bad object.");
			break;
		case WSA_QOS_TRAFFIC_CTRL_ERROR:// 11014
			strError = _HLTEXT("QOS traffic control error.");
			break;
		case WSA_QOS_GENERIC_ERROR:     // 11015
			strError = _HLTEXT("QOS generic error.");
			break;
		case WSA_QOS_ESERVICETYPE:      // 11016
			strError = _HLTEXT("QOS service type error.");
			break;
		case WSA_QOS_EFLOWSPEC:         // 11017
			strError = _HLTEXT("QOS flowspec error.");
			break;
		case WSA_QOS_EPROVSPECBUF:      // 11018
			strError = _HLTEXT("Invalid QOS provider buffer.");
			break;
		case WSA_QOS_EFILTERSTYLE:      // 11019
			strError = _HLTEXT("Invalid QOS filter style.");
			break;
		case WSA_QOS_EFILTERTYPE:       // 11020
			strError = _HLTEXT("Invalid QOS filter type.");
			break;
		case WSA_QOS_EFILTERCOUNT:      // 11021
			strError = _HLTEXT("Incorrect QOS filter count.");
			break;
		case WSA_QOS_EOBJLENGTH:        // 11022
			strError = _HLTEXT("Invalid QOS object length.");
			break;
		case WSA_QOS_EFLOWCOUNT:        // 11023
			strError = _HLTEXT("Incorrect QOS flow count.");
			break;
		case WSA_QOS_EUNKOWNPSOBJ:      // 11024
			strError = _HLTEXT("Unrecognized QOS object.");
			break;
		case WSA_QOS_EPOLICYOBJ:        // 11025
			strError = _HLTEXT("Invalid QOS policy object.");
			break;
		case WSA_QOS_EFLOWDESC:         // 11026
			strError = _HLTEXT("Invalid QOS flow descriptor.");
			break;
		case WSA_QOS_EPSFLOWSPEC:       // 11027
			strError = _HLTEXT("Invalid QOS provider-specific flowspec.");
			break;
		case WSA_QOS_EPSFILTERSPEC:     // 11028
			strError = _HLTEXT("Invalid QOS provider-specific filterspec.");
			break;
		case WSA_QOS_ESDMODEOBJ:        // 11029
			strError = _HLTEXT("Invalid QOS shape discard mode object.");
			break;
		case WSA_QOS_ESHAPERATEOBJ:     // 11030
			strError = _HLTEXT("Invalid QOS shaping rate object.");
			break;
		case WSA_QOS_RESERVED_PETYPE:   // 11031
			strError = _HLTEXT("Reserved policy QOS element type.");
			break;
#else // IPHONE
        case EAGAIN:
            strError = _HLTEXT("The file was marked for non-blocking I/O, and no data were ready to be read.");
            break;
        case EBADF:
            strError = _HLTEXT("fildes is not a valid file or socket descriptor open for reading.");
            break;
        case EFAULT:
            strError = _HLTEXT("Buf points outside the allocated address space.Or part of the iov points outside the process’s allocated address space.");
            break;
        case EINTR:
            strError = _HLTEXT("A read from a slow device was interrupted before any data arrived by the delivery of a signal.");
            break;
        case EINVAL:
            strError = _HLTEXT("The pointer associated with fildes was negative. Or the specified file offset is invalid. Or Iovcnt was less than or equal to 0, or greater than 16. Or one of the iov_len values in the iov array was negative. Or the sum of the iov_len values in the iov array overflowed a 32-bit integer.");
            break;
        case EIO:
            strError = _HLTEXT("An I/O error occurred while reading from the file system. Or the process group is orphaned. Or the file is a regular file, nbyte is greater than 0, the starting position is before the end-of-file, and the starting position is greater than or equal to the offset maximum established for the open file descriptor associated with fildes.");
            break;
        case EISDIR:
            strError = _HLTEXT("An attempt is made to read a directory.");
            break;
        case ENOBUFS:
            strError = _HLTEXT("An attempt to allocate a memory buffer fails.");
            break;
        case ENOMEM:
            strError = _HLTEXT("Insufficient memory is available.");
            break;
        case ENXIO:
            strError = _HLTEXT("An action is requested of a device that does not exist. Or a requested action cannot be performed by the device.");
            break;
        case ESPIPE:
            strError = _HLTEXT("The file descriptor is associated with a pipe, socket, or FIFO.");
            break;
        case ECONNRESET:
            strError = _HLTEXT("The connection is closed by the peer during a read attempt on a socket.");
            break;
        case ENOTCONN:
            strError = _HLTEXT("A read is attempted on an unconnected socket.");
            break;
        case ETIMEDOUT:
            strError = _HLTEXT("A transmission timeout occurs during a read attempt on a socket.");
            break;
#endif
		default:
			strError = _HLTEXT("Unknown.");
		}

		return strError;
	}
};
