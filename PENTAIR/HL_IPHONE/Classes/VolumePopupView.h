//
//  VolumePopupView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/2/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "MainView.h"

@class CAudioZone;
@class CHLProgress;

/*============================================================================*/

@interface CVolumePopupView : UIView < CSink >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    CHLProgress                         *m_pProgress;
	CGGradientRef                       m_Gradient;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone Direction:(int)iDir;
-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;
@end
