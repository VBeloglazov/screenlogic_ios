//
//  MediaArtView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/29/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MediaArtView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CMediaArtView
/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
    // initialize the view, calling super and setting the 
    // properties to nil
    if (self = [super initWithFrame:frame]) 
    {
        m_pImage = NULL;
        
		// set the background color of the view to clearn
		self.backgroundColor=[UIColor clearColor];
		
    }
    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
	
	// get the background image for the state of the element
	// position it appropriately and draw the image
    if (m_pImage == NULL)
        return;
    [m_pImage drawInRect:rect];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pImage release];
	[super dealloc];
}
/*============================================================================*/


CGImageRef AEViewCreateGradientImage (int pixelsWide,
									  int pixelsHigh)

/*============================================================================*/
{
	CGImageRef theCGImage = NULL;
    CGContextRef gradientBitmapContext = NULL;
    CGColorSpaceRef colorSpace;
	CGGradientRef grayScaleGradient;
	CGPoint gradientStartPoint, gradientEndPoint;
	
	// Our gradient is always black-white and the mask
	// must be in the gray colorspace
    colorSpace = CGColorSpaceCreateDeviceGray();
	
	// create the bitmap context
    gradientBitmapContext = CGBitmapContextCreate (NULL, pixelsWide, pixelsHigh,
												   8, 0, colorSpace, kCGImageAlphaNone);
	
	if (gradientBitmapContext != NULL) {
		// define the start and end grayscale values (with the alpha, even though
		// our bitmap context doesn't support alpha the gradient requires it)
		CGFloat colors[] = {0.0, 1.0,1.0, 1.0,};
		
		// create the CGGradient and then release the gray color space
		grayScaleGradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
		
		// create the start and end points for the gradient vector (straight down)
		gradientStartPoint = CGPointZero;
		gradientEndPoint = CGPointMake(0,pixelsHigh);
		
		// draw the gradient into the gray bitmap context
		CGContextDrawLinearGradient (gradientBitmapContext, grayScaleGradient, gradientStartPoint, gradientEndPoint, kCGGradientDrawsAfterEndLocation);
		
		// clean up the gradient
		CGGradientRelease(grayScaleGradient);
		
		// convert the context into a CGImageRef and release the
		// context
		theCGImage=CGBitmapContextCreateImage(gradientBitmapContext);
		CGContextRelease(gradientBitmapContext);
		
	}
	
	// clean up the colorspace
	CGColorSpaceRelease(colorSpace);
	
	// return the imageref containing the gradient
    return theCGImage;
}
/*============================================================================*/

- (UIImage *)reflectedImageRepresentationWithHeight:(NSUInteger)height

/*============================================================================*/
{
	CGContextRef mainViewContentContext;
    CGColorSpaceRef colorSpace;
	
    colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// create a bitmap graphics context the size of the image
    mainViewContentContext = CGBitmapContextCreate (NULL, self.bounds.size.width,height, 8,0, colorSpace, kCGImageAlphaPremultipliedLast);
	
	// free the rgb colorspace
    CGColorSpaceRelease(colorSpace);	
	
	if (mainViewContentContext==NULL)
		return NULL;
	
	// offset the context. This is necessary because, by default, the  layer created by a view for
	// caching its content is flipped. But when you actually access the layer content and have
	// it rendered it is inverted. Since we're only creating a context the size of our 
	// reflection view (a fraction of the size of the main view) we have to translate the context the
	// delta in size, render it, and then translate back (we could have saved/restored the graphics 
	// state
	
	CGFloat translateVertical=self.bounds.size.height-height;
	CGContextTranslateCTM(mainViewContentContext,0,-translateVertical);
	
	// render the layer into the bitmap context
	[self.layer renderInContext:mainViewContentContext];
	
	// translate the context back
	CGContextTranslateCTM(mainViewContentContext,0,translateVertical);
	
	// Create CGImageRef of the main view bitmap content, and then
	// release that bitmap context
	CGImageRef mainViewContentBitmapContext=CGBitmapContextCreateImage(mainViewContentContext);
	CGContextRelease(mainViewContentContext);
	
	// create a 2 bit CGImage containing a gradient that will be used for masking the 
	// main view content to create the 'fade' of the reflection.  The CGImageCreateWithMask
	// function will stretch the bitmap image as required, so we can create a 1 pixel wide
	// gradient
	CGImageRef gradientMaskImage=AEViewCreateGradientImage(1,height);
	
	// Create an image by masking the bitmap of the mainView content with the gradient view
	// then release the  pre-masked content bitmap and the gradient bitmap
	CGImageRef reflectionImage=CGImageCreateWithMask(mainViewContentBitmapContext,gradientMaskImage);
	CGImageRelease(mainViewContentBitmapContext);
	CGImageRelease(gradientMaskImage);
	
	// convert the finished reflection image to a UIImage 
	UIImage *theImage=[UIImage imageWithCGImage:reflectionImage];
	
	// image is retained by the property setting above, so we can 
	// release the original
	CGImageRelease(reflectionImage);
	
	// return the image
	return theImage;
}
/*============================================================================*/

-(void)SetImage:(UIImage*)pImage

/*============================================================================*/
{
    [m_pImage release];
    m_pImage = pImage;
    [m_pImage retain];
}
@end

@implementation CMediaArtReflectionView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MediaArtView:(CMediaArtView*)pView

/*============================================================================*/
{
    // initialize the view, calling super and setting the 
    // properties to nil
    if (self = [super initWithFrame:rFrame]) 
    {
        [self setOpaque:NO];
        m_pView = pView;
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [self setImage:[m_pView reflectedImageRepresentationWithHeight:self.bounds.size.height]];
    [super layoutSubviews];
}
@end
