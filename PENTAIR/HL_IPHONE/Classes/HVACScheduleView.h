//
//  HVACScheduleView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/20/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "hlview.h"

@class CTStat;
@class CHLButton;

/*============================================================================*/

@interface CHVACScheduleView : UIView  < CSink, CHLCommandResponder >

/*============================================================================*/
{
    CTStat                              *m_pTStat;
    CHLButton                           *m_pModes[4];
    CHLButton                           *m_pDn;
    CHLButton                           *m_pUp;
    int                                 m_iLastProgramState;
}

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat;
-(void)UpdateHoldTime;

@end
