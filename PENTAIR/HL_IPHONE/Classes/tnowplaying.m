//
//  tnowplaying.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/26/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "tnowplaying.h"
#import "AudioZone.h"
#import "PlaylistItem.h"
#import "MediaArtView.h"
#import "hlm.h"
#import "tmp3.h"
#import "MediaZonePage.h"
#import "crect.h"
#import "tplaylist.h"
#import "MainView.h"
#import "ButtonBar.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "CustomPage.h"

#define PROGRESS_SIZE           20
#define LABEL_SIZE              18

#define CMD_DELETE              1000
#define CMD_REPEAT              1001
#define CMD_RANDOM              1002
#define CMD_PREV                1003
#define CMD_NEXT                1004
#define CMD_STOP                1005
#define CMD_PAUSE               1006
#define CMD_PLAY                1007


@implementation CNowPlayingPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_pZone = pZone;
        UIView *pSuper = self;

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            m_pScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
            pSuper = m_pScrollView;
            [self addSubview:m_pScrollView];
            [m_pScrollView release];
        }

        m_pAlbumView = [[CAlbumInfoView alloc] initWithFrame:CGRectZero Zone:pZone];
        [pSuper addSubview:m_pAlbumView];
        [m_pAlbumView release];
        
        m_pPlaylistView = [[CPlaylistView alloc] initWithFrame:CGRectZero Zone:pZone];
        [pSuper addSubview:m_pPlaylistView];
        [m_pPlaylistView release];

        m_pTransportBar = [[CTransportBar alloc] initWithFrame:CGRectZero Zone:pZone NowPlayingPage:self];
        [self addSubview:m_pTransportBar];
        [m_pTransportBar release];
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    int iDYTransport = MIN(80, [CMainView DY_TOOLBAR]);
    CGRect rTransport = [CRect BreakOffBottom:&rThis DY:iDYTransport];

    int iInset = [CHLComm GetInt:INT_STD_INSET];
    if (m_pScrollView != NULL)
    {
        [CRect Inset:&rTransport DX:iInset DY:0];
    }

    [m_pTransportBar setFrame:rTransport];

    if (m_pScrollView != NULL)
    {
        [m_pScrollView setFrame:rThis];
        [m_pScrollView setContentSize:CGSizeMake(2*rThis.size.width, rThis.size.height)];
        [m_pScrollView setPagingEnabled:YES];
        CGRect rSub1 = rThis;
        rSub1.origin.x = 0;
        rSub1.origin.y = 0;
        CGRect rSub2 = rSub1;
        rSub2.origin.x += rThis.size.width;

        [CRect Inset:&rSub1 DX:iInset DY:0];
        [CRect Inset:&rSub2 DX:iInset DY:0];

        [m_pAlbumView setFrame:rSub1];
        [m_pPlaylistView setFrame:rSub2];
        
        if (!CGSizeEqualToSize(m_Size, rThis.size))
        {
            CGPoint ptOff = m_pScrollView.contentOffset;
            if (ptOff.x != 0)
            {
                ptOff.x = rThis.size.width;
            }
            m_Size = rThis.size;
            [m_pScrollView setContentOffset:ptOff animated:YES];
        }

    }
    else
    {
        int iDXList = rThis.size.width * 60 / 100;
        CGRect rList = [CRect BreakOffRight:&rThis DX:iDXList];
        [m_pPlaylistView setFrame:rList];
        [m_pAlbumView setFrame:rThis];
    }
}
/*============================================================================*/

-(void)DeleteItem

/*============================================================================*/
{
    int iIndex = [m_pPlaylistView SelectedIndex];
    if (iIndex < 0)
        return;
    CNSQuery *pQ =[[CNSQuery alloc] initWithQ:HLM_AUDIO_REMOVEITEMQ];
    [pQ autorelease];

    [pQ PutInt:m_pZone.m_iID];
    [pQ PutInt:iIndex];
    [pQ SendMessage];
}
/*============================================================================*/

-(void)PlayItem

/*============================================================================*/
{
    int iIndex = [m_pPlaylistView SelectedIndex];
    if (iIndex < 0 || iIndex == [m_pZone ActivePlaylistItem])
    {
        [m_pZone ZoneCommand:HL_PLAYLIST_PLAY Data1:0 Data2:0];
        return;
    }

    CNSQuery *pQ =[[CNSQuery alloc] initWithQ:HLM_AUDIO_PLAYTRACKQ];
    [pQ autorelease];

    [pQ PutInt:m_pZone.m_iID];
    [pQ PutInt:iIndex];
    [pQ SendMessage];
}

@end

@implementation CTransportBar

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone NowPlayingPage:(CNowPlayingPage *)pNowPlayingPage

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_pNowPlayingPage = pNowPlayingPage;
        m_pZone = pZone;
        [m_pZone AddSink:self];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            m_pDelete       = [[CButtonBar alloc] initWithFrame:CGRectZero];
            m_pRepeatRandom = [[CButtonBar alloc] initWithFrame:CGRectZero];
        }
        m_pTransport    = [[CButtonBar alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pDelete];
        [self addSubview:m_pRepeatRandom];
        [self addSubview:m_pTransport];
        [m_pDelete release];
        [m_pRepeatRandom release];
        [m_pTransport release];

        [self InitButton:m_pDelete Icon:@"DELETE.png" Data:CMD_DELETE];

        m_pRepeat = [self InitButton:m_pRepeatRandom Icon:@"REPEAT.png" Data:CMD_REPEAT];
        m_pRandom = [self InitButton:m_pRepeatRandom Icon:@"RANDOM.png" Data:CMD_RANDOM];

        [self InitButton:m_pTransport Icon:@"TRACK_BACK.png" Data:CMD_PREV];
        [self InitButton:m_pTransport Icon:@"STOP.png" Data:CMD_STOP];
        [self InitButton:m_pTransport Icon:@"PLAY.png" Data:CMD_PLAY];
        [self InitButton:m_pTransport Icon:@"PAUSE.png" Data:CMD_PAUSE];
        [self InitButton:m_pTransport Icon:@"TRACK_FORWARD.png" Data:CMD_NEXT];

        [self Notify:0 Data:SINK_STATECHANGED];
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [m_pTransport setFrame:rThis];
        return;
    }

    int iDXAvail = rThis.size.width;
    int iDXPad   = iDXAvail * 3 / 100;
    iDXAvail -= iDXPad;
    int iDXBtn = iDXAvail / 8;

    CGRect rDel = [CRect BreakOffLeft:&rThis DX:iDXBtn];
    [CRect BreakOffLeft:&rThis DX:iDXPad];
    CGRect rRR  = [CRect BreakOffLeft:&rThis DX:2*iDXBtn];
    [CRect BreakOffLeft:&rThis DX:iDXPad];

    [m_pDelete setFrame:rDel];
    [m_pRepeatRandom setFrame:rRR];
    [m_pTransport setFrame:rThis];
}
/*============================================================================*/

-(CButtonBarButton*)InitButton:(CButtonBar*)pBar Icon:(NSString*)pIcon Data:(int)iData

/*============================================================================*/
{
    if (pBar == NULL)
        return NULL;

    CButtonBarButton *pBtn = [pBar AddButtonText:NULL Icon:pIcon];
    [pBtn SetCommandID:iData Responder:NULL];
    [pBtn IconFormat:HL_ICONCENTER2X];
    [pBtn addTarget:self action:@selector(TransportEvent:) forControlEvents:UIControlEventTouchDown];
    return pBtn;
}
/*============================================================================*/

-(IBAction)TransportEvent:(id)sender

/*============================================================================*/
{
    CButtonBarButton *pBtn = (CButtonBarButton*)sender;
    switch ([pBtn CommandID])
    {
    case CMD_PLAY:      [m_pNowPlayingPage PlayItem];   break;
    case CMD_DELETE:    [m_pNowPlayingPage DeleteItem]; break;
    case CMD_PAUSE: [m_pZone ZoneCommand:HL_PLAYLIST_PAUSE      Data1:0 Data2:0];    break;
    case CMD_STOP:  [m_pZone ZoneCommand:HL_PLAYLIST_STOP       Data1:0 Data2:0];    break;
    case CMD_PREV:  [m_pZone ZoneCommand:HL_PLAYLIST_PREVIOUS   Data1:0 Data2:0];    break;
    case CMD_NEXT:  [m_pZone ZoneCommand:HL_PLAYLIST_NEXT       Data1:0 Data2:0];    break;
    case CMD_REPEAT:    [m_pZone ZoneCommand:HL_PLAYLIST_REPEAT Data1:![pBtn IsChecked] Data2:0];    break;
    case CMD_RANDOM:    [m_pZone ZoneCommand:HL_PLAYLIST_SHUFFLE Data1:![pBtn IsChecked] Data2:0];    break;
    }
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_STATECHANGED)
    {
        [m_pRepeat SetChecked:[m_pZone Repeat]];
        [m_pRandom SetChecked:[m_pZone Random]];
    }
}
@end

@implementation CAlbumInfoView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    if (self == [super initWithFrame:rect])
    {
        m_pZone = pZone;
        [m_pZone AddSink:self];
        [m_pZone InitData];

        m_pImageView = [[CMediaArtView alloc] initWithFrame:CGRectZero];
        m_pReflectView = [[CMediaArtReflectionView alloc] initWithFrame:CGRectZero MediaArtView:m_pImageView];
        m_pReflectView.alpha = 0.25;
        [self addSubview:m_pImageView];
        [self addSubview:m_pReflectView];

        m_pProgress = [[UIProgressView alloc] initWithFrame:CGRectZero];
        m_pArtist   = [[UILabel alloc] initWithFrame:CGRectZero];
        m_pAlbum    = [[UILabel alloc] initWithFrame:CGRectZero];
        m_pTrack    = [[UILabel alloc] initWithFrame:CGRectZero];

        self.backgroundColor = [UIColor clearColor];
        m_pProgress.opaque = NO;
        m_pProgress.alpha = 0.75;
        m_pProgress.contentMode = UIViewContentModeRedraw;
        m_pProgress.backgroundColor = [UIColor clearColor];


        [self addSubview:m_pProgress];
        [self addSubview:m_pArtist];
        [self addSubview:m_pAlbum];
        [self addSubview:m_pTrack];
        [CCustomPage InitStandardLabel:m_pArtist Size:14];
        [CCustomPage InitStandardLabel:m_pAlbum Size:14];
        [CCustomPage InitStandardLabel:m_pTrack Size:14];

        [self Notify:0 Data:0xFFFFFFFF];
    }
    
    return self;
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_ARTCHANGED)
    {
        UIImage *pImage = [m_pZone GetImage];
        [m_pImageView SetImage:pImage];
        [m_pImageView setNeedsDisplay];
        int iDY = m_pReflectView.bounds.size.height;
        UIImage *pRef = [m_pImageView reflectedImageRepresentationWithHeight:iDY];
        [m_pReflectView setImage:pRef];
        [m_pReflectView setNeedsDisplay];
    }

    if (iData & SINK_STATECHANGED)
    {
        float fProg = (float)[m_pZone ProgressPercent] / 100.f;
        [m_pProgress setProgress:fProg];
    }

    if ((iData & SINK_STATECHANGED) || (iData & SINK_ITEMSCHANGED))
    {
        if (([m_pZone ActiveSubSource] & 0x0000FFFF) == MP3SOURCE_IRADIO)
        {
            [m_pArtist setText:[m_pZone TunerStationText]];
            [m_pAlbum setText:[m_pZone TunerArtist]];
            [m_pTrack setText:[m_pZone TunerTrack]];
            [m_pProgress setHidden:TRUE];
            return;
        }


        int iActiveItem = [m_pZone ActivePlaylistItem];

        BOOL bNothingSelected = FALSE;
        if (iActiveItem < 0 || iActiveItem >= [m_pZone NPlaylistItems])
            bNothingSelected = TRUE;
        switch ([m_pZone PlayState])
        {
        case PLAYLIST_PLAYING:
        case PLAYLIST_PAUSED:
        case PLAYLIST_WORKING:
            break;
        default:
            bNothingSelected = TRUE;
            break;
        }

            
        if ([m_pZone NPlaylistItems] == 0)
        {
            [m_pArtist setText:@"Playlist is Empty"];
            [m_pAlbum setText:@""];
            [m_pTrack setText:@""];
            [m_pProgress setHidden:TRUE];
        }
        else if (bNothingSelected)
        {
            [m_pArtist setText:@""];
            [m_pAlbum setText:@""];
            [m_pTrack setText:@""];
            [m_pProgress setHidden:TRUE];
        }
        else
        {
            CPlaylistItem *pItem = [m_pZone PlaylistItem:iActiveItem];
            [m_pArtist setText:pItem.m_pArtist];
            [m_pAlbum setText:pItem.m_pAlbum];
            [m_pTrack setText:pItem.m_pTrack];

            [m_pProgress setHidden:FALSE];
        }
    }
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [m_pProgress release];
    [m_pArtist release];
    [m_pAlbum release];
    [m_pTrack release];
    [m_pImageView release];
    [m_pReflectView release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    [CRect Inset:&rBounds DX:8 DY:4];
    
    if (rBounds.size.width <= 0)
        return;
    if (rBounds.size.height <= 0)
        return;

    int iAspect = rBounds.size.height * 100 / rBounds.size.width;
    int iAspectThis = MIN(iAspect, 200);
    iAspectThis = MAX(iAspect, 150);
    
    CGRect rImage2 = rBounds;
    [CRect ConfineToAspect:&rImage2 Source:CGRectMake(0, 0, 100, iAspectThis)];
    CGRect rImage1= [CRect BreakOffTop:&rImage2 DY:rImage2.size.width];

    [m_pImageView setFrame:rImage1];
    [m_pReflectView setFrame:rImage2];
    
    int iDYData = 4 * LABEL_SIZE;
    [CRect CenterDownToY:&rImage2 DY:iDYData];
    
    CGRect rProgress = [CRect BreakOffTop:&rImage2 DY:LABEL_SIZE];
    [CRect Inset:&rProgress DX:10 DY:0];

    [m_pProgress setFrame:rProgress];
    [m_pArtist   setFrame:[CRect BreakOffTop:&rImage2 DY:LABEL_SIZE]];
    [m_pAlbum    setFrame:[CRect BreakOffTop:&rImage2 DY:LABEL_SIZE]];
    [m_pTrack    setFrame:[CRect BreakOffTop:&rImage2 DY:LABEL_SIZE]];

    
}
@end
