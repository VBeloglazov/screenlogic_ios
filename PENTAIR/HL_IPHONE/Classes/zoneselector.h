//
//  zoneselector.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/22/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>
#import "hlview.h"

@class CMainTab;
@class CSubTab;
@class CSubTabView;
@class CSubSystemPage;
@class CSubTabViewOverlay;
@class CSubTabView;
@class CZoneSelector;

/*============================================================================*/

@interface CZoneSelectorHorizonView : UIView

/*============================================================================*/
{
	CGGradientRef                       m_Gradient;
}

-(id)initWithFrame:(CGRect)rFrame;

@end

/*============================================================================*/

@interface CZonePageZoomDelegate : NSObject

/*============================================================================*/
{
    CSubTabView                 *m_pView;
    CSubSystemPage              *m_pPage;
    CZoneSelector               *m_pZoneSelector;
    CHLToolBar                  *m_pToolBar;
}
-(id)initWithSubTabView:(CSubTabView*)pView SubSystemPage:(CSubSystemPage*)pPage ZoneSelector:(CZoneSelector*)pZoneSelector ToolBar:(CHLToolBar*)pToolBar;
-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;

@end

/*============================================================================*/

@interface CSubTabViewOverlay : UIView < CHLView >

/*============================================================================*/
{
    CSubTabView                         *m_pSubTabView;
    BOOL                                m_bHighlightMode;
    UIView                              *m_pClientView;
}


-(id)initWithFrame:(CGRect)rect SubTabView:(CSubTabView*)pView;
-(void)SetHighlightMode:(BOOL)bNew;
-(CGRect)ClientRect;
-(void)SetClientView:(UIView*)pView;
-(void)FreeClientView;
-(void)InvalidateReflection;
-(void)NotifySubViewChanged;

@end

/*============================================================================*/

@interface CSubSystemOverlayClientView : UIView

/*============================================================================*/
{
    UIImageView                 *m_pImage;
    UILabel                     *m_pLabel;
    int                         m_iImageDX;
    int                         m_iImageDY;
}

-(id)initWithFrame:(CGRect)rFrame Icon:(NSString*)psIcon Text:(NSString*)psText;

@end

/*============================================================================*/

@interface CSubTabReflectionView : UIView

/*============================================================================*/
{
    CSubTabView                 *m_pSubTabView;
}

-(id)initWithFrame:(CGRect)rect SubTabView:(CSubTabView*)pView;
-(void)SetImage:(UIImage*)pImage;


@end

/*============================================================================*/

@interface CSubTabView : UIControl

/*============================================================================*/
{
    UILabel                 *m_pLabel;
    CMainTab                *m_pMainTab;
    CSubTab                 *m_pSubTab;
    CSubSystemPage          *m_pSysPage;
    CSubTabViewOverlay      *m_pOverlay;

    CSubTabReflectionView   *m_pReflectionView;
    BOOL                    m_bReflectionInvalid;
    NSTimer                 *m_pUpdateTimer;
    int                     m_iUpdateRepeat;
}

-(id)initWithMainTab:(CMainTab*)pMain SubTab:(CSubTab*)pSub SubSystemPage:(CSubSystemPage*)pSubSystemPage;
-(CSubSystemPage*)SubSystemPage;
-(void)DetachSystemPage;
-(CGRect)SubSystemRect;
-(CGRect)OverlayRect;
-(CGRect)LabelRect;
-(void)SetSubSystemPage:(CSubSystemPage*)pPage;
-(CSubTabViewOverlay*)GetOverlay;
-(CSubTabViewOverlay*)CreateOverlay;
-(BOOL)LandscapeMode;
-(int)PortraitLabelWidth;
-(CSubTabReflectionView*)ReflectionView;
-(UIImage*)GenerateReflectionImage;
-(void)InvalidateReflection;
-(void)BeginUpdateReflection:(int)iDuration Every:(int)iRate;
-(CGRect)MainFrameRect;
@end

/*============================================================================*/

@interface CZoneSelector : CSubSystemPage 

/*============================================================================*/
{
    CMainTab                        *m_pMainTab;
    UIScrollView                    *m_pScrollView;
    NSMutableArray                  *m_pTabs;
    int                             m_iDefaultIndex;
    CZoneSelectorHorizonView        *m_pHorizon;
}

-(id)initWithFrame:(CGRect)rFrame MainTab:(CMainTab*)pMainTab SubPageIndex:(int)iIndex ViewController:(IPAD_ViewController*)pCtlr;
-(CToolBarClientView*)CreateMenuView:(CGRect)rView ViewController:(IPAD_ViewController*)pCtlr Flags:(int)iFlags;
-(CSubTabView*)SubTabView:(int)iIndex;
-(void)FadeInReflections;
-(void)SetReflectionAlpha:(double)dA;
+(int)LabelSize;
+(int)ReflectionOffset;

@end
