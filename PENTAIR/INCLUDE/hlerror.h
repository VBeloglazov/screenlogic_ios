/*
--------------------------------------------------------------------------------

    HLERROR.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

This class is used to pass error information around.
--------------------------------------------------------------------------------
*/
#ifndef HLERROR_H
#define HLERROR_H

//
// Handy macro to throw errors.
//
#define CHLERROR( X )   CHLError(CHLError::X, __FILE__, __LINE__)


/*============================================================================*/

    class CHLError

/*============================================================================*/
{
// Variables
public:                                                                
    enum HLM_ERROR
    {
        HLE_UNKNOWN,
        HLE_TIMEOUT,
        HLE_BAD_PARAMETERS,
        HLE_NOT_CONNECTED,
        HLE_OUT_OF_RANGE,
        HLE_NOT_FOUND,
        HLE_BUSY,
        HLE_NO_RESPONSE,
        HLE_MEMORY,
        HLE_BAD_LOCK
    } m_eErrorCode;

    CHLString                   m_sFile;
    int                         m_iLine;


// Methods
public:
    CHLError();

    CHLError(
    HLM_ERROR                   eError );

    CHLError(
    HLM_ERROR                   eError,
    char                        *pFile,
    int                         iLine );
};



#endif

