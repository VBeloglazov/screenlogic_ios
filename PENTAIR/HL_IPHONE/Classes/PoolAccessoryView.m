//
//  PoolAccessoryView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolAccessoryView.h"
#import "MainView.h"
#import "hlm.h"
#import "NSPoolConfig.h"
#import "crect.h"
#import "CustomPage.h"

#define LABEL_SIZE          32//38
#define SUBLABEL_SIZE       10


@implementation CPoolAccessoryView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig Type:(int)iType

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pConfig = pConfig;
        m_iType   = iType;
    

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:LABEL_SIZE];
        m_pLabel.alpha = .5;

        m_pSubLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pSubLabel Size:SUBLABEL_SIZE];
        m_pSubLabel.alpha = .5;

        switch (m_iType)
        {
        case SUBTAB_PENTAIR_TEMP_POOL:  [m_pSubLabel setText:@"WATER"];     break;
        case SUBTAB_PENTAIR_TEMP_SPA:   [m_pSubLabel setText:@"WATER"];     break;
        case TYPE_AIRTEMP:              [m_pSubLabel setText:@"OUTSIDE"];   break;
        }
        
        [self addSubview:m_pLabel];
        [m_pLabel release];
        [self addSubview:m_pSubLabel];
        [m_pSubLabel release];

        [m_pConfig AddSink:self];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    if(NULL != m_pConfig)
    {
        [m_pConfig RemoveSink:self];
    }
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    CGRect rSubLabel = [CRect BreakOffBottom:&rBounds DY:SUBLABEL_SIZE];
    rSubLabel.origin.y -= 4;
    rSubLabel.origin.x -= 8;
    [m_pSubLabel setFrame:rSubLabel];
    [m_pLabel setFrame:rBounds];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
#ifdef PENTAIR
    switch (m_iType)
    {
    case SUBTAB_PENTAIR_TEMP_POOL:
        {
            

            int iVal = [m_pConfig WaterTemp:BODYTYPE_POOL];
            NSString *pText = [NSString stringWithFormat:@"%d°", iVal ];
            if (iVal == 0 || iVal == 0xFF)
                pText = @"-";

            if (![m_pConfig PoolIsOn])
            {
                pText = @"";
                [m_pSubLabel setHidden:YES];
            }
            else
            {
                [m_pSubLabel setHidden:NO];
            }
            [m_pLabel setText:pText];
        }
        break;

    case SUBTAB_PENTAIR_TEMP_SPA:
        {
            int iVal = [m_pConfig WaterTemp:BODYTYPE_SPA];
            NSString *pText = [NSString stringWithFormat:@"%d°", iVal];
            if (iVal == 0 || iVal == 0xFF)
                pText = @"-";
            if (![m_pConfig SpaIsOn])
            {
                pText = @"";
                [m_pSubLabel setHidden:YES];
            }
            else
            {
                [m_pSubLabel setHidden:NO];
            }

            [m_pLabel setText:pText];
        }
        break;

    case TYPE_AIRTEMP:
        {
            int iVal = [m_pConfig AirTemp];
            NSString *pText = [NSString stringWithFormat:@"%d°", iVal];
            if (iVal == 0 || iVal == 0xFF)
                pText = @"-";
            [m_pLabel setText:pText];
        }
        break;
    }
#endif
}

@end
