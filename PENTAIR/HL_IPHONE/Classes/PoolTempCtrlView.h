//
//  PoolTempCtrlView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "hlview.h"

@class CNSPoolConfig;
@class CPoolTempView;
@class CHLRadio;

/*============================================================================*/

@interface CPoolTempCtrlView : UIView < CSink >

/*============================================================================*/
{
    int                                 m_iDXLast;
    int                                 m_iDYLast;
    int                                 m_iHeatMode;

    CNSPoolConfig                       *m_pConfig;
    int                                 m_iType;

    CPoolTempView                       *m_pAct;
    CPoolTempView                       *m_pHeat;
//    CHLRadio                            *m_pMode;
   
}

-(id)initWithFrame:(CGRect)rect Type:(int)iType Config:(CNSPoolConfig*)pConfig;
-(void)UpdatePositions;


@end
