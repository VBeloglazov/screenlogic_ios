//
//  NSTunerControl.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//
#include "hlstd.h"

#import "NSTunerControl.h"
#import "NSMSG.h"

#include "tunerctrl.h"

/*============================================================================*/

@implementation NSTunerControl


/*============================================================================*/

-(id)init

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_pTunerControl = new CTunerControl();
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    delete m_pTunerControl;
    [super dealloc];
}
/*============================================================================*/

-(NSString*)ChannelText:(int)iStation State:(int)iState Type:(int)iType StationName:(NSString*)pName

/*============================================================================*/
{
    CHLString sStationName;
    [CNSMSG MakeCHLString:&sStationName From:pName];

    CHLString sText = m_pTunerControl->ChannelText(iStation, iState, iType, sStationName);
    int iLen = sText.GetLength();
    char *pBuf = sText.GetBufferCHAR(iLen);
    NSString *pRet = [NSString stringWithUTF8String:pBuf];
    return pRet;
}
/*============================================================================*/

-(int)ProcessDigit:(int)iCmdID State:(int)iState Type:(int)iType

/*============================================================================*/
{
    int iRet = m_pTunerControl->ProcessDigit(iCmdID, iState, iType);
    return iRet;
}
/*============================================================================*/

-(int)DTune:(int)iState Type:(int)iType

/*============================================================================*/
{
    int iRet = m_pTunerControl->DTune(iState, iType);
    return iRet;
}

/*============================================================================*/

-(int)PrepareDTune:(int)iState Type:(int)iType Station:(int)iStation

/*============================================================================*/
{
    return m_pTunerControl->PrepareDTune(iState, iType, iStation);
}
/*============================================================================*/

-(int)MaxStation:(int)iState Type:(int)iType

/*============================================================================*/
{
    return m_pTunerControl->MaxStation(iState, iType, FALSE);
}
/*============================================================================*/

-(int)MinStation:(int)iState Type:(int)iType

/*============================================================================*/
{
    return m_pTunerControl->MinStation(iState, iType);
}
/*============================================================================*/

-(int)NextBand:(int)iState Type:(int)iType

/*============================================================================*/
{
    return m_pTunerControl->NextBand(iState, iType);
}
/*============================================================================*/

-(int)NextSubChannel:(int)iChannel

/*============================================================================*/
{
    return m_pTunerControl->NextSubChannel(iChannel);
}
/*============================================================================*/

-(int)PrevSubChannel:(int)iChannel

/*============================================================================*/
{
    return m_pTunerControl->PrevSubChannel(iChannel);
}
@end
