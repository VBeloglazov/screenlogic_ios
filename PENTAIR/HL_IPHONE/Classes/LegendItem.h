//
//  LegendItem.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


/*============================================================================*/

@interface CLegendItem : UIView

/*============================================================================*/
{
    UIColor                             *m_pColor;
    UIColor                             *m_pColorFill;
    BOOL                                m_bRound;
}

-(id)initWithFrame:(CGRect)rFrame Color:(UIColor*)pColor FillColor:(UIColor*)pColorFill Text:(NSString*)pText Round:(BOOL)bRound;
-(id)initWithFrame:(CGRect)rFrame Color:(UIColor*)pColor Text:(NSString*)pText;

@end
