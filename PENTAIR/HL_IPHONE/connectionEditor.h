//
//  connectionEditor.h
//  XPAD_VIEW
//
//  Created by dmitriy on 4/1/14.
//
//

#import <UIKit/UIKit.h>
#import "ControllerDescriptor.h"
#import "connectionEditor.h"



@class ConnectionEditor;

@protocol ConnectionEditorDelegate <NSObject>


-(void)checkEditStatus:(ConnectionEditor*)controller isFinished:(int)status;

@end

@interface ConnectionEditor : UIViewController
{
    IBOutlet UILabel *labelSystemName;
    IBOutlet UILabel *labelNickName;
    IBOutlet UIButton *cancelButton;
    IBOutlet UILabel *systemNameText;
    IBOutlet UITextField *nickNameEditBox;
    IBOutlet UIButton *saveButton;
    IBOutlet UIButton *deleteButton;
    UIInterfaceOrientation              m_Orientation;
}
- (IBAction)editingDidBegin:(id)sender;

enum EDIT_RESULT
{
    CANCEL,
    SAVE,
    DELETE
};

typedef enum EDIT_RESULT EditResult;

@property (readonly) const int RESULT_CANCEL;
@property (assign, nonatomic) id <ConnectionEditorDelegate> checkStatusDelegate;
@property (assign) ControllerDescriptor* controllerDescriptor;
- (IBAction)cancelButtonPress:(id)sender;
- (IBAction)GoEvent:(id)sender;
- (IBAction)saveButtonPress:(id)sender;
- (IBAction)deleteButtonPress:(id)sender;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Orientation:(UIInterfaceOrientation) orientation;

@end
