//
//  HVACTempPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HVACTempPage.h"
#import "HVACTempView.h"
#import "HVACModePage_IPAD.h"
#import "tstat.h"
#import "hlm.h"
#import "hlradio.h"
#import "MainView.h"
#import "crect.h"

@implementation CHVACTempPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat ReadOnly:(BOOL)bReadOnly

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        self.autoresizingMask = 0xFFFFFFFF;

        m_bReadOnly = bReadOnly;
        m_iHVACState = -1;
        m_pTStat = pTStat;
        [m_pTStat AddSink:self];

        m_pHeat = [[CHVACTempView alloc] initWithFrame:CGRectZero TStat:m_pTStat Mode:TEMP_HEAT ReadOnly:m_bReadOnly]; 
        m_pCool = [[CHVACTempView alloc] initWithFrame:CGRectZero TStat:m_pTStat Mode:TEMP_COOL ReadOnly:m_bReadOnly]; 
        m_pRoom = [[CHVACTempView alloc] initWithFrame:CGRectZero TStat:m_pTStat Mode:TEMP_ROOM ReadOnly:m_bReadOnly]; 

        [self addSubview:m_pRoom];
        [self addSubview:m_pCool];
        [self addSubview:m_pHeat];

        CGRect rThis = CGRectMake(self.bounds.size.width/2, self.bounds.size.height/2, 0, 0);

        [m_pHeat setFrame:rThis];
        [m_pCool setFrame:rThis];
        [m_pRoom setFrame:rThis];
        [m_pHeat setAlpha:0];
        [m_pCool setAlpha:0];
        [m_pRoom setAlpha:0];

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && !m_bReadOnly)
        {
            m_pModePage = [[CHVACModePage_IPAD alloc] initWithFrame:CGRectZero TStat:m_pTStat];
            [self addSubview:m_pModePage];
            [m_pModePage release];
        }

        if ([m_pTStat StateOK])
        {
            [self UpdateControls];
//            [self Notify:0 Data:0];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTStat RemoveSink:self];
    [m_pRoom release];
    [m_pCool release];
    [m_pHeat release];
    [super dealloc];
}
/*============================================================================*/

- (void)willMoveToSuperview:(UIView *)newSuperview

/*============================================================================*/
{
    [self SetVisible:(newSuperview != NULL)];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [m_pHeat SetVisible:bVisible];
    [m_pCool SetVisible:bVisible];
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];

    [self UpdateControls];

	[UIView commitAnimations];
}
/*============================================================================*/

-(void)UpdateControls

/*============================================================================*/
{
    int iHVACMode = [m_pTStat HVACMode];
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    if (m_pModePage != NULL)
    {
        // To account for mode control on bottom of page
        iDYThis -= [CMainView DY_TOOLBAR];
    }
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        // To account for outside temp
        iDYThis -= 32;
    }

    BOOL bHeat = FALSE;
    BOOL bCool = FALSE;

    CGPoint ptHeat = m_pHeat.center;
    CGPoint ptCool = m_pCool.center;
    CGPoint ptRoom = m_pRoom.center;

    switch (iHVACMode)
    {
    case HVAC_STATE_OFF:
    case HVAC_STATE_NOTOK:
        {
            ptRoom = CGPointMake(iDXThis/2, iDYThis/2);
        }
        break;

    case HVAC_STATE_HEAT:
        {
            bHeat = TRUE;
            ptRoom = CGPointMake(iDXThis/2, iDYThis*1/4);
            ptHeat = CGPointMake(iDXThis/2, iDYThis*3/4);
        }
        break;

    case HVAC_STATE_COOL:
        {
            bCool = TRUE;
            ptRoom = CGPointMake(iDXThis/2, iDYThis*3/4);
            ptCool = CGPointMake(iDXThis/2, iDYThis*1/4);
        }
        break;

    case HVAC_STATE_AUTO:
        {
            bHeat = TRUE;
            bCool = TRUE;
            ptRoom = CGPointMake(iDXThis/4, iDYThis/2);
            ptHeat = CGPointMake(iDXThis*3/4, iDYThis*3/4);
            ptCool = CGPointMake(iDXThis*3/4, iDYThis*1/4);
        }
        break;
    }
    
    if (bHeat)
        [m_pHeat setAlpha:1.0];
    else
        [m_pHeat setAlpha:0.0];

    if (bCool)
        [m_pCool setAlpha:1.0];
    else
        [m_pCool setAlpha:0.0];

    [m_pRoom setAlpha:1.0];
    
    [m_pHeat setUserInteractionEnabled:bHeat];
    [m_pCool setUserInteractionEnabled:bCool];

    int iDXRoom = iDXThis * 45 / 100;
    int iDYRoom = iDYThis * 45 / 100;

    int iDXSet  = iDXThis * 35 / 100;
    int iDYSet  = iDYThis * 35 / 100;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iDXSet  = iDXThis * 50 / 100;
        iDYSet  = iDYThis * 45 / 100;
        iDYRoom = MAX(150, iDYRoom);
    }

    [m_pHeat setFrame:[self MakeRectAt:ptHeat DX:iDXSet DY:iDYSet]];
    [m_pCool setFrame:[self MakeRectAt:ptCool DX:iDXSet DY:iDYSet]];
    [m_pRoom setFrame:[self MakeRectAt:ptRoom DX:iDXRoom DY:iDYRoom]];

    self.autoresizingMask = 0xFFFFFFFF;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    if (iDXThis == m_iDXLast && iDYThis == m_iDYLast)
        return;

    if (m_pModePage != NULL)
    {
        int iDYMode = [CMainView DY_TOOLBAR];
        CGRect rThis = self.bounds;
        CGRect rMode = [CRect BreakOffBottom:&rThis DY:iDYMode];
        [CRect BreakOffBottom:&rMode DY:8];
        [m_pModePage setFrame:rMode];
    }

    m_iDXLast = iDXThis;
    m_iDYLast = iDYThis;

    [self UpdateControls];

}
/*============================================================================*/

-(CGRect)MakeRectAt:(CGPoint)ptC DX:(int)iDX DY:(int)iDY

/*============================================================================*/
{
    CGRect rRet = CGRectMake(ptC.x - iDX/2, ptC.y - iDY/2, iDX, iDY);
    return rRet;
}
@end
