//
//  CPhotoView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"

@class CPhoto;
@class CPhotoGridView;

#define STATE_INIT                  0
#define STATE_WAIT                  1
#define STATE_DATAOK                2

/*============================================================================*/

@interface CPhotoView : UIControl < CNSSocketSink >

/*============================================================================*/
{
    CPhotoGridView                  *m_pGridView;
    CPhotoGridView                  *_m_pGridView;
    CPhoto                          *m_pPhoto;

    int                             m_iState;
    
    void*                           *m_pImageData;
    int                             m_iNImageData;
    
    int                             m_iImageDX;
    int                             m_iImageDY;
    
    UIView                          *m_pSubView1;
    UIView                          *m_pSubView2;
}

-(id)initWithFrame:(CGRect)frame GridView:(CPhotoGridView*)pView Photo:(CPhoto*)pPhoto;
-(id)initWithFrame:(CGRect)frame Photo:(CPhoto*)pPhoto Offset:(int) iOffset;
-(BOOL)OnIdleVisible;
-(void)ReleaseImage;
-(void)DoLayout;
-(void)SetContents:(CPhotoView*)pSource;
-(CPhoto*)Photo;
-(void)OffsetPhotoIndex:(int)iOffset;
-(BOOL)IsFirstImage;
-(BOOL)IsLastImage;


@end
