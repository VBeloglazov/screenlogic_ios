/*
--------------------------------------------------------------------------------

    SOCKSINK.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlserver.h"
#include "hlsock.h"
#include "main.h"

#include "socksink.h"


#ifdef MULTI_SERVER
/*====================================================================*/

    CSocketSink::CSocketSink(
    CHLServer                   *pServer )

/*====================================================================*/
#else
/*====================================================================*/

    CSocketSink::CSocketSink()

/*====================================================================*/
#endif
{
    #ifdef MULTI_SERVER
        g_pHLServer = pServer;
    #endif

    #ifdef MAC_OS
        m_pNSSink = NULL;
    #endif

    if (g_pHLServer != NULL)
        m_shSenderID = g_pHLServer->GenerateSenderID();
    m_bWaitResponse = FALSE;
}
#ifdef MULTI_SERVER
/*============================================================================*/

    CSocketSink::CSocketSink(
    CHLServer                   *pServer,
    short                       shSenderID )

/*============================================================================*/
#else
/*============================================================================*/

    CSocketSink::CSocketSink(
    short                       shSenderID )

/*============================================================================*/
#endif
{
    #ifdef MULTI_SERVER
        g_pHLServer = pServer;
    #endif
    
    #ifdef MAC_OS
        m_pNSSink = NULL;
    #endif


    m_shSenderID = shSenderID;
    m_bWaitResponse = FALSE;
}
#ifdef MAC_OS
/*====================================================================*/

    CSocketSink::CSocketSink(
    void                        *pNSSink)

/*====================================================================*/
{
    m_pNSSink = pNSSink;

    if (g_pHLServer != NULL)
        m_shSenderID = g_pHLServer->GenerateSenderID();
    m_bWaitResponse = FALSE;
}
#endif
/*============================================================================*/

    CSocketSink::~CSocketSink()

/*============================================================================*/
{
}
/*============================================================================*/

    short                       CSocketSink::SenderID()

/*============================================================================*/
{
    return m_shSenderID;
}
/*============================================================================*/

    void                        CSocketSink::SenderID(
    short                       shNew )

/*============================================================================*/
{
    m_shSenderID = shNew;
}
/*============================================================================*/

    void                        CSocketSink::RegenSenderID()

/*============================================================================*/
{
    m_shSenderID = g_pHLServer->GenerateSenderID();
}
/*============================================================================*/

    void                        CSocketSink::SwapID(
    CSocketSink                 *pOther )

/*============================================================================*/
{
    if (pOther == NULL)
        return;

    short shTemp = pOther->SenderID();
    pOther->SenderID(m_shSenderID);
    SenderID(shTemp);
}
/*============================================================================*/

    void                        CSocketSink::SinkMessage(
    CHLMSG                      *pMSG )

/*============================================================================*/
{
    #ifdef MAC_OS
        if (m_pNSSink != NULL)
        {
            HLSinkMessage(m_pNSSink, SenderID(), pMSG);
        }
    #endif
}
/*============================================================================*/

    void                        CSocketSink::ConnectionChanged(
    BOOL                        bNowConnected )

/*============================================================================*/
{
    #ifdef MAC_OS
        if (m_pNSSink != NULL)
        {
            HLConnectionChanged(m_pNSSink, SenderID(), bNowConnected);
            //m_pNSSink = NULL;
        }
    #endif
}

