/*
--------------------------------------------------------------------------------

    SHADEPOLY.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

Coordinates when creating CShadePolys are client X,Y (increasing Y down). Angles
are degrees, CCW positive, and 0 pointing East.
--------------------------------------------------------------------------------
*/
#ifndef SHADEPOLY_H
#define SHADEPOLY_H

#include "hlrect.h"
#include "dibdc.h"
#include "hloblist.h"

class CFillRect;
class CShadeNode;
class CHLWindow;
class CHLDC;
class CHLDibDC;

#define  SHADEPOLY_VERYBRIGHT   2500
#define  SHADEPOLY_BRIGHT       5000
#define  SHADEPOLY_NORMAL       10000

#define ANTIALIAS_DECREASING    

/*========================================================================*/

    class CEdgeList

/*========================================================================*/
{
public:
    enum EDGE_TYPE
    {
        EDGE_HORIZONTAL,
        EDGE_VERTICAL
    };

protected:
    EDGE_TYPE                   m_Type;    
    int                         *m_piX;
    int                         *m_piY;
    int                         *m_piD;
    int                         m_iNPts;
    int                         m_iNAlloc;


public:
    CEdgeList(EDGE_TYPE         Type,
    int                         iBufferSize = 0 );

    CEdgeList(CEdgeList         *pOther);

    virtual ~CEdgeList();

    inline EDGE_TYPE            Type()          { return m_Type;            }
    inline int                  X(int iIndex)   { return m_piX[iIndex];     }
    inline int                  Y(int iIndex)   { return m_piY[iIndex];     }
    inline int                  D(int iIndex)   { return m_piD[iIndex];     }
    inline int                  NPoints()       { return m_iNPts;           }

    CHLRect                     GetBounds();

    void                        AddEdge(
    int                         iX,
    int                         iY,
    int                         iD  );

    void                        CreateForCorner(
    int                         iCorner,
    int                         iRad,
    int                         iBoxDim,
    int                         iRadiusType,
    BOOL                        bFill = TRUE );

    void                        CreateForCornerInt(
    int                         iCorner,
    int                         iRad,
    int                         iDX,
    int                         iDY,
    int                         iRadiusType );

    void                        CreateForRoundRectExt(
    int                         iDX,
    int                         iDY,
    int                         iInset,
    int                         iRad,
    int                         iDir );

    void                        CreateForRoundRectInt(
    int                         iDX,
    int                         iDY,
    int                         iInset,
    int                         iRad,
    int                         iDir,
    int                         iRadiusType = RADIUS_ON);

    void                        CreateForArrow(
    int                         iDX,
    int                         iDY,
    int                         iRadMajor,
    int                         iRadMinor,
    int                         iDirection,
    int                         iNestSize );

    int                         IndexOfY(int iY);

    void                        Translate(
    int                         iDX,
    int                         iDY );

    void                        Flip(
    int                         iDX,
    int                         iDY,
    BOOL                        bHz,
    BOOL                        bVt );
    
    BOOL                        ContainsPoint(
    int                         iX,
    int                         iY );
};
/*========================================================================*/

    class CPointList

/*========================================================================*/
{
protected:
    int                         *m_piX;
    int                         *m_piY;
    int                         m_iNPts;
    int                         m_iNAlloc;

public:
    CPointList();
    virtual ~CPointList();


    int                         X(int iIndex);
    void                        X(int iIndex, int iNew);
    int                         Y(int iIndex);
    void                        Y(int iIndex, int iNew);

    int                         XatY(int iY);

    int                         NPoints();

    void                        RemoveAll();

    void                        RemoveLast();

    void                        AddPoint(
    int                         iX,
    int                         iY );

    void                        InsertPointY(
    int                         iX,
    int                         iY );

    void                        RemoveCommonPoints();
    void                        RemoveAtIndex(int iIndex);
};

#endif

