//
//  PoolColorLightsView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "HLView.h"
#import "NSSocketSink.h"

@class CNSPoolConfig;
@class CHLButton;

/*============================================================================*/

@interface CPoolColorLightsView : UIView < CSink, CHLCommandResponder, CNSSocketSink >

/*============================================================================*/
{
    CNSPoolConfig               *m_pConfig;
    UIProgressView              *m_pProgress;
    UILabel                     *m_pLabel;
    CHLButton                   *m_pButtons[5];
}

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig;

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID;

@end
