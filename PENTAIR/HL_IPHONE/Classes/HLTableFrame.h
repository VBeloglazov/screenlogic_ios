//
//  HLTableFrame.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTableTitleHeaderView;

/*============================================================================*/

@interface CHLTableFrame : UIView 

/*============================================================================*/
{
    UITableView                 *m_pTableView;
    UIView                      *m_pView;
    UILabel                     *m_pEmptyLabel;
    int                         m_iRadius;
}

-(id)initWithFrame:(CGRect)rFrame DataSource:(id)pSrc Delegate:(id)pDelegate;
-(id)initWithFrame:(CGRect)rFrame View:(UIView*)pView;
-(void)ReloadData;
-(void)ClearEmpty;
-(void)SetEmpty:(NSString*)pText;
-(UITableView*)TableView;
-(int)Radius;
-(void)Radius:(int)iNew;

@end
