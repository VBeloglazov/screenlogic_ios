//
//  XPAD_VIEWAppDelegate.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//


#import "IPOD_ViewController.h"
#import "AppDelegate.h"
#import "tconnect.h"
#import "hlcomm.h"
#import "nsquery.h"
#import "NSMSG.h"
#import "maintab.h"
#import "MainTabCell.h"
#import "MainTabVC.h"
#import "SystemMode.h"
#import "hlbutton.h"
#import "SubTab.h"
#import "SubTabCell.h"
#import "HVACMainTabVC.h"
#import "HLToolBar.h"
#import <UIKit/UINavigationBar.h>
#import "MainView.h"
#import "crect.h"
#import "WeatherHeaderView.h"
#import "shader.h"
#import "CustomPage.h"

#ifdef PENTAIR
    #import "NSPoolConfig.h"
    #import "PoolAccessoryView.h"
#endif

#define MAX_SUBTAB_DIRECTNAV        1

#ifdef PENTAIR
    #define DX_ACCESSORY            80
    #define TABLEVIEW_INSETS        4
#else
    #define DX_ACCESSORY            120
    #define TABLEVIEW_INSETS        8
#endif

@implementation CIPODTableView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Controller:(IPOD_ViewController*)pController

/*============================================================================*/
{
    self = [super initWithFrame:rFrame style:UITableViewStylePlain];
    if (self)
    {
        m_pController = pController;
    }
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    if (self.bounds.size.width != m_iDXLast || self.bounds.size.height != m_iDYLast)
    {
        [m_pController TableViewResize];
        m_iDXLast = self.bounds.size.width;
        m_iDYLast = self.bounds.size.height;
    }
    [super layoutSubviews];
}
@end

@implementation CIPODMainView
/*============================================================================*/

-(void)SetToolBar:(UIView*)pView

/*============================================================================*/
{
    m_pToolBar = pView;
}
/*============================================================================*/

-(void)SetContent:(UIView*)pView

/*============================================================================*/
{
    m_pContent = pView;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rToolBar;
    if (rThis.size.width > rThis.size.height)
    {
        rToolBar = CGRectMake(0, -[CMainView DY_TOOLBAR_MAIN], rThis.size.width, [CMainView DY_TOOLBAR_MAIN]);
    }
    else
    {
        rToolBar = [CRect BreakOffTop:&rThis DY:[CMainView DY_TOOLBAR_MAIN]];
    }
    //DL - controls initial top margin for Main menu bar
    rToolBar.origin.y += 20;
    rToolBar.size.height -= 20;
    
    [m_pToolBar setFrame:rToolBar];
    //DL - this controls main table top margin
    //rThis.origin.y += 20;
    [m_pContent setFrame:rThis];
}
@end

@implementation IPOD_ViewController

static IPOD_ViewController          *g_pController = NULL;

@synthesize m_pMainTabs, m_pTableView;


/*============================================================================*/

- (void)awakeFromNib

/*============================================================================*/
{
    m_iMainTabHeight    = [CMainView DY_MAX_MAINTABCELL];
    m_bInit             = TRUE;
    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(CheckConnectTimer) userInfo:nil repeats:YES] retain];
	self.m_pMainTabs = [[NSMutableArray alloc] init];


    CIPODMainView *pMain = [[CIPODMainView alloc]init];
    [self setView:pMain];
    [self.navigationController setNavigationBarHidden:YES];
    [pMain setOpaque:NO];
    [pMain setBackgroundColor:[UIColor clearColor]];

    CGRect rTableView = CGRectMake(0, [CMainView DY_TOOLBAR_MAIN], self.view.frame.size.width, self.view.frame.size.height-[CMainView DY_TOOLBAR_MAIN]);
    m_pTableView = [[CIPODTableView alloc] initWithFrame:rTableView Controller:self] ;

    // set the tableview delegate to this object and the datasource to the datasource which has already been set
    m_pTableView.delegate = self;
    m_pTableView.dataSource = self;
    m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIScrollView *pScroller = m_pTableView;
    pScroller.delegate = self;

    m_pTableView.backgroundColor = [UIColor clearColor];
    [pMain addSubview:m_pTableView];
    [pMain SetContent:m_pTableView];


    [self.view setOpaque:NO];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationController setDelegate:self];
    
    
    g_pController = self;
}
/*============================================================================*/

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated

/*============================================================================*/
{
    if (viewController == self)
    {
        if (m_pToolBar == NULL)
        {
            CIPODMainView *pMain = (CIPODMainView*)self.view;
            CGRect rToolbarTop = CGRectMake(0, 0, 320, [CMainView DY_TOOLBAR_MAIN]);
            
            m_pToolBar = [[CHLToolBar alloc] initWithFrame:rToolbarTop Orientation:TOOLBAR_TOP];
            [pMain SetToolBar:m_pToolBar];
            [pMain addSubview:m_pToolBar];

            CGRect rHeader1 = m_pToolBar.bounds;
            //DL
            //[CRect Inset:&rHeader1 DX:8 DY:0];
            [CRect Inset:&rHeader1 DX:70 DY:0];
            [CRect CenterDownToY:&rHeader1 DY:(12+32)];
            CGRect rHeader2 = [CRect BreakOffBottom:&rHeader1 DY:12];

            //DL
            //Fish icon
            fishImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 6, 40, 40)];
            
            //add long press recognizer since the image will overlap the bar, it needs it too.
            //the selector is the same (function) as it is set by the bar
            [fishImage setUserInteractionEnabled:true];
            UILongPressGestureRecognizer *longPressGesture = [[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)] autorelease];
            [longPressGesture setMinimumPressDuration:2];
            [fishImage addGestureRecognizer:longPressGesture];
            
            //image will be assigned later on
            //fishImage.image = [UIImage imageNamed:@"PWPS_fish_76x76.png"];
            [m_pToolBar addSubview:fishImage];
            [fishImage release];
            
            rightBorder = [[UIView alloc] initWithFrame:CGRectMake(58, 2, 1, 48)];
            //[rightBorder setBackgroundColor:[UIColor grayColor]];
            [m_pToolBar addSubview:rightBorder];
            
            //Main Menu
            m_pTitleView = [[UILabel alloc] initWithFrame:rHeader1];
            [m_pTitleView setText:@""];
            [CCustomPage InitStandardLabel:m_pTitleView Size:32];
            m_pTitleView.textAlignment = UITextAlignmentLeft;
            [m_pToolBar addSubview:m_pTitleView];
            [m_pTitleView release];
            
            //System name
            m_pInfoText = [[UILabel alloc] initWithFrame:rHeader2];
            [CCustomPage InitStandardLabel:m_pInfoText Size:12];
            m_pInfoText.lineBreakMode = UILineBreakModeWordWrap;
            m_pInfoText.numberOfLines = 3;
            m_pInfoText.textAlignment = UITextAlignmentLeft;
            [m_pToolBar addSubview:m_pInfoText];
            [m_pInfoText release];
        }
    }
}
/*============================================================================*/

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated

/*============================================================================*/
{
    if (viewController == self)
    {
        NSIndexPath *tableSelection = [m_pTableView indexPathForSelectedRow];
        [m_pTableView deselectRowAtIndexPath:tableSelection animated:YES];

        if (m_pExpandedTab != NULL)
        {
            int iIndex = [self FindMainTabIndex:m_pExpandedTab];
            NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:0];
            [m_pTableView selectRowAtIndexPath:pPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        }
    }
}
/*============================================================================*/

-(NSUInteger)supportedInterfaceOrientations

/*============================================================================*/
{
    int iRet = 0;
    iRet |= UIInterfaceOrientationMaskPortrait;
    iRet |= UIInterfaceOrientationMaskPortraitUpsideDown;
    return 0;
    //return UIInterfaceOrientationMaskPortrait;
}

/*============================================================================*/

- (BOOL)shouldAutorotate

/*============================================================================*/
{
    return NO;
}
/*============================================================================*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 

/*============================================================================*/
{
    if (m_pConnectPage != NULL)
    {
        return FALSE;
    }
	return (interfaceOrientation == UIInterfaceOrientationMaskPortrait);
   
}
/*============================================================================*/

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
    double dAngle = 0.0;
    switch (interfaceOrientation)
    {
    case UIDeviceOrientationPortrait:
    case UIDeviceOrientationPortraitUpsideDown:
        dAngle = 0.0;
        break;
        
    default:
        {
            dAngle = 3.14159/2.0;
        }
        break;
    }

    CGRect rFrame = [CRect ScreenRect:interfaceOrientation];
    m_pBackground.transform = CGAffineTransformMakeRotation(dAngle);
    m_pBackground.frame = rFrame;
}
/*============================================================================*/

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
//    [self SetExpandedMode:(m_pExpandedTab != NULL)];
}
/*============================================================================*/

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation

/*============================================================================*/
{
/*
    if (self.interfaceOrientation == UIDeviceOrientationPortrait)
    {
        UIEdgeInsets insets = m_pTableView.contentInset;
        CGPoint ptOrg = CGPointMake(0, -insets.top);
        [m_pTableView setContentOffset:ptOrg animated:YES];
    }
    [self SetExpandedMode:FALSE];
    [self SetExpandedMode:(m_pExpandedTab != NULL)];
*/
}
/*============================================================================*/

- (void)didReceiveMemoryWarning 

/*============================================================================*/
{
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pTimer invalidate];

    [m_pConnectPage release];
    [m_pTimer release];
    [m_pTableView release];
	[m_pMainTabs release];
    [m_pLockView release];
    [m_pToolBar release];
	[super dealloc];    
}

/*============================================================================*/

-(void)CheckConnectTimer

/*============================================================================*/
{
    if (m_bInit)
    {
        [CHLComm AddSocketSink:self];
        m_bInit = FALSE;
    }

    if (m_pConnectPage == NULL)
    {
        if (![CHLComm Connected] && ![CHLComm IsConnecting])
        {
            [self ShowConnectPage];
        }
    }
    else
    {
        if ([CHLComm Connected] && ![CHLComm IsConnecting])
        {
            [self HideConnectPage];
        }
    }
}
/*============================================================================*/

-(void)ShowConnectPage2

/*============================================================================*/
{
    //DL make sure Connection page will go into Remote mode
    NSUserDefaults *pDefaults = [NSUserDefaults standardUserDefaults];
    [pDefaults setObject:@"REMOTE" forKey:@"HLMode"];
    //DL - the order is critical - change then disconnect (not like in iPad)
    //this is not exactly true - need to check.
    [self ConnectionChanged:true];
    [CHLComm Disconnect];
    [self ShowConnectPage];
}
/*============================================================================*/

- (void)ShowConnectPage

/*============================================================================*/
{
    if (m_pConnectPage != NULL)
        return;

//    [[self navigationController] popToRootViewControllerAnimated:NO];
    NSString *psNIB = @"tconnect";
    switch (self.interfaceOrientation)
    {
    case UIInterfaceOrientationLandscapeLeft:
    case UIInterfaceOrientationLandscapeRight:
        psNIB = @"tconnect_ls";
        break;

    case UIInterfaceOrientationPortrait:
    case UIInterfaceOrientationPortraitUpsideDown:
        break;
    }

    m_pConnectPage = [[CConnectPage alloc] initWithNibName:psNIB bundle:nil Orientation:self.interfaceOrientation];
    m_pConnectPage.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];

    UIView *pView = self.navigationController.topViewController.view;


	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];

    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:pView cache:NO];
    [pView addSubview:m_pConnectPage.view];
    [m_pConnectPage.view setAlpha:1.0];
    [m_pTableView setAlpha:0.0];
    [m_pToolBar setAlpha:0.0];

	[UIView commitAnimations];
}
/*============================================================================*/

- (void)HideConnectPage

/*============================================================================*/
{
    if (m_pConnectPage == NULL)
        return;
    [m_pConnectPage ReleaseAll];

	// present page six as a modal child or overlay view
    CGRect rTableView = m_pTableView.frame;
    rTableView.origin.y = -rTableView.size.height;
    m_pTableView.frame = rTableView;

    UIView *pView = self.navigationController.topViewController.view;

	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:pView cache:NO];
    [m_pConnectPage.view removeFromSuperview];
    CGRect rFrame = m_pToolBar.frame;
    rFrame.origin.y = 0;
    [m_pToolBar setFrame:rFrame];
    [m_pTableView setAlpha:1.0];
    [m_pToolBar setAlpha:1.0];

    rTableView.origin.y = [CMainView DY_TOOLBAR_MAIN];
    m_pTableView.frame = rTableView;

	[UIView commitAnimations];


	[[self navigationController] dismissModalViewControllerAnimated:YES];
    [m_pConnectPage release];
    m_pConnectPage = nil;
    
    //DL
    //self.view.frame = CGRectMake(0,20,self.view.frame.size.width, self.view.frame.size.height - 20);
}
#pragma mark UINavigationBar delegates
/*============================================================================*/

/*============================================================================*/

#pragma mark UIViewController delegates

/*============================================================================*/

- (void)viewWillAppear:(BOOL)animated

/*============================================================================*/
{
}
/*============================================================================*/

- (void)viewDidAppear:(BOOL)animated

/*============================================================================*/
{
}


#pragma mark UITableView delegates
/*============================================================================*/

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate

/*============================================================================*/
{
    printf("End Dragging\n");
    if (m_pExpandedTab != NULL)
    {
        int iIndex = [self FindMainTabIndex:m_pExpandedTab];
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:0];
        [m_pTableView selectRowAtIndexPath:pPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    }
}
/*============================================================================*/

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView

/*============================================================================*/
{
    printf("End Decel\n");
    if (m_pExpandedTab != NULL)
    {
        int iIndex = [self FindMainTabIndex:m_pExpandedTab];
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:0];
        //DL - A - not sure
        [m_pTableView selectRowAtIndexPath:pPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    }
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    [pDelegate PlaySoundButtonPress];

    int iIndex = indexPath.row + 1;
    int iMainTabIndex = 0;
    while (iIndex > 0)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iMainTabIndex];
        [pMainTab LoadConfig];
        iIndex--;
        if (iIndex == 0)
        {
            printf("Hit Main Tab\n");

            // This is our main tab
            if ([pMainTab IsExpanded])
            {
                CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:indexPath];
                if (pMainTabCell != NULL)
                    [pMainTabCell SetHighlighted:FALSE];

                m_pExpandedTab = NULL;
                [m_pTableView beginUpdates];
                [self SetExpandedMode:FALSE];
                [self SetExpanded:pMainTab Expanded:FALSE MainTabIndex:indexPath.row];
                [m_pTableView endUpdates];
                [m_pTableView deselectRowAtIndexPath:indexPath animated:NO];
            }
            else
            {
                CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:indexPath];
                if (pMainTabCell != NULL)
                {
                    [pMainTabCell SetHighlighted:TRUE];
                }

                [m_pTableView beginUpdates];
                CMainTab *pLastMain = m_pExpandedTab;
                m_pExpandedTab = pMainTab;
                if ([pMainTab NSubTabs] <= MAX_SUBTAB_DIRECTNAV)
                {
                    if (pLastMain != NULL)
                        [self SetExpandedMode:FALSE];
                }

                m_pExpandedTab = pLastMain;

//                [m_pTableView deselectRowAtIndexPath:indexPath animated:NO];
                int iThisIndex = indexPath.row;
                if (m_pExpandedTab != NULL)
                {
                    NSIndexPath *pPath = [NSIndexPath indexPathForRow:m_iExpandedTabIndex inSection:0];
                    CMainTabCell *pLastCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:pPath];
                    [pLastCell SetHighlighted:FALSE];
                    [pLastCell setSelected:FALSE];

                    [m_pTableView deselectRowAtIndexPath:pPath animated:YES];
                    [self SetExpanded:m_pExpandedTab Expanded:FALSE MainTabIndex:m_iExpandedTabIndex];
                    iThisIndex = [self FindMainTabIndex:pMainTab];
                }

                if ([pMainTab NSubTabs] <= MAX_SUBTAB_DIRECTNAV && [pMainTab TabID] != TAB_HOME)
                {
                    [pMainTabCell SetHighlighted:FALSE];
                    [self ShowSystemPage:pMainTab SubTabIndex:0];
                    NSIndexPath *pPath = [NSIndexPath indexPathForRow:iThisIndex inSection:0];
                    [m_pTableView endUpdates];
                    [m_pTableView selectRowAtIndexPath:pPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                    m_pExpandedTab = NULL;
                    return;
                }

                [self SetExpanded:pMainTab Expanded:TRUE MainTabIndex:iThisIndex];
                m_pExpandedTab = pMainTab;
                [self SetExpandedMode:TRUE];
                m_iExpandedTabIndex = iThisIndex;
                NSIndexPath *pPath = [NSIndexPath indexPathForRow:iThisIndex inSection:0];

                [m_pTableView endUpdates];
                [m_pTableView selectRowAtIndexPath:pPath animated:YES scrollPosition:UITableViewScrollPositionNone];

            }

            return;
        }
        else
        {
            if ([pMainTab IsExpanded])
            {
                int iSubTabIndex = 0;
                while (iIndex > 0 && iSubTabIndex < [pMainTab NSubTabs])
                {
                    iIndex--;
                    if (iIndex == 0)
                    {
                        // Hit this Sub Tab
                        printf("Hit Sub Tab\n");
                        [self ShowSystemPage:pMainTab SubTabIndex:iSubTabIndex];
                    }
                    iSubTabIndex++;
                }
            }
        }
        
        iMainTabIndex++;
    }
}


#pragma mark UITableView datasource methods
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

/*============================================================================*/
{
	return 1;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

/*============================================================================*/
{
    int iNSub = 0;
    for (int i = 0; i < [m_pMainTabs count]; i++)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:i];
        if ([pMainTab IsExpanded])
            iNSub += [pMainTab NSubTabs];
    }

	int iNMain  = [m_pMainTabs count];
    return iNMain + iNSub;
}
/*============================================================================*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int iIndex = indexPath.row + 1;
    int iMainTabIndex = 0;
    while (iIndex > 0)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iMainTabIndex];
        [pMainTab LoadConfig];
        iIndex--;
        if (iIndex == 0)
        {
            // This is our main tab
            return m_iMainTabHeight;
        }
        else
        {
            if ([pMainTab IsExpanded])
            {
                int iSubTabIndex = 0;
                while (iIndex > 0 && iSubTabIndex < [pMainTab NSubTabs])
                {
                    iIndex--;
                    iSubTabIndex++;
                    if (iIndex == 0)
                    {
                        // Hit this Sub Tab
                        return [CMainView DY_SUBTABCELL];
                    }
                }
            }
        }
        
        iMainTabIndex++;
    }

    return 32;
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int iIndex = indexPath.row + 1;
    int iMainTabIndex = 0;
    while (iIndex > 0)
    {
        if (iMainTabIndex >= [m_pMainTabs count])
        {
            UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"TEMP"] autorelease];
            //UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"TEMP"] autorelease];
            return cell;
        }

        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iMainTabIndex];
        [pMainTab LoadConfig];
        iIndex--;
        if (iIndex == 0)
        {
            // This is our main tab
            CMainTabCell *cell = (CMainTabCell*)[tableView dequeueReusableCellWithIdentifier:pMainTab.m_pName];
            if (cell == nil)
            {
                cell = [[[CMainTabCell alloc] initWithFrame:CGRectZero reuseIdentifier:pMainTab.m_pName] autorelease];
                //cell = [[[CMainTabCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:pMainTab.m_pName] autorelease];
                //[cell setBackgroundColor:[UIColor greenColor]];
            }

            // get the view controller's info dictionary based on the indexPath's row
            [cell SetMainTab:[m_pMainTabs objectAtIndex:iMainTabIndex]];
            UIView *pAccessory = [pMainTab CreateAccessoryView];
            [cell SetAccessoryView:pAccessory];

            if ([pMainTab IsExpanded])
            {
                [cell SetHighlighted:TRUE];
            }
            else
            {
                if ([pMainTab NSubTabs] > MAX_SUBTAB_DIRECTNAV)
                    [cell SetHighlighted:FALSE];
                else
                    [cell SetHighlighted:FALSE];
            }
            
            return cell;
        }
        else
        {
            if ([pMainTab IsExpanded])
            {
                int iSubTabIndex = 0;
                while (iIndex > 0 && iSubTabIndex < [pMainTab NSubTabs])
                {
                    iIndex--;
                    if (iIndex == 0)
                    {
                        // Hit this Sub Tab
                        UITableViewCell *pCell = [pMainTab CellForSubTabIndex:tableView Index:iSubTabIndex];
                        [pCell.contentView setAlpha:0.0];

                        [UIView beginAnimations:nil context:nil];
                        [UIView setAnimationDuration:0.5];
                        [pCell.contentView setAlpha:1.0];
                        [UIView commitAnimations];

                        [pCell setSelected:FALSE];
                        return pCell;
                    }
                    iSubTabIndex++;
                }
            }
        }
        
        iMainTabIndex++;
    }
  
    return NULL;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        [self HideConnectPage];
        [m_pTitleView setText:@"Main Menu"];
        fishImage.image = [UIImage imageNamed:@"PWPS_fish_76x76.png"];
        [rightBorder setBackgroundColor:[UIColor grayColor]];

        CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;



        //
        // g! Mode
        //

        CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETUSERINFOQ];
        if ([pQ1 AskQuestion])
        {
            [pQ1 GetInt]; // Base Port
            [pQ1 GetInt]; // External Port
            NSString *pSysName = [pQ1 GetString];

            // More stuff here we don't care about now
            if ([pSysName isEqual:[m_pInfoText text]])
            {
                //DL - status bar visible
                //even though the connection is the same...
                //if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
                {
                    UIView *addStausbar = [[UIView alloc] init];
                    addStausbar.frame = CGRectMake(0,0,self.view.frame.size.width,20);
                    addStausbar.backgroundColor = [UIColor colorWithWhite:200.0 alpha:0.5];
                    [self.view addSubview:addStausbar];
                }
                
                [pQ1 release];
                return;
            }

            //DL - try to get nickName from respective controller descriptor (if found)
            DBManager *dbManager = [DBManager getSharedInstance];
            ControllerDescriptor *cd = [[ControllerDescriptor alloc] init];
            cd.systemName = pSysName;
            if([dbManager getControllerByName:cd])
            {
                [m_pInfoText setText:cd.nickName];
            }
            else
            {
                [m_pInfoText setText:pSysName];
            }
            [cd release];
          
            
            // Remove any pages that may be pushed up 
            [[self navigationController] popToRootViewControllerAnimated:NO];

            // Remove all of our data
            [m_pMainTabs removeAllObjects];
            [m_pTableView reloadData];

            m_pExpandedTab = NULL;
            m_iExpandedTabIndex = 0;
        }

        [m_pToolBar Invalidate];
        [pDelegate InitSounds];

#ifdef PENTAIR
        //
        // Pentair Mode
        //
        //[self SetBackgroundImage:2000];

        CNSQuery *pQVer = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETVERSIONQ];
        if ([pQVer AskQuestion])
        {
            [pQVer GetString];
            [pQVer GetInt];
            int iVer1 = [pQVer GetInt];
            int iVer2 = [pQVer GetInt];
            [CHLComm SetVersion:iVer1 Sub:iVer2];
        }

        [pQVer release];
        [m_pMainTabs removeAllObjects];
        
        //DL - trying to avoid recreation, keep old one
        //it is not good since it remembers the very first configuration
        if(NULL == [pDelegate PoolConfig])
        {
            //DL at this point PoolConfig will be killed and recreated
            //nobody will be ntotified that the previous instance is not valid
            //anymore - deallocated
            [pDelegate InitPoolConfig];
        }
        else
        {
            [pDelegate RestartPoolConfig];
        }
        CNSPoolConfig *pConfig = [pDelegate PoolConfig];

        //DL here is somehow previously allocated pConfig needs to be nullified!
        //because next statement still thinks it exists
        //also find where did get released, who did it?
        
       if (m_pHeaderAccessory != NULL)
       {
           //here is CNSPoolConfig object has to be nullified inside m_pHeaderAccessory first
           //CPoolAccessoryView *cp = m_pHeaderAccessory;
           //[cp killPoolConfig];
           [m_pHeaderAccessory removeFromSuperview];
           m_pHeaderAccessory = NULL;
       }

        //[CHLComm RemoveAllSocketSinks];
        
        
        CGRect rToolBar = m_pToolBar.bounds;
        //DL - this controls vetical size for weather
        rToolBar.size.height += 20;
        
        CGRect rAcc = [CRect BreakOffRight:&rToolBar DX:DX_ACCESSORY];
        //DL - this controls vetical shift for weather iPhone
        //rAcc.origin.y += 20;
        
        m_pHeaderAccessory = [[CPoolAccessoryView alloc] initWithFrame:rAcc Config:pConfig Type:TYPE_AIRTEMP];
        [m_pToolBar addSubview:m_pHeaderAccessory];
        [m_pHeaderAccessory release];
 #else
        if (m_pHeaderAccessory != NULL)
        {
            [m_pHeaderAccessory removeFromSuperview];
            m_pHeaderAccessory = NULL;
        }

        CGRect rToolBar = m_pToolBar.bounds;
        CGRect rAcc = [CRect BreakOffRight:&rToolBar DX:DX_ACCESSORY];
        m_pHeaderAccessory = [[CWeatherHeaderView alloc] initWithFrame:rAcc Forecast:[pDelegate Forecast]];

//        m_pHeaderAccessory = [[CPoolAccessoryView alloc] initWithFrame:rAcc Config:pConfig Type:TYPE_AIRTEMP];
        [m_pToolBar addSubview:m_pHeaderAccessory];
        [m_pHeaderAccessory release];
 #endif

        CSystemModeList *pList =  [pDelegate SysModeList];
        [pList RemoveSink:self];
        pList = [pDelegate InitSysModeList];
        [pList AddSink:self];
        
        [pQ1 release];

        [m_pMainTabs removeAllObjects];

#ifdef PENTAIR
        // Add Pentair Stuff at the top
        [pConfig AllocMainTabs:m_pMainTabs];
#endif

        CNSQuery *pQ2 = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETTABCONFIGQ];
        if ([pQ2 AskQuestion])
        {
#ifdef PENTAIR
            [pQ2 GetInt];    // Enable VTabs
            int iNTabs = [pQ2 GetInt];
#else
            int iBGMode = [pQ2 GetInt];    // Enable VTabs
            int iNTabs = [pQ2 GetInt];
            [self SetBackgroundImage:iBGMode];
#endif

            for (int iTab = 0; iTab < iNTabs; iTab++)
            {
                int iTabType = [pQ2 GetInt];
                NSString *pTabName = [pQ2 GetString];
                BOOL bOK = TRUE;

#ifdef PENTAIR
                bOK = FALSE;
                switch (iTabType)
                {
                case TAB_VIDEO:
                case TAB_LIGHTING:
                    bOK = TRUE;
                    break;
                }
#else
                switch (iTabType)
                {
                case TAB_WEB:
                case TAB_TELEPHONE:
                case TAB_IRRIGATION:
                case 13:    // DVR from 4.0
                    bOK = FALSE;
                    break;
                }
#endif

                if (bOK)
                {
                    CMainTab *pTab = [[CMainTab alloc] initWithName:pTabName TabID:iTabType];
                    [pTab SetMainController:(UIViewController *)self];
                    [m_pMainTabs addObject:pTab];
                    [pTab release];
                }


            }
            //DL - this controls main tab vertical size
            int iDY = 440 - TABLEVIEW_INSETS - [CMainView DY_TOOLBAR_MAIN];
            //int iDY = 460 - TABLEVIEW_INSETS - [CMainView DY_TOOLBAR_MAIN];

            m_iMainTabHeight = iDY / MAX(1, [m_pMainTabs count]);
            m_iMainTabHeight = MIN([CMainView DY_MAX_MAINTABCELL], m_iMainTabHeight);

            [self SetExpandedMode:FALSE];
            
            //DL - status bar visible
            if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
            {
                UIView *addStausbar = [[UIView alloc] init];
                addStausbar.frame = CGRectMake(0,0,self.view.frame.size.width,20);
                addStausbar.backgroundColor = [UIColor colorWithWhite:200.0 alpha:0.5];
                [self.view addSubview:addStausbar];
            }
            
            //DL - long press addition
            UILongPressGestureRecognizer *longPressGesture = [[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)] autorelease];
            [longPressGesture setMinimumPressDuration:2];
            [m_pToolBar addGestureRecognizer:longPressGesture];

        }
        [pQ2 release];

        [m_pTableView reloadData];


        CNSQuery *pQLock = [[[CNSQuery alloc] initWithQ:HLM_TABLETLOCKOUTQ] autorelease];
        [pQLock SendMessageWithMyID:self];

        CNSQuery *pQVersion = [[[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETVERSIONQ] autorelease];
        [pQVersion SendMessageWithMyID:self];

        [m_pInfoText setHidden:FALSE];
        [m_pInfoText setAlpha:1.0];

        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDLIBRARYCLIENTQ];
        [pQ autorelease];
        [pQ SendMessage];
    }
    else
    {
        [m_pInfoText setHidden:TRUE];
        [m_pTitleView setText:@"Connecting..."];

    }
}
/*============================================================================*/

-(void)longPress:(UILongPressGestureRecognizer*)gesture

/*============================================================================*/
{
    switch(gesture.state)
    {
        case UIGestureRecognizerStateFailed:
            printf("Long Falied...");
            break;
        case UIGestureRecognizerStateBegan:
            printf("Long Began");
            //DL - do block animation
            [UIView animateWithDuration:0.5 delay:0 options: UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                             animations:^(void)
            {
                //here is the animation (pulse from 1 to 0.3 and back)
                [m_pToolBar setAlpha:0.3];
            }
            completion:^(BOOL finished)
            {
                //restore full alpha when finished
                [m_pToolBar setAlpha:1.0];
                return;
            }];
            break;
        case UIGestureRecognizerStateEnded:
            //printf("Long Ended or Recognized");
            //go back to login screen?
            [m_pToolBar.layer removeAllAnimations];
            [self ShowConnectPage2];
            break;
        case UIGestureRecognizerStateCancelled:
            printf("Long Cancelled");
            break;
        case UIGestureRecognizerStateChanged:
            printf("Long Changed");
            break;
        case UIGestureRecognizerStatePossible:
            printf("Long Possible");
            break;
    }
}

/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_TABLETLOCKOUTA:
        {
            int iLock = [pMSG GetInt];
            if (iLock)
            {
                if (m_pLockView == NULL)
                {
                        m_pLockView = [[UIAlertView alloc]      initWithTitle:@"HomeLogic" 
                                                                message:@"Connection Not Supported by Lock Configuration" 
                                                                delegate:NULL
                                                                cancelButtonTitle:@"Cancel" otherButtonTitles:NULL];
                }
                [m_pLockView show];

                [CHLComm Disconnect];
            }
            else
            {
                if (m_pLockView != NULL)
                {
                    [m_pLockView dismissWithClickedButtonIndex:0 animated:TRUE];
                    [m_pLockView release];
                    m_pLockView = NULL;
                }
            }
        }
        break;

    case HLM_SYSCONFIG_GETVERSIONA:
        {
            [pMSG GetString];
            [pMSG GetInt];
            int iVer1 = [pMSG GetInt];
            int iVer2 = [pMSG GetInt];

            [CHLComm SetVersion:iVer1 Sub:iVer2];
        }
    }
}
/*============================================================================*/

-(CMainTab*)SelectedMainTab

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:pPath.row];
    return pMainTab;
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if ([m_pMainTabs count] == 0)
        return;

    CMainTab *pHomeTab = (CMainTab*)[m_pMainTabs objectAtIndex:0];

    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CSystemModeList *pList =  [pDelegate SysModeList];

    for (int i = 0; i < [pList NModes]; i++)
    {
        CSubTab *pSubTab = [pHomeTab SubTab:i];
        if (pSubTab != NULL)
        {
            CSubTabCell *pSubTabCell = [pSubTab GetSubTabCell];
            if (pSubTabCell != NULL)
            {
                if (i == [pList ActiveModeIndex])
                    [pSubTabCell SetEmbeddedIcon:@"CHECK.png"];
                else
                    [pSubTabCell SetEmbeddedIcon:NULL];
            }
        }
    }
}
/*============================================================================*/

-(void)CollapseAll

/*============================================================================*/
{
    if (m_pExpandedTab == NULL)
        return;
    
    int iIndex = [self FindMainTabIndex:m_pExpandedTab];
    CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:iIndex inSection:0]];
    if (pMainTabCell != NULL)
        [pMainTabCell SetHighlighted:FALSE];

    [self SetExpandedMode:FALSE];
    [self SetExpanded:m_pExpandedTab Expanded:FALSE MainTabIndex:m_iExpandedTabIndex];
    m_pExpandedTab = NULL;

}
/*============================================================================*/

-(void)SetExpandedMode:(BOOL)bNew

/*============================================================================*/
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    UIEdgeInsets insets = m_pTableView.contentInset;


    if (!bNew)
    {
        int iDY = m_pTableView.bounds.size.height;
        int iDYAll = m_iMainTabHeight * [m_pMainTabs count];
        int iPadTop = (iDY - iDYAll)/2;
        iPadTop = MAX(iPadTop, TABLEVIEW_INSETS);
        insets.top = iPadTop;
        insets.bottom = 0;
        [m_pTableView setContentInset:insets];
        [m_pTableView setContentOffset:CGPointMake(0, -insets.top) animated:NO];
    }
    else
    {
        int iMainTabIndex = -1;
        for (int i = 0; i < [m_pMainTabs count] && iMainTabIndex == -1; i++)
        {
            if ([m_pMainTabs objectAtIndex:i] == m_pExpandedTab)
                iMainTabIndex = i;
        }

        if (iMainTabIndex < 0)
            return;

        int iThisTabTop = iMainTabIndex * m_iMainTabHeight;
        int iDYContent = [m_pExpandedTab NSubTabs] * [CMainView DY_SUBTABCELL] + m_iMainTabHeight;
        int iDYTable = m_pTableView.frame.size.height;

        int iOffset = iDYTable / 2 - iDYContent/2 - iThisTabTop;

        if (iDYContent > iDYTable)
        {
            iOffset = -iThisTabTop;
        }

        // We don't want the selected tab to appear "below" where it normally is in the unexpanded list
        int iDYAllDefault = m_iMainTabHeight * [m_pMainTabs count];
        int iOffsetDefault = iDYTable /2 - iDYAllDefault /2;
        iOffset = MIN(iOffset, iOffsetDefault);
    
        iOffset = MAX(iOffset, -iThisTabTop);
        

        int iDYAll = m_iMainTabHeight * [m_pMainTabs count] + [m_pExpandedTab NSubTabs] * [CMainView DY_SUBTABCELL];
        int iPadTop = MAX(0, iDYTable/2-iDYAll/2);
        insets.top = iPadTop;
        insets.bottom = 0;
        CGPoint ptOff = CGPointMake(0, -iOffset);
        [m_pTableView setContentSize:CGSizeMake(m_pTableView.frame.size.width, iDYAll)];

        if (iDYAll < iDYTable)
        {
            if (![self IsOldOS])
            {
                [m_pTableView setContentInset:insets];
                ptOff.y = insets.top;
            }
        }

        if ([self IsOldOS])
            [m_pTableView setContentOffset:ptOff animated:YES];
        else
            [m_pTableView setContentOffset:ptOff animated:NO];
    }


    [UIView commitAnimations];
}
/*============================================================================*/

-(void)SetExpanded:(CMainTab*)pMainTab Expanded:(BOOL)bExpanded MainTabIndex:(int)iMainIndex

/*============================================================================*/
{
    if (bExpanded == [pMainTab IsExpanded])
        return;

    [pMainTab SetExpanded:bExpanded];

    NSMutableArray *pArray = [[NSMutableArray alloc] init];
	[UIView beginAnimations:nil context:nil];
    if (bExpanded)
        [UIView setAnimationDuration:0.5];
    else
        [UIView setAnimationDuration:0.2];

    for (int i = 0; i < [pMainTab NSubTabs]; i++)
    {
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:i+iMainIndex+1 inSection:0];

        UITableViewCell *pCell = [m_pTableView cellForRowAtIndexPath:pPath];
        if (pCell != NULL)
        {
            if (bExpanded)
                [pCell.contentView setAlpha:1.0];
            else 
                [pCell.contentView setAlpha:0.0];

        }

        [pArray addObject:pPath];
    }
    
    [UIView commitAnimations];
    
    if (bExpanded)
        [m_pTableView insertRowsAtIndexPaths:pArray withRowAnimation:UITableViewRowAnimationBottom];
    else
        [m_pTableView deleteRowsAtIndexPaths:pArray withRowAnimation:UITableViewRowAnimationBottom];

    [pArray release];

}
/*============================================================================*/

-(int)FindMainTabIndex:(CMainTab*)pMainTab

/*============================================================================*/
{
    int iIndex = 0;
    for (int i = 0; i < [m_pMainTabs count]; i++)
    {
        CMainTab *pTest = (CMainTab*)[m_pMainTabs objectAtIndex:i];
        if (pTest == pMainTab)
            return iIndex;

        if ([pTest IsExpanded])
            iIndex += (1+[pTest NSubTabs]);
        else
            iIndex++;
    }

    return 0;
}
/*============================================================================*/

-(void)TableViewResize

/*============================================================================*/
{
    [self SetExpandedMode:(m_pExpandedTab != NULL)];
/*
    if (m_pExpandedTab == NULL)
        return;
        
    int iMainTabIndex = -1;
    for (int i = 0; i < [m_pMainTabs count] && iMainTabIndex == -1; i++)
    {
        if ([m_pMainTabs objectAtIndex:i] == m_pExpandedTab)
            iMainTabIndex = i;
    }

    if (iMainTabIndex < 0)
        return;

    int iThisTabTop = iMainTabIndex * m_iMainTabHeight;
    int iDYContent = [m_pExpandedTab NSubTabs] * [CMainView DY_SUBTABCELL] + m_iMainTabHeight;
    int iDYTable = m_pTableView.frame.size.height;

    int iOffset = (iDYTable - iDYContent)/2-iThisTabTop;

    if (iDYContent > iDYTable)
    {
        iOffset = iThisTabTop;
    }

    int iDYAll = m_iMainTabHeight * [m_pMainTabs count] + [m_pExpandedTab NSubTabs] * [CMainView DY_SUBTABCELL];
    int iPadTop = MAX(0, iDYTable/2-iDYAll/2);
    UIEdgeInsets insets = m_pTableView.contentInset;
    insets.top = iPadTop;
    insets.bottom = 0;
    [m_pTableView setContentInset:insets];
    CGPoint ptOff = CGPointMake(0, -iOffset);
    [m_pTableView setContentOffset:ptOff animated:YES];
*/
}
/*============================================================================*/

-(void)SetBackgroundImage:(int)iImage

/*============================================================================*/
{
    [CAppDelegate BackgroundStyle:iImage];


    if (m_pBackground != NULL)
    {
        [m_pBackground removeFromSuperview];
        m_pBackground = NULL;
    }

    UIImage *pImage = NULL;

    switch (iImage)
    {
    case 1000: pImage = [UIImage imageNamed:@"BACKGROUND1.jpg"]; break;
    case 2000: pImage = [UIImage imageNamed:@"BACKGROUND2.png"]; break;

    case 3000:
    case 4000:
        {
            pImage = [UIImage imageNamed:@"IPAD_GRADIENT1.png"];
        }
        break;

    case 5000:
        {
            pImage = [UIImage imageNamed:@"PENTAIR_BACKGROUND.jpg"];
        }
        break;
    }

    if (pImage == NULL)
        return;

    UIViewController *pParent = self.parentViewController;
    m_pBackground = [[UIImageView alloc ]initWithImage:pImage];
    [pParent.view addSubview:m_pBackground];
    [pParent.view sendSubviewToBack:m_pBackground];

    CGRect rMain = [UIScreen mainScreen].bounds;


    [m_pBackground setFrame:rMain];
    [m_pBackground release];
}
/*============================================================================*/

-(void)ShowSystemPage:(CMainTab*)pMainTab SubTabIndex:(int)iSubTabIndex

/*============================================================================*/
{
    // Weird case
    if ([pMainTab TabID] == TAB_HOME)
    {
        CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
        CSystemModeList *pList =  [pDelegate SysModeList];

        [pList SetSystemMode:iSubTabIndex];
        int iIndex = [self FindMainTabIndex:pMainTab];
        [m_pTableView beginUpdates];
        [self SetExpandedMode:FALSE];
        [self SetExpanded:pMainTab Expanded:FALSE MainTabIndex:iIndex];
        m_pExpandedTab = NULL;
        CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:iIndex inSection:0]];
        if (pMainTabCell != NULL)
            [pMainTabCell SetHighlighted:FALSE];
        [m_pTableView endUpdates];
        return;
    }


    [pMainTab SetDefaultTab:iSubTabIndex];
    UIViewController *pView = NULL;
    switch ([pMainTab TabID])
    {
    case TAB_HVAC:
      	pView = [[CHVACMainTabViewController alloc] initWithMainTab:pMainTab Background:m_pBackground];
        break;

    default:
        {
            CMainTabViewController *pMainTabPage = [[CMainTabViewController alloc] initWithMainTab:pMainTab Background:m_pBackground];
            [pMainTabPage SetMainViewController:self];
            pView = pMainTabPage;
            #ifdef PENTAIR
                if ([m_pMainTabs count] < 2)
                {
                    [pMainTabPage EnableHomeButton:FALSE];
                }
            #endif
        }
    }

    if (pView == NULL)
        return;

    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
	
    //DL - A - set to NO!
	// present the rest of the pages normally
	[[self navigationController] pushViewController:pView animated:NO];
    [pView release];
}
/*============================================================================*/

-(BOOL)IsOldOS

/*============================================================================*/
{
    NSString *pVers = [UIDevice currentDevice].systemVersion;
    unichar cCharMajor = [pVers characterAtIndex:0];

    switch (cCharMajor)
    {
    case '2':
    case '3':
        return TRUE;
    }
    return FALSE;
}
/*============================================================================*/

+(UIViewController*)MainViewController

/*============================================================================*/
{
    return g_pController.navigationController.topViewController;
}
@end

