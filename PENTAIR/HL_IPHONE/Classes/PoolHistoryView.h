//
//  PoolHistoryPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/15/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "timeview.h"

@class CNSPoolConfig;

/*============================================================================*/

@interface CPoolHistoryView : CTimeView

/*============================================================================*/
{
    CNSPoolConfig                       *m_pConfig;
}

-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig;

-(int)DataViewDY;

-(void)NotifyRotation;

-(void)InitScaleView;

@end
