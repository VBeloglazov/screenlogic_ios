//
//  AudioServiceMenu.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MP3Menu.h"

/*============================================================================*/

@interface CAudioServiceMenuItem : CMP3MenuItem <CNSSocketSink>

/*============================================================================*/
{
    CMP3Menu                            *m_pMenu;
    int                                 m_iSection;
    int                                 m_iRow;
    int                                 m_iIndex;
    int                                 m_iDataState;
    
    int                                 m_iArtistIndex;
    int                                 m_iAlbumIndex;
    int                                 m_iTrackIndex;
    
    CGImageRef                          m_pImage;
    void                                *m_pJPEG;
}

-(id)initWithData:(NSString*)pName Data:(int)iData Section:(int)iSection Row:(int)iRow Index:(int)iIndex Menu:(CMP3Menu*)pMenu;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;
-(BOOL)OnIdleVisible;

-(CGImageRef)GetImage;

@end
/*============================================================================*/

@interface CAudioServiceMenu : CMP3Menu 

/*============================================================================*/
{
    NSString                                *m_pNavData;
    int                                     m_iActiveObjectID;
}

-(id)initWithFrame:(CGRect)rFrame
             ContentView:(CMP3ContentView*)pContentView
             Zone:(CAudioZone*)pSubTab 
             Type:(int)iType    
             Sub1:(int)iSub1 
             Sub2:(int)iSub2 
             Title:(NSString*)pTitle 
             Service:(CAudioService*)pService
             NavString:(NSString*) pNavString;

-(void)LoadFrom:(CNSMSG*)pMSG;
-(void)Init;
-(void)NavigateFrom:(int)iFrom To:(int)iTo Operation:(int)iOp StringData:(NSString*)pStringData;

-(BOOL)ItemHasSubMenus:(CMP3MenuItem*)pItem;
-(IBAction)SearchEvent:(id)sender;

@end
