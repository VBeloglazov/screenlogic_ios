//
//  MediaArtView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/29/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

/*============================================================================*/

@interface CMediaArtView : UIView 

/*============================================================================*/
{
    UIImage                         *m_pImage;
}

-(UIImage *)reflectedImageRepresentationWithHeight:(NSUInteger)height;
-(void)SetImage:(UIImage*)pImage;
@end


/*============================================================================*/

@interface CMediaArtReflectionView : UIImageView

/*============================================================================*/
{
    CMediaArtView                   *m_pView;
}

-(id)initWithFrame:(CGRect)rFrame MediaArtView:(CMediaArtView*)pView;

@end
