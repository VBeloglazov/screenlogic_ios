//
//  SecurityHistoryDataView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "timedataview.h"
#import "NSSocketSink.h"

@class CSecPartition;

/*============================================================================*/

@interface CSecurityHistoryDataView : CTimeDataView <CNSSocketSink>

/*============================================================================*/
{
    CSecPartition               *m_pPartition;
    NSMutableArray              *m_pRTESeries;
    NSTimer                     *m_pQueryTimer;
}

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView Partition:(CSecPartition*)pPartition LastView:(CSecurityHistoryDataView*)pLastView;

-(NSMutableArray*)RTESeries;
-(void)PostQuery;

@end
