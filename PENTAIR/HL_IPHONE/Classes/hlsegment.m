//
//  hlsegment.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "hlsegment.h"
#import "MainView.h"
#import "crect.h"
#import "HLToolBar.h"
#import "CustomPage.h"
#import "IPAD_ViewController.h"
#import "hlcomm.h"
#import "hlm.h"
#import "shader.h"
#import "AppDelegate.h"
#import "SplitRoundRegion.h"

@implementation CHLSegment

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ImageProvider:(id)pImageProvider Items:(NSArray*)pItems Type:(int)iType Data:(BOOL)bData

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    
    if (self)
    {
        m_iType = iType;
        m_pImageProvider = pImageProvider;
        m_pItems = [[NSMutableArray alloc]init];

        if (iType == HLSEGMENT_BUTTON)
        {
            self.contentMode = UIViewContentModeRedraw;
            m_pImageProvider = self;
        }

        [self InitHighlight];

        for (int i = 0; i < [pItems count]; i++)
        {
            UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            [self addSubview:pLabel];
            pLabel.autoresizingMask = 0xFFFFFFFF;
            [CCustomPage InitStandardLabel:pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
            pLabel.textAlignment = UITextAlignmentCenter;
            pLabel.lineBreakMode = UILineBreakModeWordWrap;
            pLabel.numberOfLines = 2;
            [pLabel setTextColor:[UIColor whiteColor]];
            NSString *pText = NULL;
            
            if (bData)
            {
                CHLSegmentItem *pItem = (CHLSegmentItem*)[pItems objectAtIndex:i];
                m_iData[m_iNData] = [pItem Data];
                m_iNData++;
                pText = [pItem Text];
            }
            else 
            {
                pText = (NSString*)[pItems objectAtIndex:i];
            }


            [m_pItems addObject:pLabel];

            [pLabel setText:pText];
            [pLabel release];

        }
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ImageProvider:(id)pImageProvider Views:(NSArray*)pViews Type:(int)iType

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_iType = iType;
        m_pImageProvider = pImageProvider;

        m_pItems = [[NSMutableArray alloc]init];

        if (iType == HLSEGMENT_BUTTON)
        {
            self.contentMode = UIViewContentModeRedraw;
            m_pImageProvider = self;
        }

        [self InitHighlight];

        for (int i = 0; i < [pViews count]; i++)
        {
            UIView *pView = (UIView*)[pViews objectAtIndex:i];
            [m_pItems addObject:pView];
            [self addSubview:pView];
        }

    }

    return self;
}
/*============================================================================*/

-(void)InitHighlight

/*============================================================================*/
{
    switch (m_iType)
    {
    case HLSEGMENT_PAGE_CTRL:
    case HLSEGMENT_TOOLBAR:
        m_pHL = [[CHLSegmentHighlight alloc] initWithFrame:CGRectZero];
        break;

    case HLSEGMENT_BUTTON:
    case HLSEGMENT_PAGE_CTRL_ROUNDREGION:
        m_pHL = [[UIView alloc] initWithFrame:CGRectZero];
        break;
    }
    [self addSubview:m_pHL];
    [m_pHL release];
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
    [m_pItems release];
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    if (m_iType != HLSEGMENT_BUTTON)
        return;

    CGImageRef pRef = [self GenerateImage:ELEMENT_STD];
    [layer setContents:(id)pRef];
}
/*============================================================================*/

-(CGImageRef)GenerateImage:(int)iType

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;

    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    int iSHIN    = [CHLComm GetInt:INT_BUTTON_SHADEIN];
    int iSHOUT   = [CHLComm GetInt:INT_BUTTON_SHADEOUT];
    int iRad     = [CHLComm GetInt:INT_BUTTON_ROUT];
    int iGradient = [CHLComm GetInt:INT_BUTTON_GRADIENT];
    
    unsigned int rgbT = 0;
    unsigned int rgbB = 0;
    
    switch (iType)
    {
    case ELEMENT_STD:
        {
            rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_BUTTON]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
        }
        break;
    case ELEMENT_CHECK:
        {
            UIColor *prgbFace  = [CHLComm GetRGB:RGB_BUTTON];
            UIColor *prgbCheck = [CHLComm GetRGB:RGB_SELECT_SUBTLE];
            int iOA1 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_TOP];
            int iOA2 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_TOP];
            float fT = (float)iOA1 / 100.0f;
            float fB = (float)iOA2 / 100.0f;
            rgbT = [CShader MakeRGB:[CShader Blend:fT RGB1:prgbCheck RGB2:prgbFace]];
            rgbB = [CShader MakeRGB:[CShader Blend:fB RGB1:prgbCheck RGB2:prgbFace]];
        }
        break;
    }
    

    CGImageRef pRef = [pDelegate GetButtonImage:iDX DY:iDY Rad:iRad SHIN:iSHIN SHOUT:iSHOUT ColorT:rgbT ColorB:rgbB];
    return pRef;
}
/*============================================================================*/

-(int)NItems

/*============================================================================*/
{
    return [m_pItems count];
}
/*============================================================================*/

-(void)AddData:(int)iData

/*============================================================================*/
{
    m_iData[m_iNData] = iData;
    m_iNData++;
}
/*============================================================================*/

-(int)SelectedData

/*============================================================================*/
{
    if (m_iSelectedIndex >= 0 && m_iSelectedIndex < m_iNData)
    {
        return m_iData[m_iSelectedIndex];
    }
    return 0;
}
/*============================================================================*/

-(void)SetSelectedIndexByData:(int)iData

/*============================================================================*/
{
    for (int i = 0; i < m_iNData; i++)
    {
        if (m_iData[i] == iData)
        {
            [self SetSelectedIndex:i];
            return;
        }
    }

    [self SetSelectedIndex:-1];
}
/*============================================================================*/

-(void)SetSelectedIndex:(int)iIndex

/*============================================================================*/
{
    if (m_iSelectedIndex == iIndex)
        return;
    m_iSelectedIndex = iIndex;
    
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    if (iIndex < 0)
        [m_pHL setAlpha:0];
    else 
        [m_pHL setAlpha:1];

    iIndex = MAX(0, iIndex);

    CGRect rFrame = [self SelectedItemRect:iIndex];

    if (m_pImageProvider == NULL)
    {
        [CRect Inset:&rFrame DX:1 DY:1];
    }

    [m_pHL setFrame:rFrame];

    if (m_pImageProvider != NULL)
    {
        CGRect rContent = [self SelectedItemUnitRect:rFrame];
        [[m_pHL layer] setContentsRect:rContent];
    }

	[UIView commitAnimations];
}
/*============================================================================*/

-(int)SelectedIndex

/*============================================================================*/
{
    return m_iSelectedIndex;
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    NSArray *pTouches = [touches allObjects];
    for (int i = 0; i < [pTouches count]; i++)
    {
        UITouch *pTouch = [pTouches objectAtIndex:i];
        if (pTouch.view == self)
        {
            if (m_iLayout == LAYOUT_OVERUNDER)
            {
                int iX = [pTouch locationInView:self].x;
                int iDXSeg = self.bounds.size.width / [m_pItems count];
                int iSeg = iX / iDXSeg;
                if (iSeg != m_iSelectedIndex)
                {
                    [self SetSelectedIndex:iSeg];
                    [self sendActionsForControlEvents:UIControlEventValueChanged];
                }
            }
            else
            {
                int iY = [pTouch locationInView:self].y;
                int iDYSeg = self.bounds.size.height / [m_pItems count];
                int iSeg = iY / iDYSeg;
                if (iSeg != m_iSelectedIndex)
                {
                    [self SetSelectedIndex:iSeg];
                    [self sendActionsForControlEvents:UIControlEventValueChanged];
                }
            }
        }
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iIndex = MAX(0, m_iSelectedIndex);
    
    for (int i = 0; i < [m_pItems count]; i++)
    {
        CGRect rItem = [self SelectedItemRect:i];
        UIView *pView = (UIView*)[m_pItems objectAtIndex:i];
//        [CRect CenterDownToX:&rItem DX:80];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            [CRect CenterDownToY:&rItem DY:80];
        else
            [CRect CenterDownToY:&rItem DY:52];

        [pView setFrame:rItem];
    }

    CGRect rFrame = [self SelectedItemRect:iIndex];
    
    if (m_pImageProvider == NULL)
    {
        [CRect Inset:&rFrame DX:1 DY:1];
    }

    [m_pHL setFrame:rFrame];

    if (m_pImageProvider != NULL)
    {
        CGRect rContent = [self SelectedItemUnitRect:rFrame];
        [[m_pHL layer] setContentsRect:rContent];
    }
    
    if (m_pImageProvider != NULL)
    {
        int iDXThis = self.bounds.size.width;
        int iDYThis = self.bounds.size.height;
        if (iDXThis != m_iImageDX || iDYThis != m_iImageDY)
        {
            CGImageRef pRef = [m_pImageProvider GenerateSegmentHighlight];
            [[m_pHL layer] setContents:(id)pRef];
            m_iImageDX = iDXThis;
            m_iImageDY = iDYThis;
        }
    }
}
/*============================================================================*/

-(void)Layout:(int)iNew

/*============================================================================*/
{
    m_iLayout = iNew;
}
/*============================================================================*/

-(CGRect)SelectedItemRect:(int)iIndex

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDXThis = rThis.size.width;
    int iDYThis = rThis.size.height;
    
    int iNItems = [m_pItems count];
    if (iNItems < 1)
        return rThis;
    
    if (m_iLayout == LAYOUT_OVERUNDER)
    {
        int iDXSeg = iDXThis / iNItems;
        int iX0 = iDXSeg * iIndex;
        if (iIndex == iNItems-1)
            iDXSeg = iDXThis - iX0;
        return CGRectMake(iX0, 0, iDXSeg, iDYThis);
    }
    else
    {
        int iDYSeg = iDYThis / iNItems;
        int iY0 = iDYSeg * iIndex;
        if (iIndex == iNItems-1)
            iDYSeg = iDYThis - iY0;
        return CGRectMake(0, iY0, iDXThis, iDYSeg);
    }
}
/*============================================================================*/

-(CGRect)SelectedItemUnitRect:(CGRect)rSelectedItemRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    float fDXThis = rThis.size.width;
    float fDYThis = rThis.size.height;

    float fX0 = rSelectedItemRect.origin.x / fDXThis;
    float fY0 = rSelectedItemRect.origin.y / fDYThis;
    float fDX = rSelectedItemRect.size.width / fDXThis;
    float fDY = rSelectedItemRect.size.height / fDYThis;
    
    return CGRectMake(fX0, fY0, fDX, fDY);
}
/*============================================================================*/

-(CGImageRef)GenerateSegmentHighlight

/*============================================================================*/
{
    return [self GenerateImage:ELEMENT_CHECK];
}

@end


@implementation CHLSegmentItem

/*============================================================================*/

-(id)initWithText:(NSString*)psText Data:(int)iData

/*============================================================================*/
{
    m_pText = psText;
    [m_pText retain];
    m_iData = iData;
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pText release];
    [super dealloc];
}
/*============================================================================*/

-(NSString*)Text        {   return m_pText;     }
-(int)Data              {   return m_iData;     }

/*============================================================================*/
@end

@implementation CHLSegmentIconView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Icon:(NSString*)psIcon Text:(NSString*)psText

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        self.opaque = NO;
        [self setUserInteractionEnabled:NO];
        
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pLabel];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView TEXT_SIZE_SIDEBAR]];
        m_pLabel.textAlignment = UITextAlignmentCenter;
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        m_pLabel.numberOfLines = 2;
        [m_pLabel setTextColor:[UIColor whiteColor]];
        [m_pLabel setText:psText];
        
        UIImage *pImage = [UIImage imageNamed:psIcon];
        CGSize size = CGSizeMake(1, 1);
        if (pImage != NULL)
            size = pImage.size;
        m_iImageDX = size.width;
        m_iImageDY = size.height;

        m_pImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        [m_pImage setImage:pImage];
        [self addSubview:m_pImage];
        [m_pImage release];
    }
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    //DL
    //rThis.origin.y += 20;
    
    int iLineSize = 0;
    @try
    {
        iLineSize = m_pLabel.font.lineHeight;
    }
    @catch (NSException *ex)
    {
        iLineSize = m_pLabel.font.pointSize * 125 / 100;
    }
    
    CGRect rLabel = [CRect BreakOffBottom:&rThis DY:2*iLineSize];
    rLabel.origin.y += 2;
    CGRect rImage = rThis;
    int iDimMin = MIN(rThis.size.width, rThis.size.height);
    iDimMin = MIN(iDimMin, m_iImageDX);

    [CRect ConfineToAspect:&rImage Source:CGRectMake(0, 0, m_iImageDX, m_iImageDY)];
    rImage.origin.y += 4;
    [m_pImage setFrame:rImage];
    [m_pLabel setFrame:rLabel];
    [m_pLabel setAlpha:1.0];
}

@end
