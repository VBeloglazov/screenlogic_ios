//
//  MainTab.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MainTab.h"
#import "hlm.h"
#import "NSQuery.h"
#import "SubTab.h"
#import "SecPartition.h"
#import "AudioZone.h"
#import "VideoPage.h"
#import "MediaZonePage.h"
#import "SecurityPage.h"
#import "LightingPage.h"
#import "HVACPage.h"
#import "tstat.h"
#import "PhotoAlbum.h"
#import "PhotosPage.h"
#import "NSPoolConfig.h"
#import "PoolSubTab.h"
#import "SystemMode.h"
#import "SubTabCell.h"
#import "IPOD_ViewController.h"
#import "AppDelegate.h"
#import "home.h"

#ifdef PENTAIR
    #import "NSPoolConfig.h"
    #import "PoolHistoryMainPage.h"
    #import "PoolTempPage.h"
    #import "PoolLightsView.h"
    #import "PoolColorLightsView.h"
    #import "PoolAccessoryView.h"
    #import "PoolIntelliBriteView.h"
    #import "PoolMagicStreamView.h"
    #import "IntelliChemPage.h"
    #import "IntelliChlorPage.h"
    #import "PoolFeaturePage.h"
#endif

@implementation CMainTab

@synthesize m_pName;
@synthesize m_pSubTabs;

/*============================================================================*/

-(id)initWithName:(NSString*)pName TabID:(int)iTabID

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pName = pName;
        m_pSystemName = m_pName;
        [m_pName retain];
        m_iTabID = iTabID;
        m_bConfigLoaded = FALSE;
        m_pSubTabs = [[NSMutableArray alloc] init];
    }
    return self;
}
/*============================================================================*/

-(id)initWithName:(NSString*)pName TabID:(int)iTabID SystemName:(NSString*)systemName

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pName = pName;
        m_pSystemName = systemName;
        [m_pName retain];
        m_iTabID = iTabID;
        m_bConfigLoaded = FALSE;
        m_pSubTabs = [[NSMutableArray alloc] init];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [m_pSubTabs release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetMainController:(UIViewController*)mainController

/*============================================================================*/
{
    m_pMainViewController = mainController;
}
/*============================================================================*/

-(NSString*)ImageName

/*============================================================================*/
{
    switch (m_iTabID)
    {
    case TAB_HOME:                                  return @"Home_Main.png";
    case TAB_SECURITY:                              return @"Security_Main.png";
    case TAB_HVAC:                                  return @"Climate_Main.png";
    case TAB_LIGHTING:                              return @"Lighting_Main.png";
    case TAB_AUDIO:                                 return @"Media_Main.png";
    case TAB_TELEPHONE:                             return @"Messaging_Main.png";
    case TAB_IRRIGATION:                            return @"Irrigation_Main.png";
    case TAB_VIDEO:                                 return @"Video_Main.png";
    case TAB_POOL:                                  return @"Pool_Spa_Main.png";
    case TAB_PHOTO:                                 return @"Photos_Main.png";
    }

#ifdef PENTAIR
    switch (m_iTabID)
    {
    case SUBTAB_PENTAIR_TEMP_POOL:                  return @"Pool_Spa_Main.png";
    case SUBTAB_PENTAIR_TEMP_SPA:                   return @"Pool_Spa_Main.png";
    case SUBTAB_PENTAIR_LIGHTS_ALL:                 return @"PENTAIR_IBRITE.png";
    case SUBTAB_PENTAIR_LIGHTS_COLORS:              return @"PENTAIR_IBRITE.png";
    case SUBTAB_PENTAIR_LIGHTS_INTELLIBRITE:        return @"PENTAIR_IBRITE.png";
    case SUBTAB_PENTAIR_LIGHTS_MAGICSTREAM:         return @"PENTAIR_IBRITE.png";
    case SUBTAB_PENTAIR_FEATURES:                   return @"SETTINGS.png";
    case SUBTAB_PENTAIR_HISTORY_TEMPS:              return @"HISTORY.png";
    case SUBTAB_PENTAIR_PH_ORP:                     return @"PENTAIR_PH_ORP.png";
    case SUBTAB_PENTAIR_CHLORINATOR:                return @"PENTAIR_CHLOR.png";
    }
#endif

    return @"";
}
/*============================================================================*/

-(NSString*)SelectText

/*============================================================================*/
{
    switch (m_iTabID)
    {
    case TAB_HOME:                                  return @"";
    case TAB_SECURITY:                              return @"Select Partition";
    case TAB_HVAC:                                  return @"Select Thermostat";
    case TAB_LIGHTING:                              return @"Select Keypad";
    case TAB_AUDIO:                                 return @"Select Media Zone";
    case TAB_TELEPHONE:                             return @"Select Mail Boxe";
    case TAB_IRRIGATION:                            return @"Select Group";
    case TAB_VIDEO:                                 return @"Select Video Source";
    case TAB_POOL:                                  return @"Select Category";
    case TAB_PHOTO:                                 return @"Select Folder";
    }

    return @"";
}
/*============================================================================*/

-(int)TabID

/*============================================================================*/
{
    return m_iTabID;
}
/*============================================================================*/

-(BOOL)LoadConfig

/*============================================================================*/
{
    if (m_bConfigLoaded)
        return TRUE;
        
    int iMSGQ = 0;
    int iData1 = -1;

    switch (m_iTabID)
    {
#ifdef PENTAIR
    case SUBTAB_PENTAIR_TEMP_POOL:
    case SUBTAB_PENTAIR_TEMP_SPA:
    case SUBTAB_PENTAIR_FEATURES:
    case SUBTAB_PENTAIR_LIGHTS_ALL:
    case SUBTAB_PENTAIR_LIGHTS_COLORS:
    case SUBTAB_PENTAIR_LIGHTS_INTELLIBRITE:
    case SUBTAB_PENTAIR_LIGHTS_MAGICSTREAM:
    case SUBTAB_PENTAIR_HISTORY_TEMPS:
    case SUBTAB_PENTAIR_PH_ORP:
    case SUBTAB_PENTAIR_CHLORINATOR:
        {
            m_bConfigLoaded = TRUE;
            return TRUE;
        }
        break;
#endif


    case TAB_SECURITY:                              { iMSGQ = HLM_SECURITY_GETALLPARTSQ;            iData1 = TRUE;          }   break;
    case TAB_HVAC:                                  { iMSGQ = HLM_HVAC_GETALLTSTATSBYTYPEQ;         iData1 = HVAC_TYPE_UI;  }   break;
    case TAB_LIGHTING:                              { iMSGQ = HLM_LIGHTING_GETALLUIKEYPADSBYIDQ;    }                           break;
    case TAB_AUDIO:                                 { iMSGQ = HLM_AUDIO_GETALLZONESQ;               }                           break;
    case TAB_TELEPHONE:                             { iMSGQ = HLM_PHONE_GETMAILBOXESQ;              }                           break;
    case TAB_IRRIGATION:                            { iMSGQ = HLM_IRRIGATION_GETGROUPSQ;            }                           break;
    case TAB_VIDEO:                                 { iMSGQ = HLM_VIDEO_GETALLCAMERASQ;             }                           break;

    case TAB_HOME:
        {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                //CHomeSubTab *pHome = [[CHomeSubTab alloc] initWithName:@"HOME" ID:0 Data1:0 Data2:0];
                CHomeSubTab *pHome = [[CHomeSubTab alloc] initWithName:m_pSystemName ID:0 Data1:0 Data2:0];
                //DL
                //[pHome SetMainViewController:(UIViewController *)m_pMainViewController];
                [m_pSubTabs addObject:pHome];
            }
            else 
            {
                CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
                CSystemModeList *pModeList =  [pDelegate SysModeList];

                if (![pModeList IsInitialized])
                    return TRUE;

                for (int i = 0; i < [pModeList NModes]; i++)
                {
                    NSString *pName = [NSString stringWithFormat:@"%s Mode", [[pModeList ModeName:i] UTF8String]];
                    CSubTab *pMode = [[CSubTab alloc] initWithName:pName ID:i Data1:0 Data2:0];
                    //[pMode SetMainViewController:(UIViewController *)m_pMainViewController];
                    [m_pSubTabs addObject:pMode];
                    [pMode release];
                }
            }



            m_bConfigLoaded = TRUE;
            return TRUE;
        }
        break;

    case TAB_POOL:
        {
//            CNSPoolConfig *pNSPoolConfig = [[CNSPoolConfig alloc] init];
            CPoolSubTab *pSubTab = [[CPoolSubTab alloc] initWithName:@"Pool/Spa"         ID:POOLPAGE_MAIN        Data1:0 Data2:0];
            [m_pSubTabs addObject:pSubTab];
            [pSubTab release];
/*
            [m_pSubTabs addObject:[[CPoolSubTab alloc] initWithName:@"Lights"       ID:POOLPAGE_LIGHTS      Data1:0 Data2:0 Config:pNSPoolConfig]];
            [m_pSubTabs addObject:[[CPoolSubTab alloc] initWithName:@"Schedule"     ID:POOLPAGE_SCHEDULE    Data1:0 Data2:0 Config:pNSPoolConfig]];
            [m_pSubTabs addObject:[[CPoolSubTab alloc] initWithName:@"History"      ID:POOLPAGE_HISTORY     Data1:0 Data2:0 Config:pNSPoolConfig]];
            [m_pSubTabs addObject:[[CPoolSubTab alloc] initWithName:@"Equipment"    ID:POOLPAGE_EQUIPMENT   Data1:0 Data2:0 Config:pNSPoolConfig]];
*/
//            [pNSPoolConfig release];
            m_bConfigLoaded = TRUE;
            return TRUE;
        }
        break;
    }

    CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:iMSGQ];
    [pQ1 autorelease];
    if (iData1 >= 0)
        [pQ1 PutInt:iData1];
        
    if (![pQ1 AskQuestion])
        return FALSE;

    // Handle Object Count Prefix if any
    switch (m_iTabID)
    {
    case TAB_SECURITY:
    case TAB_HVAC:
        {
            // Pro Module
            [pQ1 GetInt];
        }
        break;

    }
        
    int iN = [pQ1 GetInt];
    for (int i = 0; i < iN; i++)
    {
        [self LoadSubTab:pQ1];
    }


    m_bConfigLoaded = TRUE;
    return TRUE;
}
/*============================================================================*/

-(void)LoadSubTab:(CNSQuery *)pQ

/*============================================================================*/
{
    NSString *pS = NULL;
    int iID = 0;
    int iData1 = 0;
    int iData2 = 0;

    switch (m_iTabID)
    {
    case TAB_SECURITY:                             
        { 
            CSecPartition *pPart = [[CSecPartition alloc] initFromQ:pQ];
            [m_pSubTabs addObject:pPart];
            [pPart release];
            return;
        }
        break;
        
    case TAB_HVAC:
        {
            iID =       [pQ GetInt];
            pS  =       [pQ GetString];
            iData1 =    [pQ GetInt];        // Flags, custom page etc

            if (iData1 & SYSCOMP_CUSTOMPAGE)
                return;
            
            CTStat *pStat = [[CTStat alloc] initWithName:pS ID:iID Data1:iData1 Data2:iData2];
            [m_pSubTabs addObject:pStat];
            [pStat release];
            return;
        }
        break;

    case TAB_LIGHTING:
        {
            iID =       [pQ GetInt];
            pS  =       [pQ GetString];
            iData1 =    [pQ GetInt];        // Flags, custom page etc

            // Schedule not supported for now
            if (iData1 == SYSCOMP_SCHEDULE)
                return;
        }
        break;

 
    case TAB_TELEPHONE:
        {
            iID =       [pQ GetInt];
            iData1 =    [pQ GetInt];        // Flags, custom page etc
            pS  =       [pQ GetString];
        }
        break;

    case TAB_IRRIGATION:
        {
            pS  =       [pQ GetString];
            iID =       [pQ GetInt];
            iData1 =    [pQ GetInt];        // Which Days
        }
        break;

    case TAB_VIDEO:
        {
            iID =       [pQ GetInt];
            pS =        [pQ GetString];
            iData1 =    [pQ GetInt];    // Flags
            iData2 =    [pQ GetInt];    // Options
        }
        break;


    case TAB_PHOTO:
        {
        }
    }

    [self AddSubTab:pS ID:iID Data1:iData1 Data2:iData2];
}
/*============================================================================*/

-(void)AddSubTab:(NSString*) pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;

/*============================================================================*/
{
    CSubTab *pSub = [[CSubTab alloc] initWithName:pName ID:iID Data1:iData1 Data2:iData2];
    [m_pSubTabs addObject:pSub];
    [pSub release];
}
/*============================================================================*/

-(void)AddSubTab:(CSubTab*) pSubTab

/*============================================================================*/
{
    [m_pSubTabs addObject:pSubTab];
}
/*============================================================================*/

-(int)NSubTabs

/*============================================================================*/
{
    return [m_pSubTabs count];
}
/*============================================================================*/

-(CSubTab*)SubTab:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pSubTabs count])
        return NULL;
    return (CSubTab*)[m_pSubTabs objectAtIndex:iIndex];
}
/*============================================================================*/

-(UITableViewCell*)CellForSubTabIndex:(UITableView*)pTableView Index:(int)iSubTabIndex

/*============================================================================*/
{
    NSString *pIdent = [NSString stringWithFormat:@"%d:%d", m_iTabID, iSubTabIndex];

    CSubTabCell *cell = (CSubTabCell*)[pTableView dequeueReusableCellWithIdentifier:pIdent];
    if (cell == nil)
    {
        //Dl - replace deprecated stuff
        //cell = [[[CSubTabCell alloc] initWithFrame:CGRectZero reuseIdentifier:pIdent] autorelease];
        cell = [[[CSubTabCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:pIdent] autorelease];
    }

    // get the view controller's info dictionary based on the indexPath's row
    CSubTab *pSubTab = (CSubTab*)[m_pSubTabs objectAtIndex:iSubTabIndex];
    //[cell SetSubTab:pSubTab];
    //[pSubTab SetSubTabCell:cell];

    [self InitSubTabCell:cell SubTab:pSubTab];

    //[cell.m_pLabel setText:pSubTab.m_pName];
    
    //DL - this is what it costs to replave that deprecated stuff.
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setText:pSubTab.m_pName];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundView = [[UIView new] autorelease];
    cell.selectedBackgroundView = [[UIView new] autorelease];

    return cell;
}
/*============================================================================*/

-(void)InitSubTabCell:(CSubTabCell*)pCell SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    if (m_iTabID != TAB_HOME)
        return;
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CSystemModeList *pModeList =  [pDelegate SysModeList];
    if ([pModeList ActiveModeIndex] == pSubTab.m_iID)
        [pCell SetEmbeddedIcon:@"CHECK.png"];
    else
        [pCell SetEmbeddedIcon:NULL];
    pCell.m_iOptions = SUBTABCELL_ACCESSORYNEVER;
}
/*============================================================================*/

-(void)SetDefaultTab:(int)iIndex

/*============================================================================*/
{
    m_iDefaultTab = iIndex;
}
/*============================================================================*/

-(int)GetDefaultTab

/*============================================================================*/
{
    return m_iDefaultTab;
}
/*============================================================================*/

-(CSubTab*)DefaultTab

/*============================================================================*/
{
    CSubTab *pSub = (CSubTab*)[m_pSubTabs objectAtIndex:m_iDefaultTab];
    return pSub;
}
/*============================================================================*/

-(BOOL)IsExpanded

/*============================================================================*/
{
    return m_bExpanded;
}
/*============================================================================*/

-(void)SetExpanded:(BOOL)bNew

/*============================================================================*/
{
    m_bExpanded = bNew;
}
/*============================================================================*/

-(CSubSystemPage*)CreateUserPage:(CGRect) rFrame SubTab:(CSubTab*)pSubTab MainTabPage:(CMainTabViewController*)pMainPage

/*============================================================================*/
{
    CSubSystemPage *pNew = NULL;

    switch (m_iTabID)
    {
    case TAB_HOME:
        {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                CHomeSubTab *pSub = (CHomeSubTab*)pSubTab;
                //[pSub SetMainViewController:(UIViewController *)m_pMainViewController];
                pNew = [pSub CreateUserPage:rFrame MainTabPage:pMainPage];
            }
            else
            {
                //[(CHomeSubTab*)pSubTab SetMainViewController:(UIViewController *)m_pMainViewController];
                pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
            }
        }
        break;

    case TAB_VIDEO:     
        pNew = [[CVideoPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
        //DL - this is huge importance - you can use it from within, it is already set!
        //UIViewController *oo = [pNew.ViewController m_pMainViewController];
        break;

    case TAB_SECURITY:
        pNew = [[CSecurityPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
        break;

    case TAB_HVAC:    
        pNew = [[CHVACPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
        break;

    case TAB_LIGHTING:
        pNew = [[CLightingPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
        break;

     case TAB_POOL:
        {
            CPoolSubTab *pPoolSubTab = (CPoolSubTab*)pSubTab;
            pNew = [pPoolSubTab CreateUserPage:rFrame MainTabPage:pMainPage];
        }
        break;

    case TAB_TELEPHONE:
    case TAB_IRRIGATION:
        pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
        break;

    }
    
#ifdef PENTAIR
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CNSPoolConfig *pConfig = [pDelegate PoolConfig];


    switch (pSubTab.m_iID)
    {
    case SUBTAB_PENTAIR_TEMP_POOL:
    case SUBTAB_PENTAIR_TEMP_SPA:
        {
            int iBodyType = BODYTYPE_POOL;
            if (pSubTab.m_iID == SUBTAB_PENTAIR_TEMP_SPA)
                iBodyType = BODYTYPE_SPA;

            pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
            CPoolTempPage *pPage = [[CPoolTempPage alloc] initWithFrame:pNew.bounds Type:iBodyType Config:pConfig];  
            pPage.autoresizingMask = 0xFFFFFFFF;
            [pNew addSubview:pPage];
            [pPage release];
        }    
        break;

    case SUBTAB_PENTAIR_LIGHTS_ALL:
        {
            pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
            CPoolLightsView *pPage = [[CPoolLightsView alloc] initWithFrame:pNew.bounds Config:pConfig];  
            pPage.autoresizingMask = 0xFFFFFFFF;
            [pNew addSubview:pPage];
            [pPage release];
        }
        break;

    case SUBTAB_PENTAIR_LIGHTS_COLORS:
        {
            pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
            CPoolColorLightsView *pPage = [[CPoolColorLightsView alloc] initWithFrame:pNew.bounds Config:pConfig];  
            pPage.autoresizingMask = 0xFFFFFFFF;
            [pNew addSubview:pPage];
            [pPage release];
        }
        break;

    case SUBTAB_PENTAIR_LIGHTS_INTELLIBRITE:
        {
            pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
            CPoolIntelliBriteView *pPage = [[CPoolIntelliBriteView alloc] initWithFrame:pNew.bounds Config:pConfig];  
            pPage.autoresizingMask = 0xFFFFFFFF;
            [pNew addSubview:pPage];
            [pPage release];
        }
        break;

    case SUBTAB_PENTAIR_LIGHTS_MAGICSTREAM:
        {
            pNew = [[CSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage SubTab:pSubTab];
            CPoolMagicStreamView *pPage = [[CPoolMagicStreamView alloc] initWithFrame:pNew.bounds];  
            pPage.autoresizingMask = 0xFFFFFFFF;
            [pNew addSubview:pPage];
            [pPage release];
        }
        break;

    case SUBTAB_PENTAIR_PH_ORP:
        {
            CIntelliChemPage *pPage = [[CIntelliChemPage alloc] initWithFrame:rFrame MainTabPage:pMainPage Config:pConfig];  
            pNew = pPage;
        }
        break;
            
    case SUBTAB_PENTAIR_CHLORINATOR:
        {
            if(![pConfig EquipPresent:POOL_ICHEMPRESENT])
            {
                CIntelliChlorPage *pPage = [[CIntelliChlorPage alloc] initWithFrame:rFrame MainTabPage:pMainPage Config:pConfig IsActive:true];
                pNew = pPage;
            }
            else
            {
                CIntelliChlorPage *pPage = [[CIntelliChlorPage alloc] initWithFrame:rFrame MainTabPage:pMainPage Config:pConfig IsActive:false];
                pNew = pPage;
            }
            
        }
        break;

    case SUBTAB_PENTAIR_FEATURES:
        {
            CPoolFeaturePage *pPage = [[CPoolFeaturePage alloc] initWithFrame:rFrame MainTabPage:pMainPage Config:pConfig];  
            pNew = pPage;
        }
        break;

    case SUBTAB_PENTAIR_HISTORY_TEMPS:
        {
            CPoolHistoryMainPage *pPage = [[CPoolHistoryMainPage alloc] initWithFrame:rFrame MainTabPage:pMainPage Config:pConfig];  
            pNew = pPage;
        }    
        break;
    }
#endif

    return pNew;
}
/*============================================================================*/

-(int)IndexOf:(CSubTab*)pSubTab

/*============================================================================*/
{
    int iN = [m_pSubTabs count];
    for (int i = 0; i < iN; i++)
    {
        CSubTab *pTest = (CSubTab*)[m_pSubTabs objectAtIndex:i];
        if (pTest == pSubTab)
            return i;
    }

    return -1;
}
/*============================================================================*/

-(UIView*)CreateAccessoryView

/*============================================================================*/
{
#ifdef PENTAIR
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CNSPoolConfig *pConfig = [pDelegate PoolConfig];

    switch (m_iTabID)
    {
    case SUBTAB_PENTAIR_TEMP_POOL:
    case SUBTAB_PENTAIR_TEMP_SPA:
        {
            CPoolAccessoryView *pView = [[CPoolAccessoryView alloc] initWithFrame:CGRectZero Config:pConfig Type:m_iTabID];
            return pView;
        }
        break;
    
    default:
        break;
    }

    return NULL;

#else
    return NULL;
#endif
}
@end
