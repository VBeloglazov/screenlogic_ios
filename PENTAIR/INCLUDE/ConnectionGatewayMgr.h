/*
--------------------------------------------------------------------------------

    ConnectionGatewayMgr.h

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once

class CStatusSink;
class CSocketHL;

/*============================================================================*/

    class CConnectionGatewayMgr

/*============================================================================*/
{
#ifndef PENTAIRMON
    static CConnectionGatewayMgr*   m_pThis;
#endif

    BOOL                        m_bValid;

    CHLString                   m_strLastError;

    CSocketHL*                  m_pSocket;
    BOOL                        m_bDisconnect;

    CHLString                   m_sGatewayVersion;
    int                         m_iSystemType;

    // Properties
#ifdef PENTAIRMON
private:
    BOOL                        IsValid()        { return m_bValid;                 }

public:
    CHLString                   LastError()      { return m_strLastError;           }
    CSocketHL*                  Socket()         { return m_pSocket;                }
    CHLString                   GatewayVersion() { return m_sGatewayVersion;        }
    int                         SystemType()     { return m_iSystemType;            }
#else
private:
    static BOOL                 IsValid()        { return m_pThis   ? m_pThis->m_bValid          : FALSE;                          }

public:
    static CHLString            LastError()      { return m_pThis   ? m_pThis->m_strLastError    : _HLTEXT("No Instance created"); }
    static CSocketHL*           Socket()         { return IsValid() ? m_pThis->m_pSocket         : NULL;                           }
    static CHLString            GatewayVersion() { return IsValid() ? m_pThis->m_sGatewayVersion : CHLString(_HLTEXT(""));         }
    static int                  SystemType()     { return IsValid() ? m_pThis->m_iSystemType     : -1;                             }
#endif

    // Interface
public:

#ifdef PENTAIRMON
    BOOL                        GetGatewayData(
    CHLString                   sUserName,
    CHLString                   sPassword,
    CHLString                   sGatewayAddress,
    WORD                        wGatewayPort,
    BOOL                        bPortOpen,
    int                         iConType,
    CStatusSink*                pStatus );

    void                        CloseSocket();

    void                        Dispose(
    BOOL                        bDisconnect = FALSE);
#else
    static BOOL                 GetGatewayData(
    CHLString                   sUserName,
    CHLString                   sPassword,
    CHLString                   sGatewayAddress,
    WORD                        wGatewayPort,
    BOOL                        bPortOpen,
    int                         iConType,
    CStatusSink*                pStatus );

    static void                 CloseSocket();

    static void                 Dispose(
    BOOL                        bDisconnect = FALSE);
#endif

    // Construction/Destruction
#ifndef PENTAIRMON
private:
#endif
    CConnectionGatewayMgr();

    CConnectionGatewayMgr(
    CHLString                   sUserName,
    CHLString                   sGatewayAddress,
    WORD                        wGatewayPort,
    BOOL                        bPortOpen,
    CStatusSink*                pStatus );

public:
    ~CConnectionGatewayMgr();

    // Helpers
private:
    void                        SetLastError(
    CHLString                   sLastError )            { m_strLastError = sLastError; }

    BOOL                        ConnectionDirect(
    CHLString                   sAddress,
    WORD                        wPort,
    CStatusSink*                pStatus );

    BOOL                        ConnectionRelay(
    CHLString                   sUserName,
    WORD                        wPort,
    CStatusSink*                pStatus );

    BOOL                        DoLogin(
    CHLString                   sLoginName,
    CHLString                   sPassword,
    int                         iConnectionType );

    BOOL                        GetChallengeString(
    CHLString*                  pChallengeStr );

    BOOL                        GetVersion();
};
