/*
--------------------------------------------------------------------------------

    ROUNDRECT.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"
#include "helpers.h"
#include "setserver.h"
#include "main.h"

#include "roundrect.h"

#ifdef IPHONE
    #define RB_MASK             0xFF00FF00
    #define G_MASK              0x00FF0000
#else
    #define RB_MASK             0xF81F
    #define G_MASK              0x07E0
#endif

/*============================================================================*/

    CRadiusAlphaMap::CRadiusAlphaMap(
    int                         iRad )

/*============================================================================*/
{
    m_iRad  = iRad;
    iRad--;
    m_pBits = new BYTE[m_iRad* m_iRad];
    memset(m_pBits, 0, m_iRad*m_iRad);

    int iRad16      = iRad * 16;
    int iRad2_Plus  = (iRad+2)*(iRad+1);

    for (int iY = 0; iY < m_iRad; iY++)
    {
        int iDY             = iRad-iY;
        int iDY2            = iDY*iDY;
        int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
        int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
        int iDEdge          = iRadToPoint16-iRad16;
        int iDXThis         = iDX;
        int iX              = m_iRad-iDX-1;                

        BYTE *pSet = GetAlpha(iX, iY);

        while (iDEdge > 0 && iX < m_iRad)
        {
            // Move right until we're completely Inside of the radius
            if (iDEdge < 16)
            {
                int iAlpha = 0;
                iAlpha = 16 - iDEdge;
                *pSet = (BYTE)iAlpha; 
            }

            pSet++;
            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }

        memset(pSet, 16, (m_iRad-iX));
    }
}
/*============================================================================*/

    CRadiusAlphaMap::~CRadiusAlphaMap()

/*============================================================================*/
{
    delete [] m_pBits;
}
/*============================================================================*/

    BYTE                        *CRadiusAlphaMap::GetAlpha(
    int                         iX,
    int                         iY )

/*============================================================================*/
{
    LOGASSERT(iX >= 0 && iY >= 0);
    LOGASSERT(iX < m_iRad && iY < m_iRad);
    return m_pBits + (iY * m_iRad) + iX;
}
/*============================================================================*/

    CRoundRect::CRoundRect(
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iPixelSize,
    COLORREF                    rgbEdge,
    COLORREF                    rgbOutside,
    COLORREF                    rgbInside,
    int                         iCornerMask,
    int                         iFlags )

/*============================================================================*/
{
    m_pX_UL         = NULL;
    m_pX_LL         = NULL;
    m_pX_UR         = NULL;
    m_pX_LR         = NULL;
    m_pUL           = NULL;
    m_pLL           = NULL;
    m_pUR           = NULL;
    m_pLR           = NULL;
    m_pMap          = NULL;
    m_iAlpha        = 100;

    m_iDibFlags     = 0;    
    m_iFlags        = iFlags;
    m_iDX           = iDX;
    m_iDY           = iDY;
    m_iRad          = iRad;
    m_iPixelSize    = iPixelSize;
    m_iCornerMask   = iCornerMask;

    if (m_iFlags & ROUNDRECT_DISCARDABLE)
        m_iDibFlags = DDFLAG_DISCARDABLE;

    m_rgbEdge       = rgbEdge;
    m_rgbOutside    = rgbOutside;
    m_rgbInside     = rgbInside;

    int iDXT = m_iDX;
    int iDXB = m_iDX;

    if (iCornerMask & Bit(CORNER_UL))
        iDXT -= m_iRad;
    if (iCornerMask & Bit(CORNER_UR))
        iDXT -= m_iRad;

    if (iCornerMask & Bit(CORNER_LL))
        iDXB -= m_iRad;
    if (iCornerMask & Bit(CORNER_LR))
        iDXB -= m_iRad;
   
}
/*============================================================================*/

    CRoundRect::~CRoundRect()

/*============================================================================*/
{
    delete m_pX_UL;
    delete m_pX_UR;
    delete m_pX_LL;
    delete m_pX_LR;

    delete m_pUL;
    delete m_pUR;
    delete m_pLL;
    delete m_pLR;

    delete m_pMap;
}
/*============================================================================*/

    void                        CRoundRect::Draw(
    CHLDC                       *pDC,
    int                         iX,
    int                         iY,
    BOOL                        bFill )

/*============================================================================*/
{
    if (bFill)
    {
        pDC->FillRect(m_rgbInside, iX+m_iPixelSize, iY+m_iPixelSize, m_iDX-2*m_iPixelSize, m_iDY-2*m_iPixelSize);
    }

    if (pDC->Attribs() & DIBATTRIB_SCALE_ANY)
    {
        pDC->DrawRect(GetRGB(m_rgbEdge, MID_SIDE_PCT), iX, iY, m_iDX, m_iDY);
        return;
    }

    if (m_pUL == NULL)
    {
        InitSolidCorners();
    }

    int iRadUL = 0;
    int iRadLL = 0;
    int iRadUR = 0;
    int iRadLR = 0;
    
    if (m_iCornerMask & Bit(CORNER_UL))
        iRadUL = m_iRad;
    if (m_iCornerMask & Bit(CORNER_LL))
        iRadLL = m_iRad;
    if (m_iCornerMask & Bit(CORNER_UR))
        iRadUR = m_iRad;
    if (m_iCornerMask & Bit(CORNER_LR))
        iRadLR = m_iRad;

    if (iRadUL > 0 && m_pUL != NULL)
        pDC->BltFromDibDC(m_pUL,    iX,                     iY);
    if (iRadUR > 0 && m_pUR != NULL)
        pDC->BltFromDibDC(m_pUR,    iX + m_iDX - m_iRad,    iY);
    
    if (iRadLL > 0 && m_pLL != NULL) 
        pDC->BltFromDibDC(m_pLL,    iX,                     iY + m_iDY - m_iRad);
    if (iRadLR > 0 && m_pLR != NULL)
        pDC->BltFromDibDC(m_pLR,    iX + m_iDX - m_iRad,    iY + m_iDY - m_iRad);

    pDC->FillRect(GetRGB(m_rgbEdge, MID_SIDE_PCT), iX,                       iY+iRadUL, m_iPixelSize, m_iDY - iRadUL-iRadLL);
    pDC->FillRect(GetRGB(m_rgbEdge, MID_SIDE_PCT), iX+m_iDX-m_iPixelSize,    iY+iRadUR, m_iPixelSize, m_iDY - iRadUR-iRadLR);

    COLORREF rgbT = GetRGB(m_rgbEdge, LIGHT_SIDE_PCT);
    COLORREF rgbB = GetRGB(m_rgbEdge, DARK_SIDE_PCT);
    
    if (m_iFlags & ROUNDRECT_INVERT)
    {
        // Swap
        COLORREF rgb = rgbT;
        rgbT = rgbB;
        rgbB = rgb;
    }

    int iDXT = m_iDX - iRadUL - iRadUR;
    if (iDXT > 0)
        pDC->FillRect(rgbT, iX+iRadUL, iY, iDXT, m_iPixelSize);

    int iDXB = m_iDX - iRadLL - iRadLR;
    if (iDXB > 0)
        pDC->FillRect(rgbB, iX+iRadLL, iY+m_iDY-m_iPixelSize, iDXB, m_iPixelSize);
}
#ifndef IPHONE
/*============================================================================*/

    void                        CRoundRect::RenderCorner(
    CHLDibDC                    *pTgt,
    CHLDibDC                    *pBG,
    int                         iCorner )

/*============================================================================*/
{
    if (m_pX_UL == NULL)
    {
        InitAlphaCorners();
    }

    Blend(iCorner, pTgt, 0, 0, pBG);

    switch (iCorner)
    {
    case CORNER_UL:  {  if (m_pX_UL != NULL ) m_pX_UL->BltTransparentToHLDC(pTgt, 0, 0, 0, 0, m_pX_UL->ImageDX(), m_pX_UL->DY());   } break;
    case CORNER_UR:  {  if (m_pX_UR != NULL ) m_pX_UR->BltTransparentToHLDC(pTgt, 0, 0, 0, 0, m_pX_UR->ImageDX(), m_pX_UR->DY());   } break;
    case CORNER_LL:  {  if (m_pX_LL != NULL ) m_pX_LL->BltTransparentToHLDC(pTgt, 0, 0, 0, 0, m_pX_LL->ImageDX(), m_pX_LL->DY());   } break;
    case CORNER_LR:  {  if (m_pX_LR != NULL ) m_pX_LR->BltTransparentToHLDC(pTgt, 0, 0, 0, 0, m_pX_LR->ImageDX(), m_pX_LR->DY());   } break;
    }

}
#endif
/*============================================================================*/

    void                        CRoundRect::DrawX(
    CHLDC                       *pDC,
    int                         iX,
    int                         iY  )

/*============================================================================*/
{
    int iRadUL = 0;
    int iRadLL = 0;
    int iRadUR = 0;
    int iRadLR = 0;
    
    if (m_iCornerMask & Bit(CORNER_UL))
        iRadUL = m_iRad;
    if (m_iCornerMask & Bit(CORNER_LL))
        iRadLL = m_iRad;
    if (m_iCornerMask & Bit(CORNER_UR))
        iRadUR = m_iRad;
    if (m_iCornerMask & Bit(CORNER_LR))
        iRadLR = m_iRad;

    int iDXT = m_iDX - iRadUL - iRadUR;
    int iDXB = m_iDX - iRadLL - iRadLR;

    if (m_iRad > 0)
    {
        if (m_pX_UL == NULL)
        {
            InitAlphaCorners();
        }
    }

    COLORREF rgbT = GetRGB(m_rgbEdge, LIGHT_SIDE_PCT);
    COLORREF rgbB = GetRGB(m_rgbEdge, DARK_SIDE_PCT);
    
    if (m_iFlags & ROUNDRECT_INVERT)
    {
        // Swap
        COLORREF rgb = rgbT;
        rgbT = rgbB;
        rgbB = rgb;
    }

    if (m_iFlags & ROUNDRECT_ADDMODE)
    {
        //
        // Add Mode
        //
        CHLDibDC *pDIB = pDC->DibDC();
        if (pDIB == NULL)
        {
            pDIB = new CHLDibDC(DX(), DY(), 0, 0, MEMORY_SYSTEM, m_iDibFlags);
            pDC->BltToDibDC(pDIB, iX, iY);
            pDIB->SetOrigin(-iX, -iY);
        }        

        if (m_pX_UL != NULL && iRadUL > 0) m_pX_UL->BltTransparentToHLDC(pDIB, iX,                  iY,                  0, 0, m_pX_UL->ImageDX(), m_pX_UL->DY());
        if (m_pX_LL != NULL && iRadLL > 0) m_pX_LL->BltTransparentToHLDC(pDIB, iX,                  iY + m_iDY - m_iRad, 0, 0, m_pX_LL->ImageDX(), m_pX_LL->DY());
        if (m_pX_UR != NULL && iRadUR > 0) m_pX_UR->BltTransparentToHLDC(pDIB, iX + m_iDX - m_iRad, iY,                  0, 0, m_pX_UR->ImageDX(), m_pX_UR->DY());
        if (m_pX_LR != NULL && iRadLR > 0) m_pX_LR->BltTransparentToHLDC(pDIB, iX + m_iDX - m_iRad, iY + m_iDY - m_iRad, 0, 0, m_pX_LR->ImageDX(), m_pX_LR->DY());

        CDXYRect rL(iX, iY+iRadUL, m_iPixelSize, m_iDY-iRadLL-iRadUL);
        CDXYRect rR(iX+m_iDX-m_iPixelSize, iY+iRadUR, m_iPixelSize, m_iDY - iRadUR-iRadLR);

        if (iRadUL == 0)    rL.BreakOffTop(1, 0);
        if (iRadLL == 0)    rL.BreakOffBottom(1, 0);
        if (iRadUR == 0)    rR.BreakOffTop(1, 0);
        if (iRadLR == 0)    rR.BreakOffBottom(1, 0);


        COLORREF rgbEdge = GetRGB(m_rgbEdge, MID_SIDE_PCT);
        if (m_iAlpha < 100)
        {
            rgbEdge = CHLDC::ScaleColorPCT(m_iAlpha, rgbEdge );
            rgbT = CHLDC::ScaleColorPCT(m_iAlpha, rgbT);
            rgbB = CHLDC::ScaleColorPCT(m_iAlpha, rgbB);
        }

        pDIB->AddRGB(GetRValue(rgbEdge), GetGValue(rgbEdge), GetBValue(rgbEdge), rL, 0);
        pDIB->AddRGB(GetRValue(rgbEdge), GetGValue(rgbEdge), GetBValue(rgbEdge), rR, 0);

        if (iDXT > 0)
            pDIB->AddRGB(GetRValue(rgbT), GetGValue(rgbT), GetBValue(rgbT), CDXYRect(iX+iRadUL, iY, iDXT, m_iPixelSize), 0 );
        if (iDXB > 0)
            pDIB->AddRGB(GetRValue(rgbB), GetGValue(rgbB), GetBValue(rgbB), CDXYRect(iX+iRadLL, iY+m_iDY-m_iPixelSize, iDXB, m_iPixelSize), 0);

        if (pDIB != pDC->DibDC())
        {
            pDC->BltFromDibDC(pDIB, iX, iY);
            delete pDIB;
        }
    }
    else
    {
        if (iRadUL > 0) m_pX_UL->BltTransparentToHLDC(pDC, iX,                  iY,                  0, 0, m_pX_UL->ImageDX(), m_pX_UL->DY());
        if (iRadLL > 0) m_pX_LL->BltTransparentToHLDC(pDC, iX,                  iY + m_iDY - m_iRad, 0, 0, m_pX_LL->ImageDX(), m_pX_LL->DY());
        if (iRadUR > 0) m_pX_UR->BltTransparentToHLDC(pDC, iX + m_iDX - m_iRad, iY,                  0, 0, m_pX_UR->ImageDX(), m_pX_UR->DY());
        if (iRadLR > 0) m_pX_LR->BltTransparentToHLDC(pDC, iX + m_iDX - m_iRad, iY + m_iDY - m_iRad, 0, 0, m_pX_LR->ImageDX(), m_pX_LR->DY());

        COLORREF rgbEdge = GetRGB(m_rgbEdge, MID_SIDE_PCT);

            CDXYRect rL(iX, iY+iRadUL, m_iPixelSize, m_iDY - iRadUL-iRadLL);
            CDXYRect rR(iX+m_iDX-m_iPixelSize, iY+iRadUR, m_iPixelSize, m_iDY-iRadUR-iRadLR);
            CDXYRect rT(iX+iRadUL, iY, iDXT, m_iPixelSize);
            CDXYRect rB(iX+iRadLL, iY+m_iDY-m_iPixelSize, iDXB, m_iPixelSize);

        if (m_iAlpha >= 100)
        {
            pDC->FillRect(rgbEdge, rL);
            pDC->FillRect(rgbEdge, rR);
            pDC->FillRect(rgbT, rT);
            pDC->FillRect(rgbB, rB);
        }       
        else
        {
            CHLDibDC *pDIB = pDC->DibDC();
            if (pDIB != NULL)
            {
                int iAlpha = m_iAlpha * 255 / 100;
                HL_PIXEL wEdge = RGBTOHLPIXEL(rgbEdge);
                HL_PIXEL wT = RGBTOHLPIXEL(rgbT);
                HL_PIXEL wB = RGBTOHLPIXEL(rgbB);

                pDIB->FillRectAlpha(rL, wEdge, iAlpha);
                pDIB->FillRectAlpha(rR, wEdge, iAlpha);
                pDIB->FillRectAlpha(rT, wT, iAlpha);
                pDIB->FillRectAlpha(rB, wB, iAlpha);
            }
        }
    }
}
/*============================================================================*/

    void                        CRoundRect::ZeroOutsideRadius(
    CHLDibDC                    *pDIB,
    int                         iCorners )

/*============================================================================*/
{
    if (m_iRad <= 0)
        return;

    int iRad        = m_iRad-1;
    int iRad16      = iRad * 16;
    iRad16 += 8; // Add half a pixel

    int iRad2_Plus  = (iRad+2)*(iRad+1);

    int iPixelSize16    = m_iPixelSize * 16;
    if (m_iPixelSize == 1)
        iPixelSize16 = 20;


    //
    // iDEdge mapping
    //
    // >= 16    = COMPLETELY OUSIDE
    // 0 -> 15  = OUTSIDE RADIUS 
    // 0        = DEAD ON RADIUS
    // -15 -> 0 = INSIDE RADIUS BUT SUBPIXEL IN            
    // <= -16   = COMPLETELY INSIDE
    //

    for (int iY = 0; iY < m_iRad; iY++)
    {
        int iDY             = iRad-iY;
        int iDY2            = iDY*iDY;
        int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
        int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
        int iDEdgeStart     = iRadToPoint16-iRad16;
        int iXStart         = m_iRad-iDX-1;                
        int iDEdge          = iDEdgeStart;
        int iX              = iXStart;            
        int iDXThis         = iDX;

        HL_PIXEL *pPixelUL  = NULL;
        HL_PIXEL *pPixelUR  = NULL;
        HL_PIXEL *pPixelLL  = NULL;
        HL_PIXEL *pPixelLR  = NULL;
        
        if (iCorners & Bit(CORNER_UL))
            pPixelUL  = pDIB->Pixel(0, iY);
        if (iCorners & Bit(CORNER_LL))
            pPixelLL  = pDIB->Pixel(0, pDIB->DY()-iY-1);
        if (iCorners & Bit(CORNER_UR))
            pPixelUR  = pDIB->Pixel(pDIB->ImageDX()-1, iY);
        if (iCorners & Bit(CORNER_LR))
            pPixelLR  = pDIB->Pixel(pDIB->ImageDX()-1, pDIB->DY()-iY-1);


        for (int iXInit = 0; iXInit < iX; iXInit++)
        {
            if (pPixelUL != NULL)   { *pPixelUL = 0; pPixelUL++;    }
            if (pPixelLL != NULL)   { *pPixelLL = 0; pPixelLL++;    }
            if (pPixelUR != NULL)   { *pPixelUR = 0; pPixelUR--;    }
            if (pPixelLR != NULL)   { *pPixelLR = 0; pPixelLR--;    }
        }

        while (iDEdge > 0 && iX < m_iRad)
        {
            // Move left until we're completely OUTSIDE of the radius
            if (iDEdge < 16)
            {
                int iAlpha = 0;
                iAlpha = 16 - iDEdge;

#ifdef IPHONE
                if (pPixelUL != NULL)   ApplyAlpha16(pPixelUL, iAlpha);
                if (pPixelLL != NULL)   ApplyAlpha16(pPixelLL, iAlpha);
                if (pPixelUR != NULL)   ApplyAlpha16(pPixelUR, iAlpha);
                if (pPixelLR != NULL)   ApplyAlpha16(pPixelLR, iAlpha);
#else
#endif
            }

            if (pPixelUL != NULL) pPixelUL++;
            if (pPixelLL != NULL) pPixelLL++;
            if (pPixelUR != NULL) pPixelUR--;
            if (pPixelLR != NULL) pPixelLR--;

            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }
    }
}
/*============================================================================*/

    void                        CRoundRect::ZeroOutsideRadiusWith(
    CHLDibDC                    *pDIB,
    int                         iCorners,
    HL_PIXEL                    wOut )

/*============================================================================*/
{
    if (m_iRad <= 0)
        return;

    int iRad        = m_iRad-1;
    int iRad16      = iRad * 16;
    iRad16 += 8; // Add half a pixel

    int iRad2_Plus  = (iRad+2)*(iRad+1);

    int iPixelSize16    = m_iPixelSize * 16;
    if (m_iPixelSize == 1)
        iPixelSize16 = 20;


    //
    // iDEdge mapping
    //
    // >= 16    = COMPLETELY OUSIDE
    // 0 -> 15  = OUTSIDE RADIUS 
    // 0        = DEAD ON RADIUS
    // -15 -> 0 = INSIDE RADIUS BUT SUBPIXEL IN            
    // <= -16   = COMPLETELY INSIDE
    //

    for (int iY = 0; iY < m_iRad; iY++)
    {
        int iDY             = iRad-iY;
        int iDY2            = iDY*iDY;
        int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
        int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
        int iDEdgeStart     = iRadToPoint16-iRad16;
        int iXStart         = m_iRad-iDX-1;                
        int iDEdge          = iDEdgeStart;
        int iX              = iXStart;            
        int iDXThis         = iDX;

        HL_PIXEL *pPixelUL  = NULL;
        HL_PIXEL *pPixelUR  = NULL;
        HL_PIXEL *pPixelLL  = NULL;
        HL_PIXEL *pPixelLR  = NULL;
        
        if (iCorners & Bit(CORNER_UL))
            pPixelUL  = pDIB->Pixel(0, iY);
        if (iCorners & Bit(CORNER_LL))
            pPixelLL  = pDIB->Pixel(0, pDIB->DY()-iY-1);
        if (iCorners & Bit(CORNER_UR))
            pPixelUR  = pDIB->Pixel(pDIB->ImageDX()-1, iY);
        if (iCorners & Bit(CORNER_LR))
            pPixelLR  = pDIB->Pixel(pDIB->ImageDX()-1, pDIB->DY()-iY-1);


        for (int iXInit = 0; iXInit < iX; iXInit++)
        {
            if (pPixelUL != NULL)   { *pPixelUL = wOut; pPixelUL++;    }
            if (pPixelLL != NULL)   { *pPixelLL = wOut; pPixelLL++;    }
            if (pPixelUR != NULL)   { *pPixelUR = wOut; pPixelUR--;    }
            if (pPixelLR != NULL)   { *pPixelLR = wOut; pPixelLR--;    }
        }

        while (iDEdge > 0 && iX < m_iRad)
        {
            // Move left until we're completely OUTSIDE of the radius
            if (iDEdge < 16)
            {
                int iAlpha = 0;
                iAlpha = 16 - iDEdge;

#ifdef IPHONE
                if (pPixelUL != NULL)   ApplyAlpha16(pPixelUL, iAlpha, wOut);
                if (pPixelLL != NULL)   ApplyAlpha16(pPixelLL, iAlpha, wOut);
                if (pPixelUR != NULL)   ApplyAlpha16(pPixelUR, iAlpha, wOut);
                if (pPixelLR != NULL)   ApplyAlpha16(pPixelLR, iAlpha, wOut);
#else
#endif
            }

            if (pPixelUL != NULL) pPixelUL++;
            if (pPixelLL != NULL) pPixelLL++;
            if (pPixelUR != NULL) pPixelUR--;
            if (pPixelLR != NULL) pPixelLR--;

            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }
    }
}
/*============================================================================*/

    void                        CRoundRect::FillInterior(
    CHLDibDC                    *pDIB,
    COLORREF                    rgbFill )

/*============================================================================*/
{
    int iR = GetRValue(rgbFill);
    int iG = GetGValue(rgbFill);
    int iB = GetBValue(rgbFill);
    HL_PIXEL wFill = iR << 8 | iG << 16 | iB << 24 | 0xFF;
    CHLRect rFill(0, 0, m_iDX, m_iDY);
//    rFill.Inset(m_iPixelSize, m_iPixelSize);
    CHLRect rTop = rFill.BreakOffTop(m_iRad, 0);
    CHLRect rBtm = rFill.BreakOffBottom(m_iRad, 0);
    rTop.BreakOffTop(m_iPixelSize, 0);
    rBtm.BreakOffBottom(m_iPixelSize, 0);
    rFill.Inset(m_iPixelSize, 0);
    rTop.Inset(m_iRad, 0);
    rBtm.Inset(m_iRad, 0);
    pDIB->FillRect(0xFF000000 | rgbFill, rTop);
    pDIB->FillRect(0xFF000000 | rgbFill, rBtm);
    pDIB->FillRect(0xFF000000 | rgbFill, rFill);
    
    if (m_iRad <= 0)
        return;

    int iRad        = m_iRad-1;
    int iRad16      = iRad * 16;
    iRad16 -= 8; // Subtract half a pixel

    int iRad2_Plus  = (iRad+2)*(iRad+1);

    int iPixelSize16    = m_iPixelSize * 16;
    if (m_iPixelSize == 1)
        iPixelSize16 = 20;


    //
    // iDEdge mapping
    //
    // >= 16    = COMPLETELY OUSIDE
    // 0 -> 15  = OUTSIDE RADIUS 
    // 0        = DEAD ON RADIUS
    // -15 -> 0 = INSIDE RADIUS BUT SUBPIXEL IN            
    // <= -16   = COMPLETELY INSIDE
    //

    for (int iY = 0; iY < m_iRad; iY++)
    {
        int iDY             = iRad-iY;
        int iDY2            = iDY*iDY;
        int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
        int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
        int iDEdgeStart     = iRadToPoint16-iRad16;
        int iXStart         = m_iRad-iDX-1;                
        int iDEdge          = iDEdgeStart;
        int iX              = iXStart;            
        int iDXThis         = iDX;

        HL_PIXEL *pPixelUL  = NULL;
        HL_PIXEL *pPixelUR  = NULL;
        HL_PIXEL *pPixelLL  = NULL;
        HL_PIXEL *pPixelLR  = NULL;
        
        if (m_iCornerMask & Bit(CORNER_UL))
            pPixelUL  = pDIB->Pixel(iX, iY);
        if (m_iCornerMask & Bit(CORNER_LL))
            pPixelLL  = pDIB->Pixel(iX, pDIB->DY()-iY-1);
        if (m_iCornerMask & Bit(CORNER_UR))
            pPixelUR  = pDIB->Pixel(pDIB->ImageDX()-1-iX, iY);
        if (m_iCornerMask & Bit(CORNER_LR))
            pPixelLR  = pDIB->Pixel(pDIB->ImageDX()-1-iX, pDIB->DY()-iY-1);


        while (iDEdge > 0 && iX < m_iRad)
        {
            // Move left until we're completely OUTSIDE of the radius
            if (pPixelUL != NULL) pPixelUL++;
            if (pPixelLL != NULL) pPixelLL++;
            if (pPixelUR != NULL) pPixelUR--;
            if (pPixelLR != NULL) pPixelLR--;

            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }

        while (iDEdge > -iPixelSize16 && iX < m_iRad)
        {
            // Move Right until we're completely INSIDE of the radius
            int iAlpha = 16;
            if (iDEdge < -(iPixelSize16-16))
                iAlpha = iPixelSize16+iDEdge;

            if (pPixelUL != NULL) { BlendPixel16(pPixelUL, iAlpha, wFill); pPixelUL++; }
            if (pPixelUR != NULL) { BlendPixel16(pPixelUR, iAlpha, wFill); pPixelUR--; }
            if (pPixelLL != NULL) { BlendPixel16(pPixelLL, iAlpha, wFill); pPixelLL++; }
            if (pPixelLR != NULL) { BlendPixel16(pPixelLR, iAlpha, wFill); pPixelLR--; }

            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }

        while (iX < m_iRad)
        {
            if (pPixelUL != NULL) { *pPixelUL = wFill; pPixelUL++; }
            if (pPixelUR != NULL) { *pPixelUR = wFill; pPixelUR--; }
            if (pPixelLL != NULL) { *pPixelLL = wFill; pPixelLL++; }
            if (pPixelLR != NULL) { *pPixelLR = wFill; pPixelLR--; }
            iX++;
        }
    }

}
/*============================================================================*/

    void                        CRoundRect::Blend(
    int                         iCorner,
    CHLDibDC                    *pTgt,
    int                         iXTgt,
    int                         iYTgt,
    CHLDibDC                    *pBG )

/*============================================================================*/
{
    int iXSrc = 0;
    int iYSrc = 0;

    if (m_pMap == NULL)
    {
        m_pMap = new CRadiusAlphaMap(m_iRad);
    }

    int iDX   = m_iRad;
    int iDY   = m_iRad;
    if (!pTgt->GetSafeDims(&iXTgt, &iYTgt, &iDX, &iDY, &iXSrc, &iYSrc, pBG))
        return;

    switch (iCorner)
    {
    case CORNER_UL:
    case CORNER_LL:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                HL_PIXEL *pDst = pTgt->Pixel(iXTgt, iYTgt+iY);
                HL_PIXEL *pSrc = pBG->Pixel(iXSrc, iYSrc+iY);
                int iYA = iYSrc+iY;
                if (iCorner == CORNER_LL)
                    iYA = iDY-iY-1;

                BYTE *pA = m_pMap->GetAlpha(iXSrc, iYA);
                for (int iX = 0; iX < iDX; iX++)
                {
                    int iA = *pA;
                    int _iA = 16-iA;
                    int wSrc = *pSrc;
                    int wTgt = *pDst;
                    int iRBS = wSrc & 0xF81F;
                    int iRBT = wTgt & 0xF81F;
                    int iRB = ((iRBS * _iA + iRBT * iA) >> 4) & 0xF81F;
                    int iGS = wSrc & 0x07E0;
                    int iGT = wTgt & 0x07E0;
                    int iG  = ((iGS * _iA + iGT * iA) >> 4)   & 0x07E0;
                    *pDst = (HL_PIXEL)(iRB|iG);                    
                    
                    pDst++;
                    pSrc++;
                    pA++;
                }
            }
        }
        break;

    case CORNER_UR:
    case CORNER_LR:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                HL_PIXEL *pDst = pTgt->Pixel(iXTgt, iYTgt+iY);
                HL_PIXEL *pSrc = pBG->Pixel(iXSrc, iYSrc+iY);

                int iYA = iYSrc+iY;
                if (iCorner == CORNER_LR)
                    iYA = iDY-iY-1;

                BYTE *pA = m_pMap->GetAlpha(iXSrc+m_iRad-1, iYA);
                for (int iX = 0; iX < iDX; iX++)
                {
                    int iA = *pA;
                    int _iA = 16-iA;
                    int wSrc = *pSrc;
                    int wTgt = *pDst;
                    int iRBS = wSrc & 0xF81F;
                    int iRBT = wTgt & 0xF81F;
                    int iRB = ((iRBS * _iA + iRBT * iA) >> 4) & 0xF81F;
                    int iGS = wSrc & 0x07E0;
                    int iGT = wTgt & 0x07E0;
                    int iG  = ((iGS * _iA + iGT * iA) >> 4)   & 0x07E0;
                    *pDst = (HL_PIXEL)(iRB|iG);                    
                    
                    pDst++;
                    pSrc++;
                    pA--;
                }
            }
        }
        break;
    }
}
/*============================================================================*/

    COLORREF                    CRoundRect::GetRGB(
    COLORREF                    rgb,
    int                         iPCT )

/*============================================================================*/
{
    int iR = GetRValue(rgb);
    int iG = GetGValue(rgb);
    int iB = GetBValue(rgb);
    iR = __min(255, iR * iPCT / 100);
    iG = __min(255, iG * iPCT / 100);
    iB = __min(255, iB * iPCT / 100);
    return RGB(iR, iG, iB);
}
/*============================================================================*/

    COLORREF                    CRoundRect::Add(
    COLORREF                    rgb1,
    COLORREF                    rgb2 )

/*============================================================================*/
{
    int iR = MIN(0xFF, GetRValue(rgb1)+GetRValue(rgb2));
    int iG = MIN(0xFF, GetGValue(rgb1)+GetGValue(rgb2));
    int iB = MIN(0xFF, GetBValue(rgb1)+GetBValue(rgb2));
    return RGB(iR,iG,iB);
}
/*============================================================================*/

    void                        CRoundRect::InitAlphaCorners()

/*============================================================================*/
{
    if (m_iRad <= 0)
        return;

    int iRad        = m_iRad-1;
    int iRad16      = iRad * 16;
    int iRad2_Plus  = (iRad+2)*(iRad+1);
    int iRadDIV2    = m_iRad/2;


#ifdef IPHONE
    int iR_ = GetRValue(m_rgbEdge);
    int iG_ = GetGValue(m_rgbEdge);
    int iB_ = GetBValue(m_rgbEdge);

    DWORD iRB = (iB_ << 18) | (iR_ << 2);
    DWORD iG  = (iG_ << 10);
#else
    HL_PIXEL wEdge = RGBTOHLPIXEL(m_rgbEdge);
    int iRB = wEdge & 0xF81F;
    int iG = wEdge & 0x07E0;
#endif

    int iFlags = m_iDibFlags;
    if (m_iFlags & ROUNDRECT_ADDMODE)
        iFlags |= DIBATTRIB_ALPHAADDMODE;

#ifdef IPHONE
    m_pX_UL = new CAlphaDIB(m_iRad, m_iRad, 0, 0, MEMORY_SYSTEM, m_iDibFlags|iFlags);
    m_pX_LL = new CAlphaDIB(m_iRad, m_iRad, 0, 0, MEMORY_SYSTEM, m_iDibFlags|iFlags);
#else
    m_pX_UL = new CAlphaDIB(m_iRad, m_iRad, 0, 0, m_iDibFlags|iFlags);
    m_pX_LL = new CAlphaDIB(m_iRad, m_iRad, 0, 0, m_iDibFlags|iFlags);
#endif

#ifdef IPHONE
    m_pX_UL->Fill(0x0000);
    m_pX_LL->Fill(0x0000);
#endif

    int iATMin          = 16 ;
    int iATMax          = 16 * MID_SIDE_PCT / 100;
    int iABMax          = 16 * MID_SIDE_PCT / 100;
    int iABMin          = 16 * DARK_SIDE_PCT / 100;
    int iPixelSize16    = m_iPixelSize * 16;
    if (m_iPixelSize == 1)
        iPixelSize16 = 20;

    if (m_iFlags & ROUNDRECT_INVERT)
    {
        // Invert
        int i1 = iATMin;
        int i2 = iATMax;
        iATMin = iABMin;
        iATMax = iABMax;
        iABMin = i1;
        iABMax = i2;
    }

    iATMin = iATMin * m_iAlpha / 100;
    iATMax = iATMax * m_iAlpha / 100;
    iABMin = iABMin * m_iAlpha / 100;
    iABMax = iABMax * m_iAlpha / 100;

    //
    // iDEdge mapping
    //
    // >= 16    = COMPLETELY OUSIDE
    // 0 -> 15  = OUTSIDE RADIUS 
    // 0        = DEAD ON RADIUS
    // -15 -> 0 = INSIDE RADIUS BUT SUBPIXEL IN            
    // <= -16   = COMPLETELY INSIDE
    //

    //
    // Do the UL and LL, the UR and LR are just flips
    //
    if (m_iFlags & ROUNDRECT_ADDMODE)
    {
        //
        // Do Premult here, don't care about our "alpha" channel
        //
        m_pX_UL->AlphaFlags(ALPHA_PREMULT);
        m_pX_LL->AlphaFlags(ALPHA_PREMULT);
        m_pX_UL->Fill(0x0000);
        m_pX_LL->Fill(0x0000);


        for (int iY = 0; iY < m_iRad; iY++)
        {
            int iDY             = iRad-iY;
            int iDY2            = iDY*iDY;
            int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
            int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
            int iDEdgeStart     = iRadToPoint16-iRad16;
            int iXStart         = m_iRad-iDX-1;                
            int iDEdge          = iDEdgeStart;
            int iX              = iXStart;            
            int iDXThis         = iDX;

            HL_PIXEL *pPixelUL  = m_pX_UL->Pixel(iX, iY);
            HL_PIXEL *pPixelLL  = m_pX_LL->Pixel(iX, m_iRad-iY-1);
            int iAlphaT16atY    = iATMax;
            int iAlphaB16atY    = iABMax;
            if (iY < iRadDIV2)
            {
                iAlphaT16atY    = iATMin + (iATMax-iATMin) * iY / iRadDIV2;
                iAlphaB16atY    = iABMin + (iABMax-iABMin) * iY / iRadDIV2;
            }

            while (iDEdge > 0 && iX < m_iRad)
            {
                // Move left until we're completely OUTSIDE of the radius
                if (iDEdge < 16)
                {
                    int iAlpha = 0;
                    iAlpha = 16 - iDEdge;
                    int iAlphaT = (iAlpha * iAlphaT16atY) >> 2;
                    int iAlphaB = (iAlpha * iAlphaB16atY) >> 2;

#ifdef IPHONE
                    int iRBThis = (iRB * iAlphaT) & RB_MASK;
                    int iGThis =  (iG  * iAlphaT) & G_MASK;
                    *pPixelUL = (HL_PIXEL)(iRBThis|iGThis);

                    iRBThis = (iRB * iAlphaB) & RB_MASK;
                    iGThis =  (iG  * iAlphaB) & G_MASK;
                    *pPixelLL = (HL_PIXEL)(iRBThis|iGThis);;
#else
                    int iRBThis = ((iRB * iAlphaT) >> 6) & 0xF81F;
                    int iGThis =  ((iG  * iAlphaT) >> 6) & 0x07E0;
                    *pPixelUL = (HL_PIXEL)(iRBThis|iGThis);

                    iRBThis = ((iRB * iAlphaB) >> 6) & 0xF81F;
                    iGThis =  ((iG  * iAlphaB) >> 6) & 0x07E0;
                    *pPixelLL = (HL_PIXEL)(iRBThis|iGThis);;
#endif
                }

                pPixelUL++; pPixelLL++;
                iX++;
                iDXThis--;
                iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
                iDEdge = iRadToPoint16-iRad16;
            }

            while (iDEdge > -iPixelSize16 && iX < m_iRad)
            {
                // Move Right until we're completely INSIDE of the radius
                int iAlpha = 16;
                if (iDEdge < -(iPixelSize16-16))
                    iAlpha = iPixelSize16+iDEdge;

                int iAlphaT = (iAlpha * iAlphaT16atY)>>2;
                int iAlphaB = (iAlpha * iAlphaB16atY)>>2;

#ifdef IPHONE
                int iRBThis = (iRB * iAlphaT) & RB_MASK;
                int iGThis =  (iG  * iAlphaT) & G_MASK;
                *pPixelUL = (HL_PIXEL)(iRBThis|iGThis);

                iRBThis = (iRB * iAlphaB) & RB_MASK;
                iGThis =  (iG  * iAlphaB) & G_MASK;
                *pPixelLL = (HL_PIXEL)(iRBThis|iGThis);;
#else

                int iRBThis = ((iRB * iAlphaT) >> 6) & 0xF81F;
                int iGThis  = ((iG  * iAlphaT) >> 6) & 0x07E0;
                *pPixelUL   = (HL_PIXEL)(iRBThis|iGThis);

                iRBThis   = ((iRB * iAlphaB) >> 6) & 0xF81F;
                iGThis    = ((iG  * iAlphaB) >> 6) & 0x07E0;
                *pPixelLL = (HL_PIXEL)(iRBThis|iGThis);
#endif

                pPixelUL++; pPixelLL++; 
                iX++;
                iDXThis--;
                iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
                iDEdge = iRadToPoint16-iRad16;
            }
        }
    }
    else
    {
        //
        // Normal alpha mode
        //
        if (m_iPixelSize == 1)
            iPixelSize16 = 24;

        for (int iY = 0; iY < m_iRad; iY++)
        {
            int iDY             = iRad-iY;
            int iDY2            = iDY*iDY;
            int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
            int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
            int iDEdgeStart     = iRadToPoint16-iRad16;
            int iXStart         = m_iRad-iDX-1;                
            int iDEdge          = iDEdgeStart;
            int iX              = iXStart;            
            int iDXThis         = iDX;


            HL_PIXEL *pPixelUL  = m_pX_UL->Pixel(iX, iY);
            HL_PIXEL *pPixelLL  = m_pX_LL->Pixel(iX, m_iRad-iY-1);
            BYTE *pAUL          = m_pX_UL->GetMask(iX, iY);
            BYTE *pALL          = m_pX_LL->GetMask(iX, m_iRad-iY-1);
            int iAlphaT16atY    = iATMax;
            int iAlphaB16atY    = iABMax;
            if (iY < iRadDIV2)
            {
                iAlphaT16atY    = iATMin + (iATMax-iATMin) * iY / iRadDIV2;
                iAlphaB16atY    = iABMin + (iABMax-iABMin) * iY / iRadDIV2;
            }

            int iRBAtYT         = ((iAlphaT16atY * iRB) >> 4) & 0xF81F;
            int iGAtYT          = ((iAlphaT16atY * iG) >> 4)  & 0x07E0;
            int iRBAtYB         = ((iAlphaB16atY * iRB) >> 4) & 0xF81F;
            int iGAtYB          = ((iAlphaB16atY * iG) >> 4)  & 0x07E0;
            HL_PIXEL wEdgeAtYT  = (HL_PIXEL)(iRBAtYT|iGAtYT);
            HL_PIXEL wEdgeAtYB  = (HL_PIXEL)(iRBAtYB|iGAtYB);
            
            while (iDEdge > 0 && iX < m_iRad)
            {
                // Move left until we're completely OUTSIDE of the radius
                if (iDEdge < 16)
                {
                    int iAlpha = (16-iDEdge);
                    *pAUL = (iAlpha * 255) >> 4;
                    *pALL = *pAUL;
                }

                *pPixelUL = wEdgeAtYT;
                *pPixelLL = wEdgeAtYB;
                pAUL++; pALL++;
                pPixelUL++; pPixelLL++;
                iX++;
                iDXThis--;
                iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
                iDEdge = iRadToPoint16-iRad16;
            }

            while (iDEdge > -iPixelSize16 && iX < m_iRad)
            {
                // Move Right until we're completely INSIDE of the radius
                int iAlpha = 16;
                if (iDEdge < -(iPixelSize16-16))
                    iAlpha = (iPixelSize16+iDEdge);
                *pAUL = (iAlpha * 255) >> 4;
                *pALL = (iAlpha * 255) >> 4;
                *pPixelUL = wEdgeAtYT;
                *pPixelLL = wEdgeAtYB;
                pAUL++; pALL++; 
                pPixelUL++; pPixelLL++; 
                iX++;
                iDXThis--;
                iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
                iDEdge = iRadToPoint16-iRad16;
            }
        }
    }

    m_pX_UR = (CAlphaDIB*)m_pX_UL->CreateFlip(TRUE, FALSE);
    m_pX_LR = (CAlphaDIB*)m_pX_LL->CreateFlip(TRUE, FALSE);
}
/*============================================================================*/

    void                        CRoundRect::InitSolidCorners()

/*============================================================================*/
{
    if (m_iRad <= 0)
        return;

    int iRad        = m_iRad-1;
    int iRad16      = iRad * 16;
    int iRad2_Plus  = (iRad+2)*(iRad+1);
    int iRadDIV2    = m_iRad/2;

    HL_PIXEL wEdge = RGBTOHLPIXEL(m_rgbEdge);
    HL_PIXEL wOut  = RGBTOHLPIXEL(m_rgbOutside);
    HL_PIXEL wIn   = RGBTOHLPIXEL(m_rgbInside);

    int iRBEdge = wEdge & 0xF81F;
    int iGEdge  = wEdge & 0x07E0;
    int iRBOut  = wOut  & 0xF81F;
    int iGOut   = wOut  & 0x07E0;
    int iRBIn   = wIn   & 0xF81F;
    int iGIn    = wIn   & 0x07E0;

    m_pUL = new CHLDibDC(m_iRad, m_iRad, 0, 0, MEMORY_SYSTEM, m_iDibFlags);
    m_pLL = new CHLDibDC(m_iRad, m_iRad, 0, 0, MEMORY_SYSTEM, m_iDibFlags);
    m_pUL->Fill(wOut);
    m_pLL->Fill(wOut);

    int iATMin  = 16 ;
    int iATMax  = 16 * MID_SIDE_PCT / 100;
    int iABMax  = 16 * MID_SIDE_PCT / 100;
    int iABMin  = 16 * DARK_SIDE_PCT / 100;

    if (m_iFlags & ROUNDRECT_INVERT)
    {
        // Invert
        int i1 = iATMin;
        int i2 = iATMax;
        iATMin = iABMin;
        iATMax = iABMax;
        iABMin = i1;
        iABMax = i2;
    }


    //
    // iDEdge mapping
    //
    // >= 16    = COMPLETELY OUSIDE
    // 0 -> 15  = OUTSIDE RADIUS 
    // 0        = DEAD ON RADIUS
    // -15 -> 0 = INSIDE RADIUS BUT SUBPIXEL IN            
    // <= -16   = COMPLETELY INSIDE
    //

    //
    // Do the UL and LL, the UR and LR are just flips
    //
    for (int iY = 0; iY < m_iRad; iY++)
    {
        int iDY             = iRad-iY;
        int iDY2            = iDY*iDY;
        int iDX             = __min(iRad, iSqrt(iRad2_Plus-iDY*iDY));
        int iRadToPoint16   = iSqrt16(iDX*iDX+iDY2);
        int iDEdgeStart     = iRadToPoint16-iRad16;
        int iXStart         = m_iRad-iDX-1;                
        int iDEdge          = iDEdgeStart;
        int iX              = iXStart;            
        int iDXThis         = iDX;

        HL_PIXEL *pPixelUL  = m_pUL->Pixel(iX, iY);
        HL_PIXEL *pPixelLL  = m_pLL->Pixel(iX, m_iRad-iY-1);
        int iAlphaT16atY    = iATMax;
        int iAlphaB16atY    = iABMax;
        if (iY < iRadDIV2)
        {
            iAlphaT16atY    = iATMin + (iATMax-iATMin) * iY / iRadDIV2;
            iAlphaB16atY    = iABMin + (iABMax-iABMin) * iY / iRadDIV2;
        }

        int iRBAtYT         = ((iAlphaT16atY * iRBEdge) >> 4) & 0xF81F;
        int iGAtYT          = ((iAlphaT16atY * iGEdge)  >> 4) & 0x07E0;
        int iRBAtYB         = ((iAlphaB16atY * iRBEdge) >> 4) & 0xF81F;
        int iGAtYB          = ((iAlphaB16atY * iGEdge)  >> 4) & 0x07E0;
        int iRBThis         = 0;
        int iGThis          = 0;

        while (iDEdge > 0)
        {
            // Move right while we're completely OUTSIDE of the radius
            if (iDEdge < 16)
            {
                int iAlpha = (16-iDEdge);
                int _iAlpha = 16-iAlpha;
                iRBThis     = ((iRBAtYT * iAlpha + iRBOut * _iAlpha) >> 4) & 0xF81F;
                iGThis      = ((iGAtYT  * iAlpha + iGOut  * _iAlpha) >> 4) & 0x07E0;
                *pPixelUL   = (HL_PIXEL)(iRBThis|iGThis);
                iRBThis     = ((iRBAtYB * iAlpha + iRBOut * _iAlpha) >> 4) & 0xF81F;
                iGThis      = ((iGAtYB  * iAlpha + iGOut  * _iAlpha) >> 4) & 0x07E0;
                *pPixelLL   = (HL_PIXEL)(iRBThis|iGThis);
            }

            pPixelUL++; pPixelLL++;
            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }

        while (iDEdge > -24 && iX < m_iRad)
        {
            // Move Right until we're completely INSIDE of the radius
            int iAlpha = 16;
            if (iDEdge < -8)
                iAlpha = (24+iDEdge);
            int _iAlpha = 16-iAlpha;

            iRBThis     = ((iRBAtYT * iAlpha + iRBIn * _iAlpha) >> 4) & 0xF81F;
            iGThis      = ((iGAtYT * iAlpha  + iGIn * _iAlpha) >> 4)  & 0x07E0;
            *pPixelUL   = (HL_PIXEL)(iRBThis|iGThis);

            iRBThis     = ((iRBAtYB * iAlpha + iRBIn * _iAlpha) >> 4) & 0xF81F;
            iGThis      = ((iGAtYB * iAlpha  + iGIn * _iAlpha) >> 4)  & 0x07E0;
            *pPixelLL   = (HL_PIXEL)(iRBThis|iGThis);

            pPixelUL++; pPixelLL++; 
            iX++;
            iDXThis--;
            iRadToPoint16 = iSqrt16(iDXThis*iDXThis+iDY2);
            iDEdge = iRadToPoint16-iRad16;
        }

        // Fill in the rest
        while (iX < m_iRad)
        {
            *pPixelUL = wIn;
            *pPixelLL = wIn;
            pPixelUL++;
            pPixelLL++;
            iX++;
        }
    }

    m_pUR = m_pUL->CreateFlip(TRUE, FALSE);
    m_pLR = m_pLL->CreateFlip(TRUE, FALSE);
}
/*============================================================================*/

    CAlphaDIB                   *CRoundRect::GenEdge(
    int                         iDir,
    int                         iDim,
    int                         iFlags,
    COLORREF                    *pRGB,
    int                         iColorType  )

/*============================================================================*/
{
    int iDX = 1;
    int iDY = 1;
    switch (iDir)
    {   
    case DIR_UP:
    case DIR_DOWN:
        iDX = iDim;
        break;    

    case DIR_LEFT:
    case DIR_RIGHT:
        iDY = iDim;
        break;    
    }

#ifdef IPHONE
    CAlphaDIB *pRet = new CAlphaDIB(iDX, iDY, 0, 0, MEMORY_SYSTEM, DIBATTRIB_ALPHAADDMODE|DDFLAG_DISCARDABLE);
#else
    CAlphaDIB *pRet = new CAlphaDIB(iDX, iDY, 0, 0, DIBATTRIB_ALPHAADDMODE|DDFLAG_DISCARDABLE);
#endif
    pRet->Attribs(DIBATTRIB_ALPHAADDMODE|DDFLAG_DISCARDABLE);
    pRet->AlphaFlags(ALPHA_PREMULT);
    COLORREF rgbEdge = g_pSetServer->GetRGB(iColorType);  
    if (pRGB != NULL)
        rgbEdge = *pRGB;

    switch (iDir)
    {
    case DIR_LEFT:
    case DIR_RIGHT:
        pRet->FillRect(GetRGB(rgbEdge, MID_SIDE_PCT), 0, 0, iDX, iDY);
        break;

    case DIR_UP:
        pRet->FillRect(GetRGB(rgbEdge, LIGHT_SIDE_PCT), 0, 0, iDX, iDY);
        break;

    case DIR_DOWN:
        pRet->FillRect(GetRGB(rgbEdge, DARK_SIDE_PCT), 0, 0, iDX, iDY);
        break;
    }

    pRet->InitMask(255);

    return pRet;
}
/*============================================================================*/

    void                        CRoundRect::ApplyAlpha16(
    HL_PIXEL                    *pDst,
    int                         iAlpha16 )

/*============================================================================*/
{
#ifdef IPHONE
    int iAlpha256 = (iAlpha16 << 4);
    HL_PIXEL wSrc = *pDst;
    wSrc = wSrc >> 8;
    DWORD dwRB = wSrc & 0x00FF00FF;
    DWORD dwG  = wSrc & 0x0000FF00;
    dwRB = (dwRB * iAlpha256 ) & RB_MASK;
    dwG  = (dwG * iAlpha256 ) & G_MASK;
    wSrc = dwRB | dwG | iAlpha256;
    *pDst = wSrc;
#else
#endif
}
/*============================================================================*/

    void                        CRoundRect::ApplyAlpha16(
    HL_PIXEL                    *pDst,
    int                         iAlpha16,
    HL_PIXEL                    wSet )

/*============================================================================*/
{
#ifdef IPHONE
    int _iAlpha16 = 16-iAlpha16;
    HL_PIXEL wSrc = *pDst;
    wSrc = wSrc >> 4;
    wSet = wSet >> 4;
    DWORD dwRBC = wSet & (RB_MASK >> 4);
    DWORD dwGC  = wSet & (G_MASK >> 4);
    DWORD dwRBS = wSrc & (RB_MASK >> 4);
    DWORD dwGS  = wSrc & (G_MASK >> 4);


    DWORD dwRB = (dwRBS * iAlpha16 + dwRBC * _iAlpha16) & RB_MASK;
    DWORD dwG  = (dwGS  * iAlpha16 + dwGC  * _iAlpha16) & G_MASK;

    wSrc = dwRB | dwG;
    *pDst = wSrc;
#else
#endif
}
/*============================================================================*/

    void                        CRoundRect::BlendPixel16(
    HL_PIXEL                    *pDst,
    int                         iAlpha16,
    HL_PIXEL                    wColor )

/*============================================================================*/
{
#ifdef IPHONE
    DWORD wDst = *pDst;
    wDst = wDst >> 8;
    DWORD wSrc = wColor >> 8;
    DWORD wRBDst = wDst & 0x00FF00FF;
    DWORD wGDst  = wDst & 0x0000FF00;
    DWORD wRBSrc = wSrc & 0x00FF00FF;
    DWORD wGSrc  = wSrc & 0x0000FF00;
    DWORD dwA256 = (iAlpha16 << 4);
    DWORD _dwA256 = 0x100-dwA256;
    wRBDst = (wRBSrc * _dwA256 + wRBDst * dwA256) & RB_MASK;
    wGDst = (wGSrc * _dwA256 + wGDst * dwA256) & G_MASK;
    *pDst = wRBDst|wGDst|0xFF;
#else
#endif
}
