//
//  HVACModePage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/29/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HVACModePage_IPOD.h"
#import "HVACTempView.h"
#import "tstat.h"
#import "hlm.h"
#import "hlradio.h"
#import "MainView.h"
#import "crect.h"
#import "hlbutton.h"
#import "CustomPage.h"

@implementation CHVACModePage_IPOD

#define DX_INSET_IPOD                   10
#define DY_MODES_IPOD                   50
#define DY_BUTTON_IPOD                  40
#define DX_LABEL_IPOD                   125
#define DY_LABEL_IPOD                   100

#define CMD_MODE                        1000
#define CMD_FAN                         1001
#define CMD_RUN_PROG                    1002

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat

/*============================================================================*/
{
    if (self = [super initWithFrame:rect])
    {
        m_iHVACState = -1;
        m_pTStat = pTStat;
        [m_pTStat AddSink:self];
        [self InitModeControl];

        self.autoresizingMask = 0xFFFFFFFF;

        if ([m_pTStat StateOK])
        {
            [self Notify:0 Data:0];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTStat RemoveSink:self];
    [m_pMode release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_CONFIGCHANGED)
        [self InitModeControl];
        
    int iHVACMode = [m_pTStat HVACMode];
    [m_pMode setHidden:(iHVACMode == HVAC_STATE_NOTOK)];

    int iIndex = -1;
    for (int i = 0; i < [m_pMode NButtons] && iIndex == -1; i++)
    {
        if (m_iModes[i] == iHVACMode)
            iIndex = i;
    }

    [m_pMode SetSelectedIndex:iIndex];

    if (m_pFan == NULL)
        return;
    iIndex = 0;
    int iFanState = [m_pTStat FanMode];
    if (iFanState != FAN_STATE_AUTO)
    {
        if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
        {
            switch (iFanState)
            {
            case FAN_STATE_LOW   : iIndex = 1;  break;
            case FAN_STATE_CONT  : iIndex = 2;  break;
            case FAN_STATE_HI    : iIndex = 3;  break;
            }
        }
        else
        {
            iIndex = 1;
        }

    }
    [m_pFan SetSelectedIndex:iIndex];
}
/*============================================================================*/

-(void)InitModeControl

/*============================================================================*/
{
    if (m_pMode != NULL)
    {
        [m_pMode removeFromSuperview];
        [m_pMode release];
        m_pMode = NULL;
    }

    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    int iNModeButtons = 1;
    int iNFanButtons = 0;

    if ([m_pTStat HasFan])
    {
        iNFanButtons = 2;
        if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
            iNFanButtons = 4;
    }

    NSMutableArray *pModes = [[NSMutableArray alloc] init];
    [pModes addObject:@"Off"];
    BOOL bHeat = FALSE;
    BOOL bCool = FALSE;
    if ([m_pTStat HasHeat])
    {
        bHeat = TRUE;
        [pModes addObject:@"Heat"];
        m_iModes[iNModeButtons] = HVAC_STATE_HEAT;
        iNModeButtons++;
    }
    if ([m_pTStat HasCool])
    {
        bCool = TRUE;
        [pModes addObject:@"Cool"];
        m_iModes[iNModeButtons] = HVAC_STATE_COOL;
        iNModeButtons++;
    }

    if (([m_pTStat HVACFlags] & HVAC_HASAUTO) && bHeat && bCool)
    {
        [pModes addObject:@"Auto"];
        m_iModes[iNModeButtons] = HVAC_STATE_AUTO;
        iNModeButtons++;
    }

    //
    // iPod top to bottom
    //
    int iDYButton = DY_BUTTON_IPOD;
    CGRect rMode = CGRectZero;
    CGRect rFan = CGRectZero;

    int iDYButtonMax = DY_BUTTON_IPOD;

    if ([m_pTStat HasFan])
    {
        int iDYAvail = iDYThis - 40;
        iDYButton = MIN(iDYButtonMax, iDYAvail / (iNModeButtons + iNFanButtons));
        int iDYModeSection = iNModeButtons * iDYButton;
        int iDYFanSection = iNFanButtons * iDYButton;
        int iDYExtra = ((iDYThis-40) - iDYModeSection - iDYFanSection)/3;
        rMode = CGRectMake(0, iDYExtra, iDXThis, iDYModeSection);
        rFan = CGRectMake(0, 2*iDYExtra+iDYModeSection, iDXThis, iDYFanSection);
    }
    else
    {
        int iDYAvail = iDYThis - 40;
        iDYButton = MIN(iDYButtonMax, iDYAvail / iNModeButtons);
        int iDYModeSection = iNModeButtons * iDYButton;
        int iDYExtra = (iDYAvail - iDYModeSection)/2;
        rMode = CGRectMake(0, iDYExtra, iDXThis, iDYModeSection);
    }
    
    int iDXInset = DX_INSET_IPOD;
    int iDXLabel = DX_LABEL_IPOD;

    CGRect rLabel = CGRectMake(iDXInset, rMode.origin.y, iDXLabel, rMode.size.height);
    [self AddLabel:rLabel Text:@"Thermostat Mode"];

    CGRect rBtns = CGRectMake(iDXInset+iDXLabel, rMode.origin.y, iDXThis-2*iDXInset-iDXLabel, rMode.size.height);

    m_pMode = [[CHLRadio alloc] initWithFrame:rBtns StringList:pModes NCols:1];
    [pModes release];
    m_pMode.autoresizingMask = 0xFFFFFFFF;
    [self addSubview:m_pMode];
    [m_pMode SetCommandID:CMD_MODE Responder:self];

    if (![m_pTStat HasFan])
        return;
    
    rLabel = CGRectMake(iDXInset, rFan.origin.y, iDXLabel, rFan.size.height);
    [self AddLabel:rLabel Text:@"Fan Mode"];
    
    pModes = [[NSMutableArray alloc] init];
    [pModes addObject:@"Auto"];
    if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
    {
        [pModes addObject:@"Low"];
        [pModes addObject:@"Medium"];
        [pModes addObject:@"High"];
    }
    else
    {
        [pModes addObject:@"Continuous"];
    }

    rBtns = CGRectMake(iDXInset+iDXLabel, rFan.origin.y, iDXThis-2*iDXInset-iDXLabel, rFan.size.height);

    m_pFan = [[CHLRadio alloc] initWithFrame:rBtns StringList:pModes NCols:1];
    [pModes release];
    [m_pFan SetCommandID:CMD_FAN Responder:self];
    m_pFan.autoresizingMask = 0xFFFFFFFF;
    [self addSubview:m_pFan];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_MODE:
        {
            int iIndex = [m_pMode GetSelectedIndex];
            [m_pTStat SetHVACMode:m_iModes[iIndex]];
        }
        break;
        
    case CMD_FAN:
        {
            int iFanMode = FAN_STATE_AUTO;
            
            int iIndex = [m_pFan GetSelectedIndex];

            if (iIndex > 0)
            {
                if ([m_pTStat HVACFlags] & HVAC_3SPEEDFAN)
                {
                    switch (iIndex)
                    {
                    case 1: iFanMode = FAN_STATE_LOW;   break;
                    case 2: iFanMode = FAN_STATE_CONT;  break;
                    case 3: iFanMode = FAN_STATE_HI;    break;
                    }
                }
                else
                {
                    iFanMode = FAN_STATE_CONT;
                }
            }

            [m_pTStat SetFanMode:iFanMode];
        }
        break;

    case CMD_RUN_PROG:
        [m_pTStat SetProgramState:PRG_STATE_RUN]; 
        break;
    }
}
/*============================================================================*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

/*============================================================================*/
{
    if (buttonIndex == 1)
    {
        int iIndex = [m_pMode GetSelectedIndex];
        [m_pTStat SetHVACMode:m_iModes[iIndex]];
    }
    else 
    {
        [self Notify:0 Data:0];
    }
}
/*============================================================================*/

-(void)AddLabel:(CGRect)rLabel Text:(NSString*)pText

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:rLabel];	
    pLabel.textColor = [UIColor whiteColor];
    [CCustomPage InitStandardLabel:pLabel Size:20];
    [pLabel setLineBreakMode:UILineBreakModeWordWrap];
    [pLabel setTextAlignment:UITextAlignmentCenter];
    [pLabel setText:pText];
    [pLabel setNumberOfLines:2];
    pLabel.autoresizingMask = 0xFFFFFFFF;
    [self addSubview:pLabel];
    [pLabel release];
}
@end
