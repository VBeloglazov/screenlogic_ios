//
//  SecZone.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/15/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNSQuery;

/*============================================================================*/

@interface CSecZone : NSObject 

/*============================================================================*/
{
    NSString                            *m_pName;
    int                                 m_iID;
    int                                 m_iCtlrID;
    int                                 m_iFlags;
    int                                 m_iState;
}

-(id)initFromQ:(CNSQuery*)pMSG;

-(NSString*)Name;
-(int)ID;
-(int)GetState;
-(void)SetState:(int)iNew;
-(BOOL)ReadyState;

@end
