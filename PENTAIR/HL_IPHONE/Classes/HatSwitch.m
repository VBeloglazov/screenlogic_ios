//
//  HatSwitch.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HatSwitch.h"
#import "hlcomm.h"
#import "hlm.h"
#import "hlbutton.h"
#import "NSQuery.h"
#import "AudioButton.h"
#import "crect.h"
#import "ControlDef.h"

#define INDEX_UL        0
#define INDEX_UC        1
#define INDEX_UR        2
#define INDEX_L         3
#define INDEX_C         4
#define INDEX_R         5
#define INDEX_LL        6
#define INDEX_LC        7
#define INDEX_LR        8

@implementation CPrimitiveSet
/*============================================================================*/

-(id)initWithDX:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_iDX = iDX;
        m_iDY = iDY;
        m_iSHIN = iSHIN;
        m_iSHOUT = iSHOUT;

        int iDimMin = MIN(iDX, iDY);
        int iRad = (iDimMin / 2 - 1);

        
        m_pMainShader = [[CNSShader alloc] initWithDX:iDX DY:iDY];
        [m_pMainShader InitStandard:iRad-iSHOUT Shading:iSHIN Type:NS_INIT_RAISED_INT Inset:iSHOUT];

        int iInset = iSHIN+iSHOUT+4;
        int iDXHole = iDX/3+2*iInset;
        int iDYHole = iDY/3+2*iInset;
        int iRadHole = MIN(iDXHole, iDYHole)/2-1;

        int iXHole = iDX/2-iDXHole/2;
        int iYHole = iDY/2-iDYHole/2;
        
        CNSShader *pHole = [[CNSShader alloc] initWithDX:iDXHole DY:iDYHole];
        [pHole InitStandard:iRadHole Shading:iSHIN Type:NS_INIT_RAISED_EXT_INV Inset:0];
        [m_pMainShader Subtract:pHole X:iXHole Y:iYHole];
        [pHole release];
        pHole = NULL;
        
        int iDXLR = (iDX-iDXHole)/2+iSHIN+iRadHole;
        int iDYTB = (iDY-iDYHole)/2+iSHIN+iRadHole;
        m_pLWedge = [[CNSShader alloc] initWithDX:iDXLR DY:iDY];
        m_pRWedge = [[CNSShader alloc] initWithDX:iDXLR DY:iDY];
        m_pTWedge = [[CNSShader alloc] initWithDX:iDX DY:iDYTB];
        m_pBWedge = [[CNSShader alloc] initWithDX:iDX DY:iDYTB];
        
        iInset = 4;
        int iDYWedge = iDYHole-2*(iSHIN+iRadHole);
        [m_pLWedge InitWedgeDX:iDY-iInset YR:iDYWedge-iInset Shading:iSHIN SHOUT:iSHOUT Raised:TRUE];
        [m_pRWedge InitWedgeDX:iDYWedge-iInset YR:iDY-iInset Shading:iSHIN SHOUT:iSHOUT Raised:TRUE];

        int iDXWedge = iDXHole-2*(iSHIN+iRadHole);
        
        [m_pTWedge InitWedgeDY:iDX-iInset XB:iDXWedge-iInset Shading:iSHIN SHOUT:iSHOUT Raised:TRUE];
        [m_pBWedge InitWedgeDY:iDXWedge-iInset XB:iDX-iInset Shading:iSHIN SHOUT:iSHOUT Raised:TRUE];

        if (iSHOUT > 0)
        {
            m_pShaderOut = [[CNSShader alloc] initWithDX:m_iDX DY:m_iDY];
            [m_pShaderOut InitStandard:iRad Shading:iSHOUT Type:NS_INIT_RAISED_EXT Inset:0];
            
            int iDXInv = iDXHole+2;
            int iDYInv = iDYHole+2;
            CNSShader *pHoleInv = [[CNSShader alloc] initWithDX:iDXInv DY:iDYInv];
            [pHoleInv InitStandard:iRadHole-iSHIN Shading:iSHOUT Type:NS_INIT_RAISED_INT Inset:iSHIN];
            int iXInv = iDX/2-iDXInv/2;
            int iYInv = iDY/2-iDYInv/2;
            [m_pShaderOut AndWith:pHoleInv X:iXInv Y:iYInv Flip:0];
            [pHoleInv release];
            
            m_pLOut = [[CNSShader alloc] initWithDX:iDXLR DY:iDY];
            m_pROut = [[CNSShader alloc] initWithDX:iDXLR DY:iDY];
            m_pTOut = [[CNSShader alloc] initWithDX:iDX DY:iDYTB];
            m_pBOut = [[CNSShader alloc] initWithDX:iDX DY:iDYTB];
            [m_pLOut InitWedgeDX:iDY-iInset YR:iDYWedge-iInset Shading:iSHOUT SHOUT:0 Raised:FALSE];
            [m_pROut InitWedgeDX:iDYWedge-iInset YR:iDY-iInset Shading:iSHOUT SHOUT:0 Raised:FALSE];
            [m_pTOut InitWedgeDY:iDX-iInset XB:iDXWedge-iInset Shading:iSHOUT SHOUT:0 Raised:FALSE];
            [m_pBOut InitWedgeDY:iDXWedge-iInset XB:iDX-iInset Shading:iSHOUT SHOUT:0 Raised:FALSE];
        }
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
    [m_pMainShader release];
    [m_pLWedge release];
    [m_pRWedge release];
    [m_pTWedge release];
    [m_pBWedge release];
    [m_pLOut release];
    [m_pROut release];
    [m_pTOut release];
    [m_pBOut release];
    [m_pShaderOut release];
    [m_pIntShader1 release];
    [m_pIntShader2 release];
}
/*============================================================================*/

-(BOOL)IsSameSet:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT

/*============================================================================*/
{
    if (iDX != m_iDX) return FALSE;
    if (iDY != m_iDY) return FALSE;
    if (iSHIN != m_iSHIN) return FALSE;
    if (iSHOUT != m_iSHOUT) return FALSE;
    return TRUE;
}
/*============================================================================*/

-(void)InitializeButton:(CHLButton*)pButton Position:(int)iPosition

/*============================================================================*/
{
    int iX = 0;
    int iY = 0;
    int iDX = m_iDX;
    int iDY = m_iDY;

    CNSShader *pAndShaderIn = NULL;
    CNSShader *pAndShaderOut = NULL;

    switch (iPosition)
    {
    case INDEX_L:   
        { 
            pAndShaderIn = m_pLWedge; 
            pAndShaderOut = m_pLOut; 
            iDX = [m_pLWedge DX];
        } 
        break;

    case INDEX_R:   
        { 
            pAndShaderIn = m_pRWedge;
            pAndShaderOut = m_pROut; 
            iDX = [m_pRWedge DX];
            iX = m_iDX - iDX;
        } 
        break;

    case INDEX_UC:  
        { 
            pAndShaderIn = m_pTWedge; 
            pAndShaderOut = m_pTOut; 
            iDY = [m_pTWedge DY];
        } 
        break;

    case INDEX_LC:  
        { 
            pAndShaderIn = m_pBWedge; 
            pAndShaderOut = m_pBOut; 
            iDY = [m_pBWedge DY];
            iY = m_iDY - iDY;
        } 
        break;
    }

    CNSShader *pThisShader = [[CNSShader alloc] initWithDX:iDX DY:iDY];
    [pThisShader BltFromX:0 Y:0 Source:m_pMainShader XSrc:iX YSrc:iY DX:iDX DY:iDY];
    [pThisShader Subtract:pAndShaderIn X:0 Y:0];

    CNSEdgeList *pOuterEdges = NULL;
    CNSEdgeList *pInnerEdges = NULL;

    CNSShader *pThisShaderOut = NULL;
    if (pAndShaderOut != NULL)
    {
        pThisShaderOut = [[CNSShader alloc] initWithDX:iDX DY:iDY];
        [pThisShaderOut BltFromX:0 Y:0 Source:m_pShaderOut XSrc:iX YSrc:iY DX:iDX DY:iDY];
        [pThisShaderOut AndWith:pAndShaderOut X:0 Y:0 Flip:0];

        pInnerEdges = [pThisShader GenEdgeListByElev:15];
        pOuterEdges = [pThisShaderOut GenEdgeListByElevSunken];
    }
    else
    {
        pOuterEdges = [pThisShader GenEdgeListByElev:15];
        pInnerEdges = pOuterEdges;
    }

    CGRect rBounds = [pOuterEdges GetBounds];

    unsigned int rgbStd1   = [CShader MakeRGB:[pButton RGBFace]];
    unsigned int rgbStd2   = rgbStd1;
    unsigned int rgbFlash1 = [CShader MakeRGB:[pButton RGBFlash]];
    unsigned int rgbFlash2 = rgbFlash1;

    int iGradient = [CHLComm GetInt:INT_BUTTON_GRADIENT];
    [CShader MakeSpread:iGradient Color1:&rgbStd1 Color2:&rgbStd2];
    [CShader MakeSpread:iGradient Color1:&rgbFlash1 Color2:&rgbFlash2];

    CGImageRef pRef1 = [pThisShader CreateImageWithColor1:rgbStd1 Color2:rgbStd2  ShadeIn:m_iSHIN ShadeOut:pThisShaderOut Bounds:rBounds];
    CGImageRef pRef2 = [pThisShader CreateImageWithColor1:rgbFlash1 Color2:rgbFlash2 ShadeIn:m_iSHIN ShadeOut:pThisShaderOut Bounds:rBounds];

    CGRect rContent = [pThisShader FindContentRect:pInnerEdges];
    
    if (pOuterEdges != pInnerEdges)
    {
        [pInnerEdges release];
    }
    
    rContent.origin.x -= rBounds.origin.x;
    rContent.origin.y -= rBounds.origin.y;

    [pOuterEdges TranslateDX:-rBounds.origin.x DY:-rBounds.origin.y];

    rBounds.origin.x += iX;
    rBounds.origin.y += iY;


//    rContent = CGRectMake(0, 0, rBounds.size.width, rBounds.size.height);

    [pButton SetRenderDataStd:pRef1 Flash:pRef2 Edges:pOuterEdges ContentRect:rContent];
    [pButton setFrame:rBounds];
    [pOuterEdges release];

    [pThisShaderOut release];
    [pThisShader release];
}
/*============================================================================*/

-(void)InitializeSatelliteButton:(CHLButton *)pButton HatSwitch:(UIView*)pThis

/*============================================================================*/
{
    int iSHIN = [pButton ShadeIn];
    int iSHOUT = [pButton ShadeOut];
    int iRad   = [pButton ROutside];
    int iDXBtn = pButton.frame.size.width;
    int iDYBtn = pButton.frame.size.height;
    int iDimMin = MIN(iDXBtn, iDYBtn);
    iRad = MIN(iRad, iDimMin / 2 - 1);
    
    CNSShader *pShaderIn = [[CNSShader alloc] initWithDX:iDXBtn DY:iDYBtn];
    [pShaderIn InitStandard:iRad Shading:iSHIN Type:NS_INIT_RAISED_INT Inset:iSHOUT];
    
    CNSShader *pShaderOut = NULL;
    if (iSHOUT > 0)
    {
        pShaderOut = [[CNSShader alloc] initWithDX:iDXBtn DY:iDYBtn];
        [pShaderOut InitStandard:iRad+iSHOUT Shading:iSHOUT+1 Type:NS_INIT_RAISED_EXT Inset:0];
    }

    int iPadding = 4 + iSHOUT;
    CNSShader *pIntShaderIn = [self GenIntersectShader:iSHIN Padding:iPadding Inset:0];
    if (pIntShaderIn != NULL)
    {
        int iOffset = iPadding + iSHIN;
        int iXOff = pThis.frame.origin.x - pButton.frame.origin.x - iOffset;
        int iYOff = pThis.frame.origin.y - pButton.frame.origin.y - iOffset;
        [pShaderIn Subtract:pIntShaderIn X:iXOff Y:iYOff];
    }

    iPadding = 4;
    if (pShaderOut != NULL)
    {
        CNSShader *pIntShaderOut = [self GenIntersectShader:-iSHOUT Padding:iPadding Inset:iSHIN];
        if (pIntShaderOut != NULL)
        {
            int iOffset = iPadding + iSHOUT;
            int iXOff = pThis.frame.origin.x - pButton.frame.origin.x - iOffset;
            int iYOff = pThis.frame.origin.y - pButton.frame.origin.y - iOffset;
            [pShaderOut AndWith:pIntShaderOut X:iXOff Y:iYOff Flip:0];

            // XOR?
        }
    }

    unsigned int rgbStd1 = [CShader MakeRGB:[pButton RGBFace]];
    unsigned int rgbStd2 = rgbStd1;
    unsigned int rgbFlash1 = [CShader MakeRGB:[pButton RGBFlash]];
    unsigned int rgbFlash2 = rgbFlash1;

    int iGradient = [CHLComm GetInt:INT_BUTTON_GRADIENT];
    [CShader MakeSpread:iGradient Color1:&rgbStd1 Color2:&rgbStd2];
    [CShader MakeSpread:iGradient Color1:&rgbFlash1 Color2:&rgbFlash2];

    CGRect rBounds = CGRectMake(0, 0, iDXBtn, iDYBtn);
    CGImageRef pRef1 = [pShaderIn CreateImageWithColor1:rgbStd1 Color2:rgbStd2 ShadeIn:m_iSHIN ShadeOut:pShaderOut Bounds:rBounds];
    CGImageRef pRef2 = [pShaderIn CreateImageWithColor1:rgbFlash1 Color2:rgbFlash2 ShadeIn:m_iSHIN ShadeOut:pShaderOut Bounds:rBounds];

    CNSEdgeList *pEdgeList = [pShaderIn GenEdgeListByElev:15];
    CGRect rContent = [pShaderIn FindContentRect:pEdgeList];

    [pButton SetRenderDataStd:pRef1 Flash:pRef2 Edges:pEdgeList ContentRect:rContent];

    [pShaderIn release];
    [pShaderOut release];
}
/*============================================================================*/

-(CNSShader*)GenIntersectShader:(int)iNShading Padding:(int)iPadding Inset:(int)iShadeInset

/*============================================================================*/
{
    int iUserData = iNShading;
    BOOL bSunken = FALSE;
    if (iNShading < 0)
    {
        bSunken = TRUE;
        iNShading = -iNShading;
    }

    int iDimMin = MIN(m_iDX, m_iDY);
    int iRad =  (iDimMin / 2 - 1);
    iRad = iRad + iNShading + iPadding;
    int iDX = m_iDX + 2 * (iPadding+iNShading);
    int iDY = m_iDY + 2 * (iPadding+iNShading);
    int iInset = 0;
    int iType = NS_INIT_RAISED_INT;

    if (!bSunken)
    {
        iType = NS_INIT_RAISED_EXT_INV;
        if (m_pIntShader1 != NULL)
        {
            if ([m_pIntShader1 DX] == iDX && [m_pIntShader1 DY] == iDY && [m_pIntShader1 GetUserData] == iUserData)
                return m_pIntShader1;
            [m_pIntShader1 release];
        }
    }
    else
    {
        iType = NS_INIT_RAISED_INT_INV;
        iInset = iShadeInset;
        if (m_pIntShader2 != NULL)
        {
            if ([m_pIntShader2 DX] == iDX && [m_pIntShader2 DY] == iDY && [m_pIntShader2 GetUserData] == iUserData)
                return m_pIntShader2;
            [m_pIntShader2 release];
        }
    }

    CNSShader *pShader = [[CNSShader alloc] initWithDX:iDX DY:iDY];
    [pShader SetUserData:iUserData];

    [pShader InitStandard:iRad Shading:iNShading Type:iType Inset:0];


    if (bSunken)
        m_pIntShader2 = pShader;
    else 
        m_pIntShader1 = pShader;

    return pShader;
}

@end

@implementation CHatSwitch

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame 
        Style:(int)iStyle 
        Data:(int)iData 
        Text:(NSString*)pText 
        TextColor:(UIColor*)pRGBText 
        Type:(int)iType 
        Flags:(int)iFlags
        ZoneID:(int)iZoneID
        ControlDef:(CControlDef*)pControlDef

/*============================================================================*/
{
    m_iData = iData;
    m_iZoneID = iZoneID;

    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_iData = iData;
        m_iSHIN     = [CHLComm GetInt:INT_BUTTON_SHADEIN];
        m_iSHOUT    = [CHLComm GetInt:INT_BUTTON_SHADEOUT];
        

        int iFlags = [pControlDef Flags];
        if ((iFlags & CONTROL_FLAG_DEFAULT_SHIN) == 0)
            m_iSHIN = [pControlDef ShadeIn];
        if ((iFlags & CONTROL_FLAG_DEFAULT_SHOUT) == 0)
            m_iSHOUT = [pControlDef ShadeOut];
        if ((iFlags & CONTROL_FLAG_DEFAULT_FACE_RGB) == 0)
        {
            m_pColor = [pControlDef RGBFace];
            [m_pColor retain];
        }

        int iIndex = 0;
        for (int iRow = 0; iRow < 3; iRow++)
        {
            for (int iCol = 0; iCol < 3; iCol++)
            {
                BOOL bButton = TRUE;
                if (iRow == 0 || iRow == 2)
                {
                    if (iCol == 0 || iCol == 2)
                    {
                        switch (iType)
                        {
                        case CONTROL_HATSWITCH_COMPLEX:
                            bButton = TRUE;
                            break;
                            
                        default:
                            bButton = FALSE;
                        }
                    }
                }

                if (bButton)
                {
                    int iIconFormat = HL_ICONCENTER;
                    int iIconStyle  = AUDIO_ICON_TEXT;
                    int iCmdID      = -1;
                    NSString *pBtnText = @"";

                    switch (iIndex)
                    {
                    case 0: { iIconStyle = AUDIO_ICON_RWND;         iCmdID = CMD_KEY_REW;   }   break;
                    case 2: { iIconStyle = AUDIO_ICON_FFWD;         iCmdID = CMD_KEY_FF;    }   break;
                    case 6: { iIconStyle = AUDIO_ICON_SKIP_BACK;    iCmdID = CMD_KEY_SKIPR; }   break;
                    case 8: { iIconStyle = AUDIO_ICON_SKIP_FWD;     iCmdID = CMD_KEY_SKIPF; }   break;


                    case 1: { iIconStyle = AUDIO_ICON_UP;     iCmdID = CMD_KEY_UP;      }    break;
                    case 3: { iIconStyle = AUDIO_ICON_LEFT;   iCmdID = CMD_KEY_LEFT;    }    break;
                    case 5: { iIconStyle = AUDIO_ICON_RIGHT;  iCmdID = CMD_KEY_RIGHT;   }    break;
                    case 7: { iIconStyle = AUDIO_ICON_DOWN;   iCmdID = CMD_KEY_DOWN;    }    break;

                    case 4: 
                        {
                            if (iStyle == 1)
                            {
                                iCmdID = CMD_KEY_SELECT;
                                pBtnText = @"Select";
                            }
                            else
                            {
                                iCmdID = CMD_KEY_ENTER;
                                pBtnText = @"Enter";
                            }
                        }
                    }
            

                    CHLButton *pBtn = NULL;
                    if (iFlags & CONTROL_FLAG_DEFAULT_BEHAVIOUR)
                    {
                        pBtn = [[CAudioButton alloc]    initWithFrame: CGRectZero
                                                        IconFormat:(HL_HCENTER|HL_VCENTER)
                                                        Style:iIconStyle 
                                                        Text:pBtnText 
                                                        TextSize:0 
                                                        TextColor:pRGBText 
                                                        Color:m_pColor 
                                                        Icon:NULL 
                                                        Data:m_iData
                                                        Flags:CONTROL_FLAG_DEFAULT_BEHAVIOUR|CONTROL_FLAG_HATBUTTON
                                                        Function:iCmdID
                                                        ZoneID:m_iZoneID ];
                    }
                    else 
                    {
                        pBtn = [[CHLButton alloc]   initWithFrame:CGRectZero
                                                    IconFormat:iIconFormat 
                                                    Style:iIconStyle 
                                                    Text:pBtnText
                                                    TextSize:0
                                                    TextColor:pRGBText
                                                    Color:m_pColor
                                                    Icon:NULL ];

                        [pBtn SetCommandID:iCmdID Responder:self];
                    }

                    m_pBtns[iIndex] = pBtn;

                    [self addSubview:pBtn];
                    [pBtn release];
                }

                iIndex++;
            }
        }
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pPrimitives release];
    [m_pColor release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    if (m_pPrimitives != NULL)
    {
        if (![m_pPrimitives IsSameSet:iDXThis DY:iDYThis SHIN:m_iSHIN SHOUT:m_iSHOUT])
        {
            [m_pPrimitives release];
            m_pPrimitives = NULL;
        }
    }
    
    if (m_pPrimitives == NULL)
    {
        m_pPrimitives = [[CPrimitiveSet alloc] initWithDX:iDXThis DY:iDYThis SHIN:m_iSHIN SHOUT:m_iSHOUT];
    }


    int iInset = m_iSHIN+m_iSHOUT+4;
    int iDXHole = iDXThis / 3 + 2 * iInset;
    int iDYHole = iDYThis / 3 + 2 * iInset;
    int iXMid = iDXThis/2;
    int iYMid = iDYThis/2;
    int iRadHole = MIN(iDXHole, iDYHole)/2-1;
    
    CGRect rHole = CGRectMake(iXMid-iDXHole/2, iYMid-iDYHole/2, iDXHole, iDYHole);
    CGRect rCenterButton = rHole;
    [CRect Inset:&rCenterButton DX:iInset DY:iInset];
    [m_pBtns[INDEX_C] setFrame:rCenterButton];
    [m_pBtns[INDEX_C] SetROutside:iRadHole-iInset];

    for (int i = 0; i < 9; i++)
    {
        [m_pBtns[i] SetShadeIn:m_iSHIN];
        [m_pBtns[i] SetShadeOut:m_iSHOUT];
        
        if (m_pBtns[i] != NULL && i != INDEX_C)
        {
            [m_pPrimitives InitializeButton:m_pBtns[i] Position:i];
        }
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_EXECDEVICEKEYQ];
    [pQ autorelease];
    [pQ PutInt:m_iData];
    [pQ PutInt:iCommandID];
    [pQ PutInt:m_iZoneID];
    [pQ SendMessage];
}
/*============================================================================*/

-(int)Radius

/*============================================================================*/
{
    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    int iDimMin = MIN(iDX, iDY);
    return (iDimMin / 2 - 1);
}
/*============================================================================*/

-(void)InitializeSatelliteButton:(CHLButton*)pButton

/*============================================================================*/
{
    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;

    if (m_pPrimitives != NULL)
    {
        if (![m_pPrimitives IsSameSet:iDX DY:iDY SHIN:m_iSHIN SHOUT:m_iSHOUT])
        {
            [m_pPrimitives release];
            m_pPrimitives = NULL;
        }
    }

    if (m_pPrimitives == NULL)
    {
        m_pPrimitives = [[CPrimitiveSet alloc] initWithDX:iDX DY:iDY SHIN:m_iSHIN SHOUT:m_iSHOUT];
    }


    [m_pPrimitives InitializeSatelliteButton:pButton HatSwitch:self];
}
@end
