//
//  SecurityHistory.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "timeview.h"

#define SECURITY_DY_ITEM_IPOD              28
#define SECURITY_DY_ITEM_IPAD              32
#define SECURITY_ITEM_INSET                2

@class CSecPartition;

/*============================================================================*/

@interface CSecurityHistoryView : CTimeView

/*============================================================================*/
{
    CSecPartition               *m_pPartition;
}

-(id)initWithFrame:(CGRect)frame Partition:(CSecPartition*)pPartition;
-(int)DataViewDY;
-(void)NotifyRotation;

+(int)DYItem;

@end
