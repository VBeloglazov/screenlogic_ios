/*
--------------------------------------------------------------------------------

    SHADEDIB.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef SHADEDIB_H
#define SHADEDIB_H

#define SHADEDIB_3D                     0
#define SHADEDIB_ELEV                   1
#define SHADEDIB_TRACKRECT              0x00001000
#define SHADEDIB_INIT                   0x00002000
#define SHADEDIB_DIRECT                 0x00004000

#define SHADEDIB_HINT_OVERSCAN          0x00000001
#define SHADEDIB_HINT_INT_OVER_EXT      0x00000002
#define SHADEDIB_DITHER_HIGH_EDGE       0x00000004
#define SHADEDIB_DITHER_LOW_EDGE        0x00000008

#define PIXEL_HZ                0x4000
#define PIXEL_VT                0x8000
#define PIXEL_ORTHO             (PIXEL_HZ|PIXEL_VT)


#define FLIP_NONE               0x0000
#define FLIP_HZ                 0x0001
#define FLIP_VT                 0x0002
#define FLIP_BOTH               0x0003

#ifdef IPHONE
class CRectList;
class CEdgeList;
#endif

class CDitherMap;
class CGradient1D;


    typedef struct
    {
        int                         m_iX0;
        int                         m_iX1;
    } HLT_SCANLINE_ROI;

    typedef struct
    {
        int                         m_iShine;
        int                         m_iElev_16;
        int                         m_iMaxElev16;
        int                         m_iDXC;
        int                         m_iDYC;
        HL_PIXEL                    m_wColor12;
        HL_PIXEL                    m_wColor34;
    } HLT_PIXEL_VARS;

    typedef void (ShaderFunc)( HLT_PIXEL_VARS *pVars, HL_PIXEL *pPIX);

/*========================================================================*/

    class CShadeDIB : public CHLDibDC

/*========================================================================*/
{
public:
    enum INIT_TYPE
    {
        INIT_RAISED_INT,
        INIT_RAISED_EXT, 
        INIT_RAISED_INT_INV,
        INIT_RAISED_EXT_INV,
        INIT_RAISED_INT_GRADIENT
    };

protected:
    int                         m_iMode;
    CRectList                   m_Solid;
    CRectList                   m_ROI;

    static HL_PIXEL             g_FillMap[256];
    static COLORREF             g_FillRGB;
    static HL_PIXEL             g_wFillRGB;
    static int                  g_wRsolid;
    static int                  g_wGsolid;
    static int                  g_wBsolid;
    static int                  g_iHints;

    DWORD                       m_iRBT;
    DWORD                       m_iGT;
    DWORD                       m_iRBB;
    DWORD                       m_iGB;

public:
    CShadeDIB(
    int                         iDX,
    int                         iDY,    
    int                         iMode = SHADEDIB_3D);

    CShadeDIB(
    CHLDibDC                    *pParentDIB,
    int                         iXOffset,
    int                         iYOffset,
    int                         iDX,
    int                         iDY,    
    int                         *piDX,
    int                         *piDY,
    int                         iMode = SHADEDIB_3D);

    void                        AddCornersQ23(  
    int                         iRad,
    int                         iNShading,
    int                         iX,
    int                         iYT,
    int                         iYB,
    INIT_TYPE                   Type );

    void                        FlipHzCopy(
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY );

    void                        FlipHz();
    void                        FlipVt();

    void                        InitStandard(
    int                         iRad,
    int                         iNShading,
    INIT_TYPE                   Type,
    int                         iEdgeInset,
    COLORREF                    rgb     = 0x0000,
    int                         iHints  = 0 );

    void                        ApplyDirectColor(
    int                         iRad,
    int                         iNShading,
    int                         iEdgeInset,
    COLORREF                    rgbT,
    COLORREF                    rgbB,
    int                         iFlags);

    void                        CorrectRounding(
    CShadeDIB                   *pShaderOut );

    void                        CorrectSubPixelAlpha(
    CShadeDIB                   *pOther,
    int                         iXDst,
    int                         iYDst );

    void                        InitWedgeDX(
    int                         iDYL,
    int                         iDYR,
    int                         iNShading,
    int                         iNShadeOut,
    BOOL                        bRaised );

    void                        InitWedgeDY(
    int                         iDXT,
    int                         iDXB,
    int                         iNShading,
    int                         iNShadeOut,
    BOOL                        bRaised );
    
    static void                 SetFillColor(
    COLORREF                    rgb );

    static void                 ShadeValueQ12_ELEV(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ34_ELEV(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ12_ELEVDirectColor(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ34_ELEVDirectColor(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ12_ELEVDirectColorGrad(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ34_ELEVDirectColorGrad(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ12_ELEVDirect(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    static void                 ShadeValueQ34_ELEVDirect(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX );

    void                        Fill(
    HL_PIXEL                    wSet,
    CHLRect                     rect );

    void                        FillGradient(
    CHLRect                     rect,
    int                         iInsets );

    void                        Subtract(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst );

    void                        Subtract(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iXSrc,
    int                         iYSrc );

    void                        Add(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst );

    void                        Add(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iXSrc,
    int                         iYSrc );

    void                        And(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst,
    int                         iFlipSrc );

    void                        XOR(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst );

    void                        AndEdgeElev(
    CHLRect                     rRect,
    int                         iDir,
    int                         iNShading,
    BOOL                        bAND = TRUE );

    void                        ApplyTo(
    CHLDibDC                    *pDIB,
    int                         iXDst = 0,
    int                         iYDst = 0 );

    void                        ApplyTo(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

    void                        ApplyShine(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iAlphaT,
    int                         iAlphaB,
    int                         iRMinor );
    
    void                        ApplyShineScanLine(
    CHLDibDC                    *pDIB,
    int                         iX,
    int                         iY,
    int                         iDX,
    int                         iA,
    int                         iR,
    int                         iFeatherL,
    int                         iFeatherR);

    void                        ApplyShineyGradient(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst );

    void                        ApplyShineyScanLine(
    CHLDibDC                    *pDIB,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iAlpha,
    int                         iYInRadius,
    int                         iRadius );    

    void                        ApplyToWithColor(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iMaxElev,
    COLORREF                    rgb);

    void                        ApplyToWithColor(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iMaxElev,
    HL_PIXEL                    wRGB );

    void                        ApplyToWithColors(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iMaxElev,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iHints );

    void                        ApplyToWithColors(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iMaxElev,
    CGradient1D                 *pGradient,
    int                         iHints);

    void                        ApplyToElev(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    COLORREF                    rgbMax,
    COLORREF                    rgbMin,
    int                         iRange  );

    void                        ApplyToElev(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    COLORREF                    rgbMax,
    COLORREF                    rgbMin,
    int                         iRange  );

    int                         FillOrthoElev(
    CHLDibDC                    *pDIB, 
    HL_PIXEL                    *pSrc,
    HL_PIXEL                    *pTgt,
    int                         iX,
    int                         iY,
    int                         iDXMax,
    int                         iDYMax,
    int                         iPitchTgt,
    CDitherMap                  *pMap );

    CEdgeList                   *GenEdgeListByElev(int iMinElev);
    CEdgeList                   *GenEdgeListByElevSunken();
    CEdgeList                   *GenEdgeListByFloor();
    CEdgeList                   *GenEdgeListByBoundingElevRaised();
    CEdgeList                   *GenEdgeListByBoundingElevSunken();

    CHLRect                     FindContentRect(
    CEdgeList                   *pEdges );

    #ifdef DEBUG
        void                    DebugElev(
        CHLDC                   *pDC,
        int                     iMaxElev );

        void                    DebugShade(
        CHLDC                   *pDC,
        int                     iMaxElev );

        void                    DebugROI(
        CHLDC                   *pDC );

        void                    DebugSolid(
        CHLDC                   *pDC );
    #endif

    BOOL                        IsInterior(
    INIT_TYPE                   Type );

    ShaderFunc                  *ShaderFor(
    int                         iQ,
    INIT_TYPE                   Type );

    int                         MaxShineInt(
    int                         iMaxElev );

    HLT_SCANLINE_ROI            GetScanlineROIQ2(
    int                         iY,
    int                         iRad,
    int                         iRad2_Plus,
    int                         iNShading );

    HL_PIXEL                    GradientAt(
    int                         iY,
    int                         iInsets );
};
#endif
