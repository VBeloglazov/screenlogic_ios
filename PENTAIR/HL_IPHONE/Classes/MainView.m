//
//  MainView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/20/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "MainView.h"
#import "hlcomm.h"
#import "hlm.h"

    #define DEFAULT_TEXT_SIZE_IPAD          24
    #define DY_TOOLBAR_IPAD                 88
    #define DY_TOOLBAR_IPAD_LARGE           140
    #define DY_TOOLBAR_SUB_IPAD             64
    #define DY_SUBTABCELL_IPAD              88
    #define DY_MAX_MAINTABCELL_IPAD         88
    #define G_BUTTON_DX_IPAD                60
    #define DX_MEDIA_CONTROLS_IPAD          100
    #define DY_MEDIA_CONTROLS_IPAD          88
    #define DY_MEDIA_CONTROL_SPACING_IPAD   20
    #define MAINTABCELL_INSET_X_IPAD        15
    #define DY_TIMEBAR_IPAD                 80
    #define TEXT_SIZE_TITLE_IPAD            32
    #define TEXT_SIZE_SIDEBAR_IPAD          16
    #define DX_SIDEBAR_IPAD                 150
    
    #define DEFAULT_TEXT_SIZE_IPOD          16
#ifdef PENTAIR
    #define DY_TOOLBAR_IPOD                 44
    #define DY_TOOLBAR_MAIN_IPOD            52
#else
    #define DY_TOOLBAR_IPOD                 44
    #define DY_TOOLBAR_MAIN_IPOD            52
#endif
    #define DY_TOOLBAR_IPOD_LARGE           64
    #define DY_TOOLBAR_SUB_IPOD             32
    #define DY_SUBTABCELL_IPOD              44

#ifdef PENTAIR
    #define DY_MAX_MAINTABCELL_IPOD         80
#else
    #define DY_MAX_MAINTABCELL_IPOD         60
#endif
    #define G_BUTTON_DX_IPOD                40
    #define DX_MEDIA_CONTROLS_IPOD          80
    #define DY_MEDIA_CONTROLS_IPOD          44
    #define DY_MEDIA_CONTROL_SPACING_IPOD   5
    #define MAINTABCELL_INSET_X_IPOD        5
    #define DY_TIMEBAR_IPOD                 45
    #define TEXT_SIZE_TITLE_IPOD            18
    #define TEXT_SIZE_SIDEBAR_IPOD          10
    #define DX_SIDEBAR_IPOD                 80

@implementation CMainView

/*============================================================================*/

- (id)initWithFrame:(CGRect)aRect

/*============================================================================*/
{
    self = [super initWithFrame:aRect];

    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)dealloc {
    [super dealloc];
}
/*============================================================================*/

+(int)DEFAULT_TEXT_SIZE                 {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DEFAULT_TEXT_SIZE_IPAD; return DEFAULT_TEXT_SIZE_IPOD; }
+(int)DY_SUBTABCELL                     {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DY_SUBTABCELL_IPAD; return DY_SUBTABCELL_IPOD;  }
+(int)DY_MAX_MAINTABCELL                {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DY_MAX_MAINTABCELL_IPAD; return DY_MAX_MAINTABCELL_IPOD;   }
+(int)G_BUTTON_DX                       {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return G_BUTTON_DX_IPAD; return G_BUTTON_DX_IPOD; }
+(int)DX_MEDIA_CONTROLS                 {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DX_MEDIA_CONTROLS_IPAD; return DX_MEDIA_CONTROLS_IPOD; }
+(int)DY_MEDIA_CONTROLS                 {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DY_MEDIA_CONTROLS_IPAD; return DY_MEDIA_CONTROLS_IPOD; }
+(int)DY_MEDIA_CONTROL_SPACING          {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DY_MEDIA_CONTROL_SPACING_IPAD; return DY_MEDIA_CONTROL_SPACING_IPOD;   }
+(int)MAINTABCELL_INSET_X               {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return MAINTABCELL_INSET_X_IPAD; return MAINTABCELL_INSET_X_IPOD; }
+(int)DY_TIMEBAR                        {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DY_TIMEBAR_IPAD; return DY_TIMEBAR_IPOD; }
+(int)TEXT_SIZE_TITLE                   {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return TEXT_SIZE_TITLE_IPAD; return TEXT_SIZE_TITLE_IPOD; }
+(int)TEXT_SIZE_SIDEBAR                 {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return TEXT_SIZE_SIDEBAR_IPAD; return TEXT_SIZE_SIDEBAR_IPOD; }
+(int)DX_SIDEBAR                        {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return DX_SIDEBAR_IPAD; return DX_SIDEBAR_IPOD; }

/*============================================================================*/

+(int)DY_TOOLBAR                        

/*============================================================================*/
{   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
    {
        // Base on a 800x600 touchscreen
        int iPCT = [CHLComm GetInt:INT_TOPBAR_SIZEPCT];
        iPCT = MIN(42, iPCT);
        return iPCT * 768 / 400;
    }
    return DY_TOOLBAR_IPOD;   
}
/*============================================================================*/

+(int)DY_TOOLBAR_LARGE

/*============================================================================*/
{   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
    {
        // Base on a 800x600 touchscreen
        int iPCT = [CHLComm GetInt:INT_TOPBAR_SIZEPCT] * 180 / 100;
        iPCT = MIN(60, iPCT);
        return iPCT * 768 / 400;
    }
    return DY_TOOLBAR_IPOD_LARGE;   
}
/*============================================================================*/

+(int)DY_TOOLBAR_SUB

/*============================================================================*/
{   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
    {
        // Base on a 800x600 touchscreen
        int iPCT = [CHLComm GetInt:INT_TOPBAR_SIZEPCT];
        return iPCT * 600 / 400;
    }
    return DY_TOOLBAR_SUB_IPOD;   
}
/*============================================================================*/

+(int)DY_TOOLBAR_MAIN

/*============================================================================*/
{   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
    {
        return [self DY_TOOLBAR];
    }

    return DY_TOOLBAR_MAIN_IPOD;   
}
/*============================================================================*/

+(int)DY_TOOLBAR_TEXT

/*============================================================================*/
{
    int iRet = [CMainView DY_TOOLBAR];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
        iRet = iRet * 65 / 100;
    return iRet;
}

@end
