//
//  SecurityPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/15/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import "Sink.h"

@class CSecPartition;
@class CSecKeypad;
@class CSecurityHistoryView;
@class CHLSegment;

/*============================================================================*/

@interface CSecurityPage : CSubSystemPage <CSink>

/*============================================================================*/
{
//    UITabBar                            *m_pTabBar;
    CHLSegment                         *m_pPageCtrl;
    BOOL                                m_bInit;
    CSecPartition                       *m_pPartition;
    UILabel                             *m_pStatusText;
    UIView                              *m_pMainView;
    UIView                              *m_pKeypad;
    UIView                              *m_pZonePage;
    CSecurityHistoryView                *m_pHistPage;
    UIView                              *m_pActivePage;
    int                                 m_iLastIndex;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)Init:(CSubTab*)pSubTab;

@end
