//
//  HVACTempPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "hlview.h"

@class CTStat;
@class CHVACTempView;
@class CHLRadio;
@class CHVACModePage_IPAD;

/*============================================================================*/

@interface CHVACTempPage : UIView < CSink, CHLView >

/*============================================================================*/
{
    int                                 m_iHVACState;

    CTStat                              *m_pTStat;
    CHVACTempView                       *m_pRoom;
    CHVACTempView                       *m_pCool;
    CHVACTempView                       *m_pHeat;
    
    int                                 m_iDXLast;
    int                                 m_iDYLast;

    BOOL                                m_bReadOnly;

    CHVACModePage_IPAD                       *m_pModePage;
}

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat ReadOnly:(BOOL)bReadOnly;
-(void)UpdateControls;
-(CGRect)MakeRectAt:(CGPoint)ptC DX:(int)iDX DY:(int)iDY;

@end
