/*
--------------------------------------------------------------------------------

    ASSERTTRACE.H : CRYSTALPAD

    Copyright 2002 HomeLogic, Marblehead, MA 01945

This file has helpers for debugging / tracing.

The first two functions are used in conjunction with the macros in hlstd.h.
Please refer to the style guide for behavior of ASSERT, TRACE and Unhandled Exceptions

--------------------------------------------------------------------------------
*/

#ifndef ASSERTTRACE_H
#define ASSERTTRACE_H


    //
    // Global assert wrapper.
    //
    extern void                 AssertDlg(
    const char                  *psFile,
    int                         iLine,
    BOOL                        bLogAssert);

    extern void                 TraceWrap(
    const TCHAR                 *pString );

    //
    // Global crash handler.
    //
    
#endif

