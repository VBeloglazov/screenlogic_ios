//
//  XPAD_VIEWAppDelegate.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import "XPAD_VIEWViewController.h"
#import "tconnect.h"
#import "hlcomm.h"
#import "nsquery.h"
#import "NSMSG.h"
#import "maintab.h"
#import "MainTabCell.h"
#import "MainTabPage.h"
#import "SystemMode.h"
#import "hlbutton.h"
#import "SubTab.h"
#import "SubTabCell.h"
#import "HVACMainTabPage.h"
#import "HLToolBar.h"
#import <UIKit/UINavigationBar.h>
#import "MainView.h"

@implementation XPAD_VIEWViewController

@synthesize m_pMainTabs, m_pTableView;

    static int                      g_iBackgroundStyle = 0;


#define CMD_MODES                       1000

/*============================================================================*/

- (void)awakeFromNib

/*============================================================================*/
{
    m_iMainTabHeight    = DY_MAX_MAINTABCELL;
    m_bInit             = TRUE;
    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(CheckConnectTimer) userInfo:nil repeats:YES] retain];
	self.m_pMainTabs = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];

    [self.view setOpaque:NO];
    [self.view setBackgroundColor:[UIColor clearColor]];

    CGRect rTableView = CGRectMake(0, DY_TOOLBAR, self.view.frame.size.width, self.view.frame.size.height-DY_TOOLBAR);
    m_pTableView = [[UITableView alloc] initWithFrame:rTableView style:UITableViewStylePlain];

    // set the autoresizing mask so that the table will always fill the view
    m_pTableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
    
    // set the tableview delegate to this object and the datasource to the datasource which has already been set
    m_pTableView.delegate = self;
    m_pTableView.dataSource = self;
    m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIScrollView *pScroller = m_pTableView;
    pScroller.delegate = self;

    m_pTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:m_pTableView];

    CGRect rToolbarTop = CGRectMake(0, 0, self.view.frame.size.width, DY_TOOLBAR);
    m_pToolBar = [[CHLToolBar alloc] initWithFrame:rToolbarTop ViewController:self];
    [self.view addSubview:m_pToolBar];

    CGRect rTitle = CGRectMake(4, 0, self.view.frame.size.width - 120, DY_TOOLBAR);

    m_pTitleView = [[UILabel alloc] initWithFrame:rTitle];
    [m_pToolBar addSubview:m_pTitleView];
    [m_pTitleView setText:@""];
    m_pTitleView.opaque = NO;
    m_pTitleView.textColor = [UIColor whiteColor];
    m_pTitleView.backgroundColor = [UIColor clearColor];
    m_pTitleView.font = [UIFont boldSystemFontOfSize:DY_TOOLBAR/2];
    m_pTitleView.textAlignment = UITextAlignmentCenter;
    
    int iDXThis = self.view.bounds.size.width;
    
    int iDXLabel = iDXThis / 3;
    CGRect rInfo = CGRectMake(iDXThis-iDXLabel, 2, iDXLabel, DY_TOOLBAR-4);
    m_pInfoText = [[UILabel alloc] initWithFrame:rInfo];

    m_pInfoText.opaque = NO;
    m_pInfoText.textColor = [UIColor whiteColor];
    m_pInfoText.backgroundColor = [UIColor clearColor];
    m_pInfoText.font = [UIFont boldSystemFontOfSize:14];
    m_pInfoText.textAlignment = UITextAlignmentCenter;
    m_pInfoText.lineBreakMode = UILineBreakModeWordWrap;
    m_pInfoText.numberOfLines = 3;

    [m_pToolBar addSubview:m_pInfoText];

    [m_pTitleView release];

    [self.view setOpaque:NO];
    [self.view setBackgroundColor:[UIColor clearColor]];
}
/*============================================================================*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 

/*============================================================================*/
{
    if (m_pConnectPage != NULL)
        return FALSE;

	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
/*============================================================================*/

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
}
/*============================================================================*/

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
}
/*============================================================================*/

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation

/*============================================================================*/
{
    if (self.interfaceOrientation == UIDeviceOrientationPortrait)
    {
        UIEdgeInsets insets = m_pTableView.contentInset;
        CGPoint ptOrg = CGPointMake(0, -insets.top);
        [m_pTableView setContentOffset:ptOrg animated:YES];
    }
}
/*============================================================================*/

- (void)didReceiveMemoryWarning 

/*============================================================================*/
{
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}
/*=======================-=====================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pTimer invalidate];
    [m_pLogoTimer invalidate];

    [m_pConnectPage release];
    [m_pTimer release];
    [m_pLogoTimer release];
    [m_pTableView release];
	[m_pMainTabs release];
    [m_pInfoText release];
    [m_pModeList release];
    [m_pLockView release];
    [m_pToolBar release];
	[super dealloc];    
}
/*============================================================================*/

-(void)CheckConnectTimer

/*============================================================================*/
{
    if (m_bInit)
    {
        [CHLComm AddSocketSink:self];
        m_bInit = FALSE;
    }

    if (m_pConnectPage == NULL)
    {
        if (![CHLComm Connected] && ![CHLComm IsConnecting])
        {
            [self ShowConnectPage];
        }
    }
    else
    {
        if ([CHLComm Connected] && ![CHLComm IsConnecting])
        {
            [self HideConnectPage];
        }
    }
}
/*============================================================================*/

-(void)HideLoginInfo

/*============================================================================*/
{
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [m_pInfoText setAlpha:0.0];
    CGRect rLogo = m_pTitleView.frame;
    #ifdef PENTAIR
        rLogo = CGRectMake(32, 4, 320-64, DY_TOOLBAR-8);
    #else
        rLogo.origin.x = m_pToolBar.frame.size.width / 2 - rLogo.size.width / 2;
    #endif

    [m_pTitleView setFrame:rLogo];

	[UIView commitAnimations];
}
/*============================================================================*/

- (void)ShowConnectPage

/*============================================================================*/
{
    if (m_pConnectPage != NULL)
        return;

//    [[self navigationController] popToRootViewControllerAnimated:NO];

    m_pConnectPage = [[CConnectPage alloc] initWithNibName:@"tconnect" bundle:nil Orientation:self.interfaceOrientation];
    m_pConnectPage.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];


//    CGRect rFramePage = m_pConnectPage.view.frame;
//    rFramePage.origin.y = self.view.frame.size.height;
//    m_pConnectPage.view.frame = rFramePage;

//    [m_pConnectPage.view setAlpha:0];

    UIView *pView = self.navigationController.topViewController.view;


	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];

    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:pView cache:NO];
    [pView addSubview:m_pConnectPage.view];
    [m_pConnectPage.view setAlpha:1.0];
    [m_pTableView setAlpha:0.0];
    [m_pToolBar setAlpha:0.0];

//    CGRect rFrame = m_pConnectPage.view.frame;
//    rFrame.origin.y = 0;
//    m_pConnectPage.view.frame = rFrame;

	[UIView commitAnimations];
}
/*============================================================================*/

- (void)HideConnectPage

/*============================================================================*/
{
    if (m_pConnectPage == NULL)
        return;
    [m_pConnectPage ReleaseAll];

	// present page six as a modal child or overlay view
    CGRect rTableView = m_pTableView.frame;
    rTableView.origin.y = -rTableView.size.height;
    m_pTableView.frame = rTableView;

    UIView *pView = self.navigationController.topViewController.view;

	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:pView cache:NO];
    [m_pConnectPage.view removeFromSuperview];
    CGRect rFrame = m_pToolBar.frame;
    rFrame.origin.y = 0;
    [m_pToolBar setFrame:rFrame];
    [m_pTableView setAlpha:1.0];
    [m_pToolBar setAlpha:1.0];

    rTableView.origin.y = DY_TOOLBAR;
    m_pTableView.frame = rTableView;

	[UIView commitAnimations];


	[[self navigationController] dismissModalViewControllerAnimated:YES];
    [m_pConnectPage release];
    m_pConnectPage = nil;
}
#pragma mark UINavigationBar delegates
/*============================================================================*/

/*============================================================================*/

#pragma mark UIViewController delegates

/*============================================================================*/

- (void)viewWillAppear:(BOOL)animated

/*============================================================================*/
{
	NSIndexPath *tableSelection = [m_pTableView indexPathForSelectedRow];
	[m_pTableView deselectRowAtIndexPath:tableSelection animated:YES];

    if (m_pExpandedTab != NULL)
    {
        int iIndex = [self FindMainTabIndex:m_pExpandedTab];
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:0];
        [m_pTableView selectRowAtIndexPath:pPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    }
}
/*============================================================================*/

- (void)viewDidAppear:(BOOL)animated

/*============================================================================*/
{
}


#pragma mark UITableView delegates
/*============================================================================*/

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate

/*============================================================================*/
{
    printf("End Dragging\n");
    if (m_pExpandedTab != NULL)
    {
        int iIndex = [self FindMainTabIndex:m_pExpandedTab];
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:0];
        [m_pTableView selectRowAtIndexPath:pPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    }
}
/*============================================================================*/

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView

/*============================================================================*/
{
    printf("End Decel\n");
    if (m_pExpandedTab != NULL)
    {
        int iIndex = [self FindMainTabIndex:m_pExpandedTab];
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:0];
        [m_pTableView selectRowAtIndexPath:pPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    }
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int iIndex = indexPath.row + 1;
    int iMainTabIndex = 0;
    while (iIndex > 0)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iMainTabIndex];
        [pMainTab LoadConfig:self];
        iIndex--;
        if (iIndex == 0)
        {
            printf("Hit Main Tab\n");

            // This is our main tab
            if ([pMainTab IsExpanded])
            {
                CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:indexPath];
                if (pMainTabCell != NULL)
                    [pMainTabCell SetAccessoryDir:DIR_DOWN];

                m_pExpandedTab = NULL;
                [m_pTableView beginUpdates];
                [self SetExpandedMode:FALSE];
                [self SetExpanded:pMainTab Expanded:FALSE MainTabIndex:indexPath.row];
                [m_pTableView endUpdates];
                [m_pTableView deselectRowAtIndexPath:indexPath animated:NO];
            }
            else
            {
                CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:indexPath];
                if (pMainTabCell != NULL)
                {
                    [pMainTabCell SetAccessoryDir:DIR_UP];
                }

                [m_pTableView beginUpdates];
                CMainTab *pLastMain = m_pExpandedTab;
                m_pExpandedTab = pMainTab;
                if ([pMainTab NSubTabs] <= 1)
                {
                    if (pLastMain != NULL)
                        [self SetExpandedMode:FALSE];
                }

                m_pExpandedTab = pLastMain;

                [m_pTableView deselectRowAtIndexPath:indexPath animated:NO];
                int iThisIndex = indexPath.row;
                if (m_pExpandedTab != NULL)
                {
                    NSIndexPath *pPath = [NSIndexPath indexPathForRow:m_iExpandedTabIndex inSection:0];
                    CMainTabCell *pLastCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:pPath];
                    [pLastCell SetAccessoryDir:DIR_DOWN];
                    
                    [pLastCell setSelected:FALSE];

                    [m_pTableView deselectRowAtIndexPath:pPath animated:YES];
                    [self SetExpanded:m_pExpandedTab Expanded:FALSE MainTabIndex:m_iExpandedTabIndex];
                    iThisIndex = [self FindMainTabIndex:pMainTab];
                }

                if ([pMainTab NSubTabs] <= 1 && [pMainTab TabID] != TAB_HOME)
                {
                    [pMainTabCell SetAccessoryDir:DIR_NONE];
                    [self ShowSystemPage:pMainTab SubTabIndex:0];
                    NSIndexPath *pPath = [NSIndexPath indexPathForRow:iThisIndex inSection:0];
                    [m_pTableView endUpdates];
                    [m_pTableView selectRowAtIndexPath:pPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                    m_pExpandedTab = NULL;
                    return;
                }

                [self SetExpanded:pMainTab Expanded:TRUE MainTabIndex:iThisIndex];
                m_pExpandedTab = pMainTab;
                [self SetExpandedMode:TRUE];
                m_iExpandedTabIndex = iThisIndex;
                NSIndexPath *pPath = [NSIndexPath indexPathForRow:iThisIndex inSection:0];

                [m_pTableView endUpdates];
                [m_pTableView selectRowAtIndexPath:pPath animated:YES scrollPosition:UITableViewScrollPositionNone];

            }

            return;
        }
        else
        {
            if ([pMainTab IsExpanded])
            {
                int iSubTabIndex = 0;
                while (iIndex > 0 && iSubTabIndex < [pMainTab NSubTabs])
                {
                    iIndex--;
                    if (iIndex == 0)
                    {
                        // Hit this Sub Tab
                        printf("Hit Sub Tab\n");
                        [self ShowSystemPage:pMainTab SubTabIndex:iSubTabIndex];
                    }
                    iSubTabIndex++;
                }
            }
        }
        
        iMainTabIndex++;
    }
}


#pragma mark UITableView datasource methods
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

/*============================================================================*/
{
	return 1;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

/*============================================================================*/
{
    int iNSub = 0;
    for (int i = 0; i < [m_pMainTabs count]; i++)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:i];
        if ([pMainTab IsExpanded])
            iNSub += [pMainTab NSubTabs];
    }

	int iNMain  = [m_pMainTabs count];
    return iNMain + iNSub;
}
/*============================================================================*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int iIndex = indexPath.row + 1;
    int iMainTabIndex = 0;
    while (iIndex > 0)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iMainTabIndex];
        [pMainTab LoadConfig:self];
        iIndex--;
        if (iIndex == 0)
        {
            // This is our main tab
            return m_iMainTabHeight;
        }
        else
        {
            if ([pMainTab IsExpanded])
            {
                int iSubTabIndex = 0;
                while (iIndex > 0 && iSubTabIndex < [pMainTab NSubTabs])
                {
                    iIndex--;
                    iSubTabIndex++;
                    if (iIndex == 0)
                    {
                        // Hit this Sub Tab
                        return DY_SUBTABCELL;
                    }
                }
            }
        }
        
        iMainTabIndex++;
    }

    return 32;
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int iIndex = indexPath.row + 1;
    int iMainTabIndex = 0;
    while (iIndex > 0)
    {
        CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:iMainTabIndex];
        [pMainTab LoadConfig:self];
        iIndex--;
        if (iIndex == 0)
        {
            // This is our main tab
            CMainTabCell *cell = (CMainTabCell*)[tableView dequeueReusableCellWithIdentifier:pMainTab.m_pName];
            if (cell == nil)
            {
                cell = [[[CMainTabCell alloc] initWithFrame:CGRectZero reuseIdentifier:pMainTab.m_pName] autorelease];
            }

            // get the view controller's info dictionary based on the indexPath's row
            [cell SetMainTab:[m_pMainTabs objectAtIndex:iMainTabIndex]];
            if ([pMainTab IsExpanded])
            {
                [cell SetAccessoryDir:DIR_UP];
            }
            else
            {
                if ([pMainTab NSubTabs] > 1)
                    [cell SetAccessoryDir:DIR_DOWN];
                else
                    [cell SetAccessoryDir:DIR_RIGHT];
            }
            
            return cell;
        }
        else
        {
            if ([pMainTab IsExpanded])
            {
                int iSubTabIndex = 0;
                while (iIndex > 0 && iSubTabIndex < [pMainTab NSubTabs])
                {
                    iIndex--;
                    if (iIndex == 0)
                    {
                        // Hit this Sub Tab
                        UITableViewCell *pCell = [pMainTab CellForSubTabIndex:tableView Index:iSubTabIndex App:self];
                        [pCell setSelected:FALSE];
                        return pCell;
                    }
                    iSubTabIndex++;
                }
            }
        }
        
        iMainTabIndex++;
    }
    
    return NULL;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        [self HideConnectPage];
        [m_pTitleView setText:@"Main Menu"];

        CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETUSERINFOQ];
        if ([pQ1 AskQuestion])
        {
            [pQ1 GetInt]; // Base Port
            [pQ1 GetInt]; // External Port
            NSString *pSysName = [pQ1 GetString];
    
            // More stuff here we don't care about now
            if ([pSysName isEqual:[m_pInfoText text]])
                return;


            [m_pInfoText setText:pSysName];

            // Remove any pages that may be pushed up 
            [[self navigationController] popToRootViewControllerAnimated:NO];

            // Remove all of our data
            [m_pMainTabs removeAllObjects];
            [m_pTableView reloadData];

            m_pExpandedTab = NULL;
            m_iExpandedTabIndex = 0;
        }

        [m_pModeList RemoveSink:self];
        [m_pModeList release];
        m_pModeList = [[CSystemModeList alloc] init];
        [m_pModeList AddSink:self];
        
        
        [pQ1 release];

        [m_pMainTabs removeAllObjects];

        CNSQuery *pQ2 = [[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETTABCONFIGQ];
        if ([pQ2 AskQuestion])
        {
            int iBGMode = [pQ2 GetInt];    // Enable VTabs
            int iNTabs = [pQ2 GetInt];
    
            [self SetBackgroundImage:iBGMode];

            for (int iTab = 0; iTab < iNTabs; iTab++)
            {
                int iTabType = [pQ2 GetInt];
                NSString *pTabName = [pQ2 GetString];
                BOOL bOK = TRUE;
                switch (iTabType)
                {
                #ifdef PENTAIR
                case TAB_HOME:
                    bOK = FALSE;
                #endif
                case TAB_WEB:
                case TAB_TELEPHONE:
                case TAB_IRRIGATION:
                case 13:    // DVR from 4.0
                    bOK = FALSE;
                    break;
                }

                if (bOK)
                {
                    CMainTab *pTab = [[CMainTab alloc] initWithName:pTabName TabID:iTabType];
                    [m_pMainTabs addObject:pTab];
                    [pTab release];
                }
            }

            int iDY = m_pTableView.bounds.size.height - DY_TOOLBAR - 16;
            m_iMainTabHeight = iDY / MAX(1, [m_pMainTabs count]);
            m_iMainTabHeight = MIN(DY_MAX_MAINTABCELL, m_iMainTabHeight);

            [self SetExpandedMode:FALSE];
        }
        [pQ2 release];
        [m_pTableView reloadData];


        CNSQuery *pQLock = [[[CNSQuery alloc] initWithQ:HLM_TABLETLOCKOUTQ] autorelease];
        [pQLock SendMessageWithMyID:self];

        CNSQuery *pQVersion = [[[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETVERSIONQ] autorelease];
        [pQVersion SendMessageWithMyID:self];

        #ifdef PENTAIR
            [m_pTitleView setFrame:CGRectMake(0, DY_TOOLBAR/4, 160, DY_TOOLBAR/2)];
        #else
            [m_pTitleView setFrame:CGRectMake(4, 0, self.view.frame.size.width-120, DY_TOOLBAR)];
        #endif

        [m_pInfoText setHidden:FALSE];
        [m_pInfoText setAlpha:1.0];

        m_pLogoTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(HideLoginInfo) userInfo:nil repeats:NO] retain];

        #ifdef PENTAIR
            if ([m_pMainTabs count] == 1)
            {
                CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:0];
                [self ShowSystemPage:pMainTab SubTabIndex:0];
            }
        #endif
    }
    else
    {
        [m_pInfoText setHidden:TRUE];
        [m_pTitleView setText:@"Connecting..."];

    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_TABLETLOCKOUTA:
        {
            int iLock = [pMSG GetInt];
            if (iLock)
            {
                if (m_pLockView == NULL)
                {
                        m_pLockView = [[UIAlertView alloc]      initWithTitle:@"HomeLogic" 
                                                                message:@"Connection Not Supported by Lock Configuration" 
                                                                delegate:NULL
                                                                cancelButtonTitle:@"Cancel" otherButtonTitles:NULL];
                }
                [m_pLockView show];

                [CHLComm Disconnect];
            }
            else
            {
                if (m_pLockView != NULL)
                {
                    [m_pLockView dismissWithClickedButtonIndex:0 animated:TRUE];
                    [m_pLockView release];
                    m_pLockView = NULL;
                }
            }
        }
        break;

    case HLM_SYSCONFIG_GETVERSIONA:
        {
            [pMSG GetString];
            [pMSG GetInt];
            int iVer1 = [pMSG GetInt];
            int iVer2 = [pMSG GetInt];

            [CHLComm SetVersion:iVer1 Sub:iVer2];
        }
    }
}
/*============================================================================*/

-(CMainTab*)SelectedMainTab

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    CMainTab *pMainTab = (CMainTab*)[m_pMainTabs objectAtIndex:pPath.row];
    return pMainTab;
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if ([m_pMainTabs count] == 0)
        return;

    CMainTab *pHomeTab = (CMainTab*)[m_pMainTabs objectAtIndex:0];

    for (int i = 0; i < [m_pModeList NModes]; i++)
    {
        CSubTab *pSubTab = [pHomeTab SubTab:i];
        if (pSubTab != NULL)
        {
            CSubTabCell *pSubTabCell = [pSubTab GetSubTabCell];
            if (pSubTabCell != NULL)
            {
                if (i == [m_pModeList ActiveModeIndex])
                    [pSubTabCell SetEmbeddedIcon:@"CHECK.png"];
                else
                    [pSubTabCell SetEmbeddedIcon:NULL];
            }
        }
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (iCommandID != CMD_MODES)
        return;

    NSString *pMode0 = [m_pModeList ModeName:0];
    NSString *pMode1 = [m_pModeList ModeName:1];
    NSString *pMode2 = [m_pModeList ModeName:2];
    NSString *pMode3 = [m_pModeList ModeName:3];
    NSString *pMode4 = [m_pModeList ModeName:4];

	UIActionSheet *pModeSheet = [[UIActionSheet alloc] initWithTitle:@"System Mode:"
								delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
								otherButtonTitles: pMode0, pMode1, pMode2, pMode3, pMode4];
                                
	// use the same style as the nav bar
	pModeSheet.actionSheetStyle = self.navigationController.navigationBar.barStyle;
	
	[pModeSheet showInView:self.view];
	[pModeSheet release];
}
/*============================================================================*/

- (void)actionSheet:(UIActionSheet *)modalView clickedButtonAtIndex:(NSInteger)buttonIndex

/*============================================================================*/
{
    if (buttonIndex < 0 || buttonIndex >= [m_pModeList NModes])
        return;

    [m_pModeList SetSystemMode:buttonIndex];
}
/*============================================================================*/

-(void)CollapseAll

/*============================================================================*/
{
    if (m_pExpandedTab == NULL)
        return;
    
    [self SetExpandedMode:FALSE];
    [self SetExpanded:m_pExpandedTab Expanded:FALSE MainTabIndex:m_iExpandedTabIndex];
    m_pExpandedTab = NULL;
}
/*============================================================================*/

-(void)SetExpandedMode:(BOOL)bNew

/*============================================================================*/
{
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(TableViewResize:finished:context:)];
    [UIView setAnimationDuration:0.5];

    UIEdgeInsets insets = m_pTableView.contentInset;


    if (!bNew)
    {
        int iDY = m_pTableView.bounds.size.height;
        int iDYAll = m_iMainTabHeight * [m_pMainTabs count];
        int iPadTop = (iDY - iDYAll)/2;
        iPadTop = MAX(iPadTop, 8);
        insets.top = iPadTop;
        insets.bottom = 0;
        [m_pTableView setContentInset:insets];
        [m_pTableView setContentOffset:CGPointMake(0, -insets.top) animated:NO];
    }
    else
    {
        int iMainTabIndex = -1;
        for (int i = 0; i < [m_pMainTabs count] && iMainTabIndex == -1; i++)
        {
            if ([m_pMainTabs objectAtIndex:i] == m_pExpandedTab)
                iMainTabIndex = i;
        }

        if (iMainTabIndex < 0)
            return;

        int iThisTabTop = iMainTabIndex * m_iMainTabHeight;
        int iDYContent = [m_pExpandedTab NSubTabs] * DY_SUBTABCELL + m_iMainTabHeight;
        int iDYTable = m_pTableView.frame.size.height;
        int iOffsetMax = iThisTabTop;
        int iOffsetBottomContent = MAX(0, iThisTabTop + iDYContent - iDYTable);
        iOffsetBottomContent = MIN(iOffsetBottomContent, iOffsetMax);

        iOffsetBottomContent -= (8);
        iOffsetMax -= (8);

        CGPoint ptOff = m_pTableView.contentOffset;
        if (ptOff.y < iOffsetBottomContent)
        {
            [m_pTableView setContentOffset:CGPointMake(0, iOffsetBottomContent) animated:YES];
            ptOff.y = iOffsetBottomContent;
        }
        if (ptOff.y > iOffsetMax)
        {
            [m_pTableView setContentOffset:CGPointMake(0, iOffsetMax) animated:YES];
        }
    }


	[UIView commitAnimations];
}
/*============================================================================*/

-(void)SetExpanded:(CMainTab*)pMainTab Expanded:(BOOL)bExpanded MainTabIndex:(int)iMainIndex

/*============================================================================*/
{
    if (bExpanded == [pMainTab IsExpanded])
        return;

    [pMainTab SetExpanded:bExpanded];

    NSMutableArray *pArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < [pMainTab NSubTabs]; i++)
    {
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:i+iMainIndex+1 inSection:0];
        [pArray addObject:pPath];
        [pPath release];
    }
    if (bExpanded)
        [m_pTableView insertRowsAtIndexPaths:pArray withRowAnimation:UITableViewRowAnimationBottom];
    else
        [m_pTableView deleteRowsAtIndexPaths:pArray withRowAnimation:UITableViewRowAnimationBottom];

//    [pArray release];

}
/*============================================================================*/

-(int)FindMainTabIndex:(CMainTab*)pMainTab

/*============================================================================*/
{
    int iIndex = 0;
    for (int i = 0; i < [m_pMainTabs count]; i++)
    {
        CMainTab *pTest = (CMainTab*)[m_pMainTabs objectAtIndex:i];
        if (pTest == pMainTab)
            return iIndex;

        if ([pTest IsExpanded])
            iIndex += (1+[pTest NSubTabs]);
        else
            iIndex++;
    }

    return 0;
}
/*============================================================================*/

-(void)TableViewResize:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (m_pExpandedTab == NULL)
        return;
        
    int iMainTabIndex = -1;
    for (int i = 0; i < [m_pMainTabs count] && iMainTabIndex == -1; i++)
    {
        if ([m_pMainTabs objectAtIndex:i] == m_pExpandedTab)
            iMainTabIndex = i;
    }

    if (iMainTabIndex < 0)
        return;

    int iThisTabTop = iMainTabIndex * m_iMainTabHeight;
    int iDYContent = [m_pExpandedTab NSubTabs] * DY_SUBTABCELL + m_iMainTabHeight;
    int iDYTable = m_pTableView.frame.size.height;
    int iOffsetMax = iThisTabTop;
    int iOffsetBottomContent = MAX(0, iThisTabTop + iDYContent - iDYTable);
    iOffsetBottomContent = MIN(iOffsetBottomContent, iOffsetMax);

    iOffsetBottomContent -= (8);
    iOffsetMax -= (8);

    CGPoint ptOff = m_pTableView.contentOffset;
    if (ptOff.y < iOffsetBottomContent)
    {
        [m_pTableView setContentOffset:CGPointMake(0, iOffsetBottomContent) animated:YES];
        ptOff.y = iOffsetBottomContent;
    }
    if (ptOff.y > iOffsetMax)
    {
        [m_pTableView setContentOffset:CGPointMake(0, iOffsetMax) animated:YES];
    }
    
}
/*============================================================================*/

-(void)SetBackgroundImage:(int)iImage

/*============================================================================*/
{
    g_iBackgroundStyle = iImage;

    if (m_pBackground != NULL)
    {
        [m_pBackground removeFromSuperview];
        [m_pBackground release];
        m_pBackground = NULL;
    }

    UIImage *pImage = NULL;

    switch (iImage)
    {
    case 1000: pImage = [UIImage imageNamed:@"BACKGROUND1.jpg"]; break;
    case 2000: pImage = [UIImage imageNamed:@"BACKGROUND2.png"]; break;
    }

    if (pImage == NULL)
        return;

    UIViewController *pParent = self.parentViewController;
    m_pBackground = [[UIImageView alloc ]initWithImage:pImage];
    [pParent.view addSubview:m_pBackground];
    [pParent.view sendSubviewToBack:m_pBackground];

    CGRect rMain = [UIScreen mainScreen].bounds;

/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    int iDX = rMain.size.width;
    int iDY = rMain.size.height;
    rMain.size.height = iDX;
    rMain.size.width = iDY;
#endif
*/

    [m_pBackground setFrame:rMain];
    [m_pBackground release];
}
/*============================================================================*/

-(void)ShowSystemPage:(CMainTab*)pMainTab SubTabIndex:(int)iSubTabIndex

/*============================================================================*/
{
    // Weird case
    if ([pMainTab TabID] == TAB_HOME)
    {
        [m_pModeList SetSystemMode:iSubTabIndex];
        int iIndex = [self FindMainTabIndex:pMainTab];
        [m_pTableView beginUpdates];
        [self SetExpandedMode:FALSE];
        [self SetExpanded:pMainTab Expanded:FALSE MainTabIndex:iIndex];
        m_pExpandedTab = NULL;
        CMainTabCell *pMainTabCell = (CMainTabCell*)[m_pTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:iIndex inSection:0]];
        if (pMainTabCell != NULL)
            [pMainTabCell SetAccessoryDir:DIR_DOWN];
        [m_pTableView endUpdates];
        return;
    }


    [pMainTab SetDefaultTab:iSubTabIndex];
    UIViewController *pView = NULL;
    switch ([pMainTab TabID])
    {
    case TAB_HVAC:
      	pView = [[CHVACMainTabPage alloc] initWithMainTab:pMainTab Background:m_pBackground];
        break;

    default:
        {
            CMainTabPage *pMainTabPage = [[CMainTabPage alloc] initWithMainTab:pMainTab Background:m_pBackground];
            pView = pMainTabPage;
            #ifdef PENTAIR
                if ([m_pMainTabs count] < 2)
                {
                    [pMainTabPage EnableHomeButton:FALSE];
                }
            #endif
        }
    }

    if (pView == NULL)
        return;

    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
	
	// present the rest of the pages normally
	[[self navigationController] pushViewController:pView animated:YES];
    [pView release];
}
/*============================================================================*/

-(CSystemModeList*)SysModeList

/*============================================================================*/
{
    return m_pModeList;
}
/*============================================================================*/

+(int)BackgroundStyle

/*============================================================================*/
{
    return g_iBackgroundStyle;
}
@end




