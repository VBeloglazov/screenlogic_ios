//
//  LightDimmer.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "LightControl.h"
#import "nsquery.h"
#import "nsmsg.h"
#import "hlcomm.h"
#import "hlbutton.h"
#import "hlslider.h"
#import "LightDimmerControl.h"
#import "CustomPage.h"

#define CMD_SWITCH              1000
#define CMD_UP_START            1001
#define CMD_UP_END              1002
#define CMD_DN_START            1003
#define CMD_DN_END              1004

@implementation CLightControl
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Type:(int)iType Style:(int)iStyle Text:(NSString*)pText ID:(int)iID

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_iID = iID;
        m_iType = iType;
        
        int iDYThis = self.frame.size.height;
        int iDYText = MIN(iDYThis / 2, 16);


        switch (iType)
        {
        case CONTROL_LIGHT_DIMMER:
        case CONTROL_LIGHT_ROCKER1:
        case CONTROL_LIGHT_ROCKER2:
            {
                CGRect rLabel =CGRectMake(0, 0, self.frame.size.width, iDYText);



                switch (iType)
                {
                case CONTROL_LIGHT_ROCKER1:
                case CONTROL_LIGHT_ROCKER2:
                    {
                        int iDXBtn = self.frame.size.width / 2 - 2;
                        CGRect rDn = CGRectMake(0, iDYText, iDXBtn, iDYThis-iDYText);
                        CGRect rUp = CGRectMake(self.frame.size.width-iDXBtn, iDYText, iDXBtn, iDYThis-iDYText);
                        CHLButton *pUp = [[CHLButton alloc] initWithFrame:rUp IconFormat:HL_ICONCENTER Style:AUDIO_ICON_UP Text:@"" TextSize:0 TextColor:NULL Color:NULL Icon:@""];
                        CHLButton *pDn = [[CHLButton alloc] initWithFrame:rDn IconFormat:HL_ICONCENTER Style:AUDIO_ICON_DOWN Text:@"" TextSize:0 TextColor:NULL Color:NULL Icon:@""];
                        [pUp SetCommandID:CMD_UP_START ReleaseID:CMD_UP_END Responder:self];
                        [pDn SetCommandID:CMD_DN_START ReleaseID:CMD_DN_END Responder:self];
                        pUp.autoresizingMask = 0xFFFFFFF;
                        pDn.autoresizingMask = 0xFFFFFFF;
                        [self addSubview:pUp];
                        [self addSubview:pDn];
                        [pUp release];
                        [pDn release];
                    }
                    break;
                    
                case CONTROL_LIGHT_DIMMER:
                    {
                        CGRect rCtrl = self.bounds;
                        rLabel = rCtrl;
                        m_pDimmer = [[CLightDimmerControl alloc] initWithFrame:rCtrl Style:iStyle Control:self];
                        [self setExclusiveTouch:YES];
                        [m_pDimmer setTag:999];
//                        [m_pDimmer addTarget:self action:@selector(UserChange:) forControlEvents:UIControlEventValueChanged];
                        m_pDimmer.autoresizingMask = 0xFFFFFFFF;
                        [self addSubview:m_pDimmer];
                    }
                    break;
                }

                UILabel *pLabel = [[UILabel alloc] initWithFrame:rLabel];
                [CCustomPage InitStandardLabel:pLabel Size:12];
                [pLabel setText:pText];
                pLabel.autoresizingMask = 0xFFFFFFFF;
                [self addSubview:pLabel];

                if (iType == CONTROL_LIGHT_DIMMER)
                {
                    [pLabel setTextAlignment:UITextAlignmentCenter];
                }

                [pLabel release];


            }
            break;
            
        case CONTROL_LIGHT_USERSCENEBUTTON:
        case CONTROL_LIGHT_KEYPADSCENEBUTTON:
        case CONTROL_LIGHT_SWITCH:
        case CONTROL_LIGHT_TOGGLE:
            {
                m_pSwitch = [[CHLButton alloc] initWithFrame:self.bounds Text:pText];
                [m_pSwitch SetCommandID:CMD_SWITCH Responder:self];
                m_pSwitch.autoresizingMask = 0xFFFFFFFF;
                [self addSubview:m_pSwitch];
            }
            break;
        }

        [CHLComm AddSocketSink:self];
        [self InitComm];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    switch (m_iType)
    {
    case CONTROL_LIGHT_TOGGLE:
    case CONTROL_LIGHT_SWITCH:
    case CONTROL_LIGHT_DIMMER:
        {
            CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_REMOVELOADCLIENTQ] autorelease];
            [pQ1 PutInt:[CHLComm SenderID:self]];
            [pQ1 SendMessageWithMyID:self];
        }
        break;

    case CONTROL_LIGHT_KEYPADSCENEBUTTON:
        {
            CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_REMOVECLIENTBYIDQ] autorelease];
            [pQ1 PutInt:m_iKeypadID];
            [pQ1 PutInt:[CHLComm SenderID:self]];
            [pQ1 SendMessageWithMyID:self];
        }
        break;
    }

    [CHLComm RemoveSocketSink:self];

    [m_pDimmer release];
    [m_pSwitch release];
    [super dealloc];
}

/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
        [self InitComm];
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_LIGHTING_LOADSTATUSCHANGE:
    case HLM_LIGHTING_GETLOADSTATUSA:
        {
            [pMSG ResetRead];
            int iDeviceID = [pMSG GetInt];
            int iStatus = [pMSG GetInt];
            if (iDeviceID != m_iID)
                return;
                
            int iPCT = 0;
            if (iStatus == STATE_ON)
                iPCT = 100;
            if (iStatus >= STATE_DIM_START && iStatus <= STATE_DIM_END)
                iPCT = iStatus - STATE_DIM_START;

            if (m_pDimmer != NULL)
            {
                [m_pDimmer NotifyState:iStatus];
            }
            if (m_pSwitch != NULL)
            {
                [m_pSwitch SetChecked:(iStatus != STATE_OFF)];
            }
        }
        break;
        
    case HLM_LIGHTING_KEYPAD_ADDCLIENTBYSCENEIDA:
        {
            m_iKeypadID = [pMSG GetInt];
            m_iSceneIndex = [pMSG GetInt];
            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_GETSTATESBYIDQ] autorelease];
            [pQ PutInt:m_iKeypadID];
            [pQ SendMessageWithMyID:self];
        }
        break;

    case HLM_LIGHTING_KEYPADSCENECHANGE:
    case HLM_LIGHTING_KEYPAD_GETSTATESBYIDA:
        {
            int iN = [pMSG GetInt];
            for (int i = 0; i < iN; i++)
            {
                int iState = [pMSG GetInt];
                if (i == m_iSceneIndex)
                {
                    [m_pSwitch SetChecked:(iState != 0)];
                }
            }
        }
        break;
        
    case HLM_LIGHTING_GETUSERSCENESTATEA:
    case HLM_LIGHTING_USERSCENESTATECHANGED:
        {
            [pMSG ResetRead];
            int iID = [pMSG GetInt];
            int iOn = [pMSG GetInt];
            if (iID != m_iID)
                return;
            [m_pSwitch SetChecked:iOn];
        }
        break;
    }
}
/*============================================================================*/

-(void)UserChange:(id)sender

/*============================================================================*/
{
    if (m_pTimer != NULL)
    {
        [m_pTimer invalidate];
        [m_pTimer release];
    }

    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(UpdateFromSlider) userInfo:nil repeats:NO] retain];
}
/*============================================================================*/

-(void)DeviceOff:(id)sender

/*============================================================================*/
{
    [self DeviceControl:ACTION_DEACTIVATE];
}
/*============================================================================*/

-(void)DeviceOn:(id)sender

/*============================================================================*/
{
    [self DeviceControl:ACTION_ACTIVATE];
}
/*============================================================================*/

-(void)DeviceControl:(int)iCmd

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_EXECUTELOADCOMMANDQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iCmd];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (m_iType)
    {
    case CONTROL_LIGHT_TOGGLE:
    case CONTROL_LIGHT_SWITCH:
        {
            BOOL bOn = ![m_pSwitch IsChecked];
            [m_pSwitch SetChecked:bOn];
            
            int iCommand = ACTION_DEACTIVATE;
            if (bOn)
                iCommand = ACTION_ACTIVATE;

            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_EXECUTELOADCOMMANDQ] autorelease];
            [pQ PutInt:m_iID];
            [pQ PutInt:iCommand];
            [pQ SendMessageWithMyID:self];
        }
        break;

    case CONTROL_LIGHT_USERSCENEBUTTON:
        {
            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_EXECUTEUSERSCENEQ] autorelease];
            [pQ PutInt:m_iID];
            [pQ SendMessageWithMyID:self];
        }
        break;

    case CONTROL_LIGHT_KEYPADSCENEBUTTON:
        {
            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_EXECUTESCENEBYIDQ] autorelease];
            [pQ PutInt:m_iKeypadID];
            [pQ PutInt:m_iSceneIndex];
            [pQ SendMessageWithMyID:self];
        }
        break;
    case CONTROL_LIGHT_ROCKER1:
    case CONTROL_LIGHT_ROCKER2:
        {
            int iAction = ACTION_END_RAISELOWER;

            switch (iCommandID)
            {
            case CMD_UP_START:  iAction = ACTION_BEGIN_RAISE;   break;
            case CMD_DN_START:  iAction = ACTION_BEGIN_LOWER;   break;
            }
            
            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_EXECUTELOADCOMMANDQ] autorelease];
            [pQ PutInt:m_iID];
            [pQ PutInt:iAction];
            [pQ SendMessageWithMyID:self];
        }
        break;
    
    }

}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    switch (m_iType)
    {
    case CONTROL_LIGHT_TOGGLE:
    case CONTROL_LIGHT_SWITCH:
    case CONTROL_LIGHT_DIMMER:
        {
            CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_ADDLOADCLIENTQ] autorelease];
            [pQ1 PutInt:m_iID];
            [pQ1 PutInt:[CHLComm SenderID:self]];
            [pQ1 SendMessageWithMyID:self];

            CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_GETLOADSTATUSQ] autorelease];
            [pQ2 PutInt:m_iID];
            [pQ2 SendMessageWithMyID:self];
        }
        break;

    case CONTROL_LIGHT_USERSCENEBUTTON:
        {
            CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_GETUSERSCENESTATEQ] autorelease];
            [pQ1 PutInt:m_iID];
            [pQ1 SendMessageWithMyID:self];
        }
        break;
        
    case CONTROL_LIGHT_KEYPADSCENEBUTTON:
        {
            CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_LIGHTING_KEYPAD_ADDCLIENTBYSCENEIDQ] autorelease];
            [pQ1 PutInt:m_iID];
            [pQ1 PutInt:[CHLComm SenderID:self]];
            [pQ1 SendMessageWithMyID:self];
        }
        break;
    }
}
@end
