//
//  Sink.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

/*============================================================================*/

@protocol CSink

/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData;

@end
