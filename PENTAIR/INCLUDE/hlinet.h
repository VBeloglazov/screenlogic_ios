/*
--------------------------------------------------------------------------------

    HLINET.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLINET_H
#define HLINET_H


    SOCKET                      ConnectHomeLogic(UINT nPort = 80);

    BOOL                        SendString(
    SOCKET                      hSocket,
    CHLString                   sOut );


#endif

