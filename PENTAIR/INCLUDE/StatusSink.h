/*
--------------------------------------------------------------------------------

    StatusSink.h

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once

//
// Little helper class to give feedback during the lookup/login process
//
/*============================================================================*/

    class CStatusSink

/*============================================================================*/
{
public:
    virtual void                SetStatus(
    CHLString                   sStatus )
    {
    }

    virtual BOOL                DoWaitProc()
    {
        return TRUE;
    }
};
