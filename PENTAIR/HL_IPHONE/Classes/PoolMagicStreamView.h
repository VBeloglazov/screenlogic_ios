//
//  PoolMagicStreamView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/9/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLView.h"

@class CHLButton;

/*============================================================================*/

@interface CPoolMagicStreamView : UIView  < CHLCommandResponder >

/*============================================================================*/
{
    CHLButton *m_pButtons[4];
}

- (id)initWithFrame:(CGRect)frame;

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID;


@end
