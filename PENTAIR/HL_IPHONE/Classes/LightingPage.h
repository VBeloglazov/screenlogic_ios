//
//  LightingPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SubSystemPage.h"

@class CCustomPage;

/*============================================================================*/

@interface CLightingPage : CSubSystemPage

/*============================================================================*/
{
    CCustomPage                     *m_pUserPage;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)Init:(CSubTab*)pSubTab;

@end
