//
//  PoolColorLightsView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolColorLightsView.h"
#import "NSPoolConfig.h"
#import "hlbutton.h"
#import "crect.h"
#import "MainView.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "NSMSG.h"
#import "CustomPage.h"

#define CMD_ALLOFF              1000
#define CMD_ALLON               1001
#define CMD_SET                 1002
#define CMD_SYNC                1003
#define CMD_SWIM                1004

@implementation CPoolColorLightsView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pConfig = pConfig;
        [m_pConfig AddSink:self];

        m_pProgress = [[UIProgressView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pProgress];
        [m_pProgress release];

        m_pLabel    = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
        m_pButtons[0] = [self AddButton:@"All Off"      CommandID:CMD_ALLOFF];
        m_pButtons[1] = [self AddButton:@"All On"       CommandID:CMD_ALLON];
        m_pButtons[2] = [self AddButton:@"Color Set"    CommandID:CMD_SET];
        m_pButtons[3] = [self AddButton:@"Color Sync"   CommandID:CMD_SYNC];
        m_pButtons[4] = [self AddButton:@"Color Swim"   CommandID:CMD_SWIM];

        [CHLComm AddSocketSink:self];
        
         CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_ADDCLIENTQ];
         [p1 PutInt:0];
         [p1 PutInt:[CHLComm SenderID:self]];
         [p1 SendMessage];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
     CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_REMOVECLIENTQ];
     [p1 PutInt:0];
     [p1 PutInt:[CHLComm SenderID:self]];
     [p1 SendMessage];
     [p1 release];

    [CHLComm RemoveSocketSink:self];
    [m_pConfig RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID

/*============================================================================*/
{
    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:pText];
    [self addSubview:pBtn];
    [pBtn release];
    [pBtn SetCommandID:iID Responder:self];
    return pBtn;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    BOOL bHz = rBounds.size.width > rBounds.size.height;
    CGRect rLabel = [CRect BreakOffBottom:&rBounds DY:32];
    CGRect rProgress = [CRect BreakOffBottom:&rBounds DY:20];
    [CRect CenterDownToX:&rProgress DX:200];

    if (!bHz)
    {
        [CRect CenterDownToX:&rBounds DX:175];
        for (int i = 0; i < 5; i++)
        {
            CGRect rBtn = [CRect CreateYSection:rBounds Index:i Of:5];
            CHLButton *pBtn = m_pButtons[i];
            [CRect Inset:&rBtn DX:0 DY:8];
            [pBtn setFrame:rBtn];
        }
    }
    else
    {
        for (int i = 0; i < 5; i++)
        {
            CGRect rBtn = [CRect CreateXSection:rBounds Index:i Of:5];
            CHLButton *pBtn = m_pButtons[i];
            [CRect CenterDownToY:&rBtn DY:80];
            [CRect Inset:&rBtn DX:2 DY:0];
            [pBtn setFrame:rBtn];
        }
    }

    [m_pProgress setFrame:rProgress];
    [m_pLabel setFrame:rLabel];

}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_ALLOFF:
    case CMD_ALLON:
    case CMD_SET:
    case CMD_SYNC:
    case CMD_SWIM:
        {
            int iPentairCmd = POOLCMD_ALLOFF + (iCommandID-CMD_ALLOFF);
            CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_COLORLIGHTSCMDQ];
            [p1 PutInt:0];
            [p1 PutInt:iPentairCmd];
            [p1 SendMessage];
            [p1 release];
        }
        break;
    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_ADDCLIENTQ];
        [p1 PutInt:0];
        [p1 PutInt:[CHLComm SenderID:self]];
        [p1 SendMessage];
        [p1 release];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_POOL_COLORUPDATE)
        return;
    /*int iMode = */[pMSG GetInt];
    int iNow  = [pMSG GetInt];
    int iTotal = [pMSG GetInt];
    NSString *pText = [pMSG GetString];
    
    [m_pProgress setProgress:(float)iNow / (float)iTotal];

    [m_pLabel setText:pText];
}
@end
