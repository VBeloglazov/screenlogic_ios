//
//  AudioZone.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubTab.h"
#import "NSSocketSink.h"
#import "SinkServer.h"

@class CNSQuery;
@class CAudioSource;
@class CAudioService;
@class CPlaylistItem;

#define SINK_SOURCECHANGED              0x00000001
#define SINK_VOLCHANGED                 0x00000002
#define SINK_ITEMSCHANGED               0x00000004
#define SINK_STATECHANGED               0x00000008
#define SINK_ARTCHANGED                 0x00000010
#define SINK_UIVOLUMEEND                0x00000020
#define SINK_TUNERSTATE                 0x00000040

/*============================================================================*/

@interface CAudioZone : CSubTab < CNSSocketSink >

/*============================================================================*/
{
    // Basic Props
    CSinkServer                         *m_pSinkServer;

    int                                 m_iVolumeType;
    int                                 m_iConfigID;
    BOOL                                m_bHasMute;
    BOOL                                m_bMute;
    NSMutableArray                      *m_pSources;

    //
    // Dynamic info About this zone
    //
    int                                 m_iActiveSourceID;
    int                                 m_iActiveSubSource;
    int                                 m_iActiveUserData;
    int                                 m_iVolume;

    NSTimer                             *m_pVolTimer;
    NSTimer                             *m_pRampTimer;
    int                                 m_iTimerInit;
    
    int                                 m_iVolDir;
    int                                 m_iLocalVol;

    //
    // MP3 Library Info
    //
    NSMutableArray                      *m_pServices;

    BOOL                                m_bStateOK;
    BOOL                                m_bDataInit;

    NSMutableArray                      *m_pPlaylistItems;

    int                                 m_wPlaylistVersion;
    int                                 m_wPlaystateVersion;
    int                                 m_wArtworkVersion;

    void                                *m_pJPEG;
    int                                 m_iNJPEG;

    NSString                            *m_sArtPath;
    BOOL                                m_bRepeat;
    BOOL                                m_bRandom;
    int                                 m_iPlayState;
    int                                 m_iActiveItemIndex;
    int                                 m_iProgressPct;
    
    //
    // Tuner Tuner Info
    // 
    int                                 m_iTunerState;
    int                                 m_iTunerStation;
    int                                 m_iTunerFavoriteID;
    NSString                            *m_psTunerArtist;
    NSString                            *m_psTunerTrack;
    NSString                            *m_psTunerStation;
}

@property (nonatomic, retain) NSString* m_sArtPath;
@property (nonatomic) int m_iVolumeType;
@property (nonatomic) int m_iVolume;


-(id)initFromQ:(CNSQuery*)pQ;

+(int)GlobalSortOptions;
+(void)GlobalSortOptions:(int)iNew;

-(int)NSources;
-(CAudioSource*)Source:(int)iIndex;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;
-(int)ProcessStateMSG:(CNSMSG*)pMSG;

-(BOOL)HasSettings;
-(int)SettingsID;
-(BOOL)HasMute;
-(void)SetMute:(BOOL)bMute;
-(BOOL)GetMute;

-(void)InitComm;
-(void)InitData;
-(BOOL)UpdatePlaylist:(NSMutableArray*)pList;
-(BOOL)IsSamePlaylist:(NSMutableArray*)pList;
-(void)CheckZoneArt;
-(BOOL)ActiveMP3;
-(BOOL)ActiveTunerAMFM;
-(BOOL)ActiveTunerXM;
-(int)MP3Flags;
-(int)ActiveSubSource;
-(int)ActiveUserData;
-(int)NServices;
-(int)Volume;
-(int)ProgressPercent;
-(int)PlayState;
-(BOOL)HasSort:(int)iSortType;
-(BOOL)Repeat;
-(BOOL)Random;

-(CAudioService*)Service:(int)iIndex;


-(int)ActiveSourceID;
-(int)ActiveSourceIndex;
-(void)SetActiveSourceIndex:(int)iIndex;

-(IBAction)VolDownEvent:(id)sender;
-(IBAction)VolUpEvent:(id)sender;
-(void)VolReleaseEvent;

-(void)StopVolume;
-(void)SetVolume:(int)iNew;
-(BOOL)OnVolumeDownStart;
-(BOOL)OnVolumeUpStart;
-(BOOL)OnVolumeEnd;
-(void)StartTimer:(int)iDir;
-(void)StopTimer;
-(void)VolTimerEvent;
-(void)VolRampTimerEvent;

-(void)ZoneCommand:(int)iCommand Data1:(int)iData1 Data2:(int)iData2;
-(void)PlayTrackIndex:(int)iIndex;
-(void)AddSortedItem:(int)iSortType Sub1:(int)iSub1 Sub2:(int)iSub2 Index:(int)iIndex Play:(BOOL)bPlay;

-(UIImage*)GetImage;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;

-(int)ActivePlaylistItem;
-(int)NPlaylistItems;
-(CPlaylistItem*)PlaylistItem:(int)iIndex;

-(int)TunerState;
-(int)TunerStation;
-(int)TunerFavoriteID;
-(NSString*)TunerArtist;
-(NSString*)TunerTrack;
-(NSString*)TunerStationText;
-(void)TunerTune:(int)iDir;
-(void)HDTunerTune:(int)iDir;

+(void)AddVolumeButtons:(NSMutableArray*)pArray Zone:(CAudioZone*)pZone;

-(void)PopVolumeUI:(int)iDir;

-(void)UpdateSubTabCell:(CSubTabCell*)pSubTabCell;

@end
