/*
--------------------------------------------------------------------------------

    NSQUERY.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifdef __cplusplus
    class CHLQuery;
    class CHLMSG;
#else
    @class CHLQuery;
    @class CHLMSG;
#endif

@class CNSMSG;

#include "hlm.h"


/*============================================================================*/

    @interface CNSQuery : NSObject

/*============================================================================*/
{
    CHLQuery                    *m_pQ;
    CHLMSG                      *m_pMSGQ;
    CHLMSG                      *m_pMSGA;
}

// Question Connstruction
-(id)initWithQ:(int)shQ;
-(void)PutInt:(int)iVal;
-(void)PutString:(NSString*)sVal;
-(void)PutTime:(NSDate*)pDate;

// QA Session
-(int)AskQuestion;
-(void)SendMessage;
-(void)SendMessageWithID:(int)iID;
-(void)SendMessageWithMyID:(id)pSender;

// Response Examination
-(int)GetInt;
-(int)GetBYTE;
-(NSString*)GetString;
-(NSString*)GetStringNoFilter;

-(CNSMSG*)CreateNSMSG;

@end
