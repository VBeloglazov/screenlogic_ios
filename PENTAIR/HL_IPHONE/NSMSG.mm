/*
--------------------------------------------------------------------------------

    NSMSG.MM

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"
#include "NSMSG.h"

#import "hlcomm.h"

#include "hlmsg.h"

@implementation CNSMSG

/*============================================================================*/

- (id) initWithMSG:(CHLMSG*)pMSG

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pMSG = pMSG;
    }
    return self;
}
/*============================================================================*/

-(CHLMSG*)Message

/*============================================================================*/
{
    return m_pMSG;
}
/*============================================================================*/

-(void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)ResetRead

/*============================================================================*/
{
    m_pMSG->ResetRead();
}
/*============================================================================*/

-(int)MessageID

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    return m_pMSG->MessageID();
}
/*============================================================================*/

-(int)SenderID

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    return m_pMSG->SenderID();
}
/*============================================================================*/

-(int)MessageSize

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    return m_pMSG->MSGSize();
}
/*============================================================================*/

-(int)DataSize

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    return m_pMSG->DataSize();
}
/*============================================================================*/
    
-(int)GetInt

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    int iVal = 0;
    m_pMSG->GetInt(&iVal);
    return iVal;
}
/*============================================================================*/
    
-(int)GetShort

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    short shVal = 0;
    m_pMSG->GetShort(&shVal);
    return shVal;
}
/*============================================================================*/
    
-(int)GetBYTE

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return 0;
    BYTE byVal = 0;
    m_pMSG->GetBYTE(&byVal);
    return byVal;
}
/*============================================================================*/

-(UIColor*)GetColor

/*============================================================================*/
{
    int byR = [self GetInt];
    int byG = [self GetInt];
    int byB = [self GetInt];
    float fR = (float)byR / 255.0f;
    float fG = (float)byG / 255.0f;
    float fB = (float)byB / 255.0f;

    UIColor *pColor = [UIColor colorWithRed:fR green:fG blue:fB alpha:1.0];
    return pColor;
}
/*============================================================================*/

-(NSString*)GetString

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return @"";
 
    CHLString sRet;
    m_pMSG->GetString(&sRet);

    int iLen = sRet.GetLength();

    // wchar_t is 32 bits on the mac, this shouldn't lose anything
    wchar_t *pWCHARBuf = sRet.GetBufferWCHAR(iLen);
    unichar *pUniBuf = new unichar[iLen];
    for (int i = 0; i < iLen; i++)
    {
        pUniBuf[i] = (unichar)pWCHARBuf[i];
    }
//        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.9]];


    unichar *pBuf = pUniBuf;
    for (int i = 0; i < (iLen-1); i++)
    {
        if (pBuf[i] == '\\' && pBuf[i+1] == 'n')
        {
            pBuf[i] = ' ';
            int iLenRem = iLen - i - 2;
            memmove(pBuf+i+1, pBuf+i+2, iLenRem * sizeof(unichar));
            iLen--;
            pBuf[iLen] = 0;
        }
    }

    NSString *pRet = [NSString stringWithCharacters:pUniBuf length:iLen];
    delete [] pUniBuf;
    return pRet;
}
/*============================================================================*/

-(NSString*)GetStringNoFilter

/*============================================================================*/
{
    if (m_pMSG == NULL)
        return @"";
 
    CHLString sRet;
    m_pMSG->GetString(&sRet);

    int iLen = sRet.GetLength();
    char *pBuf = sRet.GetBufferCHAR(iLen);

    NSString *pRet = [NSString stringWithCString:pBuf encoding:NSASCIIStringEncoding];
    return pRet;
}
/*============================================================================*/

-(NSDate*)GetTimeWith:(NSDateComponents*)pComponents Calendar:(NSCalendar*)pCalendar DateHelper:(CDateHelper*)pHelper

/*============================================================================*/
{
    short iYear = 0;
    short iMon = 0;
    short iDayOfWeek = 0;
    short iDay = 0;
    short iHour = 0;
    short iMin = 0;
    short iSec = 0;
    short iMSEC = 0;

    m_pMSG->GetShort(&iYear);
    m_pMSG->GetShort(&iMon);
    m_pMSG->GetShort(&iDayOfWeek);
    m_pMSG->GetShort(&iDay);
    m_pMSG->GetShort(&iHour);
    m_pMSG->GetShort(&iMin);
    m_pMSG->GetShort(&iSec);
    m_pMSG->GetShort(&iMSEC);
    [pComponents setYear:iYear];
    [pComponents setMonth:iMon];
    [pComponents setDay:iDay];
    [pComponents setHour:iHour];
    [pComponents setMinute:iMin];
    [pComponents setSecond:iSec];

    switch (iDayOfWeek)
    {
    case HL_MONDAY:     iDayOfWeek = 2; break;
    case HL_TUESDAY:    iDayOfWeek = 3; break;
    case HL_WEDNESDAY:  iDayOfWeek = 4; break;
    case HL_THURSDAY:   iDayOfWeek = 5; break;
    case HL_FRIDAY:     iDayOfWeek = 6; break;
    case HL_SATURDAY:   iDayOfWeek = 7; break;
    case HL_SUNDAY:     iDayOfWeek = 1; break;
    }

    [pComponents setWeekday:iDayOfWeek];
    NSDate *pDate = [pHelper GetDateFromComponents:pComponents Calendar:pCalendar];
    return pDate;
}
/*============================================================================*/

-(NSDate*)GetTimeCompressedWith:(NSDateComponents*)pComponents Calendar:(NSCalendar*)pCalendar DateHelper:(CDateHelper*)pHelper

/*============================================================================*/
{
    char byYear = 0;
    char byMonth = 0;
    char byDay = 0;
    char byHour = 0;
    char byMin = 0;
    char bySec = 0;

    m_pMSG->GetChar(&byYear);
    m_pMSG->GetChar(&byMonth);
    m_pMSG->GetChar(&byDay);
    m_pMSG->GetChar(&byHour);
    m_pMSG->GetChar(&byMin);
    m_pMSG->GetChar(&bySec);

    byMonth = byMonth & 0x0F;

    [pComponents setYear:byYear+2000];
    [pComponents setMonth:byMonth];
    [pComponents setDay:byDay];
    [pComponents setHour:byHour];
    [pComponents setMinute:byMin];
    [pComponents setSecond:bySec];
    NSDate *pDate = [pHelper GetDateFromComponents:pComponents Calendar:pCalendar];
    return pDate;
}
/*============================================================================*/

-(unsigned char*)GetDataAtReadIndex

/*============================================================================*/
{
    return m_pMSG->DataAtRead();
}
/*============================================================================*/

+(void)MakeCHLString:(CHLString*)pOut From:(NSString*)pIn

/*============================================================================*/
{
    if (pIn == NULL)
        return;
    NSData *pData = [pIn dataUsingEncoding:NSUnicodeStringEncoding allowLossyConversion:YES];
    int iLen = [pData length];
    int iNChars = iLen/2-1;
    wchar_t *pTgt = pOut->GetBufferWCHAR(iNChars);
    unichar *pSrc = (unichar*)[pData bytes];
    pSrc++;

    for (int i = 0; i < iNChars; i++)
    {
        *pTgt = (unichar)(*pSrc);
        pTgt++;
        pSrc++;
    }

    pOut->ReleaseBuffer(iNChars);
}
@end


@implementation CDateHelper

/*============================================================================*/

-(id)init

/*============================================================================*/
{
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pLastDate release];
    [m_pLastDateRet release];
    [super dealloc];
}
/*============================================================================*/

-(NSDate*)GetDateFromComponents:(NSDateComponents*)pComponents Calendar:(NSCalendar*)pCalendar

/*============================================================================*/
{
    BOOL bSameDay = TRUE;
    if ([pComponents year] != m_iYear)
        bSameDay = FALSE;
    if ([pComponents month] != m_iMonth)
        bSameDay = FALSE;
    if ([pComponents day] != m_iDay)
        bSameDay = FALSE;

    if (!bSameDay)
    {
        [m_pLastDate release];
        m_iYear     = [pComponents year];
        m_iMonth    = [pComponents month];
        m_iDay      = [pComponents day];
        NSDate *pDate = [pCalendar dateFromComponents:pComponents];

        [pComponents setHour:0];
        [pComponents setMinute:0];
        [pComponents setSecond:0];
        m_pLastDate = [[pCalendar dateFromComponents:pComponents] retain];
        return pDate;
    }

    int iSeconds = [pComponents hour] * 3600 + [pComponents minute] * 60 + [pComponents second];
    [m_pLastDateRet release];
    m_pLastDateRet = [[NSDate alloc] initWithTimeInterval:iSeconds sinceDate:m_pLastDate];
    return m_pLastDateRet;
}
@end

