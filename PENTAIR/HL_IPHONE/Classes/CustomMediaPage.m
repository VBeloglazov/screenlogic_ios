//
//  CustomMediaPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "CustomPage.h"
#import "CustomMediaPage.h"
#import "MediaZonePage.h"
#import "MainView.h"
#import "MainTabVC.h"
#import "crect.h"
#import "hlsegment.h"
#import "RoundRegion.h"

@class CHLView;

@implementation CCustomMediaPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame ID:(int)iID ZoneID:(int)iZoneID ZonePage:(CMediaZonePage*)pZonePage

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        m_iID = iID;
        m_iZoneID = iZoneID;
        m_pZonePage = pZonePage;
        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETINTERFACELAYOUTBYIDQ];
        [pQ autorelease];
        [pQ PutInt:iID];


        int iResInt = HL_240x320;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            iResInt = HL_800x600;
        }

        [pQ PutInt:iResInt];
        [pQ SendMessageWithMyID:self];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_AUDIO_GETINTERFACELAYOUTBYIDA)
        return;
        
    int iID = [pMSG GetInt];
    if (iID != m_iID)
        return;
    int iNPages = [pMSG GetInt];
    CGRect rSubPages = self.bounds;
    NSMutableArray *pPages = [[NSMutableArray alloc] init];

    for (int i = 0; i < iNPages; i++)
    {
        NSString *pName = [pMSG GetString];
        CCustomPage *pPage = [[CCustomPage alloc] initWithFrame:rSubPages MSG:pMSG ZoneID:m_iZoneID];
        [pPage SetName:pName];
        [pPages addObject:pPage];
        [pPage release];
    }

    if ([pPages count] > 1)
    {
        m_pTabPage = [[CCustomMediaTabPage alloc] initWithFrame:rSubPages CustomPages:pPages];
        [self addSubview:m_pTabPage];
        [m_pTabPage release];

    }
    else if ([pPages count] == 1)
    {
        CCustomPage *pPage = (CCustomPage*)[pPages objectAtIndex:0];
        [self addSubview:pPage];
        m_pActivePage = pPage;
    }
    
    if (m_pZonePage != NULL)
    {
        if ([m_pZonePage IsVisible])
            [m_pZonePage SetVisible:TRUE];
    }

    [pPages release];
        
    [CHLComm RemoveSocketSink:self];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bNew

/*============================================================================*/
{
    m_bVisible = bNew;
    [m_pActivePage SetVisible:bNew];
    [m_pTabPage SetVisible:bNew];
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = [self bounds];
    [m_pActivePage setFrame:rBounds];
    [m_pTabPage setFrame:rBounds];
}

@end


@implementation CCustomMediaTabPage
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame CustomPages:(NSMutableArray*)pPages

/*============================================================================*/
{
    int iStyle = SRREGION_MAIN_WINDOW;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iStyle = SRREGION_SMALL_HEADER;

    self = [super initWithFrame:rFrame Style:SRREGION_MAIN_WINDOW];
    if (self)
    {
        m_pPages = pPages;
        [m_pPages retain];
        NSMutableArray *pStrings = [[[NSMutableArray alloc] init]autorelease];
        for (int i = 0; i < [pPages count]; i++)
        {
            CCustomPage *pPage = (CCustomPage*)[pPages objectAtIndex:i];
            [pStrings addObject:[pPage GetName]];
        }

        [self InitPageWithStrings:pStrings];
        [self SetFlags:RREGION_FLAG_BASELEVEL];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pPages release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bNew

/*============================================================================*/
{
    m_bVisible = bNew;
    CCustomPage *pCurrentView = (CCustomPage*)[m_pRgn2 ClientView];

    for (int i = 0; i < [m_pPages count]; i++)
    {
        CCustomPage *pPage = (CCustomPage*)[m_pPages objectAtIndex:i];
        if (pPage == pCurrentView)
            [pPage SetVisible:bNew];
        else
            [pPage SetVisible:FALSE];

    }
}
/*============================================================================*/

-(UIView*)CreatePageForIndex:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pPages count])
        return NULL;
    CCustomPage *pPage = (CCustomPage*)[m_pPages objectAtIndex:iIndex];

    for (int i = 0; i < [m_pPages count]; i++)
    {
        CCustomPage *pTest = (CCustomPage*)[m_pPages objectAtIndex:i];
        if (pTest == pPage)
            [pTest SetVisible:m_bVisible];
        else
            [pTest SetVisible:FALSE];

    }

    return pPage;
}
@end