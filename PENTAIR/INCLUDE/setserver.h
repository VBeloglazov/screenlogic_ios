/*
--------------------------------------------------------------------------------

    SETSERVER.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

This contains data and functions to control the global settings of windows and
controls, and their properties.
--------------------------------------------------------------------------------
*/
#ifndef SETSERVER_H
#define SETSERVER_H

#include "socksink.h"
#include "hloblist.h"

#ifndef IPHONE
#include "hldc.h"
#include "dibdc.h"
#endif

#ifdef HLCONFIG
    #include "sysdef.h"
    class CHLFile;
#endif

    class CHLWAV;

#ifndef IPHONE
    class CHLWindow;
    class CHLDibDC;
    class CHLDC;
    class CEdgeList;
#endif

#ifdef HLOSD
    class CTexture;
#endif

    //
    // For RGB values, if a color is not specified, it will be set to
    // RGB_NONE: though this is a real color, we shouldn't run into it
    // as a control color.
    //

    #define N_WND_TYPES             (WND_END - WND_START + 1)
    #define FLAG_DIB_DISCARDABLE    0x00000001
    #define FLAG_DIB_NOXBLIT        0x00000002
    #define FLAG_DIB_NOSETX         0x00000004
    #define FLAG_DIB_RAISEDLEVEL    0x00000008

    #define ADDITIVE_BLEND      2

    #define                     SETSERVER_LOADED    0x00000001

/*============================================================================*/

    class CSetServer :          public CSocketSink

/*============================================================================*/
{
    friend class CHLWindow;
protected:
    // This class
#ifdef IPHONE
    CHLWAV                      *m_pButtonWAV;
#else
    CHLWAV                      *m_pConnectWAV;
    CHLWAV                      *m_pDisconnectWAV;
    CHLObList                   m_WAVList;
#endif

#ifdef HLOSD
    CTexture                    *m_pTexture;
#endif

    // Settings
    int                         m_iGlobalOpts;
    int                         m_iHostType;
    int                         m_iFlags;

    int                         m_pInts[N_INT_TYPES];
    COLORREF                    m_pRGB[N_RGB_TYPES];

    CHLString                   m_sFontFile;
    CHLString                   m_sFontFam;

    CHLString                   m_sTexture;

    CHLString                   m_sPressWAV;
    CHLString                   m_sReleaseWAV;
    CHLString                   m_sConnectWAV;
    CHLString                   m_sDisconnectWAV;

public:
    CSetServer();

    ~CSetServer();

    inline int                  HostType()                      { return m_iHostType;                       }
    inline void                 HostType(int iNew)              { m_iHostType = iNew;                       }

    BOOL                        FullGateway();
    BOOL                        OEMGateway();

    inline int                  GlobalOpts()                    { return m_iGlobalOpts;                     }
    void                        GlobalOpts(int iNew);

    // Settings specific to window types
    int                         GetInt(int iIntType);
    void                        SetInt(int iIntType, int iNew);
    
    COLORREF                    GetRGB(int iRGBType);
    void                        SetRGB(int iRGBType, COLORREF rgbNew);

    HL_PIXEL                    GetRGB16(int iRGBType);

    inline CHLString            FontFile()                      { return m_sFontFile;       }
    inline CHLString            FontFam()                       { return m_sFontFam;        }

    inline CHLString            Texture()                       { return m_sTexture;        }

#ifdef HLOSD
    CGLDIB                      *GetTexture();
#endif

    void                        SetTexture(
    CHLString                   sNew );

    void                        SetFontData(
    CHLString                   sFontFile,
    CHLString                   sFontFam );

    inline CHLString            PressWAV()                      { return m_sPressWAV;       }
    inline void                 PressWAV(CHLString sNew)        { m_sPressWAV = sNew;       }
    inline CHLString            ReleaseWAV()                    { return m_sReleaseWAV;     }
    inline void                 ReleaseWAV(CHLString sNew)      { m_sReleaseWAV = sNew;     }
    inline CHLString            ConnectWAV()                    { return m_sConnectWAV;     }
    inline void                 ConnectWAV(CHLString sNew)      { m_sConnectWAV = sNew;     }
    inline CHLString            DisconnectWAV()                 { return m_sDisconnectWAV;  }
    inline void                 DisconnectWAV(CHLString sNew)   { m_sDisconnectWAV = sNew;  }


    int                         ROutByType(int iWndType);
    int                         ShadeInByType(int iWndType);
    int                         ShadeOutByType(int iWndType);
    int                         TextHeightByType(int iWndType);
    COLORREF                    ColorByType(int iWndType);
    COLORREF                    TextColorByType(int iWndType);

    // CSocketSink overrides
    virtual void                SinkMessage(
    CHLMSG                      *pMSG );

    virtual void                ConnectionChanged(
    BOOL                        bNowConnected );

    void                        LoadSettings();

    #ifdef HLCONFIG
    BOOL                        LoadSettings(
    int                         iResMode,
    int                         iID );

    BOOL                        SaveSettings(
    int                         iResMode,
    int                         iID );
    #endif

    BOOL                        LoadSettings(
    CHLMSG                      *pMSG );

#ifndef IPHONE
    CHLWAV                      *RegisterWAVFile(
    CHLString                   sWAVFile );
#endif

#ifdef IPHONE
    CHLWAV                      *ButtonWAV()    { return m_pButtonWAV;  }
#endif

    #ifdef HLCONFIG
        BOOL                    Read(CHLFile *pFile, int iSchema);
        BOOL                    Write(CHLFile *pFile, int iSchema);
        void                    AddSounds(CHLObList *pList);
        void                    AddTextures(CHLObList *pList);
    #endif


protected:
#ifndef IPHONE
    CHLWAV                      *WAVByFileName(
    CHLString                   sName );
#endif

    void                        LoadDefaults();
};
#ifdef HLOSD
/*============================================================================*/

    class                       CTexture : public CSocketSink

/*============================================================================*/
{
protected:
    CHLString                   m_sName;

#ifdef HLOSD
    CGLDIB                      *m_pDIB;
#else
    CHLDibDC                    *m_pDIB;
#endif

    BOOL                        m_bWaitMSG;    

public:
    CTexture(
    CHLString                   sName );

    virtual ~CTexture();

#ifdef HLOSD
    inline CGLDIB               *DIB()      { return m_pDIB;    }
#else
    inline CHLDibDC             *DIB()      { return m_pDIB;    }
#endif

    inline CHLString            Name()      { return m_sName;   }

    virtual void                SinkMessage(CHLMSG *pMSG);
    virtual void                ConnectionChanged(BOOL bNowConnected);

    BOOL                        WaitLoad();
};
#endif

#endif

