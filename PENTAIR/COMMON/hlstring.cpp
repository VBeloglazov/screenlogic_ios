/*
--------------------------------------------------------------------------------

    HLSTRING.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "stdio.h"

#include "helpers.h"

#ifdef STRINGLIB
    #include "stringlib.h"
#endif

#ifndef UNDER_CE
    #include "hlstring.h"
#endif

#ifdef MAC_OS
    #include "stdlib.h"
    #include "stdarg.h"
    #define _vsnprintf vsnprintf
#endif
    //
    // Statics.
    //
    char                        g_NULL                  = '\0';
    wchar_t                     g_WNULL                 = L'\0';
    char                        *CHLString::m_pNULL     = &g_NULL;
    char                        *CHLString::m_pWNULL    = (char*) &g_WNULL;



/*============================================================================*/

    CHLString::CHLString()

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif

    #ifdef UNICODE
        #ifdef GATEWAY
            InitNULL(HLSTRING_CHAR);
        #else
            InitNULL(HLSTRING_UNICODE);
        #endif
    #else
        InitNULL(HLSTRING_CHAR);
    #endif
}
/*============================================================================*/

    CHLString::CHLString(
    CHLSTRING_TYPE              iType )

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif
    InitNULL(iType);
}
/*============================================================================*/

    CHLString::CHLString(
    const CHLString             &rhs )

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif
    InitNULL(rhs.m_iType);
    operator=(rhs);
}
/*============================================================================*/

    CHLString::CHLString(
    const char                  *prhs )

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif
    InitNULL(HLSTRING_CHAR);
    operator=(prhs);
}
/*============================================================================*/

    CHLString::CHLString(
    const wchar_t               *prhs )

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif
    InitNULL(HLSTRING_UNICODE);
    operator=(prhs);
}
/*============================================================================*/

    CHLString::CHLString(
    const char                  *prhs1,
    const char                  *prhs2 )

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif
    InitNULL(HLSTRING_CHAR);

    LOGASSERT(prhs1 != NULL);
    LOGASSERT(prhs2 != NULL);
    int iL1 = (int)strlen(prhs1);
    int iL2 = (int)strlen(prhs2);
    if (!Init(HLSTRING_CHAR, iL1 + iL2 + 1))
        return;

    memcpy(m_pBuffer, prhs1, iL1);
    memcpy(m_pBuffer + iL1, prhs2, iL2 + 1);
}
/*============================================================================*/

    CHLString::CHLString(
    const wchar_t               *prhs1,
    const wchar_t               *prhs2 )

/*============================================================================*/
{
    #ifdef DEBUG
        m_pBuffer = NULL;
    #endif
    InitNULL(HLSTRING_UNICODE);

    LOGASSERT(prhs1 != NULL);
    LOGASSERT(prhs2 != NULL);
    int iL1 = (int)wcslen(prhs1);
    int iL2 = (int)wcslen(prhs2);
    if (!Init(HLSTRING_UNICODE, 2*(iL1 + iL2 + 1)))
        return;

    memcpy(m_pBuffer, prhs1, 2*iL1);
    memcpy(m_pBuffer + 2*iL1, prhs2, 2*(iL2 + 1));
}
/*============================================================================*/

    CHLString::~CHLString()

/*============================================================================*/
{
    // Clean up memory.
    if (!IsNULL(m_pBuffer))
    {
        LOGASSERT(m_iNBuffer > 0);
        delete [] m_pBuffer;
    }

    m_pBuffer = NULL;
    #ifdef DEBUG
    m_pWBuffer = NULL;
    #endif
}
/*============================================================================*/

    CHLString                   &CHLString::operator=(
    const CHLString             &rhs )

/*============================================================================*/
{
    Empty();

    if (m_iType == rhs.m_iType)
    {
        Init(rhs.m_iType, rhs.m_iNBuffer, rhs.m_pBuffer);
    }
    else
    {
        #ifdef GATEWAY
			//final type is defined by rhs
			if(m_iType == HLSTRING_CHAR)
			{
				//means incoming is Unicode and this needs to be initialized as Unicode
				Init(HLSTRING_UNICODE, rhs.m_iNBuffer);
				memcpy(m_pBuffer, rhs.m_pBuffer, rhs.m_iNBuffer);
			}
			else
			{
				//means incoming is CHAR then initialize this as CHAR
				int iNChars = rhs.m_iNBuffer;// / 2;
                if (iNChars == 1)
                {
                    InitNULL(HLSTRING_CHAR);
                }
                else
                {
                    Init(HLSTRING_CHAR, iNChars);
				}
				memcpy(m_pBuffer, rhs.m_pBuffer, rhs.m_iNBuffer);
			}

        #else
		#ifdef CRYSTALPAD
	        Init(rhs.m_iType, rhs.m_iNBuffer, rhs.m_pBuffer);
		#else
            if (m_iType == HLSTRING_CHAR)
                operator+=( (char*) CHLString(rhs) );
            else
                operator+=( (wchar_t*) CHLString(rhs) );
		#endif
        #endif
    }

    return *this;
}
/*============================================================================*/

    CHLString                   &CHLString::operator=(
    const char                  *prhs )

/*============================================================================*/
{
    Empty();

    LOGASSERT(prhs != NULL);

    if (m_iType == HLSTRING_CHAR)
        Init(HLSTRING_CHAR, (int)strlen(prhs) + 1, prhs);
    else
        operator+=( (wchar_t*) CHLString(prhs) );

    return *this;
}
/*============================================================================*/

    CHLString                   &CHLString::operator=(
    const wchar_t               *prhs )

/*============================================================================*/
{
    Empty();

    LOGASSERT(prhs != NULL);

    if (m_iType == HLSTRING_CHAR)
        operator+=( (char*) CHLString(prhs) );
    else
        Init(HLSTRING_UNICODE, SIZEOF_WCHAR_T*((int)wcslen(prhs) + 1), (char*) prhs);

    return *this;
}
/*============================================================================*/

    CHLString::operator         char*()

/*============================================================================*/
{
    // Assumes this is char...
    if (m_iType == HLSTRING_CHAR)
        return m_pBuffer;

    // Convert us.
    if (!ConvertTochar())
    {
        #ifdef IPHONE
            return (char*)"";
        #else
            return "";
        #endif
    }

    return m_pBuffer;
}
/*============================================================================*/

    CHLString::operator         wchar_t*()

/*============================================================================*/
{
    if (m_iType == HLSTRING_UNICODE)
        return (wchar_t*) m_pBuffer;

    // Convert us.
    if (!ConvertTowchar_t())
    {
        #ifdef IPHONE
            return (wchar_t*)L"";
        #else
            return L"";
        #endif
    }

    return (wchar_t*) m_pBuffer;
}
/*============================================================================*/

    BOOL                        CHLString::ConvertToTCHAR()

/*============================================================================*/
{
    #ifdef UNICODE
        return ConvertTowchar_t();
    #else
        return ConvertTochar();
    #endif
}
/*============================================================================*/

    BOOL                        CHLString::ConvertTochar()

/*============================================================================*/
{
    if (m_iType == HLSTRING_CHAR)
        return TRUE;

    if (m_iNBuffer != 0)
    {
        // Convert just the chars first.
        int iNChars = m_iNBuffer / SIZEOF_WCHAR_T;
        char *pNew = new char[iNChars];
        if (pNew == NULL)
            return FALSE;
        if (iNChars > 1)
        {
            int iResult = (int) wcstombs(pNew, (wchar_t*) m_pBuffer, iNChars-1);
            if (iResult != (iNChars - 1))
            {
                LOGASSERT(_T("CHLString ConvertTochar failed...\n"));
                delete []pNew;
                return FALSE;
            }
        }
        pNew[iNChars-1] = '\0';

        // Swap buffers, clear out old.
        if (!IsNULL(m_pBuffer))
            delete [] m_pBuffer;
        LOGASSERT(pNew != NULL);
        m_pBuffer = pNew;
        #ifdef DEBUG
            m_pWBuffer = (wchar_t*) m_pBuffer;
        #endif
        m_iNBuffer = iNChars;
    }
    m_iType = HLSTRING_CHAR;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLString::ConvertTowchar_t()

/*============================================================================*/
{
    if (m_iType == HLSTRING_UNICODE)
        return TRUE;

    if (m_iNBuffer != 0)
    {
		//in some cases char buffer may have wchar data while type is not unicode
		//this requires different type of conversion
		//int iNonUnicodeCharIndex = IndexOfNonASCIIChar(m_pBuffer, m_iNBuffer);
		// Convert just the chars, then handle the null.
		int iNBytes = SIZEOF_WCHAR_T * m_iNBuffer;
		wchar_t *pwNew = new wchar_t[m_iNBuffer];
		char *pNew = (char*)pwNew;
		if (pNew == NULL)
			return FALSE;
		if (m_iNBuffer > 1)
		{
			//if(iNonUnicodeCharIndex < 0)
			if(this->Type() == CHLString::HLSTRING_CHAR)
			{
				int iResult = (int) mbstowcs((wchar_t*) pNew, m_pBuffer, m_iNBuffer-1);
				if (iResult != (m_iNBuffer - 1))
				{
					LOGASSERT(_T("CHLString ConvertTowchar_t failed...\n"));
					delete []pNew;
					return FALSE;
				}
			}
			else
			{
				for(int i = 0; i < m_iNBuffer; i++)
				{
					pNew[i] = m_pBuffer[i];
				}
				m_iNBuffer =  m_iNBuffer/SIZEOF_WCHAR_T + 1;
				iNBytes = m_iNBuffer * SIZEOF_WCHAR_T;
			}
		}
		((wchar_t*) pNew)[m_iNBuffer-1] = (wchar_t) '\0';

        // Swap buffers, clear out old.
        if (!IsNULL(m_pBuffer))
            delete [] m_pBuffer;
        LOGASSERT(pNew != NULL);
        m_pBuffer = pNew;
        #ifdef DEBUG
            m_pWBuffer = (wchar_t*) m_pBuffer;
        #endif
        m_iNBuffer = iNBytes;
    }
    m_iType = HLSTRING_UNICODE;

    return TRUE;
}
#ifdef GATEWAY
/*============================================================================*/

	BOOL						CHLString::ConvertTocharCodePage(
	int							iCodePage)

/*============================================================================*/
{
	if(m_iType == HLSTRING_CHAR)
		return TRUE;

	int iLength = this->GetLength();
	wchar_t*	wcBuffer = new wchar_t[iLength];
	memcpy(wcBuffer, this->GetBufferWCHAR(iLength), iLength * SIZEOF_WCHAR_T);
	int iConverted = WideCharToMultiByte(iCodePage, 0, wcBuffer, iLength, m_pBuffer, iLength * SIZEOF_WCHAR_T, NULL, FALSE);
    if (iConverted < iLength)
    {
    	ReleaseBuffer(iConverted);
        DWORD dwError = GetLastError();
        int iTest = dwError;
        return FALSE;
    }

	delete [] wcBuffer;
	m_iType = HLSTRING_CHAR;
	ReleaseBuffer(iConverted);
    return TRUE;
}
#endif

/*============================================================================*/

    CHLString                   &CHLString::operator+=(
    const char                  *prhs )

/*============================================================================*/
{
    // Not supported if we're currently a UNICODE string: convert first.
    if (m_iType != HLSTRING_CHAR)
        ConvertTochar();

    // Check.
    LOGASSERT(prhs != NULL);
    int iNNew = (int)strlen(prhs);
    if (iNNew == 0)
        return *this;

    char *pOld = m_pBuffer;
    int iOld = m_iNBuffer;
    int iNBytes = (m_iNBuffer == 0) ? (iNNew + 1) : (m_iNBuffer + iNNew);
    if (!Init(m_iType, iNBytes))
    {
        LOGASSERT(pOld != NULL);
        m_pBuffer = pOld;
        m_iNBuffer = iOld;
        return *this;
    }

    // Copy over old (less '\0'), then new (plus '\0').
    if (iOld > 1)
    {
        memcpy(m_pBuffer, pOld, iOld-1);
        memcpy(m_pBuffer+iOld-1, prhs, iNNew + 1);
    }
    else
        memcpy(m_pBuffer, prhs, iNNew + 1);

    // Cleanup.
    if (!IsNULL(pOld))
        delete [] pOld;

    return *this;
}
/*============================================================================*/

    CHLString                   &CHLString::operator+=(
    const wchar_t               *prhs )

/*============================================================================*/
{
    // Not supported if we're currently a UNICODE string: convert first.
    if (m_iType != HLSTRING_UNICODE)
        ConvertTowchar_t();

    // Check.
    LOGASSERT(prhs != NULL);
    int iNNew = (int)wcslen(prhs);
    if (iNNew == 0)
        return *this;

    char *pOld = m_pBuffer;
    int iOld = m_iNBuffer;
    int iNBytes = (m_iNBuffer == 0) ? SIZEOF_WCHAR_T*(iNNew + 1) : (m_iNBuffer + SIZEOF_WCHAR_T*iNNew);
    if (!Init(m_iType, iNBytes))
    {
        LOGASSERT(pOld != NULL);
        m_pBuffer = pOld;
        m_iNBuffer = iOld;
        return *this;
    }

    // Copy over old (less '\0'), then new (plus '\0').
    if (iOld > 1)
    {
        memcpy(m_pBuffer, pOld, iOld-SIZEOF_WCHAR_T);
        memcpy(m_pBuffer+iOld-SIZEOF_WCHAR_T, prhs, SIZEOF_WCHAR_T*(iNNew + 1));
    }
    else
        memcpy(m_pBuffer, prhs, SIZEOF_WCHAR_T*(iNNew + 1));

    // Cleanup.
    if (!IsNULL(pOld))
        delete [] pOld;

    return (*this);
}
/*============================================================================*/

    TCHAR                       &CHLString::operator[](
    int                         iIndex )

/*============================================================================*/
{
    LOGASSERT(m_iType == HLSTRING_DEFAULT);
    #ifdef UNICODE
        LOGASSERT((iIndex >= 0) && (iIndex < (m_iNBuffer/SIZEOF_WCHAR_T)));
        return ((wchar_t*) m_pBuffer)[iIndex];
    #else
        LOGASSERT((iIndex >= 0) && (iIndex < m_iNBuffer));
        return m_pBuffer[iIndex];
    #endif
}
/*============================================================================*/

    void                        *CHLString::GetBuffer(
    int                         iLength )

/*============================================================================*/
{
    LOGASSERT(iLength >= 0);
    int iNBytes;
    if (m_iType == HLSTRING_CHAR)
    {
        if (iLength < m_iNBuffer)
            return m_pBuffer;
        iNBytes = iLength + 1;
    }
    else
    {
        if (iLength < (m_iNBuffer / SIZEOF_WCHAR_T))
            return m_pBuffer;
        iNBytes = SIZEOF_WCHAR_T * (iLength + 1);
    }

    // Check: probably should do some checking if you're getting this ASSERT.
    // No point in getting a pointer to an empty string.
    LOGASSERT(iLength != 0);

    // Need to expand the buffer...
    char *pOld = m_pBuffer;
    int iOld = m_iNBuffer;
    if (!Init(m_iType, iNBytes))
    {
        LOGASSERT(pOld != NULL);
        m_pBuffer = pOld;
        m_iNBuffer = iOld;
        return NULL;
    }

    // Copy over string, including terminator, and set remaining to 0.
    memcpy(m_pBuffer, pOld, iOld);
    memset(m_pBuffer + iOld, 0, iNBytes - iOld);

    // Delete old memory.
    if (!IsNULL(pOld))
        delete [] pOld;

    return m_pBuffer;
}
/*============================================================================*/

    char                        *CHLString::GetBufferCHAR(
    int                         iLength )

/*============================================================================*/
{
    ConvertTochar();
    return (char*)GetBuffer(iLength);
}
/*============================================================================*/

    wchar_t                     *CHLString::GetBufferWCHAR(
    int                         iLength )

/*============================================================================*/
{
    ConvertTowchar_t();
    return (wchar_t*)GetBuffer(iLength);
}
/*============================================================================*/

    TCHAR                       *CHLString::GetBufferTCHAR(
    int                         iLength )

/*============================================================================*/
{
    #ifdef UNICODE
        return GetBufferWCHAR(iLength);
    #else
        return GetBufferCHAR(iLength);
    #endif
}
/*============================================================================*/

    void                        CHLString::ReleaseBuffer(
    int                         iLength )

/*============================================================================*/
{
    LOGASSERT(iLength >= -1);

    if (iLength == 0)
    {
        Empty();
        return;
    }

    //
    // This is OK if iLength < current buffer. We'll lower m_iNBuffer so we
    // have the correct length, but if later the string wants to grow again,
    // we won't know how much extra there is: !***! could fix this up later
    // so we keep track of memory allocated and string length separately.
    //
    if (m_iType == HLSTRING_CHAR)
    {
        if (iLength == -1)
            iLength = (int)strlen(m_pBuffer);

        LOGASSERT(iLength < m_iNBuffer);
        iLength = MIN(m_iNBuffer-1, iLength);
        m_pBuffer[iLength] = '\0';
        m_iNBuffer = iLength + 1;
    }
    else
    {
        if (iLength == -1)
            iLength = (int)wcslen((wchar_t*) m_pBuffer);

        LOGASSERT(iLength < (m_iNBuffer / SIZEOF_WCHAR_T));
        iLength = MIN((m_iNBuffer/SIZEOF_WCHAR_T) - 1, iLength);
        wchar_t *pWBuf = (wchar_t*)m_pBuffer;
        pWBuf[iLength] = 0x00;

        m_iNBuffer = SIZEOF_WCHAR_T * (iLength + 1);
    }
}
/*============================================================================*/

    #ifdef IPHONE
        int                         CHLString::Format(
        const char                  *pFormat,
        ... )
    #else
        int                         CHLString::Format(
        char                        *pFormat,
        ... )
    #endif

/*============================================================================*/
{
    // Clear out anything currently in our string.
    Empty();

    //
    // We first try to format to a fixed length and relatively short string,
    // and assuming that works, copy the string to our normal area. If it fails
    // then we do the much slower process of calling cprintf to get the count,
    // and then repeating the call to get the final answer.
    //
    // So this is fast in most cases, but slow for long strings.
    //
    #if defined (GATEWAY) | defined(MAC_OS)
        if (m_iType != HLSTRING_CHAR)
            ConvertTochar();

        LOGASSERT(pFormat != NULL);
        va_list args;
        char temp[256];
        va_start(args, pFormat);
        int iResult = _vsnprintf(temp, 255, pFormat, args);
        va_end(args);

        // OK?
        if (iResult != -1)
        {
            temp[iResult] = 0;
            if (!Init(m_iType, iResult+1, temp))
                return 0;
            return iResult;
        }

        // Keep going...
        int iNBuffer = 512;
        while (TRUE)
        {
            // Get next size up in memory.
            Empty();

            // This Init() does NOT Empty First
            if (!Init(m_iType, iNBuffer))
                return 0;

            // Try again.
            va_start(args, pFormat);
            iResult = _vsnprintf(m_pBuffer, iNBuffer-1, pFormat, args);
            va_end(args);

            // Check.
            if (iResult != -1)
            {
                m_iNBuffer = iResult+1;
                return iResult;
            }

            // Bump it up...
            iNBuffer += 256;
        }
    #else
        LOGASSERT(pFormat != NULL);
        va_list args;
        if (m_iType == HLSTRING_CHAR)
        {
            char temp[256];
            va_start(args, pFormat);
            int iResult = _vsnprintf(temp, 255, pFormat, args);
            va_end(args);

            // OK?
            if (iResult != -1)
            {
                temp[iResult] = 0;
                if (!Init(m_iType, iResult+1, temp))
                    return 0;
                return iResult;
            }

            // Keep going...
            int iNBuffer = 512;
            while (TRUE)
            {
                // Get next size up in memory.
                Empty();

                // This Init() does NOT Empty First
                if (!Init(m_iType, iNBuffer))
                    return 0;

                // Try again.
                va_start(args, pFormat);
                iResult = _vsnprintf(m_pBuffer, iNBuffer-1, pFormat, args);
                va_end(args);

                // Check.
                if (iResult != -1)
                {
                    m_iNBuffer = iResult+1;
                    return iResult;
                }

                // Bump it up...
                iNBuffer += 256;
            }
        }
        else
        {
            // In this case we need to convert the format list to UNICODE...
            CHLString sNew(pFormat);
            if (!sNew.ConvertTowchar_t())
                return 0;
            wchar_t *pNewFmt = sNew;

            // And then proceed as above...
            wchar_t temp[256];
            va_start(args, pFormat);
            int iResult = _vsnwprintf(temp, 255, pNewFmt, args);
            va_end(args);

            // OK?
            if (iResult != -1)
            {
                temp[iResult] = 0;
                if (!Init(m_iType, (iResult+1)* sizeof(WCHAR), (char*) &temp))
                    return 0;
                return iResult;
            }

            // Keep going...
            int iNBuffer = 512;
            while (TRUE)
            {
                // Empty First
                Empty();

                // Get next size up in memory.
                if (!Init(m_iType, iNBuffer*SIZEOF_WCHAR_T))
                    return 0;

                // Try again.
                va_start(args, pFormat);
                iResult = _vsnwprintf((wchar_t*) m_pBuffer, iNBuffer-1, pNewFmt, args);
                va_end(args);

                // Check.
                if (iResult != -1)
                {
                    m_iNBuffer = SIZEOF_WCHAR_T * (iResult+1);
                    return iResult;
                }

                // Bump it up...
                iNBuffer += 256;
            }
        }
    #endif
}
#ifndef MAC_OS
/*============================================================================*/

    int                         CHLString::Format(
    wchar_t                     *pFormat,
    ... )

/*============================================================================*/
{
    // Clear out anything currently in our string.
    Empty();

    //
    // Like above, but now we're coming in with wide string.
    //
    LOGASSERT(pFormat != NULL);
    va_list args;
    if (m_iType == HLSTRING_CHAR)
        ConvertTowchar_t();

    wchar_t temp[256];
    va_start(args, pFormat);
    int iResult = _vsnwprintf(temp, 255, pFormat, args);
    va_end(args);

    // OK?
    if (iResult != -1)
    {
        if (!Init(m_iType, SIZEOF_WCHAR_T*(iResult + 1), (char *) temp))
            return 0;
        return iResult;
    }

    // Keep going...
    int iNBuffer = 512;
    while (TRUE)
    {
        Empty();
    
        // Get next size...
        if (!Init(m_iType, iNBuffer*SIZEOF_WCHAR_T))
            return 0;

        // Try again.
        va_start(args, pFormat);
        iResult = _vsnwprintf((wchar_t*) m_pBuffer, iNBuffer-1, pFormat, args);
        va_end(args);

        // Check.
        if (iResult != -1)
        {
            m_iNBuffer = SIZEOF_WCHAR_T * (iResult+1);
            return iResult;
        }

        // Bump it up...
        iNBuffer += 256;
    }
}
#endif
/*============================================================================*/

    int                         CHLString::Find(
    int                         iChar,
    int                         iStart )

/*============================================================================*/
{
    iStart = MAX(0, iStart);

    if (m_iType == HLSTRING_CHAR)
    {
        if (m_iNBuffer <= 1)
            return -1;
        
        LOGASSERT((iStart >= 0) && (iStart < (m_iNBuffer-1)));
        char *pChar = strchr(m_pBuffer + iStart, iChar);
        if (pChar == NULL)
            return -1;
        return (int)(pChar - m_pBuffer);
    }
    else
    {
        if (m_iNBuffer <= SIZEOF_WCHAR_T)
            return -1;
        
        LOGASSERT( (iStart >= 0) && (iStart < ((m_iNBuffer)/SIZEOF_WCHAR_T - 1)) );
        wchar_t *pBuf = (wchar_t *) m_pBuffer;
        wchar_t *pChar = wcschr(pBuf + iStart, (wchar_t) iChar);
        if (pChar == NULL)
            return -1;
        return (int)(pChar - pBuf);
    }
}
/*============================================================================*/

    int                         CHLString::ReverseFind(
    int                         iChar )

/*============================================================================*/
{
    if (m_iType == HLSTRING_CHAR)
    {
        char *pChar = strrchr(m_pBuffer, iChar);
        if (pChar == NULL)
            return -1;
        return (int)(pChar - m_pBuffer);
    }
    else
    {
        wchar_t *pBuf = (wchar_t *) m_pBuffer;
        wchar_t *pChar = wcsrchr(pBuf, (wchar_t) iChar);
        if (pChar == NULL)
            return -1;
        return (int)(pChar - pBuf);
    }
}
/*============================================================================*/

    BOOL                        CHLString::IsNumeric()

/*============================================================================*/
{
    int iLen = GetLength();
    if (m_iType == HLSTRING_CHAR)
    {
        char *pChar = m_pBuffer;
        while (iLen > 0)
        {
            char cThis = *pChar;
            if (!IsNumeric(cThis))
                return FALSE;
            pChar++;
            iLen--;
        }
    }
    else
    {
        wchar_t *pChar = (wchar_t *) m_pBuffer;
        while (iLen > 0)
        {
            wchar_t cThis = *pChar;
            if (!IsNumeric(cThis))
                return FALSE;

            pChar++;
            iLen--;
        }
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLString::IsNumeric(wchar_t c) // 05/15/2012 VB

/*============================================================================*/
{
    if (c >= '0' && c <= '9') 
        return TRUE;

    switch (c)
    {
    case 'k':   
    case 'K':   
    case 'm':   
    case 'M':   
    case 'H':   
    case 'h':   
    case 'A':   
    case 'a':   
    case 'F':   
    case 'f':   
    case 'Z':   
    case 'z':   
    case '-':   
    case 0xB0:  
    case '.':  
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

    CHLString                   CHLString::Left(
    int                         iCount )

/*============================================================================*/
{
    if (iCount < 0)
    {
        LOGASSERT(FALSE);
        iCount = 0;
    }

    if (iCount > GetLength())
    {
        // We can just use __min() here but looking for asserts from MultBricks
        iCount = GetLength();
    }

    CHLString sResult;
    if (m_iType == HLSTRING_CHAR)
    {

        sResult.Init(m_iType, iCount + 1, m_pBuffer);
        sResult.m_pBuffer[iCount] = '\0';
    }
    else
    {
        sResult.Init(m_iType, SIZEOF_WCHAR_T*(iCount + 1), m_pBuffer);
        ((wchar_t*) (sResult.m_pBuffer))[iCount] = (wchar_t) '\0';
    }
    return sResult;
}
/*============================================================================*/

    CHLString                   CHLString::Mid(
    int                         iFirst,
    int                         iCount )

/*============================================================================*/
{
    LOGASSERT(iFirst >= 0);
    LOGASSERT(iCount >= 0);

    if (iCount == 0)
        return _T("");

    if (iFirst >= GetLength() || iFirst < 0)
        return _T("");

    if (iCount > (GetLength()-iFirst))
    {
        // We can just use __min() here but looking for asserts from MultBricks
        #ifdef UNDER_CE
            LOGASSERT(FALSE);
        #endif
        iCount = GetLength()-iFirst;
        iCount = __max(0, iCount);
    }

    CHLString sResult;
    if (m_iType == HLSTRING_CHAR)
    {
        sResult.Init(m_iType, iCount + 1, m_pBuffer + iFirst);
        sResult.m_pBuffer[iCount] = '\0';
    }
    else
    {
        sResult.Init(m_iType, SIZEOF_WCHAR_T*(iCount + 1), m_pBuffer + SIZEOF_WCHAR_T*iFirst);
        ((wchar_t*) (sResult.m_pBuffer))[iCount] = (wchar_t) '\0';
    }
    return sResult;
}
/*============================================================================*/

    CHLString                   CHLString::Right(
    int                         iCount )

/*============================================================================*/
{
    if (iCount < 0)
    {
        LOGASSERT(FALSE);
        iCount = 0;
    }

    if (iCount > GetLength())
    {
        // We can just use __min() here but looking for asserts from MultBricks
        #ifdef UNDER_CE
            LOGASSERT(FALSE);
        #endif
        iCount = GetLength();
    }

    CHLString sResult;
    if (m_iType == HLSTRING_CHAR)
    {
        sResult.Init(m_iType, iCount + 1, m_pBuffer + m_iNBuffer - (iCount+1));
        LOGASSERT(sResult.m_pBuffer[iCount] == '\0');
    }
    else
    {
        sResult.Init(m_iType, SIZEOF_WCHAR_T*(iCount + 1), m_pBuffer + m_iNBuffer - SIZEOF_WCHAR_T*(iCount+1));
        LOGASSERT(((wchar_t*) sResult.m_pBuffer)[iCount] == (wchar_t) '\0');
    }
    return sResult;
}
/*============================================================================*/

    void                        CHLString::TrimLeft(
    const char                  *prhs )

/*============================================================================*/
{
    if (m_iType != HLSTRING_CHAR)
    {
        if (!ConvertTochar())
            return;
    }

    // Don't do on empty string
    if (m_iNBuffer <= 1)
        return;

    // Start at the beginning and scan right: as soon as we find a character
    // that is not in prhs, then bail.
    char *pFirst = m_pBuffer;
    char *pLast = m_pBuffer + m_iNBuffer - 1;
    char *pKeep = NULL;
    while (strchr(prhs, *pFirst) != NULL)
    {
        pKeep = pFirst + 1;
        pFirst++;
        if (pFirst == pLast)
            break;
    }
    if (pKeep != NULL)
    {
        // Number of bytes to remove, then move over.
        int iNTrim = (int)(pKeep - m_pBuffer);
        m_iNBuffer -= iNTrim;
        memmove(m_pBuffer, m_pBuffer + iNTrim, m_iNBuffer);
    }
}
/*============================================================================*/

    void                        CHLString::TrimLeft(
    const wchar_t               *prhs )

/*============================================================================*/
{
    if (m_iType != HLSTRING_UNICODE)
    {
        ConvertTowchar_t();
        return;
    }

    // Don't do on empty string
    if (GetLength() < 1)
        return;

    // Start at the beginning and scan right: as soon as we find a character
    // that is not in prhs, then bail.
    int iNTCHARS = m_iNBuffer / SIZEOF_WCHAR_T;
    wchar_t *pMyBuf = (wchar_t*) m_pBuffer;
    wchar_t *pFirst = (wchar_t*) m_pBuffer;
    wchar_t *pLast = pMyBuf + iNTCHARS - 1;
    wchar_t *pKeep = NULL;
    while (wcschr(prhs, *pFirst) != NULL)
    {
        pKeep = pFirst + 1;
        if (pFirst == pLast)
            break;
        pFirst++;
    }
    if (pKeep != NULL)
    {
        // Number of TCHARS to remove, then move over.
        int iNTrim = (int)(pKeep - pMyBuf);
        m_iNBuffer -= (iNTrim * SIZEOF_WCHAR_T);
        memmove(pMyBuf, pMyBuf + iNTrim, m_iNBuffer);
    }
}
/*============================================================================*/

    void                        CHLString::TrimRight(
    const char                  *prhs )

/*============================================================================*/
{
    if (m_iType != HLSTRING_CHAR)
    {
        ConvertTochar();
        return;
    }

    if (m_iNBuffer <= 1)
        return;

    // Start at the end and scan left: as soon as we find a character that is
    // not in prhs, then bail.
    char *pLast = m_pBuffer + m_iNBuffer - 1;
    char *pTrunc = NULL;
    while (strchr(prhs, *pLast) != NULL)
    {
        pTrunc = pLast;
        if (pLast == m_pBuffer)
            break;
        pLast--;
    }
    if (pTrunc != NULL)
    {
        // Truncate at left-most matching character
        *pTrunc = '\0';
        m_iNBuffer = (int)(pTrunc - m_pBuffer) + 1;
    }
}
/*============================================================================*/

    void                        CHLString::TrimRight(
    const wchar_t               *prhs )

/*============================================================================*/
{
    if (m_iType != HLSTRING_UNICODE)
    {
        ConvertTowchar_t();
        return;
    }

    if (GetLength() < 1)
        return;

    // Start at the end and scan left: as soon as we find a character that is
    // not in prhs, then bail.
    wchar_t *pMyBuf = (wchar_t*)m_pBuffer;
    int iNTCHARS = m_iNBuffer / SIZEOF_WCHAR_T;
    wchar_t *pLast = pMyBuf + iNTCHARS - 1;
    wchar_t *pTrunc = NULL;
    while (wcschr(prhs, *pLast) != NULL)
    {
        pTrunc = pLast;
        if (pLast == (wchar_t*) m_pBuffer)
            break;
        pLast--;
    }
    if (pTrunc != NULL)
    {
        // Truncate at left-most matching character
        *pTrunc = L'\0';
        m_iNBuffer = SIZEOF_WCHAR_T*((int)(pTrunc - (wchar_t*)m_pBuffer) + 1);
    }
}
/*============================================================================*/

    void                        CHLString::Replace(
    char                        cOld,
    char                        cNew )

/*============================================================================*/
{
    ConvertTochar();

    int iLen = GetLength();
    char  *pBuf = (char*)GetBuffer(iLen);
    for (int i = 0; i < iLen; i++)
    {
        if (pBuf[i] == cOld)
            pBuf[i] = cNew;
    }

    ReleaseBuffer(iLen);
}
/*============================================================================*/

    void                        CHLString::MakeUpper()

/*============================================================================*/
{
    //ConvertTochar();
	if(m_iType == HLSTRING_CHAR)
	{
		int iLen = GetLength();
		char  *pBuf = (char*)GetBuffer(iLen);
		for (int i = 0; i < iLen; i++)
		{
			pBuf[i] = toupper(pBuf[i]);
		}

		ReleaseBuffer(iLen);
	}
	else
	{
		int iLength = GetLength();
		wchar_t* pBuf = GetBufferWCHAR(iLength);
		for(int i = 0; i < iLength; i++)
		{
			pBuf[i] = ::towupper(pBuf[i]);
		}
	}
}
/*============================================================================*/

	void                        CHLString::MakeLower()

/*============================================================================*/
{
    //ConvertTochar();
	if(m_iType == HLSTRING_CHAR)
	{
		int iLen = GetLength();
		char  *pBuf = (char*)GetBuffer(iLen);
		for (int i = 0; i < iLen; i++)
		{
			pBuf[i] = tolower(pBuf[i]);
		}

		ReleaseBuffer(iLen);
	}
	else
	{
		int iLength = GetLength();
		wchar_t* pBuf = GetBufferWCHAR(iLength);
		for(int i = 0; i < iLength; i++)
		{
			pBuf[i] = ::towlower(pBuf[i]);
		}
	}
}
#ifdef GATEWAY
/*============================================================================*/

    void                        CHLString::UTF8ToASCII()

/*============================================================================*/
{
	if(m_iType == HLSTRING_UNICODE)
        return;
    if (IsNULL(m_pBuffer))
        return;

    BOOL bConvert = FALSE;
    int iLen = m_iNBuffer-1;
    if (iLen == 12)     
        int iDebug = 1;
    int i = 0;
    for (i = 0; i < iLen && !bConvert; i++)
    {
        BYTE byChar = (BYTE)m_pBuffer[i];
        if (byChar & 0x80)
            bConvert = TRUE;
    }

    if (!bConvert)
        return;

    m_iType = HLSTRING_UNICODE;
    char *pNewBuffer = new char[m_iNBuffer*2];
    memset(pNewBuffer, 0, m_iNBuffer*2);
    m_iNBuffer = 2;

    i = 0;
    while (i < iLen)
    {
        BYTE byChar = (BYTE)m_pBuffer[i];
        wchar_t wChar = 0;
        if (byChar & 0x80)
        {
            //          11100000  11000000 
            if ((byChar & 0xE0) == 0xC0)
            {
                // 11 bit
                if (i < iLen-1)
                {
                    DWORD dwY = byChar;
                    DWORD dwX = m_pBuffer[i+1];
                    LOGASSERT((dwX & 0xC0) == 0x80);
                    wChar = (WORD)(((dwY & 0x1F) << 6) | (dwX & 0x3F));
                }            

                i+= 2;
            }
            //           11110000  11100000
            else if ((byChar & 0xF0) == 0xE0)
            {
                // 16 bit
                if (i < iLen-2)
                {
                    DWORD dwZ = byChar;
                    DWORD dwY = m_pBuffer[i+1];
                    DWORD dwX = m_pBuffer[i+2];
                    LOGASSERT((dwY & 0xC0) == 0x80);
                    LOGASSERT((dwX & 0xC0) == 0x80);
                    wChar = (WORD)(((dwZ & 0x0F) << 12) | ((dwY & 0x3F) << 6) | (dwX & 0x3F));
                }            
    
                i+= 3;
            }
            //           11000000  10000000
            else if ((byChar & 0xC0) == 0x80)
            {
                // Continuation char
                i++;
            }
            else 
            {
                // More than 16 bits, unsupported
                i++;
            }   
        }
        else
        {
            LOGASSERT((byChar & 0x80) == 0);
            wChar = byChar;
            i++;
        }

        if (wChar != 0)
        {
            if (wChar == 0x2019)
                wChar = '\'';

            memcpy(pNewBuffer+m_iNBuffer-2, &wChar, sizeof(wchar_t));
            m_iNBuffer+=2;
        }
    }

    pNewBuffer[m_iNBuffer] = 0;
    pNewBuffer[m_iNBuffer+1] = 0;

    #ifdef DEBUG
        wchar_t *pDebug = (wchar_t*)pNewBuffer;
        int iNChars = m_iNBuffer / 2 - 1;
    #endif


    delete [] m_pBuffer;
    m_pBuffer = pNewBuffer;

    #ifdef DEBUG
        m_pWBuffer = (wchar_t*) m_pBuffer;
    #endif
}
/*============================================================================*/

    void                        CHLString::NCRToASCII()

/*============================================================================*/
{
    BOOL bConvert = FALSE;

    int iLen = GetLength();
	if(m_iType == HLSTRING_UNICODE)
    {
        wchar_t *pBuf = this->GetBufferWCHAR(iLen);
        for (int i = 0; i < iLen-1 && !bConvert; i++)
        {
            if (pBuf[i] == '\\')
                bConvert = TRUE;
        }
    }
    else
    {
        char *pBuf = this->GetBufferCHAR(iLen);
        for (int i = 0; i < iLen-1 && !bConvert; i++)
        {
            if (pBuf[i] == '\\')
                bConvert = TRUE;
        }
    }
    
    if (!bConvert)
        return;


    ConvertTowchar_t();
    iLen = GetLength();
    wchar_t *pBuf = GetBufferWCHAR(iLen);

    int i = 0;
    while (i < iLen-4)
    {
        if (pBuf[i] == '\\' && pBuf[i+1] == 'x')
        {
            pBuf[i] = HexToInt(pBuf+i+2, 2);
            memmove(pBuf+i+1, pBuf+i+4, (iLen-i-3)*SIZEOF_WCHAR_T);
            iLen -= 3;
        }

        i++;
    }

    while (i < iLen-4)
    {
        if (pBuf[i] == '\\')
        {
            if (pBuf[i+1] >= '0' && pBuf[i+1] < '9')
            {
                pBuf[i] = DecToInt(pBuf+i+1, 3);
                memmove(pBuf+i+1, pBuf+i+4, (iLen-i-3)*SIZEOF_WCHAR_T);
                iLen -= 3;
            }
        }

        i++;
    }

    i = 0;
    while (i < iLen-1)
    {
        if (pBuf[i] == '\\')
        {
            // Next char is literal
            memmove(pBuf+i, pBuf+i+1, (iLen-i-1)*SIZEOF_WCHAR_T);
            iLen--;
            i++;
        }

        i++;
    }

    ReleaseBuffer(iLen);
}
#endif
/*============================================================================*/

    void                        CHLString::CopyFrom(
    char                        *pStart,
    char                        *pEnd )

/*============================================================================*/
{
    ConvertTochar();
    if (pStart == NULL || pEnd == NULL)
        return;
    if (pEnd < pStart)
        return;
    int iLen = (int)(pEnd-pStart);
    char *pOut = GetBufferCHAR(iLen);
    memcpy(pOut, pStart, iLen);
    ReleaseBuffer(iLen);
}
/*============================================================================*/

    int                         CHLString::Compare(
    const CHLString             &rhs )

/*============================================================================*/
{
    if (m_iType != rhs.m_iType)
    {
        // You are comparing a UNICODE string to a char string: convert one
        // first to make it clear.
        CHLString sCopy = rhs;
        if (Type() == HLSTRING_CHAR)
            sCopy.ConvertTochar();
        else
            sCopy.ConvertTowchar_t();

        if (m_iType == HLSTRING_CHAR)
            return strcmp(m_pBuffer, sCopy.m_pBuffer);
        else
            return wcscmp((wchar_t*) m_pBuffer, (wchar_t*) (sCopy.m_pBuffer));
    }

    if (m_iType == HLSTRING_CHAR)
        return strcmp(m_pBuffer, rhs.m_pBuffer);
    else
        return wcscmp((wchar_t*) m_pBuffer, (wchar_t*) (rhs.m_pBuffer));
}
/*============================================================================*/

    int                         CHLString::Compare(
    const char                  *prhs )

/*============================================================================*/
{
    LOGASSERT(prhs != NULL);
    if (m_iType != HLSTRING_CHAR)
    {
        // You are comparing a UNICODE string to a char string.
        // Convert this to char first to make it clear.
        CHLString sCopy = *this;
        if (!sCopy.ConvertTochar())
            return -1;
        return sCopy.Compare(prhs);
    }
    return strcmp(m_pBuffer, prhs);
}
/*============================================================================*/

    int                         CHLString::Compare(
    const wchar_t               *prhs )

/*============================================================================*/
{
    LOGASSERT(prhs != NULL);
    if (m_iType != HLSTRING_UNICODE)
    {
        // You are comparing a char string to a UNICODE string.
        // Convert this to UNICODE first to make it clear.
        CHLString sCopy = *this;
        if (!sCopy.ConvertTowchar_t())
            return -1;
        return sCopy.Compare(prhs);
    }
    return wcscmp((wchar_t*) m_pBuffer, prhs);
}
/*============================================================================*/

    int                         CHLString::CompareNoCase(
    const CHLString             &rhs )

/*============================================================================*/
{
#ifdef MAC_OS
    CHLString sCopy = rhs;
    if (m_iType != HLSTRING_CHAR)
        ConvertTochar();
    if (sCopy.m_iType != HLSTRING_CHAR)
        sCopy.ConvertTochar();
    return strcasecmp(m_pBuffer, sCopy.m_pBuffer);
#else
    if (m_iType != rhs.m_iType)
    {
        CHLString sCopy = rhs;
 
        if (m_iType == HLSTRING_CHAR)
            sCopy.ConvertTochar();
        else
            sCopy.ConvertTowchar_t();
   
        if (m_iType == HLSTRING_CHAR)
            return _stricmp(m_pBuffer, sCopy.m_pBuffer);
        else
            return _wcsicmp((wchar_t*) m_pBuffer, (wchar_t*) sCopy.m_pBuffer);
    }

    if (m_iType == HLSTRING_CHAR)
        return _stricmp(m_pBuffer, rhs.m_pBuffer);
    else
        return _wcsicmp((wchar_t*) m_pBuffer, (wchar_t*) rhs.m_pBuffer);
#endif
}
/*============================================================================*/

    int                         CHLString::CompareNoCase(
    const char                  *prhs )

/*============================================================================*/
{
    LOGASSERT(prhs != NULL);
    if (m_iType != HLSTRING_CHAR)
    {
        ConvertTochar();
    }
    #ifdef MAC_OS
        return strcasecmp(m_pBuffer, prhs);
    #else
        return _stricmp(m_pBuffer, prhs);
    #endif
}
/*============================================================================*/

    int                         CHLString::CompareNoCase(
    const wchar_t               *prhs )

/*============================================================================*/
{
    #ifdef MAC_OS
        if (m_iType != HLSTRING_CHAR)
        {
            ConvertTochar();
        }
        CHLString sCopy = prhs;
        sCopy.ConvertTochar();
        return strcasecmp(m_pBuffer, sCopy.m_pBuffer);
    #else    
        LOGASSERT(prhs != NULL);
        if (m_iType != HLSTRING_UNICODE)
        {
            ConvertTowchar_t();
        }
        return _wcsicmp((wchar_t*) m_pBuffer, prhs);
    #endif
}
/*============================================================================*/

	void						CHLString::Reverse()

/*============================================================================*/
	{
        // Convert us.
		//if (!ConvertTowchar_t())
		//	return;

		char *pwNew = new char[m_iNBuffer];

		int iNew = 0;

		for(int i = m_iNBuffer - 2; i >= 0; i--, iNew++)
		{
		    memcpy(pwNew + iNew, m_pBuffer + i, 1);
		}
		    
		memset(pwNew + m_iNBuffer - 1, 0, 1);

		memset(m_pBuffer, 0, m_iNBuffer);
		memcpy(m_pBuffer, pwNew, m_iNBuffer);

        if (!IsNULL(m_pBuffer))
            delete [] m_pBuffer;

        LOGASSERT(pwNew != NULL);
        m_pBuffer = pwNew;

        #ifdef DEBUG
            m_pWBuffer = (wchar_t*) m_pBuffer;
        #endif
	}