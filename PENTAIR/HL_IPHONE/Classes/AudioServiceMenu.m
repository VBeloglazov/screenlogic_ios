//
//  AudioServiceMenu.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "AudioServiceMenu.h"
#import "hlm.h"
#import "nsquery.h"
#import "nsmsg.h"
#import "AudioService.h"
#import "tmp3.h"
#import "hlcomm.h"
#import "MP3ItemCell.h"

@implementation CAudioServiceMenuItem

/*============================================================================*/

-(id)initWithData:(NSString*)pName Data:(int)iData Section:(int)iSection Row:(int)iRow Index:(int)iIndex Menu:(CMP3Menu*)pMenu

/*============================================================================*/
{
    if (self == [super initWithName:pName Data:iData Service:NULL])
    {
        m_pMenu = pMenu;
        m_iIndex = iIndex;
        m_iSection = iSection;
        m_iRow = iRow;
        m_iDataState = DATASTATE_INIT;
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_iDataState == DATASTATE_WAIT)
    {
        [CHLComm RemoveSocketSink:self];
    }

    if (m_pImage != NULL)
    {
        CGImageRelease(m_pImage);
    }

    free(m_pJPEG);
    m_pJPEG = NULL;

    
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_AUDIO_SERVICE_DOSERVICEOBJECTA:
        {
            if (m_pImage != NULL)
            {
                CGImageRelease(m_pImage);
                m_pImage = NULL;
                free(m_pJPEG);
                m_pJPEG = NULL;
            }

            int iSize = [pMSG GetInt];
            [pMSG GetInt];
            [pMSG GetInt];


            int iImageType = ENCODE_JPEG;
            
            if ([CHLComm VersionMajor] >= 5 && [CHLComm VersionMinor] >= 3)
            {
                iImageType = [pMSG GetInt];
            }
            
            void *pData = [pMSG GetDataAtReadIndex];

            m_pJPEG = malloc(iSize);
            memcpy(m_pJPEG, pData, iSize);


            CGDataProviderRef pSrc = CGDataProviderCreateWithData(NULL, m_pJPEG, iSize, NULL);
            switch (iImageType)
            {
            case ENCODE_PNG:
                m_pImage = CGImageCreateWithPNGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
                break;
            case ENCODE_JPEG:
                m_pImage = CGImageCreateWithJPEGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
                break;
            }
            CGDataProviderRelease(pSrc);
        }
        break;

        
    default:
        return;
    }

    [m_pMenu SetPacketsInFlight:[m_pMenu GetPacketsInFlight]-1];
    m_iDataState = DATASTATE_OK;
    [CHLComm RemoveSocketSink:self];

    CMP3ItemCell *pCell = [m_pMenu MP3CellForSection:m_iSection Index:m_iRow];
    [pCell SetImage:m_pImage];
}
/*============================================================================*/

-(BOOL)OnIdleVisible

/*============================================================================*/
{
    if (!(m_iType & ITEM_TYPE_IMAGEAVAIL))
        return FALSE;

    if (m_iDataState == DATASTATE_INIT)
    {
        [m_pMenu SetPacketsInFlight:[m_pMenu GetPacketsInFlight]+1];
        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SERVICE_DOSERVICEOBJECTQ];
        [pQ PutInt:[[m_pMenu Service] ID]];
        [pQ PutInt:[m_pMenu ActivePageID]];
        [pQ PutInt:m_iData];
        [pQ PutInt:ITEM_OPERATION_GETIMAGE];
        [pQ PutString:@""];
        [pQ PutInt:m_iData];
        [pQ SendMessageWithMyID:self];
        [pQ release];
        m_iDataState = DATASTATE_WAIT;
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(CGImageRef)GetImage

/*============================================================================*/
{
    if ((m_iType & ITEM_TYPE_IMAGEAVAIL) == 0)
        return NULL;
    if (m_pImage != NULL)
        return m_pImage;

    [self OnIdleVisible];
    return m_pImage;
}
@end


@implementation CAudioServiceMenu

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame
             ContentView:(CMP3ContentView*)pContentView
             Zone:(CAudioZone*)pSubTab 
             Type:(int)iType    
             Sub1:(int)iSub1 
             Sub2:(int)iSub2 
             Title:(NSString*)pTitle 
             Service:(CAudioService*)pService
             NavString:(NSString*) pNavString;

/*============================================================================*/
{

    if (self == [super initWithFrame:rFrame ContentView:pContentView Zone:pSubTab Type:iType Sub1:iSub1 Sub2:iSub2 Title:pTitle Service:pService])
    {
        m_pNavData = pNavString;
        [m_pNavData retain];
        m_iActiveObjectID = iSub2;
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pNavData release];
    [super dealloc];
}
/*============================================================================*/

-(void)Init

/*============================================================================*/
{
//    [self BeginWaitData];
//
//    [self NavigateFrom:m_iSubType1 To:m_iSubType2 Operation:ITEM_OPERATION_GO StringData:m_pNavData];
}
/*============================================================================*/

-(void)LoadFrom:(CNSMSG*)pMSG

/*============================================================================*/
{
    [m_pSections removeAllObjects];
    NSMutableArray *pThisSection = NULL;

    int iNObj = [pMSG GetInt];
    for (int i = 0; i < iNObj; i++)
    {
        int iID = [pMSG GetInt];
        int iType = [pMSG GetInt];
        NSString *pText = [pMSG GetString];

        //int iIndent = ((iType & 0xFFFF0000) >> 16);
        iType = iType & 0x0000FFFF;

        if (iType == 0)
        {
            // New Section
        
            // Check for empty last section
            BOOL bAdded = FALSE;
            if (pThisSection != NULL)
            {
                if ([pThisSection count] < 1)
                    bAdded = TRUE;
            }
            
            if (!bAdded)
            {
                pThisSection = [[NSMutableArray alloc] init];
                [m_pSections addObject:pThisSection];
                [m_pSectionHeaders addObject:pText];
                [pThisSection release];
            }
        }
        else
        {
            if (pThisSection == NULL)
            {
                pThisSection = [[NSMutableArray alloc] init];
                [m_pSections addObject:pThisSection];
//                    [m_pSectionHeaders addObject:m_pTitle];
                [pThisSection release];
            }

            int iSection = [m_pSections count]-1;
            int iRow = [pThisSection count];
            CAudioServiceMenuItem *pItem = [[CAudioServiceMenuItem alloc] initWithData:pText Data:iID Section:iSection Row:iRow Index:i Menu:self];
            if (iType & ITEM_TYPE_PLAYABLE)
                iType |= ITEM_TYPE_ADDABLE;

            [pItem SetType:iType];
            [pThisSection addObject:pItem];
            [pItem release];
        }

    }            

    m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    m_pTableView.separatorColor = [UIColor darkGrayColor];

    m_pTableView.sectionIndexMinimumDisplayRowCount=1000;
    [m_pTableView reloadData];
 
    [self EndWaitData];
}
/*============================================================================*/

-(void)NavigateFrom:(int)iFrom To:(int)iTo Operation:(int)iOp StringData:(NSString*)pStringData

/*============================================================================*/
{
  
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SERVICE_DOSERVICEOBJECTQ];
    [pQ autorelease];
    [pQ PutInt:[m_pService ServiceID]];
    [pQ PutInt:iFrom];
    [pQ PutInt:iTo];
    [pQ PutInt:iOp];
    [pQ PutString:pStringData];
    [pQ PutInt:iTo];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(BOOL)ItemHasSubMenus:(CMP3MenuItem*)pItem

/*===========================================y=================================*/
{
    switch (m_iType)
    {
    case MENU_IRADIO_ALL:
    case MENU_IRADIO_FAVS:
        return TRUE;
    }

    int iType = [pItem GetType];
    return (iType & ITEM_TYPE_PAGE);
}
/*============================================================================*/

-(int)ItemOptions:(CMP3MenuItem*)pItem

/*============================================================================*/
{
    switch (m_iType)
    {
    case MENU_IRADIO_ALL_STATIONS_BYGENRE:
    case MENU_IRADIO_FAV_STATIONS_BYGENRE:
        return ITEM_TYPE_PLAYABLE;
    }

    return ([pItem GetType] & (ITEM_TYPE_PLAYABLE|ITEM_TYPE_ADDABLE|ITEM_TYPE_SEARCH|ITEM_TYPE_IMAGEAVAIL));
}
/*============================================================================*/

-(void)QueueSelectedItem:(BOOL)bPlay

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    int iSection = pPath.section;
    int iIndex = pPath.row;
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:iSection];
    CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:iIndex];
    if (pItem == NULL)
        return;

    int iOp = ITEM_OPERATION_ADD;
    if (bPlay)
        iOp = ITEM_OPERATION_PLAY;

    [self BeginWaitData];
    [self NavigateFrom:m_iActiveObjectID To:[pItem Data] Operation:iOp StringData:@""];
}
/*============================================================================*/

-(IBAction)SearchEvent:(id)sender

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:pPath.section];
    CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:pPath.row];

    UITextField *pTextField = (UITextField*)sender;
    NSString *pText = pTextField.text;

    [super SearchEvent:sender];

    int iSub2 = [pItem Data];
    [m_pContentView PushNewViewFrom:m_iSubType1 To:iSub2 Data:pText];
}
/*============================================================================*/

-(int)ActivePageID

/*============================================================================*/
{
    return m_iActiveObjectID;
}
/*============================================================================*/

-(void)SetID:(int)iID

/*============================================================================*/
{
    if (m_iActiveObjectID == iID)
        return;
    m_iActiveObjectID = iID;
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SERVICE_DOSERVICEOBJECTQ];
    [pQ autorelease];
    [pQ PutInt:[m_pService ServiceID]];
    [pQ PutInt:0];
    [pQ PutInt:iID];
    [pQ PutInt:ITEM_OPERATION_GO];
    [pQ PutString:NULL];
    [pQ PutInt:iID];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)OnItemSelected:(int)iID

/*============================================================================*/
{
    [m_pContentView PushNewViewFrom:m_iSubType1 To:iID Data:NULL];
}


@end
