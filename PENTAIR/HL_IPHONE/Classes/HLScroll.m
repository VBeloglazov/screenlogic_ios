//
//  HLScroll.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/3/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HLScroll.h"
#import "HLView.h"
#import "MainTabVC.h"

@implementation CHLScrollView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame MainTabPage:(CMainTabViewController*)pMainTabPage

/*============================================================================*/
{
    if (self = [super initWithFrame:frame]) 
    {
        // Initialization code
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        m_pMainTabPage = pMainTabPage;
    }
    return self;
}
/*============================================================================*/

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event

/*============================================================================*/
{
    UIView *pHitTest = [self FindExclusiveView:self.subviews WithPoint:point Event:event];
    if (pHitTest != NULL)
    {
        [self setScrollEnabled:FALSE];
        return [super hitTest:point withEvent:event];
    }


    [self setScrollEnabled:[m_pMainTabPage EnableScroll]];
    return [super hitTest:point withEvent:event];
}
/*============================================================================*/

- (UIView *)FindExclusiveView:(NSArray*)pSubViews WithPoint:(CGPoint)point Event:(UIEvent*)event

/*============================================================================*/
{
    for (int iSubView = [pSubViews count]-1; iSubView >= 0; iSubView--)
    {
        UIView *pSub = (UIView*)[pSubViews objectAtIndex:iSubView];
        CGPoint ptSub = [self convertPoint:point toView:pSub];
        if ([pSub pointInside:ptSub withEvent:event])
        {
            // Go one level deeper
            UIView *pTag = [pSub viewWithTag:999];
            if (pTag != NULL)
            {
                CGPoint ptThisSub = [self convertPoint:point toView:pTag];
                if ([pTag pointInside:ptThisSub withEvent:event])
                    return pTag;
            }

            return [self FindExclusiveView:[pSub subviews] WithPoint:point Event:event];
        }
    }

    return NULL;
}

@end
