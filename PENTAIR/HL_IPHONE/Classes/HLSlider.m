//
//  HLSlider.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/4/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HLSlider.h"
#import "hlcomm.h"
#import "hlm.h"
#import "AppDelegate.h"
#import "shader.h"
#import "hlbutton.h"
#import "crect.h"

@implementation CHLSlider

#define SLIDER_SIZE      32

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Vertical:(BOOL)bVeritcal;

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.contentMode = UIViewContentModeRedraw;
        m_iSHOUT = [CHLComm GetInt:INT_BUTTON_SHADEOUT];
        m_iSHIN  = [CHLComm GetInt:INT_BUTTON_SHADEIN];

        if (m_iSHOUT <= 0)
        {
            m_iSHOUT = 2;
        }

        m_bVertical = bVeritcal;

        [self InitMarker];
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame;

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.contentMode = UIViewContentModeRedraw;
        m_iSHOUT = [CHLComm GetInt:INT_BUTTON_SHADEOUT];
        m_iSHIN  = [CHLComm GetInt:INT_BUTTON_SHADEIN];

        if (m_iSHOUT <= 0)
        {
            m_iSHOUT = 2;
        }

        [self InitMarker];

    }


    return self;
}
/*============================================================================*/

-(void)InitMarker;

/*============================================================================*/
{
    m_pMarker = [[CHLButton alloc] initWithFrame:CGRectZero Text:NULL];
    [self addSubview:m_pMarker];
    [m_pMarker setUserInteractionEnabled:FALSE];
    [m_pMarker SetShadeOut:0];
    [m_pMarker release];
}
/*============================================================================*/

-(void)SetPositionPCT:(int)iPCT

/*============================================================================*/
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [m_pMarker setFrame:[self MarkerPosition:iPCT]];
    [UIView commitAnimations];
    m_iPos = iPCT;
}
/*============================================================================*/

-(int)GetPositionPCT

/*============================================================================*/
{
    return m_iPos;
}
/*============================================================================*/

-(BOOL)HasCapture

/*============================================================================*/
{
    return m_bCapture;
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    int iRad = [CHLComm GetInt:INT_BUTTON_ROUT];
    CGImageRef pRef = [CShader CreateRecessedImageDX:iDX DY:iDY Rad:iRad SHOUT:m_iSHOUT];
    [layer setContents:(id)pRef];
    CGImageRelease(pRef);
    
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rMarker = [self MarkerPosition:m_iPos];
    [m_pMarker setFrame:rMarker];
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    CGRect rMarker = [self MarkerPosition:m_iPos];

    NSArray *pTouches = [touches allObjects];
    for (int i = 0; i < [pTouches count]; i++)
    {
        UITouch *pTouch = [pTouches objectAtIndex:i];
        if (!m_bCapture)
        {
            // First Touch
            m_ptTouchDown = [pTouch locationInView:self];
            if (CGRectContainsPoint(rMarker, m_ptTouchDown))
            {
                m_bCapture = TRUE;
                m_iPosStart = m_iPos;
                [m_pMarker SetChecked:YES];
            }

            printf("Touches Begin 1 Down\n");
        }
    }
}
/*============================================================================*/

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    if (!m_bCapture)
        return;

    NSArray *pTouches = [touches allObjects];
    if ([touches count] < 1)
        return;
        
    UITouch *pTouch = [pTouches objectAtIndex:0];
    CGPoint ptThis = [pTouch locationInView:self];
    

    CGRect rThis = self.bounds;
    [CRect Inset:&rThis DX:m_iSHOUT DY:m_iSHOUT];

    int iDXBtnMax = 50;
    int iDYBtnMax = 50;
    
    int iDX = ptThis.x - m_ptTouchDown.x;
    int iDY = ptThis.y - m_ptTouchDown.y;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iDXBtnMax = 110;
        iDYBtnMax = 80;
    }

    int iDXBtn = rThis.size.width;
    int iDYBtn = rThis.size.height;

    int iPosThis = m_iPos;

    if (m_bVertical)
    {
        iDYBtn = MIN(iDYBtn, rThis.size.height / 2);
        iDYBtn = MIN(iDYBtn, iDYBtnMax);
        int iRange = rThis.size.height - iDYBtn;
        iPosThis = m_iPosStart - (iDY * 100 / iRange);
    }
    else
    {
        iDXBtn = MIN(iDXBtn, rThis.size.width / 2);
        iDXBtn = MIN(iDXBtn, iDXBtnMax);
        int iRange = rThis.size.width - iDXBtn;
        iPosThis = m_iPosStart + (iDX * 100 / iRange);
    }

    int iNewPos = MIN(iPosThis, 100);
    iNewPos = MAX(0, iNewPos);

    if (iNewPos == m_iPos)
        return;

    [self SetPositionPCT:iNewPos];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}
/*============================================================================*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    [m_pMarker SetChecked:NO];
    m_bCapture = FALSE;
}
/*============================================================================*/

-(CGRect)MarkerPosition:(int)iPos

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    [CRect Inset:&rThis DX:m_iSHOUT DY:m_iSHOUT];


    int iDXBtnMax = 50;
    int iDYBtnMax = 50;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iDXBtnMax = 110;
        iDYBtnMax = 80;
    }

    int iDXBtn = rThis.size.width;
    int iDYBtn = rThis.size.height;
    int iXBtn = rThis.origin.x;
    int iYBtn = rThis.origin.y;

    if (m_bVertical)
    {
        iDYBtn = MIN(iDYBtn, rThis.size.height / 2);
        iDYBtn = MIN(iDYBtn, iDYBtnMax);
        int iRange = rThis.size.height - iDYBtn;
        iYBtn = m_iSHOUT + iRange - iPos * iRange / 100;
    }
    else
    {
        iDXBtn = MIN(iDXBtn, rThis.size.width / 2);
        iDXBtn = MIN(iDXBtn, iDXBtnMax);
        int iRange = rThis.size.width - iDXBtn;
        iXBtn = m_iSHOUT + iPos * iRange / 100;
    }

    return CGRectMake(iXBtn, iYBtn, iDXBtn, iDYBtn);
}
@end
