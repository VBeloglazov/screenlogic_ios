//
//  PoolConfig.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//
#include "hlstd.h"
#include "poolconfig.h"
#import "NSPoolConfig.h"
#import "NSPoolCircuit.h"
#import "SinkServer.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "HLComm.h"

#ifdef PENTAIR
    #import "hlm.h"
    #import "MainTab.h"
#endif

@implementation CNSPoolConfig
/*============================================================================*/

-(id)init

/*============================================================================*/
{
    DWORD dwMajor = [CHLComm VersionMajor];
    DWORD dwMinor = [CHLComm VersionMinor]; 

    m_pPoolConfig = new CPoolConfig(0, NULL, dwMajor, dwMinor);
    m_pSinkServer = [[CSinkServer alloc] init];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [self EndPoll];
    delete m_pPoolConfig;
    [m_pSinkServer release];
    [super dealloc];
}
/*============================================================================*/

-(int)NCircuitsByInterface:(int)iInterface

/*============================================================================*/
{
    CHLObList List;
    m_pPoolConfig->LoadCircuitsByInterface(&List, iInterface);
    return List.GetCount();
}
/*============================================================================*/

-(void)LoadCircuitsByInterface:(NSMutableArray*)pList Interface:(int)iInterface

/*============================================================================*/
{
    CHLObList List;
    m_pPoolConfig->LoadCircuitsByInterface(&List, iInterface);
    while (!List.IsEmpty())
    {
        CPoolCircuit *pC = (CPoolCircuit*)List.RemoveHead();
        CNSPoolCircuit *pNSC = [[CNSPoolCircuit alloc] initWithCircuit:pC];
        [pList addObject:pNSC];
        [pNSC release];
    }
}
/*============================================================================*/

-(void)SetCircuitFirstByInterface:(NSMutableArray*)pList

/*============================================================================*/
{
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
    [self InitComm];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
    if ([m_pSinkServer NSinks] == 0)
        [self ReleaseComm];
}
/*============================================================================*/

-(void)NotifySinks

/*============================================================================*/
{
    [m_pSinkServer NotifySinks:0 Data:0];
}
/*============================================================================*/

-(NSString*)PoolText

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->PoolText();
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)SpaText

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->SpaText();
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)PumpName:(int)iPumpIndex

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->PumpName(iPumpIndex);
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)HeatModeText:(int)iHeatMode

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->HeatModeText(iHeatMode);
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)HeatStatusText:(int)iHeatMode

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->HeatStatusText(iHeatMode);
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)PHORPText

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->PHORPText();
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)EnableText:(BOOL)bEnabled

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->EnableText(bEnabled);
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)FreezeModeStatusText

/*============================================================================*/
{
    CHLString sText = m_pPoolConfig->FreezeModeStatusText();
    return [self Text:&sText];
}
/*============================================================================*/

-(NSString*)Text:(CHLString*)pText

/*============================================================================*/
{
    int iLen = pText->GetLength();
    char *pBuf = pText->GetBufferCHAR(iLen);
    NSString *pRet = [NSString stringWithUTF8String:pBuf];
    return pRet;
}
/*============================================================================*/

-(NSString*)FormatDegText:(int)iDeg

/*============================================================================*/
{
    NSString *pText = [NSString stringWithFormat:@"%d°", iDeg];
    return pText;
}
/*============================================================================*/

-(int)NIntelliflow

/*============================================================================*/
{
    int iNIFlow = 0;
    int i = 0;
    for (i = 0; i < 8; i++)
    {
        if (m_pPoolConfig->EquipPresent(POOL_IFLOWPRESENT0<<i))
            iNIFlow++;
    }
    return iNIFlow;
}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    if (m_bCommInit)
    {
        return;
    }
    
    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_POOL_ADDCLIENTQ];
    [pQ autorelease];
    [pQ PutInt:0];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];
    [self PostGetState];
    
    m_bCommInit = TRUE;
}
/*============================================================================*/

-(void)ReleaseComm

/*============================================================================*/
{
    if (!m_bCommInit)
        return;

    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_POOL_REMOVECLIENTQ];
    [pQ autorelease];
    [pQ PutInt:0];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];

    [CHLComm RemoveSocketSink:self];
    
    m_bCommInit = FALSE;
}
/*============================================================================*/

-(void)BeginPoll

/*============================================================================*/
{
    if (m_pPollTimer == NULL)
        m_pPollTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(PollEvent) userInfo:nil repeats:YES] retain];
}
/*============================================================================*/

-(void)EndPoll

/*============================================================================*/
{
    [m_pPollTimer invalidate];
    [m_pPollTimer release];
    m_pPollTimer = NULL;
}
/*============================================================================*/

-(void)PollEvent

/*=======================================                                                                                                                                                                                                             =====================================*/
{
    if (![CHLComm Connected])
        return;
    [self PostGetState];
}
/*============================================================================*/

-(void)PostGetState

/*============================================================================*/
{
    CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETSTATUSQ] autorelease];
    [pQ1 PutInt:0];
    [pQ1 SendMessageWithMyID:self];

    CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETCHEMDATAQ] autorelease];
    [pQ2 PutInt:0];
    [pQ2 SendMessageWithMyID:self];
    
    //CNSQuery *pQ3 = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETSCGCONFIGQ] autorelease];
    //[pQ3 PutInt:0];
    //[pQ3 SendMessageWithID:0];
}
/*============================================================================*/

-(BOOL)HasPool

/*============================================================================*/
{
    CPoolCircuit *pCircuit = m_pPoolConfig->CircuitByFunction(POOLCIRCUIT_POOL);
    return (pCircuit != NULL);
}
/*============================================================================*/

-(BOOL)HasSpa

/*============================================================================*/
{
    CPoolCircuit *pCircuit = m_pPoolConfig->CircuitByFunction(POOLCIRCUIT_SPA);
    return (pCircuit != NULL);
}
/*============================================================================*/

-(BOOL)PoolIsOn

/*============================================================================*/
{
    CPoolCircuit *pCircuit = m_pPoolConfig->CircuitByFunction(POOLCIRCUIT_POOL);
    if (pCircuit == NULL)
        return FALSE;
    return (pCircuit->State() != 0);
}
/*============================================================================*/

-(BOOL)SpaIsOn

/*============================================================================*/
{
    CPoolCircuit *pCircuit = m_pPoolConfig->CircuitByFunction(POOLCIRCUIT_SPA);
    if (pCircuit == NULL)
        return FALSE;
    return (pCircuit->State() != 0);
}
/*============================================================================*/

-(int)AirTemp                       { return m_pPoolConfig->AirTemp();              }
-(int)WaterTemp:(int)iBodyType      { return m_pPoolConfig->CurrentTemp(iBodyType); }
-(int)HeatMode:(int)iBodyType       { return m_pPoolConfig->HeatMode(iBodyType);    }
-(int)HeatStatus:(int)iBodyType     { return m_pPoolConfig->HeatStatus(iBodyType);  }
-(int)HeatSetPoint:(int)iBodyType   { return m_pPoolConfig->SetPoint(iBodyType);    }
-(BOOL)RemotesEnabled               { return m_pPoolConfig->RemotesEnabled();       }

/*============================================================================*/

-(void)SetHeatMode:(int)iBodyType Mode:(int)iMode

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_POOL_SETHEATMODEQ] autorelease];
    [pQ PutInt:0];
    [pQ PutInt:iBodyType];
    [pQ PutInt:iMode];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)SetHeatSetPoint:(int)iBodyType Temp:(int)iTemp

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_POOL_SETHEATSPQ] autorelease];
    [pQ PutInt:0];
    [pQ PutInt:iBodyType];
    [pQ PutInt:iTemp];
    [pQ SendMessageWithMyID:self];
    m_pPoolConfig->SetPoint(iBodyType, iTemp);
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (!bNowConnected)
    {
        [self ReleaseComm];
    }
    else 
    {
        if ([m_pSinkServer NSinks] > 0)
            [self InitComm];
    }

}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_POOL_STATUSCHANGED:
    case HLM_POOL_GETSTATUSA:
        {
            m_pPoolConfig->ProcessMSG([pMSG Message]);
            [m_pSinkServer NotifySinks:0 Data:0];
        }
        break;

    case HLM_POOL_CHEMDATACHANGED:
    case HLM_POOL_GETCHEMDATAA:
    case HLM_POOL_GETSCGCONFIGA:
        {
            m_pPoolConfig->ProcessChemMSG([pMSG Message]);
            [m_pSinkServer NotifySinks:0 Data:0];
        }
        break;
    }
}
/*============================================================================*/

-(void)SetCircuitOnOff:(CNSPoolCircuit*)pCircuit On:(BOOL)bOn

/*============================================================================*/
{
    CPoolCircuit *pC = m_pPoolConfig->CircuitByID([pCircuit ID]);
    if (pC == NULL)
        return;
    m_pPoolConfig->SetCircuitOnOff(pC, bOn);
}
/*============================================================================*/

-(int)MaxSetPoint:(int)iBodyType

/*============================================================================*/
{
    return m_pPoolConfig->MaxSetPoint(iBodyType);
}
/*============================================================================*/

-(int)MinSetPoint:(int)iBodyType

/*============================================================================*/
{
    return m_pPoolConfig->MinSetPoint(iBodyType);
}
/*============================================================================*/

-(BOOL)EquipPresent:(unsigned int)iMask

/*============================================================================*/
{
    if (m_pPoolConfig->EquipPresent(iMask))
        return TRUE;
    return FALSE;
}
/*============================================================================*/

-(int)PH

/*============================================================================*/
{
    return m_pPoolConfig->GetPH();
}
/*============================================================================*/

-(int)ORP

/*============================================================================*/
{
    return m_pPoolConfig->GetORP();
}
/*============================================================================*/

-(int)Saturation

/*============================================================================*/
{
    return m_pPoolConfig->GetSaturation();
}
/*============================================================================*/

-(int)Salt

/*============================================================================*/
{
    return m_pPoolConfig->GetSaltPPM();
}
/*============================================================================*/

-(int)PHTankLevel

/*============================================================================*/
{
    return m_pPoolConfig->GetPHTankLevel();
}
/*============================================================================*/

-(int)ORPTankLevel

/*============================================================================*/
{
    return m_pPoolConfig->GetORPTankLevel();
}
#ifdef PENTAIR
/*============================================================================*/

-(void)AllocMainTabs:(NSMutableArray*)pArray

/*============================================================================*/
{
    NSMutableArray *pLights = [[[NSMutableArray alloc] init] autorelease];

    [self AllocMainTab:SUBTAB_PENTAIR_TEMP_POOL                     Array:pArray];
    [self AllocMainTab:SUBTAB_PENTAIR_TEMP_SPA                      Array:pArray];

    [self AllocMainTab:SUBTAB_PENTAIR_LIGHTS_ALL                    Array:pLights];
    [self AllocMainTab:SUBTAB_PENTAIR_LIGHTS_COLORS                 Array:pLights];
    [self AllocMainTab:SUBTAB_PENTAIR_LIGHTS_INTELLIBRITE           Array:pLights];
    [self AllocMainTab:SUBTAB_PENTAIR_LIGHTS_MAGICSTREAM            Array:pLights];

    if ([pLights count] > 0)
    {
        // 
        // Merge Lighting
        //
        
        CMainTab *pTab = [[CMainTab alloc] initWithName:@"Pool Lighting" TabID:SUBTAB_PENTAIR_LIGHTS_ALL];
        //DL - how to set MainViewController fro pTab?
        [pArray addObject:pTab];

        for (int iTab = 0; iTab < [pLights count]; iTab++)
        {
            CMainTab *pMain = (CMainTab*)[pLights objectAtIndex:iTab];
            CSubTab *pSub = [pMain SubTab:0];
            [pTab AddSubTab:pSub];
        }


        [pTab release];

        
    }

    [self AllocMainTab:SUBTAB_PENTAIR_FEATURES              Array:pArray];
    
    //DL check if Chlorinator only present
    if (m_pPoolConfig->EquipPresent(POOL_ICHEMPRESENT))//CircuitByDeviceID(POOL_ICHEMPRESENT))
    {
        [self AllocMainTab:SUBTAB_PENTAIR_PH_ORP            Array:pArray];
    }
    else if(m_pPoolConfig->EquipPresent(POOL_CHLORPRESENT))//CircuitByDeviceID(POOL_CHLORPRESENT))
    {
        [self AllocMainTab:SUBTAB_PENTAIR_CHLORINATOR       Array:pArray];
    }
    
    //leave it old way so far
    //if(m_pPoolConfig->EquipPresent(POOL_ICHEMPRESENT))
    //{
    //   [self AllocMainTab:SUBTAB_PENTAIR_PH_ORP            Array:pArray];
    //}
    
    //if(m_pPoolConfig->EquipPresent(POOL_CHLORPRESENT))
    //{
    //    [self AllocMainTab:SUBTAB_PENTAIR_CHLORINATOR       Array:pArray];
    //}
    
    [self AllocMainTab:SUBTAB_PENTAIR_HISTORY_TEMPS         Array:pArray];


}
/*============================================================================*/

-(void)AllocMainTab:(int)iID Array:(NSMutableArray*)pArray

/*============================================================================*/
{
    int iNPoolCircuits = [self NCircuitsByInterface:POOLINT_POOL];
    int iNSpaCircuits = [self NCircuitsByInterface:POOLINT_SPA];
    int iNGenCircuits = [self NCircuitsByInterface:POOLINT_GENERAL];

    NSString *pName = NULL;

    switch(iID)
    {
    case SUBTAB_PENTAIR_TEMP_POOL:
        {
            if (iNPoolCircuits <= 0)
                return;
            pName = [self PoolText];
        }
        break;
        
    case SUBTAB_PENTAIR_TEMP_SPA:
        {
            if (iNSpaCircuits <= 0)
                return;
            pName = [self SpaText];
        }
        break;

    case SUBTAB_PENTAIR_FEATURES:
        {
            if (iNGenCircuits <= 0)
                return;
            pName = @"Features";
        }
        break;
        
    case SUBTAB_PENTAIR_LIGHTS_ALL:
        {
            pName = @"Turn Lights On/Off";
        }
        break;

    case SUBTAB_PENTAIR_LIGHTS_COLORS:
        {
            if (m_pPoolConfig->EquipPresent(POOL_IBRITEPRESENT))
                return;
            pName = @"Color Light Control";
        }
        break;

    case SUBTAB_PENTAIR_LIGHTS_INTELLIBRITE:
        {
            if (!m_pPoolConfig->EquipPresent(POOL_IBRITEPRESENT))
                return;
            pName = @"Color Light Control";
        }
        break;

    case SUBTAB_PENTAIR_LIGHTS_MAGICSTREAM:
        {
            if (!m_pPoolConfig->EquipPresent(POOL_MAGICSTREAMPRESENT))
                return;
            pName = @"MagicStream Control";
        }
        break;

    case SUBTAB_PENTAIR_PH_ORP:
        {
            if (!m_pPoolConfig->EquipPresent(POOL_ICHEMPRESENT))
                return;
            pName = @"IntelliChem";
        }
        break;
            
    case SUBTAB_PENTAIR_CHLORINATOR:
        {
            if(!m_pPoolConfig->EquipPresent(POOL_CHLORPRESENT))
                return;
            pName = @"IntelliChlor";
        }
        break;
        
    case SUBTAB_PENTAIR_HISTORY_TEMPS:
        {
            pName = @"History";
        }
        break;
    }

    CMainTab *pTab = [[CMainTab alloc] initWithName:pName TabID:iID];
    //DL - how to set MainViewController fro pTab?
    [pTab AddSubTab:pName ID:iID Data1:0 Data2:0];
    [pArray addObject:pTab];
    [pTab release];
}
#endif

@end
