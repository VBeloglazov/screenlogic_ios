//
//  ImageServer.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

@class CIcon;

/*============================================================================*/

@interface CImageServer : NSObject 

/*============================================================================*/
{
}

+(void)Init;
+(void)Dealloc;
+(CIcon*)IconByName:(NSString*)pName;

@end
