//
//  PhotoGridView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PhotoGridView.h"
#import "PhotoAlbum.h"
#import "PhotoView.h"
#import "Photo.h"
#import "CustomPage.h"
#import "crect.h"

#define DX_THUMB                    200
#define DY_THUMB                    150
#define MAX_PACKETS_IN_FLIGHT       3

@implementation CPhotoGridView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MainPage:(CPhotosPage*)pMainPage Album:(CPhotoAlbum*)pAlbum

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pPhotosPage = pMainPage;
        m_pScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pScrollView];
        [m_pScrollView release];
        
        [m_pScrollView setDelegate:self];

        m_pAlbum = pAlbum;
        m_pPictures = [[NSMutableArray alloc] init];
        
        for (int iPic = 0; iPic < [pAlbum NPhotos]; iPic++)
        {
            CPhoto *pPhoto = [pAlbum Photo:iPic];
            CPhotoView *pView = [[CPhotoView alloc] initWithFrame:CGRectZero GridView:self Photo:pPhoto];
            [m_pScrollView addSubview:pView];
            [m_pPictures addObject:pView];
            [pView addTarget:m_pPhotosPage action:@selector(GoFullFrame:) forControlEvents:UIControlEventTouchDown];

            [pView release];
        }
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pTimerIO invalidate];
    [m_pTimerIO release];
    [m_pPictures release];
    [super dealloc];
}
/*============================================================================*/

- (void)scrollViewDidScroll:(UIScrollView *)sender 

/*============================================================================*/
{
    [self StartTimer];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [self StartTimer];

    CGRect rThis = self.bounds;
    int iNPictures = MAX(1, [m_pPictures count]);
    int iDXThumb = DX_THUMB;
    int iDYThumb = DY_THUMB;
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iDXThumb = 100;
        iDYThumb = 75;
    }

    int iNCols = MIN([m_pPictures count], rThis.size.width / iDXThumb);
    int iMaxCols = rThis.size.width / iDXThumb;

    iDXThumb = rThis.size.width / iMaxCols;
    iDYThumb = iDXThumb * 3 / 4;
    
    int iNRows = MAX(1, iNPictures / iNCols);
    if (iNCols * iNRows < iNPictures)
        iNRows++;

    int iDYAll = iNRows * iDYThumb;
    if (iDYAll < rThis.size.height)
    {
        [CRect CenterDownToY:&rThis DY:iDYAll];
    }

    [CRect CenterDownToX:&rThis DX:iNCols * iDXThumb];
    [m_pScrollView setFrame:rThis];

    CGRect rAll = CGRectMake(0, 0, iNCols * iDXThumb, iDYAll);

    int iRow = 0;
    int iCol = 0;

    [m_pScrollView setContentSize:CGSizeMake(rThis.size.width, iDYAll)];
    for (int i = 0; i < [m_pPictures count]; i++)
    {
        CPhotoView *pView = (CPhotoView*)[m_pPictures objectAtIndex:i];
        CGRect rRow = [CRect CreateYSection:rAll Index:iRow Of:iNRows];
        CGRect rPic = [CRect CreateXSection:rRow Index:iCol Of:iNCols];
        [CRect Inset:&rPic DX:4 DY:4];
        [pView setFrame:rPic];
        iCol++;
        if (iCol >= iNCols)
        {
            iCol = 0;
            iRow++;
        }
    }
    

}
/*============================================================================*/

-(void)TimerProc

/*============================================================================*/
{
    int iOffset = m_pScrollView.contentOffset.y;
    int iDYView = m_pScrollView.frame.size.height;
    

    BOOL bBusy = FALSE;
    for (int i = 0; i < [m_pPictures count]; i++)
    {
        CPhotoView *pView = (CPhotoView*)[m_pPictures objectAtIndex:i];
        CGRect rPicFrame = pView.frame;
        int iYTop = rPicFrame.origin.y - iOffset;
        int iYBtm = rPicFrame.size.height + iYTop;
        
        BOOL bVisible = TRUE;
        if (iYBtm < 0)
            bVisible = FALSE;
        if (iYTop > iDYView)
            bVisible = FALSE;
            
        if (bVisible)
        {
            if (m_iNPacketsInFlight < MAX_PACKETS_IN_FLIGHT)
            {
                if ([pView OnIdleVisible])
                    bBusy = TRUE;
            }
            else
            {
                bBusy = TRUE;
            }
        }
        else
        {
            if (iYBtm < iDYView / 2 || iYTop > (iDYView  * 3 / 2))
                [pView ReleaseImage];
        }
    }

    if (!bBusy)
    {
        // No more to process kill the timer
        [m_pTimerIO invalidate];
        [m_pTimerIO release];
        m_pTimerIO = NULL;
    }

}
/*============================================================================*/

-(void)StartTimer;

/*============================================================================*/
{
    if (m_pTimerIO != NULL)
        return;

    m_pTimerIO = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(TimerProc) userInfo:nil repeats:YES] retain];
    printf("Timer Start\n");
}
/*============================================================================*/

-(void)IncPacketsInFlight

/*============================================================================*/
{
    m_iNPacketsInFlight++;
}
/*============================================================================*/

-(void)DecPacketsInFlight;

/*============================================================================*/
{
    m_iNPacketsInFlight--;
}



@end
