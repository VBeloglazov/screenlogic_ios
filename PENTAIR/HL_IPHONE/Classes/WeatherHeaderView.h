//
//  WeatherHeaderView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/18/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "NSSocketSink.h"

@class CForecast;

/*============================================================================*/

@interface CWeatherTextHeaderView : UIView < CSink >

/*============================================================================*/
{
    CForecast                           *m_pForecast;

    UILabel                             *m_pTemp;
    UILabel                             *m_pDesc;
}

-(id)initWithFrame:(CGRect)frame Forecast:(CForecast*)pForecast;
-(UILabel*)AddLabel:(NSString*)pText Size:(int)iSize;

@end

/*============================================================================*/

@interface CWeatherIconHeaderView : UIView < CSink, CNSSocketSink >

/*============================================================================*/
{
    CForecast                           *m_pForecast;
    void                                *m_pData;
    CGImageRef                          m_pImage;
}

-(id)initWithFrame:(CGRect)frame Forecast:(CForecast*)pForecast;
-(BOOL)HasImage;

@end


/*============================================================================*/

@interface CWeatherHeaderView : UIView

/*============================================================================*/
{
    CForecast                           *m_pForecast;
    CWeatherTextHeaderView              *m_pText;
    CWeatherIconHeaderView              *m_pIcon;
}

-(id)initWithFrame:(CGRect)frame Forecast:(CForecast*)pForecast;

@end
