/*
--------------------------------------------------------------------------------

    RelayServerDef.h

    Copyright 2011 The AVC Group

--------------------------------------------------------------------------------
*/
#pragma once

#include "hlstd.h"
#include "HLObList.h"
#include "hltime.h"

/*============================================================================*/

    class CRelayServerDef

/*============================================================================*/
{
    CHLString                   m_sAddress;
    WORD                        m_wGatewayPort;
    WORD                        m_wClientPort;
    BOOL                        m_bActiveFlag;
    int                         m_iActiveConnectionCount;
    CHLTime                     m_dtLastUpdated;

public:
    CHLString                   Address()               { return m_sAddress;                }
    WORD                        GatewayPort()           { return m_wGatewayPort;            }
    WORD                        ClientPort()            { return m_wClientPort;             }
    BOOL                        ActiveFlag()            { return m_bActiveFlag;             }
    int                         ActiveConnectionCount() { return m_iActiveConnectionCount;  }
    CHLTime                     LastUpdated()           { return m_dtLastUpdated;           }

public:
    CRelayServerDef(
    CHLString                   sAddress,
    WORD                        wGatewayPort,
    WORD                        wClientPort,
    BOOL                        bActiveFlag,
    int                         iActiveConnectionCount,
    CHLTime                     dtLastUpdated )
    {
        m_sAddress                  = sAddress;
        m_wGatewayPort              = wGatewayPort;
        m_wClientPort               = wClientPort;
        m_bActiveFlag               = bActiveFlag;
        m_iActiveConnectionCount    = iActiveConnectionCount;
        m_dtLastUpdated             = dtLastUpdated;

    } // CRelayServerDef()

}; // class CRelayServerDef
