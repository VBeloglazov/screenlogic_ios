//
//  MainView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/20/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CMainView : UIView
{

}

+(int)DEFAULT_TEXT_SIZE;
+(int)DY_TOOLBAR;
+(int)DY_TOOLBAR_LARGE;
+(int)DY_TOOLBAR_MAIN;
+(int)DY_TOOLBAR_SUB;
+(int)DY_SUBTABCELL;
+(int)DY_MAX_MAINTABCELL;
+(int)G_BUTTON_DX;
+(int)DX_MEDIA_CONTROLS;
+(int)DY_MEDIA_CONTROLS;
+(int)DY_MEDIA_CONTROL_SPACING;
+(int)MAINTABCELL_INSET_X;
+(int)DY_TIMEBAR;
+(int)TEXT_SIZE_TITLE;
+(int)TEXT_SIZE_SIDEBAR;
+(int)DY_TOOLBAR_TEXT;
+(int)DX_SIDEBAR;

@end
