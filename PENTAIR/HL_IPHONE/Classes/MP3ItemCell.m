//
//  MP3ItemCell.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MP3ItemCell.h"
#import "iconview.h"
#import "hlm.h"
#import "crect.h"
#import "hlbutton.h"
#import "MainView.h"
#import "MP3Menu.h"
#import "CustomPage.h"

#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

#define CONTENTS_INSET                  10

@implementation CMP3ItemCell

@synthesize m_pLabel;

/*============================================================================*/

- (id)initWithFrame:(CGRect)aRect MP3Menu:(CMP3Menu*)pMenu reuseIdentifier:(NSString *)identifier Options:(int)iOptions PlayOptions:(int)iPlayOptions

/*============================================================================*/
{
	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

	if (self)
	{
		// you can do this here specifically or at the table level for all cells
		self.accessoryType = UITableViewCellAccessoryNone;

        // Create label views to contain the various pieces of text that make up the cell.
        // Add these as subviews.
        m_iOptions = iOptions;
        m_iPlayOptions = iPlayOptions;
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
        [CCustomPage InitStandardLabel:m_pLabel Size:18];
        m_pLabel.textAlignment = UITextAlignmentLeft;
        m_pLabel.highlightedTextColor = [UIColor whiteColor];
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        m_pLabel.numberOfLines = 3;

        if (m_iPlayOptions & ITEM_TYPE_PLAYABLE)
        {
            m_pPlay = [[CHLButton alloc] initWithFrame:CGRectZero Text:NULL];
            [m_pPlay SetIconEmbedded:@"LIST_PLAY.png"];
            [m_pPlay SetColor:[UIColor colorWithRed:0.0 green:0.5 blue:0.0 alpha:1.0]];
            [m_pPlay setHidden:YES];
            [self.contentView addSubview:m_pPlay];
            [m_pPlay release];
            [m_pPlay SetCommandID:HLID_NONE ReleaseID:CMD_MP3_PLAY Responder:pMenu];
        }
        if (m_iPlayOptions & ITEM_TYPE_ADDABLE)
        {
            m_pAdd = [[CHLButton alloc] initWithFrame:CGRectZero Text:NULL];
            [m_pAdd SetIconEmbedded:@"LIST_PLUS.png"];
            [m_pAdd SetColor:[UIColor blueColor]];
            [m_pAdd setHidden:YES];
            [self.contentView addSubview:m_pAdd];
            [m_pAdd release];
            [m_pAdd SetCommandID:HLID_NONE ReleaseID:CMD_MP3_ADD Responder:pMenu];
        }

        [self.contentView addSubview:m_pLabel];

        if ((m_iOptions & MP3ITEMCELL_NOACCESSORY) == 0)
        {
            CGRect frameIcon = CGRectMake(0, 0, 32, 32);
            m_pAccesory = [[CAccesoryView alloc] initWithFrame:frameIcon];	// layoutSubViews will decide the final frame
            m_pAccesory.backgroundColor = [UIColor clearColor];
        }

        [self.contentView addSubview:m_pAccesory];
    }
	
	return self;
}
/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated

/*============================================================================*/
{
    [super setSelected:selected animated:animated];
    [self layoutSubviews];
    [m_pAdd setHidden:!selected];
    [m_pPlay setHidden:!selected];
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    
    int iDY = contentRect.size.height;

    [CRect Inset:&contentRect DX:CONTENTS_INSET DY:2];

    if (m_pImageView != NULL)
    {
        CGRect rImage = [CRect BreakOffLeft:&contentRect DX:contentRect.size.height];
        [CRect Inset:&rImage DX:2 DY:2];
        [CRect BreakOffLeft:&contentRect DX:10];
        [m_pImageView setFrame:rImage];
    }
    
    // In this example we will never be editing, but this illustrates the appropriate pattern
    if (m_pUserIconView != NULL)
    {
        int iDXIcon = MIN(iDY, 60);
        CGRect frameIcon = [CRect BreakOffLeft:&contentRect DX:iDXIcon];
        m_pUserIconView.frame = frameIcon;
        [CRect BreakOffLeft:&contentRect DX:10];
    }

    if ((m_iOptions & MP3ITEMCELL_NOACCESSORY) == 0)
    {
        [m_pAccesory setFrame:[CRect BreakOffRight:&contentRect DX:10]];
        [CRect BreakOffRight:&contentRect DX:2];
    }

    int iDXBtn = 50;
    int iSpacing = 1;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iDXBtn = 50;
        iSpacing = 1;
    }
    
    CGRect rContent1 = contentRect;
    CGRect *pRect = &contentRect;
    if (![self isSelected])
    {
        pRect = &rContent1;
    }

    if (m_iPlayOptions & ITEM_TYPE_PLAYABLE)
    {
        [m_pPlay setFrame:[CRect BreakOffRight:pRect DX:iDXBtn]];
        [CRect BreakOffRight:&contentRect DX:iSpacing];
    }
    if (m_iPlayOptions & ITEM_TYPE_ADDABLE)
    {
        [m_pAdd setFrame:[CRect BreakOffRight:pRect DX:iDXBtn]];
        [CRect BreakOffRight:&contentRect DX:iSpacing];
    }
    
    if (m_pAltLabel != NULL)
    {
        CGRect rAlt = [CRect BreakOffBottom:&contentRect DY:10];
        [m_pAltLabel setFrame:rAlt];
    }

    [m_pLabel setFrame:contentRect];
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pUserIconView release];
	[m_pLabel release];
    [m_pAccesory release];
    [super dealloc];
}
/*============================================================================*/

- (void)SetAltText:(NSString*)pAltText

/*============================================================================*/
{
    if (pAltText == NULL)
    {
        if (m_pAltLabel != NULL)
        {
            [m_pAltLabel removeFromSuperview];
            m_pAltLabel = NULL;
            [self setNeedsLayout];
        }
    }
    else
    {
        if (m_pAltLabel == NULL)
        {
            m_pAltLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
            [CCustomPage InitStandardLabel:m_pAltLabel Size:8];
            m_pAltLabel.lineBreakMode = UILineBreakModeWordWrap;
            m_pAltLabel.numberOfLines = 3;
            [self.contentView addSubview:m_pAltLabel];
            [self setNeedsLayout];
        }

        [m_pAltLabel setText:pAltText];
    }
}
/*============================================================================*/

-(void)SetImage:(CGImageRef)pImage

/*============================================================================*/
{
    if (pImage == NULL)
    {
        [m_pImageView removeFromSuperview];
        m_pImageView = NULL;
    }
    else
    {
        if (m_pImageView == NULL)
        {
            m_pImageView = [[UIView alloc] initWithFrame:CGRectZero];
            [self addSubview:m_pImageView];
            [m_pImageView release];
        }
        [[m_pImageView layer] setContents:(id)pImage];
    }
}
@end
