//
//  PlaylistItemCell.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/2/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PlaylistItemCell.h"

@implementation CPlaylistItemCell

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier 

/*============================================================================*/
{
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) 
    {
        // Initialization code
		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

		CGFloat colors1[] =
		{
			  0.0, 0.0, 0.5,     0.5,
			  0.0, 0.0, 0.3,     0.5,
		};

		CGFloat colors2[] =
		{
			  0.3, 0.3, 1.0,     0.75,
			  0.2, 0.2, 0.5,     0.75,
		};

		m_Gradient1 = CGGradientCreateWithColorComponents(rgb, colors1, NULL, sizeof(colors1)/(sizeof(colors1[0])*4));
		m_Gradient2 = CGGradientCreateWithColorComponents(rgb, colors2, NULL, sizeof(colors2)/(sizeof(colors2[0])*4));
		CGColorSpaceRelease(rgb);
        m_iPosition = -1;
    }
    return self;
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    if (m_iPosition < 0)
        return;
  
    // Drawing code
    int iX = rect.size.width * m_iPosition / 100 + rect.origin.x;
    CGRect rL = rect;
    CGRect rR = rect;
    rL.size.width = iX;
    rR.size.width = rect.size.width - iX;
    rR.origin.x = iX;

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint ptStart = CGPointMake(rect.origin.y, rect.origin.y);
    CGPoint ptEnd = ptStart;
    ptEnd.y += rect.size.height;

    CGContextSaveGState(context);
	CGContextClipToRect(context, rR);
    CGContextDrawLinearGradient(context, m_Gradient1, ptStart, ptEnd, 0);
    CGContextRestoreGState(context);
	CGContextClipToRect(context, rL);
    CGContextDrawLinearGradient(context, m_Gradient2, ptStart, ptEnd, 0);
}
/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated 

/*============================================================================*/
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    CGGradientRelease(m_Gradient1);
    CGGradientRelease(m_Gradient2);
    [super dealloc];
}
/*============================================================================*/

-(void)SetPosition:(int)iNew

/*============================================================================*/
{
    if (iNew == m_iPosition)
        return;
    m_iPosition = iNew;
    [self setNeedsDisplay];
}


@end
