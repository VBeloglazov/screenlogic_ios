/*
--------------------------------------------------------------------------------

    HELPERS.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HELPERS_H
#define HELPERS_H

#include "math.h"
#include "speed.h"
#include "hlapi.h"

    //
    // Useful macros: returns same type, whereas inlines can't.
    //
    #define SGN0(x) ((x)>0 ? (1) : ((x)<0 ? (-1) : (0)))
    #define SGN(x) (((x) > 0) ? (1) : (-1))
#ifndef MAX
    #define MAX(x, y)           (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
    #define MIN(x, y)           (((x) < (y)) ? (x) : (y))
#endif

    #define SAME_BOOL(b1,b2) ((!b1 && !b2) || (b1 && b2))

    //
    // Quadrants: used for arcs.
    //
    #define QUADRANT_NONE       0
    #define QUADRANT_NE         1
    #define QUADRANT_NW         2
    #define QUADRANT_SW         3
    #define QUADRANT_SE         4

    //
    // Constants
    //
    const double        g_dPI               = 3.14159265358979333;
    const double        g_dTWOPI            = 2 * g_dPI;
    const double        g_dPI_TWO           = g_dPI / 2.0;
    const double        g_dPI_FOUR          = g_dPI / 4.0;
    const double        g_dPI_EIGHT         = g_dPI / 8.0;
    const double        g_dPI_SIXTEEN       = g_dPI / 16.0;
    const double        g_dPI_RADS_DEG      = g_dPI / 180;



/*============================================================================*/

    inline int                  WORDAlign(
    int                         iN )

/*============================================================================*/
{
    if ((iN % 2) == 0)
        return iN;
    return iN + 1;
}
/*============================================================================*/

    inline int                  DWORDAlign(
    int                         iN )

/*============================================================================*/
{
    int iR = iN % 4;
    if (iR == 0)
        return iN;
    return (iN + 4 - iR);
}
/*============================================================================*/

    inline int                  Align16(
    int                         iN )

/*============================================================================*/
{
    int iR = iN % 16;
    if (iR == 0)
        return iN;
    return (iN + 16 - iR);
}
/*============================================================================*/

    inline int                  Align32(
    int                         iN )

/*============================================================================*/
{
    int iNew = ((iN + 31)/ 32);
    return iNew * 32;
}
/*============================================================================*/

    inline int                  iSqrt(
    int                         iN )

/*
NOTES:  This function finds the square root of an integer using just integer
        operations.

        On CPUs with a coprocessor, this seems to save little or no time when
        compared with sqrt().

        On CPUs without a coprocessor (ARM), this is much faster.

        This code came from "C++ Math Class Library", Scott N. Gerard, p45.
*/
/*============================================================================*/
{
    #ifdef CRYSTALPAD
        if (iN >= 0 && iN < CACHE_SQRT && g_SQRT != NULL)
            return g_SQRT[iN];
    #endif

    int iR = iN;
    int iS = 0;
    int iT = 0x40000000;
    do
    {
        int iTemp = iS + iT;
        iS = iS >> 1;
        if (iR >= iTemp)
        {
            iR -= iTemp;
            iS += iT;
        }
        iT = iT >> 2;
    } while (iT != 0);

    return iS;
}
/*============================================================================*/

    inline int                  iSqrt16(
    int                         iN )

/*
NOTES:  This function finds the square root of an integer using just integer
        operations.

        On CPUs with a coprocessor, this seems to save little or no time when
        compared with sqrt().

        On CPUs without a coprocessor (ARM), this is much faster.

        This code came from "C++ Math Class Library", Scott N. Gerard, p45.
*/
/*============================================================================*/
{
    #ifdef CRYSTALPAD
        if (iN >= 0 && iN < CACHE_SQRT && g_SQRT != NULL)
            return g_SQRT16[iN];
    #endif

    int iR = iN * 256;
    int iS = 0;
    int iT = 0x40000000;
    do
    {
        int iTemp = iS + iT;
        iS = iS >> 1;
        if (iR >= iTemp)
        {
            iR -= iTemp;
            iS += iT;
        }
        iT = iT >> 2;
    } while (iT != 0);

    return iS;
}
/*============================================================================*/

    inline double               ToFahrenheit(
    double                      dCelsius )

/*============================================================================*/
{
    // The commonly known conversion is very accurate...
    return 32 + 1.8 * dCelsius;
}
/*============================================================================*/

    inline double               ToCelsius(
    double                      dFahrenheit )

/*============================================================================*/
{
    return (dFahrenheit - 32) / 1.8;
}
/*============================================================================*/

    inline int                  HLSgn(
    int                         iInt )

/*============================================================================*/
{
    if (iInt == 0)
        return 0;
    return (iInt > 0) ? 1 : -1;
}
/*============================================================================*/

    inline BOOL                 IsLetter(
    char                        cChar )

/*============================================================================*/
{
    if ((cChar >= 'A') && (cChar <= 'Z'))
        return TRUE;
    if ((cChar >= 'a') && (cChar <= 'z'))
        return TRUE;

    return FALSE;
}
/*============================================================================*/

    inline BOOL                 IsNumber(
    char                        cChar )

/*============================================================================*/
{
    if ((cChar >= '0') && (cChar <= '9'))
        return TRUE;

    return FALSE;
}
/*============================================================================*/

    inline BOOL                 IsLetterOrNumber(
    char                        cChar )

/*============================================================================*/
{
    if ((cChar >= 'A') && (cChar <= 'Z'))
        return TRUE;
    if ((cChar >= 'a') && (cChar <= 'z'))
        return TRUE;

    return FALSE;
}
/*============================================================================*/

    inline int                  HexCharToNumber(
    char                        cChar)

/*============================================================================*/
{
    if ((cChar >= 'A') && (cChar <= 'F'))
        return (cChar - 'A' + 10);

    else if ((cChar >= 'a') && (cChar <= 'f'))
        return (cChar - 'a' + 10);

    else if ((cChar >= '0') &&  (cChar <= '9'))
        return (cChar - '0');

    return 0;
}
/*============================================================================*/

    inline int                  HexCharsToNumber(
    char                        cChar1,
    char                        cChar2 )

/*============================================================================*/
{
    return (HexCharToNumber(cChar1) << 4) + HexCharToNumber(cChar2);
}
/*============================================================================*/

    inline int                  HexStringToInt(
    const char                  *szStr )

/*============================================================================*/
{
    if (szStr[1] == 'x')  // 0x1234
        szStr += 2;
    int iLen = (int)strlen(szStr);
    switch (iLen)
    {
    case 1:  return  HexCharToNumber(szStr[0]);
    case 2:  return (HexCharToNumber(szStr[0]) << 4)  +  HexCharToNumber(szStr[1]);
    case 3:  return (HexCharToNumber(szStr[0]) << 8)  + (HexCharToNumber(szStr[1]) << 4) + HexCharToNumber(szStr[2]);
    case 4:  return (HexCharToNumber(szStr[0]) << 12) + (HexCharToNumber(szStr[1]) << 8) + (HexCharToNumber(szStr[2]) << 4) + HexCharToNumber(szStr[3]);
    default:
        {
            ASSERT(0);
            return 0;
        }
    }
}
/*============================================================================*/

    inline DWORD                FOURCCToDWORD(
    char                        cChar1,
    char                        cChar2,
    char                        cChar3,
    char                        cChar4 )

/*============================================================================*/
{
    return (   ((DWORD)((BYTE)(cChar1))) +
             ( ((DWORD)((BYTE)(cChar2))) << 8 ) +
             ( ((DWORD)((BYTE)(cChar3))) << 16 ) +
             ( ((DWORD)((BYTE)(cChar4))) << 24 ) );
}
/*============================================================================*/

    inline DWORD                FOURCCToDWORD(
    char                        *pChar )

/*============================================================================*/
{
    return FOURCCToDWORD( *pChar, *(pChar+1), *(pChar+2), *(pChar+3) );
}
/*============================================================================*/

    inline void                 DWORDToFOURCC(
    DWORD                       dword,
    char                        *pChar )

// pChar is a pointer to a 4 char buffer
/*============================================================================*/
{
    pChar[0] = (char)(dword & 0xFF);
    dword >>= 8;
    pChar[1] = (char)(dword & 0xFF);
    dword >>= 8;
    pChar[2] = (char)(dword & 0xFF);
    dword >>= 8;
    pChar[3] = (char)(dword & 0xFF);
}
/*============================================================================*/

    inline double               ToRadians(
    double                      dDegrees )

/*============================================================================*/
{
    return dDegrees * g_dPI_RADS_DEG;
}
/*============================================================================*/

    inline double               ToDegrees(
    double                      dRadians )

/*============================================================================*/
{
    return dRadians / g_dPI_RADS_DEG;
}
/*============================================================================*/

    inline long                 Nearest(
    double                      dX )

/*============================================================================*/
{
    if (dX > 0)
        return (long) (dX + .5);
    else if (dX < 0)
        return (long) (dX - .5);
    else
        return 0;
}
/*============================================================================*/

    inline double               RADMod180(
    double                      dAngle )

/*============================================================================*/
{
    while (dAngle > g_dPI)
        dAngle -= g_dTWOPI;
    while (dAngle <= -g_dPI)
        dAngle += g_dTWOPI;

    return dAngle;
}
/*============================================================================*/

    inline double               RADMod360(
    double                      dAngle )

/*============================================================================*/
{
    while (dAngle > g_dTWOPI)
        dAngle -= g_dTWOPI;
    while (dAngle < 0)
        dAngle += g_dTWOPI;

    return dAngle;
}
/*============================================================================*/

    inline double               DEGMod180(
    double                      dAngle )

/*============================================================================*/
{
    while (dAngle > 180.0)
        dAngle -= 360.0;
    while (dAngle <= -180.0)
        dAngle += 360.0;

    return dAngle;
}
/*============================================================================*/

    inline double               DEGMod360(
    double                      dAngle )

/*============================================================================*/
{
    while (dAngle > 360.0)
        dAngle -= 360.0;
    while (dAngle < 0)
        dAngle += 360.0;

    return dAngle;
}
/*============================================================================*/

    inline double               DEGMod90(
    double                      dAngle )
/*
FUNCTION:   Reverses direction of angle vector if neccessary so vector
            always points in positive x-direction, fabs(dAngle) <= 90.0
*/
/*============================================================================*/
{
    while (dAngle > 90.0)
        dAngle -= 180.0;
    while (dAngle < -90.0)
        dAngle += 180.0;

    return dAngle;
}
/*============================================================================*/

    inline double               LengthDXDY(
    double                      dX,
    double                      dY )

/*============================================================================*/
{
    if ((dX == 0) && (dY == 0))
        return 0.0;
    else
        return sqrt(dX*dX + dY*dY);
}
/*============================================================================*/

    inline double               RADAngleDYDX(
    double                      dDY,
    double                      dDX )

/*============================================================================*/
{
    if ((dDX == 0) && (dDY == 0))
        return 0.0;

    return atan2(dDY, dDX);
}
/*============================================================================*/

    inline double               DEGAngleDYDX(
    double                      dDY,
    double                      dDX )

/*============================================================================*/
{
    return ToDegrees(RADAngleDYDX(dDY, dDX));
}
/*============================================================================*/

    inline int                  Quadrant(
    int                         iDX,
    int                         iDY,
    int                         iDT )

/*============================================================================*/
{
    BOOL bCCW = (iDT > 0);
    int iQuadrant = QUADRANT_NONE;
    if (iDX < 0)
    {
        if (iDY < 0)
            iQuadrant = bCCW ? QUADRANT_NE : QUADRANT_SW;
        else
            iQuadrant = bCCW ? QUADRANT_NW : QUADRANT_SE;
    }
    else
    {
        if (iDY > 0)
            iQuadrant = bCCW ? QUADRANT_SW : QUADRANT_NE;
        else
            iQuadrant = bCCW ? QUADRANT_SE : QUADRANT_NW;
    }

    return iQuadrant;
}
/*============================================================================*/

    inline double               QuadrantAngle(
    double                      dDX,
    double                      dDY )
/*
FUNCTION:   returns a value that sorts in the same order as the Mod360 angle
            defined by dDX,dDY, but is quicker to calculate.
            (returns value from 0.0 to 1.0 for angle in first quadrant,
                                1.0 to 2.0 for angle in second quadrant,
                                2.0 to 3.0 for angle in third quadrant,
                                3.0 to <4.0 for angle in fourth quadrant
*/
/*============================================================================*/
{
    if (fabs(dDX) < .000001)
        dDX = 0;
    if (fabs(dDY) < .000001)
        dDY = 0;

    double t;
    if ( (dDX == 0) && (dDY == 0) )
        t = 0.0;
    else
        t = dDY / ( fabs(dDX) + fabs(dDY) );

    if ( dDX < 0 )
        t = 2.0 - t;
    else if ( dDY < 0 )
        t = t + 4.0;

    return t;
}
/*============================================================================*/

    inline BOOL                 IsInRange(
    double                      d1,
    double                      d2,
    double                      d3 )

/*============================================================================*/
{
    if ((d1 < d2) && (d1 < d3))
        return FALSE;
    if ((d1 > d2) && (d1 > d3))
        return FALSE;

    return TRUE;
}
#ifndef MAC_OS
/*============================================================================*/

    inline int                  IntRand(
    int                         iMinInclusive,
    int                         iMaxInclusive )

/*============================================================================*/
{
    int iRand = rand();
    double dRand = (double)iRand / (double)RAND_MAX;
    double dNew = (double) (iMaxInclusive - iMinInclusive) * dRand;
    dNew += (iMinInclusive + 0.50);
    return (int)(dNew);
}
#endif
/*============================================================================*/

    inline WORD                 SwapWORD(WORD w)

/*============================================================================*/
{
    BYTE by1 = (w & 0xFF);
    BYTE by2 = (w & 0xFF00) >> 8;
    return (by1 << 8) | by2;
}
/*============================================================================*/

    inline DWORD                SwapDWORD(DWORD w)

/*============================================================================*/
{
    BYTE by1 = (BYTE)(w & 0x000000FF);
    BYTE by2 = (BYTE)((w & 0x0000FF00) >> 8);
    BYTE by3 = (BYTE)((w & 0x00FF0000) >> 16);
    BYTE by4 = (BYTE)((w & 0xFF000000) >> 24);

    return (by1 << 24) | (by2 << 16) | (by3 << 8) | by4;
}

/*============================================================================*/

    extern BOOL                 HLCheckDirectory(
    CHLString                   sRelDir,
    CHLString                   sTargetDir );

/*============================================================================*/

    HL_API void                 HLCleanFolder(
    CHLString                   sDir );

/*============================================================================*/

    extern void                 HLDeleteFiles(
    CHLString                   sPathMask,
    CHLString                   sMask  );

#ifdef GATEWAY
/*============================================================================*/

    HL_API void                 DeleteFolder(CHLString sDir);

/*============================================================================*/
#endif
/*============================================================================*/

    inline int                  axtoi(const char* sString)

// converts ascii hex string to int, similar to atoi()
/*============================================================================*/
{
    int iReturn = 0;

    int i=0;
#ifdef MAC_OS
    while ((sString[i] != 0) && (i < 10))
        i++;
#else
    while ((sString[i] != NULL) && (i < 10))
        i++;
#endif
    i--;

    int multx = 1;
    for ( ; i >= 0; i--)
    {
        int ic;
        if ((sString[i] >= '0') && (sString[i] <= '9'))
            ic = sString[i] - '0';
        else if ((sString[i] >= 'A') && (sString[i] <= 'F'))
            ic = sString[i] - 'A' + 10;
        else if ((sString[i] >= 'a') && (sString[i] <= 'f'))
            ic = sString[i] - 'a' + 10;
        else
            ic = 0;
        iReturn += (ic * multx);
        multx *= 16;
    }
    return iReturn;
}
/*============================================================================*/

    HL_API WORD                 HexToInt(
    char                        *pBuf,
    int                         iNChars );

/*============================================================================*/

    HL_API WORD                 HexToInt(
    wchar_t                     *pBuf,
    int                         iNChars );

/*============================================================================*/

    HL_API WORD                 DecToInt(
    char                        *pBuf,
    int                         iNChars );

/*============================================================================*/

    HL_API WORD                 DecToInt(
    wchar_t                     *pBuf,
    int                         iNChars );

/*============================================================================*/

    BOOL                        IsBit(
    BYTE                        Byte,
    BYTE                        iWhichBit );

/*============================================================================*/

    void                        SetBit(
    BYTE                        *pByte,
    BYTE                        iWhichBit,
    BOOL                        bTrue );

/*============================================================================*/

    BOOL                        ParseIP(
    char                        *pText,
    DWORD                       *pdwIP,
    WORD                        *pwPort );

#ifdef GATEWAY
/*============================================================================*/

    HL_API CHLString            RemoveNonAlpha(
    CHLString                   sIn );

/*============================================================================*/

    HL_API BOOL                 IsAlpha(
    char                        c );

/*============================================================================*/
#endif

#endif


