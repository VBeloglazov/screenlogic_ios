/*
--------------------------------------------------------------------------------

    MAIN.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifdef HLOSD
    #include "..\hlosd\hlosd.h"
    #include "..\hlosd\mainwnd.h"
#endif
#ifdef CRYSTALPAD
    #ifdef IPHONE
        // iPhone Includes Here
    #else
        #ifdef MAC_OS
            #include "../cocoapad/app.h"
            #include "../crystalpad/mainwnd.h"
        #else
            #include "..\crystalpad\crystalpad.h"
            #include "..\crystalpad\mainwnd.h"
        #endif
    #endif
#endif
#ifdef DDTEST
    #include "..\utils\ddtest\ddtest.h"
    #include "..\utils\ddtest\mainwnd.h"
#endif

#ifdef HLCONFIG
    #include "..\hlconfig\hlconfig.h"
    #include "..\hlconfig\mainwnd.h"
#endif
