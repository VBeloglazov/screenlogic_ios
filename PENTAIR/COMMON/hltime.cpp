/*
--------------------------------------------------------------------------------

    HLTIME.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "hlmsgs.h"
#include "helpers.h"
#include "stringlib.h"
#include "time.h"

#include "hltime.h"

    //
    // Local constants.
    //
    // FILETIME is a 64 bit number which stores the number of 100 nanosecond
    // cycles since Jan 1, 1601. This is also known as Universal Coordinated
    // Time (UTC).
    //                           123456789
    // 1 millisecond is         .001        seconds (10E-3)
    // 1 microsecond is         .000001     seconds (10E-6)
    // 1 nanosecond is          .000000001  seconds (10E-9)
    //                                 1
    // so: 100 nanoseconds is   .0000001    seconds
    //
    // In other words, there are 10,000 counts per millisecond, and 10,000,000
    // counts per second.
    //
    // Note: the max DWORD (0xFFFFFFFF) can reac 4,294,967,295 counts, or between
    // 429 and 430 seconds. We use __int64 to help with the math, which is
    // supported under both Win32 and CE compilers.
    //
    #define COUNT_MSEC          10000
    #define COUNT_SEC           1000 * COUNT_MSEC
    #define COUNT_MIN           60 * COUNT_SEC
    #define COUNT_HOUR          60 * COUNT_MIN
    #define COUNT_DAY           24 * COUNT_HOUR
    #define COUNT_WEEK          7 * COUNT_DAY

    int                         CHLTime::m_iGlobalOffset    = 0;

#ifndef MAC_OS
    __int64                     CHiResTimer::g_iHZ          = 0;
#endif

    CTimeServer                 *CTimeServer::g_pInstance = NULL;

struct TIME_ZONE_INFO
{
    ULONG Bias;
    ULONG StandardBias;
    ULONG DaylightBias;
    SYSTEMTIME StandardDate;
    SYSTEMTIME DaylightDate;
};


#ifdef STRINGLIB
/*============================================================================*/

    CHLString                   DayOfWeekText(
    int                         iDay )

/*============================================================================*/
{
    switch (iDay)
    {
    case HL_MONDAY:
        return CHLXString("Monday", "Day");
    case HL_TUESDAY:
        return CHLXString("Tuesday", "Day");
    case HL_WEDNESDAY:
        return CHLXString("Wednesday", "Day");
    case HL_THURSDAY:
        return CHLXString("Thursday", "Day");
    case HL_FRIDAY:
        return CHLXString("Friday", "Day");
    case HL_SATURDAY:
        return CHLXString("Saturday", "Day");
    case HL_SUNDAY:
        return CHLXString("Sunday", "Day");
    default:
        LOGASSERT(FALSE);
    }
    return CHLString("");
}
/*============================================================================*/

    CHLString                   DayOfWeekTextShort(
    int                         iDay )

/*============================================================================*/
{
    switch (iDay)
    {
    case HL_MONDAY:
        return CHLXString("Mon", "Day");
    case HL_TUESDAY:
        return CHLXString("Tue", "Day");
    case HL_WEDNESDAY:
        return CHLXString("Wed", "Day");
    case HL_THURSDAY:
        return CHLXString("Thu", "Day");
    case HL_FRIDAY:
        return CHLXString("Fri", "Day");
    case HL_SATURDAY:
        return CHLXString("Sat", "Day");
    case HL_SUNDAY:
        return CHLXString("Sun", "Day");
    default:
        LOGASSERT(FALSE);
    }
    return _T("???");
}
/*============================================================================*/

    CHLString                   MonthText(
    int                         iMonth )

/*============================================================================*/
{
    switch (iMonth)
    {
    case 1:
        return CHLXString("January", "Month");
    case 2:
        return CHLXString("February", "Month");
    case 3:
        return CHLXString("March", "Month");
    case 4:
        return CHLXString("April", "Month");
    case 5:
        return CHLXString("May", "Month");
    case 6:
        return CHLXString("June", "Month");
    case 7:
        return CHLXString("July", "Month");
    case 8:
        return CHLXString("August", "Month");
    case 9:
        return CHLXString("September", "Month");
    case 10:
        return CHLXString("October", "Month");
    case 11:
        return CHLXString("November", "Month");
    case 12:
        return CHLXString("December", "Month");
    default:
        LOGASSERT(FALSE);
    }
    return CHLXString("Unknown", "");
}
/*============================================================================*/

    CHLString                   MonthTextShort(
    int                         iMonth )

/*============================================================================*/
{
    switch (iMonth)
    {
    case 1:
        return CHLXString("Jan", "Month Abbrv");
    case 2:
        return CHLXString("Feb", "Month Abbrv");
    case 3:
        return CHLXString("Mar", "Month Abbrv");
    case 4:
        return CHLXString("Apr", "Month Abbrv");
    case 5:
        return CHLXString("May", "Month Abbrv");
    case 6:
        return CHLXString("Jun", "Month Abbrv");
    case 7:
        return CHLXString("Jul", "Month Abbrv");
    case 8:
        return CHLXString("Aug", "Month Abbrv");
    case 9:
        return CHLXString("Sep", "Month Abbrv");
    case 10:
        return CHLXString("Oct", "Month Abbrv");
    case 11:
        return CHLXString("Nov", "Month Abbrv");
    case 12:
        return CHLXString("Dec", "Month Abbrv");
    default:
        LOGASSERT(FALSE);
    }
    return _T("???");
}
#endif
/*============================================================================*/

    CHLString                   DayTimeText(
    int                         iMinutesAfterMidnight )

/*============================================================================*/
{
    int iHours = (int)(iMinutesAfterMidnight / 60);
    int iMin = iMinutesAfterMidnight - (iHours*60);

    CHLString sAMPM = _T(" AM");
    if (iHours >= 12)
    {
        iHours -= 12;
        sAMPM = _T(" PM");
    }

    if (iHours == 0)
        iHours = 12;

    CHLString sTime;
    sTime.Format(_T("%d:%02d %s"), iHours, iMin, (const TCHAR*)sAMPM);
    return sTime;
}
/*============================================================================*/

    CHLString                   DayTimeTextSmall(
    int                         iMinutesAfterMidnight )

/*============================================================================*/
{
    int iHours = (int)(iMinutesAfterMidnight / 60);
    int iMin = iMinutesAfterMidnight - (iHours*60);

    CHLString sAMPM = _T("a");
    if (iHours >= 12)
    {
        iHours -= 12;
        sAMPM = _T("p");
    }

    if (iHours == 0)
        iHours = 12;

    CHLString sTime;
    sTime.Format(_T("%d:%02d%s"), iHours, iMin, (const TCHAR*)sAMPM);
    return sTime;
}
#ifdef GATEWAY
/*============================================================================*/

    CTimeZoneInfo::CTimeZoneInfo(
    HKEY                        hkTimeZone )

/*============================================================================*/
{
    memset(&m_Info, 0, sizeof(TIME_ZONE_INFORMATION));
    memset(m_szDisplay, 0, 128 * sizeof(TCHAR));

    // Get TZI Upper Bytes
    DWORD dwDataSize = sizeof( TIME_ZONE_INFO );
    TIME_ZONE_INFO TZI;
    memset(&TZI, 0, sizeof(TIME_ZONE_INFO));
    RegQueryValueEx(hkTimeZone,_T("TZI"),NULL, NULL,(BYTE*)&TZI,&dwDataSize);

    m_Info.Bias = TZI.Bias ;
    m_Info.DaylightBias = TZI.DaylightBias ;
    m_Info.DaylightDate = TZI.DaylightDate ;
    m_Info.StandardBias = TZI.StandardBias ;
    m_Info.StandardDate = TZI.StandardDate ;
    
    // Get Text Values
    dwDataSize = 32 * sizeof( TCHAR );
    RegQueryValueEx(hkTimeZone,_T("Dlt"),NULL,NULL, (BYTE*)m_Info.DaylightName,&dwDataSize);
    dwDataSize = 32 * sizeof( TCHAR );
    RegQueryValueEx(hkTimeZone,_T("Std"),NULL,NULL, (BYTE*)m_Info.StandardName,&dwDataSize);
    dwDataSize = 128 * sizeof( TCHAR );
    RegQueryValueEx(hkTimeZone,_T("Display"),NULL,NULL, (BYTE*)m_szDisplay, &dwDataSize);
}
#endif
/*============================================================================*/

    CTimeServer::CTimeServer()

/*============================================================================*/
{
    //
    // This server will be used to handle time zone issues: for now just
    // leave this here.
    //
    // Note that for conversions from / to UTC, use this:
    //
    //      UTC = local time + bias
    //
    #ifdef MAC_OS
        m_dwZoneID  = 0;        // Zone ID: daylight or not
        m_bDSTAuto  = TRUE;     // True for DST auto adjust
    #else
        m_bDSTAuto  = FALSE;
        m_dwZoneID  = GetTimeZoneInformation(&m_TimeZone);
        m_TimeZone.DaylightDate.wYear       = 0;
        m_TimeZone.DaylightDate.wMonth      = 3;
        m_TimeZone.DaylightDate.wDayOfWeek  = 0;
        m_TimeZone.DaylightDate.wDay        = 2;

        m_TimeZone.StandardDate.wYear       = 0;
        m_TimeZone.StandardDate.wMonth      = 11;
        m_TimeZone.StandardDate.wDayOfWeek  = 0;
        m_TimeZone.StandardDate.wDay        = 1;
        SetTimeZoneInformation(&m_TimeZone);

        #ifndef UNDER_CE
            HKEY keyTZ;
            if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Control\\TimeZoneInformation"), 0, KEY_ALL_ACCESS, &keyTZ) == ERROR_SUCCESS)
            {
                BYTE byStdStart[16];   memset(byStdStart, 0, 16);
                BYTE byDayStart[16];   memset(byDayStart, 0, 16);
                byStdStart[2] = 0x0B;   // Nov
                byStdStart[4] = 0x01;
                byStdStart[6] = 0x02;
                byDayStart[2] = 0x03;   // March
                byDayStart[4] = 0x02;
                byDayStart[6] = 0x02;
                BOOL bWrite = FALSE;
                BYTE byRead[32];
                DWORD dwType = REG_BINARY;
                DWORD dwSize = 32;
                if (RegQueryValueEx(keyTZ, _T("DaylightStart"), NULL, &dwType, byRead, &dwSize) == ERROR_SUCCESS)
                {
                    for (int i = 0; i < 16 && !bWrite; i++)
                    {
                        if (byRead[i] != byDayStart[i])
                            bWrite = TRUE;
                    }
                }
                if (RegQueryValueEx(keyTZ, _T("StandardStart"), NULL, &dwType, byRead, &dwSize) == ERROR_SUCCESS)
                {
                    for (int i = 0; i < 16 && !bWrite; i++)
                    {
                        if (byRead[i] != byStdStart[i])
                            bWrite = TRUE;
                    }
                }

                if (bWrite)
                {
                    RegSetValue(keyTZ, _T("DaylightStart"), REG_BINARY, (LPTSTR)byDayStart, 16);
                    RegSetValue(keyTZ, _T("StandardStart"), REG_BINARY, (LPTSTR)byStdStart, 16);
                }

                RegCloseKey(keyTZ);
            }
        #else
            #ifdef GATEWAY
                FixCETimeZone(_T("Atlantic Standard Time"), "f0,00,00,00,00,00,00,00,c4,ff,ff,ff,00,00,0b,00,00,00,01,00,02,00,00,00,00,00,00,00,00,00,03,00,00,00,02,00,02,00,00,00,00,00,00,00" );
                FixCETimeZone(_T("Eastern Standard Time"),  "2c,01,00,00,00,00,00,00,c4,ff,ff,ff,00,00,0b,00,00,00,01,00,02,00,00,00,00,00,00,00,00,00,03,00,00,00,02,00,02,00,00,00,00,00,00,00" );
                FixCETimeZone(_T("Central Standard Time"),  "68,01,00,00,00,00,00,00,c4,ff,ff,ff,00,00,0b,00,00,00,01,00,02,00,00,00,00,00,00,00,00,00,03,00,00,00,02,00,02,00,00,00,00,00,00,00" );
                FixCETimeZone(_T("Mountain Standard Time"), "a4,01,00,00,00,00,00,00,c4,ff,ff,ff,00,00,0b,00,00,00,01,00,02,00,00,00,00,00,00,00,00,00,03,00,00,00,02,00,02,00,00,00,00,00,00,00" );
                FixCETimeZone(_T("Pacific Standard Time"),  "e0,01,00,00,00,00,00,00,c4,ff,ff,ff,00,00,0b,00,00,00,01,00,02,00,00,00,00,00,00,00,00,00,03,00,00,00,02,00,02,00,00,00,00,00,00,00" );
            #endif
        #endif
    #endif
}
/*============================================================================*/

    CTimeServer::~CTimeServer()

/*============================================================================*/
{
}
/*============================================================================*/

    void                        CTimeServer::InitInstance()

/*============================================================================*/
{
    LOGASSERT(g_pInstance == NULL);
    g_pInstance = new CTimeServer();
}
/*============================================================================*/

    void                        CTimeServer::ReleaseInstance()

/*============================================================================*/
{
    delete g_pInstance;
    g_pInstance = NULL;
}
/*============================================================================*/

    CTimeServer                 *CTimeServer::Instance()

/*============================================================================*/
{
    return g_pInstance;
}
#ifndef MAC_OS
/*============================================================================*/

    double                      CTimeServer::TimeZone()

/*============================================================================*/
{
    return (double)(m_TimeZone.Bias) / 60.0;
}
#ifdef GATEWAY
/*============================================================================*/

    void                        CTimeServer::LoadAllTimeZones(CHLObList *pList)

/*============================================================================*/
{
    HKEY hkTimeZones = NULL;

#ifdef UNDER_CE
    #define TIME_REG _T("Time Zones")
#else
    #define TIME_REG _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Time Zones")
#endif

    if( RegOpenKeyEx(HKEY_LOCAL_MACHINE,TIME_REG,0, KEY_READ,&hkTimeZones) == ERROR_SUCCESS )
    {
        DWORD dwIndex = 0;
        TCHAR tcKeyName[512];
        DWORD dwcbName = 512 * sizeof( TCHAR );
        FILETIME ftLastWrite;

        while( RegEnumKeyEx(hkTimeZones,dwIndex++,tcKeyName, &dwcbName,NULL,NULL,NULL,&ftLastWrite) !=  ERROR_NO_MORE_ITEMS )
        {
            HKEY hkTimeZone;
            if( RegOpenKeyEx(hkTimeZones,tcKeyName,0,  KEY_READ,&hkTimeZone) == ERROR_SUCCESS )
            {
//                DWORD dwTimeZoneIndex;

                // Get Index
//                DWORD dwDataSize = sizeof( DWORD );
//                RegQueryValueEx(hkTimeZone,_T("Index"),NULL, NULL,(BYTE*)&dwTimeZoneIndex,&dwDataSize);
                CTimeZoneInfo *pInfo = new CTimeZoneInfo(hkTimeZone);
                pList->AddTail(pInfo);
                RegCloseKey( hkTimeZone );
            }
            dwcbName = 512 * sizeof( TCHAR );
        }
        RegCloseKey( hkTimeZones );
    }

    // Sort
    int iN = pList->GetCount();
    CTimeZoneInfo **pItems = new CTimeZoneInfo*[iN];
    int iIndex = 0;
    while (!pList->IsEmpty())
    {   
        CTimeZoneInfo *pObj = (CTimeZoneInfo*)pList->RemoveHead();
        pItems[iIndex] = pObj;
        iIndex++;
    }

    qsort(pItems, iN, sizeof(DWORD), CompareTimeZones);

    for (int i = 0; i < iN; i++)
    {
        pList->AddTail(pItems[i]);
    }

    delete [] pItems;
}
/*============================================================================*/

    int                         CTimeServer::IndexOfCurrentTimeZone(CHLObList *pList)

/*============================================================================*/
{
    TIME_ZONE_INFORMATION info;
    GetTimeZoneInformation(&info);

    CHLPos *pPos = pList->GetHeadPosition();
    int iIndex = 0;
    while (pPos != NULL)
    {
        CTimeZoneInfo *pTZI = (CTimeZoneInfo*)pPos->Object();
        pPos = pPos->Next();
        TIME_ZONE_INFORMATION *pInfoTest = pTZI->Info();
        if (memcmp(pInfoTest, &info, sizeof(TIME_ZONE_INFORMATION)) == 0)
            return iIndex;
        iIndex++;
    }    

    pPos = pList->GetHeadPosition();
    iIndex = 0;
    while (pPos != NULL)
    {
        CTimeZoneInfo *pTZI = (CTimeZoneInfo*)pPos->Object();
        pPos = pPos->Next();
        TIME_ZONE_INFORMATION *pInfoTest = pTZI->Info();
        if (pInfoTest->Bias == info.Bias)
            return iIndex;
        iIndex++;
    }    

    LOGASSERT(FALSE);    
    return -1;
}
/*============================================================================*/

    void                        CTimeServer::SetTimeZone(
    CHLObList                   *pList, 
    int                         iIndex )

/*============================================================================*/
{
    CHLPos *pPos = pList->FindIndex(iIndex);
    if (pPos == NULL)
        return;

    CTimeZoneInfo *pInfo = (CTimeZoneInfo*)pPos->Object();
    if (!SetTimeZoneInformation(pInfo->Info()))
    {
        LOGASSERT(FALSE);
    }
    m_dwZoneID  = GetTimeZoneInformation(&m_TimeZone);
}
#endif // GATEWAY
#endif // MACOS
#ifdef UNDER_CE
    #ifdef GATEWAY
    /*============================================================================*/

        void                        CTimeServer::FixCETimeZone(
        CHLString                   sTimeZone,
        char                        *pData )

    /*============================================================================*/
    {
        CHLString sKey;
        sKey.Format(_T("Time Zones\\%s"), (const TCHAR*) sTimeZone);

        HKEY keyTZ;
        if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, sKey, 0, KEY_ALL_ACCESS, &keyTZ) != ERROR_SUCCESS)
            return;

        BYTE byInfo[44];
        char *pStart = pData;
        for (int i = 0; i < 44; i++)
        {
            BYTE byData = (BYTE)HexToInt(pStart, 2);
            byInfo[i] = byData;
            pStart += 3;
        }

        RegSetValueEx(keyTZ, _T("TZI"), 0, REG_BINARY, (BYTE*)byInfo, 44);
        RegCloseKey(keyTZ);
    }
    #endif
#endif
/*============================================================================*/

    int                         CTimeServer::CompareTimeZones( 
    const void                  *arg1, 
    const void                  *arg2 )

/*============================================================================*/
{
#ifdef GATEWAY
    CTimeZoneInfo *p1 = *(CTimeZoneInfo**)arg1;
    CTimeZoneInfo *p2 = *(CTimeZoneInfo**)arg2;
    int iBias1 = p1->Info()->Bias;
    int iBias2 = p2->Info()->Bias;
    if (iBias1 > iBias2)
        return 1;
    if (iBias1 < iBias2)
        return -1;
    return 0;
#else
    return 0;
#endif
}
/*============================================================================*/

    CHLTime::CHLTime()

/*============================================================================*/
{
    GetPresentTime();
}
/*============================================================================*/

    CHLTime::CHLTime(
    SYSTEMTIME                   tm)

/*============================================================================*/
{
    m_Time = tm;
}
/*============================================================================*/

    CHLTime::CHLTime(
    const CHLTime               &rhs )

/*============================================================================*/
{
    *this = rhs;
}
/*============================================================================*/

    CHLTime::~CHLTime()

/*============================================================================*/
{
}
/*============================================================================*/

    void                        CHLTime::Set(
    int                         iYear,
    int                         iMonth,
    int                         iDayOfWeek,
    int                         iDay,
    int                         iHour,
    int                         iMinute,
    int                         iSecond,
    int                         iMSecond )

/*============================================================================*/
{
    #ifdef MAC_OS
        tm Time;
        memset(&Time, 0, sizeof(tm));
        Time.tm_year  = iYear;
        Time.tm_mon   = iMonth;
        Time.tm_mday  = iDay;
        Time.tm_hour  = iHour;
        Time.tm_min   = iMinute;
        Time.tm_sec   = iSecond;
        m_Time = mktime(&Time);
    #else
        m_Time.wYear         = iYear;
        m_Time.wMonth        = iMonth;
        m_Time.wDayOfWeek    = iDayOfWeek;
        m_Time.wDay          = iDay;
        m_Time.wHour         = iHour;
        m_Time.wMinute       = iMinute;
        m_Time.wSecond       = iSecond;
        m_Time.wMilliseconds = iMSecond;
    #endif


}
#ifndef MAC_OS
/*============================================================================*/

    CHLTime::operator           SYSTEMTIME()

/*============================================================================*/
{
    return m_Time;
}
/*============================================================================*/

    CHLTime                     &CHLTime::operator=(
    const SYSTEMTIME            &rhs )

/*============================================================================*/
{
    memcpy(&m_Time, &rhs, sizeof(m_Time));

    return *this;
}
#endif
/*============================================================================*/

    CHLTime                     &CHLTime::operator=(
    const CHLTime               &rhs )

/*============================================================================*/
{
    memcpy(&m_Time, &(rhs.m_Time), sizeof(m_Time));

    return *this;
}
/*============================================================================*/

    CHLTime                     &CHLTime::operator+(
    const CHLTime               &rhs )

/*============================================================================*/
{
    // Don't try to add two times.
    ASSERT(FALSE);
    return *this;
}
/*============================================================================*/

    CHLTimeSpan                 CHLTime::operator-(
    const CHLTime               &rhs )

/*============================================================================*/
{
    // Convert our SYSTEMTIMEs into FILETIMEs: see comments at top.
    CHLTimeSpan hltsSpan;

    #ifdef MAC_OS
        if (m_Time > rhs.m_Time)
        {
            hltsSpan.m_iTimeSpan = (m_Time - rhs.m_Time)*1000;
            hltsSpan.m_bNegative = FALSE;
        }
        else
        {
            hltsSpan.m_iTimeSpan = (rhs.m_Time-m_Time)*1000;
            hltsSpan.m_bNegative = TRUE;
        }
    #else
        __int64 iTthis, iTrhs;
        SystemTimeToFileTime(&m_Time, (FILETIME*) &iTthis);
        SystemTimeToFileTime(&(rhs.m_Time), (FILETIME*) &iTrhs);

        //
        // The rhs is the start time, and we are the end time, so "this" should
        // be larger than rhs to avoid negative time spans.
        //
        if (iTrhs > iTthis)
        {
            hltsSpan.m_bNegative = TRUE;
            hltsSpan.m_iTimeSpan = iTrhs - iTthis;
        }
        else
        {
            hltsSpan.m_bNegative = FALSE;
            hltsSpan.m_iTimeSpan = iTthis - iTrhs;
        }
    #endif

    return hltsSpan;
}
/*============================================================================*/

    BOOL                        CHLTime::operator==(
    const CHLTime               &rhs )

/*============================================================================*/
{
    #ifdef MAC_OS
        return (m_Time == rhs.m_Time);
    #else
        if (m_Time.wYear            != rhs.m_Time.wYear)            return FALSE;
        if (m_Time.wMonth           != rhs.m_Time.wMonth)           return FALSE;
        if (m_Time.wDayOfWeek       != rhs.m_Time.wDayOfWeek)       return FALSE;
        if (m_Time.wDay             != rhs.m_Time.wDay)             return FALSE;
        if (m_Time.wHour            != rhs.m_Time.wHour)            return FALSE;
        if (m_Time.wMinute          != rhs.m_Time.wMinute)          return FALSE;
        if (m_Time.wSecond          != rhs.m_Time.wSecond)          return FALSE;

        if (m_Time.wMilliseconds    != rhs.m_Time.wMilliseconds)    return FALSE;
    #endif


    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLTime::operator>=(
    const CHLTime               &rhs )

/*============================================================================*/
{
    #ifdef MAC_OS
        return (m_Time >= rhs.m_Time);
    #else
        FILETIME ftThis;
        FILETIME ftRHS;
        if (!SystemTimeToFileTime(&m_Time, &ftThis))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }
        if (!SystemTimeToFileTime(&(rhs.m_Time), &ftRHS))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }

        ULARGE_INTEGER ulThis;
        ULARGE_INTEGER ulRHS;

        memcpy(&ulThis, &ftThis, sizeof(ULARGE_INTEGER));
        memcpy(&ulRHS, &ftRHS, sizeof(ULARGE_INTEGER));

        if (ulThis.HighPart > ulRHS.HighPart)
            return TRUE;
        else if (ulThis.HighPart < ulRHS.HighPart)
            return FALSE;

        // HighParts must be equal
        if (ulThis.LowPart >= ulRHS.LowPart)
            return TRUE;

        return FALSE;
    #endif
}
/*============================================================================*/

    BOOL                        CHLTime::operator<=(
    const CHLTime               &rhs )

/*============================================================================*/
{
    #ifdef MAC_OS
        return (m_Time <= rhs.m_Time);
    #else
        FILETIME ftThis;
        FILETIME ftRHS;
        if (!SystemTimeToFileTime(&m_Time, &ftThis))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }
        if (!SystemTimeToFileTime(&(rhs.m_Time), &ftRHS))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }

        ULARGE_INTEGER ulThis;
        ULARGE_INTEGER ulRHS;

        memcpy(&ulThis, &ftThis, sizeof(ULARGE_INTEGER));
        memcpy(&ulRHS, &ftRHS, sizeof(ULARGE_INTEGER));

        if (ulThis.HighPart < ulRHS.HighPart)
            return TRUE;
        else if (ulThis.HighPart > ulRHS.HighPart)
            return FALSE;

        // HighParts must be equal
        if (ulThis.LowPart <= ulRHS.LowPart)
            return TRUE;

        return FALSE;
    #endif
}
/*============================================================================*/

    BOOL                        CHLTime::operator>(
    const CHLTime               &rhs )

/*============================================================================*/
{
    #ifdef MAC_OS
        return (m_Time > rhs.m_Time);
    #else
        FILETIME ftThis;
        FILETIME ftRHS;
        if (!SystemTimeToFileTime(&m_Time, &ftThis))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }
        if (!SystemTimeToFileTime(&(rhs.m_Time), &ftRHS))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }

        ULARGE_INTEGER ulThis;
        ULARGE_INTEGER ulRHS;

        memcpy(&ulThis, &ftThis, sizeof(ULARGE_INTEGER));
        memcpy(&ulRHS, &ftRHS, sizeof(ULARGE_INTEGER));

        if (ulThis.HighPart > ulRHS.HighPart)
            return TRUE;
        else if (ulThis.HighPart < ulRHS.HighPart)
            return FALSE;

        // HighParts must be equal
        if (ulThis.LowPart > ulRHS.LowPart)
            return TRUE;

        return FALSE;
    #endif
}
/*============================================================================*/

    BOOL                        CHLTime::operator<(
    const CHLTime               &rhs )

/*============================================================================*/
{
    #ifdef MAC_OS
        return (m_Time < rhs.m_Time);
    #else
        FILETIME ftThis;
        FILETIME ftRHS;
        if (!SystemTimeToFileTime(&m_Time, &ftThis))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }
        if (!SystemTimeToFileTime(&(rhs.m_Time), &ftRHS))
        {
            LOGASSERT(FALSE);
            return FALSE;
        }

        ULARGE_INTEGER ulThis;
        ULARGE_INTEGER ulRHS;

        memcpy(&ulThis, &ftThis, sizeof(ULARGE_INTEGER));
        memcpy(&ulRHS, &ftRHS, sizeof(ULARGE_INTEGER));

        if (ulThis.HighPart < ulRHS.HighPart)
            return TRUE;
        else if (ulThis.HighPart > ulRHS.HighPart)
            return FALSE;

        // HighParts must be equal
        if (ulThis.LowPart < ulRHS.LowPart)
            return TRUE;

        return FALSE;
    #endif
}
/*============================================================================*/

    void                        CHLTime::AddDays(
    int                         iDays )

/*============================================================================*/
{
    AddSeconds(iDays * 86400);
}
/*============================================================================*/

    void                        CHLTime::AddHours(
    int                         iHours )

/*============================================================================*/
{
    AddSeconds(iHours * 3600);
}
/*============================================================================*/

    void                        CHLTime::AddMinutes(
    int                         iMinutes )

/*============================================================================*/
{
    AddSeconds(iMinutes * 60);
}
/*============================================================================*/

    void                        CHLTime::AddSeconds(
    int                         iSeconds )

/*============================================================================*/
{
    #ifdef MAC_OS
        m_Time += iSeconds;
    #else
        if (iSeconds == 0)
            return;

        // See comments at top: 10,000,000 counts per second...
        __int64 iThis;
        __int64 iSecs = iSeconds;
        if (!SystemTimeToFileTime(&m_Time, (FILETIME*) &iThis))
        {
            LOGASSERT(FALSE);
        }
        iThis += (iSecs * COUNT_SEC);

        // Back to system time.
        if (!FileTimeToSystemTime((FILETIME*) &iThis, &m_Time))
        {
            LOGASSERT(FALSE);
        }
    #endif
}
/*============================================================================*/

    void                        CHLTime::AddMSEC(
    int                         iMSEC )

/*============================================================================*/
{
    #ifdef MAC_OS
        AddSeconds(iMSEC / 1000);
    #else
        if (iMSEC == 0)
            return;

        // See comments at top: 10,000,000 counts per second...
        __int64 iThis;
        __int64 iMS = iMSEC;
        if (!SystemTimeToFileTime(&m_Time, (FILETIME*) &iThis))
        {
            LOGASSERT(FALSE);
        }
        iThis += (iMS * COUNT_MSEC);

        // Back to system time.
        if (!FileTimeToSystemTime((FILETIME*) &iThis, &m_Time))
        {
            DWORD dwError = GetLastError();
            LOGASSERT(FALSE);
        }
    #endif
}
/*============================================================================*/

    BOOL                        CHLTime::DaylightSavings()

/*============================================================================*/
{
    #ifdef MAC_OS
        const time_t tmThis = m_Time;
            struct tm *pLocal = localtime(&tmThis);
        return (pLocal->tm_isdst > 0);
    #else
        #ifdef UNDER_CE
            TIME_ZONE_INFORMATION info;
            return (GetTimeZoneInformation(&info) == TIME_ZONE_ID_DAYLIGHT);
        #else
            time_t thisTime;
            time(&thisTime);
            struct tm *pTemp = localtime(&thisTime);
            return (pTemp->tm_isdst > 0);
        #endif
    #endif
}
/*============================================================================*/

    void                        CHLTime::GetPresentTime(
    BOOL                        bCorrected )

/*============================================================================*/
{
    #ifdef MAC_OS
        time(&m_Time);
    #else
        GetLocalTime(&m_Time);
        #ifdef CRYSTALPAD
            if (bCorrected)
            {
                AddMinutes(m_iGlobalOffset);
            }
        #endif
    #endif
}
/*============================================================================*/

    int                         CHLTime::Year()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_year;
    #else
        return m_Time.wYear;
    #endif
}
/*============================================================================*/

    void                        CHLTime::Year(
    int                         iNew )

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        pTime->tm_year = iNew;
        m_Time = mktime(pTime);
    #else
        m_Time.wYear = iNew;
        UpdateDayOfWeek();
    #endif
}
/*============================================================================*/

    int                         CHLTime::Month()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_mon;
    #else
        return m_Time.wMonth;
    #endif
}
/*============================================================================*/

    void                        CHLTime::Month(
    int                         iNew )

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        pTime->tm_mon = iNew;
        m_Time = mktime(pTime);
    #else
        m_Time.wMonth = iNew;
        UpdateDayOfWeek();
    #endif
}
/*============================================================================*/

    int                         CHLTime::DayOfWeek()

/*============================================================================*/
{
    // SYSTEMTIME has Sunday = 0, Monday = 1, etc, but we want to return
    // Mon=0, ... Sun=6
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_wday;
    #else
        if (m_Time.wDayOfWeek == 0)
            return 6;
        else
            return m_Time.wDayOfWeek - 1;
    #endif
}
/*============================================================================*/

    void                        CHLTime::DayOfWeek(
    int                         iNew )

/*============================================================================*/
{
    // SYSTEMTIME has Sunday = 0, Monday = 1, etc, but we use Mon=0, ...
    #ifndef MAC_OS
        if (iNew == 6)
            m_Time.wDayOfWeek = 0;
        else
            m_Time.wDayOfWeek = iNew + 1;
    #endif
}
/*============================================================================*/

    int                         CHLTime::Day()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_mday;
    #else
        return m_Time.wDay;
    #endif
}
/*============================================================================*/

    void                        CHLTime::Day(
    int                         iNew )

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        pTime->tm_mday = iNew;
        m_Time = mktime(pTime);
    #else
        m_Time.wDay = iNew;
        UpdateDayOfWeek();
    #endif
}
/*============================================================================*/

    int                         CHLTime::Hour()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_hour;
    #else
        return m_Time.wHour;
    #endif
}
/*============================================================================*/

    int                         CHLTime::Hour12()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        int iHour = pTime->tm_hour;
        if (iHour == 0)
            return 12;
        else if (iHour > 12)
            return iHour - 12;
        else
            return iHour;
    #else
        if (m_Time.wHour == 0)
            return 12;
        else if (m_Time.wHour > 12)
            return m_Time.wHour - 12;
        else
            return m_Time.wHour;
    #endif
}
/*============================================================================*/

    void                        CHLTime::Hour(
    int                         iNew )

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        pTime->tm_hour = iNew;
        m_Time = mktime(pTime);
    #else
        m_Time.wHour = iNew;
    #endif
}
/*============================================================================*/

    int                         CHLTime::Minute()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_min;
    #else
        return m_Time.wMinute;
    #endif
}
/*============================================================================*/

    void                        CHLTime::Minute(
    int                         iNew )

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        pTime->tm_min = iNew;
        m_Time = mktime(pTime);
    #else
        m_Time.wMinute = iNew;
    #endif
}
/*============================================================================*/

    int                         CHLTime::Second()

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        return pTime->tm_sec;
    #else
        return m_Time.wSecond;
    #endif
}
/*============================================================================*/

    void                        CHLTime::Second(
    int                         iNew )

/*============================================================================*/
{
    #ifdef MAC_OS
        struct tm *pTime = localtime(&m_Time);
        pTime->tm_sec = iNew;
        m_Time = mktime(pTime);
    #else
        m_Time.wSecond = iNew;
    #endif
}
/*============================================================================*/

    int                         CHLTime::MSecond()

/*============================================================================*/
{
    #ifdef MAC_OS
        return 0;
    #else
        return m_Time.wMilliseconds;
    #endif
}
/*============================================================================*/

    void                        CHLTime::MSecond(
    int                         iNew )

/*============================================================================*/
{
    #ifndef MAC_OS
        m_Time.wMilliseconds = iNew;
    #endif
}
#ifdef STRINGLIB
/*============================================================================*/

    CHLString                   CHLTime::DayOfWeekText()

/*============================================================================*/
{
    return ::DayOfWeekText(DayOfWeek());
}
/*============================================================================*/

    CHLString                   CHLTime::MonthText()

/*============================================================================*/
{
    return ::MonthText(Month());
}
/*============================================================================*/

    CHLString                   CHLTime::TimeText()

/*============================================================================*/
{
    CHLString sRet;
    sRet.Format(_T("%d:%02d %s"), Hour12(), Minute(), (const TCHAR*)AMPMText());
    return sRet;
}
/*============================================================================*/

    CHLString                   CHLTime::TimeTextShort()

/*============================================================================*/
{
    TCHAR cAmPm = 'p';
    if ((Hour() == 24) || (Hour() < 12))
        cAmPm = 'a';

    CHLString sRet;
    sRet.Format(_T("%d:%02d%c"), Hour12(), Minute(), cAmPm);
    return sRet;
}
/*============================================================================*/

    CHLString                   CHLTime::TimeTextSeconds()

/*============================================================================*/
{
    CHLString sRet;
    sRet.Format(_T("%d:%02d:%02d %s"), Hour12(), Minute(), Second(), (const TCHAR*)AMPMText());
    return sRet;
}
/*============================================================================*/

    CHLString                   CHLTime::AMPMText()

/*============================================================================*/
{
    if ((Hour() == 24) || (Hour() < 12))
        return _T("AM");
    else
        return _T("PM");
}
/*============================================================================*/

    CHLString                   CHLTime::DayOfWeekTextShort()

/*============================================================================*/
{
    return ::DayOfWeekTextShort(DayOfWeek());
}
/*============================================================================*/

    CHLString                   CHLTime::MonthTextShort()

/*============================================================================*/
{
    return ::MonthTextShort(Month());
}
#endif
/*============================================================================*/

    CHLString                   CHLTime::FormatDuration(
    int                         iSeconds )

/*============================================================================*/
{
    int iTotal = iSeconds;
    int iRemaining = iSeconds;

    int iHours = iTotal / 3600;
    iRemaining -= (iHours * 3600);

    int iMinutes = iRemaining / 60;
    iRemaining -= (iMinutes * 60);

    int iSec     = iRemaining;

    CHLString sReturn;
    if (iHours > 0)
    {
        // At least an hour

        if (iHours > 1)
            sReturn.Format((TCHAR*)CHLXString("%d Hours, %d Min, %d Sec", "Time"), iHours, iMinutes, iSec);
        else
            sReturn.Format((TCHAR*)CHLXString("%d Hour, %d Min, %d Sec", "Time"), iHours, iMinutes, iSec);
    }
    else if (iMinutes > 0)
    {
        // Less than an hour but more than a minute
        if (iMinutes > 1)
            sReturn.Format((TCHAR*)CHLXString("%d Minutes, %d Sec", "Time"), iMinutes, iSec);
        else
            sReturn.Format((TCHAR*)CHLXString("%d Minute, %d Sec", "Time"), iMinutes, iSec);
    }
    else
    {
        // Just seconds
        if (iSeconds > 1 || iSeconds == 0)
            sReturn.Format((TCHAR*)CHLXString("%d Seconds", "Time"), iSec);
        else
            sReturn.Format((TCHAR*)CHLXString("1 Second", "Time"));
    }

    return sReturn;
}
/*============================================================================*/

    CHLString                   CHLTime::FormatDurationMSEC(
    int                         iMSEC )

/*============================================================================*/
{
    int iTotal = iMSEC;
    int iHours = iTotal / 3600000;
    iTotal -= iHours * 3600000;
    int iMins = iTotal / 60000;
    iTotal -= iMins * 60000;
    int iSec = iTotal / 1000;
    int iMSECRem = iTotal - (iSec*1000);
    CHLString sRet;
    sRet.Format(_T("%s, %d msec"), (const TCHAR*) FormatDuration(iMSEC / 1000), iMSECRem);
    return sRet;
}
/*============================================================================*/

    void                        CHLTime::UpdateDayOfWeek()

/*============================================================================*/
{
    #ifndef MAC_OS
        __int64 iThis;
        if (!SystemTimeToFileTime(&m_Time, (FILETIME*) &iThis))
        {
        }

        if (!FileTimeToSystemTime((FILETIME*) &iThis, &m_Time))
        {
        }
    #endif
}
/*============================================================================*/

    CHLTimeSpan::CHLTimeSpan()

/*============================================================================*/
{
    m_bNegative = FALSE;
    m_iTimeSpan = 0;
}
/*============================================================================*/

    CHLTimeSpan::CHLTimeSpan(
    int                         iDays,
    int                         iHours,
    int                         iMinutes,
    int                         iSeconds,
    int                         iMSeconds )

/*============================================================================*/
{
    Set(iDays, iHours, iMinutes, iSeconds, iMSeconds);
}
/*============================================================================*/

    CHLTimeSpan::~CHLTimeSpan()

/*============================================================================*/
{
}
/*============================================================================*/

    void                        CHLTimeSpan::Set(
    int                         iDays,
    int                         iHours,
    int                         iMinutes,
    int                         iSeconds,
    int                         iMSeconds )

/*============================================================================*/
{
    ASSERT(iDays >= 0);
    ASSERT(iHours >= 0);
    ASSERT(iMinutes >= 0);
    ASSERT(iSeconds >= 0);
    ASSERT(iMSeconds >= 0);

    m_iTimeSpan = 0;
    if (iDays != 0)
        m_iTimeSpan += ((__int64) iDays) * COUNT_DAY;
    if (iHours != 0)
        m_iTimeSpan += ((__int64) iHours) * COUNT_HOUR;
    if (iMinutes != 0)
        m_iTimeSpan += ((__int64) iMinutes) * COUNT_MIN;
    if (iSeconds != 0)
        m_iTimeSpan += ((__int64) iSeconds) * COUNT_SEC;
    if (iMSeconds != 0)
        m_iTimeSpan += ((__int64) iMSeconds) * COUNT_MSEC;
}
/*============================================================================*/

    BOOL                        CHLTimeSpan::Negative()

/*============================================================================*/
{
    return m_bNegative;
}
/*============================================================================*/

    void                        CHLTimeSpan::Negative(
    BOOL                        bNew )

/*============================================================================*/
{
    m_bNegative = bNew;
}
/*============================================================================*/

    CHLTimeSpan::operator       __int64()

/*============================================================================*/
{
    return m_iTimeSpan;
}
/*============================================================================*/

    CHLTimeSpan                 &CHLTimeSpan::operator=(
    const CHLTimeSpan           &rhs )

/*============================================================================*/
{
    if (this == &rhs)
        return *this;

    m_bNegative = rhs.m_bNegative;
    m_iTimeSpan = rhs.m_iTimeSpan;

    return *this;
}
/*============================================================================*/

    CHLTimeSpan                 &CHLTimeSpan::operator=(
    const __int64               &rhs )

/*============================================================================*/
{
    if (rhs < 0)
    {
        m_bNegative = TRUE;
        m_iTimeSpan = -rhs;
    }
    else
    {
        m_bNegative = FALSE;
        m_iTimeSpan = rhs;
    }

    return *this;
}
/*============================================================================*/

    int                         CHLTimeSpan::Days()

/*============================================================================*/
{
    __int64 iRem = ((__int64) m_iTimeSpan) % ((__int64) COUNT_WEEK);
    return (int) ( iRem / ((__int64) COUNT_DAY) );
}
/*============================================================================*/

    int                         CHLTimeSpan::Hours()

/*============================================================================*/
{
    __int64 iRem = ((__int64) m_iTimeSpan) % ((__int64) COUNT_DAY);
    return (int) ( iRem / ((__int64) COUNT_HOUR) );
}
/*============================================================================*/

    int                         CHLTimeSpan::Minutes()

/*============================================================================*/
{
    __int64 iRem = ((__int64) m_iTimeSpan) % ((__int64) COUNT_HOUR);
    return (int) ( iRem / ((__int64) COUNT_MIN) );
}
/*============================================================================*/

    int                         CHLTimeSpan::Seconds()

/*============================================================================*/
{
    __int64 iRem = ((__int64) m_iTimeSpan) % ((__int64) COUNT_MIN);
    return (int) ( iRem / ((__int64) COUNT_SEC) );
}
/*============================================================================*/

    int                         CHLTimeSpan::MSeconds()

/*============================================================================*/
{
    __int64 iRem = ((__int64) m_iTimeSpan) % ((__int64) COUNT_SEC);
    return (int) ( iRem / ((__int64) COUNT_MSEC) );
}
/*============================================================================*/

    int                         CHLTimeSpan::TotalDays()

/*============================================================================*/
{
    return (int) ( ((__int64) m_iTimeSpan) / ((__int64) COUNT_DAY) );
}
/*============================================================================*/

    int                         CHLTimeSpan::TotalHours()

/*============================================================================*/
{
    return (int) ( ((__int64) m_iTimeSpan) / ((__int64) COUNT_HOUR) );
}
/*============================================================================*/

    int                         CHLTimeSpan::TotalMinutes()

/*============================================================================*/
{
    return (int) ( ((__int64) m_iTimeSpan) / ((__int64) COUNT_MIN) );
}
/*============================================================================*/

    int                         CHLTimeSpan::TotalSeconds()

/*============================================================================*/
{
    return (int) ( ((__int64) m_iTimeSpan) / ((__int64) COUNT_SEC) );
}
/*============================================================================*/

    __int64                     CHLTimeSpan::TotalMSeconds()

/*============================================================================*/
{
    return (__int64) ( ((__int64) m_iTimeSpan) / ((__int64) COUNT_MSEC) );
}
/*============================================================================*/

    CHLString                   CHLTimeSpan::MinSecText()

/*============================================================================*/
{
    __int64 iNMins = TotalMinutes();
    __int64 iNSecs = (((__int64)m_iTimeSpan- iNMins * ((__int64) COUNT_MIN)) / ((__int64)COUNT_SEC));

    CHLString sRet;
    sRet.Format(_T("%d:%02d"), (int)iNMins, (int)iNSecs);
    return sRet;
}
/*============================================================================*/

    CHLString                   CHLTimeSpan::HourMinText()

/*============================================================================*/
{
    int iNHours = TotalHours();
    int iNMins = TotalMinutes() - 60 * iNHours;

    CHLString sRet;
    sRet.Format(_T("%d:%02d"), (int)iNHours, (int)iNMins);
    return sRet;
}
/*============================================================================*/

    CHLString                   CHLTimeSpan::HourMinSecText()

/*============================================================================*/
{
    int iNHours = TotalHours();
    int iNMins = TotalMinutes() - 60 * iNHours;
    int iNSecs = TotalSeconds() - 3600 * iNHours - 60 * iNMins;

    CHLString sRet;
    sRet.Format(_T("%d:%02d:%02d"), (int)iNHours, (int)iNMins, (int)iNSecs);
    return sRet;
}

/*============================================================================*/

    void                        HLSleep(
    DWORD                       dwMSEC )

/*============================================================================*/
{
    #ifdef MAC_OS

        timespec timeSpec;
        DWORD dwSEC = dwMSEC / 1000;
        timeSpec.tv_sec = dwSEC;
        dwMSEC = dwMSEC - timeSpec.tv_sec * 1000;
        timeSpec.tv_nsec = dwMSEC * 1000;
        nanosleep(&timeSpec, NULL);
    #else
        ::Sleep(dwMSEC);
    #endif
}

