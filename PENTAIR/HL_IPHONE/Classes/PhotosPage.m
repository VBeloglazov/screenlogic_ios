//
//  PhotosPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PhotosPage.h"
#import "PhotoAlbum.h"
#import "PhotoGridView.h"
#import "PhotosListView.h"
#import "PhotoView.h"
#import "crect.h"
#import "PhotoFullFrameView.h"
#import "IPAD_ViewController.h"
#import "MainTabVC.h"
#import "hlbutton.h"
#import "shader.h"
#import "hlm.h"

@implementation CPhotosPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:pSubTab];
    if (self)
    {
        m_pPhotoAlbumRoot = [[CPhotoAlbumRoot alloc] initWithName:@"Pictures" ID:0 Data1:0 Data2:0];
        [m_pPhotoAlbumRoot SetPhotosPage:self];
        [m_pPhotoAlbumRoot Load];
        m_pWaitGraphic = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self addSubview:m_pWaitGraphic];
        [m_pWaitGraphic release];
        [m_pWaitGraphic startAnimating];
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pPhotoAlbumRoot SetPhotosPage:NULL];
    [m_pPhotoAlbumRoot release];
    [super dealloc];
}
/*============================================================================*/

-(void)NotifyDataReady

/*============================================================================*/
{
    [m_pPhotosListView removeFromSuperview];
    m_pPhotosListView = [[CPhotosListView alloc] initWithFrame:CGRectZero Root:m_pPhotoAlbumRoot PhotosPage:self];
    [self addSubview:m_pPhotosListView];
    [m_pPhotosListView release];
    
    [m_pWaitGraphic stopAnimating];
    [m_pWaitGraphic removeFromSuperview];
    m_pWaitGraphic = NULL;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    UIView *pSuperView = self.superview;
    CGRect rFullFrame = pSuperView.bounds;
    rFullFrame.origin.y = -self.frame.origin.y;
    [m_pFullFrameView setFrame:rFullFrame];

    CGRect rWaitGraphic = rThis;
    [CRect CenterDownToX:&rWaitGraphic DX:32];
    [CRect CenterDownToY:&rWaitGraphic DY:32];
    [m_pWaitGraphic setFrame:rWaitGraphic];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iDXList = MIN(250, rThis.size.width/3);
        CGRect rList = [CRect BreakOffLeft:&rThis DX:iDXList];
        [m_pPhotosListView setFrame:rList];
        [m_pPhotosGridView setFrame:rThis];
    }
    else
    {
        if (m_bIPODShift)
            rThis.origin.x -= rThis.size.width;

        CGRect rGrid = rThis;
        rGrid.origin.x += rThis.size.width;
        [m_pPhotosListView setFrame:rThis];
        [m_pPhotosGridView setFrame:rGrid];
    }
}
/*============================================================================*/

-(void)SetActiveAlbum:(CPhotoAlbum*)pAlbum

/*============================================================================*/
{
    if (_m_pPhotosGridView != NULL)
    {
        [_m_pPhotosGridView removeFromSuperview];
        _m_pPhotosGridView = NULL;
    }


    _m_pPhotosGridView = m_pPhotosGridView;
    m_pPhotosGridView = NULL;
    
    if ([pAlbum NPhotos] > 0)
    {
        m_pPhotosGridView = [[CPhotoGridView alloc] initWithFrame:CGRectZero MainPage:self Album:pAlbum];
        [self addSubview:m_pPhotosGridView];
        [m_pPhotosGridView release];
    }

    [m_pPhotosGridView setAlpha:0];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    [UIView setAnimationDidStopSelector:@selector(GridChangeComplete:finished:context:)];


    [_m_pPhotosGridView setAlpha:0];
    [m_pPhotosGridView setAlpha:1];


    [UIView commitAnimations];

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        if (m_pMainPage != NULL)
        {
            CHLToolBar *pToolBar = [m_pMainPage ToolBar];
            if (m_pPhotosGridView != NULL)
            {
                if (m_pGoButton == NULL)
                {
                    CGRect rBtn = pToolBar.bounds;
                    rBtn = [CRect BreakOffRight:&rBtn DX:rBtn.size.width / 4];
                    m_pGoButton = [[CToolBarButton alloc] initWithFrame:rBtn Text:NULL Icon:@"Photos_Main.png"];
                    [m_pGoButton Orientation:TOOLBAR_TOP];
                    [m_pGoButton setAutoresizingMask:0xFFFFFFFF];
                    [pToolBar HLAddSubView:m_pGoButton];
                    [m_pGoButton setFrame:rBtn];
                    [m_pGoButton IconFormat:HL_ICONCENTERMAX];
                    [m_pGoButton addTarget:self action:@selector(IPodShift:) forControlEvents:UIControlEventTouchDown];
                }
            }
            else
            {
                [m_pGoButton removeFromSuperview];
                m_pGoButton = NULL;
            }
        }
    }
}
/*============================================================================*/

-(void)GoFullFrame:(id)sender 

/*============================================================================*/
{
    [self setClipsToBounds:NO];

    CPhotoView *pView = (CPhotoView*)sender;
    UIView *pSuperView = self.superview;
    CGRect rFullFrame = pSuperView.bounds;
    rFullFrame.origin.y = -self.frame.origin.y;

    m_pFullFrameView = [[CPhotoFullFrameView alloc] initWithFrame:rFullFrame PhotosPage:self PhotoView:pView];
    [self addSubview:m_pFullFrameView];
    [m_pFullFrameView release];
    CGRect rSrc = pView.bounds;
    rSrc = [self convertRect:rSrc fromView:pView];
    double dAspect = rFullFrame.size.width / rFullFrame.size.height;
    [CRect ExpandToAspect:&rSrc DXDY:dAspect];

    double dScaleX = rSrc.size.width / rFullFrame.size.width;
    double dScaleY = rSrc.size.height / rFullFrame.size.height;
    CGAffineTransform MyScaler2 = CGAffineTransformMakeScale(dScaleX, dScaleY);
    CGAffineTransform MyTrans2  = CGAffineTransformMakeTranslation([CRect XMid:rSrc]-[CRect XMid:rFullFrame], [CRect YMid:rSrc]-[CRect YMid:rFullFrame]);
    CGAffineTransform MyConcat2 = CGAffineTransformConcat(MyScaler2, MyTrans2);
    [m_pFullFrameView setTransform:MyConcat2];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];

    [m_pPhotosGridView setAlpha:0];
    [m_pPhotosListView setAlpha:0];
    [m_pMainPage HideNavBar:TRUE];
    [[IPAD_ViewController MainViewController] HideNavBar:TRUE];
    [m_pFullFrameView setTransform:CGAffineTransformIdentity];

    [UIView commitAnimations];
}
/*============================================================================*/

-(void)ExitFullFrame:(id)sender 

/*============================================================================*/
{
    [self setClipsToBounds:NO];

    if (_m_pFullFrameView != NULL)
    {
        [_m_pFullFrameView removeFromSuperview];
        _m_pFullFrameView = NULL;
    }

    _m_pFullFrameView = m_pFullFrameView;
    m_pFullFrameView = NULL;


    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    [UIView setAnimationDidStopSelector:@selector(ExitFullFrameChangeComplete:finished:context:)];
    [m_pMainPage HideNavBar:FALSE];
    [[IPAD_ViewController MainViewController] HideNavBar:FALSE];

    [m_pPhotosGridView setAlpha:1];
    [m_pPhotosListView setAlpha:1];
    [_m_pFullFrameView setAlpha:0];


    [UIView commitAnimations];
}
/*============================================================================*/

-(void)GridChangeComplete:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [_m_pPhotosGridView removeFromSuperview];
    _m_pPhotosGridView = NULL;
}
/*============================================================================*/

-(void)ExitFullFrameComplete:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [_m_pFullFrameView removeFromSuperview];
    _m_pFullFrameView = NULL;
}
/*============================================================================*/

-(void)IPodShift:(id)sender

/*============================================================================*/
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];

    if ([m_pGoButton IsChecked])
    {
        [m_pGoButton SetChecked:NO];
        m_bIPODShift = FALSE;
    }
    else
    {
        [m_pGoButton SetChecked:YES];
        m_bIPODShift = TRUE;
    }

    [self layoutSubviews];

    [UIView commitAnimations];
}

@end
