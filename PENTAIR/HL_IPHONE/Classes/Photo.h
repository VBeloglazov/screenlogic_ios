//
//  Photo.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"

@class CPhotoAlbum;
@class CPhotoReceiver;

/*============================================================================*/

@interface CPhoto : NSObject 

/*============================================================================*/
{
    CPhotoAlbum                         *m_pAlbum;
    int                                 m_iIndex;
    NSString                            *m_pName;
    CPhotoReceiver                      *m_pLoResReceiver;
    CPhotoReceiver                      *m_pHiResReceiver;
}

-(id)initWithAlbum:(CPhotoAlbum*)pAlbum Index:(int)iIndex Name:(NSString*)pName;
-(UIImage*)GetImage:(BOOL)bHiRes View:(UIView*)pView;
-(void)RemoveFromWaitList:(UIView*)pView;

-(CPhotoAlbum*)Album;
-(int)Index;
-(void)ReleaseHiRes;

@end



/*============================================================================*/

@interface CPhotoReceiver : NSObject    < CNSSocketSink >

/*============================================================================*/
{
    BOOL                            m_bWaiting;

    CPhoto                          *m_pPhoto;
    BOOL                            m_bHiRes;

    unsigned char                   *m_pData;
    int                             m_iNData;

    NSMutableArray                  *m_pWaitList;
}

-(id)initWithPhoto:(CPhoto*)pPhoto HiRes:(BOOL)bHiRes;

-(UIImage*)GetImage:(UIView*)pView;
-(void)RemoveFromWaitList:(UIView*)pView;

@end
