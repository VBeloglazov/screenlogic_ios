//
//  PhotoAlbum.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "subtab.h"
#import "NSSocketSink.h"

@class CNSMSG;
@class CPhoto;
@class CPhotosPage;
@class CPhotoAlbum;

/*============================================================================*/

@interface CPhotoAlbumRoot : CSubTab <CNSSocketSink>

/*============================================================================*/
{
    BOOL                            m_bLoaded;
    CPhotosPage                     *m_pPhotosPage;

    NSMutableArray                  *m_pAlbums;
}

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;

-(BOOL)IsLoaded;
-(void)SetPhotosPage:(CPhotosPage*)pPage;

-(void)Load;
-(void)LoadAlbum:(CNSMSG*)pMSG;

-(int)NAlbums;
-(CPhotoAlbum*)Album:(int)iIndex;

@end


/*============================================================================*/

@interface CPhotoAlbum : NSObject

/*============================================================================*/
{
    NSString                        *m_pName;
    int                             m_iID;
    NSMutableArray                  *m_pPictures;
    NSMutableArray                  *m_pAlbums;
}

-(id)initWithName:(NSString*)pName ID:(int)iID;

-(void)LoadPictures:(CNSMSG*)pMSG;
-(void)LoadAlbums:(CNSMSG*)pMSG;

-(int)ID;
-(NSString*)Name;

-(int)NPhotos;
-(CPhoto*)Photo:(int)iIndex;

-(int)NAlbums;
-(CPhotoAlbum*)Album:(int)iIndex;

-(void)ReleaseAllHiRes;


@end
