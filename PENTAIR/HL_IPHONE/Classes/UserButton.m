//
//  AudioButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "UserButton.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "NSMSG.h"

#define CMD_PRESS   1000
#define CMD_RELEASE 1001

@implementation CUserButton

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Icon:(NSString*)sIcon 
                    Data:(int)iData
                    Type:(int)iType

/*============================================================================*/
{
    int iThisStyle = iStyle;

    switch (iType)
    {
    case CONTROL_HOMEPAGE_BUTTON:
    case CONTROL_HOMEPAGE_BUTTON_TOGGLE:
    case CONTROL_HOMEPAGE_BUTTON_MOMENTARY:
        iStyle = 0;
        break;
    }

    if (self = [super initWithFrame:rFrame IconFormat:iIconFormat Style:iStyle Text:pText TextSize:iTextSize TextColor:RGBText Color:RGBFace Icon:sIcon])
    {
        m_iData = iData;
        m_iType = iType;

        if (iType == CONTROL_HOMEPAGE_BUTTON_TOGGLE)
        {
            [CHLComm AddSocketSink:self];
            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_SETSERVER_GETTOGGLEBUTTONQ] autorelease];
            [pQ PutInt:m_iData];
            [pQ SendMessageWithMyID:self];

            if (iThisStyle > 0)
                [self setUserInteractionEnabled:FALSE];
        }

        if (iType == CONTROL_HOMEPAGE_BUTTON_MOMENTARY)
        {
            [self SetCommandID:CMD_PRESS ReleaseID:CMD_RELEASE Responder:self];
        }
    }
    
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    if (m_iType == CONTROL_HOMEPAGE_BUTTON_TOGGLE)
    {
        [CHLComm RemoveSocketSink:self];
    }
    [super dealloc];
}
/*============================================================================*/

-(void)ButtonPress:(id)sender

/*============================================================================*/
{
    [m_pCommandResponder OnCommand:m_iCommandID];

    switch (m_iType)
    {
    case CONTROL_HOMEPAGE_BUTTON_TOGGLE:
        {
            [self SetChecked:!m_bChecked];
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_SYSTEM_TOGGLEBUTTONSTATE];
            [pQ autorelease];
            [pQ PutInt:m_iData];
            [pQ PutInt:m_bChecked];
            [pQ SendMessage];
        }
        break;

    case CONTROL_HOMEPAGE_BUTTON:
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_EVENTSERVER_EXECUTEHPBUTTONQ];
            [pQ autorelease];
            [pQ PutInt:HL_240x320];
            [pQ PutInt:m_iData];
            [pQ SendMessage];
        }
        break;
        
    case CONTROL_HOMEPAGE_BUTTON_MOMENTARY:
        {
        }
        break;
    }
    
    [super ButtonPress:sender];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    [pMSG ResetRead];
    int iID = [pMSG GetInt];
    BOOL bChecked = [pMSG GetInt];
    [pMSG ResetRead];


    switch ([pMSG MessageID])
    {
    case HLM_SYSTEM_TOGGLEBUTTONSTATE:
    case HLM_SETSERVER_GETTOGGLEBUTTONA:
        {
            if (iID == m_iData)
            {
                [self SetChecked:bChecked];
            }
        }
        break;
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_PRESS:
    case CMD_RELEASE:
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_EVENTSERVER_EXECUTEHPMOMENTARYQ];
            [pQ autorelease];
            [pQ PutInt:HL_240x320];
            [pQ PutInt:m_iData];
            [pQ PutInt:(iCommandID == CMD_PRESS)];
            [pQ SendMessage];
        }
    }
}
@end
