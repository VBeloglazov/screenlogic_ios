//
//  HLPageControl.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/11/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HLPageControl.h"
#import "hlm.h"
#import "MainTabVC.h"

#define DY_BAR                  6


@implementation CHLPageControl

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame MainTabPage:(CMainTabViewController*)pPage

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        m_pMainTabPage = pPage;
        self.opaque = NO;
        m_pDotView = [[CDotView alloc] initWithFrame:CGRectMake(0, frame.size.height/2-DY_BAR/2, DY_BAR, DY_BAR)];
        [m_pDotView setOpaque:NO];
        [self addSubview:m_pDotView];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pDotView release];
    [super dealloc];
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    // Drawing code
    int iRad = DY_BAR / 2;
    int iDYThis = rect.size.height;
    int iDXThis = rect.size.width;
    
    double dPI = 3.14159;
    CGContextRef pRef = UIGraphicsGetCurrentContext();
    CGContextBeginPath (pRef);
    CGContextMoveToPoint(pRef, iRad, iDYThis/2-iRad);
    CGContextAddArc(pRef, iDXThis - iRad, iDYThis/2,     iRad,  -dPI/2, dPI/2, 0);
    CGContextAddArc(pRef, iRad, iDYThis/2, iRad,  dPI/2, -dPI/2, 0);
    CGContextSetBlendMode(pRef, kCGBlendModeCopy);
    [[UIColor darkGrayColor] set];
    CGContextFillPath(pRef);
}
/*============================================================================*/

-(void)SetNumberOfPages:(int)iN

/*============================================================================*/
{
    m_iNPages = iN;
}
/*============================================================================*/

-(void)SetCurrentPage:(int)iPage

/*============================================================================*/
{
    int iDXDot = DY_BAR;
    if (m_iNPages > 1)
        iDXDot = self.frame.size.width / m_iNPages;
    iDXDot = MAX(iDXDot, DY_BAR);

    int iRange = self.frame.size.width - iDXDot;
    int iPos = 0;
    if (m_iNPages > 1)
        iPos = iPage * iRange / (m_iNPages-1);

	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    CGRect rDot = CGRectMake(iPos, self.frame.size.height/2-DY_BAR/2, iDXDot, DY_BAR);
    [m_pDotView setFrame:rDot];
	[UIView commitAnimations];
  
}
/*============================================================================*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
	UITouch *touch = [touches anyObject];
	
	// Only move the placard view if the touch was in the placard view
	if ([touch view] == self) 
    {
		// In case of a double tap outside the placard view, update the placard's display string
		if ([touch tapCount] == 1) 
        {
            CGPoint pt = [touch locationInView:self];
            int iDXThis = self.bounds.size.width;
            if (pt.x > iDXThis / 2)
                [m_pMainTabPage PageLeftRight:DIR_RIGHT];
            else
                [m_pMainTabPage PageLeftRight:DIR_LEFT];
        }
    }
}


@end


@implementation CDotView

/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    // Drawing code
    int iRad = DY_BAR / 2;
    int iDYThis = rect.size.height;
    int iDXThis = rect.size.width;
    
    double dPI = 3.14159;
    CGContextRef pRef = UIGraphicsGetCurrentContext();
    CGContextBeginPath (pRef);
    CGContextMoveToPoint(pRef, iRad, iDYThis/2-iRad);
    CGContextAddArc(pRef, iDXThis - iRad, iDYThis/2,     iRad,  -dPI/2, dPI/2, 0);
    CGContextAddArc(pRef, iRad, iDYThis/2, iRad,  dPI/2, -dPI/2, 0);
    CGContextSetBlendMode(pRef, kCGBlendModeCopy);
    [[UIColor whiteColor] set];
    CGContextFillPath(pRef);
}

@end
