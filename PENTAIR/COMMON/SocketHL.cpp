/*
--------------------------------------------------------------------------------

    SocketHL.cpp

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlsock.h"
#include "hlmsg.h"
#include "SocketError.h"

#include "SocketHL.h"

#define RESEND_PACKET_SIZE      1400
#define MAX_INPUT_BUFFER        500000

/*============================================================================*/

    void                    SetIP(
    BYTE*                   byAddress,  // in, BYTE[4]
    SOCKADDR_IN*            pAddr )     // out

/*============================================================================*/
{
#ifndef MAC_OS
    pAddr->sin_addr.S_un.S_un_b.s_b1 = byAddress[0];
    pAddr->sin_addr.S_un.S_un_b.s_b2 = byAddress[1];
    pAddr->sin_addr.S_un.S_un_b.s_b3 = byAddress[2];
    pAddr->sin_addr.S_un.S_un_b.s_b4 = byAddress[3];
    pAddr->sin_addr.S_un.S_addr	  = *(DWORD*)byAddress;
#else
    // BYTE 1 is lowest bits (ie 192.)
    DWORD dw1 = byAddress[3];
    DWORD dw2 = byAddress[2];
    DWORD dw3 = byAddress[1];
    DWORD dw4 = byAddress[0];
    DWORD dwIP = (dw1 << 24) | (dw2 << 16) | (dw3 << 8) | dw4;
    pAddr->sin_addr.s_addr = dwIP;
#endif
}
/*============================================================================*/

    DWORD                   GetIP(
    SOCKADDR_IN*            pAddr )

/*============================================================================*/
{
#ifndef MAC_OS
    return pAddr->sin_addr.S_un.S_addr;
#else
    return pAddr->sin_addr.s_addr;
#endif
}
/*============================================================================*/

    CSocketHL::CSocketHL(
    BYTE*                       pIP,
    WORD                        wPort,
    int                         iTimeout )

    : m_hSocket             ( INVALID_SOCKET ),
      m_bValid              ( FALSE ),
      m_strLastError        ( CHLString(_HLTEXT("")) ),
      m_bConnected          ( FALSE ),
      m_bBlocking           ( FALSE ),
      m_bTCPNoDelay         ( TRUE ),
      m_pBufferIn           ( NULL ),
      m_iNBufferIn          ( 0 ),
      m_iBufferIn           ( 0 ),
      m_pMessage            ( NULL ),
      m_iNMessage           ( 0 ),
      m_iNTotalBytesIn      ( 0 ),
      m_iNTotalBytesOut     ( 0 ),
      m_iNMessagesIn        ( 0 ),
      m_iNMessagesOut       ( 0 ),
      m_iNPartialHeaderRX   ( 0 ),
      m_iNPartialRX         ( 0 ),
      m_iNMultipleRX        ( 0 ),
      m_iNMultiPacketRX     ( 0 ),
      m_iNQATickCount       ( 0 ),
      m_iNOutputBuffer      ( 0 ),
      m_iBytesLastRead      ( 0 )

/*============================================================================*/
{
    m_iTimeout = iTimeout;

    SOCKADDR_IN address;
    address.sin_family = AF_INET;
    SetIP( pIP, &address );
    address.sin_port = htons(wPort);
    
    m_dwIP  = GetIP(&address);
    m_btIP1 = (BYTE)(m_dwIP      ) & 0x000000FF;
    m_btIP2 = (BYTE)(m_dwIP >>  8) & 0x000000FF;
    m_btIP3 = (BYTE)(m_dwIP >> 16) & 0x000000FF;
    m_btIP4 = (BYTE)(m_dwIP >> 24) & 0x000000FF;
	m_wPort = wPort;
	m_sAddress.Format( _HLTEXT("%d.%d.%d.%d"),
		m_btIP1, m_btIP2, m_btIP3, m_btIP4 );
	m_sAddressPort.Format( _HLTEXT("%d.%d.%d.%d:%d"),
		m_btIP1, m_btIP2, m_btIP3, m_btIP4, m_wPort );

    m_hSocket = socket( PF_INET, SOCK_STREAM, 0 );
    if ( INVALID_SOCKET == m_hSocket )
    {
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );

        CHLString strMes;
        strMes.Format( _HLTEXT("Error #%d in socket(): %s"),
            iError, (const HLTCHAR*)strSocketError );
        SetLastError( strMes );
        return;
    }

    if ( SOCKET_ERROR == connect( m_hSocket, (SOCKADDR*) &address, sizeof(address) ) )
    {
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );

        CHLString strMes;
        strMes.Format( _HLTEXT("Error #%d in connect(): %s"),
            iError, (const HLTCHAR*)strSocketError );
        SetLastError( strMes );

        closesocket(m_hSocket);
        m_hSocket = INVALID_SOCKET;        

        return;
    }

    m_bConnected = TRUE;

    SetBlocking  ( m_bBlocking   );
    SetTCPNoDelay( m_bTCPNoDelay );

    m_bValid = TRUE;
}
/*============================================================================*/

    CSocketHL::CSocketHL(
    CHLString                   sIP,
    WORD                        wPort,
    int                         iTimeout )

    : m_hSocket             ( INVALID_SOCKET ),
      m_bValid              ( FALSE ),
      m_strLastError        ( CHLString(_HLTEXT("")) ),
      m_bConnected          ( FALSE ),
      m_bBlocking           ( FALSE ),
      m_bTCPNoDelay         ( TRUE ),
      m_pBufferIn           ( NULL ),
      m_iNBufferIn          ( 0 ),
      m_iBufferIn           ( 0 ),
      m_pMessage            ( NULL ),
      m_iNMessage           ( 0 ),
      m_iNTotalBytesIn      ( 0 ),
      m_iNTotalBytesOut     ( 0 ),
      m_iNMessagesIn        ( 0 ),
      m_iNMessagesOut       ( 0 ),
      m_iNPartialHeaderRX   ( 0 ),
      m_iNPartialRX         ( 0 ),
      m_iNMultipleRX        ( 0 ),
      m_iNMultiPacketRX     ( 0 ),
      m_iNQATickCount       ( 0 ),
      m_iNOutputBuffer      ( 0 ),
      m_iBytesLastRead      ( 0 )

/*============================================================================*/
{
    m_iTimeout = iTimeout;

    BYTE byAddress[4];
    int iLenAddress = sIP.GetLength();
    char* pBufAddress = sIP.GetBufferCHAR(iLenAddress);

    const char* pStart = (const char*) pBufAddress;
    for ( int i=0; i<4; i++ )
    {
        byAddress[i] = (BYTE)atoi(pStart);
        pStart = strstr(pStart, ".");
        if ( i < 3 )
        {
            if ( !pStart )
            {
                SetLastError( _HLTEXT("Address invalid") );
                return;
            }

            pStart++;
        }
    }

#ifdef MFC_CSTRING
#ifdef HLCONFIG
	sIP.ReleaseBufferCHAR(iLenAddress);
#else
	sIP.ReleaseBuffer(iLenAddress);
#endif
#endif

    SOCKADDR_IN address;
    address.sin_family = AF_INET;
    SetIP( byAddress, &address );
    address.sin_port = htons(wPort);

    m_dwIP  = GetIP( &address );
    m_btIP1 = byAddress[0];
    m_btIP2 = byAddress[1];
    m_btIP3 = byAddress[2];
    m_btIP4 = byAddress[3];
	m_wPort = wPort;
	m_sAddress = sIP;
	m_sAddressPort.Format( _HLTEXT("%d.%d.%d.%d:%d"),
        m_btIP1, m_btIP2, m_btIP3, m_btIP4,
        m_wPort
    );

    m_hSocket = socket( PF_INET, SOCK_STREAM, 0 );
    if ( INVALID_SOCKET == m_hSocket )
    {
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );

        CHLString strMes;
        strMes.Format( _HLTEXT("Error #%d in socket(): %s"),
            iError, (const HLTCHAR*)strSocketError );
        SetLastError( strMes );
        return;
    }

    if ( SOCKET_ERROR == connect( m_hSocket, (SOCKADDR*) &address, sizeof(address) ) )
    {
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );

        CHLString strMes;
        strMes.Format( _HLTEXT("Error #%d in connect(): %s"),
            iError, (const HLTCHAR*)strSocketError );
        SetLastError( strMes );

        closesocket(m_hSocket);
        m_hSocket = INVALID_SOCKET;        

        return;
    }

    m_bConnected = TRUE;

    SetBlocking  ( m_bBlocking   );
    SetTCPNoDelay( m_bTCPNoDelay );

    m_bValid = TRUE;
}
/*============================================================================*/

    CSocketHL::CSocketHL(
    SOCKET                      hSocket,
    SOCKADDR_IN*                pAddress,
    int                         iTimeout )

	: m_hSocket				( INVALID_SOCKET ),
      m_bValid              ( FALSE ),
      m_strLastError        ( CHLString(_HLTEXT("")) ),
      m_bConnected          ( FALSE ),
	  m_bBlocking			( FALSE ),
	  m_bTCPNoDelay			( TRUE ),
	  m_pBufferIn			( NULL ),
	  m_iNBufferIn			( 0 ),
	  m_iBufferIn			( 0 ),
	  m_pMessage			( NULL ),
	  m_iNMessage			( 0 ),
	  m_iNTotalBytesIn		( 0 ),
	  m_iNTotalBytesOut		( 0 ),
	  m_iNMessagesIn		( 0 ),
	  m_iNMessagesOut		( 0 ),
	  m_iNPartialHeaderRX	( 0 ),
	  m_iNPartialRX			( 0 ),
	  m_iNMultipleRX		( 0 ),
	  m_iNMultiPacketRX		( 0 ),
	  m_iNQATickCount		( 0 ),
	  m_iNOutputBuffer		( 0 ),
      m_iBytesLastRead      ( 0 )

/*============================================================================*/
{
    if ( INVALID_SOCKET == hSocket )
    {
        SetLastError( _HLTEXT("Invalid Socket") );
        return;
    }

    m_hSocket = hSocket;

    m_iTimeout = iTimeout;

#ifndef IPHONE
    m_dwIP  = pAddress->sin_addr.S_un.S_addr;
#else
    m_dwIP  = pAddress->sin_addr.s_addr;
#endif
    m_btIP1 = (BYTE)(m_dwIP      ) & 0x000000FF;
    m_btIP2 = (BYTE)(m_dwIP >>  8) & 0x000000FF;
    m_btIP3 = (BYTE)(m_dwIP >> 16) & 0x000000FF;
    m_btIP4 = (BYTE)(m_dwIP >> 24) & 0x000000FF;
	m_wPort = htons(pAddress->sin_port);
	m_sAddress.Format( _HLTEXT("%d.%d.%d.%d"),
		m_btIP1, m_btIP2, m_btIP3, m_btIP4 );
	m_sAddressPort.Format( _HLTEXT("%d.%d.%d.%d:%d"),
		m_btIP1, m_btIP2, m_btIP3, m_btIP4, m_wPort );

    m_bConnected = TRUE;

    SetBlocking  ( m_bBlocking   );
    SetTCPNoDelay( m_bTCPNoDelay );

    m_bValid = TRUE;
}
/*============================================================================*/

    CSocketHL::~CSocketHL()

/*============================================================================*/
{
    // This destructor does NOT disconnect the socket.
    // If the socket should be closed, the Disconnect( "Destructor" )
	// must be called before to destroy the object.
}
/*============================================================================*/

    void                        CSocketHL::GetCommStats(
    CHLMSG*                     pMSG )

/*============================================================================*/
{
    pMSG->PutInt( m_btIP1 );
    pMSG->PutInt( m_btIP2 );
    pMSG->PutInt( m_btIP3);
    pMSG->PutInt( m_btIP4 );
    
    pMSG->PutInt(NTotalBytesIn());
    pMSG->PutInt(NTotalBytesOut());

    double dSec = m_hltConnect.HLTickCount() / 1000.f;
    pMSG->PutInt((int)dSec);
}
/*============================================================================*/

    void                        CSocketHL::Disconnect(
    CHLString                   sReason )

/*============================================================================*/
{
    // Close socket
    if ( IsConnected() )
    {
        shutdown(m_hSocket, 0);
        closesocket(m_hSocket);
        m_hSocket = INVALID_SOCKET;

#ifdef TRACE5
        CHLTime tmNow;	tmNow.GetPresentTime();
        TRACE5( _HLTEXT("\n%02d:%02d:%02d Disconnecting %s. Reason: '%s'"),
            tmNow.Hour(), tmNow.Minute(), tmNow.Second(),
			(const HLTCHAR*) m_sAddress,
            sReason.GetLength() > 0 ? (const HLTCHAR*) sReason : _HLTEXT("No reason!")
		);
#endif
    }

    m_bConnected = FALSE;

    // Cleanup buffers.
    delete [] m_pBufferIn;
    m_pBufferIn       = NULL;
    m_iNBufferIn      = 0;
    m_iBufferIn       = 0;

    delete [] m_pMessage;
    m_pMessage      = NULL;
    m_iNMessage     = 0;
}
/*============================================================================*/

    void                        CSocketHL::SetBlocking(
    BOOL                        bNew )

/*============================================================================*/
{
    // ioctlsocket takes non-zero for non-blocking, zero for blocking.
    BOOL bParam = bNew ? 0 : 1;
    int iReturn = ioctlsocket(m_hSocket, FIONBIO, (ULONG*)&bParam);
    LOGASSERT(iReturn == 0);

    m_bBlocking = bNew;
}
/*============================================================================*/

    BOOL                        CSocketHL::TCPNoDelay()

/*============================================================================*/
{
    return m_bTCPNoDelay;
}
/*============================================================================*/

    void                        CSocketHL::SetTCPNoDelay(
    BOOL                        bNew )

/*============================================================================*/
{
    //
    // non-zero means no delay: this disables the Nagle algorithm and sends
    // small packets as they arrive instead of grouping them.
    //
#ifndef MAC_OS
    BOOL bParam = bNew ? 1 : 0;
    int iReturn = setsockopt(m_hSocket, IPPROTO_TCP, TCP_NODELAY, (const char *) &bParam, sizeof(BOOL));
    LOGASSERT(iReturn == 0);
#endif

    m_bTCPNoDelay = bNew;
}
/*============================================================================*/

    void                        CSocketHL::IncrementNMessagesOut()

/*============================================================================*/
{
    m_iNMessagesOut++;
}
/*============================================================================*/

    void                        CSocketHL::IncrementNQATickCount(
    int                         iDTicks )

/*============================================================================*/
{
    m_iNQATickCount += iDTicks;
}
/*============================================================================*/

    BOOL                        CSocketHL::ReadyToSend()

/*============================================================================*/
{
    if ( !IsConnected() )
        return FALSE;

    //
    // See if there's room to send
    //

    #ifdef MAC_OS
        fd_set set;
        fd_set setZero;
        FD_ZERO(&setZero);
        FD_ZERO(&set);
        FD_SET(Socket(), &set);

        timeval tv;
        tv.tv_sec   = 0;
        tv.tv_usec  = 1000;

        int iSel = select(Socket()+1, &setZero, &set, &setZero, &tv);
        if (iSel > 0)
            return TRUE;
    #else
        fd_set set;
        fd_set setZero;
        setZero.fd_count    = 0;
        set.fd_count        = 1;
        set.fd_array[0]     = Socket();

        timeval tv;
        tv.tv_sec   = 0;
        tv.tv_usec  = 1000;

        int iSel = select(0, &setZero, &set, &setZero, &tv);
        if (iSel > 0)
            return TRUE;
    #endif

    return FALSE;
}
/*============================================================================*/

    BOOL                        CSocketHL::Send(
    int                         iNBytes,
    BYTE*                       pBytes,
    BOOL                        bCritical )

/*============================================================================*/
{
    if ( !IsConnected() )
        return FALSE;

    // Get length and send.
    int iSent = send(m_hSocket, (char*) pBytes, iNBytes, 0);

    if (iSent != iNBytes)
    {
        // Error sending, shutdown the socket
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );

#ifdef MAC_OS
        if ( EAGAIN == iError )
#else
		if ( WSAEWOULDBLOCK == iError )
#endif
		{
			if (  iSent >= 0 )
			{
				TRACE( _HLTEXT("Warning WOULDBLOCK") );
				m_iNTotalBytesOut += iSent;
				return TRUE;
			}

			// For non-critial messages ie pictures etc. just return on blocking errors
			if ( !bCritical )
				return TRUE;

			// Cannot send right now, add to output buffer
			if (AppendOutputBuffer(iNBytes, pBytes))
				return TRUE;
		}

        // Unable to send - socket was shut down remotely.
        TRACE( _HLTEXT("ERROR: ") + strSocketError );
        Disconnect( _HLTEXT("Cannot Send") );
        return FALSE;
    }

    m_iNTotalBytesOut += iSent;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CSocketHL::CheckReceiveMsg()

/*============================================================================*/
{
    if (!ClearOutputBuffer())
        return FALSE;

    //
    // Before reading network, dispatch any messages that were left over
    // from a previous CheckReceive.
    //
    // It is possible that we can end up with more than one message in here
    // after a read of the network. This is because we always read everything
    // off of the socket that is available, and put it in our buffer. When
    // we then go through the buffer, we may have zero or more complete
    // messages, plus a part of a message at the end.
    //
    if (m_iBufferIn >= sizeof(HLT_HEADER))
    {
        // See how big the first message is.
        HLT_HEADER *pHeader = (HLT_HEADER*) m_pBufferIn;
        int iNBytesData     = pHeader->m_iNBytes;

        // Check size, make sure its reasonable
        if (iNBytesData > MAX_INPUT_BUFFER)
        {
            Disconnect( _HLTEXT("Input Message too big") );
            return FALSE;
        }

        int iNBytesTotal    = iNBytesData + sizeof(HLT_HEADER);

        if (m_iBufferIn >= iNBytesTotal)
        {
            // So we do have a complete message ready to go. Copy it over
            // to the message area.
            AllocMessage(iNBytesTotal);
            memcpy(m_pMessage, m_pBufferIn, iNBytesTotal);

            // Then update  m_pBufferIn and return.
            int iNBytesLeft = m_iBufferIn - iNBytesTotal;
            if (iNBytesLeft != 0)
            {
                memmove(m_pBufferIn, m_pBufferIn+iNBytesTotal, iNBytesLeft);
                m_iBufferIn = iNBytesLeft;
            }
            else
                m_iBufferIn = 0;

            m_iNMultipleRX++;
            return TRUE;
        }
    }

    // Still connected?
    if ( !IsConnected() )
        return FALSE;

    //
    // At this point m_pBufferIn has nothing or just the first part of a message.
    //

    // Use ioctl call to see if anything is there.
    ULONG ulBytesAvailable = 0;
    if ( SOCKET_ERROR == ioctlsocket( m_hSocket, FIONREAD, &ulBytesAvailable ) )
    {
        // Error: assume socket is gone.
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );
        Disconnect( strSocketError );
        return FALSE;
    }
    if ( ulBytesAvailable == 0 )
    {
        // Check for socket closed: recv will return 0.
        char cTest;
        int iTest = recv(m_hSocket, (char*)(&cTest), 1, 0);
        if (iTest == 1)
        {
            m_hltRecv.Restart();
            AllocBuffer(m_iBufferIn+1);
            m_pBufferIn[m_iBufferIn] = cTest;
            m_iBufferIn++;
            return FALSE;
        }

        if ( m_iTimeout > -1  &&  m_hltRecv.HLTickCount() > (DWORD)m_iTimeout )
        {
            Disconnect( _HLTEXT("Timeout") );
            return FALSE;
        }

        if ( SOCKET_ERROR == iTest )
        {
            int iError;
            CHLString strSocketError = SocketError::LastSocketError( &iError );

#ifdef MAC_OS
            if (EAGAIN == iError )
#else
            if ( WSAEWOULDBLOCK == iError )
#endif
            {
                return FALSE;
            }
        }

        Disconnect( _HLTEXT("Assume socket is closed") );
        return FALSE;
    }
    LOGASSERT(ulBytesAvailable < 0x7FFFFFFF);
    int iNBytesAvailable = (int) ulBytesAvailable;

    //
    // There is data to read from the network: check our buffer size and expand if needed.
    //
    int iNBufferActive = m_iBufferIn + iNBytesAvailable;
    AllocBuffer(iNBufferActive);
    m_iBytesLastRead = recv(m_hSocket, (char*)(m_pBufferIn + m_iBufferIn), iNBytesAvailable, 0);
    if ( SOCKET_ERROR == m_iBytesLastRead )
    {
        // Error: assume socket is gone.
        Disconnect( _HLTEXT("Bad Receive2") );
        return FALSE;
    }

    if (m_iBytesLastRead > 0)
    {
        m_hltRecv.Restart();
    }

    // See if there was more than one message queued up.
    int iNLeft = iNBytesAvailable - m_iBytesLastRead;
    if (iNLeft != 0)
    {
        m_iNMultiPacketRX++;
        CHLTimer hltTime;
        while (iNLeft > 0)
        {
            int iNReRead = recv(m_hSocket, (char*)(m_pBufferIn + m_iBufferIn + m_iBytesLastRead), iNLeft, 0);
            m_iBytesLastRead += iNReRead;
            iNLeft = iNBytesAvailable - m_iBytesLastRead;

            // Timeout? Should not happen.
            if (hltTime.HLTickCount() > HL_TIMEOUT_PING)
            {
                // Did not read all data: reset socket.
                Disconnect( _HLTEXT("Bad Receive3") );
                return FALSE;
            }
        }
    }
    m_iNTotalBytesIn += m_iBytesLastRead;
    m_iBufferIn      += m_iBytesLastRead;

    // See if we have less than one complete header in memory.
    if (iNBufferActive < sizeof(HLT_HEADER))
    {
        // Update our insertion point, return.
        m_iNPartialHeaderRX++;
        return FALSE;
    }

    // Check the header to see how big the first message is.
    HLT_HEADER *pHeader = (HLT_HEADER*) m_pBufferIn;
    int iNBytesData     = pHeader->m_iNBytes;
    int iNBytesTotal    = iNBytesData + sizeof(HLT_HEADER);

    //
    // Sanity check: at this point, we have everything from the network that is now ready
    // to come in, and have it in our working buffer, m_pBufferIn.
    //
    // m_pBufferIn now has zero or more complete messages, plus an optional partial message
    // at the end.
    //

    // If we have no complete messages, bail: we'll process this one next time in.
    if (iNBufferActive < iNBytesTotal)
    {
        // Update our insertion point, our partial RX counter and return.
        TRACE( _HLTEXT("Partial Receive\r\n") );
        m_iNPartialRX++;
        return FALSE;
    }

    // We have at least one complete message: handle it like above.
    AllocMessage(iNBytesTotal);
    memcpy(m_pMessage, m_pBufferIn, iNBytesTotal);

    // Then update  m_pBufferIn and return.
    int iNBytesLeft = m_iBufferIn - iNBytesTotal;
    if (iNBytesLeft > 0)
    {
        memmove(m_pBufferIn, m_pBufferIn+iNBytesTotal, iNBytesLeft);
        m_iBufferIn = iNBytesLeft;
    }
    else
        m_iBufferIn = 0;

    // Update our message counter, return.
    m_iNMessagesIn++;
    LOGASSERT(iNBytesTotal != 0);
    LOGASSERT(m_pMessage != NULL);
    return TRUE;
}
/*============================================================================*/

    BOOL                        CSocketHL::CheckReceiveBuf(
    ULONG                       ulBytesExpected )

/*============================================================================*/
{
    // Still connected?
    if ( !IsConnected() )
        return FALSE;

    ULONG ulBytesAvailable = 0;
    if ( SOCKET_ERROR == ioctlsocket( m_hSocket, FIONREAD, &ulBytesAvailable ) )
    {
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );
        Disconnect( strSocketError );
        return FALSE;
    }

    ULONG uAvailableMin = (ulBytesExpected > 0) ? ulBytesExpected : 1;

    if ( ulBytesAvailable >= uAvailableMin )
    {
        LOGASSERT( ulBytesAvailable < 0x7FFFFFFF );
        int iNBytesAvailable = (int) ulBytesAvailable;

        LOGASSERT( ulBytesExpected < 0x7FFFFFFF );
        int iBytesExpected   = (int) ulBytesExpected;

        int iBytesToRead = (iBytesExpected > 0) ? iBytesExpected : iNBytesAvailable;

        //
        // There is data to read from the network: check our buffer size and expand if needed.
        //
        int iNBufferActive = m_iBufferIn + iNBytesAvailable;
        AllocBuffer(iNBufferActive);
        m_iBytesLastRead = recv( m_hSocket, (char*)(m_pBufferIn + m_iBufferIn), iBytesToRead, 0 );
        if ( SOCKET_ERROR == m_iBytesLastRead )
        {
            int iError;
            CHLString strSocketError = SocketError::LastSocketError( &iError );
            Disconnect( strSocketError );
            return FALSE;
        }

        m_iNTotalBytesIn += m_iBytesLastRead;
        m_iBufferIn      += m_iBytesLastRead;

        // Message received
        AllocMessage( m_iBytesLastRead );
        memcpy( m_pMessage, m_pBufferIn, m_iBytesLastRead );

        // Then update  m_pBufferIn and return.
        int iNBytesLeft = m_iBufferIn - m_iBytesLastRead;
        if (iNBytesLeft > 0)
        {
            memmove(m_pBufferIn, m_pBufferIn+m_iBytesLastRead, iNBytesLeft);
            m_iBufferIn = iNBytesLeft;
        }
        else
            m_iBufferIn = 0;

        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

    void                        CSocketHL::AllocBuffer(
    int                         iSize )

/*============================================================================*/
{
    // Is it bigger?
    if (iSize <= m_iNBufferIn)
        return;

    // Need a bigger buffer: expand ours now.
    int iOldSize    = m_iNBufferIn;
    BYTE *pOld      = m_pBufferIn;

    m_iNBufferIn  = 64 + 64 * (iSize / 64);
    m_pBufferIn   = new BYTE[m_iNBufferIn];
    LOGASSERT(m_pBufferIn != NULL);

    // Copy in old data
    if (iOldSize != 0)
    {
        LOGASSERT(pOld != NULL);
        memcpy(m_pBufferIn, pOld, iOldSize);
        delete [] pOld;
    }
}
/*============================================================================*/

    void                        CSocketHL::AllocMessage(
    int                         iSize )

/*============================================================================*/
{
    // Is it bigger?
    if (iSize <= m_iNMessage)
        return;

    // Need a bigger buffer: expand ours now.
    BYTE *pOld      = m_pMessage;

    m_iNMessage = 64 + 64 * (iSize / 64);
    m_pMessage  = new BYTE[m_iNMessage];
    LOGASSERT(m_pMessage != NULL);

    // Don't need to copy in old data, unlike m_pBufferIn.
    delete [] pOld;
}
/*============================================================================*/

    BOOL                        CSocketHL::AppendOutputBuffer(
    int                         iNBytes,
    BYTE                        *pData )

/*============================================================================*/
{
    TRACE( _HLTEXT("Warning: Using Overflow Buffer") );

    if ((iNBytes + m_iNOutputBuffer) > OUTPUT_BUFFER)
        return FALSE;
    memcpy(m_pOutputBuffer + m_iNOutputBuffer, pData, iNBytes);
    m_iNOutputBuffer += iNBytes;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CSocketHL::ClearOutputBuffer()

/*============================================================================*/
{
    if (m_iNOutputBuffer <= 0)
        return TRUE;

    // Stuff waiting to go out first
    int iToSend = __min(m_iNOutputBuffer, RESEND_PACKET_SIZE);

    int iSent = send(m_hSocket, (char *) m_pOutputBuffer, iToSend, 0);
    if (iSent != iToSend)
    {
        // Didn't go, see why
        int iError;
        CHLString strSocketError = SocketError::LastSocketError( &iError );

#ifdef MAC_OS
        if (EAGAIN == iError )
#else
        if ( WSAEWOULDBLOCK == iError )
#endif
        {
            return FALSE;
        }

        // Unable to send, something bad happened
        TRACE( _HLTEXT("ERROR: ") + strSocketError );
        Disconnect( _HLTEXT("Cannot Send") );
        return FALSE;
    }

    // Pulled off some more
    m_iNOutputBuffer -= iToSend;

    // All done?
    if (m_iNOutputBuffer == 0)
        return TRUE;

    // Still more, index back and try again later
    memmove(m_pOutputBuffer, m_pOutputBuffer + iToSend, m_iNOutputBuffer);
    return FALSE;
}
