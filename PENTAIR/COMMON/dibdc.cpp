/*
--------------------------------------------------------------------------------

    DIBDC.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#ifdef IPHONE
    #include "setserver.h"
    #include "helpers.h"
    #include "colormap.h"
#else
    #include "helpers.h"
    #include "hlwindow.h"
    #include "fontserver.h"
    #include "speed.h"
    #include "hlmsg.h"
    #include "vdecode.h"
    #include "gdiserv.h"
    #include "shadepoly.h"
    #include "speed.h"
    #include "main.h"
    #include "pixblender.h"
    #include "hlipp.h"
    #include "colormap.h"
#endif

#include "dibdc.h"


#if defined(DIB_TRACKER) | defined(VRAM_TRACKER)
    #include "main.h"
    #include "dibserv.h"
#endif

#define FINE_SCALE

    int                         CHLDibDC::g_iDCCount = 0;
    
#ifdef HLOSD
    #define R_MASK              0x000000FF
    #define G_MASK              0x0000FF00
    #define B_MASK              0x00FF0000
    #define A_MASK              0xFF000000
#elif IPHONE
    #define R_MASK              0x0000FF00
    #define G_MASK              0x00FF0000
    #define B_MASK              0xFF000000
    #define A_MASK              0x000000FF
#else
    #define R_MASK              0xF800
    #define G_MASK              0x07E0
    #define B_MASK              0x001F
#endif

    #define A_MASK_32           0x07E0F81F
    #define B_MASK_32           0xF81F07E0

/*============================================================================*/

    CHLDibDC::CHLDibDC(
    int                         iDX,
    int                         iDY,
    int                         iXOrgAbs,
    int                         iYOrgAbs,
    DIBDC_MEMORY_TYPE           Type,
    int                         iAttribs  )

    // Use default base class constructor.
    : CHLDC()

/*============================================================================*/
{
    START_TIMER(GDI_DIBDCCONST);

#ifndef IPHONE
    m_hOldBMP           = NULL;
    m_hCurBMP           = NULL;
#endif

    m_iXOrgAbs          = iXOrgAbs;
    m_iYOrgAbs          = iYOrgAbs;
    m_pParentDIB        = NULL;
    m_bOwnDC            = TRUE;
#ifdef IPHONE
    m_iDX               = iDX;
#else
    m_iDX               = DWORDAlign(iDX);
#endif
    m_iDY               = iDY;
    m_iImageDX          = iDX;
    m_pBits             = NULL;
    m_pMask             = NULL;
    m_iAttribs          = 0;
    m_pUpdateRectList   = NULL;
    m_iUserData         = 0;
    m_iUserData2        = 0;
    m_iAttribs          = iAttribs;

#ifdef IPHONE
    m_iAlphaFlags       = 0;
#endif

    BOOL bWantSurface = FALSE;

    if (Type == MEMORY_SYSTEM)
        Type = MEMORY_SYSTEM;

    switch (Type)
    {
    case MEMORY_SYSTEM:
        break;
    case MEMORY_SYSTEM_DISCARDABLE:
        m_iAttribs |= DDFLAG_DISCARDABLE;
        break;
    case MEMORY_DDRAW_VRAM:
        {
            bWantSurface = TRUE;
            m_iAttribs &= ~DIBATTRIB_FORCESYSMEM;
        }
        break;
    }


    #ifdef HLDDRAW
        m_pSurface      = NULL;
        if (g_pGDIServer->DDrawOK() && bWantSurface)
        {
            m_pSurface = g_pGDIServer->CreateSurface(NULL, 0, 0, m_iDX, m_iDY, &m_iDX);
            #ifdef VRAM_TRACKER
                g_pDibServer->RegisterVRAMDIB(this);
            #endif
        }
    #endif

//    LOGASSERT(m_iDX > 0 && m_iDY > 0);
    m_iDX = __max(1, m_iDX);
    m_iDY = __max(1, m_iDY);

    #ifdef DEBUG
        m_bHDC      = FALSE;
    #endif

    #ifdef HLDDRAW
        if (m_pSurface == NULL)
        {
            int iNBytes = m_iDX * m_iDY * sizeof(HL_PIXEL);
            m_pBits = (HL_PIXEL*)HLAlloc(iNBytes);
        }
    #else
        int iNBytes = m_iDX * m_iDY * sizeof(HL_PIXEL);
        m_pBits = (HL_PIXEL*)HLAlloc(iNBytes);
    #endif

    CDXYRect rGlobal(iXOrgAbs, iYOrgAbs, m_iImageDX, m_iDY);
    GlobalClip(&rGlobal);

    END_TIMER(GDI_DIBDCCONST);

    #ifdef DIB_TRACKER
        g_pDibServer->RegisterTracker(this);
    #endif
}
/*============================================================================*/

    CHLDibDC::CHLDibDC(
    int                         iDX,
    int                         iDY )

    // Use default base class constructor.
    : CHLDC()

/*============================================================================*/
{
    START_TIMER(GDI_DIBDCCONST);

#ifndef IPHONE
    m_hOldBMP           = NULL;
    m_hCurBMP           = NULL;
#endif

    m_iXOrgAbs          = 0;
    m_iYOrgAbs          = 0;
    m_pParentDIB        = NULL;
    m_bOwnDC            = TRUE;
#ifdef IPHONE
    m_iDX               = iDX;
#else
    m_iDX               = DWORDAlign(iDX);
#endif
    m_iDY               = iDY;
    m_iImageDX          = iDX;
    m_pBits             = NULL;
    m_pMask             = NULL;
    m_iAttribs          = 0;
    m_pUpdateRectList   = NULL;
    m_iUserData         = 0;
    m_iUserData2        = 0;
    m_iAttribs          = 0;

    m_iDX = __max(1, m_iDX);
    m_iDY = __max(1, m_iDY);


    int iNBytes = m_iDX * m_iDY * sizeof(HL_PIXEL);
    m_pBits = (HL_PIXEL*)HLAlloc(iNBytes);

    END_TIMER(GDI_DIBDCCONST);

    #ifdef DIB_TRACKER
        g_pDibServer->RegisterTracker(this);
    #endif
}
/*============================================================================*/

    CHLDibDC::CHLDibDC(
    CHLDibDC                    *pParentDIB,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         *piDXOrg,
    int                         *piDYOrg )

/*============================================================================*/
{
    #ifdef DEBUG
        m_bHDC      = FALSE;
    #endif

    int iX0 = iXLeft;
    int iY0 = iYTop;

    CDXYRect rParent(0, 0, pParentDIB->DX(), pParentDIB->DY());
    CHLRect rInt = rParent.IntersectWith(CDXYRect(iXLeft, iYTop, iDX, iDY));

    iXLeft = rInt.XLeft();
    iYTop = rInt.YTop();
    iDX = __max(0, rInt.DX());
    iDY = __max(0, rInt.DY());

    *piDXOrg = iXLeft - iX0;
    *piDYOrg = iYTop  - iY0;

    LOGASSERT(iXLeft >= 0);
    LOGASSERT(iYTop >= 0);
    
    m_pParentDIB        = pParentDIB;
    m_iXLeft            = iXLeft;
    m_iYTop             = iYTop;
    m_hDC               = NULL;
    m_pUpdateRectList   = NULL;
    m_iUserData   = 0;
    m_iUserData2  = 0;

    #ifdef HL_OSD
        m_pBits     = NULL;
    #else
        m_pBits     = m_pParentDIB->Bits();
    #endif

    m_pMask         = NULL;
    m_iImageDX      = iDX;
    m_iDX           = iDX;
    m_iDY           = iDY;
    m_iXOrgAbs      = 0;
    m_iYOrgAbs      = 0;
    m_bOwnDC        = FALSE;
    m_iAttribs      = 0;
    m_pClipGlobal   = NULL;

#ifdef IPHONE
    m_iAlphaFlags       = 0;
#endif


    #ifdef HLDDRAW
        m_pSurface      = NULL;
    #endif

    LOGASSERT(iXLeft >= 0);
    LOGASSERT(iYTop >= 0);
    LOGASSERT(iXLeft+iDX <= pParentDIB->DX());
    LOGASSERT(iYTop+iDY <= pParentDIB->DY());

    #ifdef DIB_TRACKER
        g_pDibServer->RegisterTracker(this);
    #endif
}
/*============================================================================*/

    CHLDibDC::CHLDibDC(
    CHLDibDC                    *pParentDIB,
    CHLRect                     rParent,
    int                         *piDXOrg,
    int                         *piDYOrg )

/*============================================================================*/
{
    #ifdef DEBUG
        m_bHDC      = FALSE;
    #endif

    int iX0 = rParent.XLeft();
    int iY0 = rParent.YTop();
    CDXYRect rParentAll(0, 0, pParentDIB->DX(), pParentDIB->DY());
    rParent = rParentAll.IntersectWith(rParent);

    *piDXOrg = rParent.XLeft() - iX0;
    *piDYOrg = rParent.YTop()  - iY0;

    int iDX = __max(0, rParent.DX());
    int iDY = __max(0, rParent.DY());

    m_pParentDIB        = pParentDIB;
    m_iXLeft            = rParent.XLeft();
    m_iYTop             = rParent.YTop();
    m_hDC               = NULL;
    m_pBits             = m_pParentDIB->Bits();
    m_pMask             = NULL;
    m_iImageDX          = iDX;
    m_iDX               = iDX;
    m_iDY               = iDY;
    m_iXOrgAbs          = 0;
    m_iYOrgAbs          = 0;
    m_bOwnDC            = FALSE;
    m_iAttribs          = 0;
    m_pClipGlobal       = NULL;
    m_pUpdateRectList   = NULL;
    m_iUserData         = 0;
    m_iUserData2        = 0;

#ifdef IPHONE
    m_iAlphaFlags       = 0;
#endif


    #ifdef HLDDRAW
        m_pSurface      = NULL;
    #endif

    #ifdef DIB_TRACKER
        g_pDibServer->RegisterTracker(this);
    #endif
}
/*============================================================================*/

    CHLDibDC::~CHLDibDC()

/*
NOTES:  The base class destructor is NOT virtual, so we have to clean up all
        the base class variables here.
*/
/*============================================================================*/
{
    FlushText();

    // Base class cleanup.
    if (m_pParentDIB == NULL)
    {
#ifdef IPHONE
        //
        // IPHONE
        //
        delete [] m_pBits;
#else
        //
        // WINDOWS
        //

    #ifdef HLDDRAW
        if (m_pSurface != NULL)
        {
            LOGASSERT(m_pBits == NULL);
            LOGASSERT(m_hDC == NULL);

            ReleaseBits();
            if (m_hDC != NULL)
                m_pSurface->ReleaseDC(m_hDC);
            if (m_pBits != NULL)
                m_pSurface->Unlock(NULL);
            m_hDC = NULL;
            m_pBits = NULL;
            ULONG ulRes = m_pSurface->Release();
            m_pSurface = NULL;
            LOGASSERT(ulRes == 0);
            #ifdef VRAM_TRACKER
                g_pDibServer->UnRegisterVRAMDIB(this);
            #endif
        }
    #endif // HLDDRAW

        // This class cleanup.
        if (m_hCurBMP != NULL)
        {
            LOGASSERT(m_hDC != NULL);
            SelectObject(m_hDC, m_hOldBMP);
            BOOL bDelDC = DeleteDC(m_hDC);
            BOOL bDelOB = DeleteObject(m_hCurBMP);
            LOGASSERT(bDelDC);
            LOGASSERT(bDelOB);
            g_iDCCount--;
        }
        else
        {
            HLFree(m_pBits);
            m_pBits = NULL;
        }
#endif // WINOWS
    }

    HLFree(m_pMask);
    m_pMask = NULL;

    delete m_pClipGlobal;
    m_pClipGlobal = NULL;

    delete m_pUpdateRectList;
    m_pUpdateRectList = NULL;

    m_bOwnDC = FALSE;

    while (!m_ClipStack.IsEmpty())
    {
        CHLRect *pRect = (CHLRect*)m_ClipStack.RemoveHead();
        delete pRect;
    }

    while (!m_OriginStack.IsEmpty())
    {
        POINT *pPt = (POINT*)m_OriginStack.RemoveHead();
        delete pPt;
    }

    #ifdef DIB_TRACKER
        g_pDibServer->UnRegisterTracker(this);
    #endif
}
/*============================================================================*/

    CHLRect                     CHLDibDC::GetAlphaRect()

/*============================================================================*/
{
    return CDXYRect(0, 0, 0, 0);
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateCopy(
    int                         iAttribs )

/*============================================================================*/
{
    if (iAttribs == -1)
        iAttribs = m_iAttribs;

    CHLDibDC *pNew = new CHLDibDC(ImageDX(), DY(), 0, 0, MEMORY_SYSTEM, iAttribs);
    FlushText();

    if (m_pParentDIB != NULL)
    {
        pNew->BltFromDibDC(0, 0, m_pParentDIB, m_iXLeft, m_iYTop, DX(), DY());        
    }
    else
    {
        pNew->BltFromDibDC(this, 0, 0);
    }

    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateCroppedCopy(CHLRect rSrc)

/*============================================================================*/
{
    CHLDibDC *pNew = new CHLDibDC(rSrc.DX(), rSrc.DY(), 0, 0, MEMORY_SYSTEM);
    pNew->BltFromDibDC(0, 0, this, rSrc.XLeft(), rSrc.YTop(), rSrc.DX(), rSrc.DY());
    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateFlip(
    BOOL                        bFlipHz, 
    BOOL                        bFlipVt )

/*============================================================================*/
{
    DIBDC_MEMORY_TYPE MemType = MEMORY_SYSTEM;

    #ifdef HLDDRAW
        if (m_pSurface != NULL)
        {
            MemType = MEMORY_SYSTEM;
        }
    #endif


    CHLDibDC *pNew = new CHLDibDC(ImageDX(), DY(), 0, 0, MemType, m_iAttribs|DIBATTRIB_FORCESYSMEM);

    if (bFlipHz && bFlipVt)
    {
        for (int iY = 0; iY < DY(); iY++)
        {
            HL_PIXEL *pIn = Pixel(ImageDX()-1, iY);
            HL_PIXEL *pOut = pNew->Pixel(0, DY()-iY-1);
            for (int iX = 0; iX < ImageDX(); iX++)
            {
                *pOut = *pIn;
                pOut++;
                pIn--;
            }
        }

        return pNew;
    }

    if (bFlipHz)
    {
        for (int iX = 0; iX < ImageDX(); iX++)
        {
            HL_PIXEL *pIn = Pixel(iX, 0);
            HL_PIXEL *pOut = pNew->Pixel(ImageDX()-iX-1, 0);
            for (int iY = 0; iY < DY(); iY++)
            {
                *pOut = *pIn;
                pIn += DXPitch();
                pOut += DXPitch();
            }
        }
    }
    if (bFlipVt)
    {
        for (int iY = 0; iY < DY(); iY++)
        {
            HL_PIXEL *pIn = Pixel(0, iY);
            HL_PIXEL *pOut = pNew->Pixel(0, DY()-iY-1);
            memcpy(pOut, pIn, DX() * sizeof(HL_PIXEL));
        }
    }

    return pNew;
}
/*============================================================================*/

    void                        CHLDibDC::DDConvertToVRAM()

/*============================================================================*/
{
#ifdef HLDDRAW
    if (m_pSurface != NULL)
        return;

    HL_DIRECTDRAWSURFACE pNewSurf = g_pGDIServer->CreateSurface(this, 0, 0, m_iImageDX, m_iDY, &m_iDX);
    if (pNewSurf == NULL)
        return;

    #ifdef VRAM_TRACKER
        g_pDibServer->RegisterVRAMDIB(this);
    #endif
    m_pSurface = pNewSurf;
    HLFree(m_pBits);
    HLFree(m_pMask);
    m_pBits = NULL;
    m_pMask = NULL;
#endif
}
/*============================================================================*/

    void                        CHLDibDC::DDConvertToSysRAM()

/*============================================================================*/
{
#ifdef HLDDRAW
    if (m_pSurface == NULL)
        return;

    ReleaseBits();

    LOGASSERT(m_pBits == NULL);
    m_pBits = (HL_PIXEL*)HLAlloc(m_iDX * m_iDY * sizeof(HL_PIXEL));

    HL_SURFACEDESC surfaceDesc;
    memset(&surfaceDesc, 0, sizeof(HL_SURFACEDESC));
    surfaceDesc.dwSize      = sizeof(HL_SURFACEDESC);
    surfaceDesc.dwFlags = DDSD_HEIGHT|DDSD_WIDTH|DDSD_LPSURFACE;

    #ifdef HL_WIN32
        HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
    #else
        #ifdef HL_DDRAWBASIC
            HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAITNOTBUSY|DDLOCK_READONLY, NULL);
        #else
            HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
        #endif
    #endif

    if (hr != S_OK)
    {
        LOGASSERT(FALSE);
        return;
    }

    HL_PIXEL *pSrc = (HL_PIXEL*) surfaceDesc.lpSurface;
    int iDXSrc = surfaceDesc.lPitch / 2;
    HL_PIXEL *pTgt = m_pBits;
    for (int i = 0; i < m_iDY; i++)
    {   
        memcpy(pTgt, pSrc, m_iImageDX * sizeof(HL_PIXEL));
        pTgt += m_iDX;
        pSrc += iDXSrc;
    }

    m_pSurface->Unlock(NULL);
    m_pSurface->Release();
    m_pSurface = NULL;
    #ifdef VRAM_TRACKER
        g_pDibServer->UnRegisterVRAMDIB(this);
    #endif
#endif
}
/*============================================================================*/

    DIBDC_MEMORY_TYPE           CHLDibDC::MemoryType()

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
        return m_pParentDIB->MemoryType();

#ifdef HLDDRAW
    if (m_pSurface != NULL)
        return MEMORY_DDRAW_VRAM;
#endif

    return MEMORY_SYSTEM;  
}
#ifdef HLDDRAW
/*============================================================================*/

    HL_DIRECTDRAWSURFACE         CHLDibDC::GetSurface()

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
        return m_pParentDIB->GetSurface();

    if (m_pSurface == NULL)
        return NULL;
    ReleaseBits();
    return m_pSurface;
}
#endif
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateScaledCopy(
    int                         iDX, 
    int                         iDY,
    int                         iAttribs,
    BOOL                        bDDrawOK )

/*============================================================================*/
{
    if (iDX == m_iImageDX && iDY == m_iDY)
    {
        return CreateCopy();
    }

    DIBDC_MEMORY_TYPE MemType = MEMORY_SYSTEM;
    if (bDDrawOK)
    {
        MemType = MEMORY_SYSTEM;
    }


    CHLDibDC *pNew = new CHLDibDC(iDX, iDY, 0, 0, MemType, iAttribs);

    if (iDX == m_iImageDX)
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pSrc = Pixel(0, iY * m_iDY / iDY);
            HL_PIXEL *pDst = pNew->Pixel(0, iY);
            memcpy(pDst, pSrc, iDX * sizeof(HL_PIXEL));
        }

        return pNew;
    }

    int iIntPartX   = m_iImageDX / iDX;
    int iFractPartX = m_iImageDX % iDX;
    int iErrorX     = 0;

    HL_PIXEL *pSrcRow = Bits();
    HL_PIXEL *pTgtRow = pNew->Bits();

    int iIntPartY   = m_iDY / iDY;
    int iFractPartY = m_iDY % iDY;
    int iErrorY     = 0;

    int iDXPitchSource = DXPitch();
    int iDXPitchTarget = pNew->DXPitch();

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pIn  = (HL_PIXEL*)pSrcRow;
        HL_PIXEL *pOut = (HL_PIXEL*)pTgtRow;

        int iDXThisRow = iDX;

        while (iDXThisRow-- > 0)
        {
            *pOut = *pIn;
            pOut++;

            pIn += iIntPartX;
            iErrorX += iFractPartX;
            if (iErrorX >= iDX)
            {
                iErrorX -= iDX;
                pIn++;
            }            
        }        

        pTgtRow += iDXPitchTarget;
        pSrcRow += (iDXPitchSource * iIntPartY);

        iErrorY += iFractPartY;
        if (iErrorY >= iDY)
        {
            iErrorY -= iDY;
            pSrcRow += iDXPitchSource;
        }

    }

    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateBicubicScale(
    int                         iDXTgt,
    int                         iDYTgt,
    int                         iAttribs )

/*============================================================================*/
{
    CHLDibDC *pRet = new CHLDibDC(iDXTgt, iDYTgt, 0, 0, MEMORY_SYSTEM, iAttribs);
    
    for (int iYTgt = 0; iYTgt < iDYTgt; iYTgt++)
    {
        HL_PIXEL *pOut = pRet->Pixel(0, iYTgt);
        for (int iXTgt = 0; iXTgt < iDXTgt; iXTgt++)
        {
            HL_PIXEL wRes = GetBicubicRGB(iXTgt, iYTgt, iDXTgt, iDYTgt);
            *pOut = wRes;
            pOut++;
        }
    }


    return pRet;
}
/*============================================================================*/

    HL_PIXEL                    CHLDibDC::GetBicubicRGB(
    int                         iXTgt,
    int                         iYTgt,
    int                         iDXTgt,
    int                         iDYTgt )

/*============================================================================*/
{
    int iXLSrc256 = (iXTgt) * m_iImageDX * 256 / iDXTgt;
    int iXRSrc256 = (iXTgt+1) * m_iImageDX * 256 / iDXTgt;
    int iYTSrc256 = iYTgt * m_iDY * 256 / iDYTgt;
    int iYBSrc256 = (iYTgt+1) * m_iDY * 256 / iDYTgt;
    int iXL = iXLSrc256 / 256;
    int iXR = (iXRSrc256-1) / 256;
    int iYT = iYTSrc256 / 256;
    int iYB = (iYBSrc256-1) / 256;

    LOGASSERT(iXR >= 0 && iXR < m_iImageDX);
    LOGASSERT(iYB >= 0 && iYB < m_iDY);

    int iXLFact = 256-(iXLSrc256-256*iXL);
    int iXRFact = (iXRSrc256-256*iXR);
    int iYTFact = 256-(iYTSrc256-256*iYT);
    int iYBFact = (iYBSrc256-256*iYB);
    int iDXMid = iXR - iXL - 1;
    int iDYMid = iYB - iYT - 1;
    int iNBlockMid = (iDXMid * iDYMid);

    // Edges (minus corners)
    int iDenom = (iNBlockMid * 256) + iDYMid * iXLFact + iDYMid * iXRFact + iDXMid * iYTFact + iDXMid * iYBFact;
    int iULFact = iXLFact * iYTFact / 256;
    int iLLFact = iXLFact * iYBFact / 256;
    int iURFact = iXRFact * iYTFact / 256;
    int iLRFact = iXRFact * iYBFact / 256;
    iDenom += (iULFact+iLLFact+iURFact+iLRFact);

    LOGASSERT(iXLFact >= 0 && iXLFact <= 256);
    LOGASSERT(iXRFact >= 0 && iXRFact <= 256);
    LOGASSERT(iYTFact >= 0 && iYTFact <= 256);
    LOGASSERT(iYBFact >= 0 && iYBFact <= 256);

    DWORD dwSumR = 0;
    DWORD dwSumG = 0;
    DWORD dwSumB = 0;

    //
    // Corners
    //
    HL_PIXEL *pUL = Pixel(iXL, iYT);
    HL_PIXEL *pLL = Pixel(iXL, iYB);
    HL_PIXEL *pUR = Pixel(iXR, iYT);
    HL_PIXEL *pLR = Pixel(iXR, iYB);

    dwSumR += ((*pUL & 0xF800) * iULFact);
    dwSumG += ((*pUL & 0x07E0) * iULFact);
    dwSumB += ((*pUL & 0x001F) * iULFact);

    dwSumR += ((*pLL & 0xF800) * iLLFact);
    dwSumG += ((*pLL & 0x07E0) * iLLFact);
    dwSumB += ((*pLL & 0x001F) * iLLFact);

    dwSumR += ((*pUR & 0xF800) * iURFact);
    dwSumG += ((*pUR & 0x07E0) * iURFact);
    dwSumB += ((*pUR & 0x001F) * iURFact);

    dwSumR += ((*pLR & 0xF800) * iLRFact);
    dwSumG += ((*pLR & 0x07E0) * iLRFact);
    dwSumB += ((*pLR & 0x001F) * iLRFact);

    int iX = 0;
    int iY = 0;

    //
    // Verticals and field
    //
    if (iDYMid > 0)
    {
        HL_PIXEL *pL = Pixel(iXL, iYT+1);
        HL_PIXEL *pR = Pixel(iXR, iYT+1);
        for (iY = 0; iY < iDYMid; iY++)
        {
            dwSumR += ((*pL & 0xF800) * iXLFact);
            dwSumG += ((*pL & 0x07E0) * iXLFact);
            dwSumB += ((*pL & 0x001F) * iXLFact);

            dwSumR += ((*pR & 0xF800) * iXRFact);
            dwSumG += ((*pR & 0x07E0) * iXRFact);
            dwSumB += ((*pR & 0x001F) * iXRFact);
        
            if (iDXMid > 0)
            {
                HL_PIXEL *pF = Pixel(iXL+1, iYT+1+iY);
                for (iX = 0; iX < iDXMid; iX++)
                {
                    dwSumR += ((*pF & 0xF800) << 8);
                    dwSumG += ((*pF & 0x07E0) << 8);
                    dwSumB += ((*pF & 0x001F) << 8);
                    pF++; 
                }
            }

            pL += m_iDX;
            pR += m_iDX;
        }
    }


    //
    // Horizontals
    //
    if (iDXMid > 0)
    {
        HL_PIXEL *pT = Pixel(iXL+1, iYT);
        HL_PIXEL *pB = Pixel(iXL+1, iYB);
        for (iX = 0; iX < iDXMid; iX++)
        {
            dwSumR += ((*pT & 0xF800) * iYTFact);
            dwSumG += ((*pT & 0x07E0) * iYTFact);
            dwSumB += ((*pT & 0x001F) * iYTFact);

            dwSumR += ((*pB & 0xF800) * iYBFact);
            dwSumG += ((*pB & 0x07E0) * iYBFact);
            dwSumB += ((*pB & 0x001F) * iYBFact);
        
            pT++;
            pB++;
        }
    }

    dwSumR = ((dwSumR / iDenom) & 0xF800);
    dwSumG = ((dwSumG / iDenom) & 0x07E0);
    dwSumB = dwSumB / iDenom;
    return (HL_PIXEL)(dwSumR|dwSumG|dwSumB);
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateScaledInstance(
    SIZE                        szNew,
    BOOL                        bSmooth,
    int                         iAttribs )

/*============================================================================*/
{
    int iDXNew = szNew.cx;
    int iDYNew = szNew.cy;

    int iScaleX = ImageDX() / szNew.cx;
    int iScaleY = DY() / szNew.cy;

    if (iScaleX >= 2 || iScaleY >= 2)
    {
        CHLDibDC *pRet = CreateBicubicScale(szNew.cx, szNew.cy, iAttribs);
        return pRet;
    }

    CHLDibDC *pNew = new CHLDibDC(iDXNew, iDYNew, 0, 0, MEMORY_SYSTEM, iAttribs);

    int iDX = ImageDX();
    int iDY = DY();

    if (bSmooth)
    {
        CHLDibDC *pSrc = this;

        //
        // No need to do this over and over, just make up some LUTs
        //
        int *piXFloor = new int[iDXNew];
        int *piXCeil  = new int[iDXNew];
        int *piFractionX = new int[iDXNew];
        int *piOneMinusX = new int[iDXNew];

        for (int x = 0; x < iDXNew; ++x)
        {
            // Setup
            int floor_x = x * iDX / iDXNew;
            int ceil_x = floor_x + 1;
            if (ceil_x >= iDX) 
                ceil_x = floor_x;

            int iFractionX = x * 100 * iDX / iDXNew - (floor_x * 100);
            int iOneMinusX = 100 - iFractionX;        
            piXFloor[x] = floor_x;
            piXCeil[x]  = ceil_x;
            piFractionX[x]  = iFractionX;
            piOneMinusX[x]  = iOneMinusX;
        }

        for (int y = 0; y < iDYNew; ++y)
        {
            int floor_y = y * iDY / iDYNew;
            int ceil_y = floor_y + 1;
            if (ceil_y >= iDY) 
                ceil_y = floor_y;

            int iFractionY = y * 100 * iDY / iDYNew - (floor_y * 100);
            int iOneMinusY = 100 - iFractionY;        

            HL_PIXEL *pOut = pNew->Pixel(0, y);

            for (int x = 0; x < iDXNew; ++x)
            {
                // Setup
                int floor_x     = piXFloor[x];
                int ceil_x      = piXCeil[x];
                int iFractionX  = piFractionX[x];
                int iOneMinusX  = piOneMinusX[x];

                HL_PIXEL c1 = *pSrc->Pixel(floor_x, floor_y);
                HL_PIXEL c2 = *pSrc->Pixel(ceil_x, floor_y);
                HL_PIXEL c3 = *pSrc->Pixel(floor_x, ceil_y);
                HL_PIXEL c4 = *pSrc->Pixel(ceil_x, ceil_y);

                BYTE byR1 = GetRValueHLPIXEL(c1);    BYTE byG1 = GetGValueHLPIXEL(c1);    BYTE byB1 = GetBValueHLPIXEL(c1);
                BYTE byR2 = GetRValueHLPIXEL(c2);    BYTE byG2 = GetGValueHLPIXEL(c2);    BYTE byB2 = GetBValueHLPIXEL(c2);
                BYTE byR3 = GetRValueHLPIXEL(c3);    BYTE byG3 = GetGValueHLPIXEL(c3);    BYTE byB3 = GetBValueHLPIXEL(c3);
                BYTE byR4 = GetRValueHLPIXEL(c4);    BYTE byG4 = GetGValueHLPIXEL(c4);    BYTE byB4 = GetBValueHLPIXEL(c4);

                // Blue
                UINT b1     = (iOneMinusX * byB1 + iFractionX * byB2); // b1 * 100
                UINT b2     = (iOneMinusX * byB3 + iFractionX * byB4); // b2 * 100
                UINT blue   = (iOneMinusY * b1   + iFractionY * b2 ) / 10000;

                // Green
                b1          = (iOneMinusX * byG1 + iFractionX * byG2);
                b2          = (iOneMinusX * byG3 + iFractionX * byG4);
                UINT green  = (iOneMinusY * b1   + iFractionY * b2) / 10000;

                // Red
                b1          = (iOneMinusX * byR1 + iFractionX * byR2);  // 0-25500
                b2          = (iOneMinusX * byR3 + iFractionX * byR4);  // 0-25500
                UINT red    = (iOneMinusY * b1   + iFractionY * b2) / 10000;    // 0-

                *pOut       = RGBtoHLPIXEL(red, green, blue);
                pOut++;
            }
        }

        delete [] piXFloor;
        delete [] piXCeil;
        delete [] piFractionX;
        delete [] piOneMinusX;

        if (pSrc != this)
            delete pSrc;
    }
    else
    {
        int *iXScale = new int[iDXNew];
        for (int iX = 0; iX < iDXNew; iX++)
        {
            int iXThis = iX * DX() / iDXNew;
            iXThis = __max(0, iXThis);
            iXThis = __min(DX()-1, iXThis);
            iXScale[iX] = iXThis;
        }

        for (int iY = 0; iY < iDYNew; iY++)
        {
            int iYThis = __max(iY * DY() / iDYNew, 0);
            iYThis = __min(iYThis, DY()-1);
        
            HL_PIXEL *pIn  = Pixel(0, iYThis);
            HL_PIXEL *pOut = pNew->Pixel(0, iY);

            for (int iX = 0; iX < iDXNew; iX++)
            {
                *pOut = *(pIn + iXScale[iX]);
                pOut++;
            }

        }
        delete [] iXScale;
    }


    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::CreateScaleDIV2()

/*============================================================================*/
{
    int iDXNew = m_iImageDX / 2;
    int iDYNew = m_iDY / 2;
    CHLDibDC *pRet = new CHLDibDC(iDXNew, iDYNew, 0, 0, MEMORY_SYSTEM);
    for (int iY = 0; iY < iDYNew; iY++)
    {
        HL_PIXEL *pTgt = pRet->Pixel(0, iY);

        HL_PIXEL *pUL = Pixel(0, iY*2);
        HL_PIXEL *pLL = Pixel(0, iY*2+1);
        HL_PIXEL *pUR = pUL+1;
        HL_PIXEL *pLR = pLL+1;

        for (int iX = 0; iX < iDXNew; iX++)
        {
            int iRSum = (*pUL & 0xF800)+(*pUR & 0xF800)+(*pLL & 0xF800)+(*pLR & 0xF800);
            int iGSum = (*pUL & 0x07E0)+(*pUR & 0x07E0)+(*pLL & 0x07E0)+(*pLR & 0x07E0);
            int iBSum = (*pUL & 0x001F)+(*pUR & 0x001F)+(*pLL & 0x001F)+(*pLR & 0x001F);
            int iR = (iRSum >> 2) & 0xF800;
            int iG = (iGSum >> 2) & 0x07E0;
            int iB = (iBSum >> 2);
            *pTgt = (HL_PIXEL)iR|iG|iB;
            pTgt++;
            pUL += 2;
            pLL += 2;
            pUR += 2;
            pLR += 2;
        }
    }

    return pRet;
}
#ifndef IPHONE
/*============================================================================*/

    void                        CHLDibDC::SetOrigin(
    CHLWindow                   *pWnd )

/*============================================================================*/
{
    CHLDC::SetOrigin(pWnd);
}
#endif
/*============================================================================*/

    void                        CHLDibDC::SetOrigin(
    int                         iXLeft,
    int                         iYTop )

/*============================================================================*/
{
    iXLeft -= m_iXOrgAbs;
    iYTop -= m_iYOrgAbs;
    CHLDC::SetOrigin(iXLeft, iYTop);
}
/*============================================================================*/

    POINT                       CHLDibDC::GetOriginAbs()

/*============================================================================*/
{
    POINT pt;
    pt.x = m_iXOrgAbs + m_iXLeft;
    pt.y = m_iYOrgAbs + m_iYTop;
    return pt;
}
/*============================================================================*/

    CAlphaDIB                   *CHLDibDC::AlphaDIB()

/*============================================================================*/
{
    return NULL;
}
/*============================================================================*/

    int                         CHLDibDC::DXPitch()

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
        return m_pParentDIB->DX();
    return m_iDX;
}
/*============================================================================*/

    int                         CHLDibDC::DX()

/*============================================================================*/
{
    return m_iDX;
}
/*============================================================================*/

    int                         CHLDibDC::DY()

/*============================================================================*/
{
    return m_iDY; 
}
/*============================================================================*/

    HL_PIXEL                    *CHLDibDC::Bits()

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        return m_pParentDIB->Pixel(m_iXLeft, m_iYTop);
    }

    #ifdef HLDDRAW
        if (m_pBits != NULL)
            return m_pBits;

        if (m_pSurface != NULL)
        {
            START_TIMER(GDI_DIBDCCONST);
            if (m_hDC != NULL)
            {
                m_pSurface->ReleaseDC(m_hDC);
                m_hDC = NULL;
            }

            CHLString sTrace;
            sTrace.Format(_T("WARNING: Locking Video mem %dx%d\r\n"), m_iDX, m_iDY);
            TraceWrap(sTrace);

            HL_SURFACEDESC surfaceDesc;
            memset(&surfaceDesc, 0, sizeof(HL_SURFACEDESC));
            surfaceDesc.dwSize      = sizeof(HL_SURFACEDESC);
            surfaceDesc.dwFlags = DDSD_HEIGHT|DDSD_WIDTH|DDSD_LPSURFACE;

            #ifdef HL_DDRAWBASIC
                HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAITNOTBUSY, NULL);
            #else
                HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
            #endif

            if (hr != S_OK)
            {
                TraceWrap(_T("Lock Surface Failed Bits"));

                if (hr == DDERR_SURFACELOST)
                {
                    g_pGDIServer->GetPrimary()->Restore();
                    m_pSurface->Restore();
                }

                if (hr == DDERR_SURFACEBUSY)
                    ReleaseBits();

                #ifdef HL_DDRAWBASIC
                    hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAITNOTBUSY, NULL);
                #else
                    hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
                #endif
            }

            m_pBits = (HL_PIXEL*) surfaceDesc.lpSurface;
            #ifdef HL_PIXEL32
                m_iDX = surfaceDesc.lPitch / 4;
            #else
                m_iDX = surfaceDesc.lPitch / 2;
            #endif 

            LOGASSERT(m_pBits != NULL);
            END_TIMER(GDI_DIBDCCONST);
        }
    #endif

    return m_pBits;
}
/*============================================================================*/

    void                        CHLDibDC::ReleaseBits()

/*============================================================================*/
{
    #ifdef HLDDRAW
        START_TIMER(GDI_DIBDCCONST);
        if (m_pSurface != NULL)
        {
            if (m_hDC != NULL)
            {
                if (m_hDC != NULL)
                    m_pSurface->ReleaseDC(m_hDC);
            }

            if (m_pBits != NULL)
                m_pSurface->Unlock(NULL);

            m_pBits = NULL;
            m_hDC   = NULL;
        }
        END_TIMER(GDI_DIBDCCONST);
    #endif
}
/*============================================================================*/

    HL_PIXEL                    *CHLDibDC::Pixel(
    int                         iX,
    int                         iY )

/*============================================================================*/
{
    if (m_pBits == NULL)
    {
        Bits();
    }


    if (m_pParentDIB != NULL)
    {
        return m_pParentDIB->Pixel(iX + m_iXLeft, iY + m_iYTop);
    }


    LOGASSERT(m_pBits != NULL);
    LOGASSERT((iX >= 0) && (iX < m_iDX));
    LOGASSERT((iY >= 0) && (iY < m_iDY));

    return m_pBits + m_iDX * iY + iX;
}
#ifndef IPHONE
/*============================================================================*/

    HDC                         CHLDibDC::GetHDC()

/*============================================================================*/
{
    #ifdef DEBUG1
    m_bHDC = TRUE;
    #endif

    if (m_pParentDIB != NULL)
    {
        return m_pParentDIB->GetHDC();
    }

    if (m_hDC != NULL)
    {
        return m_hDC;
    }

    START_TIMER(GDI_DIBDCCONST);

    #ifdef HL_ARMV4IC
    TraceWrap(_T("WARNING GetHDC()"));
    #endif

    #ifdef HLDDRAW
        if (m_pSurface != NULL)
        {
            if (m_pBits != NULL)
            {
                m_pSurface->Unlock(NULL);
                m_pBits = NULL;
            }

            m_pSurface->GetDC(&m_hDC);

            CHLRect *pClip = CreateActiveClip();
            SetActiveClip(pClip);
            delete pClip;
        }
    #endif

    // In case we came from a parent
#ifndef IPHONE
    m_iDX = DWORDAlign(m_iDX);
#endif

    if (m_hDC == NULL)
        m_hDC = CreateCompatibleDC(NULL);


    SetBkMode(m_hDC, TRANSPARENT);

    #ifdef HLDDRAW
        if (m_pSurface != NULL)
        {
            END_TIMER(GDI_DIBDCCONST);
            return m_hDC;
        }
    #endif

    BYTE byInfo[sizeof(BITMAPINFOHEADER)+3*(sizeof(DWORD))];
    BITMAPINFO *pInfo = (BITMAPINFO*) byInfo;
    pInfo->bmiHeader.biSize           = sizeof(BITMAPINFOHEADER);// + 3 * sizeof(DWORD);
    pInfo->bmiHeader.biWidth          = m_iDX;
    pInfo->bmiHeader.biHeight         = -m_iDY;
    pInfo->bmiHeader.biPlanes         = 1;

    #ifdef HL_PIXEL32
        pInfo->bmiHeader.biBitCount       = 32;
        pInfo->bmiHeader.biCompression    = BI_RGB;
        pInfo->bmiHeader.biSizeImage      = 4 * m_iDX * m_iDY;
        pInfo->bmiHeader.biXPelsPerMeter  = 10;
        pInfo->bmiHeader.biYPelsPerMeter  = 10;
        pInfo->bmiHeader.biClrUsed        = 0;
        pInfo->bmiHeader.biClrImportant   = 0;
    #else
        pInfo->bmiHeader.biBitCount       = 16;
        pInfo->bmiHeader.biCompression    = BI_BITFIELDS;
        pInfo->bmiHeader.biSizeImage      = 2 * m_iDX * m_iDY;
        pInfo->bmiHeader.biXPelsPerMeter  = 10;
        pInfo->bmiHeader.biYPelsPerMeter  = 10;
        pInfo->bmiHeader.biClrUsed        = 3;
        pInfo->bmiHeader.biClrImportant   = 0;

        //
        // This sets the bits to 5/6/5, which seems to be consistent on all the
        // CE tablets that we have seen.
        // Note that a standard 16 bit DIB section will be created with 5/5/5 if
        // the color bits are not set like this.
        //
        BYTE *pColors = byInfo + sizeof(BITMAPINFOHEADER);
        DWORD *pQ = (DWORD*) pColors;

        pQ[0] = (DWORD) 0xF800;     // 1111100000000000
        pQ[1] = (DWORD) 0x07E0;     // 0000011111100000
        pQ[2] = (DWORD) 0x001F;     // 0000000000011111
    #endif

    HL_PIXEL *pOldBits = m_pBits;

    m_pBits = NULL;
    m_hOldBMP = NULL;
    m_hCurBMP = NULL;
    if (m_iDX > 0 && m_iDY > 0)
    {
        m_hCurBMP = CreateDIBSection(m_hDC, pInfo, DIB_RGB_COLORS, (void**)&m_pBits, NULL, 0);

        m_hOldBMP = (HBITMAP) SelectObject(m_hDC, m_hCurBMP);
        LOGASSERT(m_pBits != NULL);

        if (pOldBits != NULL && m_pParentDIB == NULL)
        {
            //
            // If we get in here, we allocated the wrong type of DIB, this can take time if we
            // do this a lot, try to avoid allocating the wrong type
            //
        
            PIXEL_COPY(m_pBits, pOldBits, DX()*DY());
            HLFree(pOldBits);
            pOldBits = NULL;
        }

        if (!m_ClipStack.IsEmpty())
        {
            CHLRect *pRect = (CHLRect*)m_ClipStack.GetHead();
            SetActiveClip(pRect);    
        }
    }

    if (m_pParentDIB != NULL)
    {
        CHLDibDC *pParent = m_pParentDIB;
        m_pParentDIB = NULL;
        int iXL = m_iXLeft;
        int iYT = m_iYTop;
        m_iXLeft = 0;
        m_iYTop = 0;
        BltFromDibDC(0, 0, pParent, iXL, iYT, DX(), DY()); 
    }

    g_iDCCount++;

    END_TIMER(GDI_DIBDCCONST);
    return m_hDC;
}
/*============================================================================*/

    void                        CHLDibDC::ReleaseGDI()

/*============================================================================*/
{
    //
    // HDC may be reobtained to get DIB to screen, not required for win32 or hareware blt though
    //
    #ifdef HL_WIN32
        if (m_hDC == NULL || m_pBits == NULL)
            return;

        HL_PIXEL *pTemp = m_pBits;
        int iNBytes = m_iDX * m_iDY * sizeof(HL_PIXEL);
        m_pBits = (HL_PIXEL*)HLAlloc(iNBytes);
        memcpy(m_pBits, pTemp, m_iDX*m_iDY*sizeof(HL_PIXEL));
    
        SelectObject(m_hDC, m_hOldBMP);
        DeleteDC(m_hDC);
        DeleteObject(m_hCurBMP);

        m_hOldBMP = NULL;
        m_hDC = NULL;
        m_hCurBMP = NULL;
    #endif

    g_iDCCount--;

}
#endif
/*============================================================================*/

    void                        CHLDibDC::DrawLine(
    COLORREF                    rgb,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iPenSize )

/*============================================================================*/
{
    int iDX = iX2-iX1;
    int iDY = iY2-iY1;
    if (iDX != 0 && iDY != 0)
    {
        CHLDC::DrawLine(rgb, iX1, iY1, iX2, iY2, iPenSize);
        return;
    }

    if (iPenSize != 1)
    {
        int iYT = __min(iY1, iY2);
        int iXL = __min(iX1, iX2);
        if (iDX == 0)
        {
            int iDY = abs(iY2-iY1)+1;
            FillRect(rgb, iXL-iPenSize/2, iYT, iPenSize, iDY);
        }
        else
        {
            int iDX = abs(iX2-iX1)+1;
            FillRect(rgb, iXL, iYT-iPenSize/2, iDX, iPenSize);
        }
        return;
    }

    iX1 += m_iXLeft;
    iY1 += m_iYTop;
    iX2 += m_iXLeft;
    iY2 += m_iYTop;

    HL_PIXEL wSet = RGBTOHLPIXEL(rgb);
    CHLRect rClip = LocalClipRect();
    if (iDX == 0)
    {
        // Vertical
        if (iX1 < rClip.XLeft() || iX1 > rClip.XRight())
            return;
        int iY_0 = __max(__min(iY1, iY2), rClip.YTop());
        int iY_1 = __min(__max(iY1, iY2), rClip.YBottom());
        if (iY_1 < iY_0)
            return;
        HL_PIXEL *pPIX = Pixel(iX1, iY_0);
        for (int iY = iY_0; iY <= iY_1; iY++)
        {
            *pPIX = wSet;
            pPIX += m_iDX;
        }
    }
    else
    {
        // Horizontal
        if (iY1 < rClip.YTop() || iY1 > rClip.YBottom())
            return;
        int iX_0 = __max(__min(iX1, iX2), rClip.XLeft());
        int iX_1 = __min(__max(iX1, iX2), rClip.XRight());
        if (iX_1 < iX_0)
            return;
        HL_PIXEL *pPIX = Pixel(iX_0, iY1);
        for (int iX = iX_0; iX <= iX_1; iX++)
        {
            *pPIX = wSet;
            pPIX++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::DrawAALine(
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iPenSize )

/*============================================================================*/
{
    CHLRect rClip = LocalClipRect();
    
    iX1 += m_iXLeft;
    iY1 += m_iYTop;
    iX2 += m_iXLeft;
    iY2 += m_iYTop;

    // Basic bounds check
    if (iX2 > rClip.XRight() && iX1 > rClip.XRight())
        return;
    if (iX1 < rClip.XLeft() && iX2 < rClip.XLeft())
        return;
    if (iY2 > rClip.YBottom() && iY1 > rClip.YBottom())
        return;
    if (iY1 < rClip.YTop() && iY2 < rClip.YTop())
        return;

    if (iX2 < iX1)
    {
        int iTemp = iX2;
        iX2 = iX1;
        iX1 = iTemp;
        iTemp = iY2;
        iY2 = iY1;
        iY1 = iTemp;
    }

    if (iX1 < rClip.XLeft())
    {
        POINT ptL = XIntersect(rClip.XLeft(), iX1, iY1, iX2, iY2);
        iX1 = ptL.x;
        iY1 = ptL.y;
    }
    if (iX2 > rClip.XRight())
    {
        POINT ptR = XIntersect(rClip.XRight(), iX1, iY1, iX2, iY2);
        iX2 = ptR.x;
        iY2 = ptR.y;
    }

    int iOffset = iX1 + (iY1 * DX());
    int iDX = iX2-iX1;
    int iDY = iY2-iY1;
    /* By switching to (u,v), we combine all eight octants */

    int iDU = 0;
    int iDV = 0;
    int iU = 0;
    int iV = 0;
    int iUInc = 0;
    int iVInc = 0;
    if (abs(iDX) > abs(iDY))
    {
	    /* Note: If this were actual C, these integers would be lost
	    * at the closing brace.  That's not what I mean to do.  Do what
	    * I mean. */
	    iDU = abs(iDX);
	    iDV = abs(iDY);
	    iU = iX2;
	    iV = iY2;
	    iUInc = 1;
        iVInc = DX();
	    if (iDX < 0) iUInc = -iUInc;
	    if (iDY < 0) iVInc = -iVInc;
    }
    else
    {
	    iDU = abs(iDY);
	    iDV = abs(iDX);
	    iU = iY2;
        iV = iX2;
	    iUInc = DX();
	    iVInc = 1;
	    if (iDY < 0) iUInc = -iUInc;
	    if (iDX < 0) iVInc = -iVInc;

        if (iDU == 0)
            iDU = 1;
    }

    int uend = iU + iDU;
    int d = (2 * iDV) - iDU;	    /* Initial value as in Bresenham's */
    int incrS = 2 * iDV;	/* ?d for straight increments */
    int incrD = 2 * (iDV - iDU);	/* ?d for diagonal increments */
    int twovdu = 0;	/* Numerator of distance; starts at 0 */


    int iInvD1000 = 100000 / (2 * iSqrt(iDU*iDU + iDV * iDV));
    int iInvD2DU1000 = 2 * (iDU * iInvD1000);


//    double invD = 1.0 / (2.0*sqrt(iDU*iDU+ iDV*iDV));   /* Precomputed inverse denominator */
//    double invD2du = 2.0 * (iDU*invD);   /* Precomputed constant */


    do
    {
	    /* Note: this pseudocode doesn't ensure that the address is
	    * valid, or that it even represents a pixel on the same side of
	    * the screen as the adjacent pixel */

	    SetMaskAlpha(iOffset,         (twovdu*iInvD1000)/100);
	    SetMaskAlpha(iOffset + iVInc, (iInvD2DU1000 - twovdu * iInvD1000)/100);
	    SetMaskAlpha(iOffset - iVInc, (iInvD2DU1000 + twovdu * iInvD1000)/100);

/*
	    DrawPixelD(iOffset,         wRA, wGA, wBA, twovdu*invD);
	    DrawPixelD(iOffset + iVInc, wRA, wGA, wBA, invD2du - twovdu*invD);
	    DrawPixelD(iOffset - iVInc, wRA, wGA, wBoA, invD2du + twovdu*invD);
*/
	    if (d < 0)
	    {
	        /* choose straight (u direction) */
	        twovdu = d + iDU;
	        d = d + incrS;
	    }
	    else
	    {
	        /* choose diagonal (u+v direction) */
	        twovdu = d - iDU;
	        d = d + incrD;
	        iV = iV+1;
	        iOffset = iOffset + iVInc;
	    }
	    iU = iU+1;
	    iOffset = iOffset + iUInc;
    } while (iU <= uend);
}
/*============================================================================*/

    void                        CHLDibDC::DrawAlphaLine(
    COLORREF                    rgb,
    int                         iAlpha,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 )

/*============================================================================*/
{
    InitMask(0x00);
    DrawAALine(iX1, iY1, iX2, iY2);
    
    BYTE *pA = m_pMask;
    HL_PIXEL *pOut = m_pBits;
    int iNPIX = m_iDX * m_iDY;
    HL_PIXEL wSrc = RGBTOHLPIXEL(rgb);
    for (int iPIX = 0; iPIX < iNPIX; iPIX++)
    {   
        DWORD byA = (*pA * iAlpha / 255);
        
        if (byA != 0)
        {
            DWORD byAlphaRB = byA * 32 / 255;
            DWORD byAlphaG  = byA * 64 / 255;
            DWORD _byAlphaRB  = 32-byAlphaRB;
            DWORD _byAlphaG   = 64-byAlphaG;

            HL_PIXEL wTgt = *pOut;
            DWORD wRB_S = wSrc & 0x0000F81F;
            DWORD wG_S  = wSrc & 0x000007E0;
            DWORD wRB_T = wTgt & 0x0000F81F;
            DWORD wG_T  = wTgt & 0x000007E0;

            DWORD dwRB_Out = (((wRB_S * byAlphaRB + wRB_T * _byAlphaRB) ) >> 5) & 0xF81F;
            DWORD dwG_Out  = (((wG_S  * byAlphaG  + wG_T * _byAlphaG)   ) >> 6) & 0x07E0;
            *pOut = (WORD)(dwRB_Out | dwG_Out);
        }

        pA++;
        pOut++;

    }
}
/*============================================================================*/

    void                        CHLDibDC::CommitAALines(
    COLORREF                    rgb,
    int                         iAlpha )

/*============================================================================*/
{
    HL_PIXEL wSet = RGBTOHLPIXEL(rgb);
    DWORD dwRA = ((wSet & 0x0000F800) >> 11);
    DWORD dwGA = ((wSet & 0x000007E0) >> 5);
    DWORD dwBA = wSet & 0x0000001F;

    BYTE *pA = m_pMask;
    HL_PIXEL *pOut = m_pBits;
    int iNPIX = m_iDX * m_iDY;
    for (int iPIX = 0; iPIX < iNPIX; iPIX++)
    {   
        DWORD byA = (*pA * iAlpha / 255);
        
        switch (byA)
        {
        case 0:
        
        default:
            {
                //byA = __min(255, byA * 2);
                DWORD dwRAThis = dwRA * byA / 255;
                DWORD dwGAThis = dwGA * byA / 255;
                DWORD dwBAThis = dwBA * byA / 255;
                
                DWORD wTgt = *pOut;
                DWORD wRB = ((wTgt & 0x0000F800) >> 11);
                DWORD wGB = ((wTgt & 0x000007E0) >> 5);
                DWORD wBB = wTgt & 0x0000001F;
        
                DWORD wR = __min(31, wRB+dwRAThis);
                DWORD wG = __min(63, wGB+dwGAThis);
                DWORD wB = __min(31, wBB+dwBAThis);

                *pOut = (WORD)((wR<<11)|(wG<<5)|wB);

/*                DWORD wROut = (((dwRA * byAlphaRB + _byAlphaRB * wRB) >> 5) & 0x0000F800);
                DWORD wGOut = (((dwGA * byAlphaG  + _byAlphaG  * wGB) >> 6) & 0x000007E0);
                DWORD wBOut =  ((dwBA * byAlphaRB + _byAlphaRB * wBB) >> 5);
                *pOut = (WORD)(wROut | wGOut  | wBOut);
*/
            }
            break;
        }

        pA++;
        pOut++;

    }
}
/*============================================================================*/

    void                        CHLDibDC::FlipRectY(
    CHLRect                     rROI )

/*============================================================================*/
{
    int iXDst = rROI.XLeft();
    int iYDst = rROI.YTop();
    int iDX = rROI.DX();
    int iDY = rROI.DY();
    int iXSrc = 0;
    int iYSrc = 0;
    
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

//        HL_PIXEL *pTemp = (HL_PIXEL*)malloc(sizeof(HL_PIXEL*) * iDX * iDY);
    HL_PIXEL *pTemp = new HL_PIXEL[iDX];
    int iY = 0;
    for (iY = 0; iY < iDY / 2; iY++)
    {
        HL_PIXEL *pT  = Pixel(iXDst, iY+iYDst);
        HL_PIXEL *pB  = Pixel(iXDst, iYDst+iDY-iY-1);
        memcpy(pTemp, pT, iDX * sizeof(HL_PIXEL));
        memcpy(pT, pB, iDX * sizeof(HL_PIXEL));
        memcpy(pB, pTemp, iDX * sizeof(HL_PIXEL));
    }
    
    delete [] pTemp;
}
/*============================================================================*/

    void                        CHLDibDC::DrawRoundRect(
    COLORREF                    rgb,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad )

/*============================================================================*/
{
    if (iRad <= 0)
    {
        DrawRect(rgb, iXLeft, iYTop, iDX, iDY);
        return;
    }

    iRad = __min(iRad, iDX / 2);
    iRad = __min(iRad, iDY / 2);
    iRad++;
    DrawLine(rgb, iXLeft+iRad,  iYTop,          iXLeft + iDX - iRad,   iYTop);
    DrawLine(rgb, iXLeft+iRad,  iYTop+iDY-1,    iXLeft + iDX - iRad,   iYTop+iDY-1);
    DrawLine(rgb, iXLeft,       iYTop+iRad,     iXLeft,                iYTop + iDY - iRad);
    DrawLine(rgb, iXLeft+iDX-1, iYTop+iRad,     iXLeft+iDX-1,          iYTop + iDY - iRad);
    CPointList Points;
    iRad--;
    CHLDibDC::CalcArcPoints(&Points, iRad, FALSE, RADIUS_ON);
    HL_PIXEL wSet = RGBTOHLPIXEL(rgb);

    CHLRect rClip = LocalClipRect();
    int iXMin = rClip.XLeft();
    int iYMin = rClip.YTop();
    int iXMax = rClip.XRight();
    int iYMax = rClip.YBottom();

    iXLeft += m_iXLeft;
    iYTop += m_iYTop;

    for (int iPt = 0; iPt < Points.NPoints(); iPt++)
    {
        int iX = iRad  - Points.X(iPt);        
        int iY = iRad  - Points.Y(iPt);

        int iYT = iYTop + iY;
        int iYB = iYTop + iDY - iY - 1;
        int iXL = iXLeft + iX;
        int iXR = iXLeft + iDX - iX - 1;

        if (iYT >= iYMin && iYT <= iYMax)
        {
            if (iXL >= iXMin && iXL <= iXMax)
                *(Pixel(iXL,    iYT)) = wSet;
            if (iXR >= iXMin && iXR <= iXMax)
                *(Pixel(iXR,    iYT)) = wSet;
        }

        if (iYB >= iYMin && iYB <= iYMax)
        {
            if (iXL >= iXMin && iXL <= iXMax)
                *(Pixel(iXL,          iYB)) = wSet;
            if (iXR >= iXMin && iXR <= iXMax)
                *(Pixel(iXR,    iYB)) = wSet;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pList )

/*============================================================================*/
{
    int iNRects = pList->NRects();
    for (int i = 0; i < iNRects; i++)
    {
        FillRect(rgbFill, pList->X(i), pList->Y(i), pList->DX(i), pList->DY(i));
    }

    return;
}
/*============================================================================*/

    void                        CHLDibDC::FillRect(
    COLORREF                    rgbFill,
    CHLRect                     rect )

/*============================================================================*/
{
    FillRect(rgbFill, rect.XLeft(), rect.YTop(), rect.DX(), rect.DY());
}
/*============================================================================*/

    void                        CHLDibDC::FillRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    #ifndef HLDDRAW
        if (m_hDC != NULL)
        {
            CHLDC::FillRect(rgbFill, iXLeft, iYTop, iDX, iDY);
            return;
        }
    #endif    

    START_TIMER(GDI_FILLRECT);

    iXLeft += m_iXLeft;
    iYTop += m_iYTop;

    if (m_pParentDIB != NULL)
    {
        m_pParentDIB->FillRect(rgbFill, iXLeft, iYTop, iDX, iDY);
    }

    CHLRect rClip = LocalClipRect();

    if (iXLeft < rClip.XLeft())
    {
        iDX -= (rClip.XLeft()-iXLeft);
        iXLeft = rClip.XLeft();
    }
    if (iYTop < rClip.YTop())
    {
        iDY -= (rClip.YTop()-iYTop);
        iYTop = rClip.YTop();
    }
    iDX = __min(rClip.XRight()-iXLeft+1, iDX);
    iDY = __min(rClip.YBottom()-iYTop+1, iDY);
    if (iDX < 1 || iDY < 1)
    {
        END_TIMER(GDI_FILLRECT);
        return;
    }

    #ifdef HLDDRAW
        if (m_pSurface != NULL)
        {
            ReleaseBits();

            // Use a color fill blit to fill the primary surface with 
            // the color key. 
            DDBLTFX ddbltfx;
            ZeroMemory( &ddbltfx, sizeof( ddbltfx ) ); 
            ddbltfx.dwSize = sizeof( ddbltfx ); 
            ddbltfx.dwFillColor = RGBTOHLPIXEL(rgbFill); 
            ddbltfx.dwROP       = PATCOPY;
            RECT rect;
            rect.left   = iXLeft;
            rect.top    = iYTop;            
            rect.right  = __min(ImageDX(), iXLeft + iDX);
            rect.bottom = __min(DY(), iYTop + iDY);

            LOGASSERT(m_pBits == NULL);            
            LOGASSERT(m_hDC == NULL);
    
            #ifdef HL_DDRAWBASIC
                HRESULT res = m_pSurface->Blt( &rect, NULL, NULL, DDBLT_COLORFILL, &ddbltfx );
            #else
                HRESULT res = m_pSurface->Blt( &rect, NULL, &rect, DDBLT_COLORFILL | DDBLT_WAIT, &ddbltfx );
            #endif

            if (res != S_OK)
            {
                CHLString sTrace;
                sTrace.Format(_T("DIBDC ERROR Line = %d RES = %08X"), __LINE__, res);
                TraceWrap(sTrace);
            }
           
            END_TIMER(GDI_FILLRECT);
            return;
        }
    #endif

    HL_PIXEL *pSet = Pixel(iXLeft, iYTop);
    HL_PIXEL wSet = RGBTOHLPIXEL(rgbFill);

    if (pSet == NULL)
        return;

#ifdef HL_PIXEL32
    HL_PIXEL *pRow = Pixel(iXLeft, iYTop);

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSet = pRow;        
        for (int iX = 0; iX < iDX; iX++)
        {
            *pSet = wSet;
            pSet++;
        }
        pRow += m_iDX;
    }
#else
    if ((wSet >> 8) == (wSet & 0xFF))
    {
        HL_PIXEL *pSet = Pixel(iXLeft, iYTop);

        BYTE bySet = wSet & 0xFF;
        for (int iY = 0; iY < iDY; iY++)
        {
#ifdef DEBUG
            CheckPointer(pSet);
            CheckPointer(pSet+iDX-1);
#endif
            memset(pSet, bySet, iDX * sizeof(HL_PIXEL));
            pSet += m_iDX;
        }
    }
    else if (iDX > 8)
    {
        int iDXL = iXLeft % 2;
        int iDX2 = (iDX-iDXL) / 2;
        int iDXR = iDX - iDXL - 2 * iDX2;
        DWORD dwSet = wSet | (wSet << 16);        

        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pSet = Pixel(iXLeft, iYTop+iY);
            if (iDXL)
            {
                *pSet = wSet;
                pSet++;
            }            
            DWORD *pdwSet = (DWORD*)pSet;
            for (int iX = 0; iX < iDX2; iX++)
            {
                *pdwSet = dwSet;
                pdwSet++;
            }
            if (iDXR)
            {   
                pSet = (HL_PIXEL*)pdwSet;
                *pSet = wSet;
            }
            
        }
    }
    else
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pSet = Pixel(iXLeft, iYTop+iY);

            #if defined(XSCALE) & !defined(THUMB) & !defined(HL_WIN32)
                ippsSet_16s(wSet, (short*)pSet, iDX);
            #else
                for (int iX = 0; iX < iDX; iX++)
                {
                    *pSet = wSet;
                    pSet++;
                }
            #endif
        }
    }
#endif

    END_TIMER(GDI_FILLRECT);
}
/*============================================================================*/

    void                        CHLDibDC::FillRoundRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iGeoType )

/*============================================================================*/
{
    if (iRad < 1)
    {
        FillRect(rgbFill, iXLeft, iYTop, iDX, iDY);
        return;
    }

    int iXLOrig = iXLeft;
    int iYTOrig = iYTop;
    int iR = __min(iRad, iDX/2);
    iR = __min(iR, iDY/2);

    iXLeft += m_iXLeft;
    iYTop += m_iYTop;
    HL_PIXEL rgbSet = RGBTOHLPIXEL(rgbFill);

    int iXA = iXLeft + iR;
    int iXB = iXLeft + iDX - iR - 1;
    int iYA = iYTop  + iR;
    int iYB = iYTop  + iDY - iR - 1;

    CPointList PointList;
    if (iGeoType & RADIUS_INSET)
        CalcArcPoints(&PointList, iR+1, TRUE);
    else
        CalcArcPoints(&PointList, iR, TRUE);

    if (iGeoType & RADIUS_INSET)
    {
        InsetArcPoints(&PointList);
    }
    
    CHLRect rClip = LocalClipRect();

    int iN = PointList.NPoints();
    for (int i = 0; i < iN; i++)
    {
        int iDXR = PointList.X(i);
        int iDYR = PointList.Y(i);
        int iYOutB = iYB + iDYR;
        int iYOutA = iYA - iDYR;
        int iXOutA = iXA-iDXR;
        int iXOutB = iXB+iDXR;
        iXOutA = __max(iXOutA, rClip.XLeft());
        iXOutB = __min(iXOutB, rClip.XRight());

        if (iYOutB >= rClip.YTop() && iYOutB <= rClip.YBottom())
        {
            FillHZ(rgbSet, iXOutA, iXOutB, iYOutB);
        }
        if (iYOutA >= rClip.YTop() && iYOutA <= rClip.YBottom())
        {
            FillHZ(rgbSet, iXOutA, iXOutB, iYOutA);
        }
    }

    FillRect(rgbFill, iXLOrig, iYTOrig + iR, iDX, iDY - 2 * iR);
}
/*============================================================================*/

    void                        CHLDibDC::FillRoundRectExt(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iGeoType )

/*============================================================================*/
{
    if (iRad < 1)
        return;

    int iR = __min(iRad, iDX/2);
    iR = __min(iR, iDY/2);

    iXLeft += m_iXLeft;
    iYTop += m_iYTop;
    HL_PIXEL rgbSet = RGBTOHLPIXEL(rgbFill);

    int iXA = iXLeft;
    int iXB = iXLeft + iDX - 1;
    int iYA = iYTop  + iR;
    int iYB = iYTop  + iDY - iR - 1;

    CPointList PointList;
    if (iGeoType & RADIUS_INSET)
        CalcArcPoints(&PointList, iR+1, TRUE);
    else
        CalcArcPoints(&PointList, iR, TRUE, iGeoType);

    if (iGeoType & RADIUS_INSET)
    {
        InsetArcPoints(&PointList);
    }
    
    int iN = PointList.NPoints();
    for (int i = 0; i < iN; i++)
    {
        int iDXR = PointList.X(i);
        int iDYR = PointList.Y(i);
        int iXRad = iR - iDXR;

        FillHZ(rgbSet, iXA, iXA + iXRad, iYB + iDYR);
        FillHZ(rgbSet, iXB-iXRad, iXB,   iYB + iDYR);
        FillHZ(rgbSet, iXA, iXA + iXRad, iYA - iDYR);
        FillHZ(rgbSet, iXB-iXRad, iXB,   iYA - iDYR);
    }
}
/*============================================================================*/

    void                        CHLDibDC::BltFromGradient(
    CGradient2D                 *pGradient,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iAlpha )

/*============================================================================*/
{
    if (pGradient->IsSolidColor())
    {
        FillRect(pGradient->SolidColor(), iXDst, iYDst, iDX, iDY);
        return;
    }

#ifdef HLDDRAW
    if (m_pSurface != NULL)
    {
        CHLDC::BltFromGradient(pGradient, iXDst, iYDst, iXSrc, iYSrc, iDX, iDY, iAlpha);
        return;
    }
#endif

    CHLRect rClip = LocalClipRect();

    iXDst += m_iXLeft;
    iYDst += m_iYTop;

    if (iXDst < rClip.XLeft())
    {
        int iXTrim = rClip.XLeft()-iXDst;
        iDX -= iXTrim;
        iXSrc += iXTrim;
        iXDst += iXTrim;
    }
    if (iYDst < rClip.YTop())
    {
        int iYTrim = rClip.YTop()-iYDst;
        iDY -= iYTrim;
        iYSrc += iYTrim;
        iYDst += iYTrim;
    }

    iDX = MIN(iDX, rClip.XRight()+1-iXDst);
    iDY = MIN(iDY, rClip.YBottom()+1-iYDst);

    if (iDX <= 0 || iDY <= 0)
        return;

    HL_PIXEL *pTgt = Pixel(iXDst, iYDst);
    int iPitch = DXPitch();
    pGradient->BltTo(pTgt, iXSrc, iYSrc, iDX, iDY, iPitch, iAlpha);
}
/*============================================================================*/

    void                        CHLDibDC::BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest )

/*============================================================================*/
{
    pDibDC->FlushText();
    BltFromDibDC(iXDest, iYDest, pDibDC, 0, 0, pDibDC->ImageDX(), pDibDC->DY());
}
/*============================================================================*/

    void                        CHLDibDC::BltFromDibDC(
    CHLDibDC                    *pSrc,
    CHLRect                     rSrc,
    CHLRect                     rDst )

/*============================================================================*/
{
    int iDXOut = rDst.DX();
    int iDXIn  = rSrc.DX();

    if (iDXOut < iDXIn)
    {
        int iDYOut = rDst.DY();
        for (int iY = 0; iY < iDYOut; iY++)
        {
            int iYIn = rSrc.YTop() + iY * rSrc.DY() / rDst.DY();
            HL_PIXEL *pIn = pSrc->Pixel(rSrc.XLeft(), iYIn);
            HL_PIXEL *pOut = Pixel(rDst.XLeft(), rDst.YTop()+iY);
            int iOffsetStep = (iDXIn << 16) / iDXOut;
            int iOffset32 = 0;
            for (int iX = 0; iX < iDXOut; iX++)
            {
                *pOut = *(pIn + (iOffset32 >> 16));
                iOffset32 += iOffsetStep;
                pOut++;
            }
        }        


        return;
    }


    WORD *wX = new WORD[iDXOut];
    for (int i = 0; i < iDXOut; i++)
    {
        wX[i] = i * iDXIn / iDXOut;
        wX[i] = __min(wX[i], iDXIn);
    }


    for (int iY = 0; iY < rDst.DY(); iY++)
    {
        int iYIn = rSrc.YTop() + iY * rSrc.DY() / rDst.DY();
        HL_PIXEL *pIn = pSrc->Pixel(rSrc.XLeft(), iYIn);
        HL_PIXEL *pOut = Pixel(rDst.XLeft(), rDst.YTop()+iY);

        for (int iX = 0; iX < iDXOut; iX++)
        {
            *pOut = *(pIn+wX[iX]);
            pOut++;
        }
    }

    delete [] wX;
}
/*============================================================================*/

    void                        CHLDibDC::BltFromDibDCAlpha(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iAlpha )

/*============================================================================*/
{
    if (iAlpha < 4)
    {
        FillRect(RGB(0,0,0), iXDest, iYDest, pDibDC->DX(), pDibDC->DY());
        return;
    }

    // Make Sure we have good coords for the src DIB
    pDibDC->FlushText();
    if (m_pParentDIB != NULL)
    {
        iXDest -= m_pParentDIB->GetOrigin().x;
        iYDest -= m_pParentDIB->GetOrigin().y;
        m_pParentDIB->BltFromDibDC(iXDest+m_iXLeft, iYDest+m_iYTop, pDibDC, iXSrc, iYSrc, iDX, iDY);
        return;
    }
    
    if (pDibDC->m_pParentDIB != NULL)
    {
        CHLDibDC *pParent = pDibDC->m_pParentDIB;
        iXSrc += pDibDC->m_iXLeft;
        iYSrc += pDibDC->m_iYTop;
        BltFromDibDC(iXDest, iYDest, pParent, iXSrc, iYSrc, iDX, iDY);
        return;
    }

    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, pDibDC))
        return;

    START_TIMER(GDI_BITBLT_MEM);

    // Check Clip
    CHLRect rClip = LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();

    if (iXDest + iDX > m_iDX)
    {
        LOGASSERT(FALSE);
    }

    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    if (iDY < 1 || iDX < 1)
        return;

    if (iXDest + iDX > m_iDX)
    {
        LOGASSERT(FALSE);
    }
    
    int iDXTgt = DX();
    int iDXSrc = pDibDC->DXPitch();

    HL_PIXEL pdwLUTLo[2048];
    HL_PIXEL pdwLUTHi[32];
    for (int i1 = 0; i1 < 2048; i1++)
    {
        DWORD byR = i1 & 0x001F;
        DWORD byG = i1 & 0x07E0;
        DWORD dwROut = byR * iAlpha / 255;
        DWORD dwGOut = (((byG >> 5) * iAlpha / 255) << 5);
        pdwLUTLo[i1] = (HL_PIXEL) (dwROut | dwGOut);
    }

    for (int i2 = 0; i2 < 32; i2++)
    {
        DWORD dwR = i2 * iAlpha / 255;
        pdwLUTHi[i2] = (HL_PIXEL)(dwR << 11);
    }    


    HL_PIXEL *pSrcRow = pDibDC->Pixel(iXSrc, iYSrc);
    HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest);
    for (int iY = 0; iY < iDY; iY++)
    {
        DWORD dwLast = 0;
        DWORD dwIn   = 0;
        DWORD *pSrc = (DWORD*)pSrcRow;
        DWORD *pTgt = (DWORD*)pTgtRow;

        for (int iX = 0; iX < iDX / 2; iX++)
        {
            // 0x0000F800
            // 0x000007E0
            // 0x0000001F

            dwIn   = *pSrc;
            if (dwIn != dwLast)
            {
                switch (dwIn)
                {
                case 0x0000:
                    dwLast = 0x0000;
                    break;

                default:
                    {    
                        DWORD dwLo1 = dwIn & 0x07FF;    dwIn = dwIn >> 11;
                        DWORD dwHi1 = dwIn & 0x001F;    dwIn = dwIn >> 5;
                        DWORD dwLo2 = dwIn & 0x07FF;    dwIn = dwIn >> 11;
                        DWORD dwHi2 = dwIn & 0x001F;
                        dwLast = pdwLUTHi[dwHi2]|pdwLUTLo[dwLo2];
                        dwLast = dwLast << 16;
                        dwLast |= pdwLUTHi[dwHi1]|pdwLUTLo[dwLo1];
                    }
                    break;
                }
            }
            *pTgt = dwLast;
            pSrc++;
            pTgt++;
        }

        pTgtRow += iDXTgt;
        pSrcRow += iDXSrc;
    }

    END_TIMER(GDI_BITBLT_MEM);
}
/*============================================================================*/

    void                        CHLDibDC::BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    CEdgeList                   *pEdgeList )

/*============================================================================*/
{
    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    CHLRect rClip = LocalClipRect();

    for (int i = 0; i < pEdgeList->NPoints(); i++)
    {
        int iX = pEdgeList->X(i);    
        int iY = pEdgeList->Y(i);

        int iYIn = iY+iYSrc;
        int iYOut = iY+iYDest;
        if (iYIn >= 0 && iYIn < pDibDC->DY() && iYOut >= rClip.YTop() && iYOut <= rClip.YBottom())
        {
            int iXIn = iX+iXSrc;
            int iXOut = iX+iXDest;

            int iN = pEdgeList->D(i);
            if (iXOut < rClip.XLeft())
            {
                int iXTrim = rClip.XLeft()-iXOut;
                iN   -= iXTrim;
                iXIn += iXTrim;
                iXOut = rClip.XLeft();
            }
            if (iXIn < 0)
            {
                iN   += iXIn;
                iXOut += -iXIn;
                iXIn = 0;
            }

            iN = __min(iN, rClip.XRight() + 1 - iXOut);
            iN = __min(iN, pDibDC->ImageDX() - iXIn);

            if (iN > 0)
            {
                HL_PIXEL *pSrcPIX = pDibDC->Pixel(iXIn, iYIn);
                HL_PIXEL *pDstPIX = Pixel(iXOut, iYOut);
                memcpy(pDstPIX, pSrcPIX, iN * sizeof(HL_PIXEL));
            }
        }
    }

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        CHLRect rBounds = pEdgeList->GetBounds();
        AddUpdateRect(CDXYRect(iXDest+rBounds.XLeft(), iYDest+rBounds.YTop(), rBounds.DX(), rBounds.DY()));
    }
}
/*============================================================================*/

    void                        CHLDibDC::BltFromDibDC(
    int                         iXDest,
    int                         iYDest,
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iFlags )

/*============================================================================*/
{
    // Make Sure we have good coords for the src DIB
    pDibDC->FlushText();
    if (m_pParentDIB != NULL)
    {
        iXDest -= m_pParentDIB->GetOrigin().x;
        iYDest -= m_pParentDIB->GetOrigin().y;
        m_pParentDIB->BltFromDibDC(iXDest+m_iXLeft, iYDest+m_iYTop, pDibDC, iXSrc, iYSrc, iDX, iDY);
        return;
    }
    
    if (pDibDC->m_pParentDIB != NULL)
    {
        CHLDibDC *pParent = pDibDC->m_pParentDIB;
        iXSrc += pDibDC->m_iXLeft;
        iYSrc += pDibDC->m_iYTop;
        BltFromDibDC(iXDest, iYDest, pParent, iXSrc, iYSrc, iDX, iDY);
        return;
    }

    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, pDibDC))
        return;

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        AddUpdateRect(CDXYRect(iXDest, iYDest, iDX, iDY));
    }

    if (AlphaDIB() != NULL && pDibDC->AlphaDIB() != NULL)
    {
        BYTE *pMask = pDibDC->GetMask();
        if (pMask != NULL)
        {   
            if (m_pMask == NULL)
            {
                m_pMask = (BYTE*)HLAlloc(m_iDX * m_iDY);
            }
            for (int iY = 0; iY < iDY; iY++)
            {
                memcpy(GetMask(iXDest, iYDest+iY), pDibDC->GetMask(iXSrc, iYSrc+iY), iDX);
            }
        }    
    }

    START_TIMER(GDI_BITBLT_MEM);

    // Check Clip
    CHLRect rClip = LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();

    if (iXDest + iDX > m_iDX)
    {
        LOGASSERT(FALSE);
    }

    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    if (iDY < 1 || iDX < 1)
        return;

    if (iXDest + iDX > m_iDX)
    {
        LOGASSERT(FALSE);
    }
    
    #ifdef HLDDRAW
        if (iFlags & BLT_FLAG_SYSRAMTOVRAM && m_pSurface != NULL)
        {
            HL_SURFACEDESC surfaceDesc;
            memset(&surfaceDesc, 0, sizeof(HL_SURFACEDESC));
            surfaceDesc.dwSize      = sizeof(HL_SURFACEDESC);
            surfaceDesc.dwFlags = DDSD_HEIGHT|DDSD_WIDTH|DDSD_LPSURFACE;

            #ifdef HL_DDRAWBASIC
                HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAITNOTBUSY|DDLOCK_WRITEONLY|DDLOCK_DISCARD, NULL);
            #else
                HRESULT hr = m_pSurface->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
            #endif

            if (hr == S_OK)
            {
                BYTE *pMem = (BYTE*)surfaceDesc.lpSurface;
                int iDXPitchOut = surfaceDesc.lPitch;
                BYTE *pTgt = (BYTE*)(pMem + (iYDest * iDXPitchOut) + iXDest);
                for (int iY = 0; iY < iDY; iY++)
                {
                    HL_PIXEL *pSrc = pDibDC->Pixel(iXSrc, iYSrc+iY);
                    memcpy(pTgt, pSrc, iDX * sizeof(HL_PIXEL));
                    pTgt += iDXPitchOut;
                }

                m_pSurface->Unlock(NULL);
                return;
            }
        }

        if (m_pSurface != NULL && pDibDC->GetSurface() != NULL)
        {

            HL_DIRECTDRAWSURFACE pDst = GetSurface();
            HL_DIRECTDRAWSURFACE pSrc = pDibDC->GetSurface();
            LOGASSERT(m_pBits == NULL);

            if (pDst != NULL && pSrc != NULL)
            {
                if (pDst->IsLost())
                    pDst->Restore();    
                if (pSrc->IsLost())
                    pSrc->Restore();
                ReleaseBits();
                pDibDC->ReleaseBits();
                RECT rSrc = CDXYRect(iXSrc,  iYSrc,  iDX, iDY).GetRECT();

                #ifdef HL_DDRAWBASIC
                    RECT rDst = CDXYRect(iXDest, iYDest, iDX, iDY).GetRECT();
                    HRESULT hRes = pDst->Blt(&rDst, pSrc, &rSrc, 0, NULL);
                #else
                    HRESULT hRes = pDst->BltFast(iXDest, iYDest, pSrc, &rSrc, BLTFAST_FLAGS);
                #endif

                if (hRes != DD_OK)
                {
                    int iJunk = 1;
                    return;
                }

                return;
            }
        }
    #endif

    int iDXTgt = DX();
    int iDXSrc = pDibDC->DXPitch();

    if (iYDest > iYSrc && this == pDibDC)
    {
        HL_PIXEL *pSrcRow = pDibDC->Pixel(iXSrc, iYSrc+iDY-1);
        HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest+iDY-1);
        for (int iY = 0; iY < iDY; iY++)
        {
            #ifdef _DEBUG
                pDibDC->CheckPointer(pSrcRow);
                pDibDC->CheckPointer(pSrcRow + iDX - 1);
                CheckPointer(pTgtRow);
                CheckPointer(pTgtRow + iDX - 1);
            #endif
            PIXEL_COPY(pTgtRow, pSrcRow, iDX);
            pTgtRow -= iDXTgt;
            pSrcRow -= iDXSrc;
        }
    }
    else
    {
        HL_PIXEL *pSrcRow = pDibDC->Pixel(iXSrc, iYSrc);
        HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest);
        for (int iY = 0; iY < iDY; iY++)
        {
            #ifdef _DEBUG
                pDibDC->CheckPointer(pSrcRow);
                pDibDC->CheckPointer(pSrcRow + iDX - 1);
                CheckPointer(pTgtRow);
                CheckPointer(pTgtRow + iDX - 1);
            #endif
            PIXEL_COPY(pTgtRow, pSrcRow, iDX);
            pTgtRow += iDXTgt;
            pSrcRow += iDXSrc;
        }
    }

    END_TIMER(GDI_BITBLT_MEM);
}
/*============================================================================*/

    void                        CHLDibDC::BltFromAlphaDIB(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        iXDest += m_iXLeft;
        iYDest += m_iYTop;      
        m_pParentDIB->BltFromAlphaDIB(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
        return;
    }

    START_TIMER(GDI_DIBXBLT);

    #if defined(HLDDRAW) & defined(UNDER_CE)
        //
        // Only works on TS7 for now
        //
        if (m_pBits == NULL && m_hDC == NULL)
        {
            if (m_pSurface != NULL && pAlpha->HasDDAlphaSurface())
            {
                iXDest = m_iXLeft + iXDest;
                iYDest = m_iYTop + iYDest;

                if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, pAlpha))
                    return;

                RECT rDst = CDXYRect(iXDest, iYDest, iDX, iDY).GetRECT();
                RECT rSrc = CDXYRect(iXSrc, iYSrc, iDX, iDY).GetRECT();

                HL_DIRECTDRAWSURFACE pThis = GetSurface();
                HL_DIRECTDRAWSURFACE pSrc  = pAlpha->GetDDAlphaSurface();

                BOOL bHandled = TRUE;
                #ifdef HL_DDRAWBASIC
                    pThis->AlphaBlt(&rDst, pSrc, &rSrc, 0, NULL);
                    bHandled = TRUE;
                #else
                    LPDIRECTDRAWSURFACE5 pSurf5D = NULL;
                    LPDIRECTDRAWSURFACE5 pSurf5S = NULL;
                    pThis->QueryInterface(IID_IDirectDrawSurface5, (LPVOID*)&pSurf5D);
                    pSrc->QueryInterface(IID_IDirectDrawSurface5, (LPVOID*)&pSurf5S);
                    if (pSurf5D != NULL && pSurf5S != NULL)
                    {
                        HRESULT hRes = pSurf5D->AlphaBlt(&rDst, pSurf5S, &rSrc, 0, NULL);
                        if (hRes == S_OK)
                            bHandled = TRUE;
                    }
                    if (pSurf5D != NULL) pSurf5D->Release();
                    if (pSurf5S != NULL) pSurf5S->Release();
                #endif
                
                if (bHandled)
                    return;
                iXDest -= m_iXLeft;
                iYDest -= m_iYTop;
            }
        }
    #endif

    FlushText();
    CAlphaDIB::HL_PREMULT_MODE Mode = pAlpha->MakePreMult();

    if (Mode == CAlphaDIB::PREMULT_VALUES)
    {
        #ifdef HL_MMINTRIN
            BOOL bAddMode = (pAlpha->Attribs() & DIBATTRIB_ALPHAADDMODE);
            if (!bAddMode)
                BltFromAlphaDIBPreMultValuesMMINTRIN(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
            else
                BltFromAlphaDIBPreMultValues(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
        #else
            #ifdef HL_MMX
                BOOL bAddMode = (pAlpha->Attribs() & DIBATTRIB_ALPHAADDMODE);
                if (!bAddMode)
                    BltFromAlphaDIBPreMultValuesMMX(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
                else
                    BltFromAlphaDIBPreMultValues(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
            #else
                BltFromAlphaDIBPreMultValues(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
            #endif
        #endif
        END_TIMER(GDI_DIBXBLT);

        return;
    }

    BltFromAlphaDIBNoEncoding(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
}
#ifdef HLDDRAW
#ifdef HL_ARMV4IC
/*============================================================================*/

    void                        CHLDibDC::DDBltFromAlphaDIB(
    HL_DIRECTDRAWSURFACE        pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        iXDest += m_iXLeft;
        iYDest += m_iYTop;      
        m_pParentDIB->DDBltFromAlphaDIB(pAlpha, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
        return;
    }

    if (m_pSurface == NULL)
        return;

    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

    RECT rDst = CDXYRect(iXDest, iYDest, iDX, iDY).GetRECT();
    RECT rSrc = CDXYRect(iXSrc, iYSrc, iDX, iDY).GetRECT();

    HL_DIRECTDRAWSURFACE pThis = GetSurface();

    BOOL bHandled = TRUE;
#ifndef HL_WIN32
    pThis->AlphaBlt(&rDst, pAlpha, &rSrc, 0, NULL);
#endif
}
#endif
#endif
/*============================================================================*/

    void                        CHLDibDC::BltFromAlphaDIBNoEncoding(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY  )

/*============================================================================*/
{
    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    START_TIMER(GDI_DIBXBLT);

    if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, pAlpha))
    {
        END_TIMER(GDI_DIBXBLT);
        return;
    }

    // Check Clip
    CHLRect rClip = LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();
    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        AddUpdateRect(CDXYRect(iXDest, iYDest, iDX, iDY));
    }

    if (iDY < 1 || iDX < 1)
    {
        END_TIMER(GDI_DIBXBLT);
        return;
    }

    HL_PIXEL *pSrcRow = pAlpha->Pixel(iXSrc, iYSrc);
    BYTE *pAlphaRow   = pAlpha->GetMask(iXSrc, iYSrc);

    #ifdef HL_PIXEL32
        DWORD wRA = 0;
        DWORD wGA = 0;
        DWORD wBA = 0;
        DWORD wRB = 0;
        DWORD wGB = 0;
        DWORD wBB = 0;
    #else
        DWORD wRBA = 0;
        DWORD wGA = 0;
        DWORD wRBB = 0;
        DWORD wGB = 0;
    #endif

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest+iY);

        #ifdef HL_PIXEL32
            BYTE *pTgtR = (BYTE*)pTgtRow;
            BYTE *pTgtG = pTgtR+1;
            BYTE *pTgtB = pTgtG+1;

            BYTE *pSrcR = (BYTE*)pSrcRow;
            BYTE *pSrcG = pSrcR+1;
            BYTE *pSrcB = pSrcG+1;
        #else
            HL_PIXEL *pTgt = pTgtRow;
            HL_PIXEL *pSrc = pSrcRow;
        #endif

        BYTE *pA       = pAlphaRow;

        pSrcRow   += pAlpha->DXPitch();
        pAlphaRow += pAlpha->DXPitch();        
        pTgtRow   += DXPitch();

        for (int iX = 0; iX < iDX; iX++)
        {
            switch (*pA)
            {
            case 0xFF:
                {
                    // Fully Opaque
                    #ifdef HL_PIXEL32
                        memcpy(pTgtR, pSrcR, 3);
                    #else
                        *pTgt = *pSrc;
                    #endif
                }
                break;

            case 0x00:
                break;

            default:
                {
                    DWORD byAlpha = (*pA << 6) / 255;

                    #ifdef HL_PIXEL32
                        wRA = *pSrcR;
                        wGA = *pSrcG;
                        wBA = *pSrcB;
                        wRB = *pTgtR;
                        wGB = *pTgtG;
                        wBB = *pTgtB;
                        *pTgtR = (BYTE)((wRA * byAlpha + (0xFF-byAlpha) * wRB) >> 8);
                        *pTgtG = (BYTE)((wGA * byAlpha + (0xFF-byAlpha) * wGB) >> 8);
                        *pTgtB = (BYTE)((wBA * byAlpha + (0xFF-byAlpha) * wBB) >> 8);
                    #else
                        DWORD _byAlpha   = 64-byAlpha;
                        HL_PIXEL wSrc = *pSrc;
                        HL_PIXEL wTgt = *pTgt;

                        wRBA = wSrc & 0x0000F81F;
                        wRBB = wTgt & 0x0000F81F;
                        DWORD wRBOut = (((wRBA * byAlpha + _byAlpha * wRBB) >> 6) & 0x0000F81F);

                        wGA = wSrc & 0x000007E0;
                        wGB = wTgt & 0x000007E0;

                        DWORD wGOut = (((wGA * byAlpha  + _byAlpha  * wGB) >> 6) & 0x000007E0);
                        *pTgt = (HL_PIXEL)(wRBOut | wGOut);
                    #endif
                }
                break;
            }

            #ifdef HL_PIXEL32
                pTgtR += 4;
                pTgtG += 4;
                pTgtB += 4;
                pSrcR += 4;
                pSrcG += 4;
                pSrcB += 4;
            #else
                pTgt++;
                pSrc++;
            #endif
            pA++;
        }
    }

    END_TIMER(GDI_DIBXBLT);
}
/*============================================================================*/

    void                        CHLDibDC::BltFromAlphaDIBPreMultValues(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY  )

/*============================================================================*/
{
    FlushText();
    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, pAlpha))
        return;

    // Check Clip
    CHLRect rClip = LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();
    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        AddUpdateRect(CDXYRect(iXDest, iYDest, iDX, iDY));
    }

    if (iDY < 1 || iDX < 1)
        return;

    HL_PIXEL *pSrcRow = pAlpha->Pixel(iXSrc, iYSrc);
    BYTE *pAlphaRow   = pAlpha->GetMask(iXSrc, iYSrc);

#ifndef HL_PIXEL32
    DWORD wRA = 0;
    DWORD wGA = 0;
    DWORD wBA = 0;
    DWORD wRB = 0;
    DWORD wGB = 0;
    DWORD wBB = 0;
#endif

    BOOL bAddMode = (pAlpha->Attribs() & DIBATTRIB_ALPHAADDMODE);

    int iSrcPitch = pAlpha->DXPitch();

    if (bAddMode)
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest+iY);

            #ifdef HL_PIXEL32
                BYTE *pTgtR = (BYTE*)pTgtRow;
                BYTE *pTgtG = pTgtR+1;
                BYTE *pTgtB = pTgtG+1;

                BYTE *pSrcR = (BYTE*)pSrcRow;
                BYTE *pSrcG = pSrcR+1;
                BYTE *pSrcB = pSrcG+1;
            #else
                HL_PIXEL *pTgt = pTgtRow;
                HL_PIXEL *pSrc = pSrcRow;
            #endif

            pSrcRow   += iSrcPitch;
            
            int iDXLine = iDX;

            for (int iX = 0; iX < iDXLine; iX++)
            {
                #ifdef HL_PIXEL32
                    DWORD wRT = *pTgtR;
                    DWORD wGT = *pTgtG;
                    DWORD wBT = *pTgtB;
                    wRT += *pSrcR;
                    wGT += *pSrcG;
                    wBT += *pSrcB;
                    *pTgtR = (BYTE)wRT;
                    *pTgtG = (BYTE)wGT;
                    *pTgtB = (BYTE)wBT;
                #else
                    HL_PIXEL wSrc = *pSrc;
                    HL_PIXEL wTgt = *pTgt;

                    wRA = (wTgt & 0xF800) + (wSrc & 0xF800);
                    if (wRA > 0xF800)   wRA = 0xF800; else wRA &= 0xF800;
                    wGA = (wTgt & 0x07E0) + (wSrc & 0x07E0);
                    if (wGA > 0x07E0)   wGA = 0x07E0; else wGA &= 0x07E0;
                    wBA = (wTgt & 0x001F) + (wSrc & 0x001F);
                    if (wBA > 0x001F)   wBA = 0x001F; else wBA &= 0x001F;

                    *pTgt = (WORD)(wRA | wGA | wBA);
                #endif

                #ifdef HL_PIXEL32
                    pTgtR += 4;
                    pTgtG += 4;
                    pTgtB += 4;
                    pSrcR += 4;
                    pSrcG += 4;
                    pSrcB += 4;
                #else
                    pTgt++;
                    pSrc++;
                #endif
            }
        }
        return;
    }

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest+iY);

        HL_PIXEL *pTgt = pTgtRow;
        HL_PIXEL *pSrc = pSrcRow;

        BYTE *pA       = pAlphaRow;
        pSrcRow   += iSrcPitch;
        pAlphaRow += iSrcPitch;        
        
        int iDXLine = iDX;

        for (int iX = 0; iX < iDXLine; iX++)
        {
            DWORD dwA = *pA;
            
            HL_PIXEL wTgt = *pTgt;
            HL_PIXEL wSrc = *pSrc;

#ifdef HL_PIXEL32
            DWORD wRDst  = wTgt & 0x000000FF;
            DWORD wRSrc  = wSrc & 0x000000FF;
            DWORD wGDst  = wTgt & 0x0000FF00;
            DWORD wGSrc  = wSrc & 0x0000FF00;
            DWORD wBDst  = wTgt & 0x00FF0000;
            DWORD wBSrc  = wSrc & 0x00FF0000;


            DWORD wROut = (wRSrc + (((wRDst * dwA)) >> ALPHA_BITS)) & 0x000000FF;
            DWORD wGOut = (wGSrc + (((wGDst * dwA)) >> ALPHA_BITS)) & 0x0000FF00;
            DWORD wBOut = (wBSrc + (((wBDst * dwA)) >> ALPHA_BITS)) & 0x00FF0000;
            *pTgt = (HL_PIXEL)(wROut|wGOut|wBOut);                                
#else

            DWORD wRBDst = wTgt & 0xF81F;
            DWORD wRBSrc = wSrc & 0xF81F;
            DWORD wGDst  = wTgt & 0x07E0;
            DWORD wGSrc  = wSrc & 0x07E0;
            DWORD wRBOut = (wRBSrc + (((wRBDst * dwA)) >> ALPHA_BITS)) & 0xF81F;
            DWORD wGOut  = (wGSrc  + (((wGDst  * dwA)) >> ALPHA_BITS)) & 0x07E0;
            *pTgt = (HL_PIXEL)(wRBOut|wGOut);                                
#endif

            pTgt++;
            pSrc++;
            pA++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::BltTransparentToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
#ifdef IPHONE
     CHLDibDC *pDibDC = pDC->DibDC();
     if (pDibDC == NULL)
        return;

    if (!pDibDC->GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, this))
        return;

    // Assume ADD mode for now
    DWORD dwRS = 0;
    DWORD dwGS = 0;
    DWORD dwBS = 0;
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY);
        HL_PIXEL *pDst = pDibDC->Pixel(iXDest, iYDest+iY);
        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;
            HL_PIXEL wDst = *pDst;

            dwRS = wSrc & 0x0000FF00;
            dwGS = wSrc & 0x00FF0000;
            dwBS = wSrc & 0xFF000000;
            DWORD dwRT = wDst & 0x0000FF00;
            DWORD dwGT = wDst & 0x00FF0000;
            DWORD dwBT = wDst & 0xFF000000;
            DWORD dwRTMax = 0x0000FF00 - dwRS;
            DWORD dwGTMax = 0x00FF0000 - dwGS;
            DWORD dwBTMax = 0xFF000000 - dwBS;

            if (dwRT < dwRTMax) dwRT += dwRS; else dwRT = 0x0000FF00;
            if (dwGT < dwGTMax) dwGT += dwGS; else dwGT = 0x00FF0000;
            if (dwBT < dwBTMax) dwBT += dwBS; else dwBT = 0xFF000000;

            *pDst = (dwRT|dwGT|dwBT|0xFF);
            
            pSrc++;
            pDst++;
        }
    }
#endif
}
/*============================================================================*/

    void                        CHLDibDC::BltInvertedToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY  )

/*============================================================================*/
{
}
/*============================================================================*/

    void                        CHLDibDC::SetColor(
    COLORREF                    rgbNew )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        m_pParentDIB->SetColor(rgbNew);
        return;
    }

    if (m_hDC == NULL)
    {
        m_rgbColor = rgbNew;
        return;
    }

    CHLDC::SetColor(rgbNew);
}
/*============================================================================*/

    CHLDibDC                    *CHLDibDC::DibDC()

/*============================================================================*/
{
    return this;
}
/*============================================================================*/

    void                        CHLDibDC::SetQuickFont(
    int                         iHeight )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {   
        m_pParentDIB->SetQuickFont(iHeight);
        return;
    }

    if (m_hDC == NULL)
    {
        m_iFontHeight = iHeight;
        return;
    }

#ifndef IPHONE
    CHLDC::SetQuickFont(iHeight);
#endif
}
/*============================================================================*/

    void                        CHLDibDC::DrawString(
    CHLString                   sText,
    CHLRect                     rect,
    int                         iHLFormat   )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        rect.Translate(m_iXLeft, m_iYTop);
        m_pParentDIB->DrawString(sText, rect, iHLFormat);
        return;
    }

#ifndef IPHONE
    CHLRect rTest = rect;
    rTest.Translate(m_iXLeft, m_iYTop);
    if (rTest.YBottom() < 0)
        return;
    if (rTest.XLeft() >= ImageDX())
        return;
    if (rTest.YTop() > DY())
        return;
    if (rTest.XRight() < 0)
        return;

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        AddUpdateRect(CDXYRect(rTest.XLeft(), rTest.YTop(), rTest.DX(), rTest.DY()));
    }

    CHLDC::DrawString(sText, rect, iHLFormat);        
#endif
}
/*============================================================================*/

    void                        CHLDibDC::DrawStringAlpha(
    CHLString                   sText,
    CHLRect                     rect,
    int                         iHLFormat,
    int                         iAlpha   )

/*============================================================================*/
{
#ifndef IPHONE
#ifdef CRYSTALPAD
    CHLFont *pFont = g_pFontServer->GetFont(m_iFontHeight);
    if (pFont == NULL)
        return;

    START_TIMER(GDI_TEXT);
    rect.Translate(m_iXLeft, m_iYTop);
    pFont->RenderTextAlpha(this, &sText, rect, iHLFormat, m_rgbColor, iAlpha);
    END_TIMER(GDI_TEXT);
#endif
#endif
}
/*============================================================================*/

    void                        CHLDibDC::DimRect(
    CHLRect                     rDim,
    int                         iStep )

/*============================================================================*/
{
    int iX1 = rDim.XLeft(); 
    int iX2 = rDim.XRight();
    int iY1 = rDim.YTop();
    int iY2 = rDim.YBottom();

    iX2 = __min(DX()-1, iX2);
    iY2 = __min(DY()-1, iY2);
    iX1 = __max(0, iX1);
    iY1 = __max(0, iY1);       

    DWORD dwMask = 0x0000;
    switch (iStep)
    {
    case 1: dwMask = 0x7BEF7BEF; break;
    case 2: dwMask = 0x39E739E7; break;
    case 3: dwMask = 0x18E318E3; break;
    }

    int iDXStart = iX1 % 2;
    int iDX = iX2-iX1+1;
    int iD = iDX / 2;
    int iDXEnd = iDX - iDXStart - (iD * 2);

    LOGASSERT((iX1+iD*2) <= m_iDX);
    HL_PIXEL *pUL = Pixel(iX1, iY1);

    for (int iY = iY1; iY <= iY2; iY++)
    {
        if (iDXStart > 0)
        {
            DWORD wBits = *pUL;
            wBits = (wBits >> iStep) & dwMask;
            *pUL = (HL_PIXEL) wBits;
        }

        DWORD *pdwBits = (DWORD*)(pUL+iDXStart);

        for (int iX = 0; iX < iD; iX++)
        {
            DWORD RGB16 = *pdwBits;
            RGB16 = (RGB16 >> iStep) & dwMask;
            *pdwBits = RGB16;
            pdwBits++;
        }

        if (iDXEnd > 0)
        {
            HL_PIXEL *pUR = (HL_PIXEL*)pdwBits;
            DWORD wBits = *pUR;
            wBits = (wBits >> iStep) & dwMask;
            *pUR = (HL_PIXEL) wBits;
        }
        pUL += m_iDX;
    }

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        AddUpdateRect(CDXYRect(iX1, iY1, iX2-iX1+1, iY2-iY1+1));
    }
}
/*============================================================================*/

    void                        CHLDibDC::DimEdgeList(
    CEdgeList                   *pEdgeList,
    int                         iStep )

/*============================================================================*/
{
    DWORD dwMask = 0x0000;
    switch (iStep)
    {
    case 1: dwMask = 0x7BEF7BEF; break;
    case 2: dwMask = 0x39E739E7; break;
    case 3: dwMask = 0x18E318E3; break;
    }

    HL_PIXEL *pMem = Bits();

    for (int iEdge = 0; iEdge < pEdgeList->NPoints(); iEdge++)
    {
        int iX = pEdgeList->X(iEdge);
        int iY = pEdgeList->Y(iEdge);
        int iD = pEdgeList->D(iEdge);

        // Word Align
        iX = (iX / 2) * 2;
        iD = __min(iD, m_iDX - iX - 1);
        iD /= 2;        

        DWORD *pBits = (DWORD*)(pMem + iY * m_iDX + iX);
        
        for (int iP = 0; iP <= iD; iP++)
        {
            DWORD RGB32 = *pBits;
            RGB32 = (RGB32 >> iStep) & dwMask;
            *pBits = RGB32;
            pBits++;
        }
    }

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        CHLRect rBounds = pEdgeList->GetBounds();
        AddUpdateRect(CDXYRect(rBounds.XLeft(), rBounds.YTop(), rBounds.DX(), rBounds.DY()));
    }
}
/*============================================================================*/

    void                        CHLDibDC::Replace(
    HL_PIXEL                    wOld,
    HL_PIXEL                    wNew )

/*============================================================================*/
{
    int iDX = DX();
    for (int iY = 0; iY < DY(); iY++)
    {
        HL_PIXEL *pPix = Pixel(0, iY);
        for (int i = 0; i < iDX; i++)
        {
            if (*pPix == wOld)
                *pPix = wNew;
            pPix++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::Replace(
    CHLRect                     rROI,
    HL_PIXEL                    wOld,
    HL_PIXEL                    wNew )

/*============================================================================*/
{
    int iX0 = rROI.XLeft();
    int iY0 = rROI.YTop();
    int iDX = rROI.DX();
    int iDY = rROI.DY();

    if (iX0 < 0)
    {
        iDX += iX0;
        iX0 = 0;
    }
    if (iY0 < 0)
    {
        iDY += iY0;
        iY0 = 0;
    }
    iDX = __min(DX()-iX0, iDX);
    iDY = __min(DY()-iY0, iDY);

    if (iDX <= 0 || iDY <= 0)
        return;

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pPix = Pixel(iX0, iY+iY0);
        for (int iX = 0; iX < iDX; iX++)
        {
            if (*pPix == wOld)
                *pPix = wNew;
            pPix++;
        }

    }
}
/*============================================================================*/

    void                        CHLDibDC::Blur(
    CHLRect                     rROI,
    int                         iNPass )

/*============================================================================*/
{
    CHLDibDC dcTemp(rROI.DX(), rROI.DY(), 0, 0, MEMORY_SYSTEM);


    int iYB = rROI.YTop();
    int iXB = rROI.XLeft();

    int iDX = rROI.DX();
    int iDY = rROI.DY();

    for (int iN = 0; iN < iNPass; iN++)
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pU1 = Pixel(iXB-1, iY+iYB-1);
            HL_PIXEL *pC1 = Pixel(iXB-1, iY+iYB);
            HL_PIXEL *pD1 = Pixel(iXB-1, iY+iYB+1);

            HL_PIXEL *pOut = dcTemp.Pixel(0, iY);

            for (int iX = 0; iX < iDX; iX++)
            {
                HL_PIXEL *pU = pU1;
                HL_PIXEL *pC = pC1;
                HL_PIXEL *pD = pD1;

                DWORD dwRBSum = (*pU & 0xF81F)+(*pC & 0xF81F)+(*pD & 0xF81F);
                DWORD dwGSum  = (*pU & 0x07E0)+(*pC & 0x07E0)+(*pD & 0x07E0);
                pU++;
                pD++;
                dwRBSum += (*pU & 0xF81F)+(*pD & 0xF81F);
                dwGSum  += (*pU & 0x07E0)+(*pD & 0x07E0);
                pU++;
                pD++;
                dwRBSum += (*pU & 0xF81F)+(*pD & 0xF81F);
                dwGSum  += (*pU & 0x07E0)+(*pD & 0x07E0);
                pC+=2;
                dwRBSum += (*pC & 0xF81F);
                dwGSum  += (*pC & 0x07E0);

                DWORD dwRB = (dwRBSum >> 3) & 0xF81F;
                DWORD dwG = (dwGSum >> 3) & 0x07E0;
                *pOut = (HL_PIXEL)(dwRB|dwG);

                pU1++;
                pC1++;
                pD1++;
                pOut++;
            }
        }

        BltFromDibDC(&dcTemp, rROI.XLeft()-m_iXLeft, rROI.YTop()-m_iYTop);
    }
}
/*============================================================================*/

    void                        CHLDibDC::Blur(
    HL_PIXEL                    *pTransparent )

/*============================================================================*/
{
//    HL_PIXEL *pOut = (HL_PIXEL*)malloc(sizeof(HL_PIXEL) * m_iDX * m_iDY);
    HL_PIXEL *pOut = new HL_PIXEL[DX()*DY()];
    PIXEL_COPY(pOut, m_pBits, DX() * DY());

    HL_PIXEL *pPix[3];
    for (int iY = 1; iY < DY()-1; iY++)
    {
        // Do a row 3x3 scan
        pPix[0] = Pixel(0, iY);
        pPix[1] = Pixel(0, iY-1);
        pPix[2] = Pixel(0, iY+1);
        HL_PIXEL *pOutRow = pOut + iY * DX() + 1;
    
        for (int iX = 0; iX < DX()-2; iX++)
        {
            WORD wSumR = 0;
            WORD wSumG = 0;
            WORD wSumB = 0;
            for (int iSubX = 0; iSubX < 3; iSubX++)
            {
                for (int iSubY = 0; iSubY < 3; iSubY++)
                {
                    wSumR += GetRValueHLPIXEL(*pPix[iSubY]);
                    wSumG += GetGValueHLPIXEL(*pPix[iSubY]);               
                    wSumB += GetBValueHLPIXEL(*pPix[iSubY]);               
                }

                pPix[0]++;
                pPix[1]++;
                pPix[2]++;
            }

            HL_PIXEL rgbNew = RGBtoHLPIXEL(wSumR/9, wSumG/9, wSumB/9);
            if (rgbNew == *pTransparent)
                rgbNew = 0x0000;

            #ifdef _DEBUG
                LOGASSERT(pOutRow >= pOut);
                LOGASSERT(pOutRow < (pOut+DX()*DY()));
            #endif

            *pOutRow = rgbNew;
            pOutRow++;
            
            pPix[0] -= 2;
            pPix[1] -= 2;
            pPix[2] -= 2;
        }
    }
    
    PIXEL_COPY(Bits(), pOut, DX() * DY());
//    free(pOut);
    delete [] pOut;
}
/*============================================================================*/

    void                        CHLDibDC::BlurMask()

/*============================================================================*/
{
    if (m_pMask == NULL)
        return;
   
    BYTE *pOut = (BYTE*)HLAlloc(m_iDX * m_iDY);
    memset(pOut, 0, DX()*DY());
    for (int iY = 1; iY < DY()-1; iY++)
    {
        // Do a row 3x3 scan
        BYTE *p1A = m_pMask + (iY+0) * DX();
        BYTE *p2A = m_pMask + (iY-1) * DX();
        BYTE *p3A = m_pMask + (iY+1) * DX();
        BYTE *p1B = p1A + 1;
        BYTE *p2B = p2A + 1;
        BYTE *p3B = p3A + 1;
        BYTE *p1C = p1B + 1;
        BYTE *p2C = p2B + 1;
        BYTE *p3C = p3B + 1;

        BYTE *pOutRow = pOut + iY * DX() + 1;
    
        for (int iX = 0; iX < DX()-2; iX++)
        {
            WORD wSum = *p1A + *p1B + *p1C + *p2A + *p2B + *p2C + *p3A + *p3B + *p3C;
            *pOutRow = (BYTE)(wSum/9);
            pOutRow++;
            p1A++;
            p2A++;
            p3A++;
            p1B++;
            p2B++;
            p3B++;
            p1C++;
            p2C++;
            p3C++;
        }
    }
    
    memcpy(m_pMask, pOut, DX()*DY());
    HLFree(pOut);
}
/*============================================================================*/

    void                        CHLDibDC::BlurEdges(
    CHLDibDC                    *pMaskDC )

/*============================================================================*/
{
    HL_PIXEL *pOut = new HL_PIXEL[m_iDX * m_iDY];
//    HL_PIXEL *pOut = (HL_PIXEL*)malloc(sizeof(HL_PIXEL) * m_iDX * m_iDY);
    PIXEL_COPY(pOut, Bits(), DX()*DY());

    HL_PIXEL *pPix[3];
    for (int iY = 1; iY < DY()-1; iY++)
    {
        // Do a row 3x3 scan
        pPix[0] = Pixel(0, iY);
        pPix[1] = Pixel(0, iY-1);
        pPix[2] = Pixel(0, iY+1);
        HL_PIXEL *pMask = pMaskDC->Pixel(1, iY);
        HL_PIXEL *pOutRow = pOut + iY * DX() + 1;
    
        for (int iX = 0; iX < DX()-2; iX++)
        {
            WORD wSumR = 0;
            WORD wSumG = 0;
            WORD wSumB = 0;
            for (int iSubX = 0; iSubX < 3; iSubX++)
            {
                for (int iSubY = 0; iSubY < 3; iSubY++)
                {
                    wSumR += GetRValueHLPIXEL(*pPix[iSubY]);
                    wSumG += GetGValueHLPIXEL(*pPix[iSubY]);               
                    wSumB += GetBValueHLPIXEL(*pPix[iSubY]);               
                }

                pPix[0]++;
                pPix[1]++;
                pPix[2]++;
            }

            if (*pMask)
            {
                //HL_PIXEL rgbNew = RGBTOHLPIXEL(RGBtoHLPIXEL(255,255,0));
                HL_PIXEL rgbNew = RGBtoHLPIXEL(wSumR/9, wSumG/9, wSumB/9);
                *pOutRow = rgbNew;
            }
            else if ((*(pMask + 1) || *(pMask-1)) && *pOutRow)
            {
                HL_PIXEL rgbMid;
                if (*(pMask + 1))
                    rgbMid = *(pPix[1] - 2);
                else
                    rgbMid = *(pPix[1] - 4);
                wSumR += GetRValueHLPIXEL(rgbMid) * 9;
                wSumG += GetGValueHLPIXEL(rgbMid) * 9;               
                wSumB += GetBValueHLPIXEL(rgbMid) * 9;               
                HL_PIXEL rgbNew = RGBtoHLPIXEL(wSumR/18, wSumG/18, wSumB/18);
                *pOutRow = rgbNew;
            }

            pMask ++;
            pOutRow++;
            
            pPix[0] -= 2;
            pPix[1] -= 2;
            pPix[2] -= 2;
        }
    }
    
    PIXEL_COPY(Bits(), pOut, DX()*DY());
//    free(pOut);
    delete [] pOut;
}
/*============================================================================*/

    void                        CHLDibDC::FillRectAlpha(
    CHLRect                     rRect,
    HL_PIXEL                    wFill,
    int                         iAlpha  )

/*============================================================================*/
{
    int iXDst = rRect.XLeft() + m_iXLeft;
    int iYDst = rRect.YTop() + m_iYTop;
    int iDX = rRect.DX();
    int iDY = rRect.DY();
    int iXSrc = 0;
    int iYSrc = 0;

    CHLRect rClipThis = LocalClipRect();
    CDXYRect rInt(iXDst, iYDst, iDX, iDY);
    CHLRect rClip = rClipThis.IntersectWith(rInt);
    iXDst = rClip.XLeft();
    iYDst = rClip.YTop();
    iDX = rClip.DX();
    iDY = rClip.DY();    
    
    
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

    #if defined(HLDDRAW) & defined(UNDER_CE)
/*
        CDXYRect rDst(iXDst, iYDst, iDX, iDY);
        RECT rDstRECT = rDst.GetRECT();
        HL_DIRECTDRAWSURFACE pDst = GetSurfaceVRAM();
        if (pDst != NULL)
        {
            DDALPHABLTFX fx;
            memset(&fx, 0, sizeof(DDALPHABLTFX));
            fx.dwSize = sizeof(DDALPHABLTFX);
            BYTE by8Bit = (BYTE)iAlpha;
            fx.ddargbScaleFactors.alpha = by8Bit;
            fx.ddargbScaleFactors.red   = 0xFF;
            fx.ddargbScaleFactors.green = 0xFF;
            fx.ddargbScaleFactors.blue  = 0xFF;
            fx.dwFillColor = wFill;
 
            HRESULT hRes = pDst->AlphaBlt(&rDstRECT, NULL, NULL, DDABLT_COLORFILL, &fx); 
            if (hRes == S_OK)
                return;
        }
*/
    #endif

    

    START_TIMER(GDI_BLENDBLT);

    if (iAlpha >= 0xFF)
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pSet = Pixel(iXDst, iYDst+iY);

            #if defined(XSCALE) & !defined(THUMB) & !defined(HL_WIN32)
                ippsSet_16s(wFill, (short*)pSet, iDX);
            #else
                for (int iX = 0; iX < iDX; iX++)
                {
                    *pSet = wFill;
                    pSet++;
                }
            #endif
        }

        return;    
    }

    int iX1 = iXDst;
    int iY1 = iYDst;
    int iX2 = iXDst + iDX;
    int iY2 = iYDst + iDY;
    LOGASSERT(iY1 <= m_iDY);
    LOGASSERT(iY2 <= m_iDY);
    iY1 = __min(iY1, DY());
    iY2 = __min(iY2, DY());

    int iDXBlend = iX2 - iX1;
    int iDYBlend = iY2 - iY1;

    DWORD wRB = 0;
    DWORD wG  = 0;

    WORD wAlpha = MAX(0, iAlpha);
    wAlpha = MIN(255, iAlpha);
    DWORD byAlphaRB = wAlpha * 32 / 255;
    DWORD byAlphaG  = wAlpha * 64 / 255;
    DWORD _byAlphaRB  = 32-byAlphaRB;
    DWORD _byAlphaG   = 64-byAlphaG;


    if (wFill == 0x0000)
    {
        for (int iY = 0; iY < iDYBlend ; iY++)
        {
            HL_PIXEL *pPIX = Pixel(iX1, iY1+iY);

            for (int iX = 0; iX < iDXBlend; iX++)
            {
                HL_PIXEL wTgt = *pPIX;
                wRB = wTgt & 0x0000F81F;
                wG  = wTgt & 0x000007E0;

//                DWORD wRBOut = (((_byAlphaRB * wRB) >> 5) & 0x0000F81F);
//                DWORD wGOut = (((_byAlphaG  * wG) >> 6) & 0x000007E0);
//                *pPIX = (WORD)(wRBOut | wGOut);

                DWORD wRBOut = ((_byAlphaRB * wRB) & 0x001F03E0);
                DWORD wGOut  = ((_byAlphaRB * wG)  & 0x0000FC00);
                *pPIX = (WORD)((wRBOut|wGOut)>>5);
                pPIX++;
            }
        }

        return;
    }

    DWORD wRB_S = wFill & 0x0000F81F;
    DWORD wG_S  = wFill & 0x000007E0;
    wRB_S *= byAlphaRB;
    wG_S  *= byAlphaG;

    for (int iY = 0; iY < iDYBlend ; iY++)
    {
        HL_PIXEL *pPIX = Pixel(iX1, iY1+iY);

        for (int iX = 0; iX < iDXBlend; iX++)
        {
            HL_PIXEL wTgt = *pPIX;
            wRB = wTgt & 0x0000F81F;
            wG  = wTgt & 0x000007E0;
            DWORD wRBOut = (((wRB_S + _byAlphaRB * wRB) >> 5) & 0x0000F81F);
            DWORD wGOut = (((wG_S  + _byAlphaG  * wG)  >> 6) & 0x000007E0);
            *pPIX = (WORD)(wRBOut | wGOut);
            pPIX++;
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::FillEdgeList(
    COLORREF                    rgb,
    int                         iX0,
    int                         iY0,
    CEdgeList                   *pEdges )

/*============================================================================*/
{
    iX0 += m_iXLeft;
    iY0 += m_iYTop;

    CHLRect rClip = LocalClipRect();
    HL_PIXEL wSet = RGBTOHLPIXEL(rgb);

    for (int iEdge = 0; iEdge < pEdges->NPoints(); iEdge++)
    {
        int iX = pEdges->X(iEdge) + iX0;
        int iY = pEdges->Y(iEdge) + iY0;
        int iD = pEdges->D(iEdge);

        if (iY >= rClip.YTop() && iY <= rClip.YBottom())
        {
            if (iX < rClip.XLeft())
            {
                iD += (iX - rClip.XLeft());
                iX = rClip.XLeft();
            }

            iD = __min(iD, rClip.XRight()+1-iX);
            if (iD > 0)
            {
                HL_PIXEL *pIn = Pixel(iX, iY);

                #if defined(XSCALE) & !defined(THUMB) & !defined(HL_WIN32)
                    ippsSet_16s(wSet, (short*)pIn, iD);
                #else
                    for (int iX = 0; iX < iD; iX++)
                    {
                        *pIn= wSet;
                        pIn++;
                    }
                #endif
            }
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::FillEdgeListAlpha(
    CEdgeList                   *pEdges,
    HL_PIXEL                    wFill,
    int                         iAlphaTop,
    int                         iAlphaBtm  )

/*============================================================================*/
{
    START_TIMER(GDI_BLENDBLT);

    DWORD wRB_B = 0;
    DWORD wG_B  = 0;

    int iDAlpha = iAlphaBtm - iAlphaTop;


    if (wFill == 0x0000)
    {
        for (int iEdge = 0; iEdge < pEdges->NPoints(); iEdge++)
        {
            int iX = pEdges->X(iEdge);
            int iY = pEdges->Y(iEdge);
            int iD = pEdges->D(iEdge);
            
            int iAlpha = iAlphaTop + iY * iDAlpha / DY();
            iAlpha = MIN(255, iAlpha);
            DWORD byAlphaRB = (iAlpha >> 3);
            DWORD byAlphaG  = (iAlpha >> 2);
            DWORD _byAlphaRB  = 31-byAlphaRB;
            DWORD _byAlphaG   = 63-byAlphaG;

            DWORD wRA = wFill & 0x0000F800;
            DWORD wGA = wFill & 0x000007E0;
            DWORD wBA = wFill & 0x0000001F;
            wRA *= byAlphaRB;
            wGA *= byAlphaRB;
            wBA *= byAlphaRB;

            HL_PIXEL *pPIX = Pixel(iX, iY);

            for (int iP = 0; iP < iD; iP++)
            {
                HL_PIXEL wTgt = *pPIX;
                wRB_B = wTgt & 0x0000F81F;
                wG_B  = wTgt & 0x000007E0;
                DWORD wRBOut = ((wRB_B * _byAlphaRB) >> 5) & 0x0000F81F;
                DWORD wGOut =  ((wG_B  * _byAlphaG)  >> 6) & 0x000007E0;
                *pPIX = (HL_PIXEL)(wRBOut | wGOut);
                pPIX++;
            }
        }

        return;
    }


    for (int iEdge = 0; iEdge < pEdges->NPoints(); iEdge++)
    {
        int iX = pEdges->X(iEdge);
        int iY = pEdges->Y(iEdge);
        int iD = pEdges->D(iEdge);

        int iAlpha = iAlphaTop + iY * iDAlpha / DY();
        iAlpha = MIN(255, iAlpha);
        DWORD byAlphaRB = iAlpha * 32 / 255;
        DWORD byAlphaG  = iAlpha * 64 / 255;
        DWORD _byAlphaRB  = 32-byAlphaRB;
        DWORD _byAlphaG   = 64-byAlphaG;

        DWORD wRB_A = wFill & 0x0000F81F;
        DWORD wG_A  = wFill & 0x000007E0;
        wRB_A *= byAlphaRB;
        wG_A *= byAlphaG;


        HL_PIXEL *pPIX = Pixel(iX, iY);

        for (int iP = 0; iP < iD; iP++)
        {
            HL_PIXEL wTgt = *pPIX;
            wRB_B = wTgt & 0x0000F81F;
            wG_B  = wTgt & 0x000007E0;
            DWORD wRBOut = (((wRB_A + _byAlphaRB * wRB_B) >> 5) & 0x0000F81F);
            DWORD wGOut  = (((wG_A + _byAlphaG  * wG_B) >> 6)  & 0x000007E0);
            *pPIX = (WORD)(wRBOut | wGOut);
            pPIX++;
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::FillRectAlphaDX(
    CHLRect                     rRect,
    HL_PIXEL                    wFill,
    int                         iAlpha0,
    int                         iAlpha1 )

/*============================================================================*/
{
    int iXDst = rRect.XLeft() + m_iXLeft;
    int iYDst = rRect.YTop() + m_iYTop;
    int iDX = rRect.DX();
    int iDY = rRect.DY();
    int iXSrc = 0;
    int iYSrc = 0;
    
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

    START_TIMER(GDI_BLENDBLT);

    int iX1 = iXDst;
    int iY1 = iYDst;
    int iX2 = iXDst + iDX;
    int iY2 = iYDst + iDY;
    LOGASSERT(iY1 <= m_iDY);
    LOGASSERT(iY2 <= m_iDY);
    iY1 = __min(iY1, DY());
    iY2 = __min(iY2, DY());

    int iDXBlend = iX2 - iX1;
    int iDYBlend = iY2 - iY1;

    DWORD wRB = 0;
    DWORD wGB = 0;
    DWORD wBB = 0;

    int iDAlpha = iAlpha1 - iAlpha0;

    for (int iX = 0; iX < iDXBlend ; iX++)
    {
        int iAlpha = iAlpha0 + (iX * iDAlpha) / iDXBlend; 
        WORD wAlpha = MAX(0, iAlpha);
        wAlpha = MIN(255, iAlpha);
        DWORD byAlphaRB = iAlpha * 32 / 255;
        DWORD byAlphaG  = iAlpha * 64 / 255;
        DWORD _byAlphaRB  = 32-byAlphaRB;
        DWORD _byAlphaG   = 64-byAlphaG;

        HL_PIXEL *pPIX = Pixel(iX1+iX, iY1);

        DWORD wRA = wFill & 0x0000F800;
        DWORD wGA = wFill & 0x000007E0;
        DWORD wBA = wFill & 0x0000001F;
        wRA *= byAlphaRB;
        wGA *= byAlphaG;
        wBA *= byAlphaRB;

        for (int iY = 0; iY < iDYBlend; iY++)
        {
            HL_PIXEL wTgt = *pPIX;
            wRB = wTgt & 0x0000F800;
            wGB = wTgt & 0x000007E0;
            wBB = wTgt & 0x0000001F;
            DWORD wROut = (((wRA + _byAlphaRB * wRB) >> 5) & 0x0000F800);
            DWORD wGOut = (((wGA + _byAlphaG  * wGB) >> 6) & 0x000007E0);
            DWORD wBOut =  ((wBA + _byAlphaRB * wBB) >> 5);
            *pPIX = (WORD)(wROut | wGOut  | wBOut);
            pPIX += m_iDX;
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::FillRectAlphaDY(
    CHLRect                     rRect,
    HL_PIXEL                    wFill,
    int                         iAlpha0,
    int                         iAlpha1,
    int                         iRUpper,
    int                         iRLower )

/*============================================================================*/
{
    rRect.Translate(m_iXLeft, m_iYTop);

    int iXRect  = rRect.XLeft();
    int iYRect  = rRect.YTop();
    int iDXRect = rRect.DX();
    int iDYRect = rRect.DY();

    CHLRect rClip = LocalClipRect();

    int iYMin = rRect.YTop();
    int iYMax = iYMin + iDYRect;
    int iXMin = rRect.XLeft();
    int iXMax = iXMin + iDXRect;

    iYMin = __max(iYMin, rClip.YTop());
    iYMax = __min(iYMax, rClip.YBottom()+1);
    iYMax = __min(iYMax, m_iDY);
    iXMin = __max(iXMin, rClip.XLeft());
    iXMax = __min(iXMax, rClip.XRight()+1);
    iXMax = __min(iXMax, m_iDX);
    
    int iRad2U = iRUpper * iRUpper;
    int iRad2L = iRLower * iRLower;

    if (m_iAttribs & DIBATTRIB_HIRESBLEND)
    {
        iAlpha0 = iAlpha0 * 256 / 255;
        iAlpha1 = iAlpha1 * 256 / 255;

        int iRErrorSum = 0;
        int iGErrorSum = 0;
        int iBErrorSum = 0;

        int iRA16 = (wFill & 0x0000F800);
        int iGA16 = (wFill & 0x000007E0);
        int iBA16 = (wFill & 0x0000001F);

        for (int iY = 0; iY < iDYRect; iY++)
        {
            int iYTgt = iY + iYRect;
            if (iYTgt >= iYMin && iYTgt < iYMax)
            {
                int iXRad = 0;
                if (iY < iRUpper)
                {
                    int iYR = iRUpper - iY;
                    iXRad = iRUpper - iSqrt(iRad2U - iYR*iYR);
                }
                else if (iY > iDYRect-iRLower)
                {
                    int iYR = iY - (iDYRect - iRLower);
                    iXRad = iRLower - iSqrt(iRad2L - iYR*iYR);
                }

                int iX0 = iXRect + iXRad;
                int iX1 = iXRect + iDXRect - iXRad;
                iX0 = __max(iX0, iXMin);
                iX1 = __min(iX1, iXMax);
                HL_PIXEL *pFill = Pixel(iX0, iYTgt);

                int iAlpha = iAlpha0 + (iY * (iAlpha1-iAlpha0)) / iDYRect; 
                int _iAlpha = 256-iAlpha;


                DWORD wRA24 = (iRA16 * iAlpha); // Red overlay component in place at RSH by 11
                DWORD wGA24 = (iGA16 * iAlpha); // Grn overlay component in place at RSH by 5
                DWORD wBA24 = (iBA16 * iAlpha); // Blu overlay component in place at RSH by 0

                HL_PIXEL *pPIX = Pixel(iX0, iYTgt);
                int iN = iX1-iX0;

                while (iN > 0)
                {
                    HL_PIXEL wTgt = *pPIX;
                    DWORD wRB24 = ((wTgt & 0x0000F800) * _iAlpha);    // Red target component in place at RSH by 11
                    DWORD wGB24 = ((wTgt & 0x000007E0) * _iAlpha);    // Grn target component in place at RSH by 5
                    DWORD wBB24 = ((wTgt & 0x0000001F) * _iAlpha);    // Blu target component in place at RSH by 0

                    int iROut24 = (wRB24 + wRA24);  // Compute in place Red result (x256)
                    int iGOut24 = (wGB24 + wGA24);  // Compute in place Red result (x256)
                    int iBOut24 = (wBB24 + wBA24);  // Compute in place Red result (x256)

                    int iR16 = ((iROut24 >> 8) & 0xF800);   // Shift down to 16 bit location
                    int iG16 = ((iGOut24 >> 8) & 0x07E0);   // Shift down to 16 bit location 
                    int iB16 = iBOut24 >> 8;                // Shift down to 16 bit location

                    //
                    // Check Error Limits
                    //
                    if (iRErrorSum > 0x00400000) iR16 -= 0x800; else if (iRErrorSum < -0x00400000)   iR16 += 0x800;
                    if (iGErrorSum > 0x00080000) iG16 -= 0x020; else if (iGErrorSum < -0x00080000)   iG16 += 0x020;
                    if (iBErrorSum > 0x00000800) iB16--;        else if (iBErrorSum < -0x00000800)   iB16++;

                    //
                    // Update Error Sums
                    //
                    iRErrorSum += ((iR16 << 8) - iROut24);
                    iGErrorSum += ((iG16 << 8) - iGOut24);
                    iBErrorSum += ((iB16 << 8) - iBOut24);

                    *pPIX = (WORD)(iR16|iG16|iB16);
                    pPIX++;
                    iN--;
                }
            }
        }
    }   
    else
    {
        for (int iY = 0; iY < iDYRect; iY++)
        {
            int iYTgt = iY + iYRect;
            if (iYTgt >= iYMin && iYTgt < iYMax)
            {
                int iXRad = 0;
                if (iY < iRUpper)
                {
                    int iYR = iRUpper - iY;
                    iXRad = iRUpper - iSqrt(iRad2U - iYR*iYR);
                }
                else if (iY > iDYRect-iRLower)
                {
                    int iYR = iY - (iDYRect - iRLower);
                    iXRad = iRLower - iSqrt(iRad2L - iYR*iYR);
                }

                int iX0 = iXRect + iXRad;
                int iX1 = iXRect + iDXRect - iXRad;
                iX0 = __max(iX0, iXMin);
                iX1 = __min(iX1, iXMax);
                HL_PIXEL *pFill = Pixel(iX0, iYTgt);

                HL_PIXEL *pPIX = Pixel(iX0, iYTgt);
                int iN = iX1-iX0;

                int iAlpha = iAlpha0 + (iY * (iAlpha1-iAlpha0)) / iDYRect; 
                WORD wAlpha = MAX(0, iAlpha);
                wAlpha = MIN(255, iAlpha);
                DWORD byAlphaRB = wAlpha * 32 / 255;
                DWORD byAlphaG  = wAlpha * 64 / 255;
                DWORD _byAlphaRB  = 32-byAlphaRB;
                DWORD _byAlphaG   = 64-byAlphaG;

                DWORD wRB_S = (wFill & 0x0000F81F) * byAlphaRB;
                DWORD wG_S  = (wFill & 0x000007E0) * byAlphaG;

                if (wFill == 0x0000)
                {
                    while (iN > 0)
                    {
                        DWORD wTgt = *pPIX;
                        switch (wTgt)
                        {
                        case 0x0000:
                            break;
                        default:    
                            {
                                DWORD wRB_T = wTgt & 0x0000F81F;
                                DWORD wG_T  = wTgt & 0x000007E0;
                                DWORD wRB_Out = (((_byAlphaRB * wRB_T) >> 5) & 0x0000F81F);
                                DWORD wG_Out =  (((_byAlphaG  * wG_T) >> 6) & 0x000007E0);
                                *pPIX = (WORD)(wRB_Out|wG_Out);
                            }
                            break;
                        }
                        pPIX++;
                        iN--;
                    }
                }
                else
                {
                    while (iN > 0)
                    {
                        HL_PIXEL wTgt = *pPIX;
                        DWORD wRB_T = wTgt & 0x0000F81F;
                        DWORD wG_T  = wTgt & 0x000007E0;

                        DWORD wRB_Out = ((((_byAlphaRB * wRB_T + wRB_S) >> 5)) & 0x0000F81F);
                        DWORD wG_Out = ((((_byAlphaG  * wG_T + wG_S) >> 6)) & 0x000007E0);

                        *pPIX = (WORD)(wRB_Out | wG_Out);
                        pPIX++;
                        iN--;
                    }
                }
            }
        }
    }


    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::FillRoundRectAlpha(
    CHLRect                     rRect,
    COLORREF                    wFillT,
    COLORREF                    wFillB,
    COLORREF                    wFillC,
    int                         iAlpha,
    int                         iAlphaC,
    int                         iRadOuter )

/*============================================================================*/
{
    CHLDibDC temp1(rRect.DX(), rRect.DY(), 0, 0, MEMORY_SYSTEM_DISCARDABLE);
    CHLDibDC temp2(rRect.DX(), rRect.DY(), 0, 0, MEMORY_SYSTEM_DISCARDABLE);
    temp2.BltFromDibDC(0, 0, this, rRect.XLeft(), rRect.YTop(), rRect.DX(), rRect.DY());
    temp1.Attribs(DIBATTRIB_HIRESBLEND);
    
    if (iAlphaC > 0)
    {
        CDXYRect rB = CDXYRect(0, 0, rRect.DX(), rRect.DY());
        CHLRect rT  = rB.BreakOffTop(rB.DY()/2, 0);
        COLORREF rgbFillC = CHLDC::BlendPCT(wFillT, wFillB, 50);
        rgbFillC = CHLDC::BlendPCT(rgbFillC, wFillC, iAlphaC);

        temp1.GradientFill(rT, wFillT, rgbFillC, DIR_DOWN);
        temp1.GradientFill(rB, rgbFillC, wFillB, DIR_DOWN);
    }
    else
    {
        temp1.GradientFill(CDXYRect(0, 0, rRect.DX(), rRect.DY()), wFillT, wFillB, DIR_DOWN);
    }

    CEdgeList edges(CEdgeList::EDGE_HORIZONTAL);
    edges.CreateForRoundRectExt(rRect.DX(), rRect.DY(), 0, iRadOuter, DIR_NONE);
    BlendBltPCT(&temp1, rRect.XLeft(), rRect.YTop(), iAlpha);
    BltFromDibDC(&temp2, rRect.XLeft(), rRect.YTop(), 0, 0, &edges);
}
/*============================================================================*/

    void                        CHLDibDC::GradientFillCurve(
    CHLRect                     rRect,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iDir )

/*============================================================================*/
{
    COLORREF _rgb1 = rgb1;
    COLORREF _rgb2 = rgb2;
    if (iDir == DIR_LEFT)
    {
        iDir = DIR_RIGHT;
        rgb2 = _rgb1;
        rgb1 = _rgb2;
    }
    if (iDir == DIR_UP)
    {
        iDir = DIR_DOWN;
        rgb2 = _rgb1;
        rgb1 = _rgb2;
    }

    int iPCT = 50;
    
    int iDim = 0;
    if (iDir == DIR_DOWN)
    {
        iDim = rRect.DY();
    }
    else
    {
        iDim = rRect.DX();
    }
    
    CNonLinearColorMap map(iDim, iDim * iPCT / 100, rgb1, rgb2);
    GradientFill(rRect, rgb1, rgb2, iDir, &map);
}
/*============================================================================*/

    void                        CHLDibDC::GradientFill(
    CHLRect                     rRect,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iDir,
    CColorMap                   *pMap )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        rRect.Translate(m_iXLeft, m_iYTop);
        m_pParentDIB->GradientFill(rRect, rgb1, rgb2, iDir, pMap);
        return;
    }
    
#ifdef IPHONE
    int iRT = GetRValue(rgb1);
    int iGT = GetGValue(rgb1);
    int iBT = GetBValue(rgb1);
    int iRB = GetRValue(rgb2);
    int iGB = GetGValue(rgb2);
    int iBB = GetBValue(rgb2);
    
    int iRBT = iRT | (iBT << 16);
    iGT  = iGT << 8;
    int iRBB = iRB | (iBB << 16);
    iGB  = iGB << 8;
        
    int iDX = rRect.DX();
    int iDY = rRect.DY();
    
    for (int iY = 0; iY < iDY; iY++)
    {
        int iY255 = iY * 0xFF / iDY;
        int _iY255 = 0xFF - iY255;
        DWORD dwRG = ((iRBT * _iY255) + (iRBB * iY255)) & 0xFF00FF00;
        DWORD dwG  = ((iGT * _iY255) + (iGB * iY255))   & 0x00FF0000;
        HL_PIXEL wSet = dwRG|dwG|0xFF;
        HL_PIXEL *pSet = Pixel(rRect.XLeft(), rRect.YTop()+iY);
        for (int iX = 0; iX < iDX; iX++)
        {
            *pSet = wSet;
            pSet++;
        }

    }
#else
    if (iDir == DIR_LEFT || iDir == DIR_UP)
    {
        COLORREF temp = rgb1;
        rgb1 = rgb2;
        rgb2 = temp;
    }

    int iXDst = rRect.XLeft();
    int iYDst = rRect.YTop();
    int iDX = rRect.DX();
    int iDY = rRect.DY();
    int iXSrc = 0;
    int iYSrc = 0;

    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

    START_TIMER(GDI_TEXTURIZE);

    //
    // Make Red & Blue ranges 0-248 for 0-31 * 8
    //
    int iR1 = GetRValue(rgb1);
    int iG1 = GetGValue(rgb1);
    int iB1 = GetBValue(rgb1);
    int iR2 = GetRValue(rgb2);
    int iG2 = GetGValue(rgb2);
    int iB2 = GetBValue(rgb2);

    int iDR = iR2-iR1;
    int iDG = iG2-iG1;
    int iDB = iB2-iB1;

    int iDMajor = iDY;
    int iDMinor = iDX;
    int iStepMajor = m_iDX;
    int iStepMinor = 1;
    if (iDir == DIR_LEFT || iDir == DIR_RIGHT)
    {   
        iDMajor = iDX;
        iDMinor = iDY;
        iStepMajor = 1;
        iStepMinor = m_iDX;
    }

    HL_PIXEL *pPIX = Pixel(iXDst, iYDst);

    int iROut = 0;
    int iGOut = 0;
    int iBOut = 0;

    CDitherMap map;
    
    for (int iMajor = 0; iMajor < iDMajor; iMajor++)
    {
        // 24 Bit Target Color for this row
        if (pMap != NULL)
        {
            COLORREF rgb = pMap->ColorAtPosition(iMajor);
            iROut = GetRValue(rgb);
            iGOut = GetGValue(rgb);
            iBOut = GetBValue(rgb);
//            pMap->GetRGB(iMajor, &iROut, &iGOut, &iBOut);
        }
        else
        {
            iROut = iR1 + iDR * iMajor / iDMajor; // 0-248
            iGOut = iG1 + iDG * iMajor / iDMajor; // 0-252
            iBOut = iB1 + iDB * iMajor / iDMajor; // 0-248
        }

        map.Set(iROut, iGOut, iBOut);

//        CDitherMap map(RGB(iROut, iGOut, iBOut), bOdd);

        switch (iDir)
        {
        case DIR_UP:
        case DIR_DOWN:
            {
                map.FillHZ(pPIX, iDX);
            }
            break;

        case DIR_LEFT:
        case DIR_RIGHT:
            {
                HL_PIXEL *pTgt = pPIX;
                for (int iMinor = 0; iMinor < iDMinor; iMinor++)
                {
                    *pTgt = map.NextColor();
                    pTgt += iStepMinor;
                }
            }
            break;
        }                            

        pPIX += iStepMajor;
    }

    END_TIMER(GDI_TEXTURIZE);
#endif
}
/*============================================================================*/

    void                        CHLDibDC::BlendEdgeList(
    int                         iX0,
    int                         iY0,
    COLORREF                    rgb,
    int                         iAlphaPCT,
    CEdgeList                   *pEdges )

/*============================================================================*/
{
    iX0 += m_iXLeft;
    iY0 += m_iYTop;

    HL_PIXEL wFill = RGBTOHLPIXEL(rgb);
    DWORD wRB_S = wFill & 0x0000F81F;
    DWORD wG_S  = wFill & 0x000007E0;

    DWORD byAlphaRB = iAlphaPCT * 32 / 100;
    DWORD byAlphaG  = iAlphaPCT * 64 / 100;
    DWORD _byAlphaRB  = 32-byAlphaRB;
    DWORD _byAlphaG   = 64-byAlphaG;

    wRB_S *= byAlphaRB;
    wG_S  *= byAlphaG;

    CHLRect rClip = LocalClipRect();     

    for (int i = 0; i < pEdges->NPoints(); i++)
    {
        int iX0Edge = pEdges->X(i);
        int iY0Edge = pEdges->Y(i);
        int iYOut = iY0 + iY0Edge;
        int iXOut = iX0 + iX0Edge;

        if (iYOut >= rClip.YTop() && iYOut <= rClip.YBottom())
        {
            int iD = pEdges->D(i);
            if (iXOut < rClip.XLeft())
            {
                iD += (iXOut-rClip.XLeft());
                iXOut = rClip.XLeft();
            }
            iD = __min(iD, rClip.XRight()-iXOut+1);

            if (iD > 0)
            {
                HL_PIXEL *pPIX = Pixel(iXOut, iYOut);

                for (int iPix = 0; iPix < iD; iPix++)
                {
                    HL_PIXEL wTgt = *pPIX;
                    DWORD wRB = wTgt & 0x0000F81F;
                    DWORD wG  = wTgt & 0x000007E0;
                    DWORD wRBOut = (((wRB_S + _byAlphaRB * wRB) >> 5) & 0x0000F81F);
                    DWORD wGOut = (((wG_S  + _byAlphaG  * wG)  >> 6) & 0x000007E0);
                    *pPIX = (WORD)(wRBOut | wGOut);
                    pPIX++;
                }
            }
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::BlendBltPCT(
    CHLDibDC                    *pSrcDib,
    int                         iXDst,
    int                         iYDst,
    int                         iPCTSrc )

/*============================================================================*/
{
    BlendBltPCT(pSrcDib, 0, 0, iXDst, iYDst, pSrcDib->DX(), pSrcDib->DY(), iPCTSrc);
}
/*============================================================================*/

    void                        CHLDibDC::BlendBltPCTEdgeList(
    CHLDibDC                    *pSrcDib,
    int                         iXDst,
    int                         iYDst,
    CEdgeList                   *pEdgeList,
    int                         iPCTSrc,
    int                         iFlags )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        m_pParentDIB->BlendBltPCTEdgeList(pSrcDib, iXDst+m_iXLeft, iYDst+m_iYTop, pEdgeList, iPCTSrc, iFlags);
        return;
    }

    START_TIMER(GDI_BLENDBLT);

    HL_PIXEL wIn1 = 0x0000;
    HL_PIXEL wIn2 = 0x0000;
    HL_PIXEL wOut = 0x0000;
    UINT wR1 = 0;
    UINT wG1 = 0;
    UINT wB1 = 0;
    UINT wR2 = 0;
    UINT wG2 = 0;
    UINT wB2 = 0;

    int iStep1 = 1;
    int iStep2 = 1;
    CEdgeList::EDGE_TYPE Type = pEdgeList->Type();
    if (Type == CEdgeList::EDGE_VERTICAL)
    {
        iStep1 = pSrcDib->DXPitch();
        iStep2 = DXPitch();
    }

    UINT wMult1 = 256 * iPCTSrc / 100;
    UINT wMult2 = 256 - wMult1;

#ifndef HL_PIXEL32
    UINT wMult1R = wMult1 * 8;
    UINT wMult2R = wMult2 * 8;
    UINT wMult1G = wMult1 / 8;
    UINT wMult2G = wMult2 / 8;
#endif

    CHLRect rClip = LocalClipRect();
    DWORD dwSrc[400];
    DWORD dwTgt[400];

    int iA = 16 * iPCTSrc / 100;
    int _iA = 16 - iA;

    for (int iEdge = 0; iEdge < pEdgeList->NPoints(); iEdge++)
    {
        int iXEdge  = pEdgeList->X(iEdge);
        int iYEdge  = pEdgeList->Y(iEdge);
        int iN      = pEdgeList->D(iEdge);
        int iXEdgeSrc   = iXEdge;
        int iYEdgeSrc   = iYEdge;

        int iXOut = iXEdge + iXDst + m_iXLeft;
        int iYOut = iYEdge + iYDst + m_iYTop;
        if (iYOut >= rClip.YTop() && iYOut <= rClip.YBottom())
        {
            if (iXOut < rClip.XLeft())
            {
                iXEdgeSrc += (rClip.XLeft()-iXOut);
                iN        -= (rClip.XLeft()-iXOut);
                iXOut      = rClip.XLeft();
            }
        
            iN = __min(iN, rClip.XRight()-iXOut);

            // Basic checking
            BOOL bOK = TRUE;
            if (iXEdge < 0)                 bOK = FALSE;
            if (iYEdge < 0)                 bOK = FALSE;
            if (iXEdgeSrc < 0)              bOK = FALSE;
            if (iYEdgeSrc < 0)              bOK = FALSE;
            if (iYEdge >= DY())             bOK = FALSE;
            if (iYEdgeSrc >= pSrcDib->DY()) bOK = FALSE;

            if (Type == CEdgeList::EDGE_HORIZONTAL)
            {
                iN = __min(iN, DX()-iXEdge);
                iN = __min(iN, pSrcDib->DX()-iXEdgeSrc);
            }
            else
            {
                iN = __min(iN, DY()-iYEdge);
                iN = __min(iN, pSrcDib->DY()-iYEdgeSrc);
            }

            if (iN <= 0)       
                bOK = FALSE;
            
            if (bOK)
            {
                HL_PIXEL *pPIX1 = pSrcDib->Pixel(iXEdgeSrc, iYEdgeSrc);
                HL_PIXEL *pPIX2 = Pixel(iXOut, iYOut);
                memcpy(dwSrc, pPIX1, iN * sizeof(HL_PIXEL));
                memcpy(dwTgt, pPIX2, iN * sizeof(HL_PIXEL));

                for (int i = 0; i < iN; i++)
                {
                    DWORD pmmSrc = dwSrc[i];
                    DWORD pmmDst = dwTgt[i];

                    DWORD dwS = (pmmSrc & A_MASK_32) * iA;
                    DWORD dwD = (pmmDst & A_MASK_32) * _iA;
                    DWORD dwRes = (dwS + dwD) >> 4;
                    dwRes &= A_MASK_32;


                    dwS = (pmmSrc & B_MASK_32) >> 4;
                    dwD = (pmmDst & B_MASK_32) >> 4;
                    dwS = dwS * iA;
                    dwD = dwD * _iA;
                    DWORD dwRes2 = dwS + dwD;
                    dwRes2 &= B_MASK_32;

                    dwRes |= dwRes2;

                    // Done, save result
                    dwTgt[i] = dwRes;
                }

                memcpy(pPIX2, dwTgt, iN * sizeof(HL_PIXEL));
            }
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
#pragma optimize("t", on)
/*============================================================================*/

    void                        CHLDibDC::BlendBltPCT(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iPCTSrc )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        m_pParentDIB->BlendBltPCT(pSrcDib, iXSrc, iYSrc, iXDst+m_iXLeft, iYDst+m_iYTop, iDX, iDY, iPCTSrc);
        return;
    }

    if (iPCTSrc >= 100)
    {
        BltFromDibDC(iXDst, iYDst, pSrcDib, iXSrc, iYSrc, iDX, iDY);
        return;
    }

    if (iPCTSrc <= 0)
        return;

    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pSrcDib))
        return;

    #if defined(HLDDRAW) & defined(UNDER_CE) & defined(HL_DDRAWBASIC)
        CDXYRect rSrc(iXSrc, iYSrc, iDX, iDY);
        CDXYRect rDst(iXDst, iYDst, iDX, iDY);
        RECT rSrcRECT = rSrc.GetRECT();
        RECT rDstRECT = rDst.GetRECT();
        HL_DIRECTDRAWSURFACE pDst = GetSurface();
        if (pDst != NULL)
        {
            HL_DIRECTDRAWSURFACE pSrc = pSrcDib->GetSurface();
            if (pSrc != NULL)
            {
                DDALPHABLTFX fx;
                memset(&fx, 0, sizeof(DDALPHABLTFX));
                fx.dwSize = sizeof(DDALPHABLTFX);
                int i8Bit = (iPCTSrc * 255 / 100);
                BYTE by8Bit = (BYTE)i8Bit;
                fx.ddargbScaleFactors.alpha = by8Bit;
                fx.ddargbScaleFactors.red   = 0xFF;
                fx.ddargbScaleFactors.green = 0xFF;
                fx.ddargbScaleFactors.blue  = 0xFF;
                HRESULT hRes = pDst->AlphaBlt(&rDstRECT, pSrc, &rSrcRECT, 0, &fx); 
                if (hRes == S_OK)
                    return;
            }
        }
    #endif

#ifdef HL_MMX
    BlendBltMMX(pSrcDib, iXSrc, iYSrc, iXDst, iYDst, iDX, iDY, iPCTSrc);
    return;
#endif

#ifdef HL_MMINTRIN
    BlendBltMMINTRIN(pSrcDib, iXDst, iYDst, iXSrc, iYSrc, iDX, iDY, iPCTSrc);
    return;
#endif

    START_TIMER(GDI_BLENDBLT);

    // Allocate these and memcpy so everything is aligned correctly
    int iDX2 = WORDAlign(iDX);
    DWORD *pmmSrcRow = new DWORD[iDX2/2];   // Each of these will do 4 pixels
    DWORD *pmmTgtRow = new DWORD[iDX2/2];   // Each of these will do 4 pixels

    HL_PIXEL *pSrcRow = pSrcDib->Pixel(iXSrc, iYSrc);
    HL_PIXEL *pTgtRow = Pixel(iXDst, iYDst);

    int iSrcPitch = pSrcDib->DXPitch();
    int iTgtPitch = DXPitch();    

    int iA = 16 * iPCTSrc / 100;
    int _iA = 16 - iA;

    for (int iY = 0; iY < iDY; iY++)
    {
        memcpy(pmmSrcRow, pSrcRow, iDX * sizeof(HL_PIXEL));
        memcpy(pmmTgtRow, pTgtRow, iDX * sizeof(HL_PIXEL));

        int iN = iDX2 / 2;

        //
        // Each pass here will do 2 pixels
        //
        for (int iX = 0; iX < iN; iX++)
        {
            //
            // Load up 4 source and dest pixels
            //
            DWORD pmmSrc = pmmSrcRow[iX];
            DWORD pmmDst = pmmTgtRow[iX];

            DWORD dwS = (pmmSrc & A_MASK_32) * iA;
            DWORD dwD = (pmmDst & A_MASK_32) * _iA;
            DWORD dwRes = (dwS + dwD) >> 4;
            dwRes &= A_MASK_32;


            dwS = (pmmSrc & B_MASK_32) >> 4;
            dwD = (pmmDst & B_MASK_32) >> 4;
            dwS = dwS * iA;
            dwD = dwD * _iA;
            DWORD dwRes2 = dwS + dwD;
            dwRes2 &= B_MASK_32;

            dwRes |= dwRes2;

            // Done, save result
            pmmTgtRow[iX] = dwRes;
        }

        // Copy whole row back to target
        memcpy(pTgtRow, pmmTgtRow, iDX * sizeof(HL_PIXEL));

        pSrcRow   += iSrcPitch;
        pTgtRow   += iTgtPitch;
    }
    delete [] pmmSrcRow;
    delete [] pmmTgtRow;

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::BlendBltPCT(
    CHLDibDC                    *pSrcDib1,
    CHLDibDC                    *pSrcDib2,
    int                         iXSrc1,
    int                         iYSrc1,
    int                         iXSrc2,
    int                         iYSrc2,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iPCTSrc1 )

/*============================================================================*/
{
    if (m_pParentDIB != NULL)
    {
        m_pParentDIB->BlendBltPCT(pSrcDib1, pSrcDib2, iXSrc1, iYSrc1, iXSrc2, iYSrc2, iXDst+m_iXLeft, iYDst+m_iYTop, iDX, iDY, iPCTSrc1);
        return;
    }

    if (iPCTSrc1 >= 100)
    {
        BltFromDibDC(iXDst, iYDst, pSrcDib1, iXSrc1, iYSrc1, iDX, iDY);
        return;
    }

    if (iPCTSrc1 <= 0)
    {
        BltFromDibDC(iXDst, iYDst, pSrcDib2, iXSrc2, iYSrc2, iDX, iDY);
        return;
    }

    iXDst += m_iXLeft;
    iYDst += m_iYTop;

    if (iXDst > 0)
        int iTest= 1;

    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc1, &iYSrc1, pSrcDib1))
        return;
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc2, &iYSrc2, pSrcDib2))
        return;

    if (iDY > 50)
        int iTest = 1;

    START_TIMER(GDI_BLENDBLT);

    HL_PIXEL *pSrcRow1 = pSrcDib1->Pixel(iXSrc1, iYSrc1);
    HL_PIXEL *pSrcRow2 = pSrcDib2->Pixel(iXSrc2, iYSrc2);
    HL_PIXEL *pTgtRow = Pixel(iXDst, iYDst);

    int iA1 = 16 * iPCTSrc1 / 100;
    int iA2 = 16 - iA1;

    long lSrc1 = (long)pSrcRow1;
    long lSrc2 = (long)pSrcRow2;
    long lTgt  = (long)pTgtRow;
    BOOL bAligned = TRUE;
    if (lSrc1 & 0x03)   bAligned = FALSE;
    if (lSrc2 & 0x03)   bAligned = FALSE;
    if (lTgt  & 0x03)   bAligned = FALSE;
    if (iDX   & 0x01)   bAligned = FALSE;


    if (bAligned)
    {
        // Allocate these and memcpy so everything is aligned correctly
        int iSrcPitch1 = pSrcDib1->DXPitch();
        int iSrcPitch2 = pSrcDib2->DXPitch();
        int iTgtPitch = DXPitch();    


        for (int iY = 0; iY < iDY; iY++)
        {
            int iN = iDX / 2;
            DWORD *pTgt  = (DWORD*)pTgtRow;
            DWORD *pSrc1 = (DWORD*)pSrcRow1;
            DWORD *pSrc2 = (DWORD*)pSrcRow2;

            //
            // Each pass here will do 2 pixels
            //
            while (iN > 0)
            {
                DWORD pmmSrc1 = *pSrc1;
                DWORD pmmSrc2 = *pSrc2;
                DWORD dwS1 = (pmmSrc1 & A_MASK_32) * iA1;
                DWORD dwS2 = (pmmSrc2 & A_MASK_32) * iA2;
                DWORD dwRes = (dwS1 + dwS2) >> 4;
                dwRes &= A_MASK_32;
                dwS1 = (pmmSrc1 & B_MASK_32) >> 4;
                dwS2 = (pmmSrc2 & B_MASK_32) >> 4;
                dwS1 = dwS1 * iA1;
                dwS2 = dwS2 * iA2;
                DWORD dwRes2 = dwS1 + dwS2;
                dwRes2 &= B_MASK_32;
                dwRes |= dwRes2;

                // Done, save result
                *pTgt = dwRes;

                // Move to next 2 pixels
                pTgt++;
                pSrc1++;
                pSrc2++;
                iN--;
            }

            pSrcRow1   += iSrcPitch1;
            pSrcRow2   += iSrcPitch2;
            pTgtRow   += iTgtPitch;
        }
        return;
    }

    // Allocate these and memcpy so everything is aligned correctly
    int iDX2 = WORDAlign(iDX);
    DWORD *pmmSrcRow1 = new DWORD[iDX2/2];   // Each of these will do 4 pixels
    DWORD *pmmSrcRow2 = new DWORD[iDX2/2];   // Each of these will do 4 pixels
    DWORD *pmmTgtRow  = new DWORD[iDX2/2];   // Each of these will do 4 pixels

    int iSrcPitch1 = pSrcDib1->DXPitch();
    int iSrcPitch2 = pSrcDib2->DXPitch();
    int iTgtPitch = DXPitch();    

    for (int iY = 0; iY < iDY; iY++)
    {
        memcpy(pmmSrcRow1, pSrcRow1, iDX * sizeof(HL_PIXEL));
        memcpy(pmmSrcRow2, pSrcRow2, iDX * sizeof(HL_PIXEL));

        int iN = iDX2 / 2;

        //
        // Each pass here will do 2 pixels
        //
        for (int iX = 0; iX < iN; iX++)
        {
            DWORD pmmSrc1 = pmmSrcRow1[iX];
            DWORD pmmSrc2 = pmmSrcRow2[iX];
            DWORD dwS1 = (pmmSrc1 & A_MASK_32) * iA1;
            DWORD dwS2 = (pmmSrc2 & A_MASK_32) * iA2;
            DWORD dwRes = (dwS1 + dwS2) >> 4;
            dwRes &= A_MASK_32;
            dwS1 = (pmmSrc1 & B_MASK_32) >> 4;
            dwS2 = (pmmSrc2 & B_MASK_32) >> 4;
            dwS1 = dwS1 * iA1;
            dwS2 = dwS2 * iA2;
            DWORD dwRes2 = dwS1 + dwS2;
            dwRes2 &= B_MASK_32;
            dwRes |= dwRes2;

            // Done, save result
            pmmTgtRow[iX] = dwRes;
        }

        // Copy whole row back to target
        memcpy(pTgtRow, pmmTgtRow, iDX * sizeof(HL_PIXEL));

        pSrcRow1   += iSrcPitch1;
        pSrcRow2   += iSrcPitch2;
        pTgtRow   += iTgtPitch;
    }
    delete [] pmmSrcRow1;
    delete [] pmmSrcRow2;
    delete [] pmmTgtRow;

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::BlendBltAlphaDY(
    CHLDibDC                    *pSrcDib,
    CHLRect                     rDst,
    CHLRect                     rSrc,
    int                         iAlpha0,
    int                         iAlpha1 )

/*============================================================================*/
{
    int iDAlpha = iAlpha1-iAlpha0;
    int iYDstOrig = rDst.YTop() + m_iYTop;
    int iDYOrig = rDst.DY();

    int iXDst = rDst.XLeft() + m_iXLeft;
    int iYDst = rDst.YTop() + m_iYTop;
    int iDX = rDst.DX();
    int iDY = rDst.DY();
    int iXSrc = rSrc.XLeft();
    int iYSrc = rSrc.YTop();

    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pSrcDib))
        return;

    START_TIMER(GDI_BLENDBLT);

    int iX1 = iXDst;
    int iY1 = iYDst;
    int iX2 = iXDst + iDX;
    int iY2 = iYDst + iDY;
    LOGASSERT(iY1 <= m_iDY);
    LOGASSERT(iY2 <= m_iDY);
    iY1 = __min(iY1, DY());
    iY2 = __min(iY2, DY());

    int iDXBlend = iX2 - iX1;
    int iDYBlend = iY2 - iY1;

    DWORD wRB_S = 0;
    DWORD wG_S  = 0;
    DWORD wRB_T = 0;
    DWORD wG_T  = 0;




    if (((iXSrc % 2) == 0) && ((iX1 % 2) == 0))
    {
        //
        // 2 Pixels at a time
        //
        DWORD wR1, wG1, wB1;
        DWORD wR2, wG2, wB2;
        DWORD wR,  wG,  wB;

        int iNRem = iDXBlend % 2;

        for (int iY = 0; iY < iDYBlend ; iY++)
        {
            DWORD *pPIX1 = (DWORD*)pSrcDib->Pixel(iXSrc, iYSrc+iY);
            DWORD *pPIX2 = (DWORD*)Pixel(iX1, iY1+iY);
            int iDXBlend2 = iDXBlend / 2;
            DWORD wAlpha = (DWORD)(iAlpha0 + ((iY+iY1)-iYDstOrig) * iDAlpha / iDYOrig);
            int byAlpha1 = wAlpha * 128 / 255;
            int byAlpha2 = 128 - byAlpha1;

            for (int iX = 0; iX < iDXBlend2; iX++)
            {
                DWORD wSrc1 = *pPIX1;
                DWORD wSrc2 = *pPIX2;

                wR1 = wSrc1 & 0xF800F800;
                wG1 = wSrc1 & 0x07E007E0;
                wB1 = wSrc1 & 0x001F001F;
                wR2 = wSrc2 & 0xF800F800;
                wG2 = wSrc2 & 0x07E007E0;
                wB2 = wSrc2 & 0x001F001F;
                wR1 = wR1 >> 7;
                wR2 = wR2 >> 7;
                wG1 = wG1 >> 2;
                wG2 = wG2 >> 2;

                wR =  (wR1 * byAlpha1 + wR2 * byAlpha2)       & 0xF800F800;
                wG = ((wG1 * byAlpha1 + wG2 * byAlpha2) >> 5) & 0x07E007E0;
                wB = ((wB1 * byAlpha1 + wB2 * byAlpha2) >> 7) & 0x001F001F;

                *pPIX2 = (wR|wG|wB);

                pPIX1++;
                pPIX2++;
            }

            int iNRemThis = iNRem;
            if (iNRemThis > 0)
            {
                HL_PIXEL *p1 = pSrcDib->Pixel(iXSrc+iDXBlend2*2, iYSrc+iY);
                HL_PIXEL *p2 = Pixel(iX1+iDXBlend2*2, iY1+iY);
                while (iNRemThis > 0)
                {
                    HL_PIXEL wSrc1 = *p1;
                    HL_PIXEL wSrc2 = *p2;

                    wR1 = wSrc1 & 0x0000F800;
                    wG1 = wSrc1 & 0x000007E0;
                    wB1 = wSrc1 & 0x0000001F;
                    wR2 = wSrc2 & 0x0000F800;
                    wG2 = wSrc2 & 0x000007E0;
                    wB2 = wSrc2 & 0x0000001F;
                    wR1 = wR1 >> 7;
                    wR2 = wR2 >> 7;
                    wG1 = wG1 >> 2;
                    wG2 = wG2 >> 2;

                    wR =  (wR1 * byAlpha1 + wR2 * byAlpha2)       & 0xF800F800;
                    wG = ((wG1 * byAlpha1 + wG2 * byAlpha2) >> 5) & 0x07E007E0;
                    wB = ((wB1 * byAlpha1 + wB2 * byAlpha2) >> 7) & 0x001F001F;

                    *p2= (HL_PIXEL)(wR|wG|wB);

                    p1++;
                    p2++;
                    iNRemThis--;
                }
            }
        }

        return;
    }


    for (int iY = 0; iY < iDYBlend ; iY++)
    {
        DWORD wAlpha = (DWORD)(iAlpha0 + ((iY+iY1)-iYDstOrig) * iDAlpha / iDYOrig);
        DWORD byAlphaRB = wAlpha * 32 / 255;
        DWORD byAlphaG  = wAlpha * 64 / 255;
        DWORD _byAlphaRB  = 32-byAlphaRB;
        DWORD _byAlphaG   = 64-byAlphaG;

        HL_PIXEL *pPIX1 = pSrcDib->Pixel(iXSrc, iYSrc+iY);
        HL_PIXEL *pPIX2 = Pixel(iX1, iY1+iY); 

        for (int iX = 0; iX < iDXBlend; iX++)
        {
            HL_PIXEL wSrc = *pPIX1;
            HL_PIXEL wTgt = *pPIX2;
            if (wSrc != wTgt)
            {
                wRB_S = wSrc & 0x0000F81F;
                wG_S  = wSrc & 0x000007E0;
                wRB_T = wTgt & 0x0000F81F;
                wG_T  = wTgt & 0x000007E0;

                DWORD dwRB_Out = (((wRB_S * byAlphaRB + wRB_T * _byAlphaRB) ) >> 5) & 0xF81F;
                DWORD dwG_Out  = (((wG_S  * byAlphaG  + wG_T * _byAlphaG)   ) >> 6) & 0x07E0;
                *pPIX2 = (WORD)(dwRB_Out | dwG_Out);
            }
            pPIX2++;
            pPIX1++;
        }
    }

    END_TIMER(GDI_BLENDBLT);
}

/*============================================================================*/

    void                        CHLDibDC::BlendBltAdditive(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    CEdgeList                   *pEdgeList )

/*============================================================================*/
{
    START_TIMER(GDI_BLENDBLT);

    HL_PIXEL wIn1 = 0x0000;
    HL_PIXEL wIn2 = 0x0000;
    HL_PIXEL wOut = 0x0000;
    int shR1 = 0;
    int shG1 = 0;
    int shB1 = 0;
    int shR2 = 0;
    int shG2 = 0;
    int shB2 = 0;
    BOOL bDiff = FALSE;

    int iStep1 = 1;
    int iStep2 = 1;
    CEdgeList::EDGE_TYPE Type = pEdgeList->Type();
    if (Type == CEdgeList::EDGE_VERTICAL)
    {
        iStep1 = pSrcDib->DXPitch();
        iStep2 = DXPitch();
    }

    for (int iEdge = 0; iEdge < pEdgeList->NPoints(); iEdge++)
    {
        int iXEdge = pEdgeList->X(iEdge);
        int iYEdge = pEdgeList->Y(iEdge);
        int iN     = pEdgeList->D(iEdge);
        int iXEdgeSrc   = iXEdge + iXSrc;
        int iYEdgeSrc   = iYEdge + iYSrc;

        // Basic checking
        BOOL bOK = TRUE;
        if (iXEdge < 0)                 bOK = FALSE;
        if (iYEdge < 0)                 bOK = FALSE;
        if (iXEdgeSrc < 0)              bOK = FALSE;
        if (iYEdgeSrc < 0)              bOK = FALSE;
        if (iYEdge >= DY())             bOK = FALSE;
        if (iYEdgeSrc >= pSrcDib->DY()) bOK = FALSE;

        if (Type == CEdgeList::EDGE_HORIZONTAL)
        {
            iN = __min(iN, DX()-iXEdge);
            iN = __min(iN, pSrcDib->DX()-iXEdgeSrc);
        }
        else
        {
            iN = __min(iN, DY()-iYEdge);
            iN = __min(iN, pSrcDib->DY()-iYEdgeSrc);
        }

        if (iN <= 0)       
            bOK = FALSE;

        if (bOK)
        {
            HL_PIXEL *pPIX1 = pSrcDib->Pixel(iXEdgeSrc, iYEdgeSrc);
            HL_PIXEL *pPIX2 = (HL_PIXEL*)Bits() + ((iYEdge * DX()) + iXEdge);

            for (int i = 0; i < iN; i++)
            {
                if (*pPIX1 != wIn1)
                {
                    wIn1 = *pPIX1;
                    #ifdef HL_PIXEL32
                        shR1 = GetRValueHLPIXEL(wIn1);
                        shG1 = GetGValueHLPIXEL(wIn1);
                        shB1 = GetBValueHLPIXEL(wIn1);
                    #else
                        shR1 = (wIn1 >> 11);
                        shG1 = ((wIn1 >> 5)  & 0x003F);
                        shB1 = (wIn1 & 0x001F);
                    #endif
                    bDiff = TRUE;
                }    

                if (*pPIX2 != wIn2)
                {
                    wIn2 = *pPIX2;
                    #ifdef HL_PIXEL32
                        shR2 = GetRValueHLPIXEL(wIn2);
                        shG2 = GetGValueHLPIXEL(wIn2);
                        shB2 = GetBValueHLPIXEL(wIn2);
                    #else
                        shR2 = (wIn2 >> 11);
                        shG2 = ((wIn2 >> 5)  & 0x003F);
                        shB2 = (wIn2 & 0x001F);
                    #endif
                    bDiff = TRUE;
                }

                if (bDiff)
                {
                    #ifdef HL_PIXEL32
                        UINT wR = __max(0, __min(255, shR1 + shR2 - 127));    
                        UINT wG = __max(0, __min(255, shG1 + shG2 - 127));    
                        UINT wB = __max(0, __min(255, shB1 + shB2 - 127));    
                        wOut = RGBtoHLPIXEL(wR, wG, wB);
                    #else
                        UINT wR = __max(0, __min(31, shR1 + shR2 - 15));    
                        UINT wG = __max(0, __min(63, shG1 + shG2 - 31));    
                        UINT wB = __max(0, __min(31, shB1 + shB2 - 15));    
                        wOut = ((wR<<11)|(wG<<5)|wB);
                    #endif
                } 

                *pPIX2 = wOut;

                pPIX1 += iStep1;
                pPIX2 += iStep2;
            }
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::BlendBltAdditive(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    START_TIMER(GDI_BLENDBLT);

    iDX = __min(iDX, pSrcDib->DX()-iXSrc);
    iDY = __min(iDY, pSrcDib->DY()-iYSrc);
    iDX = __min(iDX, DX()-iXDst);
    iDY = __min(iDY, DY()-iYDst);

    int iX1 = iXDst;
    int iY1 = iYDst;
    int iX2 = iXDst + iDX;
    int iY2 = iYDst + iDY;
    iY1 = __min(iY1, DY());
    iY2 = __min(iY2, DY());
    HL_PIXEL wIn1 = 0x0000;
    HL_PIXEL wIn2 = 0x0000;
    HL_PIXEL wOut = 0x0000;
    int shR1 = 0;
    int shG1 = 0;
    int shB1 = 0;
    int shR2 = 0;
    int shG2 = 0;
    int shB2 = 0;
    BOOL bDiff = FALSE;

    for (int iYBlend = iY1; iYBlend < iY2; iYBlend++)
    {
        HL_PIXEL *pPIX1 = pSrcDib->Pixel(iXSrc, iYSrc+iYBlend-iY1);
        HL_PIXEL *pPIX2 = Pixel(iX1, iYBlend);
        for (int iXBlend = iX1; iXBlend < iX2; iXBlend++)
        {
            if (*pPIX1 != wIn1)
            {
                wIn1 = *pPIX1;
                #ifdef HL_PIXEL32
                    shR1 = GetRValueHLPIXEL(wIn1);
                    shG1 = GetGValueHLPIXEL(wIn1);
                    shB1 = GetBValueHLPIXEL(wIn1);
                #else
                    shR1 = (wIn1 >> 11);
                    shG1 = ((wIn1 >> 5)  & 0x003F);
                    shB1 = (wIn1 & 0x001F);
                #endif
                bDiff = TRUE;
            }    

            if (*pPIX2 != wIn2)
            {
                wIn2 = *pPIX2;
                #ifdef HL_PIXEL32
                    shR2 = GetRValueHLPIXEL(wIn2);
                    shG2 = GetGValueHLPIXEL(wIn2);
                    shB2 = GetBValueHLPIXEL(wIn2);
                #else
                    shR2 = (wIn2 >> 11);
                    shG2 = ((wIn2 >> 5)  & 0x003F);
                    shB2 = (wIn2 & 0x001F);
                #endif
                bDiff = TRUE;
            }

            if (bDiff)
            {
                #ifdef HL_PIXEL32
                    UINT wR = __max(0, __min(255, shR1 + shR2 - 127));    
                    UINT wG = __max(0, __min(255, shG1 + shG2 - 127));    
                    UINT wB = __max(0, __min(255, shB1 + shB2 - 127));    
                    wOut = RGBtoHLPIXEL(wR,wG,wB);
                #else
                    UINT wR = __max(0, __min(31, shR1 + shR2 - 15));    
                    UINT wG = __max(0, __min(63, shG1 + shG2 - 31));    
                    UINT wB = __max(0, __min(31, shB1 + shB2 - 15));    
                    wOut = ((wR<<11)|(wG<<5)|wB);
                #endif
            } 

            *pPIX2 = wOut;
            pPIX1++;
            pPIX2++;
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::BlendBltAdditiveAbs(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    COLORREF                    rgbBase )

/*============================================================================*/
{
    START_TIMER(GDI_BLENDBLT);

    if (iXSrc < 0)
    {
        iXDst -= iXSrc;
        iDX += iXSrc;
        iXSrc = 0;
    }

    if (iYSrc < 0)
    {
        iYDst -= iYSrc;
        iDY += iYSrc;
        iYSrc = 0;
    }

    iDX = __min(iDX, pSrcDib->ImageDX()-iXSrc);
    iDY = __min(iDY, pSrcDib->DY()-iYSrc);
    iDX = __min(iDX, ImageDX()-iXDst);
    iDY = __min(iDY, DY()-iYDst);

    int iX1 = iXDst;
    int iY1 = iYDst;
    int iX2 = iXDst + iDX;
    int iY2 = iYDst + iDY;
    iY1 = __min(iY1, DY());
    iY2 = __min(iY2, DY());
    DWORD wIn1 = 0x0000;
    DWORD wIn2 = 0x0000;
    int shR1 = 0;
    int shG1 = 0;
    int shB1 = 0;
    int shR2 = 0;
    int shG2 = 0;
    int shB2 = 0;

    if (iDX <= 0 || iDY <= 0)
        return;

    HL_PIXEL wBase = RGBTOHLPIXEL(rgbBase);
    int iRBase = wBase & 0xF800;
    int iGBase = wBase & 0x07E0;
    int iBBase = wBase & 0x001F;

    for (int iYBlend = iY1; iYBlend < iY2; iYBlend++)
    {
        HL_PIXEL *pPIX1 = pSrcDib->Pixel(iXSrc, iYSrc+iYBlend-iY1);
        HL_PIXEL *pPIX2 = Pixel(iX1, iYBlend);
        for (int iXBlend = iX1; iXBlend < iX2; iXBlend++)
        {
            wIn1 = *pPIX1;
            wIn2 = *pPIX2;
            shR1 = wIn1 & 0xF800;
            shG1 = wIn1 & 0x07E0;
            shB1 = wIn1 & 0x001F;
            shR2 = wIn2 & 0xF800;
            shG2 = wIn2 & 0x07E0;
            shB2 = wIn2 & 0x001F;
            int wR = __max(0, __min(0xF800, shR1 + shR2 - iRBase));    
            int wG = __max(0, __min(0x07E0, shG1 + shG2 - iGBase));    
            int wB = __max(0, __min(0x001F, shB1 + shB2 - iBBase));    

            *pPIX2 = (wR|wG|wB);
            pPIX1++;
            pPIX2++;
        }
    }

    END_TIMER(GDI_BLENDBLT);
}
/*============================================================================*/

    void                        CHLDibDC::TrapBlt(
    CHLDibDC                    *pSrc,
    CHLRect                     rROISrc,
    int                         iXArc,
    int                         iYArc )

/*============================================================================*/
{
    int iDY = 2 * iYArc;
    int iXInset0 = iXArc;
    int iXInset1 = -iXArc;

    int iDXT = rROISrc.DX() + iXInset0;
    int iDXB = rROISrc.DX() + iXInset1;

    int iYOut = DY()/2-iYArc;

    int iSourcePitch = rROISrc.DX();
    for (int iY = 0; iY < iDY; iY++)
    {
        int iDXOut = iDXT + iY * (iDXB-iDXT) / iDY - abs(2*iXArc);
        int iXOffset = 0;
        int iXOut = ImageDX()/2-iDXOut/2;  
        if (iXOut < 0)
        {
            iXOffset = -iXOut;
            iXOut = 0;
        }
        int iDXWrite = __min(iDXOut, ImageDX()-iXOut);

        HL_PIXEL *pOut = Pixel(iXOut, iYOut+iY);
        HL_PIXEL *pIn = pSrc->Pixel(rROISrc.XLeft(), rROISrc.YTop()+rROISrc.DY()* iY / iDY);

        int iIntPartX   = iSourcePitch / iDXOut;
        int iFractPartX = iSourcePitch % iDXOut;
        int iErrorX     = 0;

        int iDX = iDXWrite;
        while (iDX-- > 0)
        {
            *pOut++ = *pIn;
            pIn += iIntPartX;
            iErrorX += iFractPartX;
            if (iErrorX >= iDXOut)
            {
                iErrorX -= iDXOut;
                pIn++;
            }            
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::MakeGrayscale()

/*============================================================================*/
{
    for (int iY = 0; iY < DY(); iY++)
    {
        HL_PIXEL *pP = Pixel(0, iY);
        for (int iX = 0; iX < m_iDX; iX++)
        {
            HL_PIXEL wIn = *pP;
            DWORD dwSum = (wIn & 0x1F) + ((wIn >> 6) & 0x1F) + (wIn >> 11);
            DWORD dwR = dwSum / 3;
            DWORD dwB = (dwR << 11);
            DWORD dwG = (dwR << 6);
            *pP = (HL_PIXEL)(dwR|dwB|dwG);
            pP++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::ScaleToMonochrome(
    COLORREF                    rgb)

/*============================================================================*/
{
    DWORD wRMax = GetRValue(rgb) * 31 / 255;
    DWORD wGMax = GetGValue(rgb) * 63 / 255;
    DWORD wBMax = GetBValue(rgb) * 31 / 255;

    for (int iY = 0; iY < DY(); iY++)
    {
        HL_PIXEL *pP = Pixel(0, iY);
        for (int iX = 0; iX < m_iDX; iX++)
        {
            HL_PIXEL wIn = *pP;

            // 0-93
            DWORD dwSum = (wIn & 0x1F) + ((wIn >> 6) & 0x1F) + (wIn >> 11);

            DWORD dwR = wRMax * dwSum / 93;
            DWORD dwG = wGMax * dwSum / 93;
            DWORD dwB = wBMax * dwSum / 93;

            *pP = (HL_PIXEL)((dwR<<11)|(dwG<<5)|dwB);
            pP++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::SubtractLevel(
    int                         iLevel )

/*============================================================================*/
{
    int iAddR     = ((iLevel) * 31 / 255) << 11;
    int iAddG     = ((iLevel) * 63 / 255) << 5;
    int iAddB     = ((iLevel) * 31 / 255);
    int wR, wG, wB;

    HL_PIXEL wLastIn = 0x0000;
    HL_PIXEL wLastOut = 0x0000;
    for (int iY = 0; iY < DY(); iY++)
    {
        HL_PIXEL *pIn = Pixel(0, iY);
        for (int i = 0; i < m_iImageDX; i++)
        {
            if (*pIn != wLastIn)
            {
                wLastIn = *pIn;
                wR = wLastIn & 0x0000F800;
                wG = wLastIn & 0x000007E0;
                wB = wLastIn & 0x0000001F;
                wR = __max(0, (wR - iAddR));
                wG = __max(0, (wG - iAddG));
                wB = __max(0, (wB - iAddB));
                wLastOut = (HL_PIXEL)(wR|wG|wB);
            }

            *pIn = wLastOut;
            pIn++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::Add(
    CHLDibDC                    *pSrcDib,
    int                         iSrcPCT )

/*============================================================================*/
{
    Add(0, 0, pSrcDib, 0, 0, ImageDX(), DY(), TRUE, iSrcPCT);
}
/*============================================================================*/

    void                        CHLDibDC::Add(
    int                         iXDest,
    int                         iYDest,
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    BOOL                        bClamp,
    int                         iSrcPCT )

/*============================================================================*/
{
    // Make Sure we have good coords for the src DIB
    iXDest = m_iXLeft + iXDest;
    iYDest = m_iYTop + iYDest;

    if (!GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, pDibDC))
        return;

    if (m_iAttribs & DIBATTRIB_TRACKUPDATES)
    {
        AddUpdateRect(CDXYRect(iXDest, iYDest, iDX, iDY));
    }

    // Check Clip
    CHLRect rClip = LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();

    if (iXDest + iDX > m_iDX)
    {
        LOGASSERT(FALSE);
    }

    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    if (iDY < 1 || iDX < 1)
        return;

    int iDXTgt = DX();
    int iDXSrc = pDibDC->DXPitch();

    HL_PIXEL *pSrcRow = pDibDC->Pixel(iXSrc, iYSrc);
    HL_PIXEL *pTgtRow = Pixel(iXDest, iYDest);

    BOOL bScale = (iSrcPCT < 100);
    int iScale = 32 * iSrcPCT / 100;

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pTgt = pTgtRow;
        HL_PIXEL *pSrc = pSrcRow;

        if (bScale)
        {
            for (int iX = 0; iX < iDX; iX++)
            {
                DWORD wTgt = *pTgt;
                DWORD wSrc = *pSrc;
        
                int wRDst = wTgt & 0xF800;
                int wRSrc = ((wSrc & 0xF800) * iScale) >> 5;
                int wROut = wRSrc + wRDst;  if (wROut > 0x0000F800)    wROut = 0x0000F800; else wROut &= 0x0000F800;

                int wGDst = wTgt & 0x07E0;
                int wGSrc = ((wSrc & 0x07E0) * iScale) >> 5;
                int wGOut = wGSrc + wGDst;  if (wGOut > 0x000007E0)    wGOut = 0x000007E0; else wGOut &= 0x000007E0;

                int wBDst = wTgt & 0x001F;
                int wBSrc = ((wSrc & 0x001F) * iScale) >> 5;
                int wBOut = wBSrc + wBDst;  if (wBOut > 0x0000001F)    wBOut = 0x0000001F;

                *pTgt = (HL_PIXEL)(wROut|wGOut|wBOut);

                pTgt++;
                pSrc++;
            }
        }
        else if (bClamp)
        {
            for (int iX = 0; iX < iDX; iX++)
            {
                DWORD wTgt = *pTgt;
                DWORD wSrc = *pSrc;
        
                int wRDst = wTgt & 0xF800;
                int wRSrc = wSrc & 0xF800;
                int wROut = wRSrc + wRDst;  if (wROut > 0x0000F800)    wROut = 0x0000F800; else wROut &= 0x0000F800;

                int wGDst = wTgt & 0x07E0;
                int wGSrc = wSrc & 0x07E0;
                int wGOut = wGSrc + wGDst;  if (wGOut > 0x000007E0)    wGOut = 0x000007E0; else wGOut &= 0x000007E0;

                int wBDst = wTgt & 0x001F;
                int wBSrc = wSrc & 0x001F;
                int wBOut = wBSrc + wBDst;  if (wBOut > 0x0000001F)    wBOut = 0x0000001F;

                *pTgt = (HL_PIXEL)(wROut|wGOut|wBOut);

                pTgt++;
                pSrc++;
            }
        }
        else
        {
            for (int iX = 0; iX < iDX; iX++)
            {
                int wTgt = *pTgt;
                int wSrc = *pSrc;
        
                int wRBDst = wTgt & 0xF81F;
                int wRBSrc = wSrc & 0xF81F;
                int wGDst  = wTgt & 0x07E0;
                int wGSrc  = wSrc & 0x07E0;
                int wRBOut = (wRBSrc + wRBDst) & 0xF81F;
                int wGOut  = (wGSrc  + wGDst) & 0x07E0;
                *pTgt = (HL_PIXEL)(wRBOut|wGOut);

                pTgt++;
                pSrc++;
            }
        }

        pTgtRow += iDXTgt;
        pSrcRow += iDXSrc;
    }

    END_TIMER(GDI_BITBLT_MEM);
}
/*============================================================================*/

    void                        CHLDibDC::AddRGB(
    int                         iRAdd,
    int                         iGAdd,
    int                         iBAdd,
    CEdgeList                   *pEdges )

/*============================================================================*/
{
    if (pEdges == NULL)
    {
        CDXYRect rThis(0, 0, m_iImageDX, m_iDY);
        AddRGB(iRAdd, iGAdd, iBAdd, rThis);
        return;
    }
    
    HL_PIXEL wLastIn  = 0xFFFF;
    HL_PIXEL wLastOut = 0;
    int wR = 0;
    int wG = 0;
    int wB = 0;

    if (iRAdd < 0 || iGAdd < 0 || iBAdd < 0)
    {
        BOOL bRNeg = iRAdd < 0;
        BOOL bGNeg = iGAdd < 0;
        BOOL bBNeg = iBAdd < 0;
        int iAddR     = (abs(iRAdd) * 31 / 255) << 11;
        int iAddG     = (abs(iGAdd) * 63 / 255) << 5;
        int iAddB     =  abs(iBAdd) * 31 / 255;

        for (int iEdge = 0; iEdge < pEdges->NPoints(); iEdge++)
        {
            HL_PIXEL *pIn = Pixel(pEdges->X(iEdge), pEdges->Y(iEdge));
            int iDX = pEdges->D(iEdge);
            for (int i = 0; i < iDX; i++)
            {
                if (*pIn != wLastIn)
                {
                    wLastIn = *pIn;
                    wR = wLastIn & 0x0000F800;
                    wG = wLastIn & 0x000007E0;
                    wB = wLastIn & 0x0000001F;
                    if (bRNeg)            
                        wR = __max(0, (wR - iAddR));
                    else
                        wR = __min(wR + iAddR, 0x0000F800) & 0x0000F800;

                    if (bGNeg)
                        wG = __max(0, (wG - iAddG));
                    else
                        wG = __min(wG + iAddG, 0x000007E0) & 0x000007E0;

                    if (bBNeg)
                        wB = __max(0, (wB - iAddB));
                    else
                        wB = __min(wB + iAddB, 0x0000001F);
                    wLastOut = (HL_PIXEL)(wR|wG|wB);
                }

                *pIn = wLastOut;
                pIn++;
            }
        }


        return;
    }

    int iAddR     = (iRAdd * 31 / 255) << 11;
    int iAddG     = (iGAdd * 63 / 255) << 5;
    int iAddB     = iBAdd * 31 / 255;

    for (int iEdge = 0; iEdge < pEdges->NPoints(); iEdge++)
    {
        HL_PIXEL *pIn = Pixel(pEdges->X(iEdge), pEdges->Y(iEdge));
        int iDX = pEdges->D(iEdge);
        for (int i = 0; i < iDX; i++)
        {
            if (*pIn != wLastIn)
            {
                wLastIn = *pIn;
                wR = wLastIn & 0x0000F800;
                wG = wLastIn & 0x000007E0;
                wB = wLastIn & 0x0000001F;
                wR = __min(wR + iAddR, 0x0000F800) & 0x0000F800;
                wG = __min(wG + iAddG, 0x000007E0) & 0x000007E0;
                wB = __min(wB + iAddB, 0x0000001F);
                wLastOut = (HL_PIXEL)(wR|wG|wB);
            }

            *pIn = wLastOut;
            pIn++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::AddRGB(
    int                         iRAdd,
    int                         iGAdd,
    int                         iBAdd,
    CHLRect                     rROI )

/*============================================================================*/
{
    HL_PIXEL wLastIn  = 0xFFFF;
    HL_PIXEL wLastOut = 0;
    int wR = 0;
    int wG = 0;
    int wB = 0;

    CDXYRect rThis = CDXYRect(0, 0, m_iImageDX, m_iDY);
    rROI = rThis.IntersectWith(rROI);

    int iDX = rROI.DX();
    int iDY = rROI.DY();

    if (iRAdd < 0 || iGAdd < 0 || iBAdd < 0)
    {
        BOOL bRNeg = iRAdd < 0;
        BOOL bGNeg = iGAdd < 0;
        BOOL bBNeg = iBAdd < 0;
        int iAddR     = (abs(iRAdd) * 31 / 255) << 11;
        int iAddG     = (abs(iGAdd) * 63 / 255) << 5;
        int iAddB     =  abs(iBAdd) * 31 / 255;

        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pIn = Pixel(rROI.XLeft(), rROI.YTop()+iY);
            for (int i = 0; i < iDX; i++)
            {
                if (*pIn != wLastIn)
                {
                    wLastIn = *pIn;
                    wR = wLastIn & 0x0000F800;
                    wG = wLastIn & 0x000007E0;
                    wB = wLastIn & 0x0000001F;
                    if (bRNeg)            
                        wR = __max(0, (wR - iAddR));
                    else
                        wR = __min(wR + iAddR, 0x0000F800) & 0x0000F800;

                    if (bGNeg)
                        wG = __max(0, (wG - iAddG));
                    else
                        wG = __min(wG + iAddG, 0x000007E0) & 0x000007E0;

                    if (bBNeg)
                        wB = __max(0, (wB - iAddB));
                    else
                        wB = __min(wB + iAddB, 0x0000001F);
                    wLastOut = (HL_PIXEL)(wR|wG|wB);
                }

                *pIn = wLastOut;
                pIn++;
            }
        }


        return;
    }

    int iAddR     = (iRAdd * 31 / 255) << 11;
    int iAddG     = (iGAdd * 63 / 255) << 5;
    int iAddB     = iBAdd * 31 / 255;

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pIn = Pixel(rROI.XLeft(), rROI.YTop()+iY);
        for (int i = 0; i < iDX; i++)
        {
            if (*pIn != wLastIn)
            {
                wLastIn = *pIn;
                wR = wLastIn & 0x0000F800;
                wG = wLastIn & 0x000007E0;
                wB = wLastIn & 0x0000001F;
                wR = __min(wR + iAddR, 0x0000F800) & 0x0000F800;
                wG = __min(wG + iAddG, 0x000007E0) & 0x000007E0;
                wB = __min(wB + iAddB, 0x0000001F);
                wLastOut = (HL_PIXEL)(wR|wG|wB);
            }

            *pIn = wLastOut;
            pIn++;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::AddRGB(
    BYTE                        byRAdd,
    BYTE                        byGAdd,
    BYTE                        byBAdd,
    CHLRect                     rROI,
    int                         iRad )

/*============================================================================*/
{
    CHLRect rROIIn = rROI;
    rROIIn.Translate(m_iXLeft, m_iYTop);

    int iXSrc = rROI.XLeft() + m_iXLeft;
    int iYSrc = rROI.YTop() + m_iYTop;
    int iDX = rROI.DX();
    int iDY = rROI.DY();
    int iXDst = iXSrc;
    int iYDst = iYSrc;
    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, NULL))
        return;

    CHLRect rClip = LocalClipRect();
    if (iXSrc < rClip.XLeft())
    {
        iDX -= (rClip.XLeft()-iXSrc);
        iXSrc = rClip.XLeft();
    }
    if (iYSrc < rClip.YTop())
    {
        iDY -= (rClip.YTop()-iYSrc);
        iYSrc = rClip.YTop();
    }
    iDX = __min(rClip.XRight()-iXSrc+1, iDX);
    iDY = __min(rClip.YBottom()-iYSrc+1, iDY);

    if (iDX < 1 || iDY < 1)
        return;

    DWORD wLastIn  = 0xFFFFFFFF;

    #ifdef IPHONE
        DWORD wAddR     = byRAdd << 8;
        DWORD wAddG     = byGAdd << 16;
        DWORD wAddB     = byBAdd << 24;
        DWORD wRMax     = 0x0000FF00 - wAddR;
        DWORD wGMax     = 0x00FF0000 - wAddG;
        DWORD wBMax     = 0xFF000000 - wAddB;
    #else
        DWORD wAddR     = (byRAdd * 31 / 255) << 11;
        DWORD wAddG     = (byGAdd * 63 / 255) << 5;
        DWORD wAddB     = byBAdd * 31 / 255;
        DWORD wR = 0;
        DWORD wG = 0;
        DWORD wB = 0;
    #endif

    if (iRad > 0)
    {
        CPointList Rad;
        CalcArcPoints(&Rad, iRad, TRUE);

        for (int iY = 0; iY < iDY; iY++)
        {
            int iXInset = 0;
            int iYOrig = iY + (iYSrc-rROIIn.YTop());
            if (iYOrig < iRad)
                iXInset = iRad - Rad.X(iYOrig);
            else if (iYOrig > rROIIn.DY()-iRad)
                iXInset = iRad - Rad.X(rROIIn.DY()-iYOrig);

            int iX0This = iXSrc;
            int iX1This = iXSrc+iDX-1;

            iX0This = __max(iX0This, rROIIn.XLeft()+iXInset);
            iX1This = __min(iX1This, rROIIn.XRight()-iXInset);
            int iDXThis = iX1This - iX0This + 1;

            HL_PIXEL *pIn = Pixel(iX0This, iY+iYSrc);
            for (int iX = 0; iX < iDXThis; iX++)
            {
                wLastIn = *pIn;

#ifdef IPHONE
                DWORD dwRIn = wLastIn & 0x0000FF00;
                DWORD dwGIn = wLastIn & 0x00FF0000;
                DWORD dwBIn = wLastIn & 0xFF000000;
                if (dwRIn < wRMax) dwRIn += wAddR; else dwRIn = 0x0000FF00;
                if (dwGIn < wGMax) dwGIn += wAddG; else dwGIn = 0x00FF0000;
                if (dwBIn < wBMax) dwBIn += wAddB; else dwBIn = 0xFF000000;
                *pIn = dwRIn|dwGIn|dwBIn|0xFF;
#else
                wR = (wLastIn & 0x0000F800) + wAddR;
                if (wR > 0xF800) wR = 0xF800; else wR &= 0xF800;
                wG = (wLastIn & 0x000007E0) + wAddG;
                if (wG > 0x07E0) wG = 0x07E0; else wG &= 0x07E0;
                wB = (wLastIn & 0x0000001F) + wAddB;
                if (wB > 0x001F) wB = 0x001F;
                *pIn = (HL_PIXEL)(wR|wG|wB);
#endif

                pIn++;
            }
        }
    }
    else
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            wLastIn = 0xFFFFFFFF;
            HL_PIXEL *pIn = Pixel(iXSrc, iY+iYSrc);

            for (int iX = 0; iX < iDX; iX++)
            {
                wLastIn = *pIn;

#ifdef IPHONE
                DWORD dwRIn = wLastIn & 0x0000FF00;
                DWORD dwGIn = wLastIn & 0x00FF0000;
                DWORD dwBIn = wLastIn & 0xFF000000;
                if (dwRIn < wRMax) {dwRIn += wAddR; dwRIn &= 0x0000FF00;} else dwRIn = 0x0000FF00;
                if (dwGIn < wGMax) {dwGIn += wAddG; dwGIn &= 0x00FF0000;} else dwGIn = 0x00FF0000;
                if (dwBIn < wBMax) {dwBIn += wAddB; dwBIn &= 0xFF000000;} else dwBIn = 0xFF000000;
                *pIn = dwRIn|dwGIn|dwBIn|0xFF;
//                *pIn = 0xFFFFFFFF;
#else
                wR = (wLastIn & 0x0000F800) + wAddR;
                if (wR > 0xF800) wR = 0xF800; else wR &= 0xF800;
                wG = (wLastIn & 0x000007E0) + wAddG;
                if (wG > 0x07E0) wG = 0x07E0; else wG &= 0x07E0;
                wB = (wLastIn & 0x0000001F) + wAddB;
                if (wB > 0x001F) wB = 0x001F;
                *pIn = (HL_PIXEL)(wR|wG|wB);
#endif
                pIn++;
            }
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::SetRGB(
    COLORREF                    rgb,
    int                         iX,
    int                         iY,
    CEdgeList                   *pEdges )

/*============================================================================*/
{
    HL_PIXEL wSet = RGBTOHLPIXEL(rgb);
    DWORD dwSet = (wSet << 16 ) | wSet;

    int iXOut = iX + m_iXLeft;
    int iYOut = iY + m_iYTop;

    for (int i = 0; i < pEdges->NPoints(); i++)
    {
        int iX0 = pEdges->X(i) + iXOut;
        int iY0 = pEdges->Y(i) + iYOut;
        int iD = pEdges->D(i);

        if (iY0 >= 0 && iY0 < DY())
        {
            if (iX0 < 0)
            {
                iD += iX0;
                iX0 = 0;
            }
            iD = __min(iD, m_iImageDX - iX0);
            if (iD > 0)
            {
                HL_PIXEL *pIn = Pixel(iX0, iY0);
                #if defined(XSCALE) & !defined(THUMB) & !defined(HL_WIN32)
                    ippsSet_16s(wSet, (short*)pIn, iD);
                #else
                    if ((iX0 % 2) != 0)
                    {
                        *pIn = wSet;
                        pIn++;
                        iD--;
                    }

                    int iNDWORDS = iD / 2;
                    DWORD *pdwIn = (DWORD*)pIn;
                    for (int iDWORD = 0; iDWORD < iNDWORDS; iDWORD++)
                    {
                        *pdwIn = dwSet;
                        pdwIn++;
                    }

                    if ((iD % 2) != 0)
                    {
                        pIn += (iNDWORDS*2);
                        *pIn = wSet;
                    }
                #endif
            }
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::BevelEdges()

/*============================================================================*/
{
    for (int iY = 0; iY < DY(); iY++)
    {
        HL_PIXEL *pL = Pixel(0, iY);
        HL_PIXEL *pR = Pixel(ImageDX()-1, iY);
        SCALEUP(pL);
        SCALEDN(pR);
    }
    HL_PIXEL *pT = Pixel(0, 0);
    HL_PIXEL *pB = Pixel(0, DY()-1);
    for (int iX = 0; iX < ImageDX(); iX++)
    {
        SCALEUP(pT);
        SCALEDN(pB);
        pT++;
        pB++;
    }
}
/*============================================================================*/

    void                        CHLDibDC::ChangeColor(
    COLORREF                    rgbFrom,
    COLORREF                    rgbTo )

/*============================================================================*/
{
    HL_PIXEL wFrom = RGBTOHLPIXEL(rgbFrom);
    HL_PIXEL wTo = RGBTOHLPIXEL(rgbTo);

    HL_PIXEL *pPix = m_pBits;
    int iNPix = m_iDX * m_iDY;
    for (int i = 0; i < iNPix; i++)
    {
        if (*pPix == wFrom)
            *pPix = wTo;
        pPix++;
    }
}
/*============================================================================*/

    void                        CHLDibDC::SetPointer(
    void                        *pOld,
    void                        *pNew )

/*============================================================================*/
{
    if (pOld == m_pBits)
    {
        m_pBits = (HL_PIXEL*)pNew;
        return;
    }
    if (pOld == m_pMask)
    {
        m_pMask = (BYTE*)pNew;
        return;
    }
    LOGASSERT(FALSE);
}
/*============================================================================*/

    BYTE                        *CHLDibDC::GetMask()

/*============================================================================*/
{
    return m_pMask;
}
/*============================================================================*/

    BYTE                        *CHLDibDC::GetMask(
    int                         iX,
    int                         iY )

/*============================================================================*/
{
    LOGASSERT(iX >= 0 && iX < m_iDX);
    LOGASSERT(iY >= 0 && iY < m_iDY);

    if (m_pMask == NULL)
        return NULL;
    return (m_pMask + iY * m_iDX + iX);
}
/*============================================================================*/

    void                        CHLDibDC::InitMask(
    BYTE                        byVal )

/*============================================================================*/
{
    int iNPix = DX() * DY();
    if (m_pMask == NULL)
    {
        m_pMask = (BYTE*)HLAlloc(m_iDX * m_iDY);
    }

    memset(m_pMask, byVal, iNPix);
}
/*============================================================================*/

    void                        CHLDibDC::SetMask(
    HL_PIXEL                    rgbSet,
    BYTE                        byVal  )

/*============================================================================*/
{
    int iNPix = DX() * DY();
    if (m_pMask == NULL)
    {
        m_pMask = (BYTE*)HLAlloc(m_iDX * m_iDY);
        memset(m_pMask, 0, iNPix);
    }

    HL_PIXEL *pPix = Bits();
    BYTE *pMask = m_pMask;
    for (int i = 0; i < iNPix; i++)
    {
        if (*pPix == rgbSet)
            *pMask = byVal;
        pPix++;
        pMask++;
    }
}
/*============================================================================*/

    void                        CHLDibDC::FillMaskHZ(
    BYTE                        bySet,
    int                         iX1,
    int                         iX2,
    int                         iY )

/*============================================================================*/
{
    if (iY < 0 || iY >= DY())
        return;
    if (m_pMask == NULL)
        return;
    iX1 = __max(0, iX1);
    iX2 = __min(DX()-1, iX2);
    if (iX1 > iX2)
        return;

    BYTE *pSet = GetMask(iX1, iY);
    memset(pSet, bySet, iX2-iX1+1);
}
/*============================================================================*/

    void                        CHLDibDC::SetMaskNOT(
    HL_PIXEL                    rgbSet,
    BYTE                        byVal  )

/*============================================================================*/
{
    int iNPix = DX() * DY();
    if (m_pMask == NULL)
    {
        m_pMask = (BYTE*)HLAlloc(m_iDX * m_iDY);
        memset(m_pMask, 0, iNPix);
    }

    HL_PIXEL *pPix = Bits();
    BYTE *pMask = m_pMask;
    for (int i = 0; i < iNPix; i++)
    {
        if (*pPix != rgbSet)
            *pMask = byVal;
        pPix++;
        pMask++;
    }
}
/*============================================================================*/

    void                        CHLDibDC::EncodeRLEMask()

/*============================================================================*/
{
    InitMask(0);
    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pPIX = Pixel(m_iDX-1, iY);
        HL_PIXEL wLast = 0;
        BYTE *pMask = GetMask(m_iDX-1, iY);
        WORD wCounter = 0;

        for (int iX = 0; iX < m_iDX; iX++)
        {
            if (*pPIX != wLast || iX == 0 || wCounter >= 255)
            {
                wCounter = 0;
                wLast = *pPIX;
            }            
            else
            {
                wCounter++;
            }

            *pMask = (BYTE)wCounter;
            pMask--;
            pPIX--;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::GenEdgeListByColor(
    CHLRect                     rROI,
    HL_PIXEL                    wTransparent,
    CEdgeList                   *pEdges )

/*============================================================================*/
{
    for (int iY = rROI.YTop(); iY <= rROI.YBottom(); iY++)
    {
        HL_PIXEL *pIn = Pixel(rROI.XLeft(), iY);
        int iXStart = rROI.XLeft();
        BOOL bXLast = (*pIn == wTransparent);
        int iX0 = rROI.XLeft();
        int iXR = rROI.XRight();
        for (int iX = iX0; iX <= iXR; iX++)
        {
            BOOL bXNow = (*pIn == wTransparent);
            if (bXNow != bXLast)
            {
                if (bXNow && !bXLast)
                    pEdges->AddEdge(iXStart, iY, iX-iXStart);
                else
                    iXStart = iX;
                bXLast = bXNow;
            }
            pIn++;
        }

        if (!bXLast)
        {
            pEdges->AddEdge(iXStart, iY, iXR-iXStart);
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::DilateMask(
    BYTE                        bySet )

/*============================================================================*/
{
    if (m_pMask == NULL)
        return;
    BYTE *pOldMask = (BYTE*)HLAlloc(m_iDX * m_iDY);
    memcpy(pOldMask, m_pMask, DX()*DY());
    int iDD = DX();

    for (int iY = 1; iY < DY()-1; iY++)
    {
        BYTE *pIn   = pOldMask + iY * DX() + 1;
        BYTE *pOut  = m_pMask + iY * DX() + 1;
        for (int iX = 1; iX < DX()-1; iX++)
        {
            if (*(pIn) != 0)
            {
                if (*(pIn-1) == 0)
                    *(pOut-1) = bySet;
                if (*(pIn+1) == 0)
                    *(pOut+1) = bySet;
                if (*(pIn+iDD) == 0)
                    *(pOut+iDD) = bySet;
                if (*(pIn-iDD) == 0)
                    *(pOut-iDD) = bySet;

                if (*(pIn-iDD-1) == 0)
                    *(pOut-iDD-1) = bySet;
                if (*(pIn-iDD+1) == 0)
                    *(pOut-iDD+1) = bySet;
                if (*(pIn+iDD-1) == 0)
                    *(pOut+iDD-1) = bySet;
                if (*(pIn+iDD+1) == 0)
                    *(pOut+iDD+1) = bySet;
            }

            pIn++;
            pOut++;
        }
    }

    HLFree(pOldMask);
}
/*============================================================================*/

    void                        CHLDibDC::RenderHighlight(
    int                         iX,
    int                         iY,
    BYTE                        *pAlphaData,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    iX += m_iXLeft;
    iY += m_iYTop;

    int iDXPitch    = iDX;
    int iXSrc       = 0;
    int iYSrc       = 0;
    if (!GetSafeDims(&iX, &iY, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

    for (int iYScan = 0; iYScan < iDY; iYScan++)
    {
        HL_PIXEL *pDst = Pixel(iX, iY+iYScan);
        BYTE *pSrc = pAlphaData + ((iYSrc + iYScan) * iDXPitch) + iXSrc;

        for (int iXScan = 0; iXScan < iDX; iXScan++)
        {
            BYTE byAlpha = *pSrc;
            HL_PIXEL wPix = *pDst;
            DWORD   byB = wPix & 0x001F;
            DWORD   byG = wPix & 0x07E0;
            DWORD   byR = wPix & 0xF800;

            byG = __min(byG+(byAlpha<<5), 0x07E0);
            byR = __min(byR+(byAlpha<<10), 0xF800) & 0xF800;
            byB = __min(byB+byAlpha, 31);
            wPix = (HL_PIXEL)(byR|byG|byB);
            *pDst = wPix;
            pSrc++;
            pDst++;
        }
    }
    
}
/*============================================================================*/

    void                        CHLDibDC::RenderButtonOverlay(
    int                         iROutside,
    int                         iCorners,
    int                         iEdgeSize,
    int                         iButtonFlags,
    int                         iButtonAttribs )

/*============================================================================*/
{
    if (m_iDX <= 2 || m_iDY <= 2)
        return;


    if (iButtonFlags & BUTTON_OPT_NOSHINE)
    {
        return;
    }

    int iAlphaMax = -1;
    if (iButtonFlags & BUTTON_OPT_NOSHINE_UPPER)
    {
        iAlphaMax = 0;
    }
    
    ApplyShineyGradient(iROutside, iCorners, iEdgeSize, iAlphaMax, iButtonAttribs, -1, -1, -1);    
}
/*============================================================================*/

    void                        CHLDibDC::ApplyShineyGradient(
    int                         iROutside,
    int                         iCorners,
    int                         iEdgeSize,
    int                         iAlphaMax,
    int                         iButtonAttribs,
    int                         iAlphaRatio,
    int                         iRMinor,
    int                         iAlphaLower,
    HIGHLIGHT_TYPE              Type )

/*============================================================================*/
{
    START_TIMER(GDI_SHINE);

    iROutside = __max(0, iROutside);

    if (iAlphaMax < 0)
    {
        iAlphaMax = g_pSetServer->GetInt(INT_ALPHAMAX);
        if (DY() > 50)
            iAlphaMax = iAlphaMax * 50 / DY();
    }

    int iRLower = iRMinor;
    if (iRLower < 0)
    {
        iRLower = __max(g_pSetServer->GetInt(INT_ALPHA_RMINOR), iROutside / 4);
        iRLower = __min(iRLower, DY()/3);
    }

    #ifdef HLOSD
        iRLower = iROutside;
    #endif

    int iDYAvail = m_iDY - 2*iEdgeSize;
    int iDYTop = iDYAvail/2;
    int iDYBtm = iDYAvail/2;

    if (Type == HIGHLIGHT_UPPER)
    {
        iDYTop = m_iDY;
        iDYBtm = 0;
    }

    if (iAlphaRatio < 0)
        iAlphaRatio = g_pSetServer->GetInt(INT_ALPHA_UPPER_RATIO);

    int iAlphaMin       = iAlphaMax * iAlphaRatio / 100;
    int iAlphaBottom    = g_pSetServer->GetInt(INT_ALPHA_BOTTOM_EDGE);

    if (iAlphaLower != -1)
        iAlphaBottom = iAlphaLower;

    DrawAlphaShape(iEdgeSize, iDYTop, iROutside, iRLower, iAlphaMax, iAlphaMin, iEdgeSize, iCorners);

    if (Type == HIGHLIGHT_UPPER_LOWER)
    {
        iRLower = __min(4, iRLower);
        if (iAlphaBottom > 0)
            DrawAlphaShape(m_iDY-iEdgeSize-iDYBtm, iDYBtm, 0, iROutside, 0, iAlphaBottom, iEdgeSize, iCorners);
    }

    END_TIMER(GDI_SHINE);
}
/*============================================================================*/

    void                        CHLDibDC::DrawAlphaShape(
    int                         iY0,
    int                         iDY,
    int                         iR0,
    int                         iR1,
    int                         iA0,
    int                         iA1,
    int                         iXInset,
    int                         iCorners  )   

/*============================================================================*/
{    
    if (iA0 <= 0 && iA1 <= 0)
        return;

    iR0 = __min(iR0, iDY-1);

    if (iR0 + iR1 > iDY)
    {
        // Can't fit both radius' in this iDY
        iR1 = __min(iR1, iDY / 2);
        int iDY0 = __min(iDY - iR1, iR0);
        iR1 = iDY-iDY0;
        int iA0_1 = iA0 + (iA1-iA0) * iDY0 / iDY;
        DrawAlphaRadius(iY0, iDY0, iR0, iA0, iA0_1, iXInset, FALSE, iCorners & Bit(CORNER_UL), iCorners & Bit(CORNER_UR));
        DrawAlphaRadiusCompound(iY0, iDY0, iXInset, iR0, iR1, iA0_1, iA1);
        return; 
    }

    iR1 = __min(iR1, iDY - iR0-1);

    int iA0_1 = iA0 + (iA1-iA0) * iR0 / iDY;
    int iA0_2 = iA0 + (iA1-iA0) * (iDY-iR1) / iDY;

    DrawAlphaRadius(iY0, iR0, iR0, iA0, iA0_1, iXInset, FALSE, iCorners & Bit(CORNER_UL), iCorners & Bit(CORNER_UR));
    DrawAlphaRadius(iY0+iDY-iR1, iR1, iR1, iA0_2, iA1, iXInset, TRUE, iCorners & Bit(CORNER_LL), iCorners & Bit(CORNER_LR));

    int iDYMid = iDY-iR0-iR1;
    DrawAlphaRect(iY0+iR0, iDYMid, iA0_1, iA0_2, iXInset);
}    
/*============================================================================*/

    void                        CHLDibDC::DrawAlphaRadius(
    int                         iY0,
    int                         iDY,
    int                         iRad, 
    int                         iATop,
    int                         iABtm,
    int                         iXInset,
    BOOL                        bInvertGeo,
    BOOL                        bLRad,
    BOOL                        bRRad )

/*============================================================================*/
{
    int iRadius16 = iRad * 16 - 8;
    int iRad2_Plus  = (iRad+2)*(iRad+1);

    CDitherMap map;
    for (int iY = 0; iY < iDY; iY++)
    {
        int iYInRadius = iRad-iY;
        int iYInRadius_2 = iYInRadius * iYInRadius;
        int iXInRadius = __min(iSqrt(iRad2_Plus-iYInRadius_2)+1, iRad);

        int iThisXInset = iXInset + iRad - iXInRadius;

        int iYInDib = iY0 + iY;
        int iYInGeo = iY;
        if (bInvertGeo)
        {
            iYInDib = iY0+iRad-iY-1;
            iYInGeo = iRad-iY-1;
        }

        int iAlphaThisLine = iATop + (iABtm-iATop) * iYInGeo / iRad;
        int iAlphaThis = iAlphaThisLine;

        HL_PIXEL *pL = Pixel(iThisXInset, iYInDib);
        HL_PIXEL *pR = Pixel(m_iImageDX - iThisXInset - 1, iYInDib);

        int iRAdd = (iAlphaThis << 7) & 0xF800;
        int iGAdd = (iAlphaThis << 2) & 0x07E0;
        int iBAdd = (iAlphaThis >> 4);

        int iRMax = 0xF800 - iRAdd;
        int iGMax = 0x07E0 - iGAdd;
        int iBMax = 0x001F - iBAdd;

        if (!bLRad)
        {
            DWORD wTgt = *pL;
            int iR = wTgt & 0xF800;
            int iG = wTgt & 0x07E0;
            int iB = wTgt & 0x001F;

            if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
            if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
            if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

            *pL= (HL_PIXEL)(iR|iG|iB);
            pL++;
        }

        if (!bRRad)
        {
            DWORD wTgt = *pR;
            int iR = wTgt & 0xF800;
            int iG = wTgt & 0x07E0;
            int iB = wTgt & 0x001F;

            if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
            if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
            if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

            *pR= (HL_PIXEL)(iR|iG|iB);
            pR--;
        }

        int iRadToPoint16 = iSqrt16(iYInRadius_2 + iXInRadius * iXInRadius);
        int iDEdge = iRadToPoint16 - iRadius16;
        while (iDEdge > 0 && iXInRadius > 0)
        {
            if (iDEdge < 16)
            {
                int iAlphaThisPixel = 16-iDEdge;
                iAlphaThis = (iAlphaThisLine * iAlphaThisPixel) >> 4;

                if (iAlphaThis > 0)
                {
#ifdef HLOSD
                    HL_PIXEL wTgt = *pL;
                    unsigned int iR = (wTgt & R_MASK) + iAlphaThis;
                    unsigned int iG = (wTgt & G_MASK) + (iAlphaThis << 8);
                    unsigned int iB = (wTgt & B_MASK) + (iAlphaThis << 16);
                    unsigned int iA = wTgt & A_MASK;

                    if (iR >= R_MASK) iR = R_MASK; else { iR &= R_MASK;   }
                    if (iG >= G_MASK) iG = G_MASK; else { iG &= G_MASK;   }
                    if (iB >= B_MASK) iB = B_MASK; else { iB &= B_MASK;   }

                    if (bLRad)
                        *pL = (HL_PIXEL)(iR|iG|iB|iA);

                    wTgt = *pR;
                    iR = (wTgt & R_MASK) + iAlphaThis;
                    iG = (wTgt & G_MASK) + (iAlphaThis << 8);
                    iB = (wTgt & B_MASK) + (iAlphaThis << 16);
                    iA = wTgt & A_MASK;

                    if (iR >= R_MASK) iR = R_MASK; else { iR &= R_MASK;   }
                    if (iG >= G_MASK) iG = G_MASK; else { iG &= G_MASK;   }
                    if (iB >= B_MASK) iB = B_MASK; else { iB &= B_MASK;   }

                    if (bRRad)
                        *pR = (HL_PIXEL)(iR|iG|iB|iA);
#elif IPHONE
                    HL_PIXEL wTgt = *pL;
                    unsigned int iR = wTgt & R_MASK;
                    unsigned int iG = wTgt & G_MASK;
                    unsigned int iB = wTgt & B_MASK;
                    unsigned int iA = wTgt & A_MASK;
                    unsigned int iRAdd = (iAlphaThis * iA)         & R_MASK;
                    unsigned int iGAdd = ((iAlphaThis * iA) << 8 ) & G_MASK;
                    unsigned int iBAdd = ((iAlphaThis * iA) << 16) & B_MASK;
                    unsigned int iRMax = R_MASK - iRAdd;
                    unsigned int iGMax = G_MASK - iGAdd;
                    unsigned int iBMax = B_MASK - iBAdd;

                    if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
                    if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
                    if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   }
                    if (bLRad)
                        *pL = (HL_PIXEL)(iR|iG|iB|iA);

                    wTgt = *pR;
                    iR = wTgt & R_MASK;
                    iG = wTgt & G_MASK;
                    iB = wTgt & B_MASK;
                    iA = wTgt & A_MASK;

                    iRAdd = (iAlphaThis * iA)         & R_MASK;
                    iGAdd = ((iAlphaThis * iA) << 8 ) & G_MASK;
                    iBAdd = ((iAlphaThis * iA) << 16) & B_MASK;
                    iRMax = R_MASK - iRAdd;
                    iGMax = G_MASK - iGAdd;
                    iBMax = B_MASK - iBAdd;

                    if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
                    if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
                    if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   }
                    if (bRRad)
                        *pR = (HL_PIXEL)(iR|iG|iB|iA);
#else
                    unsigned int iRAdd = (iAlphaThis << 8) & 0xF800;
                    unsigned int iGAdd = (iAlphaThis << 3) & 0x07E0;
                    unsigned int iBAdd = (iAlphaThis >> 3);

                    unsigned int iRMax = 0xF800 - iRAdd;
                    unsigned int iGMax = 0x07E0 - iGAdd;
                    unsigned int iBMax = 0x001F - iBAdd;

                    HL_PIXEL wTgt = *pL;
                    unsigned int iR = wTgt & 0xF800;
                    unsigned int iG = wTgt & 0x07E0;
                    unsigned int iB = wTgt & 0x001F;

                    if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
                    if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
                    if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

                    if (bLRad)
                        *pL= (HL_PIXEL)(iR|iG|iB);

                    wTgt = *pR;
                    iR = wTgt & 0xF800;
                    iG = wTgt & 0x07E0;
                    iB = wTgt & 0x001F;

                    if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
                    if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
                    if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

                    if (bRRad)
                        *pR= (HL_PIXEL)(iR|iG|iB);
#endif
                }
            }

            iXInRadius--;
            iRadToPoint16 = iSqrt16(iYInRadius_2 + iXInRadius * iXInRadius);
            iDEdge = iRadToPoint16 - iRadius16;
            if (bLRad)
                pL++;
            if (bRRad)
                pR--;        
        }

        if (iDEdge <= 0)
            iAlphaThis = iAlphaThisLine;

        map.Set(iAlphaThis, iAlphaThis, iAlphaThis);
        map.AddHZ(pL, pR-pL+1);
    }
}
/*============================================================================*/

    void                        CHLDibDC::DrawAlphaRadiusCompound(
    int                         iY0,
    int                         iDY0,
    int                         iXInset,
    int                         iROuter,
    int                         iRInner,
    int                         iATop,
    int                         iABtm )

/*============================================================================*/
{
    int iRadiusInner16 = iRInner * 16 - 8;
//    int iRadiusOuter_2 = iROuter * iROuter;
    int iRad2_Plus  = (iRInner+2)*(iRInner+1);

    CDitherMap map;
    for (int iY = 0; iY < iRInner; iY++)
    {
        // Calculate the offset to the "left" that will will shift our inner radius
        int iXInRadiusOuter16 = 0;
        if (iY+iDY0 <= iROuter) 
        {
            int iYInRadiusOuter = iROuter-(iY+iDY0);
            iXInRadiusOuter16 = iROuter*16-iSqrt16(iROuter * iROuter - iYInRadiusOuter * iYInRadiusOuter);
        }

        int iYInRadiusInner = iY;
        int iYInRadius16 = (iYInRadiusInner << 4);
        int iYInRadius16_2 = iYInRadius16 * iYInRadius16;

        int iThisXInset = iXInset;//iXInset + iRInner - iXInRadiusInner;
        int iYInDib = iY0 + iY + iDY0;
        int iYInGeo = iY;
        int iAlphaThisLine = iATop + (iABtm-iATop) * iYInGeo / iRInner;

        if (iY == iRInner-1)
            iAlphaThisLine = iABtm * 75 / 100;

        int iAlphaThis = iAlphaThisLine;

        int iXInRadiusInner16 = (iRInner << 4);
        iXInRadiusInner16 += iXInRadiusOuter16;

        // Calculate a start offset so we don't have to search so far
        int iXInRadiusStart = __min(iSqrt(iRad2_Plus-iYInRadiusInner*iYInRadiusInner)+1, iRInner);
        int iXOffset = iRInner - iXInRadiusStart;
        
        iThisXInset += iXOffset;
        iXInRadiusInner16 -= 16 * iXOffset;


        int iRadToPoint16 = iSqrt(iYInRadius16_2 + iXInRadiusInner16 * iXInRadiusInner16);
        int iDEdge = iRadToPoint16 - iRadiusInner16;

        HL_PIXEL *pL = Pixel(iThisXInset, iYInDib);
        HL_PIXEL *pR = Pixel(m_iImageDX - iThisXInset - 1, iYInDib);

        // Left and right edges are 50%
        while (iDEdge > 0 && iXInRadiusInner16 > 0)
        {
            if (iDEdge < 16)
            {
                int iAlphaThisPixel = 16-iDEdge;
                iAlphaThis = (iAlphaThisLine * iAlphaThisPixel) >> 4;

                if (iAlphaThis > 0)
                {
#ifdef IPHONE
                    HL_PIXEL wTgt = *pL;
                    unsigned int iR = wTgt & R_MASK;
                    unsigned int iG = wTgt & G_MASK;
                    unsigned int iB = wTgt & B_MASK;
                    unsigned int iA = wTgt & 0xFF;
                    unsigned int iRAdd = (iAlphaThis * iA)         & R_MASK;
                    unsigned int iGAdd = ((iAlphaThis * iA) << 8 ) & G_MASK;
                    unsigned int iBAdd = ((iAlphaThis * iA) << 16) & B_MASK;
                    unsigned int iRMax = R_MASK - iRAdd;
                    unsigned int iGMax = G_MASK - iGAdd;
                    unsigned int iBMax = B_MASK - iBAdd;

                    if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
                    if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
                    if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   }
                    *pL = (HL_PIXEL)(iR|iG|iB|iA);

                    wTgt = *pR;
                    iR = wTgt & R_MASK;
                    iG = wTgt & G_MASK;
                    iB = wTgt & B_MASK;
                    iA = wTgt & 0xFF;
                    iRAdd = (iAlphaThis * iA)         & R_MASK;
                    iGAdd = ((iAlphaThis * iA) << 8 ) & G_MASK;
                    iBAdd = ((iAlphaThis * iA) << 16) & B_MASK;
                    iRMax = R_MASK - iRAdd;
                    iGMax = G_MASK - iGAdd;
                    iBMax = B_MASK - iBAdd;

                    if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
                    if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
                    if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   }
                    *pR = (HL_PIXEL)(iR|iG|iB|iA);
#else
                    int iRAdd = (iAlphaThis << 8) & 0xF800;
                    int iGAdd = (iAlphaThis << 3) & 0x07E0;
                    int iBAdd = (iAlphaThis >> 3);

                    int iRMax = 0xF800 - iRAdd;
                    int iGMax = 0x07E0 - iGAdd;
                    int iBMax = 0x001F - iBAdd;

                    HL_PIXEL wTgt = *pL;
                    int iR = wTgt & 0xF800;
                    int iG = wTgt & 0x07E0;
                    int iB = wTgt & 0x001F;

                    if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
                    if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
                    if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

                    *pL= (HL_PIXEL)(iR|iG|iB);

                    wTgt = *pR;
                    iR = wTgt & 0xF800;
                    iG = wTgt & 0x07E0;
                    iB = wTgt & 0x001F;

                    if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
                    if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
                    if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

                    *pR= (HL_PIXEL)(iR|iG|iB);
#endif
                }
            }

            iXInRadiusInner16 -= 16;
            iRadToPoint16 = iSqrt(iYInRadius16_2 + iXInRadiusInner16 * iXInRadiusInner16);
            iDEdge = iRadToPoint16 - iRadiusInner16;
            pL++;
            pR--;        
        }

        if (iDEdge <= 0)
            iAlphaThis = iAlphaThisLine;

        map.Set(iAlphaThis, iAlphaThis, iAlphaThis);
        map.AddHZ(pL, pR-pL+1);
    }
}
/*============================================================================*/

    void                        CHLDibDC::DrawAlphaRect(
    int                         iY0,
    int                         iDY,
    int                         iATop,
    int                         iABtm,
    int                         iXInset )

/*============================================================================*/
{
    CDitherMap map;
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pL = Pixel(iXInset, iY0+iY);
        HL_PIXEL *pR = Pixel(m_iImageDX - iXInset - 1, iY0+iY);

        int iAlphaThisLine = iATop + (iABtm-iATop) * iY / iDY;
        int iAlphaThis = iAlphaThisLine / 2;
        
#ifdef IPHONE
        CDitherMap edges;
        edges.Set(iAlphaThis, iAlphaThis, iAlphaThis);
        edges.AddHZ(pL, 1);
        edges.AddHZ(pR, 1);
#else
        // Left and right edges are 50%
        int iRAdd = (iAlphaThis << 8) & 0xF800;
        int iGAdd = (iAlphaThis << 3) & 0x07E0;
        int iBAdd = (iAlphaThis >> 3);

        int iRMax = 0xF800 - iRAdd;
        int iGMax = 0x07E0 - iGAdd;
        int iBMax = 0x001F - iBAdd;

        HL_PIXEL wTgt = *pL;
        int iR = wTgt & 0xF800;
        int iG = wTgt & 0x07E0;
        int iB = wTgt & 0x001F;

        if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
        if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
        if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

        *pL= (HL_PIXEL)(iR|iG|iB);

        wTgt = *pR;
        iR = wTgt & 0xF800;
        iG = wTgt & 0x07E0;
        iB = wTgt & 0x001F;

        if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
        if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
        if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 

        *pR= (HL_PIXEL)(iR|iG|iB);
#endif

        pL++;
        pR--;
        
        map.Set(iAlphaThisLine, iAlphaThisLine, iAlphaThisLine);
        map.AddHZ(pL, pR-pL+1);
    }
}
/*============================================================================*/

    void                            CHLDibDC::Solidify(
    COLORREF                        rgb )

/*============================================================================*/
{
    if (m_pMask == NULL)
        return;

    int iNPIX = DX()*DY();
    HL_PIXEL *pPIX = Bits();
    
    BYTE *pA = GetMask();
    HL_PIXEL wBG = RGBTOHLPIXEL(rgb);
    DWORD wRB = wBG & 0x0000F800;
    DWORD wGB = wBG & 0x000007E0;
    DWORD wBB = wBG & 0x0000001F;

    // Premults
    DWORD wRBM  = 0;
    DWORD wGBM  = 0;
    DWORD wBBM  = 0;
    DWORD wRA   = 0;
    DWORD wGA   = 0;
    DWORD wBA   = 0;

    DWORD wALast = 0;
    HL_PIXEL wOutLast = 0;

    DWORD byAlphaLast   = 0;
    DWORD byAlphaRB     = 0;
    DWORD byAlphaG      = 0;

    for (int iPIX = 0; iPIX <iNPIX; iPIX++)
    {
        BYTE byAlpha = *pA;
        switch (byAlpha)
        {
        case 0xFF:
            // Fully Opaque
            break;

        case 0:
            // Full Trans
            *pPIX = wBG;
            break;

        default:
            {    
                BYTE byAlpha = *pA;
                BOOL bDiff = FALSE;
                if (byAlpha != byAlphaLast)
                {
                    bDiff = TRUE;
                    byAlphaLast = byAlpha;
                    byAlphaRB   = (byAlpha >> 3);
                    byAlphaG    = (byAlpha >> 2);
                    DWORD _byAlphaRB  = 31-byAlphaRB;
                    DWORD _byAlphaG   = 63-byAlphaG;
                    wRBM        = _byAlphaRB * wRB;
                    wGBM        = _byAlphaG  * wGB;
                    wBBM        = _byAlphaRB * wBB;
                }

                HL_PIXEL wSrc = *pPIX;

                if (wSrc != wALast)
                {
                    wRA = wSrc & 0x0000F800;
                    wGA = wSrc & 0x000007E0;
                    wBA = wSrc & 0x0000001F;
                    wALast = wSrc;
                    bDiff = TRUE;
                }

                if (bDiff)
                {
                    DWORD wROut = (((wRA * byAlphaRB + wRBM) >> 5) & 0x0000F800);
                    DWORD wGOut = (((wGA * byAlphaG  + wGBM) >> 6) & 0x000007E0);
                    DWORD wBOut =  ((wBA * byAlphaRB + wBBM) >> 5);
                    wOutLast = (WORD)(wROut | wGOut  | wBOut);
                }

                *pPIX = wOutLast;
            }
            break;        
        }

        pPIX++;
        pA++;
    }
}

#ifdef _DEBUG
    /*============================================================================*/

        void                        CHLDibDC::CheckPointer(
        HL_PIXEL                    *pWORD )

    /*============================================================================*/
    {
        LOGASSERT(m_pBits != NULL);
        DWORD dwOffset = pWORD - m_pBits;
        LOGASSERT(dwOffset >= 0);
        LOGASSERT(dwOffset < (DWORD)(m_iDX * m_iDY));
    }
#endif
/*============================================================================*/

    void                        CHLDibDC::CalcArcPoints(
    CPointList                  *pPointList,
    int                         iR,
    BOOL                        bOmitRepeatX )

/*============================================================================*/
{
    pPointList->RemoveAll();

    int iD      = 2*(1-iR);
    int iX      = 0;
    int iY      = iR;
    int iYLast  = -1;

    pPointList->AddPoint(iX, iY);
    iYLast = iY;

    while (iY >= 0)
    {
        if (iD < 0)
        {
            int iSig = 2 * iD + 2 * iY -1;
            if (iSig > 0)
            {
                iX++;
                iY--;
                iD = iD + 2 * iX - 2 * iY + 2;                
            }
            else
            {
                iX++;
                iD = iD + 2*iX + 1;
            }
        }
        else if (iD > 0)
        {
            int iSig = 2 * iD - 2 * iX - 1;
            if (iSig > 0)
            {
                iY--;
                iD = iD - 2 * iY + 1;
            }
            else
            {
                iX++;
                iY--;
                iD = iD + 2*iX - 2*iY + 2;
            }
        }
        else
        {
            iX++;
            iY--;
            iD = iD + 2 * iX - 2 * iY + 2;
        }

        if (iY >= 0)
        {
            if (bOmitRepeatX && iYLast == iY)
                pPointList->RemoveLast();
            pPointList->AddPoint(iX, iY);
            iYLast = iY;
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::CalcArcPoints(
    CPointList                  *pPointList,
    int                         iRadius,
    BOOL                        bOmitDupX,
    int                         iRadiusType )

/*============================================================================*/
{
    int iGeoType = (iRadiusType & GEO_MASK);
    if (iGeoType == RADIUS_ON)
    {
        CHLDibDC::CalcArcPoints(pPointList, iRadius, bOmitDupX);
        return;
    }

    if (iGeoType == RADIUS_INSET)
    {
        CHLDibDC::CalcArcPoints(pPointList, iRadius+1, bOmitDupX);
        CHLDibDC::InsetArcPoints(pPointList);
        return;
    }

    if (iGeoType == RADIUS_OUTSET || iGeoType == RADIUS_OUTSET_REL)
    {

        if (iGeoType == RADIUS_OUTSET_REL)
            CHLDibDC::CalcArcPoints(pPointList, iRadius, FALSE);
        else
            CHLDibDC::CalcArcPoints(pPointList, iRadius-1, FALSE);

        // Pass 1 Simple inset
        int iXDiag = -1;
        int iYDiag = -1;
        BOOL bVt = FALSE;

        int iNArc = pPointList->NPoints();
        for (int i = 0; i < iNArc; i++)
        {
            int iX = pPointList->X(i);
            int iY = pPointList->Y(i);
            if (iY >= iX)
            {
                iY++;
            }
            else if (iY < iX)
            {
                iX++;
                if (!bVt)
                {
                    // Switching for Hz to Vt, remember this coord
                    iXDiag = iX;
                    iYDiag = iY+1;
                    if (i > 0)
                    {
                        if (pPointList->X(i-1) < iX-1)
                            iXDiag = iX-1;
                    }
                    else
                    {
                        iXDiag = iX-1;
                    }

                    bVt = TRUE;
                }
            }


            if (iY > iRadius || iX > iRadius)
            {
                pPointList->RemoveAtIndex(i);
                i--;
                iNArc--;
            }
            else
            {
                pPointList->X(i, iX);
                pPointList->Y(i, iY);
            }
        }

        // Note: the +1 is from point inserted above
        if (iXDiag >= 0 && iYDiag >= 0 && iXDiag <= iRadius && iYDiag <= iRadius)
        {
//            pPointList->AddPoint(iXDiag, iYDiag);
            pPointList->InsertPointY(iXDiag, iYDiag);
        }

        if (bOmitDupX)
        {
            int iNArc = pPointList->NPoints();
            for (int i = 1; i < iNArc; i++)
            {
                if (pPointList->Y(i-1) == pPointList->Y(i))
                {
                    pPointList->RemoveAtIndex(i-1);
                    i--;
                    iNArc--;
                }
            }
        }
    }
}
/*============================================================================*/

    void                        CHLDibDC::InsetArcPoints(
    CPointList                  *pPointList )

/*============================================================================*/
{
    // Pass 1 Simple inset
    int iNPts = pPointList->NPoints();
    for (int i = 0; i < iNPts; i++)
    {
        int iX = pPointList->X(i);
        int iY = pPointList->Y(i);
        if (iY > iX)
            pPointList->Y(i, iY-1);
        else
            pPointList->X(i, iX-1);
    }

    // Pass 2 remove overlaps
    pPointList->RemoveCommonPoints();
}
/*============================================================================*/

    void                        CHLDibDC::ScaleFast(
    CHLRect                     rSrc,
    CHLRect                     rDst,
    CHLDibDC                    *pSrc, 
    CHLDibDC                    *pDst )

/*============================================================================*/
{
    if (rSrc.DX() == rDst.DX() && rSrc.DY() == rDst.DY())
    {
        pDst->BltFromDibDC(rDst.XLeft(), rDst.YTop(), pSrc, rSrc.XLeft(), rSrc.YTop(), rSrc.DX(), rSrc.DY());
        return;
    }

    #ifdef HLDDRAW
        if (pSrc->GetSurface() != NULL && pDst->GetSurface() != NULL)
        {
            pDst->DDrawScale(pSrc, rDst, rSrc);
            return;
        }
    #endif    


    POINT pt = pDst->GetOrigin();
    rDst.Translate(pt.x, pt.y);
    CorrectScaleRects(pSrc, pDst, &rSrc, &rDst);

    if (rSrc.DX() <= 0 || rSrc.DY() <= 0)
        return;
    if (rDst.DX() <= 0 || rDst.DY() <= 0)
        return;

    int iIntPartX   = rSrc.DX() / rDst.DX();
    int iFractPartX = rSrc.DX() % rDst.DX();
    int iErrorX     = 0;

    HL_PIXEL *pSrcRow = pSrc->Pixel(rSrc.XLeft(), rSrc.YTop());
    HL_PIXEL *pTgtRow = pDst->Pixel(rDst.XLeft(), rDst.YTop());

    int iIntPartY   = rSrc.DY() / rDst.DY();
    int iFractPartY = rSrc.DY() % rDst.DY();
    int iErrorY     = 0;

    int iDXPitchSource = pSrc->DXPitch();
    int iDXPitchTarget = pDst->DXPitch();
    
    int iDX = rDst.DX();
    int iDY = rDst.DY();

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pIn  = (HL_PIXEL*)pSrcRow;
        HL_PIXEL *pOut = (HL_PIXEL*)pTgtRow;

        int iDXThisRow = iDX;

        while (iDXThisRow-- > 0)
        {
//            pDst->CheckPointer(pOut);
//            pSrc->CheckPointer(pIn);

            *pOut = *pIn;
            pOut++;

            pIn += iIntPartX;
            iErrorX += iFractPartX;
            if (iErrorX >= iDX)
            {
                iErrorX -= iDX;
                pIn++;
            }            
        }        

        pTgtRow += iDXPitchTarget;
        pSrcRow += (iDXPitchSource * iIntPartY);

        iErrorY += iFractPartY;
        if (iErrorY >= iDY)
        {
            iErrorY -= iDY;
            pSrcRow += iDXPitchSource;
        }

    }

/*
    int iXSrc32 = rSrc.XLeft();
    int iXDst32 = rDst.XLeft();
    if (iXDst32 % 2)
    {
        iXDst32--;
        rDst.DX(rDst.DX()+1);
    }
    if (iXSrc32 % 2)
    {
        iXSrc32--;
        rSrc.DX(rSrc.DX()+1);
    }
    
    int iDXOut = rDst.DX();
    if (iDXOut % 2)
    {
        if (pDst->DX()-iXSrc32 > iDXOut)
            iDXOut++;
        else
            iDXOut--;
    }
    iDXOut /= 2;

    if (iDXOut <= 0)
        return;

    int iDYOut = rDst.DY();
    int iDXIn  = rSrc.DX()/2;
    int iDYIn  = rSrc.DY();
    int iIntPartX   = iDXIn / iDXOut;
    int iFractPartX = iDXIn % iDXOut;
    int iErrorX     = 0;

    if (iDYOut <= 0)
        return;
    
    HL_PIXEL *pSrcRow = pSrc->Pixel(iXSrc32, rSrc.YTop());
    HL_PIXEL *pTgtRow = pDst->Pixel(iXDst32, rDst.YTop());

    int iDXPitchTarget = pDst->DX();
    int iDXPitchSource = pSrc->DX();

    int iIntPartY   = iDYIn / iDYOut;
    int iFractPartY = iDYIn % iDYOut;
    int iErrorY     = 0;

    HL_PIXEL *pSrcRowLast = NULL;
    int iYIn = 0;
    for (int iY = 0; iY < iDYOut; iY++)
    {
        if (pSrcRow == pSrcRowLast)
        {
            memcpy(pTgtRow, pTgtRow-iDXPitchTarget, iDXOut * sizeof(DWORD));            
        }
        else
        {
            iErrorX = 0;
            DWORD *pIn  = (DWORD*)pSrcRow;
            DWORD *pOut = (DWORD*)pTgtRow;
            int iDX = iDXOut;
            int iXIn = 0;
            while (iDX-- > 0)
            {
                LOGASSERT(iYIn < iDYIn);
                LOGASSERT(iXIn < iDXIn);
                *pOut++ = *pIn;
                pIn += iIntPartX;
                iXIn += iIntPartX;
                iErrorX += iFractPartX;
                if (iErrorX >= iDXOut)
                {
                    iErrorX -= iDXOut;
                    pIn++;
                    iXIn++;
                }            
            }        
        }
        pSrcRowLast = pSrcRow;

        pTgtRow += iDXPitchTarget;
        pSrcRow += (iDXPitchSource * iIntPartY);
        iYIn += iIntPartY;

        iErrorY += iFractPartY;
        if (iErrorY >= iDYOut)
        {
            iYIn++;
            iErrorY -= iDYOut;
            pSrcRow += iDXPitchSource;
        }

    }
*/
}
/*============================================================================*/

    void                        CHLDibDC::ScaleFast(
    CHLRect                     rSrc,
    CHLRect                     rDst,
    CHLDibDC                    *pSrc, 
    CHLDibDC                    *pDst,
    COLORREF                    rgbOver,
    int                         iAlpha )

/*============================================================================*/
{
    CorrectScaleRects(pSrc, pDst, &rSrc, &rDst);

    int iDXOut = rDst.DX();
    int iDYOut = rDst.DY();
    int iDXIn  = rSrc.DX();
    int iDYIn  = rSrc.DY();

    int iIntPartX   = iDXIn / iDXOut;
    int iFractPartX = iDXIn % iDXOut;
    int iErrorX     = 0;

    HL_PIXEL *pSrcRow = pSrc->Pixel(rSrc.XLeft(), rSrc.YTop());
    HL_PIXEL *pTgtRow = pDst->Pixel(rDst.XLeft(), rDst.YTop());

    int iDXPitchTarget = pDst->DX();
    int iDXPitchSource = pSrc->DX();

    int iIntPartY   = iDYIn / iDYOut;
    int iFractPartY = iDYIn % iDYOut;
    int iErrorY     = 0;

    WORD wAlpha = MAX(0, iAlpha);
    wAlpha = MIN(255, iAlpha);
    DWORD byAlphaRB = wAlpha * 32 / 255;
    DWORD byAlphaG  = wAlpha * 64 / 255;
    DWORD _byAlphaRB  = 32-byAlphaRB;
    DWORD _byAlphaG   = 64-byAlphaG;
    DWORD wRB, wG;

    HL_PIXEL wFill = RGBTOHLPIXEL(rgbOver);
    DWORD wRB_S = wFill & 0x0000F81F;
    DWORD wG_S  = wFill & 0x000007E0;
    wRB_S *= byAlphaRB;
    wG_S  *= byAlphaG;

    for (int iY = 0; iY < iDYOut; iY++)
    {
        HL_PIXEL *pIn  = (HL_PIXEL*)pSrcRow;
        HL_PIXEL *pOut = (HL_PIXEL*)pTgtRow;

        int iDX = iDXOut;

        if (wFill == 0x000)
        {
            while (iDX-- > 0)
            {
                HL_PIXEL wTgt = *pIn;
                wRB = wTgt & 0x0000F81F;
                wG  = wTgt & 0x000007E0;
                DWORD wRBOut = ((_byAlphaRB * wRB) & 0x001F03E0);
                DWORD wGOut  = ((_byAlphaRB * wG)  & 0x0000FC00);
                *pOut = (WORD)((wRBOut|wGOut)>>5);
                pOut++;

                pIn += iIntPartX;
                iErrorX += iFractPartX;
                if (iErrorX >= iDXOut)
                {
                    iErrorX -= iDXOut;
                    pIn++;
                }            
            }        
        }
        else
        {
            while (iDX-- > 0)
            {
                HL_PIXEL wTgt = *pIn;
                wRB = wTgt & 0x0000F81F;
                wG  = wTgt & 0x000007E0;
                DWORD wRBOut = (((wRB_S + _byAlphaRB * wRB) >> 5) & 0x0000F81F);
                DWORD wGOut = (((wG_S  + _byAlphaG  * wG)  >> 6) & 0x000007E0);
                *pOut = (WORD)(wRBOut | wGOut);
                pOut++;

                pIn += iIntPartX;
                iErrorX += iFractPartX;
                if (iErrorX >= iDXOut)
                {
                    iErrorX -= iDXOut;
                    pIn++;
                }            
            }        
        }

        pTgtRow += iDXPitchTarget;
        pSrcRow += (iDXPitchSource * iIntPartY);

        iErrorY += iFractPartY;
        if (iErrorY >= iDYOut)
        {
            iErrorY -= iDYOut;
            pSrcRow += iDXPitchSource;
        }

    }
}
/*============================================================================*/

    void                        CHLDibDC::CorrectScaleRects(
    CHLDibDC                    *pSrc,
    CHLDibDC                    *pDst,
    CHLRect                     *prSrc,
    CHLRect                     *prDst )

/*============================================================================*/
{
    CorrectScaleRects(pSrc, pDst->ImageDX(), pDst->DY(), prSrc, prDst);
}
/*============================================================================*/

    void                        CHLDibDC::CorrectScaleRects(
    CHLDibDC                    *pSrc,
    int                         iDstDXMax,
    int                         iDstDYMax,
    CHLRect                     *prSrc,
    CHLRect                     *prDst )

/*============================================================================*/
{
    int iSrcDXI = prSrc->DX();
    int iSrcDYI = prSrc->DY();
    int iDstDXI = prDst->DX();
    int iDstDYI = prDst->DY();

    if (prSrc->XLeft() < 0)
    {
        int iDXTrimSrc = - prSrc->XLeft();
        int iDXTrimTgt = iDstDXI * iDXTrimSrc / iSrcDXI;
        prDst->BreakOffLeft(iDXTrimTgt, 0);
        prSrc->BreakOffLeft(iDXTrimSrc, 0);
    }
    if (prSrc->XRight() >= pSrc->ImageDX())
    {
        int iDXTrimSrc = prSrc->XRight()-pSrc->ImageDX()+1;
        int iDXTrimTgt = iDstDXI * iDXTrimSrc / iSrcDXI;
        prDst->BreakOffRight(iDXTrimTgt, 0);
        prSrc->BreakOffRight(iDXTrimSrc, 0);
    }
    if (prSrc->YTop() < 0)
    {
        int iDYTrimSrc = - prSrc->YTop();
        int iDYTrimTgt = iDstDYI * iDYTrimSrc / iSrcDYI;
        prDst->BreakOffTop(iDYTrimTgt, 0);
        prSrc->BreakOffTop(iDYTrimSrc, 0);
    }
    if (prSrc->YBottom() >= pSrc->DY())
    {
        int iDYTrimSrc = prSrc->YBottom()-pSrc->DY()+1;
        int iDYTrimTgt = iDstDYI * iDYTrimSrc / iSrcDYI;
        prDst->BreakOffBottom(iDYTrimTgt, 0);
        prSrc->BreakOffBottom(iDYTrimSrc, 0);
    }


    if (prDst->XLeft() < 0)
    {
        int iDXTrimTgt = - prDst->XLeft();
        int iDXTrimSrc = iSrcDXI * iDXTrimTgt / iDstDXI;
        prDst->BreakOffLeft(iDXTrimTgt, 0);
        prSrc->BreakOffLeft(iDXTrimSrc, 0);
        LOGASSERT(prDst->XLeft() >= 0);
        LOGASSERT(prSrc->XLeft() >= 0);
    }
    if (prDst->XRight() >= iDstDXMax)
    {
        int iDXTrimTgt = prDst->XRight()-iDstDXMax+1;
        int iDXTrimSrc = iSrcDXI * iDXTrimTgt / iDstDXI ;
        prDst->BreakOffRight(iDXTrimTgt, 0);
        prSrc->BreakOffRight(iDXTrimSrc, 0);
    }
    if (prDst->YTop() < 0)
    {
        int iDYTrimTgt = - prDst->YTop();
        int iDYTrimSrc = iSrcDYI * iDYTrimTgt / iDstDYI ;
        prDst->BreakOffTop(iDYTrimTgt, 0);
        prSrc->BreakOffTop(iDYTrimSrc, 0);
    }
    if (prDst->YBottom() >= iDstDYMax)
    {
        int iDYTrimTgt = prDst->YBottom()-iDstDYMax+1;
        int iDYTrimSrc = iSrcDYI * iDYTrimTgt / iDstDYI;
        prDst->BreakOffBottom(iDYTrimTgt, 0);
        prSrc->BreakOffBottom(iDYTrimSrc, 0);
    }
}
/*============================================================================*/

    void                        CHLDibDC::Fill(
    HL_PIXEL                    rgbSet )

/*============================================================================*/
{
    int i = 0;
    int iNPIX = m_iDX * m_iDY;
    HL_PIXEL *pSet = Pixel(0, 0);

    #ifndef HL_PIXEL32
        if ((rgbSet >> 8) == (rgbSet & 0x00FF))
        {
            BYTE bySet = (BYTE)(rgbSet & 0x00FF);
            memset(pSet, bySet, iNPIX * sizeof(HL_PIXEL));
            return;
        }


        int iN2 = iNPIX / 2;
        DWORD dwSet = rgbSet | (rgbSet<<16);
        DWORD *pTgt = (DWORD*)pSet;
        for (i = 0; i < iN2; i++)
        {
            *pTgt = dwSet;
            pTgt++;
        }
        return;
    #endif

    #if defined(XSCALE) & !defined(THUMB) & !defined(HL_WIN32)
        ippsSet_16s(rgbSet, (short*)pSet, iNPIX);
    #else
        for (i = 0; i < iNPIX; i++)
        {
            *pSet = rgbSet;
            pSet++;
        }
    #endif
}
/*============================================================================*/

    void                        CHLDibDC::FillHZ(
    HL_PIXEL                    rgbSet,
    int                         iX1,
    int                         iX2,
    int                         iY )

/*============================================================================*/
{
    if (iY < 0 || iY >= DY())
        return;
    iX1 = __max(0, iX1);
    iX2 = __min(DX()-1, iX2);
    if (iX1 > iX2)
        return;
    HL_PIXEL *pSet = Pixel(iX1, iY);

    #ifndef HL_PIXEL32
        int iDX = iX2-iX1+1;
        if (iDX > 12 && m_pParentDIB == NULL)
        {
            if ((iX1 % 2) != 0)
            {
                *Pixel(iX1, iY) = rgbSet;
                iX1++;
                iDX--;
            }

            int iDX_2 = iDX / 2;
            DWORD dwSet = rgbSet | (rgbSet<<16);
            DWORD *pTgt = (DWORD*)Pixel(iX1, iY);
            for (int i = 0; i < iDX_2; i++)
            {
                *pTgt = dwSet;
                pTgt++;
            }
            if ((iDX % 2) != 0)
            {
                *Pixel(iX1+iDX-1, iY) = rgbSet;
            }
            return;
        }
    #endif

    #if defined(XSCALE) & !defined(THUMB) &  !defined(HL_WIN32)
        ippsSet_16s(rgbSet, (short*)pSet, iDX);
    #else
        for (int i = iX1; i <= iX2; i++)
        {
            *pSet = rgbSet;
            pSet++;
        }
    #endif
}
/*============================================================================*/

    void                        CHLDibDC::FillVT(
    HL_PIXEL                    rgbSet,
    int                         iX,
    int                         iY1,
    int                         iY2 )

/*============================================================================*/
{
    if (iX < 0 || iX >= DX())
        return;
    iY1 = __max(0, iY1);
    iY2 = __min(DY()-1, iY2);
    if (iY1 > iY2)
        return;
    int iDXPitch = m_iDX;
    if (m_pParentDIB != NULL)
        iDXPitch = m_pParentDIB->DX();
    HL_PIXEL *pSet = Pixel(iX, iY1);
    for (int i = iY1; i <= iY2; i++)
    {
        *pSet = rgbSet;
        pSet += iDXPitch;
    }
}
/*============================================================================*/

    BOOL                        CHLDibDC::GetSafeDims(
    int                         *piXThis,
    int                         *piYThis,
    int                         *piDX,
    int                         *piDY,
    int                         *piXOther,
    int                         *piYOther,
    CHLDibDC                    *pOther  )

/*============================================================================*/
{
    int iXThis = *piXThis;
    int iYThis = *piYThis;
    int iDX = *piDX;
    int iDY = *piDY;
    int iXOther = *piXOther;
    int iYOther = *piYOther;

    if (iXThis < 0)
    {
        iDX += iXThis;
        iXOther -= iXThis;
        iXThis = 0;
    }
    if (iYThis < 0)
    {
        iDY += iYThis;
        iYOther -= iYThis;
        iYThis = 0;
    }

    iDX = __min(iDX, m_iImageDX - iXThis);
    iDY = __min(iDY, m_iDY - iYThis);

    *piXThis    = iXThis;  
    *piYThis    = iYThis;
    *piDX       = iDX;
    *piDY       = iDY;
    *piXOther   = iXOther;
    *piYOther   = iYOther;
    
    if (iDX <= 0 || iDY <= 0)
        return FALSE;

    if (pOther != NULL)
    {
        return pOther->GetSafeDims(piXOther, piYOther, piDX, piDY, piXThis, piYThis, NULL);
    }

    return TRUE;
}    
/*============================================================================*/

    BOOL                        CHLDibDC::TestClip(
    CHLRect                     rIn )

/*============================================================================*/
{
    if (m_pClipGlobal != NULL)
    {
        rIn.Translate(m_iXLeft+m_iXOrgAbs, m_iYTop+m_iYOrgAbs);
        return m_pClipGlobal->IntersectsWith(&rIn);
    }

    return TRUE;
}
/*============================================================================*/

    CHLRect                     CHLDibDC::LocalClipRect()

/*=============================================================O===============*/
{
    CHLRect rClip = CDXYRect(0, 0, ImageDX(), DY());
    CHLRect *pClip = CreateActiveClip();

    if (pClip == NULL)
    {
        if (m_pClipGlobal != NULL)
        {
            // m_rGlobalClip are in screen coords
            CHLRect rGlobal = *m_pClipGlobal;

            // Make rGlobal into our coords
            POINT ptAbs = GetOriginAbs();
            ptAbs.x -= m_iXLeft;
            ptAbs.y -= m_iYTop;
            rGlobal.Translate(-ptAbs.x, -ptAbs.y);

            rClip.XLeft(    __max(rClip.XLeft(),    rGlobal.XLeft()));
            rClip.XRight(   __min(rClip.XRight(),   rGlobal.XRight()));
            rClip.YTop(     __max(rClip.YTop(),     rGlobal.YTop()));
            rClip.YBottom(  __min(rClip.YBottom(),  rGlobal.YBottom()));
        }

        return rClip;
    }
    pClip->Translate(m_iXLeft, m_iYTop);

    CHLRect rRet = rClip.IntersectWith(*pClip);
    delete pClip;
    return rRet;
}
/*============================================================================*/

    BOOL                        CHLDibDC::IsIntersected(
    CHLDibDC                    *pDIB,
    CHLRect                     rTest )

/*============================================================================*/
{
    return IsIntersected(pDIB, rTest.XLeft(), rTest.YTop(), rTest.DX(), rTest.DY());
}
/*============================================================================*/

    BOOL                        CHLDibDC::IsIntersected(
    CHLDibDC                    *pDIB,
    int                         iXL,
    int                         iYT,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    CDXYRect rParent(0, 0, pDIB->DX(), pDIB->DY());
    CHLRect rInt = rParent.IntersectWith(CDXYRect(iXL, iYT, iDX, iDY));
    if (rInt.DX() <= 0 || rInt.DY() <= 0)
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    void                        CHLDibDC::ResetUpdateRects()

/*============================================================================*/
{
    if (m_pUpdateRectList == NULL)
        return;
    m_pUpdateRectList->Clear();
}
/*============================================================================*/

    void                        CHLDibDC::SetActiveClip(
    CHLRect                     *pClip )

/*============================================================================*/
{
    if (m_hDC != NULL)
    {
        CHLDC::SetActiveClip(pClip);
    }

}
/*============================================================================*/

    POINT                       CHLDibDC::XIntersect(
    int                         iXI,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 )

/*============================================================================*/
{
    int iDYDX1000 = (iY2-iY1)*1000 / (iX2-iX1);
    int iB = iY1 - iDYDX1000 * iX1 / 1000;
    int iYI = iDYDX1000 * iXI / 1000 + iB;
    POINT pt;
    pt.x = iXI;
    pt.y = iYI;
    return pt;
}
/*============================================================================*/

    POINT                       CHLDibDC::YIntersect(
    int                         iYI,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 )

/*============================================================================*/
{
    int iDXDY1000 = (iX2-iX1)*1000 / (iY2-iY1);
    int iB = iX1 - iDXDY1000 * iY1 / 10000;
    int iXI = iDXDY1000 * iYI / 1000 + iB;
    POINT pt;
    pt.x = iXI;
    pt.y = iYI;
    return pt;
}
/*============================================================================*/

    void                        CHLDibDC::SetMaskAlpha(
    int                         iOffset,
    int                         iAlpha1500 )

/*============================================================================*/
{
    if (iOffset < 0)
        return;
    if (iOffset >= m_iDX * m_iDY)
        return;

    iAlpha1500 = abs(iAlpha1500);

    int iAlpha = iAlpha1500 * 255 / 1500;
    iAlpha = __min(255, iAlpha);
    iAlpha = __max(0, iAlpha);
    iAlpha = 255 - iAlpha;

    BYTE *pA = m_pMask + iOffset;
    *(pA) = __max(*pA, (BYTE)iAlpha);
}
/*============================================================================*/

    void                    *CHLDibDC::HLAlloc(int iNBytes)

/*============================================================================*/
{
    #ifdef GDISERV_MEMMAN
        void *pRet = g_pGDIServer->AllocScratchDIB(iNBytes, this);
        LOGASSERT(pRet != NULL);
        return pRet;
    #else
        void *pRet = malloc(iNBytes);
        return pRet;
    #endif
}
/*============================================================================*/

    void                    CHLDibDC::HLFree(
    void                    *pMem )

/*============================================================================*/
{
    if (pMem == NULL)
        return;

    #ifdef GDISERV_MEMMAN
        g_pGDIServer->DeallocScratchDIB((void*)pMem);
    #else
        free(pMem);
    #endif
}
/*============================================================================*/

    void                        CHLDibDC::AddUpdateRect(
    CHLRect                     rect )

/*============================================================================*/
{
    if (m_pUpdateRectList == NULL)
        m_pUpdateRectList = new CRectList;
    m_pUpdateRectList->AddRectRef(&rect);
 }
/*============================================================================*/

    CAlphaDIB::CAlphaDIB(
    int                         iDX,
    int                         iDY,
    int                         iXScreen0,
    int                         iYScreen0,
    int                         iAttribs )

    : CHLDibDC(iDX, iDY, iXScreen0, iYScreen0, MEMORY_SYSTEM, iAttribs)

/*============================================================================*/
{
    m_iAlphaFlags = 0;
    InitMask(0);
#ifdef HLDDRAW
    m_pAlphaSurface = NULL;
#endif
}
/*============================================================================*/

    CAlphaDIB::~CAlphaDIB()

/*============================================================================*/
{
#ifdef HLDDRAW
    if (m_pAlphaSurface != NULL)
    {
        ULONG ulRes = m_pAlphaSurface->Release();
        LOGASSERT(ulRes == 0);
        #ifdef VRAM_TRACKER
            g_pDibServer->UnRegisterVRAMDIB(this);
        #endif
    }
#endif
    
    delete m_pClipGlobal;
    m_pClipGlobal = NULL;
}
/*============================================================================*/

    void                        CAlphaDIB::DDConvertToVRAM()

/*============================================================================*/
{
#ifdef HLDDRAW
    InitAlphaSurface();
#endif
}
/*============================================================================*/

    void                        CAlphaDIB::DDConvertToSysRAM()

/*============================================================================*/
{
#ifdef HLDDRAW
	ReleaseAlphaSurface();
#endif
}
#ifdef HLDDRAW
/*============================================================================*/

    BOOL                        CAlphaDIB::HasDDAlphaSurface()  

/*============================================================================*/
{ 
    return (m_pAlphaSurface != NULL);
}
/*============================================================================*/

    HL_DIRECTDRAWSURFACE        CAlphaDIB::GetDDAlphaSurface()

/*============================================================================*/
{
    if (m_pAlphaSurface == NULL)
        InitAlphaSurface();
    return m_pAlphaSurface;
}
#endif
/*============================================================================*/

    void                        CAlphaDIB::BltTransparentToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    if (pDC == NULL)
        return;

    FlushText();
    
    CHLDibDC *pTgt = pDC->DibDC();
    if (pTgt != NULL)
    {
        pTgt->BltFromAlphaDIB(this, iXDest, iYDest, iXSrc, iYSrc, iDX, iDY);
    }
    else
    {
        CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
        pDC->BltToDibDC(&dib, iXDest, iYDest, 0, 0, iDX, iDY);
        dib.BltFromAlphaDIB(this, 0, 0, iXSrc, iYSrc, iDX, iDY);
        pDC->BltFromDibDC(iXDest, iYDest, &dib, 0, 0, iDX, iDY);
    }

}
/*============================================================================*/

    void                        CAlphaDIB::BltInvertedToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY  )

/*============================================================================*/
{
    CHLDibDC *pDst = pDC->DibDC();
    if (pDst == NULL)
        return;
    CAlphaDIB *pAlphaDst = pDst->AlphaDIB();
    if (pAlphaDst == NULL)
        return;

    CAlphaDIB::HL_PREMULT_MODE Mode = MakePreMult();

    int iXSrc = 0;
    int iYSrc = 0;
    iXDest = pDC->GetOrigin().x + iXDest;
    iYDest = pDC->GetOrigin().y + iYDest;

    START_TIMER(GDI_DIBXBLT);

    if (!pDst->GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, this))
    {
        END_TIMER(GDI_DIBXBLT);
        return;
    }

    // Check Clip
    CHLRect rClip = pDst->LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();
    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    for (int iY = 0; iY < iDY; iY++)
    {
        BYTE *pAlphaIn = GetMask(iXSrc, iYSrc+iY);
        BYTE *pAlphaOut = pAlphaDst->GetMask(iXDest, iYDest+iY);

        switch (Mode)
        {
        case CAlphaDIB::PREMULT_VALUES:
            {
                for (int iX = 0; iX < iDX; iX++)
                {
                    DWORD dwInvAlpha = *pAlphaIn;
                    DWORD dwAlphaOut = *pAlphaOut;
                    dwAlphaOut = dwAlphaOut * dwInvAlpha / 16;
                    *pAlphaOut = (BYTE)dwAlphaOut;
                    pAlphaIn++;
                    pAlphaOut++;
                }
            }
            break;

        case CAlphaDIB::PREMULT_NONE:
            {
                for (int iX = 0; iX < iDX; iX++)
                {
                    DWORD dwInvAlpha = *pAlphaIn;
                    DWORD dwAlphaOut = *pAlphaOut;
                    dwAlphaOut = dwAlphaOut * (255-dwInvAlpha) / 255;
                    *pAlphaOut = (BYTE)dwAlphaOut;
                    pAlphaIn++;
                    pAlphaOut++;
                }
            }
            break;
        }
    }
}
/*============================================================================*/

    void                        CAlphaDIB::BltAlphaToHLDC(
    CHLDibDC                    *pDC ,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY  )

/*============================================================================*/
{
    CHLDibDC *pDst = pDC->DibDC();
    if (pDst == NULL)
        return;
    CAlphaDIB *pAlphaDst = pDst->AlphaDIB();
    if (pAlphaDst == NULL)
        return;

    CAlphaDIB::HL_PREMULT_MODE Mode = MakePreMult();

    int iXSrc = 0;
    int iYSrc = 0;
    iXDest = pDC->GetOrigin().x + iXDest;
    iYDest = pDC->GetOrigin().y + iYDest;

    START_TIMER(GDI_DIBXBLT);

    if (!pDst->GetSafeDims(&iXDest, &iYDest, &iDX, &iDY, &iXSrc, &iYSrc, this))
    {
        END_TIMER(GDI_DIBXBLT);
        return;
    }

    // Check Clip
    CHLRect rClip = pDst->LocalClipRect();
        
    // Clip the dest rect with the clip rect
    CDXYRect rDestOrig(iXDest, iYDest, iDX, iDY);
    CHLRect rDestInt = rClip.IntersectWith(rDestOrig);
    iDX = rDestInt.DX();
    iDY = rDestInt.DY();
    iXSrc += rDestInt.XLeft() - rDestOrig.XLeft();
    iYSrc += rDestInt.YTop() - rDestOrig.YTop();
    iXDest = rDestInt.XLeft();
    iYDest = rDestInt.YTop();

    for (int iY = 0; iY < iDY; iY++)
    {
        BYTE *pAlphaIn = GetMask(iXSrc, iYSrc+iY);
        BYTE *pAlphaOut = pAlphaDst->GetMask(iXDest, iYDest+iY);

        switch (Mode)
        {
        case CAlphaDIB::PREMULT_VALUES:
        case CAlphaDIB::PREMULT_NONE:
            {
                for (int iX = 0; iX < iDX; iX++)
                {
                    BYTE dwAlphaIn  = *pAlphaIn;
                    BYTE dwAlphaOut = *pAlphaOut;
                    *pAlphaOut = __max(dwAlphaIn, dwAlphaOut);
                    pAlphaIn++;
                    pAlphaOut++;
                }
            }
            break;
        }
    }
}
/*============================================================================*/

    void                        CAlphaDIB::DrawAlphaRadius(
    int                         iY0,
    int                         iDY,
    int                         iRad, 
    int                         iATop,
    int                         iABtm,
    int                         iXInset,
    BOOL                        bInvertGeo )

/*============================================================================*/
{
    int iRadius16 = iRad * 16 - 8;
    int iRad2_Plus  = (iRad+2)*(iRad+1);

    for (int iY = 0; iY < iDY; iY++)
    {
        int iYInRadius = iRad-iY;
        int iYInRadius_2 = iYInRadius * iYInRadius;
        int iXInRadius = __min(iSqrt(iRad2_Plus-iYInRadius_2)+1, iRad);

        int iThisXInset = iXInset + iRad - iXInRadius;

        int iYInDib = iY0 + iY;
        int iYInGeo = iY;
        if (bInvertGeo)
        {
            iYInDib = iY0+iRad-iY-1;
            iYInGeo = iRad-iY-1;
        }

        int iAlphaThisLine = iATop + (iABtm-iATop) * iYInGeo / iRad;
        int iAlphaThis = iAlphaThisLine;

        HL_PIXEL *pL = Pixel(iThisXInset, iYInDib);
        HL_PIXEL *pR = Pixel(m_iImageDX - iThisXInset - 1, iYInDib);

        int iRadToPoint16 = iSqrt16(iYInRadius_2 + iXInRadius * iXInRadius);
        int iDEdge = iRadToPoint16 - iRadius16;
        while (iDEdge > 0 && iXInRadius > 0)
        {
            if (iDEdge < 16)
            {
                int iAlphaThisPixel = 16-iDEdge;
                iAlphaThis = (iAlphaThisLine * iAlphaThisPixel) >> 4;

                if (iAlphaThis > 0)
                {
#ifdef HLOSD
                    if (m_iAttribs & DIBATTRIB_BACKGROUNDEMBEDDED)
                    {
                        int iA = *pL & 0xFF000000;
                        int iR = ((*pL & 0x00FF0000) >> 16)  + iAlphaThis;
                        int iG = ((*pL & 0x0000FF00) >> 8)   + iAlphaThis;
                        int iB =  (*pL & 0x000000FF)          + iAlphaThis;
                        *pL = iA|(iR<<16)|(iG<<8)|iB;

                        iA = *pR & 0xFF000000;
                        iR = ((*pR & 0x00FF0000) >> 16)  + iAlphaThis;
                        iG = ((*pR & 0x0000FF00) >> 8)   + iAlphaThis;
                        iB =  (*pR & 0x000000FF)          + iAlphaThis;
                        *pR = iA|(iR<<16)|(iG<<8)|iB;
                    }
                    else
                    {                    
                        DWORD dwRes = iAlphaThis << 24 | 0x00FFFFFF;
                        *pL= dwRes;
                        *pR= dwRes;
                    }
#else
                    int iRAdd = (iAlphaThis << 8) & 0xF800;
                    int iGAdd = (iAlphaThis << 3) & 0x07E0;
                    int iBAdd = (iAlphaThis >> 3);
                    *pL= (HL_PIXEL)(iRAdd|iGAdd|iBAdd);
                    *pR= (HL_PIXEL)(iRAdd|iGAdd|iBAdd);
#endif
                }
            }

            iXInRadius--;
            iRadToPoint16 = iSqrt16(iYInRadius_2 + iXInRadius * iXInRadius);
            iDEdge = iRadToPoint16 - iRadius16;
            pL++;
            pR--;        
        }

        if (iDEdge <= 0)
            iAlphaThis = iAlphaThisLine;


        int iRAdd = (iAlphaThis << 8) & 0xF800;
        int iGAdd = (iAlphaThis << 3) & 0x07E0;
        int iBAdd = (iAlphaThis >> 3);

#ifdef HLOSD
        if (m_iAttribs & DIBATTRIB_BACKGROUNDEMBEDDED)
        {
            while (pL <= pR)
            {
                int iA = *pL & 0xFF000000;
                int iR = ((*pL & 0x00FF0000) >> 16)  + iAlphaThis;
                int iG = ((*pL & 0x0000FF00) >> 8)   + iAlphaThis;
                int iB =  (*pL & 0x000000FF)          + iAlphaThis;
                *pL = iA|(iR<<16)|(iG<<8)|iB;
                pL++;
            }
        }
        else
        {
            HL_PIXEL wRes = iAlphaThis << 24 | 0x00FFFFFF;
            while (pL <= pR)
            {
                *pL= (HL_PIXEL)wRes;
                pL++;
            }
        }
#else
        HL_PIXEL wRes = iRAdd|iGAdd|iBAdd;
        while (pL <= pR)
        {
            *pL= (HL_PIXEL)wRes;
            pL++;
        }
#endif

    }
}
/*============================================================================*/

    void                        CAlphaDIB::DrawAlphaRadiusCompound(
    int                         iY0,
    int                         iDY0,
    int                         iXInset,
    int                         iROuter,
    int                         iRInner,
    int                         iATop,
    int                         iABtm )

/*============================================================================*/
{
    int iRadiusInner16 = iRInner * 16 - 8;
    int iRad2_Plus  = (iRInner+2)*(iRInner+1);

    for (int iY = 0; iY < iRInner; iY++)
    {
        // Calculate the offset to the "left" that will will shift our inner radius
        int iYInRadiusOuter = iROuter-(iY+iDY0);
        int iXInRadiusOuter16 = (iROuter<<4)-iSqrt16(iROuter * iROuter - iYInRadiusOuter * iYInRadiusOuter);

        int iYInRadiusInner = iY;
        int iYInRadius16 = (iYInRadiusInner << 4);
        int iYInRadius16_2 = iYInRadius16 * iYInRadius16;

        int iThisXInset = iXInset;//iXInset + iRInner - iXInRadiusInner;
        int iYInDib = iY0 + iY + iDY0;
        int iYInGeo = iY;
        int iAlphaThisLine = iATop + (iABtm-iATop) * iYInGeo / iRInner;

        if (iY == iRInner-1)
            iAlphaThisLine = iABtm * 75 / 100;

        int iAlphaThis = iAlphaThisLine;


        int iXInRadiusInner16 = (iRInner << 4);
        iXInRadiusInner16 += iXInRadiusOuter16;

        // Calculate a start offset so we don't have to search so far
        int iXInRadiusStart = __min(iSqrt(iRad2_Plus-iYInRadiusInner*iYInRadiusInner)+1, iRInner);
        int iXOffset = iRInner - iXInRadiusStart;
        
        iThisXInset += iXOffset;
        iXInRadiusInner16 -= (iXOffset<<4);


        int iRadToPoint16 = iSqrt(iYInRadius16_2 + iXInRadiusInner16 * iXInRadiusInner16);
        int iDEdge = iRadToPoint16 - iRadiusInner16;

        HL_PIXEL *pL = Pixel(iThisXInset, iYInDib);
        HL_PIXEL *pR = Pixel(m_iImageDX - iThisXInset - 1, iYInDib);

        while (iDEdge > 0 && iXInRadiusInner16 > 0)
        {
            if (iDEdge < 16)
            {
                int iAlphaThisPixel = 16-iDEdge;
                iAlphaThis = (iAlphaThisLine * iAlphaThisPixel) >> 4;

                if (iAlphaThis > 0)
                {
                    int iRAdd = (iAlphaThis << 8) & 0xF800;
                    int iGAdd = (iAlphaThis << 3) & 0x07E0;
                    int iBAdd = (iAlphaThis >> 3);
                    *pR = (HL_PIXEL)(iRAdd|iGAdd|iBAdd);
                    *pL = (HL_PIXEL)(iRAdd|iGAdd|iBAdd);
                }
            }

            iXInRadiusInner16 -= 16;
            iRadToPoint16 = iSqrt(iYInRadius16_2 + iXInRadiusInner16 * iXInRadiusInner16);
            iDEdge = iRadToPoint16 - iRadiusInner16;
            pL++;
            pR--;        
        }

        if (iDEdge <= 0)
            iAlphaThis = iAlphaThisLine;

        int iRAdd = (iAlphaThis << 8) & 0xF800;
        int iGAdd = (iAlphaThis << 3) & 0x07E0;
        int iBAdd = (iAlphaThis >> 3);

        HL_PIXEL wSet = iRAdd|iGAdd|iBAdd;

        while (pL <= pR)
        {
            *pL= wSet;
            pL++;
        }
    }
}
/*============================================================================*/

    void                        CAlphaDIB::DrawAlphaRect(
    int                         iY0,
    int                         iDY,
    int                         iATop,
    int                         iABtm,
    int                         iXInset )

/*============================================================================*/
{
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pL = Pixel(iXInset, iY0+iY);
        HL_PIXEL *pR = Pixel(m_iImageDX - iXInset - 1, iY0+iY);

        int iAlphaThisLine = iATop + (iABtm-iATop) * iY / iDY;
        int iAlphaThis = iAlphaThisLine / 2;
        
        // Left and right edges are 50%
        int iRAdd = (iAlphaThis << 8) & 0xF800;
        int iGAdd = (iAlphaThis << 3) & 0x07E0;
        int iBAdd = (iAlphaThis >> 3);
#ifdef HLOSD
        if (m_iAttribs & DIBATTRIB_BACKGROUNDEMBEDDED)
        {
            int iA = *pL & 0xFF000000;
            int iR = ((*pL & 0x00FF0000) >> 16)  + iAlphaThis;
            int iG = ((*pL & 0x0000FF00) >> 8)   + iAlphaThis;
            int iB =  (*pL & 0x000000FF)          + iAlphaThis;
            *pL = iA|(iR<<16)|(iG<<8)|iB;

            iA = *pR & 0xFF000000;
            iR = ((*pR & 0x00FF0000) >> 16)  + iAlphaThis;
            iG = ((*pR & 0x0000FF00) >> 8)   + iAlphaThis;
            iB =  (*pR & 0x000000FF)          + iAlphaThis;
            *pR = iA|(iR<<16)|(iG<<8)|iB;
        }
        else
        {
            HL_PIXEL wRes = (iAlphaThisLine << 23)|0x00FFFFFF;
            *pL = wRes;
            *pR = wRes;
        }
#else
        *pL= (HL_PIXEL)(iRAdd|iGAdd|iBAdd);
        *pR= (HL_PIXEL)(iRAdd|iGAdd|iBAdd);
#endif


        pL++;
        pR--;
        
        iRAdd = (iAlphaThisLine << 8) & 0xF800;
        iGAdd = (iAlphaThisLine << 3) & 0x07E0;
        iBAdd = (iAlphaThisLine >> 3);

#ifdef HLOSD
        if (m_iAttribs & DIBATTRIB_BACKGROUNDEMBEDDED)
        {
            while (pL <= pR)
            {
                int iA = *pL & 0xFF000000;
                int iR = ((*pL & 0x00FF0000) >> 16)  + iAlphaThisLine;
                int iG = ((*pL & 0x0000FF00) >> 8)   + iAlphaThisLine;
                int iB =  (*pL & 0x000000FF)          + iAlphaThisLine;
                *pL = iA|(iR<<16)|(iG<<8)|iB;
                pL++;
            }
        }
        else
        {
            HL_PIXEL wRes = (iAlphaThisLine << 24)|0x00FFFFFF;
            while (pL <= pR)
            {
                *pL= wRes;
                pL++;
            }
        }
#else
        HL_PIXEL wRes = iRAdd|iGAdd|iBAdd;
        while (pL <= pR)
        {
            *pL= wRes;
            pL++;
        }
#endif
    }
}
/*============================================================================*/

    CHLRect                     CAlphaDIB::GetAlphaRect()

/*============================================================================*/
{
    int iXMin = DX();
    int iXMax = 0;
    int iYMin = DY();
    int iYMax = 0;
    for (int iY = 0; iY < m_iDY; iY++)
    {
        BYTE *pMask = GetMask(0, iY);
        for (int iX = 0; iX < m_iImageDX; iX++)
        {
            if (*pMask > 8)
            {
                iXMin = __min(iXMin, iX);
                iXMax = __max(iXMax, iX);
                iYMin = __min(iYMin, iY);
                iYMax = __max(iYMax, iY);
            }

            pMask++;
        }
    }

    return CDXYRect(iXMin, iYMin, iXMax-iXMin+1, iYMax-iYMin+1);
}
/*============================================================================*/

        CHLDibDC                    *CAlphaDIB::CreateCopy(
        int                         iAttribs )

/*============================================================================*/
{
    if (iAttribs == -1)
        iAttribs = m_iAttribs;

    CAlphaDIB *pNew = new CAlphaDIB(ImageDX(), DY(), 0, 0, iAttribs);
    pNew->BltFromDibDC(this, 0, 0);

    if (m_pMask != NULL)
    {
        pNew->InitMask(0);
        BYTE *pTgt = pNew->GetMask();
        memcpy(pTgt, m_pMask, DX()*DY());
    }

    pNew->AlphaFlags(AlphaFlags());

    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CAlphaDIB::CreateCroppedCopy(CHLRect rSrc)

/*============================================================================*/
{
    CAlphaDIB *pNew = new CAlphaDIB(rSrc.DX(), rSrc.DY(), 0, 0);
    pNew->BltFromDibDC(0, 0, this, rSrc.XLeft(), rSrc.YTop(), rSrc.DX(), rSrc.DY());
    for (int iY = 0; iY < rSrc.DY(); iY++)
    {
        BYTE *pSrc = GetMask(rSrc.XLeft(), rSrc.YTop()+iY);
        BYTE *pTgt = pNew->GetMask(0, iY);
        memcpy(pTgt, pSrc, rSrc.DX());
    }

    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CAlphaDIB::CreateFlip(
    BOOL                        bFlipHz, 
    BOOL                        bFlipVt )

/*============================================================================*/
{
    CAlphaDIB *pNew = new CAlphaDIB(ImageDX(), DY(), 0, 0, m_iAttribs);

    if (bFlipHz && bFlipVt)
    {
        for (int iY = 0; iY < DY(); iY++)
        {
            HL_PIXEL *pIn   = Pixel(ImageDX()-1, iY);
            HL_PIXEL *pOut  = pNew->Pixel(0, DY()-iY-1);

            BYTE *pAIn      = GetMask(ImageDX()-1, iY);
            BYTE *pAOut     = pNew->GetMask(0, DY()-iY-1);

            for (int iX = 0; iX < ImageDX(); iX++)
            {
                *pOut = *pIn;
                *pAOut = *pAIn;
                pOut++;
                pIn--;
                pAOut++;
                pAIn--;
            }
        }

        return pNew;
    }

    if (bFlipHz)
    {
        for (int iX = 0; iX < ImageDX(); iX++)
        {
            HL_PIXEL *pIn   = Pixel(iX, 0);
            HL_PIXEL *pOut  = pNew->Pixel(ImageDX()-iX-1, 0);
            BYTE *pAIn      = GetMask(iX, 0);
            BYTE *pAOut     = pNew->GetMask(ImageDX()-iX-1, 0);

            for (int iY = 0; iY < DY(); iY++)
            {
                *pOut = *pIn;
                *pAOut = *pAIn;

                pIn += DXPitch();
                pOut += DXPitch();

                pAIn += DX();
                pAOut += DX();                
            }
        }
    }
    if (bFlipVt)
    {
        for (int iY = 0; iY < DY(); iY++)
        {
            HL_PIXEL *pIn   = Pixel(0, iY);
            HL_PIXEL *pOut  = pNew->Pixel(0, DY()-iY-1);
            BYTE *pAIn      = GetMask(0, iY);
            BYTE *pAOut     = pNew->GetMask(0, DY()-iY-1);

            memcpy(pOut, pIn, DX() * sizeof(HL_PIXEL));
            memcpy(pAOut, pAIn, DX());
        }
    }

    pNew->AlphaFlags(m_iAlphaFlags);
    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CAlphaDIB::CreateScaledInstance(
    SIZE                        szNew,
    BOOL                        bSmooth, 
    int                         iAttribs )

/*============================================================================*/
{
    START_TIMER(GDI_SCALE);

    int iDXNew = szNew.cx;
    int iDYNew = szNew.cy;

    int iScaleX = ImageDX() / szNew.cx;
    int iScaleY = DY() / szNew.cy;

    if (iScaleX >= 2 || iScaleY >= 2)
    {
        CAlphaDIB *pRet = CreateBicubicScale(szNew.cx, szNew.cy, iAttribs);
        return pRet;
    }

    CAlphaDIB *pNew = new CAlphaDIB(iDXNew, iDYNew, 0, 0, iAttribs);
    int iDX = ImageDX();
    int iDY = DY();

    CHLDibDC *pSrc = this;

    //
    // No need to do this over and over, just make up some LUTs
    //
    int *piXFloor = new int[iDXNew];
    int *piXCeil  = new int[iDXNew];
    int *piFractionX = new int[iDXNew];
    int *piOneMinusX = new int[iDXNew];

    for (int x = 0; x < iDXNew; ++x)
    {
        // Setup
        int floor_x = x * iDX / iDXNew;
        int ceil_x = floor_x + 1;
        if (ceil_x >= iDX) 
            ceil_x = floor_x;

        int iFractionX = x * 128* iDX / iDXNew - (floor_x * 128);
        int iOneMinusX = 128 - iFractionX;        
        piXFloor[x] = floor_x;
        piXCeil[x]  = ceil_x;
        piFractionX[x]  = iFractionX;
        piOneMinusX[x]  = iOneMinusX;
    }

    for (int y = 0; y < iDYNew; ++y)
    {
        int floor_y = y * iDY / iDYNew;
        int ceil_y = floor_y + 1;
        if (ceil_y >= iDY) 
            ceil_y = floor_y;

        int iFractionY = y * 128 * iDY / iDYNew - (floor_y * 128);
        int iOneMinusY = 128 - iFractionY;        

        HL_PIXEL *pOut = pNew->Pixel(0, y);
        BYTE *pAlpha = pNew->GetMask(0, y);


        HL_PIXEL c1Last = 0;
        HL_PIXEL c2Last = 0;
        HL_PIXEL c3Last = 0;
        HL_PIXEL c4Last = 0;
        DWORD byR1 = 0, byG1 = 0, byB1 = 0;
        DWORD byR2 = 0, byG2 = 0, byB2 = 0;
        DWORD byR3 = 0, byG3 = 0, byB3 = 0;
        DWORD byR4 = 0, byG4 = 0, byB4 = 0;

        for (int x = 0; x < iDXNew; ++x)
        {
            // Setup
            int floor_x     = piXFloor[x];
            int ceil_x      = piXCeil[x];
            int iFractionX  = piFractionX[x];
            int iOneMinusX  = piOneMinusX[x];


            HL_PIXEL c1 = *pSrc->Pixel(floor_x, floor_y);
            HL_PIXEL c2 = *pSrc->Pixel(ceil_x, floor_y);
            HL_PIXEL c3 = *pSrc->Pixel(floor_x, ceil_y);
            HL_PIXEL c4 = *pSrc->Pixel(ceil_x, ceil_y);

            // Alpha
            BYTE a1 = *pSrc->GetMask(floor_x, floor_y);
            BYTE a2 = *pSrc->GetMask(ceil_x, floor_y);
            BYTE a3 = *pSrc->GetMask(floor_x, ceil_y);
            BYTE a4 = *pSrc->GetMask(ceil_x, ceil_y);


            #ifdef HL_PIXEL32
                if (c1 != c1Last)
                {
                    byR1 = GetRValueHLPIXEL(c1);
                    byG1 = GetGValueHLPIXEL(c1);
                    byB1 = GetBValueHLPIXEL(c1);
                    c1Last = c1;
                }
                if (c2 != c2Last)
                {
                    byR2 = GetRValueHLPIXEL(c2);
                    byG2 = GetGValueHLPIXEL(c2);
                    byB2 = GetBValueHLPIXEL(c2);
                    c2Last = c2;
                }
                if (c3 != c3Last)
                {
                    byR3 = GetRValueHLPIXEL(c3);
                    byG3 = GetGValueHLPIXEL(c3);
                    byB3 = GetBValueHLPIXEL(c3);
                    c3Last = c3;
                }
                if (c4 != c4Last)
                {
                    byR4 = GetRValueHLPIXEL(c4);
                    byG4 = GetGValueHLPIXEL(c4);
                    byB4 = GetBValueHLPIXEL(c4);
                    c4Last = c4;
                }
            #else
                if (c1 != c1Last)
                {
                    byR1 = c1 & 0x0000F800;
                    byG1 = c1 & 0x000007E0;
                    byB1 = c1 & 0x0000001F;
                    c1Last = c1;
                }
                if (c2 != c2Last)
                {
                    byR2 = c2 & 0x0000F800;
                    byG2 = c2 & 0x000007E0;
                    byB2 = c2 & 0x0000001F;
                    c2Last = c2;
                }
                if (c3 != c3Last)
                {
                    byR3 = c3 & 0x0000F800;
                    byG3 = c3 & 0x000007E0;
                    byB3 = c3 & 0x0000001F;
                    c3Last = c3;
                }
                if (c4 != c4Last)
                {
                    byR4 = c4 & 0x0000F800;
                    byG4 = c4 & 0x000007E0;
                    byB4 = c4 & 0x0000001F;
                    c4Last = c4;
                }
            #endif

            // Blue
            #ifdef HL_PIXEL32
                DWORD b1     = (iOneMinusX * byB1 + iFractionX * byB2); // b1 * 128
                DWORD b2     = (iOneMinusX * byB3 + iFractionX * byB4); // b2 * 128
                DWORD blue   = ((iOneMinusY * b1   + iFractionY * b2 ) >> 14);

                // Green
                b1          = (iOneMinusX * byG1 + iFractionX * byG2);
                b2          = (iOneMinusX * byG3 + iFractionX * byG4);
                DWORD green = ((iOneMinusY * b1   + iFractionY * b2) >> 14);

                // Red
                b1          = (iOneMinusX * byR1 + iFractionX * byR2);  // 0-25500
                b2          = (iOneMinusX * byR3 + iFractionX * byR4);  // 0-25500
                DWORD red   = (iOneMinusY * b1   + iFractionY * b2) >> 14;    // 0-
            #else
                UINT b1     = (iOneMinusX * byB1 + iFractionX * byB2); // b1 * 128
                UINT b2     = (iOneMinusX * byB3 + iFractionX * byB4); // b2 * 128
                UINT blue   = (((iOneMinusY * b1 + iFractionY * b2) >> 14) & 0x0000001F);

                // Green
                b1          = (iOneMinusX * byG1 + iFractionX * byG2);
                b2          = (iOneMinusX * byG3 + iFractionX * byG4);
                UINT green  = (((iOneMinusY * b1 + iFractionY * b2) >> 14) & 0x000007E0);

                // Red
                b1          = (iOneMinusX * byR1 + iFractionX * byR2);  // 0-25500
                b2          = (iOneMinusX * byR3 + iFractionX * byR4);  // 0-25500
                UINT red    = (((iOneMinusY * b1 + iFractionY * b2) >> 14) & 0x0000F800);
            #endif

            #ifdef HL_PIXEL32
                LOGASSERT(red < 256);
                LOGASSERT(green < 256);
                LOGASSERT(blue < 256);
                *pOut       = RGBtoHLPIXEL(red, green, blue);
            #else
                *pOut       = (red  | green  | blue);
            #endif

            // Alpha
            b1              = (iOneMinusX * a1 + iFractionX * a2);
            b2              = (iOneMinusX * a3 + iFractionX * a4);
            BYTE byAlpha    = (BYTE)((iOneMinusY * b1 + iFractionY * b2) >> 14);

            *pAlpha = byAlpha;
            pOut++;
            pAlpha++;
        }
    }

    if (pSrc != this)
        delete pSrc;

    delete [] piXFloor;
    delete [] piXCeil;
    delete [] piFractionX;
    delete [] piOneMinusX;

    END_TIMER(GDI_SCALE);

    return pNew;
}
/*============================================================================*/

    CHLDibDC                    *CAlphaDIB::CreateScaleDIV2()

/*============================================================================*/
{
    int iDXNew = m_iImageDX / 2;
    int iDYNew = m_iDY / 2;
    CAlphaDIB *pRet = new CAlphaDIB(iDXNew, iDYNew, 0, 0);
    for (int iY = 0; iY < iDYNew; iY++)
    {
        HL_PIXEL *pTgt = pRet->Pixel(0, iY);

        HL_PIXEL *pUL = Pixel(0, iY*2);
        HL_PIXEL *pLL = Pixel(0, iY*2+1);
        HL_PIXEL *pUR = pUL+1;
        HL_PIXEL *pLR = pLL+1;

        for (int iX = 0; iX < iDXNew; iX++)
        {
            int iRSum = (*pUL & 0xF800)+(*pUR & 0xF800)+(*pLL & 0xF800)+(*pLR & 0xF800);
            int iGSum = (*pUL & 0x07E0)+(*pUR & 0x07E0)+(*pLL & 0x07E0)+(*pLR & 0x07E0);
            int iBSum = (*pUL & 0x001F)+(*pUR & 0x001F)+(*pLL & 0x001F)+(*pLR & 0x001F);
            int iR = (iRSum >> 2) & 0xF800;
            int iG = (iGSum >> 2) & 0x07E0;
            int iB = (iBSum >> 2);
            *pTgt = (HL_PIXEL)iR|iG|iB;
            pTgt++;
            pUL += 2;
            pLL += 2;
            pUR += 2;
            pLR += 2;
        }
    }

    for (int iY = 0; iY < iDYNew; iY++)
    {
        BYTE *pTgt = pRet->GetMask(0, iY);

        BYTE *pUL = GetMask(0, iY*2);
        BYTE *pLL = GetMask(0, iY*2+1);
        BYTE *pUR = pUL+1;
        BYTE *pLR = pLL+1;

        for (int iX = 0; iX < iDXNew; iX++)
        {
            int iSum = *pUL + *pLL + *pUR + *pLR;
            *pTgt = (BYTE)(iSum>>2);
            pTgt++;
            pUL += 2;
            pLL += 2;
            pUR += 2;
            pLR += 2;
        }
    }

    return pRet;
}
/*============================================================================*/

    CAlphaDIB                   *CAlphaDIB::CreateFastScaleCopy(
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    CAlphaDIB *pNew = new CAlphaDIB(iDX, iDY, 0, 0);

    DWORD *wX = new DWORD[iDX];
    for (int i = 0; i < iDX; i++)
    {
        wX[i] = i * m_iImageDX / iDX;
        wX[i] = __min(wX[i], (DWORD)m_iImageDX-1);
    }

    DWORD *dwXC = new DWORD[iDX];
    int iN = 1;
    int iLast = 0;
    int iMax = 0;
    for (int iX = iDX-1; iX >= 0; iX--)
    {
        if (wX[iX] != (DWORD)(iLast-1))
            iN = 1;
        else
            iN++;
        iMax = __max(iMax, iN);
        iLast = wX[iX];
        dwXC[iX] = iN;                        
    }

    if (iMax < 3)
    {
        delete [] dwXC;
        dwXC = NULL;

        for (int iY = 0; iY < iDY; iY++)
        {
            int iYIn = iY * DY() / iDY;
            iYIn = __min(iYIn, DY()-1);

            HL_PIXEL *pIn = Pixel(0, iYIn);
            HL_PIXEL *pOut = pNew->Pixel(0, iY);
            BYTE *pAIn = GetMask(0, iYIn);
            BYTE *pAOut = pNew->GetMask(0, iY);
            for (int iX = 0; iX < iDX; iX++)
            {
                *pOut = *(pIn+wX[iX]);
                *pAOut = *(pAIn+wX[iX]);
                pOut++;
                pAOut++;
            }
        }
    }
    else
    {
        for (int iY = 0; iY < iDY; iY++)
        {
            int iYIn = iY * DY() / iDY;
            iYIn = __min(iYIn, DY()-1);

            HL_PIXEL *pIn = Pixel(0, iYIn);
            HL_PIXEL *pOut = pNew->Pixel(0, iY);
            BYTE *pAIn = GetMask(0, iYIn);
            BYTE *pAOut = pNew->GetMask(0, iY);
            int iXOut = 0;
            while (iXOut < iDX)
            {
                int iN = dwXC[iXOut];
                int iOff = wX[iXOut];
                PIXEL_COPY(pOut, pIn+iOff, iN);
                memcpy(pAOut, pAIn+iOff, iN);
                
                iXOut += iN;
                pAOut += iN;
                pOut += iN;
            }
        }

        delete [] dwXC;
    }

    delete [] wX;

    return pNew;
}
/*============================================================================*/

    CAlphaDIB                   *CAlphaDIB::CreateScaleCopy(
    int                         iFactor )

/*============================================================================*/
{
    int iDXNew = m_iImageDX / iFactor;
    int iDYNew = m_iDY / iFactor;
    CAlphaDIB *pRet = new CAlphaDIB(iDXNew, iDYNew, 0, 0, m_iAttribs);
    pRet->m_iAlphaFlags = m_iAlphaFlags;
    for (int iY = 0; iY < iDYNew; iY++)
    {   
        HL_PIXEL *pPixSource = Pixel(0, iY*iFactor);
        HL_PIXEL *pPixTarget = pRet->Pixel(0, iY);
        BYTE *pASource = GetMask(0, iY*iFactor);
        BYTE *pATarget = pRet->GetMask(0, iY);

        for (int iX = 0; iX < iDXNew; iX++)
        {
            *pPixTarget = *pPixSource;
            *pATarget = *pASource;
            pPixTarget++;
            pATarget++;
            pPixSource += iFactor;
            pASource += iFactor;
        }
    }

    return pRet;
}
/*============================================================================*/

    CAlphaDIB                   *CAlphaDIB::CreateBicubicScale(
    int                         iDXTgt,
    int                         iDYTgt,
    int                         iAttribs )

/*============================================================================*/
{
    CAlphaDIB *pRet = new CAlphaDIB(iDXTgt, iDYTgt, 0, 0, iAttribs);
    
    for (int iYTgt = 0; iYTgt < iDYTgt; iYTgt++)
    {
        HL_PIXEL *pOut = pRet->Pixel(0, iYTgt);
        BYTE *pMask = pRet->GetMask(0, iYTgt);
        for (int iXTgt = 0; iXTgt < iDXTgt; iXTgt++)
        {
            HL_PIXEL wRes = GetBicubicRGB(iXTgt, iYTgt, iDXTgt, iDYTgt);
            BYTE wAlpha = GetBicubicAlpha(iXTgt, iYTgt, iDXTgt, iDYTgt);
            *pOut = wRes;
            *pMask = wAlpha;
            pOut++;
            pMask++;
        }
    }


    return pRet;
}
/*============================================================================*/

    BYTE                        CAlphaDIB::GetBicubicAlpha(
    int                         iXTgt,
    int                         iYTgt,
    int                         iDXTgt,
    int                         iDYTgt )

/*============================================================================*/
{
    int iXLSrc256 = (iXTgt) * m_iImageDX * 256 / iDXTgt;
    int iXRSrc256 = (iXTgt+1) * m_iImageDX * 256 / iDXTgt;
    int iYTSrc256 = iYTgt * m_iDY * 256 / iDYTgt;
    int iYBSrc256 = (iYTgt+1) * m_iDY * 256 / iDYTgt;
    int iXL = iXLSrc256 / 256;
    int iXR = (iXRSrc256-1) / 256;
    int iYT = iYTSrc256 / 256;
    int iYB = (iYBSrc256-1) / 256;

    LOGASSERT(iXR >= 0 && iXR < m_iImageDX);
    LOGASSERT(iYB >= 0 && iYB < m_iDY);

    int iXLFact = 256-(iXLSrc256-256*iXL);
    int iXRFact = (iXRSrc256-256*iXR);
    int iYTFact = 256-(iYTSrc256-256*iYT);
    int iYBFact = (iYBSrc256-256*iYB);
    int iDXMid = iXR - iXL - 1;
    int iDYMid = iYB - iYT - 1;
    int iNBlockMid = (iDXMid * iDYMid);

    // Edges (minus corners)
    int iDenom = (iNBlockMid * 256) + iDYMid * iXLFact + iDYMid * iXRFact + iDXMid * iYTFact + iDXMid * iYBFact;
    int iULFact = iXLFact * iYTFact / 256;
    int iLLFact = iXLFact * iYBFact / 256;
    int iURFact = iXRFact * iYTFact / 256;
    int iLRFact = iXRFact * iYBFact / 256;
    iDenom += (iULFact+iLLFact+iURFact+iLRFact);

    DWORD dwSum = 0;

    //
    // Corners
    //
    BYTE *pUL = GetMask(iXL, iYT);
    BYTE *pLL = GetMask(iXL, iYB);
    BYTE *pUR = GetMask(iXR, iYT);
    BYTE *pLR = GetMask(iXR, iYB);

    dwSum += (*pUL * iULFact);
    dwSum += (*pLL * iLLFact);
    dwSum += (*pUR * iURFact);
    dwSum += (*pLR * iLRFact);

    int iX = 0;
    int iY = 0;

    //
    // Verticals and field
    //
    if (iDYMid > 0)
    {
        BYTE *pL = GetMask(iXL, iYT+1);
        BYTE *pR = GetMask(iXR, iYT+1);
        for (iY = 0; iY < iDYMid; iY++)
        {
            dwSum += (*pL * iXLFact);
            dwSum += (*pR * iXRFact);
        
            BYTE *pF = GetMask(iXL+1, iYT+1+iY);
            for (iX = 0; iX < iDXMid; iX++)
            {
                dwSum += (*pF << 8);
                pF++; 
            }

            pL += m_iDX;
            pR += m_iDX;
        }
    }

    //
    // Horizontals
    //
    if (iDXMid > 0)
    {
        BYTE *pT = GetMask(iXL+1, iYT);
        BYTE *pB = GetMask(iXL+1, iYB);
        for (iX = 0; iX < iDXMid; iX++)
        {
            dwSum += (*pT * iYTFact);
            dwSum += (*pB * iYBFact);
            pT++;
            pB++;
        }
    }

    return (BYTE)(dwSum / iDenom);
}
/*============================================================================*/

    BYTE                        *CAlphaDIB::GetMask()

/*============================================================================*/
{
    return CHLDibDC::GetMask();
}
/*============================================================================*/

    BYTE                        *CAlphaDIB::GetMask(int iX, int iY)

/*============================================================================*/
{
    return CHLDibDC::GetMask(iX, iY);
}
/*============================================================================*/

    void                        CAlphaDIB::GenAlphaChannel(
    COLORREF                    rgbBG,
    CHLRect                     *pROI )

/*============================================================================*/
{
    FlushText();

    HL_PIXEL wBG = RGBTOHLPIXEL(rgbBG);

    //
    // Max diff = 31 * 3 = 93
    //
    int iX0 = 0;
    int iY0 = 0;
    int iDX = ImageDX();
    int iDY = DY();

    if (pROI != NULL)
    {
        iX0 = pROI->XLeft();
        iY0 = pROI->YTop();
        iDX = pROI->DX();
        iDY = pROI->DY();
    }

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pPIX = Pixel(iX0, iY+iY0);
        BYTE *pAlpha = GetMask(iX0, iY+iY0);
        memset(pAlpha, 0xFF, iDX);

        for (int iX = 0; iX < iDX; iX++)
        {
            if (*pPIX == wBG)
                *pAlpha = 0;

            pPIX++;
            pAlpha++;
        }
    }
}
/*============================================================================*/

    void                        CAlphaDIB::FillMaskRoundRectExt(
    BYTE                        byMask,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad )

/*============================================================================*/
{
    if (iRad < 1)
        return;

    int iR = __min(iRad, iDX/2);
    iR = __min(iR, iDY/2);

    iXLeft += m_iXLeft;
    iYTop += m_iYTop;

    int iXA = iXLeft;
    int iXB = iXLeft + iDX - 1;
    int iYA = iYTop  + iR;
    int iYB = iYTop  + iDY - iR - 1;

    CPointList PointList;
    CalcArcPoints(&PointList, iR, TRUE);

    int iN = PointList.NPoints();
    for (int i = 0; i < iN; i++)
    {
        int iDXR = PointList.X(i);
        int iDYR = PointList.Y(i);
        int iXRad = iR - iDXR;

        FillMaskHZ(byMask, iXA, iXA + iXRad, iYB + iDYR);
        FillMaskHZ(byMask, iXB-iXRad, iXB,   iYB + iDYR);
        FillMaskHZ(byMask, iXA, iXA + iXRad, iYA - iDYR);
        FillMaskHZ(byMask, iXB-iXRad, iXB,   iYA - iDYR);
    }
}
/*============================================================================*/

    void                        CAlphaDIB::FillRoundRectAA(
    COLORREF                    rgb,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iAlphaMax )

/*============================================================================*/
{
    int iRad16 = iRad*16+8;
    FillRect(rgb, iXLeft, iYTop, iDX, iDY);
    BYTE bAMax = (BYTE)(iAlphaMax * 0xFF / 100);
    for (int iY = 0; iY < iRad; iY++)
    {
        BYTE *pUL = GetMask(iXLeft, iYTop+iY);
        BYTE *pLL = GetMask(iXLeft, iYTop+iDY-iY-1);
        BYTE *pUR = GetMask(iXLeft+iDX-1, iYTop+iY);
        BYTE *pLR = GetMask(iXLeft+iDX-1, iYTop+iDY-iY-1);

        int iYInRad = iRad-iY;
        int iXInRad = iRad;
        int iYInRad_2 = iYInRad*iYInRad;

        while (iXInRad > 0)
        {
            int iRadToPoint16 = iSqrt16(iYInRad_2+iXInRad*iXInRad);
            int iDEdge = iRadToPoint16 - iRad16;
            int iA = 0;
            if (iDEdge < 16)
            {
                if (iDEdge <= 0)
                {
                    iXInRad = 0;
                    iA = 0xFF;
                }
                else
                {
                    iA = 0xFF-(iDEdge << 4);
                }
                iA = (iA * iAlphaMax) >> 8;
            }

            *pUL = (BYTE)iA;
            *pUR = (BYTE)iA;
            *pLL = (BYTE)iA;
            *pLR = (BYTE)iA;

            pUL++;
            pLL++;
            pUR--;
            pLR--;
            iXInRad--;
        }

        memset(pUL, bAMax, pUR-pUL+1);
        memset(pLL, bAMax, pLR-pLL+1);
    }
    for (int iY = 0; iY < iDY-2*iRad; iY++)
    {
        BYTE *pMask = GetMask(iXLeft, iY+iYTop+iRad);
        memset(pMask, bAMax, iDX);
    }
}
/*============================================================================*/

    void                        CAlphaDIB::ScaleAlpha(
    int                         iPCT )

/*============================================================================*/
{
    if (m_iAlphaFlags & ALPHA_PREMULT)
    {
        int iFact = iPCT * 64 / 100;

        if (m_iAttribs & DIBATTRIB_ALPHAADDMODE)
        {
            for (int iY = 0; iY < m_iDY ; iY++)
            {
                HL_PIXEL *pPIX = Pixel(0, iY);

                for (int iX = 0; iX < m_iImageDX; iX++)
                {
                    HL_PIXEL wSrc = *pPIX;
                    DWORD wRB = wSrc & 0xF81F;
                    DWORD wG  = wSrc & 0x07E0;
                    DWORD wRBOut = ((wRB * iFact) >> 6) & 0xF81F;
                    DWORD wGOut = ((wG * iFact) >> 6) & 0x07E0;
                    *pPIX = (HL_PIXEL)(wRBOut|wGOut);
                    pPIX++;
                }
            }
        }
        else
        {
            for (int iY = 0; iY < m_iDY ; iY++)
            {
                BYTE *pAlpha = GetMask(0, iY);
                HL_PIXEL *pPIX = Pixel(0, iY);

                for (int iX = 0; iX < m_iImageDX; iX++)
                {
                    int byAlpha = *pAlpha;
                    byAlpha = 255 - byAlpha * 255 / 16;
                    HL_PIXEL wSrc = *pPIX;
                    DWORD wRB = wSrc & 0xF81F;
                    DWORD wG  = wSrc & 0x07E0;
                    DWORD wRBOut = ((wRB * iFact) >> 6) & 0xF81F;
                    DWORD wGOut = ((wG * iFact) >> 6) & 0x07E0;
                    *pPIX = (HL_PIXEL)(wRBOut|wGOut);
                    pPIX++;
                    int iA = (int)byAlpha * iPCT / 100;
                    *pAlpha = (BYTE)(16 - iA * 16 / 255);
                    pAlpha++;
                }
            }
        }
        
        return;
    }


    int iNPIX = m_iDX * m_iDY;
    BYTE *pA = GetMask();
    for (int i = 0; i < iNPIX; i++)
    {
        int iA = *pA;
        iA = iA * iPCT / 100;
        *pA = iA;
        pA++;
    }
}
#ifdef HLOSD
/*============================================================================*/

    void                        CAlphaDIB::ZeroOutsideRadius(
    int                         iRad )

/*============================================================================*/
{
    int iRad16 = iRad*16+8;
    for (int iY = 0; iY < iRad; iY++)
    {
        HL_PIXEL *pUL = Pixel(0, iY);
        HL_PIXEL *pLL = Pixel(0, m_iDY-iY-1);
        HL_PIXEL *pUR = Pixel(m_iImageDX-1, iY);
        HL_PIXEL *pLR = Pixel(m_iImageDX-1, m_iDY-iY-1);

        int iYInRad = iRad-iY;
        int iXInRad = iRad;
        int iYInRad_2 = iYInRad*iYInRad;

        while (iXInRad > 0)
        {
            int iRadToPoint16 = iSqrt16(iYInRad_2+iXInRad*iXInRad);
            int iDEdge = iRadToPoint16 - iRad16;
            int iA = 0;
            if (iDEdge < 16)
            {
                if (iDEdge <= 0)
                {
                    iXInRad = 0;
                    iA = 0xFF;
                }
                else
                    iA = 0xFF-(iDEdge << 4);
            }

            HL_PIXEL wUL = (*pUL) & 0x00FFFFFF; 
            int iAUL = (((wUL >> 24) * iA) << 16) & 0xFF000000;
            *pUL = iAUL | wUL;

            HL_PIXEL wLL = (*pLL) & 0x00FFFFFF; 
            int iALL = (((wLL >> 24) * iA) << 16) & 0xFF000000;
            *pLL = iALL | wLL;

            HL_PIXEL wUR = (*pUR) & 0x00FFFFFF;     
            int iAUR = (((wUR >> 24) * iA) << 16) & 0xFF000000;
            *pUR = iAUR | wUR;

            HL_PIXEL wLR = (*pLR) & 0x00FFFFFF;     
            int iALR = (((wLR >> 24) * iA) << 16) & 0xFF000000;
            *pLR = iALR | wLR;
            
            pUL++;
            pLL++;
            pUR--;
            pLR--;
            iXInRad--;
        }
    }
}
#endif
/*============================================================================*/

    void                        CAlphaDIB::CopyFrom(
    int                         iXDst,
    int                         iYDst,
    CAlphaDIB                   *pSrc,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pSrc))
        return;

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pIn   = pSrc->Pixel(iXSrc, iYSrc+iY);
        HL_PIXEL *pOut  = Pixel(iXDst, iYDst+iY);
        BYTE *pAlphaIn  = pSrc->GetMask(iXSrc, iYSrc+iY);
        BYTE *pAlphaOut = GetMask(iXDst, iYDst+iY);
        memcpy(pOut, pIn, iDX * sizeof(HL_PIXEL));
        memcpy(pAlphaOut, pAlphaIn, iDX);
    }
}
/*============================================================================*/

    CAlphaDIB                   *CAlphaDIB::AlphaDIB()

/*============================================================================*/
{
    return this;
}
/*============================================================================*/

    CAlphaDIB::HL_PREMULT_MODE  CAlphaDIB::MakePreMult()

/*============================================================================*/
{
    if (m_iAttribs & DIBATTRIB_ONESHOT)
        return PREMULT_NONE;

    // Fully encoded?
    if ((m_iAlphaFlags & ALPHA_PREMULT) == 0)
    {
        //
        // Our values have not yet been pre-multiplied
        //
        for (int iY = 0; iY < m_iDY ; iY++)
        {
            BYTE *pAlpha = GetMask(0, iY);
            HL_PIXEL *pPIX = Pixel(0, iY);

            if (m_iAttribs & DIBATTRIB_ALPHAADDMODE)
            {
                for (int iX = 0; iX < m_iImageDX; iX++)
                {
                    DWORD byAlpha = *pAlpha;
                    switch (byAlpha)
                    {
                    case 0xFF:
                        break;

                    case 0x00:
                        *pPIX = 0x000;
                        break;

                    default:    
                        {
                            DWORD dwAlpha_FIX = byAlpha;
                            dwAlpha_FIX = dwAlpha_FIX * 32 / 255;
                            HL_PIXEL wSrc = *pPIX;
                            DWORD wRB = wSrc & 0xF81F;
                            DWORD wG  = wSrc & 0x07E0;
                            DWORD wRBOut = ((wRB * dwAlpha_FIX) >> 5) & 0xF81F;
                            DWORD wGOut = ((wG * dwAlpha_FIX) >> 5 ) & 0x07E0;
                            *pPIX = (HL_PIXEL)(wRBOut|wGOut);
                            byAlpha = 32-dwAlpha_FIX;
                        }
                        break;
                    }

                    pPIX++;
                    pAlpha++;
                }
            }
            else
            {
                for (int iX = 0; iX < m_iImageDX; iX++)
                {
                    DWORD byAlpha = *pAlpha;
                    DWORD dwAlpha_FIX = byAlpha;
                    dwAlpha_FIX = dwAlpha_FIX * MAX_ALPHA_PREMULT / 255;

                    if (byAlpha != 0xFF)
                    {
                        HL_PIXEL wSrc = *pPIX;
                        DWORD wRB = wSrc & 0xF81F;
                        DWORD wG  = wSrc & 0x07E0;
                        DWORD wRBOut = ((wRB * dwAlpha_FIX) >> ALPHA_BITS) & 0xF81F;
                        DWORD wGOut = ((wG * dwAlpha_FIX) >> ALPHA_BITS) & 0x07E0;
                        *pPIX = (HL_PIXEL)(wRBOut|wGOut);
                    }
/*
                    else
                    {
                        dwAlpha_FIX = 0;
                    }
*/

                    pPIX++;
                    *pAlpha = (BYTE)(MAX_ALPHA_PREMULT- dwAlpha_FIX);
                    pAlpha++;
                }
            }
        }
    }

    // This is as far as we can go, keep our real alpha values and return
    m_iAlphaFlags |= ALPHA_PREMULT;
    return PREMULT_VALUES;
}
/*============================================================================*/

    void                        CAlphaDIB::AddRGB(
    BYTE                        byRAdd,
    BYTE                        byGAdd,
    BYTE                        byBAdd,
    CEdgeList                   *pEdges )

/*============================================================================*/
{
    HL_PIXEL wLastIn  = 0;
    HL_PIXEL wLastOut = 0;
    int iNPix = DX()*DY();
    HL_PIXEL *pIn = Bits();
    BYTE *pMask = GetMask();

    for (int i = 0; i < iNPix; i++)
    {
        if (*pIn != wLastIn)
        {
            wLastIn = *pIn;
            WORD byR = __min(255, GetRValueHLPIXEL(wLastIn) + byRAdd);
            WORD byG = __min(255, GetGValueHLPIXEL(wLastIn) + byGAdd);
            WORD byB = __min(255, GetBValueHLPIXEL(wLastIn) + byBAdd);
            wLastOut = RGBtoHLPIXEL(byR, byG, byB);
        }

        *pIn = wLastOut;

        pIn++;
        pMask++;
    }
}
/*============================================================================*/

    void                            CAlphaDIB::CreateHalo(
    int                             iSize,
    int                             iRad,
    COLORREF                        rgbOut,
    COLORREF                        rgbIn,
    int                             iCornerMask,
    int                             iAlphaMax,
    int                             iAlphaMin,
    BOOL                            bFill  )

/*============================================================================*/
{
    START_TIMER(GDI_SHINE);

    BOOL bReverse = FALSE;
    if (iSize < 0)
    {
        iSize = -iSize;
        bReverse = TRUE;
    }

    if (bFill)
    {
#ifdef HL_PIXEL32
        InitMask(iAlphaMax);
        FillRect(0xFF000000 | rgbIn, CDXYRect(0, 0, DX(), DY()));
#else
        InitMask(iAlphaMax);
        FillRect(rgbIn, CDXYRect(0, 0, DX(), DY()));
#endif
    }

    COLORREF rgb1 = rgbIn;
    COLORREF rgb2 = rgbOut;
    BYTE *pAlpha = new BYTE[iSize];
    HL_PIXEL *pColors = new HL_PIXEL[iSize];
    int iAlphaStart = 0;
    int i = 0;
    int iDAlpha = iAlphaMax-iAlphaMin;
    for (i = 0; i < iSize; i++)
    {
        if (i < iAlphaStart)
            pAlpha[i] = iAlphaMax;
        else
        {
            int iDOuter = iSize - i;
            int iAlpha = iAlphaMin + (iDOuter * iDAlpha / (iSize+1-iAlphaStart));
            iAlpha = MIN(iAlphaMax, iAlpha);
            if (bReverse)
                pAlpha[iSize-i-1] = iAlpha;
            else
                pAlpha[i] = iAlpha;
        }
    
        double dBlend = (double)(i*150/100) / (double)iSize;        
        dBlend = __min(1.0, dBlend);

        if (bReverse)
            pColors[iSize-i-1] = RGBTOHLPIXEL(CHLDC::Blend(rgb1, rgb2, dBlend));
        else
            pColors[i] = RGBTOHLPIXEL(CHLDC::Blend(rgb1, rgb2, dBlend));
    }

    BOOL bUL = (iCornerMask & Bit(CORNER_UL));
    BOOL bLL = (iCornerMask & Bit(CORNER_LL));
    BOOL bUR = (iCornerMask & Bit(CORNER_UR));
    BOOL bLR = (iCornerMask & Bit(CORNER_LR));

    for (i = 1; i < iSize; i++)
    {
        //
        // Top and bottom edges
        //
        int iXT0 = 0;
        int iXT1 = 0;
        int iXB0 = 0;
        int iXB1 = 0;

        if (bUL)    iXT0 = iSize+iRad;          else    iXT0 = iSize-i;
        if (bUR)    iXT1 = DX()-iSize-iRad-1;   else    iXT1 = DX()-iSize+i-1; 
        if (bLL)    iXB0 = iSize+iRad;          else    iXB0 = iSize-i;
        if (bLR)    iXB1 = DX()-iSize-iRad-1;   else    iXB1 = DX()-iSize+i-1; 

        int iDXT = iXT1 - iXT0 + 1;
        HL_PIXEL *pT    = Pixel(iXT0, iSize-i-1);
        BYTE *pAT       = GetMask(iXT0, iSize-i-1);
        int iX = 0;
        for (iX = 0; iX < iDXT; iX++)
        {
#ifdef HL_PIXEL32
            *pT = pColors[i-1] | (pAlpha[i-1] << 24);
#else
            *pT = pColors[i-1];
#endif
            *pAT = pAlpha[i-1];
            pT++;   
            pAT++;
        }

        int iDXB = iXB1-iXB0 + 1;
        HL_PIXEL *pB    = Pixel(iXB0, DY()-iSize+i);
        BYTE *pAB       = GetMask(iXB0, DY()-iSize+i);
        for (iX = 0; iX < iDXB; iX++)
        {
#ifdef HL_PIXEL32
            *pB = pColors[i-1] | (pAlpha[i-1] << 24);
#else
            *pB = pColors[i-1];
#endif
            *pAB = pAlpha[i-1];
            pB++;   
            pAB++;
        }

        //
        // Left and right edges
        //
        int iYL0 = 0;
        int iYL1 = 0;
        int iYR0 = 0;
        int iYR1 = 0;

        if (bUL)    iYL0 = iSize+iRad;          else    iYL0 = iSize-i-1;
        if (bLL)    iYL1 = DY()-iSize-iRad-1;   else    iYL1 = DY()-iSize+i; 
        if (bUR)    iYR0 = iSize+iRad;          else    iYR0 = iSize-i-1;
        if (bLR)    iYR1 = DY()-iSize-iRad-1;   else    iYR1 = DY()-iSize+i; 

        int iDYL = iYL1 - iYL0 + 1;
        HL_PIXEL *pL    = Pixel(iSize-i-1, iYL0);
        BYTE *pAL       = GetMask(iSize-i-1, iYL0);
        int iY = 0;
        for (iY = 0; iY < iDYL; iY++)
        {
#ifdef HL_PIXEL32
            *pL = pColors[i-1] | (pAlpha[i-1] << 24);
#else
            *pL = pColors[i-1];
#endif
            *pAL = pAlpha[i-1];
            pL += m_iDX;  
            pAL += m_iDX;
        }

        int iDYR = iYR1 - iYR0 + 1;
        HL_PIXEL *pR    = Pixel(m_iImageDX-iSize+i, iYR0);
        BYTE *pAR       = GetMask(m_iImageDX-iSize+i, iYR0);
        for (iY = 0; iY < iDYR; iY++)
        {
#ifdef HL_PIXEL32
            *pR = pColors[i-1] | (pAlpha[i-1] << 24);
#else
            *pR = pColors[i-1];
#endif
            *pAR = pAlpha[i-1];
            pR += m_iDX;  
            pAR += m_iDX;
        }
    }

    int iRad16 = (iRad << 4);
    int iSize16 = (iSize << 4);
    for (int iX = 0; iX < (iSize+iRad); iX++)
    {
        int iInset = iSize+iRad;
        int iX_2 = iX * iX;

        for (int iY = 0; iY < (iSize+iRad); iY++)
        {
            int iD16 = iSqrt16(iX_2+iY*iY) - iRad16;
            if (iD16 < iSize16 && iD16 >= 0)
            {
                int iD = iD16 >> 4;
                int iAlpha = pAlpha[iD];
                iAlpha = pAlpha[iD];

                if (bUL)
                {
#ifdef HL_PIXEL32
                    *Pixel(iInset-iX-1, iInset-iY-1) = pColors[iD] | (iAlpha << 24);
#else
                    *Pixel(iInset-iX-1, iInset-iY-1) = pColors[iD];
#endif                    
                    *GetMask(iInset-iX-1, iInset-iY-1) = iAlpha;
                }
                if (bUR)
                {
#ifdef HL_PIXEL32
                    *Pixel(ImageDX()-iInset+iX, iInset-iY-1) = pColors[iD] | (iAlpha << 24);
#else
                    *Pixel(ImageDX()-iInset+iX, iInset-iY-1) = pColors[iD];
#endif
                    *GetMask(ImageDX()-iInset+iX, iInset-iY-1) = iAlpha;
                }
                if (bLL)
                {
#ifdef HL_PIXEL32
                    *Pixel(iInset-iX-1, DY()-iInset+iY) = pColors[iD] | (iAlpha << 24);
#else
                    *Pixel(iInset-iX-1, DY()-iInset+iY) = pColors[iD];
#endif
                    *GetMask(iInset-iX-1, DY()-iInset+iY) = iAlpha;
                }
                if (bLR)
                {
#ifdef HL_PIXEL32
                    *Pixel(ImageDX()-iInset+iX, DY()-iInset+iY) = pColors[iD] | (iAlpha << 24);
#else
                    *Pixel(ImageDX()-iInset+iX, DY()-iInset+iY) = pColors[iD];
#endif
                    *GetMask(ImageDX()-iInset+iX, DY()-iInset+iY) = iAlpha;
                }
            }
            else if (bFill && iD16 >= iSize16)
            {
                if (bUL)    *GetMask(iInset-iX-1, iInset-iY-1)              = 0;
                if (bUR)    *GetMask(ImageDX()-iInset+iX, iInset-iY-1)      = 0;
                if (bLL)    *GetMask(iInset-iX-1, DY()-iInset+iY)           = 0;
                if (bLR)    *GetMask(ImageDX()-iInset+iX, DY()-iInset+iY)   = 0;
            }
        }
    }

    if (iCornerMask != 0xFFFFFFFF)
    {
        for (int iX = 0; iX < iSize; iX++)
        {
            int iInset = iSize;
            for (int iY = 0; iY < iSize; iY++)
            {
                int iD = iSqrt(iX*iX+iY*iY);
                BYTE byAlpha = 0;
                if (iD < iSize)
                    byAlpha = pAlpha[iD];

                if (!bUL)
                    *GetMask(iInset-iX-1, iInset-iY-1) = byAlpha;
                if (!bUR)
                    *GetMask(ImageDX()-iInset+iX, iInset-iY-1) = byAlpha;
                if (!bLL)
                    *GetMask(iInset-iX-1, DY()-iInset+iY) = byAlpha;
                if (!bLR)
                    *GetMask(ImageDX()-iInset+iX, DY()-iInset+iY) = byAlpha;
            }
        }
    }

    delete [] pAlpha;
    delete [] pColors;
    END_TIMER(GDI_SHINE);
}
#ifdef HLDDRAW
/*============================================================================*/

    HL_DIRECTDRAWSURFACE        CAlphaDIB::CreateDDAlphaSurface()

/*============================================================================*/
{
    // Create a 32 bit surface

    #ifdef HL_X86C
        HL_DIRECTDRAWSURFACE pRet = g_pGDIServer->CreateAlphaSurface32(m_iImageDX, m_iDY);
    #else
        HL_DIRECTDRAWSURFACE pRet = g_pGDIServer->CreateAlphaSurface24(m_iImageDX, m_iDY);
    #endif

    if (pRet == NULL)
        return NULL;

    // Lock the output surface
    HL_SURFACEDESC surfaceDesc;
    memset(&surfaceDesc, 0, sizeof(HL_SURFACEDESC));
    surfaceDesc.dwSize      = sizeof(HL_SURFACEDESC);
    surfaceDesc.dwFlags = DDSD_HEIGHT|DDSD_WIDTH|DDSD_LPSURFACE;

    if (pRet->IsLost())
        pRet->Restore();    

#ifdef HL_WIN32
        HRESULT hr = pRet->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
#else
    #ifdef HL_DDRAWBASIC
        HRESULT hr = pRet->Lock(NULL, &surfaceDesc, DDLOCK_WAITNOTBUSY|DDLOCK_WRITEONLY|DDLOCK_DISCARD, NULL);
    #else
        HRESULT hr = pRet->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
    #endif
#endif

    if (hr != S_OK)
    {
        TraceWrap(_T("Lock Failed: CreateDDAlpha\n"));
        pRet->Release();
        return NULL;
    }

    BYTE *pMem = (BYTE*)surfaceDesc.lpSurface;
    int iDXPitchOut = surfaceDesc.lPitch;
    
    for (int iY = 0; iY < m_iDY; iY++)
    {
        BYTE *pAIn = GetMask(0, iY);
        HL_PIXEL *pIn = Pixel(0, iY);

        BYTE *pRow = (BYTE*)(pMem + (iY * iDXPitchOut));

        #ifdef HL_X86C
            DWORD *pdwOut = (DWORD*)pRow;

            for (int iX = 0; iX < m_iDX; iX++)
            {
                // Alpha, easy
                HL_PIXEL wIn = *pIn;
                DWORD  dwOut  = 0;
                dwOut = (*pAIn << 24);

                DWORD wR = ((((wIn & 0xF800) >> 11) * 255 / 31) << 16);
                DWORD wG = ((((wIn & 0x07E0) >> 5) * 255 / 63) << 8);
                DWORD wB = (wIn & 0x001F) * 255 / 31;
                dwOut |= (wR|wG|wB);

                *pdwOut = dwOut;

                pAIn++;
                pIn++;
                pdwOut++;
            }
        #else
            BYTE *pOut1 = pRow;
            BYTE *pOut2 = pOut1+1;
            BYTE *pOut3 = pOut1+2;

            for (int iX = 0; iX < m_iDX; iX++)
            {
                // Alpha, easy
                HL_PIXEL wIn = *pIn;
                DWORD  dwOut  = 0;

                DWORD wR = ((wIn & 0xF800) >> 11) * 63 / 31;
                DWORD wG = ((wIn & 0x07E0) >> 5);
                DWORD wB = (wIn & 0x001F) * 63 / 31;
    
                BYTE by1 = (BYTE)(*pAIn & 0x0FC);   // Top 6 bits from A -> Top 6 bits of byte1
                by1 |= (wR >> 4);                   // Top 2 bits from R -> Lower 2 bits of byte1
                BYTE by2 = (BYTE)(wR << 4) & 0xF0;  
                by2 |= (wG >> 2);
                BYTE by3 = (BYTE)((wG << 6) & 0xC0);
                by3 |= wB;

                *pOut1 = by3;
                *pOut2 = by2;
                *pOut3 = by1;

                pAIn++;
                pIn++;

                pOut1 += 3;
                pOut2 += 3;
                pOut3 += 3;
            }
        #endif
    }


    pRet->Unlock(NULL);
    return pRet;
}
/*============================================================================*/

    HL_DIRECTDRAWSURFACE        CAlphaDIB::CreateDDAlphaTextSurface(
    COLORREF                    rgb )

/*============================================================================*/
{
    // Create a 32 bit surface

    #ifdef HL_X86C
        HL_DIRECTDRAWSURFACE pRet = g_pGDIServer->CreateAlphaSurface32(m_iImageDX, m_iDY);
    #else
        HL_DIRECTDRAWSURFACE pRet = g_pGDIServer->CreateAlphaSurface24(m_iImageDX, m_iDY);
    #endif

    if (pRet == NULL)
        return NULL;

    // Lock the output surface
    HL_SURFACEDESC surfaceDesc;
    memset(&surfaceDesc, 0, sizeof(HL_SURFACEDESC));
    surfaceDesc.dwSize      = sizeof(HL_SURFACEDESC);
    surfaceDesc.dwFlags = DDSD_HEIGHT|DDSD_WIDTH|DDSD_LPSURFACE;

    if (pRet->IsLost())
        pRet->Restore();    

#ifdef HL_WIN32
        HRESULT hr = pRet->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
#else
    #ifdef HL_DDRAWBASIC
        HRESULT hr = pRet->Lock(NULL, &surfaceDesc, DDLOCK_WAITNOTBUSY|DDLOCK_WRITEONLY|DDLOCK_DISCARD, NULL);
    #else
        HRESULT hr = pRet->Lock(NULL, &surfaceDesc, DDLOCK_WAIT|DDLOCK_SURFACEMEMORYPTR, NULL);
    #endif
#endif

    if (hr != S_OK)
    {
        TraceWrap(_T("Lock Failed: CreateDDAlphaText\n"));
        pRet->Release();
        return NULL;
    }

    BYTE *pMem = (BYTE*)surfaceDesc.lpSurface;
    int iDXPitchOut = surfaceDesc.lPitch;
    
    int iR = GetRValue(rgb);
    int iG = GetGValue(rgb);
    int iB = GetBValue(rgb);


    //  AAAAAARR RRRRGGGG GGBBBBBB

    BYTE by1 = (iR >> 6);
    BYTE by2 = (iR << 2) & 0xF0 | (iG >> 4);
    BYTE by3 = (iG << 4) & 0xC0 | (iB >> 2);

    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pIn = Pixel(0, iY);
        BYTE *pRow = (BYTE*)(pMem + (iY * iDXPitchOut));

        BYTE *pOut1 = pRow;
        BYTE *pOut2 = pOut1+1;
        BYTE *pOut3 = pOut1+2;

        for (int iX = 0; iX < m_iDX; iX++)
        {
            // Alpha, easy
            HL_PIXEL wIn = *pIn;

            BYTE by1This = by1 | ((wIn >> 3) & 0xFC);

            *pOut1 = by3;
            *pOut2 = by2;
            *pOut3 = by1This;

            pIn++;
            pOut1 += 3;
            pOut2 += 3;
            pOut3 += 3;
        }
    }


    pRet->Unlock(NULL);
    return pRet;
}
/*============================================================================*/

    void                        CAlphaDIB::InitAlphaSurface()

/*============================================================================*/
{
#ifdef HL_ARMV4IC
    if (m_pAlphaSurface != NULL)
    {
        m_pAlphaSurface->Release();
    }
    m_pAlphaSurface = CreateDDAlphaSurface();
    #ifdef VRAM_TRACKER
        g_pDibServer->RegisterVRAMDIB(this);
    #endif
#endif
}
/*============================================================================*/

    void                        CAlphaDIB::InitAlphaSurfaceText(
    COLORREF                    rgb )

/*============================================================================*/
{
#ifdef HL_ARMV4IC
    if (m_pAlphaSurface != NULL)
    {
        m_pAlphaSurface->Release();
    }
    m_pAlphaSurface = CreateDDAlphaTextSurface(rgb);
#endif
}
/*============================================================================*/

    void                        CAlphaDIB::ReleaseAlphaSurface()

/*============================================================================*/
{
    if (m_pAlphaSurface != NULL)
    {
        m_pAlphaSurface->Release();
        #ifdef VRAM_TRACKER
            g_pDibServer->UnRegisterVRAMDIB(this);
        #endif
    }
    m_pAlphaSurface = NULL;
}
#endif
/*============================================================================*/

    CBilinearScaler::CBilinearScaler()

/*============================================================================*/
{
    m_iDXSrc    = 0;
    m_iDYSrc    = 0;
    m_iDXTgt    = 0;
    m_iDYTgt    = 0;

    m_pdwSrcRow0        = NULL;
    m_pdwSrcRow1        = NULL;
    m_pdwSrcAlphaRow0   = NULL;
    m_pdwSrcAlphaRow1   = NULL;
    m_pdwSrcCol0        = NULL;
    m_pdwSrcCol1        = NULL;
    m_pdwSrcAlphaCol0   = NULL;
    m_pdwSrcAlphaCol1   = NULL;
    m_pClipRadTgt       = NULL;
}
/*============================================================================*/

    CBilinearScaler::~CBilinearScaler()

/*============================================================================*/
{
    FreeAll();
}
/*============================================================================*/

    void                        CBilinearScaler::Add(
    CHLDibDC                    *pSrcDC,
    CHLDibDC                    *pTgtDC,
    CHLRect                     rROISrc,
    int                         iClipRadTgt,
    COLORREF                    rgbBase )

/*============================================================================*/
{
    BOOL bInitVars = FALSE;
    if (pSrcDC->ImageDX() != m_iDXSrc || pSrcDC->DY() != m_iDYSrc)
        bInitVars = TRUE;
    if (pTgtDC->ImageDX() != m_iDXTgt || pTgtDC->DY() != m_iDYTgt)
        bInitVars = TRUE;

    if (iClipRadTgt != m_iClipRadTgt)
        bInitVars = TRUE;


    if (bInitVars)
    {
        InitScaleVars(pSrcDC->ImageDX(), pSrcDC->DY(), pTgtDC->ImageDX(), pTgtDC->DY(), iClipRadTgt);
    }

    if (pSrcDC->ImageDX() == pTgtDC->ImageDX() && pSrcDC->DY() == pTgtDC->DY())
    {
        AddOneToOne(pSrcDC, pTgtDC, rROISrc, iClipRadTgt, rgbBase);
        return;
    }

    HL_PIXEL wBase = RGBTOHLPIXEL(rgbBase);
    int iRBase = wBase & 0xF800;
    int iGBase = wBase & 0x07E0;
    int iBBase = wBase & 0x001F;

    int iX0 = rROISrc.XLeft()   * m_iDXTgt / m_iDXSrc;
    int iY0 = rROISrc.YTop()    * m_iDYTgt / m_iDYSrc;
    int iDX = rROISrc.DX()      * m_iDXTgt / m_iDXSrc;
    int iDY = rROISrc.DY()      * m_iDYTgt / m_iDYSrc;

    for (int iY = 0; iY < iDY; iY++)
    {
        int iYOut = iY0 + iY;
        int iXInset = 0;

        if (m_pClipRadTgt != NULL)
        {
            if (iYOut < m_iClipRadTgt)
            {
                iXInset = m_iClipRadTgt-m_pClipRadTgt->X(iYOut);
            }
            else if (iYOut > (m_iDYTgt-m_iClipRadTgt))
            {
                int iYToEdge = m_iDYTgt - iYOut;
                iXInset = m_iClipRadTgt-m_pClipRadTgt->X(iYToEdge);
            }
        }

        int iX0ThisRow = iX0;
        int iDXThisRow = iDX;    

        if (iX0ThisRow < iXInset)
        {
            int iShift = iXInset - iX0ThisRow;
            iDXThisRow = iDXThisRow - iShift;
            iX0ThisRow = iXInset;
        }

        iDXThisRow = __min(iDXThisRow, m_iDXTgt - iXInset - iX0ThisRow);
    
        DWORD iYSrc0 = m_pdwSrcRow0[iYOut];
        DWORD iYSrc1 = m_pdwSrcRow1[iYOut];
        DWORD iAlpha0 = m_pdwSrcAlphaRow0[iYOut];
        DWORD iAlpha1 = m_pdwSrcAlphaRow1[iYOut];

/*

        int iYSrc64 = (iYOut * iDYBase * 64) / iDYThis + 31;
        int iYSrc0   = iYSrc64 / 64;
        int iYSrc1   = iYSrc0 + 1;
        int iAlpha0  = (iYSrc1 * 64 - iYSrc64);
        int iAlpha1  = 64-iAlpha0;
*/
        if (iDXThisRow > 0)
        {
            HL_PIXEL *pOut = pTgtDC->Pixel(iX0ThisRow, iYOut);
            
            HL_PIXEL *pInRow0  = pSrcDC->Pixel(0, iYSrc0);
            HL_PIXEL *pInRow1  = pSrcDC->Pixel(0, iYSrc1);

            for (int iX = 0; iX < iDXThisRow; iX++)
            {
                DWORD dwOffset0 = m_pdwSrcCol0[iX+iX0ThisRow];
                DWORD dwOffset1 = m_pdwSrcCol1[iX+iX0ThisRow];
                DWORD iAlpha_X0 = m_pdwSrcAlphaCol0[iX+iX0ThisRow];

                DWORD wSrcUL = *(pInRow0+dwOffset0);
                DWORD wSrcLL = *(pInRow1+dwOffset0);
                DWORD wSrcUR = *(pInRow0+dwOffset1);
                DWORD wSrcLR = *(pInRow1+dwOffset1);

                DWORD wTgt = *pOut;

                int iAlphaUL = (iAlpha0 * iAlpha_X0) >> 6;
                int iAlphaUR = iAlpha0 - iAlphaUL;
                int iAlphaLL = (iAlpha1 * iAlpha_X0) >> 6;
                int iAlphaLR = iAlpha1 - iAlphaLL;

                DWORD wRSrcUL = (((wSrcUL & 0x0000F800) * iAlphaUL) >> 6);
                DWORD wGSrcUL = (((wSrcUL & 0x000007E0) * iAlphaUL) >> 6);
                DWORD wBSrcUL = (((wSrcUL & 0x0000001F) * iAlphaUL) >> 6);

                DWORD wRSrcLL = (((wSrcLL & 0x0000F800) * iAlphaLL) >> 6);
                DWORD wGSrcLL = (((wSrcLL & 0x000007E0) * iAlphaLL) >> 6);
                DWORD wBSrcLL = (((wSrcLL & 0x0000001F) * iAlphaLL) >> 6);

                DWORD wRSrcUR = (((wSrcUR & 0x0000F800) * iAlphaUR) >> 6);
                DWORD wGSrcUR = (((wSrcUR & 0x000007E0) * iAlphaUR) >> 6);
                DWORD wBSrcUR = (((wSrcUR & 0x0000001F) * iAlphaUR) >> 6);

                DWORD wRSrcLR = (((wSrcLR & 0x0000F800) * iAlphaLR) >> 6);
                DWORD wGSrcLR = (((wSrcLR & 0x000007E0) * iAlphaLR) >> 6);
                DWORD wBSrcLR = (((wSrcLR & 0x0000001F) * iAlphaLR) >> 6);

                DWORD wRDst = wTgt & 0x0000F800;
                DWORD wGDst = wTgt & 0x000007E0;
                DWORD wBDst = wTgt & 0x0000001F;

                int wR = __max(0, __min(wRSrcLL + wRSrcUL + wRSrcUR + wRSrcLR + wRDst - iRBase, 0x0000F800) & 0xF800);
                int wG = __max(0, __min(wGSrcLL + wGSrcUL + wGSrcUR + wGSrcLR + wGDst - iGBase, 0x000007E0) & 0x07E0);
                int wB = __max(0, __min(wBSrcLL + wBSrcUL + wBSrcUR + wBSrcLR + wBDst - iBBase, 0x0000001F));
                *pOut = (HL_PIXEL) (wR|wG|wB);
                pOut++;
            }
        }
    }
}
/*============================================================================*/

    void                        CBilinearScaler::AddOneToOne(
    CHLDibDC                    *pSrcDC,
    CHLDibDC                    *pTgtDC,
    CHLRect                     rROISrc,
    int                         iClipRadTgt,
    COLORREF                    rgbBase  )

/*============================================================================*/
{
    int iX0 = rROISrc.XLeft();
    int iY0 = rROISrc.YTop();
    int iDX = rROISrc.DX();
    int iDY = rROISrc.DY();

    HL_PIXEL wBase = RGBTOHLPIXEL(rgbBase);
    int iRBase = wBase & 0xF800;
    int iGBase = wBase & 0x07E0;
    int iBBase = wBase & 0x001F;

    for (int iY = 0; iY < iDY; iY++)
    {
        int iYOut = iY0 + iY;
        int iXInset = 0;

        if (m_pClipRadTgt != NULL)
        {
            if (iYOut < m_iClipRadTgt)
            {
                iXInset = m_iClipRadTgt-m_pClipRadTgt->X(iYOut);
            }
            else if (iYOut > (m_iDYTgt-m_iClipRadTgt))
            {
                int iYToEdge = m_iDYTgt - iYOut;
                iXInset = m_iClipRadTgt-m_pClipRadTgt->X(iYToEdge);
            }
        }

        int iX0ThisRow = iX0;
        int iDXThisRow = iDX;    

        if (iX0ThisRow < iXInset)
        {
            int iShift = iXInset - iX0ThisRow;
            iDXThisRow = iDXThisRow - iShift;
            iX0ThisRow = iXInset;
        }

        iDXThisRow = __min(iDXThisRow, m_iDXTgt - iXInset - iX0ThisRow);
    
        if (iDXThisRow > 0)
        {
            HL_PIXEL *pOut  = pTgtDC->Pixel(iX0ThisRow, iYOut);
            HL_PIXEL *pIn   = pSrcDC->Pixel(iX0ThisRow, iYOut);

            for (int iX = 0; iX < iDXThisRow; iX++)
            {
                DWORD wTgt = *pOut;
                DWORD wSrc = *pIn;
                int wRDst = wTgt & 0xF800;
                int wGDst = wTgt & 0x07E0;
                int wBDst = wTgt & 0x001F;
                int wRSrc = wSrc & 0xF800;
                int wGSrc = wSrc & 0x07E0;
                int wBSrc = wSrc & 0x001F;
                int iROut = __max(0, __min(0xF800, wRDst + wRSrc - iRBase));
                int iGOut = __max(0, __min(0x07E0, wGDst + wGSrc - iGBase));
                int iBOut = __max(0, __min(0x001F, wBDst + wBSrc - iBBase));

                *pOut = (HL_PIXEL) (iROut|iGOut|iBOut);
                pOut++;
                pIn++;
            }
        }
    }
}
/*============================================================================*/

    void                        CBilinearScaler::FreeAll()

/*============================================================================*/
{
    delete [] m_pdwSrcRow0;
    delete [] m_pdwSrcRow1;
    delete [] m_pdwSrcAlphaRow0;
    delete [] m_pdwSrcAlphaRow1;
    delete [] m_pdwSrcCol0;
    delete [] m_pdwSrcCol1;
    delete [] m_pdwSrcAlphaCol0;
    delete [] m_pdwSrcAlphaCol1;

    delete m_pClipRadTgt;

    m_pdwSrcRow0        = NULL;
    m_pdwSrcRow1        = NULL;
    m_pdwSrcAlphaRow0   = NULL;
    m_pdwSrcAlphaRow1   = NULL;
    m_pdwSrcCol0        = NULL;
    m_pdwSrcCol1        = NULL;
    m_pdwSrcAlphaCol0   = NULL;
    m_pdwSrcAlphaCol1   = NULL;

    m_pClipRadTgt       = NULL;
}
/*============================================================================*/

    void                        CBilinearScaler::InitScaleVars(
    int                         iDXSrc,
    int                         iDYSrc,
    int                         iDXTgt,
    int                         iDYTgt,
    int                         iRadClip )

/*============================================================================*/
{
    FreeAll();

    m_iDXSrc        = iDXSrc;
    m_iDYSrc        = iDYSrc;
    m_iDXTgt        = iDXTgt;
    m_iDYTgt        = iDYTgt;
    m_iClipRadTgt   = iRadClip;

    if (iRadClip > 0)
    {
        m_pClipRadTgt = new CPointList;
        CHLDibDC::CalcArcPoints(m_pClipRadTgt, iRadClip, TRUE, RADIUS_ON);
    }

    m_pdwSrcRow0        = new DWORD[iDYTgt];
    m_pdwSrcRow1        = new DWORD[iDYTgt];
    m_pdwSrcAlphaRow0   = new DWORD[iDYTgt];
    m_pdwSrcAlphaRow1   = new DWORD[iDYTgt];
    m_pdwSrcCol0        = new DWORD[iDXTgt];
    m_pdwSrcCol1        = new DWORD[iDXTgt];
    m_pdwSrcAlphaCol0   = new DWORD[iDXTgt];
    m_pdwSrcAlphaCol1   = new DWORD[iDXTgt];

    // Figure out the Y scale vars
    for (int iY = 0; iY < iDYTgt; iY++)
    {
        int iYSrc64 = (iY * iDYSrc * 64) / iDYTgt + 31;
        int iYSrc0   = iYSrc64 / 64;
        int iYSrc1   = iYSrc0 + 1;
        int iAlpha0  = (iYSrc1 * 64 - iYSrc64);
        int iAlpha1  = 64-iAlpha0;

        if (iYSrc1 >= iDYSrc)
        {
            iYSrc1 = iDYSrc-1;
            iAlpha0 = 64;
            iAlpha1 = 0;
        }

        LOGASSERT(iYSrc0 >= 0 && iYSrc0 < iDYSrc);
        LOGASSERT(iYSrc1 >= 0 && iYSrc1 < iDYSrc);

        m_pdwSrcRow0[iY] = iYSrc0;
        m_pdwSrcRow1[iY] = iYSrc1;
        m_pdwSrcAlphaRow0[iY] = iAlpha0;
        m_pdwSrcAlphaRow1[iY] = iAlpha1;
    }

    for (int iX = 0; iX < iDXTgt; iX++)
    {
        DWORD dwOffset64 = (iX * iDXSrc * 64) / iDXTgt + 31;
        DWORD dwOffset0 = dwOffset64 / 64;
        DWORD dwOffset1 = dwOffset0 + 1;
        int iAlpha_X0 = (dwOffset1 * 64 - dwOffset64);
        int iAlpha_X1 = 64-iAlpha_X0;

        if (dwOffset1 >= (DWORD)iDXSrc)
        {
            dwOffset1 = iDXSrc-1;
            iAlpha_X0 = 64;
            iAlpha_X1 = 0;
        }

        LOGASSERT(dwOffset0 >= 0 && dwOffset0 < (DWORD)iDXSrc);
        LOGASSERT(dwOffset1 >= 0 && dwOffset1 < (DWORD)iDXSrc);

        m_pdwSrcCol0[iX] = dwOffset0;
        m_pdwSrcCol1[iX] = dwOffset1;
        m_pdwSrcAlphaCol0[iX] = iAlpha_X0;
        m_pdwSrcAlphaCol1[iX] = iAlpha_X1;
    }
}

