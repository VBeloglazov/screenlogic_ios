/*
--------------------------------------------------------------------------------

    SPEED.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "helpers.h"

#include "speed.h"

    //
    // Global LUT for converting from RGB to 16 bit (5/6/5) color.
    //
    // General notes:
    //  - For a normal 3 bytes per pixel DIB, bytes are stored in memory: (B)(G)(R).
    //
    //  - For a 2 bytes per pixel DIB, a WORD is stored in memory with the low byte
    //    first and the high byte second (low byte)(high byte).
    //
    //    For normal DIB sections, the WORD is packed with 5 bits per pixel: red is
    //    high order, blue low order. The most significant bit is ignored.
    //      ie:
    //          Red:    bits 14,13,12,11,10     0x0111110000000000  = 0x7C00
    //          Green:  bits 9,8,7,6,5          0x0000001111100000  = 0x03E0
    //          Blue:   bits 4,3,2,1,0          0x0000000000011111  = 0x001F
    //
    //    For the CE devices that we have seen so far, they all use 5/6/5 instead of
    //    the normal 5/5/5.
    //      ie:
    //          Red:    bits 15,14,13,12,11     0x1111100000000000 = 0xF800
    //          Green:  bits 10,9,8,7,6,5       0x0000011111100000 = 0x07E0
    //          Blue:   bits 4,3,2,1,0          0x0000000000011111 = 0x001F
    //
    BYTE                        *g_SQRT = NULL;
    WORD                        *g_SQRT16 = NULL;
    BYTE                        g_SIN100[100];

#ifdef IPHONE
    CSpeedServer                *g_pSpeedServer = NULL;
#endif

/*============================================================================*/

    CSpeedServer::CSpeedServer()

/*============================================================================*/
{
    g_SQRT = new BYTE[CACHE_SQRT];
    g_SQRT16 = new WORD[CACHE_SQRT];
    int i = 0;
    for (i = 0; i < CACHE_SQRT; i++)
    {
        int iR = i;
        int iS = 0;
        int iT = 0x40000000;
        do
        {
            int iTemp = iS + iT;
            iS = iS >> 1;
            if (iR >= iTemp)
            {
                iR -= iTemp;
                iS += iT;
            }
            iT = iT >> 2;
        } while (iT != 0);

        g_SQRT[i] = iS;
    }

    for (i = 0; i < CACHE_SQRT; i++)
    {
        int iR = i * 256;
        int iS = 0;
        int iT = 0x40000000;
        do
        {
            int iTemp = iS + iT;
            iS = iS >> 1;
            if (iR >= iTemp)
            {
                iR -= iTemp;
                iS += iT;
            }
            iT = iT >> 2;
        } while (iT != 0);

        g_SQRT16[i] = iS;
    }

    for (i = 0; i < 100; i++)
    {
        float fTheta = (float) i * 3.14159f / 50.f;
        float fSin = (float)sin(fTheta) + 1.f;
        g_SIN100[i] = (BYTE) (fSin * 50.f);
    }

#ifndef IPHONE
    QueryPerformanceFrequency(&m_liClockHz);
#endif
}
/*============================================================================*/

    CSpeedServer::~CSpeedServer()

/*============================================================================*/
{
    delete [] g_SQRT;
    delete [] g_SQRT16;
}
#ifndef IPHONE
/*============================================================================*/

    void                        CSpeedServer::StartTimer()

/*============================================================================*/
{
    QueryPerformanceCounter(&m_liClockStart);
}
/*============================================================================*/

    DWORD                       CSpeedServer::ElapsedMicroSeconds()    

/*============================================================================*/
{
    LARGE_INTEGER liNow;
    QueryPerformanceCounter(&liNow);
    DWORD dwDiv = (DWORD)m_liClockHz.LowPart / 1000;
    DWORD dwDiff = (DWORD)liNow.LowPart - (DWORD)m_liClockStart.LowPart;
    dwDiff = dwDiff / dwDiv * 1000;
    return dwDiff;    
}
/*============================================================================*/

    DWORD                       CSpeedServer::ElapsedMilliSeconds()

/*============================================================================*/
{
    LARGE_INTEGER liNow;
    QueryPerformanceCounter(&liNow);
    DWORD dwDiv = (DWORD)m_liClockHz.LowPart / 1000;
    DWORD dwDiff = (DWORD)liNow.LowPart - (DWORD)m_liClockStart.LowPart;
    dwDiff = dwDiff / dwDiv;
    return dwDiff;    
}
#endif