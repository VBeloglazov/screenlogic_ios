//
//  SysModeButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "SysModeButton.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"

@implementation CSysModeButton

#define CMD_GOSYSMODE 1000

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Mode:(int)iMode

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame IconFormat:iIconFormat Style:iStyle Text:pText TextSize:iTextSize TextColor:RGBText Color:RGBFace Icon:NULL])
    {
        m_iMode = iMode;
        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_MODE_GETMODEQ] autorelease];
        [pQ SendMessageWithMyID:self];
        [self SetCommandID:CMD_GOSYSMODE Responder:self];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_MODE_CHANGED:
    case HLM_MODE_GETMODEA:
        {
            [pMSG ResetRead];
            int iMode = [pMSG GetInt];
            [self SetChecked:(iMode == m_iMode)];
        }
        break;
    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_GOSYSMODE:
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_MODE_SETMODEQ];
            [pQ autorelease];
            [pQ PutInt:m_iMode];
            [pQ SendMessage];
        }
    }
}


@end
