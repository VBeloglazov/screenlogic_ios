/*
--------------------------------------------------------------------------------

    DIBDC.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

Use this to manipulate a DIB section: see MEMDC too.
--------------------------------------------------------------------------------
*/
#ifndef DIBDC_H
#define DIBDC_H

#include "hldc.h"
#include "shadepoly.h"

#ifdef HLDDRAW
    #include "ddraw.h"
#endif

class CDitherMap;
class CShadeDIB;

    //
    // Macro to add pointer checking in debug builds.
    //
    #ifdef _DEBUG
        #define CHECK_DIB_POINTER(pDC, pWORD) pDC->CheckPointer(pWORD)
    #else
        #define CHECK_DIB_POINTER(pDC, pWORD)
    #endif

    #ifndef HL_PIXEL
        #ifdef HL_PIXEL32
            #define     HL_PIXEL    DWORD
        #else
            #define     HL_PIXEL    WORD
        #endif
    #endif

    #if defined(HL_WIN32) | defined(HL_X86A) | defined(HL_X86B)
        #define HL_MMX
    #endif

    #ifdef HL_ARMV4IC
        #ifndef THUMB
//            #define HL_MMINTRIN
        #endif
    #endif

    class CHLMSG;
    class CPointList;
    class CAlphaDIB;
    class CEdgeList;
    class CRectList;
    class CColorMap;

    #define Bit(x)              (1<<x)

    #define ALPHA_BITS          4
    #define MAX_ALPHA_PREMULT   16
    #define ALPHA_BINARY                0x00000001
    #define ALPHA_PREMULT               0x00000002
    #define RENDER_OVER                 0x00000001
    #define RENDER_UNDER                0x00000002
    #define BLT_FLAG_SYSRAMTOVRAM       0x00000001

    enum HIGHLIGHT_TYPE
    {
        HIGHLIGHT_UPPER,
        HIGHLIGHT_UPPER_LOWER
    };

    enum DIBDC_MEMORY_TYPE
    {
        MEMORY_SYSTEM,
        MEMORY_SYSTEM_DISCARDABLE,
        MEMORY_DDRAW_VRAM
    };

    typedef void (BlendFunc)( HL_PIXEL *pTgt, HL_PIXEL *pSrc, int iN);

/*============================================================================*/

    class CHLDibDC :            public CHLDC

/*============================================================================*/
{
protected:
    static int                  g_iDCCount;

    int                         m_iImageDX;
    int                         m_iDX;
    int                         m_iDY;

    HL_PIXEL                    *m_pBits;
    BYTE                        *m_pMask;

    int                         m_iXOrgAbs;
    int                         m_iYOrgAbs;

    CHLDibDC                    *m_pParentDIB;
    int                         m_iUserData;
    int                         m_iUserData2;

    #ifdef DEBUG
        BOOL                    m_bHDC;
    #endif

    HBITMAP                     m_hCurBMP;
    HBITMAP                     m_hOldBMP;

    CRectList                   *m_pUpdateRectList;

#ifdef IPHONE
    int                         m_iAlphaFlags;
#endif

public:
    //
    // Default to NODDRAW here, gotta be careful when accessing the frame buffer here
    //
    CHLDibDC(
    int                         iDX,
    int                         iDY,
    int                         iXOrgAbs,
    int                         iYOrbAbs,
    DIBDC_MEMORY_TYPE           Type        = MEMORY_SYSTEM,
    int                         iAttribs    = 0 );

    CHLDibDC(
    int                         iDX,
    int                         iDY );

    CHLDibDC(
    CHLDibDC                    *pParentDIB,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         *piDXOrg,
    int                         *piDYOrg  );

    CHLDibDC(
    CHLDibDC                    *pParentDIB,
    CHLRect                     rParent,
    int                         *piDXOrg,
    int                         *piDYOrg  );

#ifdef IPHONE
    inline int                  AlphaFlags()            { return m_iAlphaFlags;         }
    inline void                 AlphaFlags(int iNew)    { m_iAlphaFlags = iNew;         }
#endif

    inline CHLDibDC             *ParentDIB()    { return m_pParentDIB;          }
    inline int                  ParentX()       { return m_iXLeft;              }
    inline int                  ParentY()       { return m_iYTop;               }

    inline int                  UserData()                  { return m_iUserData;       }
    inline void                 UserData(int iNew)          { m_iUserData = iNew;       }
    inline int                  UserData2()                 { return m_iUserData2;      }
    inline void                 UserData2(int iNew)         { m_iUserData2 = iNew;      }

    virtual void                DDConvertToVRAM();
    virtual void                DDConvertToSysRAM();

    inline void                 SCALEUP(HL_PIXEL *pIn)
    {
        HL_PIXEL wIn = *pIn;
        DWORD dwR = __min(0x0000F800, (wIn & 0x0000F800)+0x000052AA) & 0x0000F800;
        DWORD dwG = __min(0x000007E0, (wIn & 0x000007E0)+0x000002A0) & 0x000007E0;
        DWORD dwB = __min(0x0000001F, (wIn & 0x0000001F)+0x0000000A) & 0x0000001F;
        *pIn = (HL_PIXEL)(dwR|dwG|dwB);
    }
    inline void                 SCALEDN(HL_PIXEL *pIn)
    {
        HL_PIXEL wIn = *pIn;
        DWORD dwR = (((wIn & 0x0000F800)>>1) & 0x0000F800);
        DWORD dwG = (((wIn & 0x000007E0)>>1) & 0x000007E0);
        DWORD dwB = (((wIn & 0x0000001F)>>1) & 0x0000001F);
        *pIn = (HL_PIXEL)(dwR|dwG|dwB);
    }

    // Note non-virtual destructor: base class destructor also not virtual.
    ~CHLDibDC();

#ifdef HLDDRAW
    virtual HL_DIRECTDRAWSURFACE    GetSurface();
#endif

    DIBDC_MEMORY_TYPE           MemoryType();

    CHLDibDC                    *CreateScaledCopy(
    int                         iDX,    
    int                         iDY,
    int                         iAttribs = 0,
    BOOL                        bDDrawOK = FALSE);

    CHLDibDC                    *CreateBicubicScale(
    int                         iDXTgt,
    int                         iDYTgt,
    int                         iAttribs );

    HL_PIXEL                    GetBicubicRGB(
    int                         iXTgt,
    int                         iYTgt,
    int                         iDXTgt,
    int                         iDYTgt );

    virtual CHLRect             GetAlphaRect();

    virtual CHLDibDC            *CreateCopy(int iAttribs = -1);
    virtual CHLDibDC            *CreateCroppedCopy(CHLRect rSrc);
    virtual CHLDibDC            *CreateFlip(BOOL bFlipHz, BOOL bFlipVt);

    virtual CHLDibDC            *CreateScaledInstance(
    SIZE                        szNew,
    BOOL                        bSmooth     = FALSE, 
    int                         iAttribs    = 0);

    virtual CHLDibDC            *CreateScaleDIV2();

#ifndef IPHONE
    virtual void                SetOrigin(
    CHLWindow                   *pWnd );
#endif

    virtual void                SetOrigin(
    int                         iXLeft,
    int                         iYTop );

    virtual POINT               GetOriginAbs();

    virtual CAlphaDIB           *AlphaDIB();

    inline POINT                AbsOrigin()             { POINT pt; pt.x = m_iXOrgAbs; pt.y = m_iYOrgAbs; return pt;    }

    inline void                 AbsOrigin(
    int                         iX,
    int                         iY )                    { m_iXOrgAbs = iX; m_iYOrgAbs = iY; }

    //
    // The width of the bitmap in pixels can be used as the padded width when
    // incrementing a WORD pointer: m_iDX is always properly DWORD aligned.
    //
    int                         DXPitch();
    int                         DX();
    int                         DY();
    int                         ImageDX()           { return m_iImageDX;    }
    void                        ImageDX(int iNew)   { m_iImageDX = iNew;    }

    HL_PIXEL                    *Bits();
    void                        ReleaseBits();

    HL_PIXEL                    *Pixel(
    int                         iX,
    int                         iY );

#ifndef IPHONE
    virtual HDC                 GetHDC();
    inline BOOL                 HasHDC()    { return (m_hDC != NULL);    }
    
    void                        ReleaseGDI();
#endif

    virtual void                DrawLine(
    COLORREF                    rgb,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iPenSize = 1 );

    virtual void                DrawAALine(
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iPenSize = 1 );

    virtual void                DrawAlphaLine(
    COLORREF                    rgb,
    int                         iAlpha,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 );

    void                        CommitAALines(
    COLORREF                    rgb,
    int                         iAlpha );

    void                        FlipRectY(
    CHLRect                     rROI );

    virtual void                DrawRoundRect(
    COLORREF                    rgb,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad );

    virtual void                FillRectList(
    COLORREF                    rgbFill,
    CRectList                   *pRectList );

    virtual void                FillRect(
    COLORREF                    rgbFill,
    CHLRect                     rect );

    virtual void                FillRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY );

    virtual void                FillRoundRect(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iGeoType = RADIUS_ON );

    virtual void                FillRoundRectExt(
    COLORREF                    rgbFill,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iGeoType = RADIUS_ON );

    virtual void                BltFromGradient(
    CGradient2D                 *pGradient,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iAlpha = 100 );

    virtual void                BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest );

    virtual void                BltFromDibDCAlpha(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iAlpha );

    virtual void                BltFromDibDC(
    CHLDibDC                    *pDibDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    CEdgeList                   *pEdgeList );

    void                        BltFromDibDC(
    CHLDibDC                    *pSrc,
    CHLRect                     rSrc,
    CHLRect                     rDst );

    virtual void                BltFromAlphaDIB(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

#ifdef HLDDRAW
#ifdef HL_ARMV4IC
    virtual void                DDBltFromAlphaDIB(
    HL_DIRECTDRAWSURFACE        pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );
#endif
#endif

    virtual void                BltFromAlphaDIBNoEncoding(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY  );

    virtual void                BltFromAlphaDIBPreMultValues(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

#ifdef HL_MMX
    virtual void                BltFromAlphaDIBPreMultValuesMMX(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );
#endif

#ifdef HL_MMINTRIN
    virtual void                BltFromAlphaDIBPreMultValuesMMINTRIN(
    CAlphaDIB                   *pAlpha,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );
#endif

    virtual void                BltFromDibDC(
    int                         iXDest,
    int                         iYDest,
    CHLDibDC                    *pDibDC,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iFlags = 0 );

    virtual void                BltTransparentToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

    virtual void                BltInvertedToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY  );

    virtual void                SetColor(
    COLORREF                    rgbNew );

    virtual CHLDibDC            *DibDC();    
    
    virtual void                SetQuickFont(
    int                         iHeight );

    virtual void                DrawString(
    CHLString                   sText,
    CHLRect                     rect,
    int                         iHLFormat );

    virtual void                DrawStringAlpha(
    CHLString                   sText,
    CHLRect                     rect,
    int                         iHLFormat,
    int                         iAlpha );

    void                        DimRect(
    CHLRect                     rDim,
    int                         iShift = 1 );

    void                        DimEdgeList(
    CEdgeList                   *pList,
    int                         iShift = 1 );

    void                        Replace(
    HL_PIXEL                    wOld,
    HL_PIXEL                    wNew );

    void                        Replace(
    CHLRect                     rROI, 
    HL_PIXEL                    wOld,
    HL_PIXEL                    wNew );

    void                        Blur(CHLRect rROI, int iNPass);
    void                        Blur(HL_PIXEL *pTransparent);
    void                        BlurMask();
    void                        BlurEdges(CHLDibDC *pMaskDC);

    void                        FillRectAlpha(
    CHLRect                     rRect,
    HL_PIXEL                    wFill,
    int                         iAlpha );

    virtual void                FillEdgeList(
    COLORREF                    rgb,
    int                         iX0,
    int                         iY0,
    CEdgeList                   *pEdges );

    void                        FillEdgeListAlpha(
    CEdgeList                   *pEdges,
    HL_PIXEL                    wFill,
    int                         iAlphaTop,
    int                         iAlphaBtm );

    void                        FillRectAlphaDX(
    CHLRect                     rRect,
    HL_PIXEL                    wFill,
    int                         iAlpha0,
    int                         iAlpha1 );

    void                        FillRectAlphaDY(
    CHLRect                     rRect,
    HL_PIXEL                    wFill,
    int                         iAlpha0,
    int                         iAlpha1,
    int                         iRUpper = 0,
    int                         iRLower = 0 );

    void                        FillRoundRectAlpha(
    CHLRect                     rRect,
    COLORREF                    wFillT,
    COLORREF                    wFillB,
    COLORREF                    wFillC,
    int                         iAlpha,
    int                         iAlphaC,
    int                         iRad );

    void                        GradientFillCurve(
    CHLRect                     rRect,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iDir );

    void                        GradientFill(
    CHLRect                     rRect,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iDir,
    CColorMap                   *pMap = NULL);

    void                        BlendBltPCT(
    CHLDibDC                    *pSrcDib,
    int                         iXDst,
    int                         iYDst,
    int                         iPCTSrc );

    void                        BlendBltAlphaDY(
    CHLDibDC                    *pSrcDib,
    CHLRect                     rDst,
    CHLRect                     rSrc,
    int                         iAlpha0,
    int                         iAlpha1 );

    void                        TrapBlt(
    CHLDibDC                    *pSrc,
    CHLRect                     rROISrc,
    int                         iXArc,
    int                         iYArc );

    void                        MakeGrayscale();
    void                        ScaleToMonochrome(COLORREF rgb);
        
    void                        SubtractLevel(
    int                         iLevel );

    void                        Add(
    CHLDibDC                    *pSrcDib,
    int                         iSrcPCT     = 100 );

    void                        Add(
    int                         iXDest,
    int                         iYDest,
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    BOOL                        bClamp,
    int                         iSrcPCT     = 100 );

    virtual void                AddRGB(
    int                         iRAdd,
    int                         iGAdd,
    int                         iBAdd,
    CEdgeList                   *pEdges = NULL );

    virtual void                AddRGB(
    int                         byRAdd,
    int                         byGAdd,
    int                         byBAdd, 
    CHLRect                     rROI );

    virtual void                AddRGB(
    BYTE                        byRAdd,
    BYTE                        byGAdd,
    BYTE                        byBAdd,
    CHLRect                     rROI,
    int                         iRad );

    void                        SetRGB(
    COLORREF                    rgb,
    int                         iX,
    int                         iY,
    CEdgeList                   *pEdges );

    virtual void                BevelEdges();

    virtual void                BlendEdgeList(
    int                         iX0,
    int                         iY0,
    COLORREF                    rgb,
    int                         iAlphaPCT,
    CEdgeList                   *pEdges );

    void                        BlendBltPCTEdgeList(
    CHLDibDC                    *pSrcDib,
    int                         iXDst,
    int                         iYDst,
    CEdgeList                   *pEdgeList,
    int                         iPCTSrc,
    int                         iFlags );

    void                        BlendBltPCT(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iPCTSrc );

    void                        BlendBltPCT(
    CHLDibDC                    *pSrcDib1,
    CHLDibDC                    *pSrcDib2,
    int                         iXSrc1,
    int                         iYSrc1,
    int                         iXSrc2,
    int                         iYSrc2,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iPCTSrc1 );

#ifdef HL_MMX
    void                        BlendBltMMX(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iPCTSrc );
#endif

#ifdef HL_MMINTRIN
    void                        BlendBltMMINTRIN(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iPCTSrc );
#endif

    void                        BlendBltAdditive(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    CEdgeList                   *pEdgeList );

    void                        BlendBltAdditive(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY );

    void                        BlendBltAdditiveAbs(
    CHLDibDC                    *pSrcDib,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    COLORREF                    rgbBase = 0x0000 );

    void                        ChangeColor(
    COLORREF                    rgbFrom,
    COLORREF                    rgbTo );

    void                        SetPointer(
    void                        *pOld,
    void                        *pNew );

    virtual BYTE                *GetMask();
    virtual BYTE                *GetMask(int iX, int iY);

    void                        InitMask(
    BYTE                        byVal );

    void                        SetMask(
    HL_PIXEL                    rgbSet,
    BYTE                        byVal  );

    void                        FillMaskHZ(
    BYTE                        bySet,
    int                         iX1,
    int                         iX2,
    int                         iY );

    void                        SetMaskNOT(
    HL_PIXEL                    rgbSet,
    BYTE                        byVal  );

    void                        DilateMask(
    BYTE                        bySet = 1);

    void                        EncodeRLEMask();

    void                        GenEdgeListByColor(
    CHLRect                     rROI,
    HL_PIXEL                    wTransparent,
    CEdgeList                   *pEdges );

    void                        RenderHighlight(
    int                         iX,
    int                         iY,
    BYTE                        *pAlphaData,
    int                         iDX,
    int                         iDY );

    void                        RenderButtonOverlay(
    int                         iROutside,
    int                         iCorners,
    int                         iEdgeSize,
    int                         iButtonFlags,
    int                         iButtonAttribs );

    void                        ApplyShineyGradient(
    int                         iROutside,
    int                         iCorners,
    int                         iEdgeSize,
    int                         iAlphaMax       = -1,
    int                         iButtonAttribs  = 0,
    int                         iAlphaRatio     = -1,
    int                         iRMinor         = -1,
    int                         iLowerAlpha     = -1,
    HIGHLIGHT_TYPE              Type            = HIGHLIGHT_UPPER_LOWER);

    void                        DrawAlphaShape(
    int                         iY0,
    int                         iDY,
    int                         iR0,
    int                         iR1,
    int                         iA0,
    int                         iA1,
    int                         iXInset,
    int                         iCorners        = 0xFFFFFFFF );

    virtual void                DrawAlphaRadius(
    int                         iY0,
    int                         iDY,
    int                         iRad, 
    int                         iATop,
    int                         iABtm,
    int                         iXInset,
    BOOL                        bInvertGeo,
    BOOL                        bLRad,
    BOOL                        bRRad );

    virtual void                DrawAlphaRadiusCompound(
    int                         iY0,
    int                         iDY0,
    int                         iXInset,
    int                         iROuter,
    int                         iRInner,
    int                         iATop,
    int                         iABtm );

    virtual void                DrawAlphaRect(
    int                         iY0,
    int                         iDY,
    int                         iATop,
    int                         iABtm,
    int                         iXInset );

    virtual void                Solidify(
    COLORREF                    rgb );

    static void                 CalcArcPoints(
    CPointList                  *pPointList,
    int                         iRad,
    BOOL                        bOmitRepeatX );

    static void                 CalcArcPoints(
    CPointList                  *pPointList,
    int                         iRadius,
    BOOL                        bOmitDupX,
    int                         iRadiusType );

    static void                 InsetArcPoints(
    CPointList                  *pPointList );

    static void                 ScaleFast(
    CHLRect                     rSrc,
    CHLRect                     rDst,
    CHLDibDC                    *pSrc, 
    CHLDibDC                    *pDst );

    static void                 ScaleFast(
    CHLRect                     rSrc,
    CHLRect                     rDst,
    CHLDibDC                    *pSrc, 
    CHLDibDC                    *pDst,
    COLORREF                    rgbOver,
    int                         iAlpha  );

    static void                 CorrectScaleRects(
    CHLDibDC                    *pSrc,
    CHLDibDC                    *pDst,
    CHLRect                     *prSrc,
    CHLRect                     *prDst );

    static void                 CorrectScaleRects(
    CHLDibDC                    *pSrc,
    int                         iDstDXI,
    int                         iDstDYI,
    CHLRect                     *prSrc,
    CHLRect                     *prDst );


    //
    // Helper function to detect pointer overruns.
    //
#ifdef _DEBUG
    BOOL                        HasTransparentBits();

    void                        CheckPointer(
    HL_PIXEL                    *pWORD );
#endif
    void                        Highlight(
    BYTE                        *pMap,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2,
    int                         iRTop,
    int                         iRBottom );

    void                        Fill(
    HL_PIXEL                    rgbSet);

    void                        FillHZ(
    HL_PIXEL                    rgbSet,
    int                         iX1,
    int                         iX2,
    int                         iY );

    void                        FillVT(
    HL_PIXEL                    rgbSet,
    int                         iX,
    int                         iY1,
    int                         iY2 );

    BOOL                        GetSafeDims(
    int                         *piXSrc,
    int                         *piYSrc,
    int                         *piDX,
    int                         *piDY,
    int                         *piXOther,
    int                         *piYOther,
    CHLDibDC                    *pOther );

    virtual BOOL                TestClip(
    CHLRect                     rIn );

    virtual CHLRect             LocalClipRect();

    static BOOL                 IsIntersected(
    CHLDibDC                    *pDIB,
    CHLRect                     rTest );

    static BOOL                 IsIntersected(
    CHLDibDC                    *pDIB,
    int                         iXL,
    int                         iYL,
    int                         iDX,
    int                         iDY );

    void                        ResetUpdateRects();

    void                        AddUpdateRect(
    CHLRect                     rect );    

    inline CRectList            *UpdateRects()          { return m_pUpdateRectList; }


protected:
    virtual void                SetActiveClip(
    CHLRect                     *pClip );

    POINT                       XIntersect(
    int                         iXI,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 );

    POINT                       YIntersect(
    int                         iYI,
    int                         iX1,
    int                         iY1,
    int                         iX2,
    int                         iY2 );

    void                        SetMaskAlpha(
    int                         iOffset,
    int                         iAlpha1500 );    

    void                        *HLAlloc(int iNBytes);
    void                        HLFree(void *pMem);
};
/*============================================================================*/

    class CAlphaDIB : public CHLDibDC

/*============================================================================*/
{
public:
    enum HL_PREMULT_MODE
    {
        PREMULT_NONE,
        PREMULT_VALUES, 
    };

protected:
    int                         m_iAlphaFlags;

#ifdef HLDDRAW
    HL_DIRECTDRAWSURFACE        m_pAlphaSurface;
#endif
    
public:
    CAlphaDIB(
    int                         iDX,
    int                         iDY,
    int                         iXScreen0,
    int                         iYScreen0,
    int                         iAttribs   = DIBATTRIB_FORCESYSMEM );

    virtual ~CAlphaDIB();

    inline int                  AlphaFlags()                { return m_iAlphaFlags;     }
    inline void                 AlphaFlags(int iNew)        { m_iAlphaFlags = iNew;     }

    virtual void                    DDConvertToVRAM();
    virtual void                    DDConvertToSysRAM();

#ifdef HLDDRAW
    virtual BOOL                    HasDDAlphaSurface();
    virtual HL_DIRECTDRAWSURFACE    GetDDAlphaSurface();
#endif

    inline BOOL                 IsBinaryAlpha()             { return (m_iAlphaFlags & ALPHA_BINARY);    }

    virtual void                BltTransparentToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );
    
    virtual void                BltInvertedToHLDC(
    CHLDC                       *pDC,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY  );

    void                        BltAlphaToHLDC(
    CHLDibDC                    *pDC ,
    int                         iXDest,
    int                         iYDest,
    int                         iDX,
    int                         iDY  );

    virtual void                DrawAlphaRadius(
    int                         iY0,
    int                         iDY,
    int                         iRad, 
    int                         iATop,
    int                         iABtm,
    int                         iXInset,
    BOOL                        bInvertGeo );

    virtual void                DrawAlphaRadiusCompound(
    int                         iY0,
    int                         iDY0,
    int                         iXInset,
    int                         iROuter,
    int                         iRInner,
    int                         iATop,
    int                         iABtm );

    virtual void                DrawAlphaRect(
    int                         iY0,
    int                         iDY,
    int                         iATop,
    int                         iABtm,
    int                         iXInset );

    virtual CHLRect             GetAlphaRect();

    virtual CHLDibDC            *CreateCopy(int iAttribs = -1);
    virtual CHLDibDC            *CreateCroppedCopy(CHLRect rSrc);
    virtual CHLDibDC            *CreateFlip(BOOL bFlipHz, BOOL bFlipVt);

    virtual CHLDibDC            *CreateScaledInstance(
    SIZE                        szNew,
    BOOL                        bSmooth = FALSE, 
    int                         iAttribs = 0 );

    virtual CHLDibDC            *CreateScaleDIV2();

    CAlphaDIB                   *CreateFastScaleCopy(
    int                         iDX,
    int                         iDY );

    CAlphaDIB                   *CreateScaleCopy(
    int                         iFactor );

    CAlphaDIB                   *CreateBicubicScale(
    int                         iDXTgt,
    int                         iDYTgt,
    int                         iAttribs );

    BYTE                        GetBicubicAlpha(
    int                         iXTgt,
    int                         iYTgt,
    int                         iDXTgt,
    int                         iDYTgt );

    virtual BYTE                *GetMask();
    virtual BYTE                *GetMask(int iX, int iY);

    void                        GenAlphaChannel(
    COLORREF                    rgbBG,
    CHLRect                     *pROI );

    void                        FillMaskRoundRectExt(
    BYTE                        byMask,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad );

    void                        FillRoundRectAA(
    COLORREF                    rgb,
    int                         iXLeft,
    int                         iYTop,
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iAlphaMax );

    void                        ScaleAlpha(
    int                         iPCT );

#ifdef HLOSD
    void                        ZeroOutsideRadius(
    int                         iRad );
#endif

    void                        CopyFrom(
    int                         iXDst,
    int                         iYDst,
    CAlphaDIB                   *pSrc,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY );

    virtual CAlphaDIB           *AlphaDIB();

    CAlphaDIB::HL_PREMULT_MODE  MakePreMult();

    virtual void                AddRGB(
    BYTE                        byRAdd,
    BYTE                        byGAdd,
    BYTE                        byBAdd,
    CEdgeList                   *pEdges = NULL );

    void                        CreateHalo(
    int                         iSize,
    int                         iRad        = 0,
    COLORREF                    rgbOut      = RGB(0,0,255),
    COLORREF                    rgbIn       = RGB(128,128,255),
    int                         iCorners    = 0xFFFFFFFF,
    int                         iAlphaMax   = 200,
    int                         iAlphaMin   = 0,
    BOOL                        bFill       = FALSE );

#ifdef HLDDRAW
    HL_DIRECTDRAWSURFACE        CreateDDAlphaSurface();
    HL_DIRECTDRAWSURFACE        CreateDDAlphaTextSurface(COLORREF rgb);
    void                        InitAlphaSurface();
    void                        InitAlphaSurfaceText(COLORREF rgbText);
    void                        ReleaseAlphaSurface();
#endif
};
/*============================================================================*/

    class                       CBilinearScaler

/*============================================================================*/
{
protected:
    int                         m_iDXSrc;
    int                         m_iDYSrc;                  
    int                         m_iDXTgt;
    int                         m_iDYTgt;                  
    int                         m_iClipRadTgt;

    CPointList                  *m_pClipRadTgt;
    
    DWORD                       *m_pdwSrcRow0;
    DWORD                       *m_pdwSrcRow1;
    DWORD                       *m_pdwSrcAlphaRow0;
    DWORD                       *m_pdwSrcAlphaRow1;
    DWORD                       *m_pdwSrcCol0;
    DWORD                       *m_pdwSrcCol1;
    DWORD                       *m_pdwSrcAlphaCol0;
    DWORD                       *m_pdwSrcAlphaCol1;

public:
    CBilinearScaler();
    virtual ~CBilinearScaler();

    void                        Add(
    CHLDibDC                    *pSrcDC,
    CHLDibDC                    *pTgtDC,
    CHLRect                     rROISrc,
    int                         iClipRadTgt,
    COLORREF                    rgbBase );

    void                        AddOneToOne(
    CHLDibDC                    *pSrcDC,
    CHLDibDC                    *pTgtDC,
    CHLRect                     rROISrc,
    int                         iClipRadTgt,
    COLORREF                    rgbBase );

protected:
    void                        FreeAll();

    void                        InitScaleVars(
    int                         iDXSrc,
    int                         iDYSrc,
    int                         iDXTgt,
    int                         iDYTgt,
    int                         iRadClip );

};

#endif

