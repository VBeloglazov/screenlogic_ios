//
//  PoolPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PoolSubTab.h"
#import "NSPoolConfig.h"
#import "SubSystemPage.h"
#import "PoolSubSystemPage.h"
#import "PoolHistoryMainPage.h"


@implementation CPoolSubTab

/*============================================================================*/

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2

/*============================================================================*/
{
    if (self = [super initWithName:pName ID:iID Data1:iData1 Data2:iData2])
    {
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pConfig release];
    [super dealloc];
}
/*============================================================================*/

-(CSubSystemPage*)CreateUserPage:(CGRect)rFrame MainTabPage:(CMainTabViewController*)pMain

/*============================================================================*/
{
    if (m_pConfig == NULL)
    {
        m_pConfig = [[CNSPoolConfig alloc] init];
    }

    CSubSystemPage *pNew = NULL;

    switch (m_iID)
    {
    case POOLPAGE_MAIN:
        pNew  = [[CPoolSubSystemPage alloc] initWithFrame:rFrame MainTabPage:pMain Config:m_pConfig SubTab:self];
        break;
    }

    return pNew;
}
@end
