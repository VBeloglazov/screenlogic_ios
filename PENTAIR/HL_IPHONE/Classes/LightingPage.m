//
//  LightingPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "LightingPage.h"
#import "keypad.h"
#import "CustomPage.h"
#import "Subtab.h"
#import "hlm.h"
#import "MainTabVC.h"

@implementation CLightingPage


/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:pSubTab]) 
    {
        [self Init:pSubTab];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)Init:(CSubTab*)pSubTab

/*============================================================================*/
{
    [super Init:pSubTab];

    CGRect rFrame = self.bounds;
    UIView *pView = NULL;
    if (m_pSubTab.m_iData1 & SYSCOMP_CUSTOMPAGE)
    {
        UIInterfaceOrientation ioNow = m_pMainPage.interfaceOrientation;
        m_pUserPage = [[CCustomPage alloc] initWithFrame:rFrame SysFam:FAMILY_LIGHTDEVCTLR ID:m_pSubTab.m_iID Orientation:ioNow];
        pView = m_pUserPage;
    }
    else
    {
        rFrame.origin.x += 10;
        rFrame.origin.y += 10;
        rFrame.size.width -= 20;
        rFrame.size.height -= 20;
        pView = [[CLightingKeypad alloc] initWithFrame:rFrame ID:m_pSubTab.m_iID];
    }

    [self addSubview:pView];
    [pView release];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [super SetVisible:bVisible];
    [CCustomPage SetVisible:m_pUserPage Visible:bVisible];
    if (bVisible)
    {
        BOOL bLockScroll = FALSE;
        [self LockHorizontalScroll:bLockScroll];
    }
}


@end
