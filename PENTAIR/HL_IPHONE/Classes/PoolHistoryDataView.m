//
//  PoolHistoryDataView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/15/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "PoolHistoryDataView.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "timeview.h"
#import "tstat.h"
#import "nsmsg.H"
#import "RTEView.h"
#import "TimeBar.h"
#import "NSPoolConfig.h"
#import "PoolHistoryView.h"
#import "LegendItem.h"
#import "CustomPage.h"

#define DY_RTE                          16


@implementation CPoolHistoryDataView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView Config:(CNSPoolConfig*)pConfig LastView:(CPoolHistoryDataView*)pLastView

/*============================================================================*/
{
    if (self = [super initWithFrame:frame TimeView:pTimeView]) 
    {
        m_pRTEViews = [[NSMutableArray alloc] init];
        m_pRTEViewLabels = [[NSMutableArray alloc] init];

        m_iTempMin = MIN_TEMP;
        m_iTempMax = MAX_TEMP;
        m_iXOrigin = frame.origin.x;
        int iXLeft = frame.origin.x;
        int iXRight = iXLeft + frame.size.width;
        NSDate *pStart = [[pTimeView GetTimeAtX:iXLeft] autorelease];
        NSDate *pEnd   = [[pTimeView GetTimeAtX:iXRight] autorelease];
        BOOL bNeedData = TRUE;

        m_pConfig = pConfig;

        // Initialization code
        if (pLastView != NULL)
        {
            m_iTempMin = [pLastView MinTemp];
            m_iTempMax = [pLastView MaxTemp];

            if (([pStart laterDate:[pLastView StartDataDate]] == pStart) && ([pEnd earlierDate:[pLastView EndDataDate]] == pEnd))
            {
                [m_pDateDataStart release];
                [m_pDateDataEnd release];
                m_pDateDataStart = [pLastView StartDataDate];
                m_pDateDataEnd = [pLastView EndDataDate];
                [m_pDateDataStart retain];
                [m_pDateDataEnd retain];
                bNeedData = FALSE;
                m_bDataOK = TRUE;
            }

            m_pTempSeries = [pLastView TempSeries];
            m_pRTESeries = [pLastView RTESeries];
            [self AddRTEViews];


            [m_pTempSeries retain];
            [m_pRTESeries retain];

            if (![pLastView DataOK])
            {
                m_pDateDataStart = [pTimeView GetTimeAtX:frame.origin.x];
                m_pDateDataEnd = [pTimeView GetTimeAtX:frame.origin.x + frame.size.width];
                bNeedData = TRUE;
            }
        }

        if (bNeedData)
        {
            [m_pTimeView SetWorking:YES];

            if (pLastView == NULL)
                [self PostQuery];
            else
                m_pQueryTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(PostQuery) userInfo:nil repeats:NO] retain];
        }
        else
        {
            [m_pTimeView SetWorking:NO];
            m_bCheckScale = TRUE;
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    m_pTimeView = NULL;

    for (int i = 0; i < [m_pRTEViewLabels count]; i++)
    {
        CLegendItem *pLabel = (CLegendItem*)[m_pRTEViewLabels objectAtIndex:i];
        [pLabel removeFromSuperview];
    }

    [m_pRTEViews release];
    [m_pRTEViewLabels release];
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    [m_pTempSeries release];
    [m_pRTESeries release];
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    if (self.bounds.size.width != m_iDXLastWidth)
    {
        m_bCheckScale = TRUE;
        m_iDXLastWidth = self.bounds.size.width;
    }

    CGContextRef pRef = UIGraphicsGetCurrentContext();

    printf("BeginDraw\n");

    [self DrawDayRects:pRef];

    UIColor *pTick = [UIColor colorWithRed:.25 green:.25 blue:0 alpha:1.0 ];
    [pTick set];
    CGPoint pPts[2];
    pPts[0].x = 0;
    pPts[1].x = self.frame.size.width;
    
    for (int iTick = m_iTempMin; iTick <= m_iTempMax; iTick += 10)
    {
        int iY = [self YLogical:iTick];
        pPts[0].y = iY;
        pPts[1].y = iY;
        CGContextStrokeLineSegments(pRef, pPts, 2);
    }

//    CGContextSetAllowsAntialiasing(pRef, FALSE);
    CGContextSetLineWidth(pRef, 2);

    NSDate *pDate0 = [[m_pTimeView GetTimeAtX:m_iXOrigin] autorelease];
    for (int i = 0; i < [m_pTempSeries count]; i++)
    {
        BOOL bDraw = TRUE;
        switch (i)
        {
        case 2:
        case 4:
            bDraw = FALSE;
            break;
        }

        if (bDraw)
        {
            CTempSeries *pSeries = (CTempSeries*)[m_pTempSeries objectAtIndex:i];
            UIColor *pColor = [pSeries Color];
            [pColor setStroke];
            NSMutableArray *pPoints = [pSeries PointList];

            CGContextBeginPath (pRef);

            BOOL bPoints = FALSE;
            BOOL bDone = FALSE;
            CDataPoint *pStartPoint = NULL;
            
            for (int iPoint = 0; iPoint < [pPoints count] && !bDone; iPoint++)
            {
                CDataPoint *pDataPoint = (CDataPoint*)[pPoints objectAtIndex:iPoint];
                NSDate *pPointDate = [pDataPoint Date];
                if ([pDate0 earlierDate:pPointDate] == pDate0)
                {

                    if (!bPoints)
                    {
                        if (pStartPoint == NULL)
                            pStartPoint = pDataPoint;
                        int iX0 = [m_pTimeView XLogical:[pStartPoint Date]] - m_iXOrigin;
                        int iY0 = [self YLogical:[pStartPoint Val]];
                        CGContextMoveToPoint(pRef, iX0, iY0);
                        bPoints = TRUE;
                    }

                    if (pStartPoint != pDataPoint)
                    {
                        int iX = [m_pTimeView XLogical:pPointDate] - m_iXOrigin;
                        int iY = [self YLogical:[pDataPoint Val]];
                        CGContextAddLineToPoint(pRef, iX, iY);

                        if (iX > self.frame.size.width)
                            bDone= TRUE;
                    }


                }
                else
                {
                    pStartPoint = pDataPoint;
                }
                
            }

            CGContextDrawPath(pRef, kCGPathStroke);
        }
    }

    if (m_bDataOK)
        [m_pTimeView SetWorking:NO];
        
    if (m_bCheckScale)
    {
        int iTempMin = -100;
        int iTempMax = -100;
        m_bCheckScale = FALSE;
        NSDate *pStart = [[m_pTimeView GetTimeAtX:0] autorelease];
        NSDate *pEnd   = [[m_pTimeView GetTimeAtX:m_pTimeView.frame.size.width] autorelease];
        for (int i = 0; i < [m_pTempSeries count]; i++)
        {
            BOOL bCheck = TRUE;
            switch (i)
            {
            case 2:
            case 4:
                bCheck = FALSE;
                break;
            }

            if (bCheck)
            {
                CTempSeries *pSeries = (CTempSeries*)[m_pTempSeries objectAtIndex:i];
                int iMin = [pSeries MinTempFrom:pStart To:pEnd];
                int iMax = [pSeries MaxTempFrom:pStart To:pEnd];
            
                if (iMin > HVAC_MIN_TEMP && iMin < HVAC_MAX_TEMP)
                {
                    if (iTempMin == -100)
                        iTempMin = iMin;
                    else
                        iTempMin = MIN(iMin, iTempMin);
                }

                if (iMax > HVAC_MIN_TEMP && iMax < HVAC_MAX_TEMP)
                {
                    if (iTempMax == -100)
                        iTempMax = iMax;
                    else
                        iTempMax = MAX(iMax, iTempMax);
                }
            }
        }

        printf("1: Min %d Max %d\n", iTempMin, iTempMax);

        if (iTempMin == -100)
            iTempMin = MIN_TEMP;
        if (iTempMax == -100)
            iTempMax = MAX_TEMP;
        
        iTempMax = [self RoundTempUp:iTempMax];
        iTempMin = [self RoundTempDown:iTempMin];
        iTempMin = [self AdjustMinTemp:iTempMin Max:iTempMax];
        [self SetScaleMin:iTempMin Max:iTempMax];
    }
    
    if (m_iFrameResizeNew != m_iFrameResizeOld)
    {
        [self ResizeFrameFrom:m_iFrameResizeOld To:m_iFrameResizeNew];
        m_iFrameResizeOld = m_iFrameResizeNew;
    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_POOL_HISTORYDATA)
        return;

    for (int i = 0; i < [m_pRTEViews count]; i++)
    {
        CRTEView *pRTEView = (CRTEView*)[m_pRTEViews objectAtIndex:i];
        [pRTEView removeFromSuperview];
    }
    [m_pRTEViews removeAllObjects];

    for (int i = 0; i < [m_pRTEViewLabels count]; i++)
    {
        CLegendItem *pLabel = (CLegendItem*)[m_pRTEViewLabels objectAtIndex:i];
        [pLabel removeFromSuperview];
    }
    [m_pRTEViewLabels removeAllObjects];

    [m_pTempSeries release];
    [m_pRTESeries release];

    m_pTempSeries = [[NSMutableArray alloc]init];
    m_pRTESeries = [[NSMutableArray alloc]init];
    
    //
    // Temperature data
    // 

    int iTempMin = -100;
    int iTempMax = -100;
    NSDate *pStart = [[m_pTimeView GetTimeAtX:0] autorelease];
    NSDate *pEnd   = [[m_pTimeView GetTimeAtX:m_pTimeView.frame.size.width] autorelease];

    for (int i = 0; i < 5; i++)
    {
        UIColor *pRGB = NULL;
        switch (i)
        {
        case 0: pRGB = [CPoolHistoryDataView ItemColor:RGB_OUTSIDE_TEMP];   break;  // Air
        case 1: pRGB = [CPoolHistoryDataView ItemColor:RGB_POOL_TEMP];      break;  // Pool
        case 2: pRGB = [CPoolHistoryDataView ItemColor:RGB_POOL_TEMP];      break;  // Pool SP
        case 3: pRGB = [CPoolHistoryDataView ItemColor:RGB_SPA_TEMP];       break;  // Spa
        case 4: pRGB = [CPoolHistoryDataView ItemColor:RGB_SPA_TEMP];       break;  // Spa SP
        }
    
        CTempSeries *pSeries = [[CTempSeries alloc] initWithColor:pRGB];
        [m_pTempSeries addObject:pSeries];
        [pSeries ReadFrom:pMSG];
        [pSeries release];

        BOOL bCheck = TRUE;
        switch (i)
        {
        case 2:
        case 4:
            bCheck = FALSE;
            break;
        }

        if (bCheck)
        {
            int iMin = [pSeries MinTempFrom:pStart To:pEnd];
            int iMax = [pSeries MaxTempFrom:pStart To:pEnd];
        
            if (iMin > HVAC_MIN_TEMP && iMin < HVAC_MAX_TEMP)
            {
                if (iTempMin == -100)
                    iTempMin = iMin;
                else
                    iTempMin = MIN(iMin, iTempMin);
            }
            if (iMax > HVAC_MIN_TEMP && iMax < HVAC_MAX_TEMP)
            {
                if (iTempMax == -100)
                    iTempMax = iMax;
                else
                    iTempMax = MAX(iMax, iTempMax);
            }
        }
    }

    if (iTempMin == -100)
        iTempMin = MIN_TEMP;
    if (iTempMax == -100)
        iTempMax = MAX_TEMP;

    printf("2: Min %d Max %d\n", iTempMin, iTempMax);
    iTempMax = [self RoundTempUp:iTempMax];
    iTempMin = [self RoundTempDown:iTempMin];
    iTempMin = [self AdjustMinTemp:iTempMin Max:iTempMax];

    //
    // RTE Data
    //
    for (int i = 0; i < 5; i++)
    {
        UIColor *pRGB = NULL;
        switch (i)
        {
        case 0: pRGB = [CPoolHistoryDataView ItemColor:RGB_POOL_TEMP];  break; // Pool
        case 1: pRGB = [CPoolHistoryDataView ItemColor:RGB_SPA_TEMP];   break; // Spa
        case 2: pRGB = [CPoolHistoryDataView ItemColor:RGB_SOLAR];      break; // Solar
        case 3: pRGB = [CPoolHistoryDataView ItemColor:RGB_HEAT];       break; // Heater
        case 4: pRGB = [CPoolHistoryDataView ItemColor:RGB_LIGHTS];     break; // Lights
        }
    
        CRTESeries *pSeries = [[CRTESeries alloc] initWithColor:pRGB];
        [m_pRTESeries addObject:pSeries];
        [pSeries ReadFrom:pMSG];
        [pSeries release];
    }

    [self AddRTEViews];
    [self UpdateTimeBar];

    printf("End Read MSG\n");
    m_bDataOK = TRUE;
    if (![self SetScaleMin:iTempMin Max:iTempMax])
        [self setNeedsDisplay];
}
/*============================================================================*/

-(int)YLogical:(int)iTemp

/*============================================================================*/
{
    int iDYThis = self.frame.size.height;
    int iDY = (iTemp - m_iTempMin) * iDYThis / (m_iTempMax-m_iTempMin);
    return iDYThis - iDY;
}
/*============================================================================*/

-(NSMutableArray*)TempSeries

/*============================================================================*/
{
    return m_pTempSeries;
}
/*============================================================================*/

-(NSMutableArray*)RTESeries

/*============================================================================*/
{
    return m_pRTESeries;
}
/*============================================================================*/

-(void)PostQuery

/*============================================================================*/
{
    int iXLeft = m_iXOrigin;
    int iXRight = iXLeft + self.frame.size.width;
    NSDate *pStart = [[m_pTimeView GetTimeAtX:iXLeft] autorelease];
    NSDate *pEnd   = [[m_pTimeView GetTimeAtX:iXRight] autorelease];
    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETHISTORYDATAQ] autorelease];
    [pQ PutInt:0];  // Controller Index
    [pQ PutTime:pStart];
    [pQ PutTime:pEnd];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
}
/*============================================================================*/

-(void)StopQuery

/*============================================================================*/
{
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
}
/*============================================================================*/

-(void)NotifyPosition

/*============================================================================*/
{
    int iDYRTE = DY_RTE;
    int iDYSuper = m_pTimeView.frame.size.height;
    int iYRTE = iDYSuper - [CMainView DY_TIMEBAR] - 1 - self.frame.origin.y - 2*iDYRTE;

    for (int i = [m_pRTEViews count]-1; i >= 0; i--)
    {
        CRTEView *pView = (CRTEView*)[m_pRTEViews objectAtIndex:i];
        CGRect rView = pView.frame;
        rView.origin.y = iYRTE;
        rView.origin.x = 0;
        rView.size.width = self.frame.size.width;
        [pView setFrame:rView];

        iYRTE -= iDYRTE;
    }

    iYRTE = iDYSuper - [CMainView DY_TIMEBAR] - 1 - 2*iDYRTE;
    for (int i = [m_pRTEViewLabels count]-1; i >= 0; i--)
    {
        CLegendItem *pView = (CLegendItem*)[m_pRTEViewLabels objectAtIndex:i];
        CGRect rView = pView.frame;
        rView.origin.y = iYRTE;
        [pView setFrame:rView];
        iYRTE -= iDYRTE;
    }

}
/*============================================================================*/

-(void)AddRTEViews

/*============================================================================*/    
{
    int iDYRTE = DY_RTE;
    int iDYSuper = m_pTimeView.frame.size.height;
    int iYRTE = iDYSuper - [CMainView DY_TIMEBAR] - 1 - 2*iDYRTE;

    NSDate *pStart = [[m_pTimeView GetTimeAtX:0] autorelease];
    NSDate *pEnd   = [[m_pTimeView GetTimeAtX:m_pTimeView.frame.size.width] autorelease];

    for (int i = 0; i < [m_pRTESeries count]; i++)
    {
        BOOL bOK = TRUE;
        if (i == 2 && ![m_pConfig EquipPresent:POOL_SOLARPRESENT|POOL_SOLARHEATPUMP])
            bOK = FALSE;
            
        if (bOK)
        {
            CGRect rView = CGRectMake(0, iYRTE, self.frame.size.width, iDYRTE);
            iYRTE -= iDYRTE;
            CRTESeries *pSeries = [m_pRTESeries objectAtIndex:i];
            CRTEView *pView = [[CRTEView alloc] initWithFrame:rView XOrigin:m_iXOrigin Series:pSeries TimeView:m_pTimeView];
            [self addSubview:pView];
            [m_pRTEViews addObject:pView];
            [pView release];

            int iType = -1;
            NSString *pText = NULL;

            switch (i)
            {
            case 0: { iType = RGB_POOL_TEMP;    pText = [m_pConfig PoolText];    }   break;
            case 1: { iType = RGB_SPA_TEMP;     pText = [m_pConfig SpaText];     }   break;
            case 2: { iType = RGB_SOLAR;        pText = @"Solar";   }   break;
            case 3: { iType = RGB_HEAT;         pText = @"Heat";    }   break;
            case 4: { iType = RGB_LIGHTS;       pText = @"Lights";  }   break;
            }

            int iPCT = [self CalcUsage:pSeries StartDate:pStart EndDate:pEnd];

            NSString *pTextAll = [NSString stringWithFormat:@"%s: %d%%", [pText UTF8String], iPCT];


            rView.origin.x = 65;
            rView.size.width = 65; 
            CLegendItem *pLegend = [[CLegendItem alloc] initWithFrame:rView Color:[CPoolHistoryDataView ItemColor:iType] Text:pTextAll];
            UIView *pSuper = m_pTimeView;
            [pSuper addSubview:pLegend];
            [m_pRTEViewLabels addObject:pLegend];
            [pLegend release];
        }
    }

    [self UpdateRTEViews];
}
/*============================================================================*/

-(void)UpdateRTEViews

/*============================================================================*/
{
    int iDYRTE = DY_RTE;
    int iDYSuper = m_pTimeView.frame.size.height;
    int iYRTE = iDYSuper - [CMainView DY_TIMEBAR] - 1 - 2*iDYRTE;

    for (int i = [m_pRTEViews count]-1; i >= 0 ; i--)
    {
        CRTEView *pView = (CRTEView*)[m_pRTEViews objectAtIndex:i];
        [pView SetXOrigin:m_iXOrigin];
        CGRect rView = pView.frame;
        rView.size.width = self.frame.size.width;
        rView.origin.y = iYRTE;
        [pView setFrame:rView];
        iYRTE -= DY_RTE;
    }

    iYRTE = iDYSuper - [CMainView DY_TIMEBAR] - 1 - 2*iDYRTE;
    for (int i = [m_pRTEViewLabels count]-1; i >= 0 ; i--)
    {
        CLegendItem *pView = (CLegendItem*)[m_pRTEViewLabels objectAtIndex:i];
        CGRect rView = pView.frame;
        rView.origin.x = 65;
        rView.origin.y = iYRTE;
        [pView setFrame:rView];
        iYRTE -= DY_RTE;
    }
}
/*============================================================================*/

-(void)UpdateTimeBar

/*============================================================================*/
{
    CTimeBar *pTimeBar = [m_pTimeView GetTimeBar];
    NSMutableArray *pLabels = [pTimeBar LabelList];
    for (int iLabel = 0; iLabel < [pLabels count]; iLabel++)
    {
        CTimeBarLabel *pLabel = (CTimeBarLabel*)[pLabels objectAtIndex:iLabel];
        [self UpdateLabel:pLabel];
    }
}
/*============================================================================*/

-(void)UpdateLabel:(CTimeBarLabel*)pLabel

/*============================================================================*/
{
}
/*============================================================================*/

-(void)AddInfoTextToView:(UIView*)pView Rect:(CGRect)rect PCT:(int)iPCT Color:(UIColor*)pColor

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:rect];
    [CCustomPage InitStandardLabel:pLabel Size:10];
    NSString *pText = [NSString stringWithFormat:@"%d%%", iPCT];
    [pLabel setText:pText];
    [pLabel setTextColor:pColor];
    [pView addSubview:pLabel];
    [pLabel release];
}
/*============================================================================*/

-(int)CalcUsage:(CRTESeries*)pSeries StartDate:(NSDate*)pStartDate EndDate:(NSDate*)pEndDate

/*============================================================================*/
{
    NSMutableArray *pPoints = [pSeries PointList];
    int iSum = 0;
    for (int i = 0; i < [pPoints count]; i++)
    {
        CRunTimeEvent *pRTE = (CRunTimeEvent*)[pPoints objectAtIndex:i];
        iSum += [pRTE OverlapWith:pStartDate End:pEndDate];
    }

    int iTotal = [pEndDate timeIntervalSinceDate:pStartDate];
    return iSum * 100 / iTotal;
}
/*============================================================================*/

-(int)MinTemp       { return m_iTempMin;    }
-(int)MaxTemp       { return m_iTempMax;    }

/*============================================================================*/

-(BOOL)SetScaleMin:(int)iMin Max:(int)iMax

/*============================================================================*/
{
    int iDiffNew = iMax-iMin;
    int iAdder = iDiffNew / 4;
    iAdder = iAdder / 5 * 5;
    iMin -= iAdder;
    iMax += iAdder / 2;

    int iScaleOld = m_iTempMax - m_iTempMin;
    int iScaleNew = iMax - iMin;
    double dScaleY = (double)iScaleOld / (double)iScaleNew;
    int iCenterF = (iMax + iMin ) / 2;
    int iCenterOld = [self YLogical:iCenterF];

    if (iMin == m_iTempMin && iMax == m_iTempMax)
        return FALSE;

    m_iTempMin = iMin;
    m_iTempMax = iMax;

    int iDYShift = (double)(self.frame.size.height / 2 - iCenterOld) * dScaleY;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    CGAffineTransform transform1 = CGAffineTransformMakeScale(1.0, dScaleY);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, iDYShift);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    self.transform = transform;
    UIView *pScaleView = [m_pTimeView GetScaleView];
    if (pScaleView != NULL)
      pScaleView.transform = transform;

    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];

    for (int i = 0; i < [m_pRTEViews count]; i++)
    {
        CRTEView *pView = (CRTEView*)[m_pRTEViews objectAtIndex:i];
        [pView setHidden:YES];
    }

    return TRUE;
}
/*============================================================================*/

-(BOOL)ResizeFrameFrom:(int)iFrom To:(int)iTo

/*============================================================================*/
{
    double dScaleY = (double)iTo / (double)iFrom;
    int iDYShift = (iTo - iFrom) / 2;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    CGAffineTransform transform1 = CGAffineTransformMakeScale(1.0, dScaleY);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, iDYShift);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    UIView *pScaleView = [m_pTimeView GetScaleView];
    if (pScaleView != NULL)
      pScaleView.transform = transform;

    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView commitAnimations];

    for (int i = 0; i < [m_pRTEViews count]; i++)
    {
        CRTEView *pView = (CRTEView*)[m_pRTEViews objectAtIndex:i];
        [pView setHidden:YES];
    }

    return TRUE;
}
/*============================================================================*/

-(void)SetCheckScale

/*============================================================================*/
{
    m_bCheckScale = TRUE;
}
/*============================================================================*/

-(void)SetFrameResizeOld:(int)iDYDataOld New:(int)iDYDataNew

/*============================================================================*/
{
    m_iFrameResizeOld = iDYDataOld;
    m_iFrameResizeNew = iDYDataNew;
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    CGAffineTransform transform1 = CGAffineTransformMakeScale(1.0, 1.0);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(0, 0);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    self.transform = transform;
    [self setNeedsDisplay];
    CPoolHistoryView *pPoolView = (CPoolHistoryView*)m_pTimeView;
    for (int i = 0; i < [m_pRTEViews count]; i++)
    {
        CRTEView *pView = (CRTEView*)[m_pRTEViews objectAtIndex:i];
        [pView setHidden:NO];
    }
    //DL - for unknown reason this pPoolView becomes invalid in iPhone
    //after several calls in here.
    //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [pPoolView InitScaleView];
    }
}
/*============================================================================*/

+(UIColor*)ItemColor:(int)iType

/*============================================================================*/
{
    switch (iType)
    {
    case RGB_POOL_TEMP:         return [UIColor colorWithRed:0 green:0 blue:1 alpha:1];
    case RGB_SPA_TEMP:          return [UIColor colorWithRed:0 green:.5 blue:1 alpha:1];
    case RGB_OUTSIDE_TEMP:      return [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    case RGB_SOLAR:             return [UIColor colorWithRed:.25 green:1 blue:.25 alpha:1];
    case RGB_HEAT:              return [UIColor colorWithRed:1 green:0 blue:0 alpha:1];
    case RGB_LIGHTS:            return [UIColor colorWithRed:1 green:0 blue:1 alpha:1];
    }

    return NULL;
}
/*============================================================================*/

+(UIColor*)FillColor:(int)iType

/*============================================================================*/
{
    switch (iType)
    {
    case RGB_POOL_TEMP:         return [UIColor colorWithRed:0 green:0 blue:.25 alpha:1];
    case RGB_SPA_TEMP:          return [UIColor colorWithRed:0 green:.2 blue:1 alpha:1];
    case RGB_OUTSIDE_TEMP:      return [UIColor colorWithRed:.5 green:.5 blue:.5 alpha:1];
    case RGB_SOLAR:             return [UIColor colorWithRed:.8 green:.5 blue:.1 alpha:1];
    case RGB_HEAT:              return [UIColor colorWithRed:.5 green:0 blue:0 alpha:1];
    case RGB_LIGHTS:            return [UIColor colorWithRed:.5 green:0 blue:.5 alpha:1];
    }

    return NULL;
}
/*============================================================================*/

-(int)AdjustMinTemp:(int)iMin Max:(int)iMax

/*============================================================================*/
{
    if (iMax - iMin < 15)
        iMin -= 10;
    else if (iMax - iMin < 20)
        iMin -= 5;
    return iMin;
}
@end
