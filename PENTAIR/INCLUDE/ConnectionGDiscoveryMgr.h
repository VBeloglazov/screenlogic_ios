/*
--------------------------------------------------------------------------------

    ConnectionGDiscoveryMgr.h

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once

#include "HLObList.h"

class CStatusSink;
class CSocketHL;

/*============================================================================*/

    class CConnectionGDiscoveryMgr

/*============================================================================*/
{
#ifndef PENTAIRMON
    static CConnectionGDiscoveryMgr*    m_pThis;
#endif

    BOOL                        m_bValid;

    CHLString                   m_strLastError;

    CSocketHL*                  m_pSocket;

    // Gateway Data
    CHLString                   m_sGatewayAddress;
    WORD                        m_wGatewayPort;
    BOOL                        m_bPortOpen;
    BOOL                        m_bRelayOn;

    // Gateway Recordset
    int                         m_iRowCount;
    int                         m_iColCount;
    CHLString*                  m_Schema;       // CHLString[m_iColCount]
    CHLObList                   m_Recordset;    // Object[m_iRowCount][m_iColCount], element is void*

    CHLObList                   m_Relays;       // List<CRelayServerDef*>

    // Properties
#ifdef PENTAIRMON
private:
    BOOL                        IsValid()             { return m_bValid;            }
public:
    CHLString                   LastError()           { return m_strLastError;      }
    CHLString                   GatewayAddress()      { return m_sGatewayAddress;   }
    WORD                        GatewayPort()         { return m_wGatewayPort;      }
    BOOL                        PortOpen()            { return m_bPortOpen;         }
    BOOL                        RelayOn()             { return m_bRelayOn;          }
    int                         RecordsetRowCount()   { return m_iRowCount;         }
    int                         RecordsetColCount()   { return m_iColCount;         }
    CHLString*                  RecordsetSchema()     { return m_Schema;            }
    CHLObList*                  Recordset()           { return &m_Recordset;         }
#else
private:
    static BOOL                 IsValid()             { return m_pThis   ? m_pThis->m_bValid               : FALSE;                          }
public:
    static CHLString            LastError()           { return m_pThis   ? m_pThis->m_strLastError         : _HLTEXT("No Instance created"); }
    static CHLString            GatewayAddress()      { return IsValid() ? m_pThis->m_sGatewayAddress      : _HLTEXT("");                    }
    static WORD                 GatewayPort()         { return IsValid() ? m_pThis->m_wGatewayPort         : -1;                             }
    static BOOL                 PortOpen()            { return IsValid() ? m_pThis->m_bPortOpen            : FALSE;                          }
    static BOOL                 RelayOn()             { return IsValid() ? m_pThis->m_bRelayOn             : FALSE;                          }
    static int                  RecordsetRowCount()   { return IsValid() ? m_pThis->m_iRowCount            : -1;                             }
    static int                  RecordsetColCount()   { return IsValid() ? m_pThis->m_iColCount            : -1;                             }
    static CHLString*           RecordsetSchema()     { return IsValid() ? m_pThis->m_Schema               : NULL;                           }
    static CHLObList*           Recordset()           { return IsValid() ? &m_pThis->m_Recordset           : NULL;                           }
    static CHLObList*           Relays()              { return IsValid() ? &m_pThis->m_Relays              : NULL;                           }
#endif

    // Interface
public:
#ifdef PENTAIRMON
    BOOL                        GetGatewayData(
    CHLString                   sGatewayName,
    CHLString                   sLoginName,
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );

    BOOL                        GetGatewayDbDump(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );

    BOOL                        GetActiveRelayList(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );

    void                        Dispose();
#else
    static BOOL                 GetGatewayData(
    CHLString                   sGatewayName,
    CHLString                   sLoginName,
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );

    static BOOL                 GetGatewayDbDump(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );

    static BOOL                 GetActiveRelayList(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );

    static void                 Dispose();
#endif

    // Construction/Destruction
#ifndef PENTAIRMON
private:
#endif
    CConnectionGDiscoveryMgr();

    CConnectionGDiscoveryMgr(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort );

public:
    ~CConnectionGDiscoveryMgr();

    // Helpers
private:
#ifdef PENTAIRMON
    BOOL                        Connect(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );
#else
    static BOOL                 Connect(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus );
#endif

    void                        SetLastError(
    CHLString                   sLastError )            { m_strLastError = sLastError; }

    BOOL                        GatewayConnectionRequest(
    CHLString                   sGatewayName,
    CHLString                   sLoginName );

    BOOL                        DownloadGatewayDB();

    BOOL                        DownloadActiveRelayList();

    void                        CleanRelayList();

}; // class CConnectionGDiscoveryMgr
