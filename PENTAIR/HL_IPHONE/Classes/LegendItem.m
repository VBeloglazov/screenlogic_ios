//
//  LegendItem.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "LegendItem.h"
#import "CustomPage.h"

@implementation CLegendItem
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Color:(UIColor*)pColor FillColor:(UIColor*)pColorFill Text:(NSString*)pText Round:(BOOL)bRound;

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame])
    {
        [self setOpaque:FALSE];
        [self setBackgroundColor:[UIColor clearColor]];
        

        m_pColor = pColor;
        m_pColorFill = pColorFill;

        [m_pColor retain];
        [m_pColorFill retain];

        m_bRound = bRound;

        UILabel *pLabel = [[UILabel alloc] initWithFrame:self.bounds];
        [CCustomPage InitStandardLabel:pLabel Size:10];
        [pLabel setText:pText];
        [self addSubview:pLabel];
        [pLabel release];

        if (!bRound)
        {
            [self setOpaque:NO];
            [self setBackgroundColor:[UIColor blackColor]];
        }
    }
    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Color:(UIColor*)pColor Text:(NSString*)pText

/*============================================================================*/
{
    CGColorRef pRGB = [pColor CGColor];
    const CGFloat *pfRGB = CGColorGetComponents(pRGB);
    UIColor *pFillColor = [UIColor colorWithRed:pfRGB[0]/2 green:pfRGB[1]/2 blue:pfRGB[2]/2 alpha:1.0  ];
    return [self initWithFrame:rFrame Color:pColor FillColor:pFillColor Text:pText Round:YES];
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pColor release];
    [m_pColorFill release];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(pRef, FALSE);
    int iInset = self.frame.size.height;
    int iDX = self.frame.size.width;
    int iDY = self.frame.size.height;

    double dPI = 3.14159;

    [m_pColorFill set];

    if (!m_bRound)
    {
        CGContextFillRect(pRef, CGRectMake(1, 1, rect.size.width, rect.size.height));
        [m_pColor set];
        CGContextStrokeRect(pRef, CGRectMake(1, 1, rect.size.width-2, rect.size.height-1));
        return;
    }

    CGContextFillRect(pRef, CGRectMake(iInset/2, 0, rect.size.width-iInset, rect.size.height));
    CGContextSaveGState(pRef);
    CGContextClipToRect(pRef, CGRectMake(0, 0, iInset/2, iInset));
    CGContextFillEllipseInRect(pRef, CGRectMake(0, 0, iInset, iInset));
    CGContextRestoreGState(pRef);
    CGContextSaveGState(pRef);
    CGContextClipToRect(pRef, CGRectMake(rect.size.width-iInset/2, 0, iInset/2,iInset));
    CGContextFillEllipseInRect(pRef, CGRectMake(rect.size.width-iInset, 0, iInset, iInset));
    CGContextRestoreGState(pRef);

    [m_pColor set];
    CGContextBeginPath (pRef);
    CGContextMoveToPoint(pRef, iInset/2, 1);
    CGContextAddLineToPoint(pRef, iDX - iInset/2, 1);
    CGContextAddArc(pRef, iDX - iInset / 2, iDY / 2, iDY / 2, -dPI/2, dPI/2, 0);
    CGContextMoveToPoint(pRef, iDX-iInset/2, iDY);
    CGContextAddLineToPoint(pRef, iInset/2, iDY);
    CGContextAddArc(pRef, iInset / 2, iDY / 2, iDY / 2, dPI/2, -dPI/2, 0);
    CGContextDrawPath(pRef, kCGPathStroke);

}
@end
