//
//  RoundRegion.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "RoundRegion.h"
#import "MainView.h"
#import "hlcomm.h"
#import "hlm.h"
#import "crect.h"
#import "TableTitleHeaderView.h"
#import "shader.h"
#import "hlm.h"
#import "HLToolBar.h"

@implementation CRoundRegion


/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame FrameStyle:(int)iFrameStyle RadiusStyle:(int)iRadiusStyle

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_iRad = [CHLComm GetInt:INT_TAB_ROUT];


        self.contentMode = UIViewContentModeRedraw;
        m_iFrameStyle = iFrameStyle;    

        if (iRadiusStyle == RADIUSSTYLE_NORADIUS)
            m_iRad = 0;

        if (rFrame.size.height > 0 && rFrame.size.width > 0)
        {
            [self displayLayer:self.layer];
        }
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pModifier release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetClientView:(UIView*)pView

/*============================================================================*/
{
    if (pView == m_pClientView)
        return;

    [m_pClientView removeFromSuperview];
    m_pClientView = NULL;

    m_pClientView = pView;
    
    if (m_pHighlight != NULL)
        [self insertSubview:pView belowSubview:m_pHighlight];
    else
        [self addSubview:pView];
}
/*============================================================================*/

-(void)SetClientViewAnimated:(UIView *)pView Direction:(int)iDir

/*============================================================================*/
{
    CGRect rFrameEnd = [self ClientRect:RECT_STDINSET];
    CGRect rFrameStart = rFrameEnd;
    CGRect rFrameOld = rFrameEnd;
    
    if (m_pClientView != NULL)
    {
        rFrameOld = m_pClientView.frame;
    }
    
    int iDX = rFrameStart.size.width;
    int iDY = rFrameStart.size.height;
    
    switch (m_iFrameStyle)
    {
    case FRAMESTYLE_LEFT:
    case FRAMESTYLE_RIGHT:
        iDX = 0;
        break;

    default:
        iDY = 0;
        break;
    }

    int iDX2 = iDX/2;
    int iDY2 = iDY/2;
    int iAlphaLo = 0;
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iDX2 = iDX;
        iDY2 = iDY;
        iAlphaLo = 1;
    }

    if (iDir == DIR_LEFT)
    {
        rFrameStart.origin.x += iDX;
        rFrameOld.origin.x -= iDX2;
        rFrameStart.origin.y += iDY;
        rFrameOld.origin.y -= iDY2;
    }
    else
    {
        rFrameStart.origin.x -= iDX;
        rFrameOld.origin.x += iDX2;
        rFrameStart.origin.y -= iDY;
        rFrameOld.origin.y += iDY2;
    }

    if (_m_pClientView != NULL)
    {
       [_m_pClientView removeFromSuperview];
        _m_pClientView = NULL;
    }
    
    if (m_pClientView == NULL)
    {
        [pView setFrame:rFrameEnd];
        [self SetClientView:pView];
        return;
    }


    [pView setFrame:rFrameStart];
    [pView setAlpha:iAlphaLo];

    if (m_pHighlight != NULL)
        [self insertSubview:pView belowSubview:m_pHighlight];
    else
        [self addSubview:pView];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView setAnimationDuration:0.25];
    [pView setFrame:rFrameEnd];
    [m_pClientView setFrame:rFrameOld];
    [m_pClientView setAlpha:iAlphaLo];
    [pView setAlpha:1];
    _m_pClientView = m_pClientView;
    m_pClientView = pView;
    
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [_m_pClientView removeFromSuperview];
    _m_pClientView = NULL;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pClientView setFrame:[self ClientRect:RECT_STDINSET]];

    int iDYHighlight = [CMainView DY_TOOLBAR] / 2;
    iDYHighlight = MIN(iDYHighlight, self.bounds.size.height / 2);
    [m_pHighlight setFrame:CGRectMake(0,0,self.bounds.size.width, iDYHighlight)];
}
/*============================================================================*/

-(CGRect)ClientRect:(int)iType

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    if (m_iFrameStyle == FRAMESTYLE_UPPER || m_iFrameStyle == FRAMESTYLE_LEFT)
    {
        return rThis;
    }

    int iXInset = 1;
    int iYInset = 1;
    
    if (iType == RECT_STDINSET)
    {
        iXInset += [CHLComm GetInt:INT_STD_INSET];
        iYInset += [CHLComm GetInt:INT_STD_INSET];

        if (m_iContentMode == CONTENT_MODE_FULLWIDTH)
        {
            iXInset = 1;
        }
        if (m_iContentMode == CONTENT_MODE_FULLHEIGHT_FULLWIDTH)
        {
            iXInset = 1;
            iYInset = 1;
        }
    }

    [CRect Inset:&rThis DX:iXInset DY:iYInset];
    return rThis;
}
/*============================================================================*/

-(UIView*)ClientView

/*============================================================================*/
{
    return m_pClientView;
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    CGImageRef pRef = [self GenerateImage:ELEMENT_STD];
    [layer setContents:(id)pRef];

    @try
    {
        [layer setShouldRasterize:YES];
    }
    @catch (NSException *ex)
    {
    }

    CGImageRelease(pRef);
}
/*============================================================================*/

-(CGImageRef)GenerateSegmentHighlight

/*============================================================================*/
{
    CGImageRef pImage = [self GenerateImage:ELEMENT_CHECK];
    return pImage;
}
/*============================================================================*/

-(int)FrameStyle                { return m_iFrameStyle;         }
-(void)FrameStyle:(int)iNew     { m_iFrameStyle = iNew;         }    
-(int)ContentMode               { return m_iContentMode;         }
-(void)ContentMode:(int)iNew    { m_iContentMode = iNew;         }    
-(int)Radius                    { return m_iRad;                }

/*============================================================================*/

-(void)HasHighlight:(BOOL)bHighlight

/*============================================================================*/
{
    if (bHighlight)
    {
        if (m_pHighlight != NULL)
            return;

        int iR1 = m_iRad;
        int iR2 = [CHLComm GetInt:INT_ALPHA_RMINOR];
        m_pHighlight = [[CToolBarHighlight alloc] initWithFrame:CGRectZero UpperRad:iR1 LowerRad:iR2];
        [self addSubview:m_pHighlight];
        [m_pHighlight release];
    }
    else
    {
        [m_pHighlight removeFromSuperview];
        m_pHighlight = NULL;
    }
}
/*============================================================================*/

-(CGImageRef)GenerateImage:(int)iType

/*============================================================================*/
{
    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    int iEdges = RREGION_EDGE_TOP|RREGION_EDGE_BOTTOM;
    unsigned int rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_TOP]];
    unsigned int rgbB = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_BTM]];
    unsigned int rgbM = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_MID]];

    unsigned int *prgbT = &rgbT;
    unsigned int *prgbM = &rgbM;
    
    int iFlags =[CHLComm GetInt:INT_TOPBARFLAGS];
    if ((iFlags & TOPBAR_FLAG_ENABLE_PAGETAB_RGB3) == 0)
    {
        prgbM = NULL;
    }
    if ((iFlags & TOPBAR_FLAG_ENABLE_PAGETAB_RGB2) == 0)
    {
        prgbT = NULL;
        prgbM = NULL;
    }

    switch (m_iFrameStyle)
    {
    case FRAMESTYLE_UPPER:
        {
            iEdges = RREGION_EDGE_TOP;
            rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_UNSELECT_TOP]];
            rgbB = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_UNSELECT_BTM]];
            prgbM = NULL;
        }
        break;
        
    case FRAMESTYLE_LOWER:
        iEdges = RREGION_EDGE_BOTTOM;
        break;
    case FRAMESTYLE_LEFT:
        iEdges = RREGION_EDGE_LEFT;
        break;
    case FRAMESTYLE_RIGHT:
        iEdges = RREGION_EDGE_RIGHT;
        break;
    }

    if (iType == ELEMENT_CHECK)
    {
        if (m_iFrameStyle == FRAMESTYLE_UPPER)
        {
            rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_SELECT_TOP_LS]];
            rgbB = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_SELECT_BTM_LS]];
            prgbM = NULL;
            prgbT = &rgbT;
        }
        else
        {
            if (prgbM != NULL)
            {
                rgbT = [CShader AddRGB:rgbT Val:30];
                rgbB = [CShader AddRGB:rgbB Val:30];
                rgbM = [CShader AddRGB:rgbM Val:30];
                prgbT = &rgbT;
                prgbM = &rgbM;
            }
            else
            {
                rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_SELECT_TOP_PT]];
                rgbB = [CShader MakeRGB:[CHLComm GetRGB:RGB_PAGETAB_SELECT_BTM_PT]];
                prgbT = &rgbT;
            }
        }
    }

    UIColor *pBG = NULL;
    if (m_iFlags & RREGION_FLAG_BASELEVEL)
        pBG = [CHLComm GetRGB:RGB_APP_BACKGROUND];

    CGImageRef pRef = [CShader CreateRegionImageDX:iDX 
                                                DY:iDY 
                                                Rad:m_iRad 
                                                RoundedEdges:iEdges 
                                                ColorT:prgbT 
                                                ColorM:prgbM
                                                ColorB:rgbB  
                                                FillCenter:NO 
                                                FillColor:0
                                                Modifier:m_pModifier
                                                SolidBG:pBG ];

    return pRef;
}
/*============================================================================*/

-(void)Modifier:(id)pModifier

/*============================================================================*/
{
    [m_pModifier release];
    m_pModifier = pModifier;
    
}
/*============================================================================*/

-(id)Modifier

/*============================================================================*/
{
    return m_pModifier;
}
/*============================================================================*/

-(int)Flags

/*============================================================================*/
{
    return m_iFlags;
}
/*============================================================================*/

-(void)Flags:(int)iNew

/*============================================================================*/
{
    m_iFlags = iNew;
}
@end
/*============================================================================*/

@implementation CRoundRegionSeparator

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
    
    }
    return self;
}
/*============================================================================*/

-(void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    CGImageRef pRef = [self GenerateImage];
    [layer setContents:(id)pRef];
}
/*============================================================================*/

-(CGImageRef)GenerateImage

/*============================================================================*/
{
    int iDX = self.bounds.size.width;
    CGImageRef pRef = [CShader CreateRegionSeparatorDX:iDX Position:m_iPositionPCT];
    return pRef;
}
/*============================================================================*/

-(void)SetPosition:(int)iPosition

/*============================================================================*/
{
    if (iPosition == m_iPositionPCT)
        return;
    m_iPositionPCT = iPosition;
    [[self layer] setContents:(id)[self GenerateImage]];
}
@end
