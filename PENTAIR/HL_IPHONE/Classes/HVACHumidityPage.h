//
//  HVACHumidityPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 7/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "hlview.h"

@class CTStat;
@class CHVACTempView;
@class CHLRadio;
@class CHVACModePage_IPAD;

/*============================================================================*/

@interface CHVACHumidityPage : UIView < CSink, CHLView >

/*============================================================================*/
{
    int                                 m_iHVACState;

    CTStat                              *m_pTStat;
    CHVACTempView                       *m_pAct;
    CHVACTempView                       *m_pSPLo;
    CHVACTempView                       *m_pSPHi;
    
    int                                 m_iDXLast;
    int                                 m_iDYLast;
}

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat;
-(void)UpdateControls;
-(CGRect)MakeRectAt:(CGPoint)ptC DX:(int)iDX DY:(int)iDY;

@end
