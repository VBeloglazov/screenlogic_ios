//
//  MP3Menu.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "IPOD_ViewController.h"
#import "AppDelegate.h"
#import "subtab.h"
#import "AudioZone.h"
#import "SubTabCell.h"
#import "AudioSource.h"
#import "AudioService.h"
#import "hlm.h"
#import "MP3ItemCell.h"
#import "MP3LibraryMenuItem.h"
#import "MP3Menu.h"
#import "HLComm.h"
#import "NSMSG.h"
#import "NSquery.h"
#import "tnowplaying.h"
#import "MediaZonePage.h"
#import "AudioServiceMenu.h"
#import "tmp3.h"
#import "VolumeControl.h"
#import "CustomPage.h"
#import "MainView.h"
#import "crect.h"
#import "MP3ContentView.h"

#define SECTION_SIZE                       30
#define ALPHA_SIZE                         20
#define ITEM_SIZE                          40


/*============================================================================*/

@implementation CMP3MenuItem

/*============================================================================*/

-(id)initWithName:(NSString*)pText Data:(int)iData Service:(CAudioService*)pService

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pText = pText;
        [m_pText retain];
        m_iData = iData;
        m_pService = pService;
    }

    return self;
}
/*============================================================================*/

-(int)Data

/*============================================================================*/
{
    return m_iData;
}
/*============================================================================*/

-(CAudioService*)Service

/*============================================================================*/
{
    return m_pService;
}
/*============================================================================*/

-(BOOL)OnIdleVisible

/*============================================================================*/
{
    return FALSE;
}
/*============================================================================*/

-(NSString*)Text

/*============================================================================*/
{
    return m_pText;
}
/*============================================================================*/

-(NSString*)AltText

/*============================================================================*/
{
    return m_pAltText;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pAltText release];
    [m_pText release];
    [super dealloc];
}
/*============================================================================*/

-(int)Index                     { return 0; }
-(int)ArtistIndex               { return 0; }
-(int)AlbumIndex                { return 0; }
-(int)TrackIndex                { return 0; }

-(int)GetType                   { return m_iType;   }
-(void)SetType:(int)iType       { m_iType = iType;  }
-(CGImageRef)GetImage           { return NULL;      }

/*============================================================================*/

@end

@implementation CMP3Menu

/*============================================================================*/

-(id)initWithFrame: (CGRect)rFrame
                ContentView:(CMP3ContentView *)pModalView 
                Zone:(CAudioZone*)pSubTab 
                Type:(int)iType 
                Sub1:(int)iSub1 
                Sub2:(int)iSub2 
                Title:(NSString*)pTitle 
                Service:(CAudioService*)pService

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain ];
        [self addSubview:m_pTableView];
        [m_pTableView release];

        [m_pTableView setDataSource:self];
        [m_pTableView setDelegate:self];
        m_pTableView.backgroundColor = [UIColor clearColor];
        
        
        m_pTitle = pTitle;
        [m_pTitle retain];
        m_pContentView = pModalView;
        m_pZone = (CAudioZone*)pSubTab;
        m_iType = iType;
        m_iSubType1 = iSub1;
        m_iSubType2 = iSub2;
        m_pService = pService;
        [m_pZone AddSink:self];
        m_pSections = [[NSMutableArray alloc] init];
        m_pSectionHeaders = [[NSMutableArray alloc] init];
        m_iLastSel = -1;
        m_iLastOptions = -1;

        m_pTableView.sectionIndexMinimumDisplayRowCount=10;

        if ([self HasAlphaInformation])
        {
            m_pTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            m_pTableView.separatorColor = [UIColor darkGrayColor];
        }
        else
        {
            m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
            m_pTableView.separatorStyle = UITableViewCellSeparatorStyleNone;


        [self Init];
    }
    
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pTableView setFrame:self.bounds];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(NSString*)Title           { return m_pTitle;      }
-(CAudioService*)Service    { return m_pService;    }

/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] == HLM_AUDIO_NSORTITEMSA)
    {
        if ([self HasAlphaInformation])
        {
            int piAlphaCount[28];
            for (int i = 0; i < 28; i++)
            {
                piAlphaCount[i] = [pMSG GetInt];
            }
            
            int iIndex = 0;
            for (int iChar = 1; iChar < 28; iChar++)
            {
                char cStart = '-';
                if (iChar > 1)
                    cStart = 'A' + iChar - 2;
                int iNRepChars = piAlphaCount[iChar];

                if (iNRepChars > 0)
                {
                    NSMutableArray *pItems = [[NSMutableArray alloc]init];
                    int iSection = [m_pSections count];
                    [m_pSections addObject:pItems];

                    NSString *pName1 = [NSString stringWithFormat:@"%c...", cStart];
                    NSString *pName2 = [NSString stringWithFormat:@"%c", cStart];
                    [m_pSectionHeaders addObject:pName2];

                    for (int iItem = 0; iItem < iNRepChars; iItem++)
                    {
                        CMP3LibraryMenuItem *pItem = [[CMP3LibraryMenuItem alloc] initWithData:pName1 Data:m_iType Section:iSection Row:iItem Index:iIndex Menu:self];
                        [pItems addObject:pItem];
                        iIndex++;
                    }
                }
            }
        }
        else
        {
            int iIndex = 0;
            int iN = [pMSG GetInt];
            NSMutableArray *pItems = [self InitSingleSection];
            for (int i = 0; i < iN; i++)
            {
                CMP3LibraryMenuItem *pItem = [[CMP3LibraryMenuItem alloc] initWithData:@"-" Data:m_iType Section:0 Row:iIndex Index:iIndex Menu:self];
                [pItems addObject:pItem];
                iIndex++;
            }
        }

        // We're not expecting anything more directly
        [self EndWaitData];

        [m_pTableView reloadData];        
        [self StartTimer];
    }
}
/*============================================================================*/

-(void)Init

/*============================================================================*/
{
    switch (m_iType)
    {
    case MENU_AUDIO_ROOT:
        {
            [self AddItem:@"Artists" Data:MENU_AUDIO_LIBRARY_ARTISTS_ALL Service:NULL];
            [self AddItem:@"Albums" Data:MENU_AUDIO_LIBRARY_ALBUMS_ALL Service:NULL];

            if ([m_pZone MP3Flags] & MP3SOURCE_HASGENRESORT)
                [self AddItem:@"Genres" Data:MENU_AUDIO_LIBRARY_ALBUM_GENRES Service:NULL];

            [self AddItem:@"Playlists" Data:MENU_AUDIO_LIBRARY_PLAYLISTS Service:NULL];
/*
            for (int iService = 0; iService < [m_pZone NServices]; iService++)
            {
                CAudioService *pService = [m_pZone Service:iService];
                [self AddItem:[pService Name] Data:MENU_AUDIO_SERVICE_ROOT Service:pService];
            }
*/
        }
        break;


    case MENU_AUDIO_LIBRARY_ARTISTS_ALL:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_ALLARTISTS Sub1:0 Sub2:0];
        }
        break;

    case MENU_AUDIO_LIBRARY_ALBUMS_ALL:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_ALLALBUMS Sub1:0 Sub2:0];
        }
        break;

    case MENU_AUDIO_LIBRARY_TRACKSBYALBUM:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_TRACKSBYALBUM Sub1:m_iSubType1 Sub2:0];
        }
        break;

    case MENU_AUDIO_LIBRARY_TRACKS_ALL:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_ALLTRACKS Sub1:0 Sub2:0];
        }
        break;

    case MENU_AUDIO_LIBRARY_ALBUMSBYARTIST:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_ALLALBUMSBYARTIST Sub1:m_iSubType1 Sub2:m_iSubType2];
        }
        break;
        
    case MENU_AUDIO_LIBRARY_PLAYLISTS:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_ALLPLAYLISTS Sub1:0 Sub2:0];
        }
        break;

    case MENU_AUDIO_LIBRARY_TRACKSBYPLAYLIST:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_TRACKSINPLAYLIST Sub1:m_iSubType1 Sub2:0];
        }
        break;

    case MENU_AUDIO_LIBRARY_TRACKSBYARTISTALBUM:
        {
            [self BeginWaitData];
            [self PostGetCount:SORT_TRACKSBYARTISTALBUM Sub1:m_iSubType1 Sub2:m_iSubType2];
        }
        break;
    }
   
}
/*============================================================================*/

-(void)AddItem:(NSString*)pText Data:(int)iData Service:(CAudioService*)pService

/*============================================================================*/
{
    CMP3MenuItem *pItem = [[CMP3MenuItem alloc] initWithName:pText Data:iData Service:pService];
    NSMutableArray *pItems = [self InitSingleSection];
    [pItems addObject:pItem];
}
/*============================================================================*/

-(NSMutableArray*)InitSingleSection

/*============================================================================*/
{
    NSMutableArray *pItems = NULL;
    if ([m_pSections count] > 0)
    {
        pItems = (NSMutableArray*)[m_pSections objectAtIndex:0];
    }
    else
    {
        pItems = [[NSMutableArray alloc] init];
        [m_pSections addObject:pItems];
    }

    return pItems;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_iType == MENU_AUDIO_ROOT)
    {
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_REMOVELIBRARYCLIENTQ];
        [pQ autorelease];
        [pQ SendMessage];
    }

    [m_pTitle release];
    [m_pTextView release];
    [m_pTimerIO invalidate];
    [m_pTimerIO release];
    [self EndWaitData];
    [m_pSections release];
    [m_pSectionHeaders release];
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return MAX(1, [m_pSections count]);
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    if ([m_pSections count] < 1)
        return 0;
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:section];
    return [pItems count];

}
/*============================================================================*/

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView 

/*============================================================================*/
{
	// returns the array of section titles. There is one entry for each unique character that an element begins with
	// [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    return m_pSectionHeaders;
}
/*============================================================================*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int iIndex = indexPath.row;
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:indexPath.section];
    CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:iIndex];
    if ([self ItemOptions:pItem] & ITEM_TYPE_IMAGEAVAIL)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            return 100;
        return 48;
    }
    return 40;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index 

/*============================================================================*/
{
	return index;
}
/*============================================================================*/

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 

/*============================================================================*/
{
    if (section >= [m_pSectionHeaders count])
        return @"";

    NSString *pS = (NSString*)[m_pSectionHeaders objectAtIndex:section];
    return pS;
}

/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    int iIndex = indexPath.row;
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:indexPath.section];
    CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:iIndex];

    int iOptions = 0;
    int iPlayOptions = [self ItemOptions:pItem];

    if ([self HasAlphaInformation] || ![self ItemHasSubMenus:pItem])
        iOptions = MP3ITEMCELL_NOACCESSORY;

    NSString *CellIdentifier = [NSString stringWithFormat:@"CELL%d", (iOptions|(iPlayOptions<<16))];
    CMP3ItemCell *cell = (CMP3ItemCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == NULL)
    {
        cell = [[[CMP3ItemCell alloc] initWithFrame:CGRectZero MP3Menu:self reuseIdentifier:CellIdentifier Options:iOptions PlayOptions:iPlayOptions] autorelease ];
    }

    [cell.m_pLabel setText:[pItem Text]];
    [cell SetAltText:[pItem AltText]];
    [cell SetImage:[pItem GetImage]];
    

    // Configure the cell
    return cell;
}
/*============================================================================*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

/*============================================================================*/
{
    if (buttonIndex == 1)
    {
        [self AnimateSelectedItem:YES];
        [self QueueSelectedItem:YES];
    }
}
/*============================================================================*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView

/*============================================================================*/
{
    [CHLComm RunIO:0];
    [self StartTimer];
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:indexPath.section];
    CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:indexPath.row];

    int iType               = [pItem Data];
    NSString *pTitle        = [pItem Text];
    CAudioService *pService = [pItem Service];

    int iOptions = [self ItemOptions:pItem];

    if (m_pTextView != NULL)
    {
        [m_pTextView removeFromSuperview];
        [m_pTextView release];
        m_pTextView = NULL;
    }

    if (iOptions & ITEM_TYPE_SEARCH)
    {
        CGRect rect = [m_pTableView rectForRowAtIndexPath:indexPath];
        int iYMIN = 20;
        [m_pTableView setContentOffset:CGPointMake(0, rect.origin.y - iYMIN) animated:YES];
        m_pTextView = [[UIView alloc] initWithFrame:rect];
        m_pTextView.backgroundColor = [UIColor blackColor];
        UIFont *pFont = [UIFont boldSystemFontOfSize:[CMainView DEFAULT_TEXT_SIZE]];
        CGRect rView  = CGRectMake(2, 0, rect.size.width-4, rect.size.height);

        UITextField *pTextField = [[UITextField alloc] initWithFrame:rView];
        pTextField.textColor = [UIColor blackColor];
        pTextField.font = pFont;
        pTextField.backgroundColor = [UIColor clearColor];
        pTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [pTextField setTextAlignment:UITextAlignmentLeft];
        [pTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [pTextField setReturnKeyType:UIReturnKeySearch];
        [pTextField addTarget:self action:@selector(SearchEvent:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [m_pTextView addSubview:pTextField];

        [pTextField release];

        [m_pTableView addSubview:m_pTextView];
        [pTextField becomeFirstResponder];
        return;
    }

    if (iOptions != 0)
    {
        if (indexPath.row != m_iLastSel)
        {
            m_iLastSel = indexPath.row;
            return;
        }
    }


    if (![self ItemHasSubMenus:pItem])
        return;

    int iSub1 = 0;
    int iSub2 = 0;
    CMP3Menu *pView = NULL;

    switch (m_iType)
    {
    case MENU_AUDIO_ROOT:
        {
            // We're at the root see if they're asking for a service
            CAudioService *pService = [pItem Service];
            if (pService != NULL)
                pView = [[CAudioServiceMenu alloc] initWithFrame:self.bounds ContentView:m_pContentView Zone:m_pZone Type:iType Sub1:iSub1 Sub2:iSub2 Title:pTitle Service:pService];
        }
        break;

    case MENU_AUDIO_SERVICE_ROOT:
        {
            iSub2 = [pItem Data];
            [self OnItemSelected:(int)iSub2];
            m_iLastSel = indexPath.row;
            return;
            

//            pView = [[CAudioServiceMenu alloc] initWithFrame:self.bounds ContentView:m_pContentView Zone:m_pZone Type:MENU_AUDIO_SERVICE_ROOT Sub1:m_iSubType1 Sub2:iSub2 Title:pTitle Service:m_pService NavString:@""];
        }
        break;

    case MENU_IRADIO_ALL:
    case MENU_IRADIO_FAVS:
        {
            // We are an iRadio genre sort
            iType = MENU_IRADIO_ALL_STATIONS_BYGENRE;
            if (m_iType == MENU_IRADIO_FAVS)
                iType = MENU_IRADIO_FAV_STATIONS_BYGENRE;
            pView = [[CAudioServiceMenu alloc] initWithFrame:self.bounds ContentView:m_pContentView Zone:m_pZone Type:iType Sub1:[pItem Index] Sub2:0 Title:pTitle Service:m_pService NavString:@""];
        }
        break;
        
    case MENU_AUDIO_LIBRARY_ARTISTS_ALL:
        {
            iType = MENU_AUDIO_LIBRARY_ALBUMSBYARTIST;
            iSub1 = [pItem ArtistIndex];
        }
        break;

    case MENU_AUDIO_LIBRARY_ALBUMS_ALL:
        {
            iType = MENU_AUDIO_LIBRARY_TRACKSBYALBUM;
            iSub1 = [pItem AlbumIndex];
        }
        break;

    case MENU_AUDIO_LIBRARY_PLAYLISTS:
        {
            iType = MENU_AUDIO_LIBRARY_TRACKSBYPLAYLIST;
            iSub1 = [pItem ArtistIndex];
        }
        break;

    case MENU_AUDIO_LIBRARY_ALBUMSBYARTIST:
        {
            iType = MENU_AUDIO_LIBRARY_TRACKSBYARTISTALBUM;
            iSub1 = [pItem ArtistIndex];
            iSub2 = [pItem AlbumIndex];
        }
        break;
    }

    m_iLastSel = indexPath.row;
    if (pView == NULL)
    {
        pView = [[CMP3Menu alloc] initWithFrame:self.bounds ContentView:m_pContentView Zone:m_pZone Type:iType Sub1:iSub1 Sub2:iSub2 Title:pTitle Service:pService];
    }

    if (pView != NULL)
    {
        [self PushView:pView];
    }
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
}
/*============================================================================*/

-(void)BeginWaitData

/*============================================================================*/
{
    if (m_bWaitData)
        return;
    [CHLComm AddSocketSink:self];
    m_bWaitData = TRUE;
}
/*============================================================================*/

-(void)EndWaitData

/*============================================================================*/
{
    if (!m_bWaitData)
        return;
    [CHLComm RemoveSocketSink:self];
    m_bWaitData = FALSE;
}
/*============================================================================*/

-(void)PostGetCount:(int)iType Sub1:(int)iSub1 Sub2:(int)iSub2

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc]initWithQ:HLM_AUDIO_NSORTITEMSQ];
    [pQ autorelease];
    [pQ PutInt:iType];
    [pQ PutInt:iSub1];
    [pQ PutInt:iSub2];
    [pQ PutInt:[m_pZone ActiveSourceID]];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(int)GetPacketsInFlight

/*============================================================================*/
{
    return m_iNPacketsInFlight;
}
/*============================================================================*/

-(void)SetPacketsInFlight:(int)iNew

/*============================================================================*/
{
    m_iNPacketsInFlight = iNew;
}
/*============================================================================*/

-(CAudioZone*)Zone

/*============================================================================*/
{
    return m_pZone;
}
/*============================================================================*/

-(int)SortType

/*============================================================================*/
{
    int iSortType = 0;
    switch (m_iType)
    {
    case MENU_AUDIO_LIBRARY_ARTISTS_ALL:            iSortType = SORT_ALLARTISTS;            break;
    case MENU_AUDIO_LIBRARY_ALBUMS_ALL:             iSortType = SORT_ALLALBUMS;             break;
    case MENU_AUDIO_LIBRARY_TRACKSBYALBUM:          iSortType = SORT_TRACKSBYALBUM;         break;
    case MENU_AUDIO_LIBRARY_TRACKS_ALL:             iSortType = SORT_ALLTRACKS;             break;
    case MENU_AUDIO_LIBRARY_PLAYLISTS:              iSortType = SORT_ALLPLAYLISTS;          break;
    case MENU_AUDIO_LIBRARY_TRACKSBYPLAYLIST:       iSortType = SORT_TRACKSINPLAYLIST;      break;
    case MENU_AUDIO_LIBRARY_ALBUMSBYARTIST:         iSortType = SORT_ALLALBUMSBYARTIST;     break;
    case MENU_AUDIO_LIBRARY_TRACKSBYARTISTALBUM:    iSortType = SORT_TRACKSBYARTISTALBUM;   break;

    default:
        break;
    }

    return iSortType;
}
/*============================================================================*/

-(int)SortSubType1

/*============================================================================*/
{
    return m_iSubType1;
}
/*============================================================================*/

-(int)SortSubType2

/*============================================================================*/
{
    return m_iSubType2;
}
/*============================================================================*/

-(int)ActivePageID

/*============================================================================*/
{
    return 0;
}
/*============================================================================*/

-(int)ItemOptions:(CMP3MenuItem*)pItem

/*============================================================================*/
{
    switch (m_iType)
    {
    case MENU_AUDIO_ROOT:
        return 0;
    case MENU_AUDIO_LIBRARY_ARTISTS_ALL:
    case MENU_AUDIO_LIBRARY_ALBUMS_ALL:
    case MENU_AUDIO_LIBRARY_PLAYLISTS:
    case MENU_AUDIO_LIBRARY_ALBUMSBYARTIST:
    case MENU_AUDIO_LIBRARY_TRACKSBYARTISTALBUM:
    case MENU_AUDIO_LIBRARY_TRACKSBYALBUM:
    case MENU_AUDIO_LIBRARY_TRACKSBYPLAYLIST:
    case MENU_AUDIO_LIBRARY_TRACKS_ALL:
        return ITEM_TYPE_PLAYABLE|ITEM_TYPE_ADDABLE;

    default:
        break;
    }
    
    return 0;
}
/*============================================================================*/

-(BOOL)ItemHasSubMenus:(CMP3MenuItem*)pItem;

/*============================================================================*/
{
    switch (m_iType)
    {
    case MENU_AUDIO_ROOT:
    case MENU_AUDIO_LIBRARY_ARTISTS_ALL:
    case MENU_AUDIO_LIBRARY_ALBUMS_ALL:
    case MENU_AUDIO_LIBRARY_PLAYLISTS:
    case MENU_AUDIO_LIBRARY_ALBUMSBYARTIST:
        return TRUE;

    default:
        break;
    }
    
    return FALSE;
}
/*============================================================================*/

-(CMP3ItemCell*)MP3CellForSection:(int)iSection Index:(int)iIndex

/*============================================================================*/
{
    NSIndexPath *pPath = [NSIndexPath indexPathForRow:iIndex inSection:iSection];
    UITableViewCell *pCell = [m_pTableView cellForRowAtIndexPath:pPath];
    if (pCell == NULL)
        return NULL;
    CMP3ItemCell *pMP3Cell = (CMP3ItemCell*)pCell;
    return pMP3Cell;
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_MP3_PLAY:
        {
            if ([m_pZone NPlaylistItems] > 0)
            {
                UIAlertView *pAlert = [[UIAlertView alloc] initWithTitle:@"Play Item" message:@"This operation will replace the current playlist"
                                        delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Play", nil];

                [pAlert show];	
                [pAlert release];
                return;
            }

            [self AnimateSelectedItem:YES];
            [self QueueSelectedItem:YES];
        }
        break;

    case CMD_MP3_ADD:
        {
            [self AnimateSelectedItem:NO];
            [self QueueSelectedItem:NO];
        }
        break;
    }

}
/*============================================================================*/

-(IBAction)AddItem:(id)sender

/*============================================================================*/
{
    [self QueueSelectedItem:NO];
}
/*============================================================================*/

-(void)AnimateSelectedItem:(BOOL)bPlay

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    CGRect rect = [m_pTableView rectForRowAtIndexPath:pPath];
    UIView *pRect = [[UIView alloc] initWithFrame:rect];
    if (bPlay)
        pRect.backgroundColor = [UIColor greenColor];
    else
        pRect.backgroundColor = [UIColor blueColor];
    [m_pTableView addSubview:pRect];
    [pRect release];

    CGRect rNew = rect;
    rNew.origin.y -= rNew.size.height / 2;
    rNew.size.height *= 2;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [pRect setAlpha:0.0];
    [pRect setFrame:rNew];
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)QueueSelectedItem:(BOOL)bPlay

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];

    int iSection = pPath.section;
    int iIndex = pPath.row;
    NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:iSection];
    CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:iIndex];

    [m_pZone AddSortedItem:[self SortType] Sub1:m_iSubType1 Sub2:m_iSubType2 Index:[pItem Index] Play:bPlay];
}
/*============================================================================*/

-(void)TimerProc

/*============================================================================*/
{
    BOOL bBusy = FALSE;
    NSArray *pArray = [m_pTableView indexPathsForVisibleRows];
    for (int i = 0; i < [pArray count]; i++)
    {
        NSIndexPath *pPath = (NSIndexPath*)[pArray objectAtIndex:i];
        NSMutableArray *pItems = (NSMutableArray*)[m_pSections objectAtIndex:pPath.section];
        CMP3MenuItem *pItem = (CMP3MenuItem*)[pItems objectAtIndex:pPath.row];
        if ([pItem OnIdleVisible])
            bBusy = TRUE;
    }

    if (!bBusy)
    {
        [m_pTimerIO invalidate];
        [m_pTimerIO release];
        m_pTimerIO = NULL;
        printf("Timer Stop\n");
    }
}
/*============================================================================*/

-(BOOL)HasAlphaInformation

/*============================================================================*/
{
    switch (m_iType)
    {
    case MENU_AUDIO_LIBRARY_ARTISTS_ALL:
    case MENU_AUDIO_LIBRARY_ALBUMS_ALL:
    case MENU_AUDIO_LIBRARY_TRACKS_ALL:
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(IBAction)SearchEvent:(id)sender

/*============================================================================*/
{
    if (m_pTextView != NULL)
    {
        [m_pTextView removeFromSuperview];
        [m_pTextView release];
        m_pTextView = NULL;
    }
}
/*============================================================================*/

-(void)StartTimer;

/*============================================================================*/
{
    if (m_pTimerIO != NULL)
        return;

    m_pTimerIO = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(TimerProc) userInfo:nil repeats:YES] retain];
    printf("Timer Start\n");
}
/*============================================================================*/

-(void)PushView:(CMP3Menu*)pView;

/*============================================================================*/
{
    [m_pContentView PushView:pView];
}
/*============================================================================*/

-(void)SetID:(int)iID

/*============================================================================*/
{
}
/*============================================================================*/

-(void)OnItemSelected:(int)iID

/*============================================================================*/
{
}
@end
