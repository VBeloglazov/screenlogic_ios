/*
--------------------------------------------------------------------------------

    NSSTATUSSINK.MM

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "NSStatusSink.h"

extern "C"
{

/*============================================================================*/

    void                        NSStatusSinkSetStatus(
    void                        *pSink,
    const char                  *pMSG )

/*============================================================================*/
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    NSString *pNSMSG = [NSString stringWithUTF8String:pMSG];
    id pNSSink = (id)pSink;
    [pNSSink SetStatusString:pNSMSG];
    [pPool release];
}
}