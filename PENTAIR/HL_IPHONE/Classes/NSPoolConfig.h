//
//  PoolConfig.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"

#ifdef __cplusplus
    class CPoolConfig;
    class CHLString;
#else
    @class CPoolConfig;
    @class CHLString;
#endif

@class CSinkServer;
@class CNSPoolCircuit;

#ifdef PENTAIR
@class CMainTab;
#endif

#define SUBTAB_PENTAIR_TEMP_POOL                    20001
#define SUBTAB_PENTAIR_TEMP_SPA                     20002
#define SUBTAB_PENTAIR_FEATURES                     20003
#define SUBTAB_PENTAIR_LIGHTS_ALL                   20004
#define SUBTAB_PENTAIR_LIGHTS_COLORS                20005
#define SUBTAB_PENTAIR_LIGHTS_INTELLIBRITE          20006
#define SUBTAB_PENTAIR_LIGHTS_MAGICSTREAM           20008
#define SUBTAB_PENTAIR_HISTORY_TEMPS                20009
#define SUBTAB_PENTAIR_PH_ORP                       20010
#define SUBTAB_PENTAIR_CHLORINATOR                  20011


/*============================================================================*/

@interface CNSPoolConfig : NSObject < CNSSocketSink >

/*============================================================================*/
{
    CPoolConfig                         *m_pPoolConfig;
    CSinkServer                         *m_pSinkServer;
    BOOL                                m_bCommInit;
    NSTimer                             *m_pPollTimer;
}

-(id)init;
-(int)NCircuitsByInterface:(int)iInterface;
-(void)LoadCircuitsByInterface:(NSMutableArray*)pList Interface:(int)iInterface;
-(void)SetCircuitFirstByInterface:(NSMutableArray*)pList;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;
-(void)NotifySinks;

-(NSString*)PoolText;
-(NSString*)SpaText;
-(NSString*)PumpName:(int)iPumpIndex;
-(NSString*)HeatModeText:(int)iHeatMode;
-(NSString*)HeatStatusText:(int)iHeatMode;
-(NSString*)FormatDegText:(int)iDeg;
-(NSString*)PHORPText;
-(NSString*)EnableText:(BOOL)bEnabled;
-(NSString*)FreezeModeStatusText;
-(NSString*)Text:(CHLString*)pText;

-(int)NIntelliflow;

-(void)InitComm;
-(void)ReleaseComm;
-(void)PostGetState;

-(void)BeginPoll;
-(void)EndPoll;
-(void)PollEvent;

-(BOOL)HasPool;
-(BOOL)HasSpa;
-(BOOL)PoolIsOn;
-(BOOL)SpaIsOn;

-(int)AirTemp;
-(int)WaterTemp:(int)iBodyType;

-(int)HeatMode:(int)iBodyType;
-(int)HeatStatus:(int)iBodyType;
-(int)HeatSetPoint:(int)iBodyType;

-(BOOL)RemotesEnabled;

-(void)SetHeatMode:(int)iBodyType Mode:(int)iMode;
-(void)SetHeatSetPoint:(int)iBodyType Temp:(int)iTemp;

-(void)SetCircuitOnOff:(CNSPoolCircuit*)pCircuit On:(BOOL)bOn;

-(int)MaxSetPoint:(int)iBodyType;
-(int)MinSetPoint:(int)iBodyType;

-(BOOL)IsChlorinatorInstalled;
-(BOOL)IsChlorinatorOK;
-(int)ChlorinatorPoolOutputLevel;
-(int)ChlorinatorSpaOutputLevel;
-(int)ChlorinatorSaltLevel;
-(Byte)ChlorinatorFlags;
-(Byte)ChlorinatorSuperTimer;

-(int)PH;
-(int)ORP;
-(int)Saturation;
-(int)Salt;
-(int)PHTankLevel;
-(int)ORPTankLevel;

-(BOOL)EquipPresent:(unsigned int)iMask;

#ifdef PENTAIR
-(void)AllocMainTabs:(NSMutableArray*)pArray;
-(void)AllocMainTab:(int)iID Array:(NSMutableArray*)pArray;
#endif

@end
