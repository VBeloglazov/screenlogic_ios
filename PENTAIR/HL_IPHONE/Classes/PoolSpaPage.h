//
//  PoolSpaPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPoolTempPage;
@class CNSPoolConfig;

/*============================================================================*/

@interface CPoolSpaPage : UIView 

/*============================================================================*/
{
    CNSPoolConfig               *m_pConfig;
    UIView                      *m_pPage1;
    UIView                      *m_pPage2;
}

-(id)initWithFrame:(CGRect) rFrame Config:(CNSPoolConfig*)pConfig;

@end
