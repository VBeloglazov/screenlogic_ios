//
//  PhotoFullFrameView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 8/31/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PhotoFullFrameView.h"
#import "Photo.h"
#import "PhotoView.h"
#import "PhotoAlbum.h"
#import "hlbutton.h"
#import "hlm.h"
#import "hlcomm.h"
#import "crect.h"

#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>


@implementation CPhotoFullFrameView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame PhotosPage:(CPhotosPage*)pPhotosPage PhotoView:(CPhotoView*)pView

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        [m_pScrollView setPagingEnabled:YES];
        [m_pScrollView setDelegate:self];
        [m_pScrollView setBounces:NO];
        [self addSubview:m_pScrollView];
        [m_pScrollView release];

        m_pL = [[CPhotoView alloc] initWithFrame:CGRectZero Photo:[pView Photo] Offset:-1];
        m_pC = [[CPhotoView alloc] initWithFrame:CGRectZero Photo:[pView Photo] Offset:0];
        m_pR = [[CPhotoView alloc] initWithFrame:CGRectZero Photo:[pView Photo] Offset:+1];
        [m_pScrollView addSubview:m_pL];
        [m_pScrollView addSubview:m_pC];
        [m_pScrollView addSubview:m_pR];
        [m_pL release];
        [m_pC release];
        [m_pR release];

        [m_pL setUserInteractionEnabled:NO];
        [m_pC setUserInteractionEnabled:NO];
        [m_pR setUserInteractionEnabled:NO];
        
        [m_pC SetContents:pView];

        m_pPhotosPage = pPhotosPage;
        m_pGButton = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTERMAX Style:0 Text:NULL TextSize:0 TextColor:NULL Color:NULL Icon:NULL];
        m_pNextButton = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTERMAX Style:0 Text:NULL TextSize:0 TextColor:NULL Color:NULL Icon:NULL];
        m_pPrevButton = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTERMAX Style:0 Text:NULL TextSize:0 TextColor:NULL Color:NULL Icon:NULL];
        [m_pGButton SetIconEmbedded:@"G.PNG"];
        [m_pNextButton SetIconEmbedded:@"RIGHT.png"];
        [m_pPrevButton SetIconEmbedded:@"LEFT.png"];
        [self addSubview:m_pGButton];
        [self addSubview:m_pPrevButton];
        [self addSubview:m_pNextButton];
        [m_pGButton release];
        [m_pNextButton release];
        [m_pPrevButton release];
        [m_pGButton addTarget:m_pPhotosPage action:@selector(ExitFullFrame:) forControlEvents:UIControlEventTouchDown];
        [m_pPrevButton addTarget:self action:@selector(Shift:) forControlEvents:UIControlEventTouchDown];
        [m_pNextButton addTarget:self action:@selector(Shift:) forControlEvents:UIControlEventTouchDown];

        [self UpdateButtons];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView

/*============================================================================*/
{
    [self scrollViewDidEndDecelerating:scrollView];
}
/*============================================================================*/

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView

/*============================================================================*/
{
    int iDX = m_pScrollView.bounds.size.width;
    int iOffset = m_pScrollView.contentOffset.x;

    if (iOffset > iDX / 2 && iOffset < iDX * 3 / 2)
    {
        [m_pScrollView setContentOffset:CGPointMake(iDX, 0) animated:YES];
        return;
    }

    if (iOffset < iDX)
    {
        printf("Shift Right\n");

        // User shifted to the right
        CPhotoView *pTemp = m_pR;
        m_pR = m_pC;
        m_pC = m_pL;
        m_pL = pTemp;
        [m_pL OffsetPhotoIndex:-3];
        [self layoutSubviews];
        [m_pScrollView setContentOffset:CGPointMake(iDX, 0)];
    }
    else if (iOffset > iDX)
    {
        printf("Shift Left\n");

        // User shifted to the left
        CPhotoView *pTemp = m_pL;
        m_pL = m_pC;
        m_pC = m_pR;
        m_pR = pTemp;
        [m_pR OffsetPhotoIndex:+3];
        [self layoutSubviews];
        [m_pScrollView setContentOffset:CGPointMake(iDX, 0)];
    }

    [self UpdateButtons];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    [m_pScrollView setFrame:rThis];
    int iDX = rThis.size.width;
    int iDXAll = 3 * iDX;
    [m_pScrollView setContentSize:CGSizeMake(iDXAll, rThis.size.height)];
    
    CGSize rThisSize = rThis.size;
    if (!CGSizeEqualToSize(rThisSize, m_SizeLast))
    {
        [m_pScrollView setContentOffset:CGPointMake(iDX, 0)];
        m_SizeLast = rThis.size;
    }


    [m_pL setFrame:CGRectMake(iDX * 0, 0, iDX, rThis.size.height)];
    [m_pC setFrame:CGRectMake(iDX * 1, 0, iDX, rThis.size.height)];
    [m_pR setFrame:CGRectMake(iDX * 2, 0, iDX, rThis.size.height)];

    if ([m_pC IsFirstImage])
    {
        int iOffset = m_pScrollView.contentOffset.x;
        if (iOffset < iDX)
            [m_pScrollView setContentOffset:CGPointMake(iDX, 0)];
    }
    if ([m_pC IsLastImage])
    {
        int iOffset = m_pScrollView.contentOffset.x;
        if (iOffset > iDX)
            [m_pScrollView setContentOffset:CGPointMake(iDX, 0)];
    }

    int iDYBtn = 80;
    int iDXBtn = 100;
    int iInset = [CHLComm GetInt:INT_PAGE_INSET];
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iDYBtn = 60;
        iDXBtn = 75;
        iInset = 4;
    }
    
    [CRect Inset:&rThis DX:iInset DY:iInset];
    CGRect rBtns = [CRect BreakOffBottom:&rThis DY:iDYBtn];
    CGRect rG    = [CRect BreakOffLeft:&rBtns DX:iDXBtn];
    CGRect rPrev = [CRect BreakOffLeft:&rBtns DX:iDXBtn];
    CGRect rNext = [CRect BreakOffLeft:&rBtns DX:iDXBtn];
    [CRect Inset:&rG DX:4 DY:4];
    [CRect Inset:&rNext DX:4 DY:4];
    [CRect Inset:&rPrev DX:4 DY:4];
    [m_pGButton setFrame:rG];
    [m_pNextButton setFrame:rNext];
    [m_pPrevButton setFrame:rPrev];
    
    [self OnIdleVisible];
}
/*============================================================================*/

-(void)OnIdleVisible

/*============================================================================*/
{
    [m_pL OnIdleVisible];
    [m_pC OnIdleVisible];
    [m_pR OnIdleVisible];
}
/*============================================================================*/

-(void)Shift:(id)sender

/*============================================================================*/
{
    if (sender == m_pPrevButton)
    {
        [m_pScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [m_pNextButton SetEnabled:NO];
        [m_pPrevButton SetEnabled:NO];
    }
    if (sender == m_pNextButton)
    {
        int iDX = m_pScrollView.bounds.size.width;
        [m_pScrollView setContentOffset:CGPointMake(2*iDX, 0) animated:YES];
        [m_pNextButton SetEnabled:NO];
        [m_pPrevButton SetEnabled:NO];
    }
}
/*============================================================================*/

-(void)UpdateButtons

/*============================================================================*/
{
    if ([m_pC IsFirstImage])
    {
        [m_pPrevButton SetEnabled:NO];
        [m_pPrevButton setAlpha:0.25];
    }
    else
    {
        [m_pPrevButton SetEnabled:YES];
        [m_pPrevButton setAlpha:0.75];
    }

    if ([m_pC IsLastImage])
    {
        [m_pNextButton SetEnabled:NO];
        [m_pNextButton setAlpha:0.25];
    }
    else
    {
        [m_pNextButton SetEnabled:YES];
        [m_pNextButton setAlpha:0.75];
    }
}
@end
