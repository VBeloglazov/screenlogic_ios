/*
--------------------------------------------------------------------------------

    HLINET.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "hltime.h"
#include "hlsock.h"

#include "hlinet.h"

/*============================================================================*/

    SOCKET                      ConnectHomeLogic(
    UINT                        nPort )

/*============================================================================*/
{
    //
    // Get the address
    //
    HOSTENT *pHE = gethostbyname("homelogic.com");
    if (pHE == NULL)
        return INVALID_SOCKET;

    char *pAddress = pHE->h_addr_list[0];
    BYTE byAd1 = pAddress[0];
    BYTE byAd2 = pAddress[1];
    BYTE byAd3 = pAddress[2];
    BYTE byAd4 = pAddress[3];

    // Put it into a SOCKADDR_IN
    SOCKADDR_IN address;
    SetAddress(&address, byAd1, byAd2, byAd3, byAd4, nPort);

    #ifdef MAC_OS
        address.sin_addr.s_addr = *((DWORD*)pHE->h_addr_list[0]);
    #else
        address.sin_addr.S_un.S_addr = *((DWORD*)pHE->h_addr_list[0]);
    #endif

    //
    // Make the socket and try to connect
    //
    SOCKET hSocket = socket(PF_INET, SOCK_STREAM, 0);
    if (hSocket == INVALID_SOCKET)
        return INVALID_SOCKET;

    if (connect(hSocket, (SOCKADDR*) &address, sizeof(address)) == SOCKET_ERROR)
    {
        CloseSocket(hSocket);
        return INVALID_SOCKET;
    }

    return hSocket;
}
/*============================================================================*/

    BOOL                        SendString(
    SOCKET                      hSocket,
    CHLString                   sOut )

/*============================================================================*/
{
    if (hSocket == INVALID_SOCKET)
        return FALSE;

    int iLen = sOut.GetLength();

    #if defined(POOLCONFIG) & defined(UNDER_CE)
        TCHAR *pOut = (TCHAR*)sOut.GetBuffer(iLen);
        char *pcOut = new char[iLen];        
        wcstombs(pcOut, pOut, iLen);

        if (send(hSocket, pcOut, iLen, 0) != iLen)
        {
            sOut.ReleaseBuffer(iLen);
            return FALSE;
        }

        delete [] pcOut;
    #else
#if defined(HLCONFIG) && defined(UNICODE)
		wchar_t *pBuf = (wchar_t*)sOut.GetBufferWCHAR(iLen);
		char	*pOut = new char[iLen];
		for(int i = 0; i < iLen; i++)
		{
			pOut[i] = (char)pBuf[i];
		}
        if (send(hSocket, pOut, iLen, 0) != iLen)
        {
            sOut.ReleaseBuffer(iLen);
			delete pOut;
            return FALSE;
        }
		delete pOut;
#else
        char *pOut = (char*)sOut.GetBufferCHAR(iLen);
        if (send(hSocket, pOut, iLen, 0) != iLen)
        {
            sOut.ReleaseBuffer(iLen);
            return FALSE;
        }
#endif
    #endif

    sOut.ReleaseBuffer(iLen);

    return TRUE;
}
