//
//  NSPoolCircuit.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


#ifdef __cplusplus
    class CPoolCircuit;
#else
    @class CPoolCircuit;
#endif

/*============================================================================*/

@interface CNSPoolCircuit : NSObject

/*============================================================================*/
{
    CPoolCircuit            *m_pCircuit;
}

-(id)initWithCircuit:(CPoolCircuit*)pCircuit;

-(NSString*)Name;
-(int)ID;
-(int)State;
-(int)Function;

@end
