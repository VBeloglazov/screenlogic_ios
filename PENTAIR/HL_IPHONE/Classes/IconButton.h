//
//  IconButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CUserTabIconView;

/*============================================================================*/

@interface CIconButton : UISegmentedControl

/*============================================================================*/
{
    CUserTabIconView                *m_pIconView;
}

-(id)init;
-(void)SetIcon:(NSString*)pName;


@end
