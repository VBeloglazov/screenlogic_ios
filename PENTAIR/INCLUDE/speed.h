/*
--------------------------------------------------------------------------------

    SPEED.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

This class is the collection place for data and functions that are used to make
the program go faster, and that don't cleanly go into "helpers.h" in the INCLUDE
directory, or in another existing class.

CRYSTALPAD instantiates one speed server during startup, so we can use that to
do one-time initialization of LUTs, etc.
--------------------------------------------------------------------------------
*/
#ifndef SPEED_H
#define SPEED_H

#ifndef HL_PIXEL
    #include "dibdc.h"
#endif

    //
    // Globals. These are used to convert to and from 5/6/5 16 bit colors.
    // See SPEED.CPP for more comments on how the image data is stored in
    // memory.
    //
    #define CACHE_SQRT          4097

    extern BYTE                 *g_SQRT;
    extern WORD                 *g_SQRT16;
    extern BYTE                 g_SIN100[100];


    inline HL_PIXEL             NonZero(
    HL_PIXEL                    wIn )                       { if (wIn == 0x0000) return 0x0821; else return wIn;    }

    #define PIXEL_COPY(d, s, n) memcpy(d, s, n * sizeof(HL_PIXEL))

    #ifdef HL_PIXEL32
        #ifdef IPHONE
            // iPhone is BGRA
            // Win32 is ARGB
            #define RGBTOHLPIXEL(x)     htonl(x)
            #define RGBFROMHLPIXEL(x)   htonl(x)
        #else
            #define RGBTOHLPIXEL(x)     x
            #define RGBFROMHLPIXEL(x)   (((x & 0xFF0000) >> 16)|((x & 0xFF) << 16)|(x & 0x00FF00))
        #endif

        #define GetRValueHLPIXEL(x) GetBValue(x)
        #define GetGValueHLPIXEL(x) GetGValue(x)
        #define GetBValueHLPIXEL(x) GetRValue(x)
        #define RGBtoHLPIXEL(r,g,b) ((r<<16)|(g<<8)|b)
    #else
        inline BYTE                 GetRValueHLPIXEL(HL_PIXEL wIn)   { return (((wIn >> 11) & 0x001F) * 255 / 31);  }
        inline BYTE                 GetGValueHLPIXEL(HL_PIXEL wIn)   { return (((wIn >> 5)  & 0x003F) * 255 / 63);  }
        inline BYTE                 GetBValueHLPIXEL(HL_PIXEL wIn)   { return ((wIn & 0x001F) * 255 / 31) ;         }

        inline COLORREF             RGBFROMHLPIXEL(
        short                       sh16 )
        {
            

            DWORD bRed5 = (sh16 >> 11 ) & 0x001F;
            DWORD bGrn6 = (sh16 >> 5  ) & 0x003F;
            DWORD bBlu5 = sh16          & 0x001F;
            return RGB(bRed5 * 255/31, bGrn6 * 255/63, bBlu5 * 255/31);
        }

        inline HL_PIXEL             RGBtoHLPIXEL(
        int                         iR,
        int                         iG,
        int                         iB )
        {
            iR = (iR << 8)  & 0xF800;
            iG = (iG << 3)  & 0x07E0;
            iB = (iB >> 3);

            return (iR|iG|iB);
        }

        inline HL_PIXEL             RGBTOHLPIXEL(COLORREF c)
        { 
            return RGBtoHLPIXEL(GetRValue(c), GetGValue(c), GetBValue(c));  
        }
    #endif

/*============================================================================*/

    class CSpeedServer

/*============================================================================*/
{
protected:
    #ifndef MAC_OS
    LARGE_INTEGER               m_liClockHz;
    LARGE_INTEGER               m_liClockStart;
    #endif

public:
    CSpeedServer();

    virtual ~CSpeedServer();

    #ifndef MAC_OS
    void                        StartTimer();
    DWORD                       ElapsedMicroSeconds();    
    DWORD                       ElapsedMilliSeconds();
    #endif
};

#endif

