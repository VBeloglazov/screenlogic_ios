//
//  ControlDef.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNSMSG;
@class CHLView;

#define CONTROLDEF_FLAG_SCALETEXT   0x00000001

/*============================================================================*/

@interface CControlDef : NSObject 

/*============================================================================*/
{
    int                         m_iType;
    int                         m_iData;
    NSString                    *m_sText;
    int                         m_iX;
    int                         m_iY;
    int                         m_iDX;
    int                         m_iDY;
    UIColor                     *m_RGBText;
    UIColor                     *m_RGBFace;
    char                        m_byTextSize;
    char                        m_byTextAlign;
    char                        m_byTextQuality;
    char                        m_byBorder;
    char                        m_byRadius;
    char                        m_byShadeIn;
    char                        m_byShadeOut;
    char                        m_byStyle;
    int                         m_wFunction;
    NSString                    *m_sIcon;
    int                         m_iFlags;

    int                         m_iZoneID;
    
    CGRect                      m_rAltBounds;

    int                         m_iXBL;
    int                         m_iYBT;
    int                         m_iXBR;
    int                         m_iYBB;
    
    UIView                      *m_pView;
}

-(id)initWithMSG:(CNSMSG*)pMSG Flags:(int)iFlags;

-(int)Type;
-(int)Data;

-(int)XLeft;
-(int)XRight;
-(int)YTop;
-(int)YBottom;

-(NSString*)Text;
-(NSString*)Icon;
-(int)Flags;
-(int)Style;
-(int)TextSize;
-(int)TextAlignment;
-(int)ShadeIn;
-(int)ShadeOut;
-(int)Radius;
-(UIColor*)RGBFace;
-(UIColor*)RGBText;
-(int)Function;

-(void)SetXLeft:(int)iX;
-(void)SetXRight:(int)iX;
-(void)SetYTop:(int)iY;
-(void)SetYBottom:(int)iY;

-(void)SetZoneID:(int)iZoneID;
-(int)GetZoneID;

-(CGRect)RectInBounds:(CGRect)rect;
-(UIView*)CreateView:(CGRect)rect;

-(BOOL)IntersectsWith:(CControlDef*) pDef;
-(BOOL)WantScrollLock;

-(UIView*)GetView;
-(void)SetView:(UIView*)pView;

@end
