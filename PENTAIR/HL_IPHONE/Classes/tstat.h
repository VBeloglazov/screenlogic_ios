//
//  tstat.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "SubTab.h"

@class CNSQuery;
@class CSinkServer;

#define SINK_CONFIGCHANGED      0x00000001

/*============================================================================*/

@interface CAltObject : NSObject

/*============================================================================*/
{
    int                         m_iID;
    NSString                    *m_pName;
    UIColor                     *m_pRGB;
}

-(id)initWithMSG:(CNSMSG*)pMSG;

@end

/*============================================================================*/

@interface CTStat : CSubTab < CNSSocketSink >

/*============================================================================*/
{
    // Basic Props
    CSinkServer                         *m_pSinkServer;

    int                         m_iNPacketsInFlight;

    int                         m_iHVACFlags;
    int                         m_iHeatUnitID;
    int                         m_iCoolUnitID;
    int                         m_iVentUnitID;

    // Status vars
    int                         m_iHeatSP;
    int                         m_iCoolSP;
    int                         m_iActualTemp;
    int                         m_iHVACState;
    int                         m_iFanState;
    int                         m_iProgramState;
    int                         m_iTempTime;
    BOOL                        m_bHeatActive;
    BOOL                        m_bCoolActive;
    BOOL                        m_bFanActive;
    int                         m_iR1;
    int                         m_iR2;
    BOOL                        m_bReady;
    
    int                         m_iHumidity;
    int                         m_iHumSPLo;
    int                         m_iHumSPHi;

    int                         m_iMinHeatSP;
    int                         m_iMaxHeatSP;
    int                         m_iMinCoolSP;
    int                         m_iMaxCoolSP;

    BOOL                        m_bAlarmState;

    NSMutableArray              *m_AltTemps;
    NSMutableArray              *m_AltStates;

    BOOL                        m_bCommInit;
    BOOL                        m_bStateOK;
    
    BOOL                        m_bHasSchedule;
}

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;

-(BOOL)CustomPage;

-(BOOL)HasHeat;
-(BOOL)HasCool;
-(BOOL)HasFan;
-(int)HVACFlags;
-(int)HVACMode;
-(int)FanMode;
-(int)ProgramState;
-(BOOL)StateOK;
-(BOOL)HasSchedule;
-(int)HoldTimeMinutes;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;

-(void)InitComm;
-(void)ReleaseComm;

-(void)PostGetState;
-(BOOL)ProcessStateMSG:(CNSMSG*)pMSG;

-(int)RoomTemp;
-(int)CoolSP;
-(int)HeatSP;
-(BOOL)HeatActive;
-(BOOL)CoolActive;
-(int)Humidity;
-(int)HumSPLo;
-(int)HumSPHi;
-(void)HumSPLo:(int)iNew;
-(void)HumSPHi:(int)iNew;

-(int)MaxHeatSP;
-(int)MinHeatSP;
-(int)MaxCoolSP;
-(int)MinCoolSP;

-(void)SetHVACMode:(int)iMode;
-(void)SetFanMode:(int)iMode;
-(void)SetProgramState:(int)iMode;
-(void)SetHeatSP:(int)iNew;
-(void)SetCoolSP:(int)iNew;
-(void)SetHoldTimeMinutes:(int)iNew;

-(void)UpdateSubTabCell:(CSubTabCell*)pSubTabCell;

@end
