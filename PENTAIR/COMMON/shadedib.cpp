/*
--------------------------------------------------------------------------------

    SHADEDIB.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"
#include "dibdc.h"
#include "speed.h"
#include "helpers.h"
#include "math.h"

#ifdef IPHONE
    #include "colormap.h"
    #include "hlm.h"
    #include "hlrect.h"
    #include "setserver.h"
#else
    #include "colormap.h"
    #include "main.h"
#endif

#include "shadedib.h"


#define SHINE_EXT               100
#define SHINE_INT               50

    HL_PIXEL                    CShadeDIB::g_FillMap[256];

    COLORREF                    CShadeDIB::g_FillRGB        = 0xFFFFFFFF;
    HL_PIXEL                    CShadeDIB::g_wFillRGB       = 0xFFFF;
    int                         CShadeDIB::g_wRsolid        = 0xFFFF;
    int                         CShadeDIB::g_wGsolid        = 0xFFFF;
    int                         CShadeDIB::g_wBsolid        = 0xFFFF;
    int                         CShadeDIB::g_iHints         = 0;

#ifdef IPHONE
    #define B_MASK              0xFF000000
    #define G_MASK              0x00FF0000
    #define R_MASK              0x0000FF00
#else
    #define R_MASK              0xF800
    #define G_MASK              0x07E0
    #define B_MASK              0x001F
#endif


/*========================================================================*/

    CShadeDIB::CShadeDIB(
    int                         iDX,
    int                         iDY,
    int                         iMode )

    : CHLDibDC(iDX, iDY)

/*========================================================================*/
{
    m_iMode = iMode;
    int iNPIX = m_iDX * m_iDY;
    memset(m_pBits, 0, iNPIX * sizeof(HL_PIXEL));
}
/*========================================================================*/

    CShadeDIB::CShadeDIB(
    CHLDibDC                    *pParentDIB,
    int                         iXOffset,
    int                         iYOffset,
    int                         iDX,
    int                         iDY,    
    int                         *piDX,
    int                         *piDY,
    int                         iMode )

    : CHLDibDC(pParentDIB, iXOffset, iYOffset, iDX, iDY, piDX, piDY)

/*========================================================================*/
{
    m_iMode = iMode|SHADEDIB_DIRECT;
}
/*========================================================================*/

    void                        CShadeDIB::AddCornersQ23(  
    int                         iRad,
    int                         iNShading,
    int                         iX,
    int                         iYT,
    int                         iYB,
    INIT_TYPE                   Type )

/*========================================================================*/
{
    int iRad16 = (iRad << 4)+8;
    int iRad16Max = iRad16+16;
    int iMaxElev16      = iNShading * 16;
    
    iNShading = __min(255, iNShading);
    int iPixelElevMax16 = (iNShading+1)*16;
    int iNShading16 = (iNShading << 4);
    int iRadInt16 = iRad16 - iNShading16;
    
    int iDX = iRad+1;
    int iDY = iRad+1;
    int iXOrg = iRad;
    int iYOrg = iRad;

    HL_PIXEL wOut = 0x0000;
    switch (Type)
    {
    case INIT_RAISED_EXT:
    case INIT_RAISED_EXT_INV:
        wOut = 0x00FF;
        break;

    case INIT_RAISED_INT:
    case INIT_RAISED_INT_INV:
    case INIT_RAISED_INT_GRADIENT:
        break;
    }

    int iQT=2;
    int iQB=3;

    if (!IsInterior(Type))
    {
        iQT=3;
        iQB=2;
    }    

    int iShine = MaxShineInt(iNShading);

    switch (Type)
    {
    case INIT_RAISED_INT_GRADIENT:
    case INIT_RAISED_INT:
        break;

    case INIT_RAISED_EXT:
        iShine = SHINE_EXT;
        break;

    case INIT_RAISED_EXT_INV:
        break;

    case INIT_RAISED_INT_INV:
        iShine = SHINE_EXT;
        break;
    }

    ShaderFunc *pShaderT = ShaderFor(iQT, Type);
    ShaderFunc *pShaderB = ShaderFor(iQB, Type);
    int iRad2_Plus  = (iRad+2)*(iRad+2);

    if (m_iMode & SHADEDIB_DIRECT)
    {
        switch (Type)
        {
        case INIT_RAISED_INT_GRADIENT:
        case INIT_RAISED_INT:
        case INIT_RAISED_INT_INV:
            {
                for (int iYSet = 0; iYSet < iDY; iYSet++)
                {
                    HLT_SCANLINE_ROI roi = GetScanlineROIQ2(iYSet, iRad2_Plus, iRad, iNShading);
                    HL_PIXEL *pSetT = Pixel(iX+roi.m_iX0, iYT + iYSet);
                    HL_PIXEL *pSetB = Pixel(iX+roi.m_iX0, iYB - iYSet);

                    int iDYC = iYSet-iYOrg;
                    int iDYC2 = iDYC * iDYC;

                    HLT_PIXEL_VARS vars;
                    vars.m_iDYC         = iDYC;
                    vars.m_iMaxElev16   = iMaxElev16;
                    vars.m_iShine       = iShine;

                    if (Type == INIT_RAISED_INT_GRADIENT)
                    {
                        vars.m_wColor12 = GradientAt(iYT + iYSet, iYT);
                        vars.m_wColor34 = GradientAt(iYB - iYSet, iYT);
                        FillHZ(vars.m_wColor12, iX+roi.m_iX1, iX+iDX-1, iYT+iYSet);
                        FillHZ(vars.m_wColor34, iX+roi.m_iX1, iX+iDX-1, iYB-iYSet);
                    }
                    else
                    {
                        FillHZ(g_wFillRGB, iX+roi.m_iX1, iX+iDX-1, iYT+iYSet);
                        FillHZ(g_wFillRGB, iX+roi.m_iX1, iX+iDX-1, iYB-iYSet);
                    }

                    for (int iXSet = roi.m_iX0; iXSet < roi.m_iX1; iXSet++)
                    {
                        int iDXC = iXSet-iXOrg;   
                        int iRadToPoint16 = iSqrt16(iDXC*iDXC + iDYC2);
                        if (iRadToPoint16<=iRad16Max)
                        {
                            int iElevThis16 = iRad16 - iRadToPoint16;
                            vars.m_iDXC = iDXC;
                            vars.m_iElev_16 = iElevThis16;
                            pShaderT(&vars, pSetT);
                            pShaderB(&vars, pSetB);
                        }

                        pSetT++;
                        pSetB++;
                    }
                }
            }
            break;
        
        case INIT_RAISED_EXT:
        case INIT_RAISED_EXT_INV:
            {
                for (int iYSet = 0; iYSet < iDY; iYSet++)
                {
                    HLT_SCANLINE_ROI roi = GetScanlineROIQ2(iYSet, iRad2_Plus, iRad, iNShading);
                    HL_PIXEL *pSetT = Pixel(iX+roi.m_iX0, iYT + iYSet);
                    HL_PIXEL *pSetB = Pixel(iX+roi.m_iX0, iYB - iYSet);
                    int iDYC = iYSet-iYOrg;
                    int iDYC2 = iDYC * iDYC;

                    HLT_PIXEL_VARS vars;
                    vars.m_iDYC         = iDYC;
                    vars.m_iMaxElev16   = iMaxElev16;
                    vars.m_iShine       = iShine;
                
                    for (int iXSet = roi.m_iX0; iXSet < roi.m_iX1; iXSet++)
                    {
                        int iDXC = iXSet-iXOrg;   
                        int iRadToPoint16 = iSqrt16(iDXC*iDXC + iDYC2);
                        if (iRadToPoint16<=iRad16Max)
                        {
                            int iElevThis16 = iRadToPoint16 - iRadInt16;
                            vars.m_iDXC = iDXC;
                            vars.m_iElev_16 = iElevThis16;
                            pShaderT(&vars, pSetT);
                            pShaderB(&vars, pSetB);
                        }
                        pSetT++;
                        pSetB++;
                    }
                }
            }
            break;
        }

        return;
    }


    switch (Type)
    {
    case INIT_RAISED_INT:
    case INIT_RAISED_INT_INV:
    case INIT_RAISED_INT_GRADIENT:
        {
            for (int iYSet = 0; iYSet < iDY; iYSet++)
            {
                HLT_SCANLINE_ROI roi = GetScanlineROIQ2(iYSet, iRad2_Plus, iRad, iNShading);
                HL_PIXEL *pSetT = Pixel(iX+roi.m_iX0, iYT + iYSet);
                HL_PIXEL *pSetB = Pixel(iX+roi.m_iX0, iYB - iYSet);
                int iDYC = iYSet-iYOrg;
                int iDYC2 = iDYC * iDYC;

                // These are inclusive
                FillHZ(wOut, iX, iX+roi.m_iX0-1, iYT+iYSet);
                FillHZ(wOut, iX, iX+roi.m_iX0-1, iYB-iYSet);
                FillHZ(0x00FF, iX+roi.m_iX1, iX+iDX-1, iYT+iYSet);
                FillHZ(0x00FF, iX+roi.m_iX1, iX+iDX-1, iYB-iYSet);

                HLT_PIXEL_VARS vars;
                vars.m_iDYC         = iDYC;
                vars.m_iMaxElev16   = iMaxElev16;
                vars.m_iShine       = iShine;

                for (int iXSet = roi.m_iX0; iXSet < roi.m_iX1; iXSet++)
                {
                    int iDXC = iXSet-iXOrg;   
                    int iRadToPoint16 = iSqrt16(iDXC*iDXC + iDYC2);
                    if (iRadToPoint16<=iRad16Max)
                    {
                        int iElevThis16 = iRad16 - iRadToPoint16;
                        if (iElevThis16 > iPixelElevMax16)
                        {
                            // Up on the surface (inside)
                            *pSetT = (HL_PIXEL)0x00FF;
                            *pSetB = (HL_PIXEL)0x00FF;
                        }
                        else
                        {
                            // Coming up side up to surface
                            vars.m_iDXC = iDXC;
                            vars.m_iElev_16 = iElevThis16;
                            pShaderT(&vars, pSetT);
                            pShaderB(&vars, pSetB);
                        }
                    }
                    else
                    {
                        // Outside
                        *pSetT = wOut;
                        *pSetB = wOut;
                    }

                    pSetT++;
                    pSetB++;
                }
            }
        }
        break;
    
    case INIT_RAISED_EXT:
    case INIT_RAISED_EXT_INV:
        {
            for (int iYSet = 0; iYSet < iDY; iYSet++)
            {
                HLT_SCANLINE_ROI roi = GetScanlineROIQ2(iYSet, iRad2_Plus, iRad, iNShading);

                // These are inclusive

                FillHZ(0x00FF, iX, iX+roi.m_iX0-1, iYT+iYSet);
                FillHZ(0x00FF, iX, iX+roi.m_iX0-1, iYB-iYSet);
                FillHZ(0x0000, iX+roi.m_iX1, iX+iDX-1, iYT+iYSet);
                FillHZ(0x0000, iX+roi.m_iX1, iX+iDX-1, iYB-iYSet);

                HL_PIXEL *pSetT = Pixel(iX+roi.m_iX0, iYT + iYSet);
                HL_PIXEL *pSetB = Pixel(iX+roi.m_iX0, iYB - iYSet);
                int iDYC = iYSet-iYOrg;
                int iDYC2 = iDYC * iDYC;
            
                HLT_PIXEL_VARS vars;
                vars.m_iDYC         = iDYC;
                vars.m_iMaxElev16   = iMaxElev16;
                vars.m_iShine       = iShine;

                for (int iXSet = roi.m_iX0; iXSet < roi.m_iX1; iXSet++)
                {
                    int iDXC = iXSet-iXOrg;   
                    int iRadToPoint16 = iSqrt16(iDXC*iDXC + iDYC2);
                    if (iRadToPoint16<=iRad16Max)
                    {
                        int iElevThis16 = iRadToPoint16 - iRadInt16;
                        if (iElevThis16 > iPixelElevMax16)
                        {
                            *pSetT = (HL_PIXEL)0x00FF;
                            *pSetB = (HL_PIXEL)0x00FF;
                        }
                        else
                        {
                            vars.m_iDXC = iDXC;
                            vars.m_iElev_16 = iElevThis16;
                            pShaderT(&vars, pSetT);
                            pShaderB(&vars, pSetB);
                        }
                    }
                    else
                    {
                        *pSetT = wOut;
                        *pSetB = wOut;
                    }

                    pSetT++;
                    pSetB++;
                }
            }
        }
        break;
    }

}
/*========================================================================*/

    void                        CShadeDIB::FlipHzCopy(
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iDY )

/*========================================================================*/
{
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY);
        HL_PIXEL *pDst = Pixel(iXDst+iDX-1, iYDst+iY);
        int iD = iDX;
        while (iD > 0)
        {
            *pDst = *pSrc;
            pDst--;
            pSrc++;
            iD--;
        }
    }
}
/*========================================================================*/

    void                        CShadeDIB::FlipHz()

/*========================================================================*/
{
    CHLDibDC *pCopy = CreateCopy();

    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pSrc = pCopy->Pixel(m_iImageDX-1, iY);
        HL_PIXEL *pTgt = Pixel(0, iY);
        for (int iX = 0; iX < m_iImageDX; iX++)
        {
            *pTgt = *pSrc;
            pTgt++;
            pSrc--;
        }
    }

    delete pCopy;
}
/*========================================================================*/

    void                        CShadeDIB::FlipVt()

/*========================================================================*/
{
    CHLDibDC *pCopy = CreateCopy();
    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pSrc = pCopy->Pixel(0, m_iDY-iY-1);
        HL_PIXEL *pTgt = Pixel(0, iY);
        memcpy(pTgt, pSrc, m_iDX * sizeof(HL_PIXEL));
    }

    delete pCopy;
}
/*========================================================================*/

    void                        CShadeDIB::InitStandard(
    int                         iRad,
    int                         iNShading,
    INIT_TYPE                   Type,
    int                         iEdgeInset,
    COLORREF                    rgb,
    int                         iHints  )

/*========================================================================*/
{
    START_TIMER(GDI_SHADEPOLY);

    g_iHints = iHints;

    if (iHints & SHADEDIB_HINT_OVERSCAN)
        iNShading++;

    if ((m_iMode & SHADEDIB_DIRECT) && IsInterior(Type))
    {
        SetFillColor(rgb);
    }

    HL_PIXEL wInterior = 0x00FF;
    switch (Type)
    {
    case INIT_RAISED_EXT:
    case INIT_RAISED_EXT_INV:
        wInterior = 0x0000;
        break;

    case INIT_RAISED_INT:
    case INIT_RAISED_INT_GRADIENT:
    case INIT_RAISED_INT_INV:
        break;
    }

    if (m_iMode & SHADEDIB_DIRECT)
    {
        wInterior = g_wFillRGB;
    }

    int iDX = ImageDX()-2*iEdgeInset;
    int iDY = DY()-2*iEdgeInset;

    iEdgeInset = __max(0, iEdgeInset);
    iEdgeInset = __min(iEdgeInset, m_iDX/2);
    iEdgeInset = __min(iEdgeInset, m_iDY/2);

    int iMaxRad = __min(iRad, iDX/2-1);
    iMaxRad     = __min(iMaxRad, iDY/2-1);

    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        if (iNShading >= iMaxRad)
        {
            CHLRect rAll(0, 0, m_iImageDX, m_iDY);
            rAll.Inset(iEdgeInset, iEdgeInset);
            CHLRect rSolid = rAll;        
            m_ROI.AddRectRef(&rAll);
            rSolid.Inset(iNShading, iNShading);
            if (rSolid.DX() > 0 && rSolid.DY() > 0)
            {
                CHLRect rSub = rSolid;
                rSub.Inset(1, 1);
                m_ROI.Subtract(&rSub);
                m_Solid.AddRectRef(&rSolid);
            }
        }
        else
        {
            CDXYRect rAll(0, 0, m_iImageDX, m_iDY);
            rAll.Inset(iEdgeInset, iEdgeInset);
            m_ROI.AddRectRef(&rAll);

            CDXYRect rHZ(iEdgeInset+iNShading, iEdgeInset+iMaxRad, m_iImageDX-2*(iNShading+iEdgeInset), m_iDY - 2 * (iEdgeInset+iMaxRad));
            rHZ.Inset(1, 1);
            if (rHZ.DX() > 0 && rHZ.DY() > 0)
            {
                m_ROI.Subtract(&rHZ);
                m_Solid.AddRectDirectRef(&rHZ);
            }

            int iDYSolid = iMaxRad-iNShading;


            CHLRect rVT1 = CDXYRect(iEdgeInset+iMaxRad, iEdgeInset + iNShading + 1, m_iImageDX-2*(iMaxRad+iEdgeInset), iDYSolid-1);
            m_ROI.Subtract(&rVT1);
            m_Solid.AddRectDirectRef(&rVT1);

            CHLRect rVT2 = CDXYRect(iEdgeInset+iMaxRad, m_iDY-iEdgeInset-iNShading-iDYSolid, m_iImageDX-2*(iMaxRad+iEdgeInset), iDYSolid-1);
            m_ROI.Subtract(&rVT2);
            m_Solid.AddRectDirectRef(&rVT2);
        }
        m_ROI.Consolidate();
        m_Solid.Consolidate();
    }

    int iDCopy = iMaxRad+1;

    // Q1 is a mirror of Q2
    // Q4 is a mirror of Q3
    AddCornersQ23(iMaxRad, iNShading, iEdgeInset, iEdgeInset, m_iDY-iEdgeInset-1, Type);
    FlipHzCopy(iEdgeInset, iEdgeInset, m_iImageDX-iEdgeInset-iDCopy, iEdgeInset, iDCopy, iDCopy);
    FlipHzCopy(iEdgeInset, m_iDY-iEdgeInset-iDCopy, m_iImageDX-iEdgeInset-iDCopy, m_iDY-iEdgeInset-iDCopy, iDCopy, iDCopy);

    if (IsInterior(Type) || (m_iMode & SHADEDIB_DIRECT) == 0)
    {
        if (iMaxRad < iNShading)
        {
            CDXYRect rFill(0, 0, m_iImageDX, m_iDY);
            rFill.Inset(iMaxRad+iEdgeInset, iMaxRad+iEdgeInset);
            Fill(wInterior, rFill);
        }
        else
        {
            CDXYRect rFill(0, 0, m_iImageDX, m_iDY);
            rFill.Inset(iEdgeInset, iEdgeInset);
            CHLRect rT = rFill.BreakOffTop(iMaxRad+1, 0);
            CHLRect rB = rFill.BreakOffBottom(iMaxRad+1, 0);
            rT.BreakOffTop(iNShading, 0);        
            rB.BreakOffBottom(iNShading, 0);        
            rT.Inset(iMaxRad+1, 0);
            rB.Inset(iMaxRad+1, 0);
            rFill.Inset(iNShading, 0);
            Fill(wInterior, rT);
            Fill(wInterior, rFill);
            Fill(wInterior, rB);
        }
    }

    int iShine = MaxShineInt(iNShading);
    if (Type == INIT_RAISED_EXT || Type == INIT_RAISED_INT_INV)
        iShine = SHINE_EXT;

    int iMaxElev16 = (iNShading * 16);
    
    if (m_iMode & SHADEDIB_DIRECT)
    {
        ShaderFunc *pQ12 = ShadeValueQ12_ELEVDirectColor;
        ShaderFunc *pQ34 = ShadeValueQ34_ELEVDirectColor;

        if (!IsInterior(Type))
        {
            pQ12 = ShadeValueQ12_ELEVDirect;
            pQ34 = ShadeValueQ34_ELEVDirect;
        }

        HLT_PIXEL_VARS vars;
        vars.m_iShine       = iShine;
        vars.m_iMaxElev16   = iMaxElev16;

        for (int iEdge = 0; iEdge <= iNShading; iEdge++)
        {
            int iInset = 0;
            if (iEdge > iMaxRad)
                iInset = iEdge - iMaxRad;

            int iX0 = iMaxRad+iInset+iEdgeInset+1;
            int iY0 = iMaxRad+iInset+iEdgeInset+1;

            int iX1TB = m_iImageDX-iMaxRad-iEdgeInset-iInset-2;
            int iY1LR = m_iDY-iMaxRad-iEdgeInset-iInset-2;
    

            BOOL bTB = ((iX1TB-iX0+1) >= 1);
            BOOL bLR = ((iY1LR-iY0+1) >= 1);

            HL_PIXEL *pT = Pixel(iX0, iEdge+iEdgeInset);
            HL_PIXEL *pB = Pixel(iX0, m_iDY-iEdge-1-iEdgeInset);
            HL_PIXEL *pL = Pixel(iEdge+iEdgeInset,              iY0);
            HL_PIXEL *pR = Pixel(m_iImageDX-iEdgeInset-iEdge-1, iY0);


            if (IsInterior(Type))
            {
                int iD_16 = ((iEdge+1)<<4)-8;
                vars.m_iElev_16 = iD_16;
            
                if (bTB)
                {
                    vars.m_iDXC = 0;
                    vars.m_iDYC = -1;
                    pQ12(&vars, pT);
                    vars.m_iDYC = 1;
                    pQ34(&vars, pB);
                }

                if (bLR)
                {
                    vars.m_iDXC = -1;
                    vars.m_iDYC = 0;
                    pQ34(&vars, pL);
                    vars.m_iDXC = 1;
                    pQ34(&vars, pR);
                }

                FillVT(*pL, iEdge+iEdgeInset,               iY0, iY1LR);
                FillVT(*pR, m_iImageDX-iEdgeInset-iEdge-1,  iY0, iY1LR);
            }
            else
            {
                int iD_16 = ((iNShading-iEdge)<<4)-8;
                vars.m_iElev_16 = iD_16;

                if (bTB)
                {
                    vars.m_iDXC = 0;
                    vars.m_iDYC = 1;
                    pQ34(&vars, pT);
                    vars.m_iDYC = -1;
                    pQ12(&vars, pB);
                }

                int iPitch = DXPitch();
                int iDYFill = iY1LR - iY0 + 1;
                for (int i = 0; i < iDYFill; i++)
                {
                    vars.m_iDXC = 1;
                    vars.m_iDYC = 0;
                    pQ34(&vars, pL);
                    vars.m_iDXC = -1;
                    pQ34(&vars, pR);
                    pL += iPitch;
                    pR += iPitch;
                }
            }

            FillHZ(*pT, iX0, iX1TB, iEdge+iEdgeInset);
            FillHZ(*pB, iX0, iX1TB, m_iDY-iEdge-1-iEdgeInset);
        }
    }
    else
    {
        HL_PIXEL wT, wB, wL, wR;

        HLT_PIXEL_VARS vars;
        vars.m_iShine       = iShine;
        vars.m_iMaxElev16   = iMaxElev16;

        for (int iEdge = 0; iEdge <= iNShading; iEdge++)
        {
            if (IsInterior(Type))
            {
                int iD_16 = ((iEdge+1)<<4)-8;
                vars.m_iElev_16 = iD_16;

                vars.m_iDXC = 0;
                vars.m_iDYC = -1;
                ShadeValueQ12_ELEV(&vars, &wT);
                vars.m_iDYC = 1;
                ShadeValueQ34_ELEV(&vars, &wB);
                vars.m_iDXC = -1;
                vars.m_iDYC = 0;
                ShadeValueQ34_ELEV(&vars, &wL);
                vars.m_iDXC = 1;
                ShadeValueQ34_ELEV(&vars, &wR);
            }
            else
            {
                int iD_16 = ((iNShading-iEdge)<<4)-8;
                vars.m_iElev_16 = iD_16;

                vars.m_iDXC = 0;
                vars.m_iDYC = 1;
                ShadeValueQ34_ELEV(&vars, &wT);
                vars.m_iDYC = -1;
                ShadeValueQ12_ELEV(&vars, &wB);
                vars.m_iDXC = 1;
                vars.m_iDYC = 0;
                ShadeValueQ34_ELEV(&vars, &wL);
                vars.m_iDXC = -1;
                ShadeValueQ34_ELEV(&vars, &wR);
            }
            

            int iInset = 0;
            if (iEdge > iMaxRad)
                iInset = iEdge - iMaxRad;
            FillHZ(wT, iMaxRad+iInset+iEdgeInset, m_iImageDX-iMaxRad-iEdgeInset-iInset-2, iEdge+iEdgeInset);
            FillHZ(wB, iMaxRad+iInset+iEdgeInset, m_iImageDX-iMaxRad-iEdgeInset-iInset-2, m_iDY-iEdge-1-iEdgeInset);
            FillVT(wL, iEdge+iEdgeInset, iMaxRad+iInset+iEdgeInset, m_iDY-iMaxRad-iEdgeInset-iInset-2);
            FillVT(wR, m_iImageDX-iEdgeInset-iEdge-1, iMaxRad+iInset+iEdgeInset, m_iDY-iMaxRad-iEdgeInset-iInset-2);
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyDirectColor(
    int                         iRad,
    int                         iNShading,
    int                         iEdgeInset,
    COLORREF                    rgbT,
    COLORREF                    rgbB,
    int                         iFlags )

/*========================================================================*/
{
    g_iHints = iFlags;

    int iRT = GetRValue(rgbT);
    int iGT = GetGValue(rgbT);
    int iBT = GetBValue(rgbT);
    int iRB = GetRValue(rgbB);
    int iGB = GetGValue(rgbB);
    int iBB = GetBValue(rgbB);

    #ifdef HL_PIXEL32
        m_iRBT = iRT | (iBT << 16);
        m_iGT  = iGT << 8;
        m_iRBB = iRB | (iBB << 16);
        m_iGB  = iGB << 8;
    #else
        m_iRBT = ((iRT << 8) & 0xF800) | (iBT >> 3);
        m_iGT  = (iGT << 3) & 0x07E0;
        m_iRBB = ((iRB << 8) & 0xF800) | (iBB >> 3);
        m_iGB  = (iGB << 3) & 0x07E0;
    #endif

    START_TIMER(GDI_SHADEPOLY);

    if (!(m_iMode & SHADEDIB_DIRECT))
    {
        LOGASSERT(FALSE);
        return;
    }

    int iDX = ImageDX()-2*iEdgeInset;
    int iDY = DY()-2*iEdgeInset;

    iEdgeInset = __max(0, iEdgeInset);
    iEdgeInset = __min(iEdgeInset, m_iDX/2);
    iEdgeInset = __min(iEdgeInset, m_iDY/2);

    int iMaxRad = __min(iRad, iDX/2-1);
    iMaxRad     = __min(iMaxRad, iDY/2-1);

    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        if (iNShading >= iMaxRad)
        {
            CDXYRect rAll(0, 0, m_iImageDX, m_iDY);
            rAll.Inset(iEdgeInset, iEdgeInset);
            m_ROI.AddRectRef(&rAll);
            CHLRect rSolid = rAll;        
            rSolid.Inset(iNShading+1, iNShading+1);
            if (rSolid.DX() > 0 && rSolid.DY() > 0)
            {
                m_ROI.Subtract(&rSolid);
                m_Solid.AddRectRef(&rSolid);
            }
        }
        else
        {
            CHLRect rAll(0, 0, m_iImageDX, m_iDY);
            rAll.Inset(iEdgeInset, iEdgeInset);
            m_ROI.AddRectRef(&rAll);

            CDXYRect rHZ(iEdgeInset+iNShading, iEdgeInset+iMaxRad, m_iImageDX-2*(iNShading+iEdgeInset), m_iDY - 2 * (iEdgeInset+iMaxRad));
            if (rHZ.DX() > 0 && rHZ.DY() > 0)
            {
                m_ROI.Subtract(&rHZ);
                m_Solid.AddRectDirectRef(&rHZ);
            }

            int iDYSolid = iMaxRad-iNShading;

            CDXYRect rVT1(iEdgeInset+iMaxRad, iEdgeInset + iNShading, m_iImageDX-2*(iMaxRad+iEdgeInset), iDYSolid);
            m_ROI.Subtract(&rVT1);
            m_Solid.AddRectDirectRef(&rVT1);

            CDXYRect rVT2(iEdgeInset+iMaxRad, m_iDY-iEdgeInset-iNShading-iDYSolid, m_iImageDX-2*(iMaxRad+iEdgeInset), iDYSolid);
            m_ROI.Subtract(&rVT2);
            m_Solid.AddRectDirectRef(&rVT2);

        }
        m_ROI.Consolidate();
        m_Solid.Consolidate();
    }

    int iDCopy = iMaxRad+1;

    // Q1 is a mirror of Q2
    // Q4 is a mirror of Q3
    AddCornersQ23(iMaxRad, iNShading, iEdgeInset, iEdgeInset, m_iDY-iEdgeInset-1, INIT_RAISED_INT_GRADIENT);
    FlipHzCopy(iEdgeInset, iEdgeInset, m_iImageDX-iEdgeInset-iDCopy, iEdgeInset, iDCopy, iDCopy);
    FlipHzCopy(iEdgeInset, m_iDY-iEdgeInset-iDCopy, m_iImageDX-iEdgeInset-iDCopy, m_iDY-iEdgeInset-iDCopy, iDCopy, iDCopy);

    if (iMaxRad < iNShading)
    {
        CDXYRect rFill(0, 0, m_iImageDX, m_iDY);
        rFill.Inset(iMaxRad+iEdgeInset, iMaxRad+iEdgeInset);
        FillGradient(rFill, iEdgeInset);
    }
    else
    {
        CDXYRect rFill(0, 0, m_iImageDX, m_iDY);
        rFill.Inset(iEdgeInset, iEdgeInset);
        CHLRect rT = rFill.BreakOffTop(iMaxRad+1, 0);
        CHLRect rB = rFill.BreakOffBottom(iMaxRad+1, 0);
        rT.BreakOffTop(iNShading, 0);        
        rB.BreakOffBottom(iNShading, 0);        
        rT.Inset(iMaxRad+1, 0);
        rB.Inset(iMaxRad+1, 0);
        rFill.Inset(iNShading, 0);
        FillGradient(rT, iEdgeInset);
        FillGradient(rFill, iEdgeInset);
        FillGradient(rB, iEdgeInset);
    }

    int iShine = MaxShineInt(iNShading);
    int iMaxElev16 = (iNShading * 16);
   
    ShaderFunc *pQ12 = ShadeValueQ12_ELEVDirectColorGrad;
    ShaderFunc *pQ34 = ShadeValueQ34_ELEVDirectColorGrad;

    HLT_PIXEL_VARS vars;
    vars.m_iShine       = iShine;
    vars.m_iMaxElev16   = iMaxElev16;

    for (int iEdge = 0; iEdge <= iNShading; iEdge++)
    {
        int iInset = 0;
        if (iEdge > iMaxRad)
            iInset = iEdge - iMaxRad;

        int iX0 = iMaxRad+iInset+iEdgeInset+1;
        int iY0 = iMaxRad+iInset+iEdgeInset+1;

        vars.m_wColor12 = GradientAt(iEdge+iEdgeInset, iEdgeInset);
        vars.m_wColor34 = GradientAt(m_iDY-iEdge-1-iEdgeInset, iEdgeInset);

        HL_PIXEL *pT = Pixel(iX0, iEdge+iEdgeInset);
        HL_PIXEL *pB = Pixel(iX0, m_iDY-iEdge-1-iEdgeInset);

        int iD_16 = ((iEdge+1)<<4)-8;
        vars.m_iElev_16 = iD_16;
        vars.m_iDXC = 0;

        vars.m_iDYC = -1;
        pQ12(&vars, pT);
        vars.m_iDYC = 1;
        pQ34(&vars, pB);

        FillHZ(*pT, iX0, m_iImageDX-iMaxRad-iEdgeInset-iInset-1, iEdge+iEdgeInset);
        FillHZ(*pB, iX0, m_iImageDX-iMaxRad-iEdgeInset-iInset-1, m_iDY-iEdge-1-iEdgeInset);


        int iYMin = iY0;
        int iYMax = m_iDY-iMaxRad-iEdgeInset-iInset-1;
        vars.m_iDYC = 0;

        vars.m_iDYC = 0;
        for (int iYFill = iYMin; iYFill < iYMax; iYFill++) 
        {
            HL_PIXEL *pL = Pixel(iEdge+iEdgeInset,              iYFill);
            HL_PIXEL *pR = Pixel(m_iImageDX-iEdgeInset-iEdge-1, iYFill);

            vars.m_wColor12 = GradientAt(iYFill, iEdgeInset);
            vars.m_wColor34 = vars.m_wColor12;
            vars.m_iDXC = -1;
            pQ34(&vars, pL);
            vars.m_iDXC = 1;
            pQ34(&vars, pR);
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::CorrectRounding(
    CShadeDIB                   *pShaderOut )

/*========================================================================*/
{
    if (pShaderOut->ImageDX() != ImageDX() || pShaderOut->DY() != DY())
        return;

    START_TIMER(GDI_SHADEPOLY);

    for (int iY = 1; iY < DY()-1; iY++)
    {
        HL_PIXEL *pIn1 = Pixel(1, iY);
        HL_PIXEL *pIn2 = pShaderOut->Pixel(1, iY);

        for (int iX = 1; iX < m_iImageDX-1; iX++)
        {
            if (*pIn1 == 0x0000 && *pIn2 == 0x0000)
            {
                int iNValid = 0;
                DWORD dwElevSum = 0;
                DWORD dwValSum  = 0;
                HL_PIXEL wL = *(pIn1-1);
                HL_PIXEL wR = *(pIn1+1);
                HL_PIXEL wU = *(pIn1-m_iDX);
                HL_PIXEL wD = *(pIn1+m_iDX);
                if (wL != 0)   { dwElevSum += wL & 0x00FF;  dwValSum += wL & 0xFF00; iNValid++;  }
                if (wR != 0)   { dwElevSum += wR & 0x00FF;  dwValSum += wR & 0xFF00; iNValid++;  }
                if (wU != 0)   { dwElevSum += wU & 0x00FF;  dwValSum += wU & 0xFF00; iNValid++;  }
                if (wD != 0)   { dwElevSum += wD & 0x00FF;  dwValSum += wD & 0xFF00; iNValid++;  }
                
                if (iNValid > 0)
                {
                    DWORD dwElev = dwElevSum / iNValid;
                    DWORD dwVal  = (dwValSum / iNValid) & 0xFF00;
                    *pIn1 = (HL_PIXEL)(dwElev | dwVal);
                }
            }
            pIn2++;
            pIn1++;
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::CorrectSubPixelAlpha(
    CShadeDIB                   *pOther,
    int                         iXDst,
    int                         iYDst )

/*========================================================================*/
{
    int iDX = ImageDX();
    int iDY = DY();
    int iXSrc = 0;
    int iYSrc = 0;

    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pOther))
        return;

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pThisPIX = Pixel(iXDst, iYDst+iY);
        HL_PIXEL *pOtherPIX = pOther->Pixel(iXSrc, iYSrc+iY);

        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wTgt = *pThisPIX;
            int iAlpha16 = wTgt & 0x00FF;
            if (iAlpha16 > 0 && iAlpha16 < 16)
            {
                HL_PIXEL wSrc = *pOtherPIX;
                int iAlphaOther = __min(16, wSrc & 0x00FF);
                WORD wThisElev = (WORD)(16 - iAlphaOther);
                *pThisPIX = (wTgt & 0xFF00) | wThisElev;
            }

            pThisPIX++;
            pOtherPIX++;            
        }        
    }
}
/*========================================================================*/

    void                        CShadeDIB::InitWedgeDX(
    int                         iDYL,
    int                         iDYR,
    int                         iNShading,
    int                         iNShadeOutOffset,
    BOOL                        bRaised )

/*========================================================================*/
{
    START_TIMER(GDI_SHADEPOLY);
    int iDYDX100 = abs(iDYL-iDYR)/2 * 100 / m_iImageDX;
/*
    if (bRaised && iNShadeOutOffset > 0)
    {
        int iNShadeAspectOut = iNShadeOutOffset * 100 / iDYDX100;
        int iNShadeOutThis = iNShadeAspectOut;
        iNShadeOutThis = __max(1, iNShadeOutThis);

        iDYL -= 2*iNShadeOutThis;
        iDYR -= 2*iNShadeOutThis;
    }
*/
    int iDXEdge = m_iImageDX;
    int iDYEdge = abs(iDYL - iDYR)/2;
    int iEdgeStep = iDXEdge * 16 / iSqrt(iDXEdge*iDXEdge+iDYEdge*iDYEdge);

//    int iEdgeStep = 16 * iDYDX100 / 100;
    int iDY = iDYR-iDYL;
    int iDYT = -100;
    int iDYB = 100;
    int iDXT = iDYDX100;
    int iQT = 1;
    int iQB = 4;

    HL_PIXEL wInnerColor = 0x00FF;
    HL_PIXEL wOuterColor = 0x0000;

    if (bRaised)
    {
        // Raised sec
        if (iDYR > iDYL)
        {
            iDXT = -iDXT;
            iQT = 2;
            iQB = 3;
        }
    }
    else
    {
        wInnerColor = 0x0000;
        wOuterColor = 0x00FF;

        // Sunken sec
        iQT = 3;
        iQB = 2;

        if (iDYR > iDYL)
        {
            iQT = 4;
            iQB = 1;
        }
        else
        {
            iDXT = -iDXT;
        }
    }

    int iYMid32 = m_iDY * 16;

    ShaderFunc *pShaderT = ShaderFor(iQT, INIT_RAISED_INT);
    ShaderFunc *pShaderB = ShaderFor(iQB, INIT_RAISED_INT);

    for (int iX = 0; iX < m_iImageDX; iX++)
    {
        int iDYThis16 = iDYL * 16 + iX * iDY * 16 / m_iImageDX;

        HL_PIXEL *pPIXT = Pixel(iX, 0);
        HL_PIXEL *pPIXB = Pixel(iX, m_iDY-1);

        if (iDYThis16 < 8)
        {
            for (int iY = 0; iY < m_iDY; iY++)
            {
                *pPIXT = wOuterColor;
                pPIXT += m_iDX;
            }
        }
        else
        {
            int iY0_32  = iYMid32 - iDYThis16;
            int iY0     = (iY0_32 >> 5);
            int iY1     = DY()-iY0-1;

            pPIXT = Pixel(iX, iY0);
            pPIXB = Pixel(iX, iY1);

            int iYError16 = ((iY0 << 5)- iY0_32 ) >> 1;
           
            if (bRaised)
            {
                int iShine = MaxShineInt(iNShading);
                HLT_PIXEL_VARS vars;
                vars.m_iShine = iShine;
                vars.m_iMaxElev16 = __max(16, iNShading<<4);
                vars.m_iDXC = iDXT;

                vars.m_iElev_16 = 16+iYError16-16*iNShadeOutOffset;

                while (vars.m_iElev_16 < vars.m_iMaxElev16)
                {
                    vars.m_iDYC = iDYT;
                    pShaderT(&vars, pPIXT);
                    vars.m_iDYC = iDYB;
                    pShaderB(&vars, pPIXB);
                    pPIXT += m_iDX;
                    pPIXB -= m_iDX;
                    vars.m_iElev_16 += iEdgeStep;
                }
            }
            else
            {
                int iYFill = 0;
                HL_PIXEL *pFillT = Pixel(iX, 0);
                for (iYFill = 0; iYFill < iY0; iYFill++)
                {
                    *pFillT  = 0x00FF;
                    pFillT += m_iDX;
                }
                HL_PIXEL *pFillB = Pixel(iX, m_iDY-1);
                for (iYFill = m_iDY-1; iYFill > iY1; iYFill--)
                {
                    *pFillB  = 0x00FF;
                    pFillB -= m_iDX;
                }

                HLT_PIXEL_VARS vars;
                vars.m_iShine = SHINE_EXT;
                vars.m_iMaxElev16 = (iNShading<<4);
                vars.m_iDXC = iDXT;
                vars.m_iElev_16 = iNShading*16-iYError16;

                while (vars.m_iElev_16 > 0)
                {
//                    vars.m_iElev_16 = iShade*16+iYError16;

                    vars.m_iDYC = -iDYT;
                    pShaderT(&vars, pPIXT);
                    vars.m_iDYC = -iDYB;
                    pShaderB(&vars, pPIXB);
                    pPIXT += m_iDX;
                    pPIXB -= m_iDX;
                    vars.m_iElev_16 -= iEdgeStep;
                }
            }

            while (pPIXT <= pPIXB)
            {
                *pPIXT = wInnerColor;
                pPIXT += m_iDX;
            }
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::InitWedgeDY(
    int                         iDXT,
    int                         iDXB,
    int                         iNShading,
    int                         iNShadeOutOffset,
    BOOL                        bRaised )

/*========================================================================*/
{
    START_TIMER(GDI_SHADEPOLY);
/*
    if (bRaised && iNShadeOutOffset > 0)
    {
        int iDXDY100 = abs(iDXT-iDXB)/2 * 100 / DY();
        int iNShadeAspectOut = iNShadeOutOffset * iDXDY100 / 100;
        int iNShadeOutThis = iNShadeAspectOut;
        iNShadeOutThis = __max(1, iNShadeOutThis);
        iDXT -= 2*iNShadeOutThis;
        iDXB -= 2*iNShadeOutThis;
    }
*/
    int iDX = iDXB-iDXT;
    int iDXDY100 = abs(iDXT-iDXB)/2 * 100 / DY();


    int iDXEdge = abs(iDXT - iDXB)/2;
    int iDYEdge = m_iDY;
    int iEdgeStep = iDYEdge * 16 / iSqrt(iDXEdge*iDXEdge+iDYEdge*iDYEdge);

//    int iEdgeStep = 16 * 100 / iDXDY100;

    int iDXCL = 100;
    int iDXCR = 100;
    int iDYT =  -iDXDY100;

//    int iMaxElev2 = iNShading * iNShading;
    int iQL = 3;
    int iQR = 4;

    HL_PIXEL wInnerColor = 0x00FF;
    HL_PIXEL wOuterColor = 0x0000;

    if (bRaised)
    {
        if (iDXB > iDXT)
        {
//            iDYT = -iDYT;
            iQL = 2;
            iQR = 1;
        }
    }
    else
    {
        wInnerColor = 0x0000;
        wOuterColor = 0x00FF;
        iQL = 1;
        iQR = 2;
        if (iDXB > iDXT)
        {
            iQL = 4;
            iQR = 3;
            iDYT = -iDYT;
        }
    }

    ShaderFunc *pShaderL = ShaderFor(iQL, INIT_RAISED_INT);
    ShaderFunc *pShaderR = ShaderFor(iQR, INIT_RAISED_INT);

    int iXMid16 = m_iImageDX * 8;
    for (int iY = 0; iY < m_iDY; iY++)
    {
        int iDXThis16 = iDXT * 16 + iY * iDX * 16 / DY();
        HL_PIXEL *pPIXL = Pixel(0, iY);
        HL_PIXEL *pPIXR = Pixel(m_iImageDX-1, iY);

        if (iDXThis16 < 8)
        {
            for (int i = 0; i < m_iImageDX; i++)
            {
                *pPIXL = wOuterColor;
                pPIXL++;
            }            
        }
        else
        {
            int iX0_16 = iXMid16 - iDXThis16 / 2;
            int iX0 = (iX0_16 >> 4);
            int iX1 = m_iImageDX-iX0-1;
            int iXError16 = (iX0 << 4)- iX0_16;

            pPIXL = Pixel(iX0, iY);
            pPIXR = Pixel(iX1, iY);
    
            if (bRaised)
            {
                HLT_PIXEL_VARS vars;
                vars.m_iShine = MaxShineInt(iNShading);
                vars.m_iMaxElev16 = __max(16, iNShading<<4);
                vars.m_iDYC = iDYT;

                vars.m_iElev_16 = 16+iXError16-(16*iNShadeOutOffset);
                while (vars.m_iElev_16 < vars.m_iMaxElev16)
                {
                    vars.m_iDXC = iDXCL;
                    pShaderL(&vars, pPIXL);
                    vars.m_iDXC = iDXCR;
                    pShaderR(&vars, pPIXR);
                    pPIXL++;
                    pPIXR--;
                    vars.m_iElev_16 += iEdgeStep;
                }
            }
            else
            {
                HL_PIXEL *pLFill = Pixel(0, iY);
                HL_PIXEL *pRFill = Pixel(m_iImageDX-1, iY);
                int iXFill = 0;
                for (iXFill = 0; iXFill < iX0; iXFill++)
                {
                    *pLFill = 0x00FF;
                    pLFill++;
                }
                for (iXFill = m_iImageDX-1; iXFill > iX1; iXFill--)
                {
                    *pRFill = 0x00FF;
                    pRFill--;
                }

                HLT_PIXEL_VARS vars;
                vars.m_iShine = SHINE_EXT;
                vars.m_iMaxElev16 = (iNShading<<4);
                vars.m_iDYC = iDYT;
                vars.m_iElev_16 = iNShading*16-iXError16;

                while (vars.m_iElev_16 > 0)
                {
                    vars.m_iDXC = iDXCL;
                    pShaderL(&vars, pPIXL);
                    vars.m_iDXC = iDXCR;
                    pShaderR(&vars, pPIXR);
                    pPIXL++;
                    pPIXR--;
                    vars.m_iElev_16 -= iEdgeStep;
                }
            }

            while (pPIXL <= pPIXR)
            {
                *pPIXL = wInnerColor;
                pPIXL++;
            }
        }
    }
    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::SetFillColor(
    COLORREF                    rgb )

/*========================================================================*/
{
    if (rgb == g_FillRGB)
        return;

#ifdef HL_PIXEL32
    g_wFillRGB = (RGBTOHLPIXEL(rgb) >> 8);
    g_wRsolid = g_wFillRGB & 0x000000FF;
    g_wGsolid = g_wFillRGB & 0x0000FF00;
    g_wBsolid = g_wFillRGB & 0x00FF0000;

    int wR = 0;
    int wG = 0;
    int wB = 0;

    for (int i = 0; i < 256; i++)
    {
        int iAdder = i - 128;
        if (iAdder < 0)
        {
            int iAddG     = -iAdder << 8;
            int iAddR     = iAddG >> 8;
            int iAddB     = iAddG << 8;
            if (g_wRsolid <= iAddR)    wR = 0; else  { wR = g_wRsolid - iAddR; wR &= 0x000000FF;   }
            if (g_wGsolid <= iAddG)    wG = 0; else  { wG = g_wGsolid - iAddG; wG &= 0x0000FF00;   }
            if (g_wBsolid <= iAddB)    wB = 0; else  { wB = g_wBsolid - iAddB; wB &= 0x00FF0000;   }
        }
        else
        {   
            int iAddG     = iAdder << 8;
            int iAddR     = iAddG >> 8;
            int iAddB     = iAddG << 8;
            wR = g_wRsolid+iAddR;
            wG = g_wGsolid+iAddG;
            wB = g_wBsolid+iAddB;
            if (wR > 0x000000FF)    wR = 0x000000FF; else wR &= 0x000000FF;
            if (wG > 0x0000FF00)    wG = 0x0000FF00; else wG &= 0x0000FF00;
            if (wB > 0x00FF0000)    wB = 0x00FF0000; else wB &= 0x00FF0000;
        }
        g_FillMap[i] = ((wR|wG|wB) << 8 ) | 0xFF;
    }

    g_FillRGB = rgb;
    g_wFillRGB = (RGBTOHLPIXEL(rgb)) | 0xFF;
#else
    g_wFillRGB = RGBTOHLPIXEL(rgb);
    g_wRsolid = g_wFillRGB & 0x0000F800;
    g_wGsolid = g_wFillRGB & 0x000007E0;
    g_wBsolid = g_wFillRGB & 0x0000001F;

    int wR = 0;
    int wG = 0;
    int wB = 0;

    for (int i = 0; i < 256; i++)
    {
        int iAdder = i - 128;
        if (iAdder < 0)
        {
            int iAddG     = -iAdder << 3;
            int iAddB     = iAddG >> 6;
            int iAddR     = iAddB << 11;
            if (g_wRsolid <= iAddR)    wR = 0; else  { wR = g_wRsolid - iAddR; wR &= 0x0000F800;   }
            if (g_wGsolid <= iAddG)    wG = 0; else  { wG = g_wGsolid - iAddG; wG &= 0x000007E0;   }
            if (g_wBsolid <= iAddB)    wB = 0; else    wB = g_wBsolid - iAddB;
        }
        else
        {   
            int iAddG     = iAdder << 3;
            int iAddB     = iAddG  >> 6;
            int iAddR     = iAddB  << 11;
            wR = g_wRsolid+iAddR;
            wG = g_wGsolid+iAddG;
            wB = g_wBsolid+iAddB;
            if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
            if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
            if (wB > 0x0000001F)    wB = 0x0000001F;
        }
        g_FillMap[i] = (wR|wG|wB);
    }

    g_FillRGB = rgb;
#endif
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ12_ELEV(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
    {
        *pPIX = 0;
        return;
    }

    int iPixelMask = 0;
    int iMaxElev16 = pVars->m_iMaxElev16;
    if (iPixelElev16 >= 16)
        iPixelMask = __min(254, 16 + (iPixelElev16 >> 4));
    else
        iPixelMask = iPixelElev16;

/*
    if (iMaxElev16 <= 0)
    {
        *pPIX = (HL_PIXEL)((100<<8)|iPixelMask);
        return;
    }
*/
    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = (HL_PIXEL)(100<<8)|iPixelMask;
        return;
    }

    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = (128<<8)|iPixelMask;
        return;
    }

    // Figure out alpha value based on elevation
    // elevations less than 16 are sub-pixels, 
    // elevations above iMaxElev are also sub-pixels
    // elevations a full pixel above iMaxElev are on the surface
    int iAlpha = 255;
    if (g_iHints & SHADEDIB_DITHER_LOW_EDGE)
    {
        if (iPixelElev16 < 16)
            iAlpha = iPixelElev16 << 4;
    }
    if (g_iHints & SHADEDIB_DITHER_HIGH_EDGE)
    {
        if (iPixelElev16 > iMaxElev16)
            iAlpha = 255 - (((iPixelElev16-iMaxElev16) * 255) >> 4);
    }

    // 
    // For small elevations, minimize the alpha value here if needed, some pixels may hit exact
    // and others not so much depending on where they land relative to iMaxElev16, this looks crappy
    //
    LOGASSERT(iAlpha >= 0 && iAlpha <= 255);

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            // Inverse elevation from 0-32
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }


    //  
    // Figure out the spot factor from the iDX, iDY vector given,
    //

    int iSpot = 0;
    //
    // Rotate the vector by 45 deg so that x=0, y=1 becomes x=.7 y=.7
    //
    int iDXVect = iDX + iDY;
    int iDYVect = iDY - iDX;

    int iShine = pVars->m_iShine;
    if (iDXVect < iDYVect && iDXVect != 0)
    {
        iSpot = iShine + ((iShine * iDYVect ) / iDXVect);
    }
    else if (iDYVect != 0)
    {
        iSpot = iShine + ((iShine * iDXVect) / iDYVect);
    }

//    if (iMaxElev16 > 16)
    {
        // Our Max elevation is more than 1 pixel, calculate the spot ratio
        // from ambient, essentially spot value maxes out at 45 degress of elevation
        int iDR256 = 128 - iAmbient;
        int iVRatio = 0x10000 - 4 * (iDR256 * iDR256);
        iSpot = (iSpot * iVRatio) >> 16;
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient + iSpot);

    if (iAlpha != 0xFF && g_iHints & (SHADEDIB_DITHER_HIGH_EDGE|SHADEDIB_DITHER_LOW_EDGE))
    {
        iVal = (iVal * iAlpha + ((0xFF-iAlpha)<<7)) & 0xFF00;
        *pPIX = (HL_PIXEL)(iVal|iPixelMask);
        return;
    }   

    *pPIX = (HL_PIXEL)(iVal << 8)|iPixelMask;
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ34_ELEV(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
    {
        *pPIX = 0;
        return;
    }

    // Pixels from 0-15 get pixel mask of 0-15
    // Pixels from 16 + get mask of 16+
    int iPixelMask = 0;
    if (iPixelElev16 >= 16)
        iPixelMask = __min(254, 16 + (iPixelElev16 >> 4));
    else
        iPixelMask = iPixelElev16;

    int iMaxElev16 = pVars->m_iMaxElev16;
    
    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = (HL_PIXEL)(100<<8)|iPixelMask;
        return;
    }
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = (128<<8)|iPixelMask;
        return;
    }

    // Figure out alpha value based on elevation
    // elevations less than 16 are sub-pixels, 
    // elevations above iMaxElev are also sub-pixels
    // elevations a full pixel above iMaxElev are on the surface
    int iAlpha = 255;
    if (g_iHints & SHADEDIB_DITHER_LOW_EDGE)
    {
        if (iPixelElev16 < 16 && (g_iHints & SHADEDIB_HINT_OVERSCAN) == 0)
            iAlpha = iPixelElev16 << 4;
    }
    if (g_iHints & SHADEDIB_DITHER_HIGH_EDGE)
    {
        if (iPixelElev16 > iMaxElev16)
            iAlpha = 255 - (((iPixelElev16-iMaxElev16) * 255) >> 4);
    }

    // 
    // For small elevations, minimize the alpha value here if needed, some pixels may hit exact
    // and others not so much depending on where they land relative to iMaxElev16, this looks crappy
    //
    LOGASSERT(iAlpha >= 0 && iAlpha <= 255);

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient);

    if (g_iHints & (SHADEDIB_DITHER_HIGH_EDGE|SHADEDIB_DITHER_LOW_EDGE))
    {
        if (iAlpha != 0xFF)
        {
            iVal = (iVal * iAlpha + ((0xFF-iAlpha)<<7)) & 0xFF00;
            *pPIX = (HL_PIXEL)(iVal|iPixelMask);
            return;
        }   
    }

    *pPIX = (HL_PIXEL)(iVal << 8)|iPixelMask;
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ12_ELEVDirect(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
        return;

    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = g_wFillRGB;
        return;
    }

    int iMaxElev16 = pVars->m_iMaxElev16;
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = g_wFillRGB;
        return;
    }

    // Figure out alpha value based on elevation
    // elevations less than 16 are sub-pixels, 
    // elevations above iMaxElev are also sub-pixels
    // elevations a full pixel above iMaxElev are on the surface
    int iAlpha = 16;
    if (iPixelElev16 < 16)
        iAlpha = iPixelElev16;

    if (iPixelElev16 > iMaxElev16)
        iAlpha = 16  -(iPixelElev16-iMaxElev16);

    // 
    // For small elevations, minimize the alpha value here if needed, some pixels may hit exact
    // and others not so much depending on where they land relative to iMaxElev16, this looks crappy
    //
    LOGASSERT(iAlpha >= 0 && iAlpha <= 255);

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }


    //  
    // Figure out the spot factor from the iDX, iDY vector given,
    //

    int iSpot = 0;
    //
    // Rotate the vector by 45 deg so that x=0, y=1 becomes x=.7 y=.7
    //
    int iDXVect = iDX + iDY;
    int iDYVect = iDY - iDX;
    int iShine = pVars->m_iShine;

    if (iDXVect < iDYVect && iDXVect != 0)
    {
        iSpot = iShine + ((iShine * iDYVect ) / iDXVect);
    }
    else if (iDYVect != 0)
    {
        iSpot = iShine + ((iShine * iDXVect) / iDYVect);
    }

    if (iMaxElev16 > 16)
    {
        // Our Max elevation is more than 1 pixel, calculate the spot ratio
        // from ambient, essentially spot value maxes out at 45 degress of elevation
        int iDR256 = 128 - iAmbient;
        int iVRatio = 0x10000 - 4 * (iDR256 * iDR256);
        iSpot = (iSpot * iVRatio) >> 16;
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient + iSpot);
    iVal = __max(0, iVal);

#ifndef IPHONE
    if (iAlpha != 16)
    {
        iVal = (iVal * iAlpha + (16-iAlpha) * 128) >> 4;
    }   
#endif

    int iAdder = iVal - 128;

#ifdef IPHONE
    HL_PIXEL wResult;
    if (iAdder > 0)
    {
        iAlpha = ((iAlpha * iAdder) >> 4);
        wResult = (iAlpha << 24)|(iAlpha << 16)|(iAlpha <<8)|iAlpha;
        *pPIX = wResult;
    }
    else
    {
        iAlpha = ((iAlpha * -iAdder) >> 4);
        wResult = iAlpha;
        *pPIX = wResult;
    }
    return;
#else
    HL_PIXEL wTgt = *pPIX;

    int wR = wTgt & 0x0000F800;
    int wG = wTgt & 0x000007E0;
    int wB = wTgt & 0x0000001F;

    if (iAdder < 0)
    {
        int iAddG     = -iAdder << 3;
        int iAddB     = iAddG >> 6;;
        int iAddR     = iAddB << 11;
        if (wR <= iAddR)    wR = 0; else  { wR = wR - iAddR; wR &= 0x0000F800;   }
        if (wG <= iAddG)    wG = 0; else  { wG = wG - iAddG; wG &= 0x000007E0;   }
        if (wB <= iAddB)    wB = 0; else  wB = wB - iAddB;
    }
    else
    {   
        int iAddG     = iAdder << 3;
        int iAddB     = iAddG  >> 6;;
        int iAddR     = iAddB  << 11;
        wR += iAddR;
        if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
        wG += iAddG;
        if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
        wB += iAddB;
        if (wB > 0x0000001F)    wB = 0x0000001F;
    }

    *pPIX = (HL_PIXEL)(wR|wG|wB);
#endif
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ34_ELEVDirect(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
        return;

    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = g_wFillRGB;
        return;
    }

    int iMaxElev16 = pVars->m_iMaxElev16;    
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = g_wFillRGB;
        return;
    }

    int iAlpha = 16;
    if (iPixelElev16 < 16)
        iAlpha = iPixelElev16;
    if (iPixelElev16 > iMaxElev16)
        iAlpha = 16 - (iPixelElev16-iMaxElev16);

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient);
    iVal = __max(0x00, iAmbient);

#ifndef IPHONE
    if (iAlpha != 16)
    {
        iVal = (iVal * iAlpha + (16-iAlpha) * 128) >> 4;
    }   
#endif

    int iAdder = iVal - 128;

#ifdef IPHONE
    HL_PIXEL wResult;
    if (iAdder > 0)
    {
        iAlpha = ((iAlpha * iAdder) >> 4);
        wResult = (iAlpha << 24)|(iAlpha << 16)|(iAlpha <<8)|iAlpha;
        *pPIX = wResult;
    }
    else
    {
        iAlpha = ((iAlpha * -iAdder) >> 4);
        wResult = iAlpha;
        *pPIX = wResult;
    }
    return;
#else
    HL_PIXEL wTgt = *pPIX;

    int wR = wTgt & 0x0000F800;
    int wG = wTgt & 0x000007E0;
    int wB = wTgt & 0x0000001F;

    if (iAdder < 0)
    {
        int iAddG     = -iAdder << 3;
        int iAddB     = iAddG >> 6;;
        int iAddR     = iAddB << 11;
        if (wR <= iAddR)    wR = 0; else  { wR = wR - iAddR; wR &= 0x0000F800;   }
        if (wG <= iAddG)    wG = 0; else  { wG = wG - iAddG; wG &= 0x000007E0;   }
        if (wB <= iAddB)    wB = 0; else  wB = wB - iAddB;
    }
    else
    {   
        int iAddG     = iAdder << 3;
        int iAddB     = iAddG  >> 6;;
        int iAddR     = iAddB  << 11;
        wR += iAddR;
        if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
        wG += iAddG;
        if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
        wB += iAddB;
        if (wB > 0x0000001F)    wB = 0x0000001F;
    }

    *pPIX = (HL_PIXEL)(wR|wG|wB);
#endif
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ12_ELEVDirectColor(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
        return;

    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = g_wFillRGB;
        return;
    }

    int iMaxElev16 = pVars->m_iMaxElev16;
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = g_wFillRGB;
        return;
    }

    // Figure out alpha value based on elevation
    // elevations less than 16 are sub-pixels, 
    // elevations above iMaxElev are also sub-pixels
    // elevations a full pixel above iMaxElev are on the surface
    int iAlpha = 16;
    if (iPixelElev16 < 16)
        iAlpha = iPixelElev16;

    // 
    // For small elevations, minimize the alpha value here if needed, some pixels may hit exact
    // and others not so much depending on where they land relative to iMaxElev16, this looks crappy
    //
    LOGASSERT(iAlpha >= 0 && iAlpha <= 255);

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }


    //  
    // Figure out the spot factor from the iDX, iDY vector given,
    //

    int iSpot = 0;
    //
    // Rotate the vector by 45 deg so that x=0, y=1 becomes x=.7 y=.7
    //
    int iDXVect = iDX + iDY;
    int iDYVect = iDY - iDX;
    int iShine = pVars->m_iShine;

    if (iDXVect < iDYVect && iDXVect != 0)
    {
        iSpot = iShine + ((iShine * iDYVect ) / iDXVect);
    }
    else if (iDYVect != 0)
    {
        iSpot = iShine + ((iShine * iDXVect) / iDYVect);
    }

    if (iMaxElev16 > 16)
    {
        // Our Max elevation is more than 1 pixel, calculate the spot ratio
        // from ambient, essentially spot value maxes out at 45 degress of elevation
        int iDR256 = 128 - iAmbient;
        int iVRatio = 0x10000 - 4 * (iDR256 * iDR256);
        iSpot = (iSpot * iVRatio) >> 16;
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient + iSpot);
    iVal = __max(0, iVal);
    if (iAlpha != 16)
    {
#ifdef IPHONE
        // For the phone, RGB are premult
        HL_PIXEL wResult = (g_FillMap[iVal] >> 8);
        int iMult = (iAlpha << 4);
        int iR = (((wResult & 0x000000FF) * iAlpha) & 0x0000FF00);
        int iG = (((wResult & 0x0000FF00) * iAlpha) & 0x00FF0000);
        int iB = (((wResult & 0x00FF0000) * iAlpha) & 0xFF000000);
        wResult = (iR|iG|iB|iMult);
        *pPIX = wResult;
        return;
#else
        HL_PIXEL wTgt = *pPIX;
        int iRBG = wTgt & 0x0000F800;
        int iGBG = wTgt & 0x000007E0;
        int iBBG = wTgt & 0x0000001F;
        
        int _iAlpha = 16-iAlpha;
        int wR = 0;
        int wG = 0;
        int wB = 0;

        int iAdder = iVal - 128;
        if (iAdder < 0)
        {
            int iAddG     = -iAdder << 3;
            int iAddB     = iAddG >> 6;;
            int iAddR     = iAddB << 11;
            if (g_wRsolid <= iAddR)    wR = 0; else  { wR = g_wRsolid - iAddR; wR &= 0x0000F800;   }
            if (g_wGsolid <= iAddG)    wG = 0; else  { wG = g_wGsolid - iAddG; wG &= 0x000007E0;   }
            if (g_wBsolid <= iAddB)    wB = 0; else    wB = g_wBsolid - iAddB;
        }
        else
        {   
            int iAddG     = iAdder << 3;
            int iAddB     = iAddG  >> 6;;
            int iAddR     = iAddB  << 11;
            wR = g_wRsolid+iAddR;
            wG = g_wGsolid+iAddG;
            wB = g_wBsolid+iAddB;
            if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
            if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
            if (wB > 0x0000001F)    wB = 0x0000001F;
        }

        wR = ((wR * iAlpha + iRBG * _iAlpha) >> 4) & 0xF800;
        wG = ((wG * iAlpha + iGBG * _iAlpha) >> 4) & 0x07E0;
        wB = ((wB * iAlpha + iBBG * _iAlpha) >> 4);

        *pPIX = (HL_PIXEL)(wR|wG|wB);
        return;
#endif
    }   

    *pPIX = g_FillMap[iVal];
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ34_ELEVDirectColor(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
        return;

    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = g_wFillRGB;
        return;
    }

    int iMaxElev16 = pVars->m_iMaxElev16;
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = g_wFillRGB;
        return;
    }

    int iAlpha = 16;
    if (iPixelElev16 < 16)
        iAlpha = iPixelElev16;
    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient);
    iVal = __max(0x00, iAmbient);

    if (iAlpha != 16)
    {
#ifdef IPHONE
        // For the phone, RGB are premult
        HL_PIXEL wResult = (g_FillMap[iVal] >> 8);
        int iMult = (iAlpha << 4);
        int iR = (((wResult & 0x000000FF) * iAlpha) & 0x0000FF00);
        int iG = (((wResult & 0x0000FF00) * iAlpha) & 0x00FF0000);
        int iB = (((wResult & 0x00FF0000) * iAlpha) & 0xFF000000);
        wResult = (iR|iG|iB|iMult);
        *pPIX = wResult;
        return;
#else
        int _iAlpha = 16-iAlpha;
        HL_PIXEL wTgt = *pPIX;
        int iRBG = wTgt & 0x0000F800;
        int iGBG = wTgt & 0x000007E0;
        int iBBG = wTgt & 0x0000001F;

        int wR = 0;
        int wG = 0;
        int wB = 0;

        int iAdder = iVal - 128;

        if (iAdder < 0)
        {
            int iAddG     = -iAdder << 3;
            int iAddB     = iAddG >> 6;;
            int iAddR     = iAddB << 11;
            if (g_wRsolid <= iAddR)    wR = 0; else  { wR = g_wRsolid - iAddR; wR &= 0x0000F800;   }
            if (g_wGsolid <= iAddG)    wG = 0; else  { wG = g_wGsolid - iAddG; wG &= 0x000007E0;   }
            if (g_wBsolid <= iAddB)    wB = 0; else    wB = g_wBsolid - iAddB;
        }
        else
        {   
            int iAddG     = iAdder << 3;
            int iAddB     = iAddG  >> 6;;
            int iAddR     = iAddB  << 11;
            wR = g_wRsolid+iAddR;
            wG = g_wGsolid+iAddG;
            wB = g_wBsolid+iAddB;
            if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
            if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
            if (wB > 0x0000001F)    wB = 0x0000001F;
        }

        wR = ((wR * iAlpha + iRBG * _iAlpha) >> 4) & 0xF800;
        wG = ((wG * iAlpha + iGBG * _iAlpha) >> 4) & 0x07E0;
        wB = ((wB * iAlpha + iBBG * _iAlpha) >> 4);

        *pPIX = (HL_PIXEL)(wR|wG|wB);
        return;
#endif
    }   

    *pPIX = g_FillMap[iVal];
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ12_ELEVDirectColorGrad(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
        return;

    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = pVars->m_wColor12;
        return;
    }

    int iMaxElev16 = pVars->m_iMaxElev16;
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = pVars->m_wColor12;
        return;
    }

    // Figure out alpha value based on elevation
    // elevations less than 16 are sub-pixels, 
    // elevations above iMaxElev are also sub-pixels
    // elevations a full pixel above iMaxElev are on the surface
    int iAlpha = 16;
    if (iPixelElev16 < 16)
        iAlpha = iPixelElev16;

    // 
    // For small elevations, minimize the alpha value here if needed, some pixels may hit exact
    // and others not so much depending on where they land relative to iMaxElev16, this looks crappy
    //
    LOGASSERT(iAlpha >= 0 && iAlpha <= 255);

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }


    //  
    // Figure out the spot factor from the iDX, iDY vector given,
    //

    int iSpot = 0;
    //
    // Rotate the vector by 45 deg so that x=0, y=1 becomes x=.7 y=.7
    //
    int iDXVect = iDX + iDY;
    int iDYVect = iDY - iDX;
    int iShine = pVars->m_iShine;

    if (iDXVect < iDYVect && iDXVect != 0)
    {
        iSpot = iShine + ((iShine * iDYVect ) / iDXVect);
    }
    else if (iDYVect != 0)
    {
        iSpot = iShine + ((iShine * iDXVect) / iDYVect);
    }

    if (iMaxElev16 > 16)
    {
        // Our Max elevation is more than 1 pixel, calculate the spot ratio
        // from ambient, essentially spot value maxes out at 45 degress of elevation
        int iDR256 = 128 - iAmbient;
        int iVRatio = 0x10000 - 4 * (iDR256 * iDR256);
        iSpot = (iSpot * iVRatio) >> 16;
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient + iSpot);
    iVal = __max(0, iVal);

#ifdef IPHONE
    DWORD dwRFill = pVars->m_wColor12 & 0x0000FF00;
    DWORD dwGFill = pVars->m_wColor12 & 0x00FF0000;
    DWORD dwBFill = pVars->m_wColor12 & 0xFF000000;
    DWORD dwRMax  = 0x0000FF00 - dwRFill;
    DWORD dwGMax  = 0x00FF0000 - dwGFill;
    DWORD dwBMax  = 0xFF000000 - dwBFill;
#else
    int dwRFill = pVars->m_wColor12 & 0xF800;
    int dwGFill = pVars->m_wColor12 & 0x07E0;
    int dwBFill = pVars->m_wColor12 & 0x001F;
#endif


#ifdef IPHONE
    DWORD wR = 0;
    DWORD wG = 0;
    DWORD wB = 0;

    if (iVal < 128)
    {
        wR = ((dwBFill >> 7) * iVal) & 0xFF000000;
        wG = ((dwGFill >> 7) * iVal) & 0x00FF0000;
        wB = ((dwRFill >> 7) * iVal) & 0x0000FF00;
    }
    else
    {   
        int iAdder = iVal - 128;
        DWORD  iAddR     = iAdder << 8;
        DWORD  iAddG     = iAddR << 8;
        DWORD  iAddB     = iAddG << 8;
        if (iAddR < dwRMax) { wR = ((iAddR + dwRFill) & 0x0000FF00);    } else { wR = 0x0000FF00;   }
        if (iAddG < dwGMax) { wG = ((iAddG + dwGFill) & 0x00FF0000);    } else { wG = 0x00FF0000;   }
        if (iAddB < dwBMax) { wB = ((iAddB + dwBFill) & 0xFF000000);    } else { wB = 0xFF000000;   }
    }

    DWORD dwRes = 0;
    if (iAlpha == 16)
    {
        dwRes = (wR|wG|wB|0xFF);
    }
    else
    {
        if (g_iHints & SHADEDIB_HINT_INT_OVER_EXT)
        {
            // Interior over exterior, blend with existing result is fully opaque
            DWORD wIn = *pPIX;
            wIn = wIn >> 8;
            int iAlpha256 = iAlpha << 4;
            int _iAlpha256 = 256-iAlpha256;
            DWORD dwRIn = wIn & 0x000000FF;
            DWORD dwGIn = wIn & 0x0000FF00;
            DWORD dwBIn = wIn & 0x00FF0000;
            DWORD dwROut = ((wR >> 8)* iAlpha256 + dwRIn * _iAlpha256) & 0x0000FF00;
            DWORD dwGOut = ((wG >> 8)* iAlpha256 + dwGIn * _iAlpha256) & 0x00FF0000;
            DWORD dwBOut = ((wB >> 8)* iAlpha256 + dwBIn * _iAlpha256) & 0xFF000000;
            dwRes = dwROut|dwGOut|dwBOut|iAlpha256;
        }
        else
        {
            // Must Pre Multiply on stupid iPhone
            int iAlpha256 = (iAlpha << 4);
            int iRB = (((wR|wB)>>8) * iAlpha256) & 0xFF00FF00;
            int iG  = (((wG)>>8) * iAlpha256) & 0x00FF0000;
            dwRes = (iRB|iG|iAlpha256);
        }
    }

    *pPIX = dwRes;
    return;
#else
    int _iAlpha = 16-iAlpha;
    int wR = 0;
    int wG = 0;
    int wB = 0;

    int iAdder = iVal - 128;
    if (iAdder < 0)
    {
        int iAddG     = -iAdder << 3;
        int iAddB     = iAddG >> 6;;
        int iAddR     = iAddB << 11;
        if (dwRFill <= iAddR)    wR = 0; else  { wR = dwRFill - iAddR; wR &= 0x0000F800;   }
        if (dwGFill <= iAddG)    wG = 0; else  { wG = dwGFill - iAddG; wG &= 0x000007E0;   }
        if (dwBFill <= iAddB)    wB = 0; else    wB = dwBFill- iAddB;
    }
    else
    {   
        int iAddG     = iAdder << 3;
        int iAddB     = iAddG  >> 6;;
        int iAddR     = iAddB  << 11;
        wR = dwRFill+iAddR;
        wG = dwGFill+iAddG;
        wB = dwBFill+iAddB;
        if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
        if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
        if (wB > 0x0000001F)    wB = 0x0000001F;
    }

    if (iAlpha != 16)
    {
        HL_PIXEL wTgt = *pPIX;
        int iRBG = wTgt & 0x0000F800;
        int iGBG = wTgt & 0x000007E0;
        int iBBG = wTgt & 0x0000001F;

        wR = ((wR * iAlpha + iRBG * _iAlpha) >> 4) & 0xF800;
        wG = ((wG * iAlpha + iGBG * _iAlpha) >> 4) & 0x07E0;
        wB = ((wB * iAlpha + iBBG * _iAlpha) >> 4);
    }

    *pPIX = (HL_PIXEL)(wR|wG|wB);
#endif
}
/*========================================================================*/

    void                        CShadeDIB::ShadeValueQ34_ELEVDirectColorGrad(
    HLT_PIXEL_VARS              *pVars,
    HL_PIXEL                    *pPIX )

/*========================================================================*/
{
    // Need to have some elevation
    int iPixelElev16 = pVars->m_iElev_16;
    if (iPixelElev16 <= 0)
        return;

    int iDX = pVars->m_iDXC;
    int iDY = pVars->m_iDYC;
    if (iDX == 0 && iDY == 0)
    {
        *pPIX = pVars->m_wColor34;
        return;
    }

    int iMaxElev16 = pVars->m_iMaxElev16;
    if (iPixelElev16 > (iMaxElev16 + 16))
    {
        *pPIX = pVars->m_wColor34;
        return;
    }

    int iAlpha = 16;
    if (iPixelElev16 < 16)
        iAlpha = iPixelElev16;

    // Calculate ambient?
    int iAmbient = 255;
    if (iMaxElev16 > 16 && iPixelElev16 < iMaxElev16)
    {
        // Max elevation is more than 1 pixel and pixel elev is less
        // than max, need to calculate ambient
        if (iPixelElev16 > 0)
        {
            int iDElev64 = ((iMaxElev16 - iPixelElev16) << 6) / iMaxElev16;
            iAmbient = iSqrt(0x1000-iDElev64*iDElev64) << 2;
        }
        else
        {
            iAmbient = 0;
        }
    }

    // Divide ambient by 2 (0-128)
    iAmbient = iAmbient >> 1;
    int iVal = __min(0xFF, iAmbient);
    iVal = __max(0x00, iAmbient);

#ifdef IPHONE
    DWORD dwRFill = pVars->m_wColor34 & 0x0000FF00;
    DWORD dwGFill = pVars->m_wColor34 & 0x00FF0000;
    DWORD dwBFill = pVars->m_wColor34 & 0xFF000000;
    DWORD dwRMax  = 0x0000FF00 - dwRFill;
    DWORD dwGMax  = 0x00FF0000 - dwGFill;
    DWORD dwBMax  = 0xFF000000 - dwBFill;
#else
    int dwRFill = pVars->m_wColor34 & 0xF800;
    int dwGFill = pVars->m_wColor34 & 0x07E0;
    int dwBFill = pVars->m_wColor34 & 0x001F;
#endif

#ifdef IPHONE
    int wR = 0;
    int wG = 0;
    int wB = 0;
    DWORD dwRes = 0;

    if (iVal < 128)
    {
        wR = ((dwBFill >> 7) * iVal) & 0xFF000000;
        wG = ((dwGFill >> 7) * iVal) & 0x00FF0000;
        wB = ((dwRFill >> 7) * iVal) & 0x0000FF00;
    }
    else
    {   
        int iAdder = iVal - 128;
        DWORD  iAddR     = iAdder << 8;
        DWORD  iAddG     = iAddR << 8;
        DWORD  iAddB     = iAddG << 8;
        if (iAddR < dwRMax) { wR = ((iAddR + dwRFill) & 0x0000FF00);    } else { wR = 0x0000FF00;   }
        if (iAddG < dwGMax) { wG = ((iAddG + dwGFill) & 0x00FF0000);    } else { wG = 0x00FF0000;   }
        if (iAddB < dwBMax) { wB = ((iAddB + dwBFill) & 0xFF000000);    } else { wB = 0xFF000000;   }
    }
                           
                           

    if (iAlpha == 16)
    {
        dwRes = (wR|wG|wB|0xFF);
    }
    else
    {
        if (g_iHints & SHADEDIB_HINT_INT_OVER_EXT)
        {
            // Interior over exterior, blend with existing result is fully opaque
            DWORD wIn = *pPIX;
            wIn = wIn >> 8;
            int iAlpha256 = iAlpha << 4;
            int _iAlpha256 = 256-iAlpha256;
            DWORD dwRIn = wIn & 0x000000FF;
            DWORD dwGIn = wIn & 0x0000FF00;
            DWORD dwBIn = wIn & 0x00FF0000;
            DWORD dwROut = ((wR >> 8)* iAlpha256 + dwRIn * _iAlpha256) & 0x0000FF00;
            DWORD dwGOut = ((wG >> 8)* iAlpha256 + dwGIn * _iAlpha256) & 0x00FF0000;
            DWORD dwBOut = ((wB >> 8)* iAlpha256 + dwBIn * _iAlpha256) & 0xFF000000;
            dwRes = dwROut|dwGOut|dwBOut|iAlpha256;
        }
        else
        {
            // Must Pre Multiply on stupid iPhone
            int iAlpha256 = (iAlpha << 4);
            int iRB = (((wR|wB)>>8) * iAlpha256) & 0xFF00FF00;
            int iG  = (((wG)>>8) * iAlpha256) & 0x00FF0000;
            dwRes = (iRB|iG|iAlpha256);
        }
    }

    *pPIX = dwRes;
    return;
#else
    int _iAlpha = 16-iAlpha;

    int wR = 0;
    int wG = 0;
    int wB = 0;

    int iAdder = iVal - 128;

    if (iAdder < 0)
    {
        int iAddG     = -iAdder << 3;
        int iAddB     = iAddG >> 6;;
        int iAddR     = iAddB << 11;
        if (dwRFill <= iAddR)    wR = 0; else  { wR = dwRFill - iAddR; wR &= 0x0000F800;   }
        if (dwGFill <= iAddG)    wG = 0; else  { wG = dwGFill - iAddG; wG &= 0x000007E0;   }
        if (dwBFill <= iAddB)    wB = 0; else    wB = dwBFill - iAddB;
    }
    else
    {   
        int iAddG     = iAdder << 3;
        int iAddB     = iAddG  >> 6;;
        int iAddR     = iAddB  << 11;
        wR = dwRFill+iAddR;
        wG = dwGFill+iAddG;
        wB = dwBFill+iAddB;
        if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
        if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
        if (wB > 0x0000001F)    wB = 0x0000001F;
    }

    if (iAlpha != 16)
    {
        HL_PIXEL wTgt = *pPIX;
        int iRBG = wTgt & 0x0000F800;
        int iGBG = wTgt & 0x000007E0;
        int iBBG = wTgt & 0x0000001F;
        wR = ((wR * iAlpha + iRBG * _iAlpha) >> 4) & 0xF800;
        wG = ((wG * iAlpha + iGBG * _iAlpha) >> 4) & 0x07E0;
        wB = ((wB * iAlpha + iBBG * _iAlpha) >> 4);
    }

    *pPIX = (HL_PIXEL)(wR|wG|wB);
#endif
}
/*========================================================================*/

    void                        CShadeDIB::Fill(
    HL_PIXEL                    wSet,
    CHLRect                     rect )

/*========================================================================*/
{
    int iX1 = rect.XLeft();
    int iX2 = rect.XRight();
    int iY1 = rect.YTop();
    for (int iY = 0; iY < rect.DY(); iY++)
    {
        FillHZ(wSet, iX1, iX2, iY+iY1);
    }
}
/*========================================================================*/

    void                        CShadeDIB::FillGradient(
    CHLRect                     rect,
    int                         iInsets )

/*========================================================================*/
{
    int iX1 = rect.XLeft();
    int iX2 = rect.XRight();
    int iY1 = rect.YTop();
    for (int iY = 0; iY < rect.DY(); iY++)
    {
        HL_PIXEL wSet = GradientAt(iY+iY1, iInsets);

        #ifdef IPHONE
            wSet |= 0x000000FF;
        #endif

        FillHZ(wSet, iX1, iX2, iY+iY1);
    }
}
/*========================================================================*/

    void                        CShadeDIB::Subtract(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst )

/*========================================================================*/
{
    int iDX = pOther->ImageDX();
    int iDY = pOther->DY();
    int iXSrc = 0;
    int iYSrc = 0;
    Subtract(pOther, iXDst, iYDst, iDX, iDY, iXSrc, iYSrc);
}
/*========================================================================*/

    void                        CShadeDIB::Subtract(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iXSrc,
    int                         iYSrc )

/*========================================================================*/
{
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pOther))
        return;

    START_TIMER(GDI_SHADEPOLY);
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = pOther->Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;
            HL_PIXEL wTgt = *pTgt;

            if ((wSrc & 0x00FF) < (wTgt & 0x00FF))  
                *pTgt = wSrc;

            pSrc++;
            pTgt++;
        }
    }
    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::Add(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst )

/*========================================================================*/
{
    int iDX = pOther->ImageDX();
    int iDY = pOther->DY();
    int iXSrc = 0;
    int iYSrc = 0;
    Add(pOther, iXDst, iYDst, iDX, iDY, iXSrc, iYSrc);
}
/*========================================================================*/

    void                        CShadeDIB::Add(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst,
    int                         iDX,
    int                         iDY,
    int                         iXSrc,
    int                         iYSrc )

/*========================================================================*/
{
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pOther))
        return;

    START_TIMER(GDI_SHADEPOLY);
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = pOther->Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;
            HL_PIXEL wTgt = *pTgt;

            if ((wSrc & 0x00FF) > (wTgt & 0x00FF))  
                *pTgt = wSrc;

            pSrc++;
            pTgt++;
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::And(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst,
    int                         iFlipSrc )

/*========================================================================*/
{
    int iXSrc = 0;
    int iYSrc = 0;
    int iDX   = pOther->ImageDX();
    int iDY   = pOther->DY();
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pOther))
        return;

    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        CDXYRect rSub(iXDst, iYDst, iDX, iDY);
        m_Solid.Subtract(&rSub);
    }

    START_TIMER(GDI_SHADEPOLY);

    switch (iFlipSrc)
    {
    case FLIP_NONE:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                HL_PIXEL *pSrc = pOther->Pixel(iXSrc, iY+iYSrc);
                HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

                for (int iX = 0; iX < iDX; iX++)
                {
                    HL_PIXEL wSrc = *pSrc;
                    HL_PIXEL wTgt = *pTgt;

                    if ((wSrc & 0x00FF) == 0x00FF)  
                    {
                        *pTgt = 0x00FF;
                    }
                    else
                    {
                        if ((wSrc & 0x00FF) >= (wTgt & 0x00FF))  
                            *pTgt = wSrc;
                    }

                    pSrc++;
                    pTgt++;

                }
            }
        }
        break;

    case FLIP_HZ:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                HL_PIXEL *pSrc = pOther->Pixel(iXSrc+iDX-1, iY+iYSrc);
                HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

                for (int iX = 0; iX < iDX; iX++)
                {
                    HL_PIXEL wSrc = *pSrc;
                    HL_PIXEL wTgt = *pTgt;

                    if ((wSrc & 0x00FF) == 0x00FF)  
                    {
                        *pTgt = 0x00FF;
                    }
                    else
                    {
                        if ((wSrc & 0x00FF) >= (wTgt & 0x00FF))  
                            *pTgt = wSrc;
                    }

                    pSrc--;
                    pTgt++;

                }
            }
        }
        break;

    case FLIP_VT:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                HL_PIXEL *pSrc = pOther->Pixel(iXSrc, iYSrc+iDY-iY-1);
                HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

                for (int iX = 0; iX < iDX; iX++)
                {
                    HL_PIXEL wSrc = *pSrc;
                    HL_PIXEL wTgt = *pTgt;

                    if ((wSrc & 0x00FF) == 0x00FF)  
                    {
                        *pTgt = 0x00FF;
                    }
                    else
                    {
                        if ((wSrc & 0x00FF) > (wTgt & 0x00FF))  
                            *pTgt = wSrc;
                    }

                    pSrc++;
                    pTgt++;

                }
            }
        }
        break;

    case FLIP_BOTH:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                HL_PIXEL *pSrc = pOther->Pixel(iXSrc+iDX-1, iYSrc+iDY-iY-1);
                HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

                for (int iX = 0; iX < iDX; iX++)
                {
                    HL_PIXEL wSrc = *pSrc;
                    HL_PIXEL wTgt = *pTgt;

                    if ((wSrc & 0x00FF) == 0x00FF)  
                    {
                        *pTgt = 0x00FF;
                    }
                    else
                    {
                        if ((wSrc & 0x00FF) >= (wTgt & 0x00FF))  
                            *pTgt = wSrc;
                    }

                    pSrc--;
                    pTgt++;

                }
            }
        }
        break;
    }
    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::XOR(
    CShadeDIB                   *pOther,
    int                         iXDst, 
    int                         iYDst )

/*========================================================================*/
{
    int iXSrc = 0;
    int iYSrc = 0;
    int iDX   = pOther->ImageDX();
    int iDY   = pOther->DY();
    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, pOther))
        return;

    START_TIMER(GDI_SHADEPOLY);

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = pOther->Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = Pixel(iXDst, iY+iYDst);

        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;
            HL_PIXEL wTgt = *pTgt;

//            if ((wTgt & 0x00FF) != 0x00FF && (wSrc & 0x00FF) != 0)
            if ((wTgt & 0x00FF) && (wSrc & 0x00FF))
            {
                int iSrcElev = wSrc & 0x00FF;
                if (iSrcElev >= 16)
                {
                    *pTgt = 0x0000;
                }
                else
                {
                    HL_PIXEL wTgt = *pTgt & 0xFF00;
                    wTgt = wTgt | (16-iSrcElev);
                    *pTgt = wTgt;
                }
            }
            pSrc++;
            pTgt++;
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::AndEdgeElev(
    CHLRect                     rRect,
    int                         iDir,
    int                         iNShading,
    BOOL                        bAND )

/*========================================================================*/
{
    int iDXIn = rRect.DX();
    int iDYIn = rRect.DY();
    int iXSrc = 0;
    int iYSrc = 0;
    int iDX   = rRect.DX();
    int iDY   = rRect.DY();
    int iXDst = rRect.XLeft();
    int iYDst = rRect.YTop();

    if (!GetSafeDims(&iXDst, &iYDst, &iDX, &iDY, &iXSrc, &iYSrc, NULL))
        return;

    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        CDXYRect rSub(iXDst, iYDst, iDX, iDY);
        m_Solid.Subtract(&rSub);
    }

    switch (iDir)
    {
    case DIR_LEFT:
    case DIR_RIGHT:
        {       
            for (int iX = 0; iX < iDX; iX++)
            {
                int iPos = iX + iXSrc + 1;
                if (iDir == DIR_LEFT)  
                    iPos = iDXIn - (iX + iXSrc);
                HL_PIXEL wVal = (HL_PIXEL)(iPos * iNShading / iDXIn);
                HL_PIXEL wThis = wVal|PIXEL_VT;
                HL_PIXEL *pSet = Pixel(iXDst+iX, iYDst);
                if (bAND)
                {
                    for (int iY = 0; iY < iDY; iY++)
                    {
                        HL_PIXEL wSet = *pSet;
                        if ((wSet & 0xFF) < wVal)
                            *pSet = wThis;
                        pSet += m_iDX;
                    }
                }
                else
                {
                    for (int iY = 0; iY < iDY; iY++)
                    {
                        *pSet = wThis;
                        pSet += m_iDX;
                    }
                }
            }
        }
        break;

    case DIR_UP:
    case DIR_DOWN:
        {
            for (int iY = 0; iY < iDY; iY++)
            {
                int iPos = iY + iYSrc + 1;
                if (iDir == DIR_UP)  
                    iPos = iDYIn - (iY + iYSrc);
                HL_PIXEL wVal = (HL_PIXEL)(iPos * iNShading / iDYIn);
                HL_PIXEL wThis = wVal|PIXEL_HZ;
                HL_PIXEL *pSet = Pixel(iXDst, iYDst+iY);
                if (bAND)
                {
                    for (int iX = 0; iX < iDX; iX++)
                    {
                        HL_PIXEL wSet = *pSet;
                        if ((wSet &0xFF) < wVal)
                            *pSet = wThis;
                        pSet++;
                    }
                }
                else
                {
                    for (int iX = 0; iX < iDX; iX++)
                    {
                        *pSet = wThis;
                        pSet++;
                    }
                }
            }
        }
        break;
    }
}
/*========================================================================*/

    void                        CShadeDIB::ApplyTo(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst )

/*========================================================================*/
{
    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        for (int i = 0; i < m_ROI.NRects(); i++)
        {
            int iX = m_ROI.X(i);
            int iY = m_ROI.Y(i);
            int iDX = m_ROI.DX(i);
            int iDY = m_ROI.DY(i);
            ApplyTo(pDIB, iXDst+iX, iYDst+iY, iX, iY, iDX, iDY);
        }

        return;
    }

    ApplyTo(pDIB, iXDst, iYDst, 0, 0, m_iImageDX, m_iDY);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyTo(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY )

/*========================================================================*/
{
    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, pDIB))
        return;

    START_TIMER(GDI_SHADEPOLY);
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = pDIB->Pixel(iXDst, iY+iYDst);

        for (int iX = 0; iX < iDX; iX++)
        {
#ifndef IPHONE
            HL_PIXEL wTgt = *pTgt;
#endif
            HL_PIXEL wSrc = *pSrc;                
            if ((wSrc & 0x00FF) && (wSrc & 0xFF00))
            {
                int iAdder = (wSrc >> 8) - 128;

#ifdef IPHONE
                HL_PIXEL wResult;
                int iAlpha = 16;
                if (iAdder > 0)
                {
                    iAlpha = ((iAlpha * iAdder) >> 4);
                    wResult = (iAlpha << 24)|(iAlpha << 16)|(iAlpha <<8)|iAlpha;
                    *pTgt = wResult;
                }
                else
                {
                    iAlpha = ((iAlpha * -iAdder) >> 4);
                    wResult = iAlpha;
                    *pTgt = wResult;
                }
#else
                int wR = wTgt & 0x0000F800;
                int wG = wTgt & 0x000007E0;
                int wB = wTgt & 0x0000001F;

                if (iAdder < 0)
                {
                    int iAddG     = -iAdder << 3;
                    int iAddB     = iAddG >> 6;;
                    int iAddR     = iAddB << 11;
                    if (wR <= iAddR)    wR = 0; else  { wR = wR - iAddR; wR &= 0x0000F800;   }
                    if (wG <= iAddG)    wG = 0; else  { wG = wG - iAddG; wG &= 0x000007E0;   }
                    if (wB <= iAddB)    wB = 0; else  wB = wB - iAddB;
                }
                else
                {   
                    int iAddG     = iAdder << 3;
                    int iAddB     = iAddG  >> 6;;
                    int iAddR     = iAddB  << 11;
                    wR += iAddR;
                    if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
                    wG += iAddG;
                    if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
                    wB += iAddB;
                    if (wB > 0x0000001F)    wB = 0x0000001F;
                }

                *pTgt = (HL_PIXEL)(wR|wG|wB);
#endif
            }

            pTgt++;
            pSrc++;
        }
    }
    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyShine(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iAlphaT,
    int                         iAlphaB,
    int                         iRMinor )

/*========================================================================*/
{
    int iXSrc = 0;
    int iYSrc = 0;
    int iDX = m_iDX;
    int iDY = m_iDY;
    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, pDIB))
        return;
        
    int iR2 = iRMinor * iRMinor;

    int iDYApply = iDY / 2;
    for (int iY = 0; iY < iDYApply; iY++)
    {
        int iAlphaThisScanLine = iAlphaT + (iAlphaB-iAlphaT) * iY / iDYApply;

        int iRadThis = 0;

        int iDYBottom = iDYApply - iY;
        if (iDYBottom < iRMinor)
        {
            iDYBottom = iRMinor - iDYBottom;
            iRadThis = iRMinor - iSqrt(iR2 - iDYBottom*iDYBottom);
        }

        int iX = 0;
        HL_PIXEL *pSrc = Pixel(iXSrc+iRadThis, iYSrc+iY);
        HL_PIXEL *pTgt = pDIB->Pixel(iXDst+iRadThis, iYDst+iY);
        int iDXThisLine = iDX - 2 * iRadThis;

        while (iX < iDXThisLine)
        {
            HL_PIXEL wThis = *pSrc;
            if ((wThis & 0x00FF) > 0)
            {
                int iAlpha = wThis & 0x00FF;
                iAlpha = MIN(iAlpha, 16);

                int iAThis = (iAlpha * iAlphaThisScanLine) >> 4;
                int _iAThis = 256-iAThis;
                DWORD dwRBInThis = (iAThis * 0x00FF00FF) & 0xFF00FF00;
                DWORD dwGInThis  = (iAThis * 0x0000FF00) & 0x00FF0000;

        #ifdef IPHONE
                DWORD wP = *pTgt;
                wP = wP >> 8;
                DWORD dwRB = wP & 0x00FF00FF;
                DWORD dwG  = wP & 0x0000FF00;
                dwRB = (dwRB * _iAThis + dwRBInThis) & 0xFF00FF00;
                dwG  = (dwG  * _iAThis + dwGInThis)  & 0x00FF0000;
                *pTgt = dwRB|dwG|0xFF;
        #endif
            }

            iX++;
            pSrc++;
            pTgt++;
        }
    }
}
/*========================================================================*/

    void                        CShadeDIB::ApplyShineScanLine(
    CHLDibDC                    *pDIB,
    int                         iX,
    int                         iY,
    int                         iDX,
    int                         iA,
    int                         iR,
    int                         iFeatherL,
    int                         iFeatherR )

/*========================================================================*/
{
    if (iDX < 2 * iR)
        return;
    iX += iR;
    iDX -= 2 * iR;

    HL_PIXEL *pTgt = pDIB->Pixel(iX, iY);
    for (int i = 0; i < iFeatherL; i++)
    {
        int iAThis = i * iA / iFeatherL;
        int _iAThis = 256-iAThis;
        DWORD dwRBInThis = (iAThis * 0x00FF00FF) & 0xFF00FF00;
        DWORD dwGInThis  = (iAThis * 0x0000FF00) & 0x00FF0000;

#ifdef IPHONE
        DWORD wP = *pTgt;
        wP = wP >> 8;
        DWORD dwRB = wP & 0x00FF00FF;
        DWORD dwG  = wP & 0x0000FF00;
        dwRB = (dwRB * _iAThis + dwRBInThis) & 0xFF00FF00;
        dwG  = (dwG  * _iAThis + dwGInThis)  & 0x00FF0000;
        *pTgt = dwRB|dwG|0xFF;
#endif

        pTgt++;
    }

    pTgt = pDIB->Pixel(iX+iDX-1, iY);
    for (int i = 0; i < iFeatherR; i++)
    {
        int iAThis = i * iA / iFeatherR;
        int _iAThis = 256-iAThis;
        DWORD dwRBInThis = (iAThis * 0x00FF00FF) & 0xFF00FF00;
        DWORD dwGInThis  = (iAThis * 0x0000FF00) & 0x00FF0000;

#ifdef IPHONE
        DWORD wP = *pTgt;
        wP = wP >> 8;
        DWORD dwRB = wP & 0x00FF00FF;
        DWORD dwG  = wP & 0x0000FF00;
        dwRB = (dwRB * _iAThis + dwRBInThis) & 0xFF00FF00;
        dwG  = (dwG  * _iAThis + dwGInThis)  & 0x00FF0000;
        *pTgt = dwRB|dwG|0xFF;
#endif

        pTgt--;
    }

    int _iA = 256-iA;
    DWORD dwRBIn = (iA * 0x00FF00FF) & 0xFF00FF00;
    DWORD dwGIn  = (iA * 0x0000FF00) & 0x00FF0000;

    iX += iFeatherL;
    iDX -= (iFeatherL+iFeatherR);

    pTgt = pDIB->Pixel(iX, iY);
    for (int i = 0; i < iDX; i++)
    {
#ifdef IPHONE
        DWORD wP = *pTgt;
        wP = wP >> 8;
        DWORD dwRB = wP & 0x00FF00FF;
        DWORD dwG  = wP & 0x0000FF00;
        dwRB = (dwRB * _iA + dwRBIn) & 0xFF00FF00;
        dwG  = (dwG  * _iA + dwGIn)  & 0x00FF0000;
        *pTgt = dwRB|dwG|0xFF;
#endif

        pTgt++;
    }
}
/*========================================================================*/

    void                        CShadeDIB::ApplyShineyGradient(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst )

/*========================================================================*/
{
    int iXSrc = 0;
    int iYSrc = 0;
    int iDX = m_iImageDX;
    int iDY = m_iDY;

    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, pDIB))
        return;

    int iRMinor = g_pSetServer->GetInt(INT_ALPHA_RMINOR);
    int iAT     = g_pSetServer->GetInt(INT_ALPHAMAX);
    int iAB     = iAT * g_pSetServer->GetInt(INT_ALPHA_UPPER_RATIO) / 100;
    int iDYAll = __min(iDY, pDIB->DY()/2);

    int iYTopEdge = -1;
    for (int iY = 0; iY < iDYAll && iYTopEdge == -1; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY);
        for (int iX = 0; iX < iDX; iX++)
        {
            if (*pSrc & 0x00FF)
                iYTopEdge = iY;
            pSrc++;
        }
    }

    iRMinor = __min(iRMinor, iDYAll - iYTopEdge);
    int iDYTopSection = iDYAll - iRMinor;
    int iDYBtmSection = pDIB->DY()/2;

    for (int iY = 0; iY < iDYTopSection; iY++)
    {
        unsigned int iAlphaThisLine = iAT + (iAB-iAT)*iY/ iDYAll;


#ifdef IPHONE
        unsigned int iRAdd = (iAlphaThisLine << 8);
        unsigned int iGAdd = (iAlphaThisLine << 16);
        unsigned int iBAdd = (iAlphaThisLine << 24);

        unsigned int iRMax = R_MASK - iRAdd;
        unsigned int iGMax = G_MASK - iGAdd;
        unsigned int iBMax = B_MASK - iBAdd;

        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY);
        HL_PIXEL *pDst = pDIB->Pixel(iXDst, iYDst+iY);
        for (int iX = 0; iX < iDX; iX++)
        {
            int iElev = *pSrc & 0x00FF;
            HL_PIXEL wTgt = *pDst;
            unsigned int iR = wTgt & R_MASK;
            unsigned int iG = wTgt & G_MASK;
            unsigned int iB = wTgt & B_MASK;
            unsigned int iA = wTgt & 0xFF;

            if (iElev > 0)
            {
                if (iElev >= 16 && iA == 0xFF)
                {
                    if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
                    if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
                    if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   } 
                }
                else
                {
                    iElev = MIN(iElev, 16);
                    unsigned int iFactor = (iA * iElev)>>4;
                    unsigned int iRAddThis = ((iRAdd >> 8) * iFactor) & R_MASK;
                    unsigned int iGAddThis = ((iGAdd >> 8) * iFactor) & G_MASK;
                    unsigned int iBAddThis = ((iBAdd >> 8) * iFactor) & B_MASK;
                    unsigned int iRMaxThis = R_MASK - iRAddThis;
                    unsigned int iGMaxThis = G_MASK - iGAddThis;
                    unsigned int iBMaxThis = B_MASK - iBAddThis;

                    if (iR >= iRMaxThis) iR = R_MASK; else { iR+=iRAddThis; iR &= R_MASK; }
                    if (iG >= iGMaxThis) iG = G_MASK; else { iG+=iGAddThis; iG &= G_MASK; }
                    if (iB >= iBMaxThis) iB = B_MASK; else { iB+=iBAddThis; iB &= B_MASK; }
                }    
            }

            *pDst = (HL_PIXEL)(iR|iG|iB|iA);
#else
        int iRAdd = (iAlphaThisLine << 8) & 0xF800;
        int iGAdd = (iAlphaThisLine << 3) & 0x07E0;
        int iBAdd = (iAlphaThisLine >> 3);

        int iRMax = 0xF800 - iRAdd;
        int iGMax = 0x07E0 - iGAdd;
        int iBMax = 0x001F - iBAdd;

        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY);
        HL_PIXEL *pDst = pDIB->Pixel(iXDst, iYDst+iY);
        for (int iX = 0; iX < iDX; iX++)
        {
            int iElev = *pSrc & 0x00FF;
            HL_PIXEL wTgt = *pDst;
            int iR = wTgt & 0xF800;
            int iG = wTgt & 0x07E0;
            int iB = wTgt & 0x001F;

            if (iElev > 0)
            {
                if (iElev >= 16)
                {
                    if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
                    if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
                    if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 
                }
                else
                {
                    int iRAddThis = ((iRAdd * iElev) >> 4) & 0xF800;
                    int iGAddThis = ((iGAdd * iElev) >> 4) & 0x07E0;
                    int iBAddThis = (iBAdd * iElev) >> 4;
                    int iRMaxThis = 0xF800 - iRAddThis;
                    int iGMaxThis = 0x07E0 - iGAddThis;
                    int iBMaxThis = 0x001F - iBAddThis;

                    if (iR >= iRMaxThis) iR = 0xF800; else { iR+=iRAddThis; iR &= 0xF800; }
                    if (iG >= iGMaxThis) iG = 0x07E0; else { iG+=iGAddThis; iG &= 0x07E0; }
                    if (iB >= iBMaxThis) iB = 0x001F; else   iB+=iBAddThis; 
                }    
            }

            *pDst = (HL_PIXEL)(iR|iG|iB);
#endif

            pSrc++;
            pDst++;
        }
    }

    for (int iY = 0; iY < iRMinor; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY+iDYTopSection);
        int iAlphaThisLine = iAT + (iAB-iAT)*(iY+iDYTopSection)/iDYAll;

        if (iY == iRMinor -1)
            iAlphaThisLine /= 2;

        int iX = 0;
        int iXStart = 0;
        BOOL bInSegment = FALSE;
        while (iX < iDX)
        {
            HL_PIXEL wSrc = *pSrc;
            if (bInSegment)
            {
                if ((wSrc & 0x00FF) == 0)
                {
                    int iDXThisSegment = iX - iXStart + 2;
                    ApplyShineyScanLine(pDIB, iXStart+iXSrc - 1, iYSrc+iY+iDYTopSection, iXDst+iXStart-1, iYDst+iY+iDYTopSection, iDXThisSegment, iAlphaThisLine, iY, iRMinor);
                    bInSegment = FALSE;
                }
            }
            else
            {
                if (wSrc & 0x00FF)
                {
                    iXStart = iX;
                    bInSegment = TRUE;
                }
            }                        

            pSrc++;
            iX++;
        }

        if (bInSegment)
        {
            int iDXThisSegment = iX - iXStart + 2;
            ApplyShineyScanLine(pDIB, iXStart+iXSrc - 1, iYSrc+iY+iDYTopSection, iXDst+iXStart-1, iYDst+iY+iDYTopSection, iDXThisSegment, iAlphaThisLine, iY, iRMinor);
        }
    }


    iAB = g_pSetServer->GetInt(INT_ALPHA_BOTTOM_EDGE);

    if (iAB <= 0)
        return;

    for (int iY = 0; iY < iDYBtmSection; iY++)
    {
        int iAlphaThisLine = iAB * iY / iDYBtmSection;

#ifdef IPHONE
        unsigned int iRAdd = (iAlphaThisLine << 8);
        unsigned int iGAdd = (iAlphaThisLine << 16);
        unsigned int iBAdd = (iAlphaThisLine << 24);

        unsigned int iRMax = R_MASK - iRAdd;
        unsigned int iGMax = G_MASK - iGAdd;
        unsigned int iBMax = B_MASK - iBAdd;

        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY+pDIB->DY()-iDYBtmSection);
        HL_PIXEL *pDst = pDIB->Pixel(iXDst, iYDst+iY+pDIB->DY()-iDYBtmSection);
        for (int iX = 0; iX < iDX; iX++)
        {
            int iElev = *pSrc & 0x00FF;
            HL_PIXEL wTgt = *pDst;
            unsigned int iR = wTgt & R_MASK;
            unsigned int iG = wTgt & G_MASK;
            unsigned int iB = wTgt & B_MASK;
            unsigned int iA = wTgt & 0xFF;

            if (iElev > 0)
            {
                if (iElev >= 16 && iA == 0xFF)
                {
                    if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK; }
                    if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK; }
                    if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK; }
                }
                else
                {
                    int iFactor = (iA * iElev) >> 4;
                    int iRAddThis = ((iRAdd >> 8) * iFactor) & R_MASK;
                    int iGAddThis = ((iGAdd >> 8) * iFactor) & G_MASK;
                    int iBAddThis = ((iBAdd >> 8) * iFactor) & B_MASK;
                    int iRMaxThis = R_MASK - iRAddThis;
                    int iGMaxThis = G_MASK - iGAddThis;
                    int iBMaxThis = B_MASK - iBAddThis;

                    if (iR >= iRMaxThis) iR = R_MASK; else { iR+=iRAddThis; iR &= R_MASK; }
                    if (iG >= iGMaxThis) iG = G_MASK; else { iG+=iGAddThis; iG &= G_MASK; }
                    if (iB >= iBMaxThis) iB = B_MASK; else { iB+=iBAddThis; iB &= B_MASK; }
                }    

                *pDst = iR|iG|iB|iA;
            }
            
            pSrc++;
            pDst++;
        }
#else
        int iRAdd = (iAlphaThisLine << 8) & 0xF800;
        int iGAdd = (iAlphaThisLine << 3) & 0x07E0;
        int iBAdd = (iAlphaThisLine >> 3);

        int iRMax = 0xF800 - iRAdd;
        int iGMax = 0x07E0 - iGAdd;
        int iBMax = 0x001F - iBAdd;

        HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc+iY+pDIB->DY()-iDYBtmSection);
        HL_PIXEL *pDst = pDIB->Pixel(iXDst, iYDst+iY+pDIB->DY()-iDYBtmSection);
        for (int iX = 0; iX < iDX; iX++)
        {
            int iElev = *pSrc & 0x00FF;
            HL_PIXEL wTgt = *pDst;
            int iR = wTgt & 0xF800;
            int iG = wTgt & 0x07E0;
            int iB = wTgt & 0x001F;

            if (iElev > 0)
            {
                if (iElev >= 16)
                {
                    if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
                    if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
                    if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 
                }
                else
                {
                    int iRAddThis = ((iRAdd * iElev) >> 4) & 0xF800;
                    int iGAddThis = ((iGAdd * iElev) >> 4) & 0x07E0;
                    int iBAddThis = (iBAdd * iElev) >> 4;
                    int iRMaxThis = 0xF800 - iRAddThis;
                    int iGMaxThis = 0x07E0 - iGAddThis;
                    int iBMaxThis = 0x001F - iBAddThis;

                    if (iR >= iRMaxThis) iR = 0xF800; else { iR+=iRAddThis; iR &= 0xF800; }
                    if (iG >= iGMaxThis) iG = 0x07E0; else { iG+=iGAddThis; iG &= 0x07E0; }
                    if (iB >= iBMaxThis) iB = 0x001F; else   iB+=iBAddThis; 
                }    
            }

            *pDst = (HL_PIXEL)(iR|iG|iB);
            pSrc++;
            pDst++;
        }
#endif
    }
}
/*========================================================================*/

    void                        CShadeDIB::ApplyShineyScanLine(
    CHLDibDC                    *pDIB,
    int                         iXSrc,
    int                         iYSrc,
    int                         iXDst,
    int                         iYDst,
    int                         iDX,
    int                         iAlpha,
    int                         iYInRadius,
    int                         iRadius )

/*========================================================================*/
{
    int iRadius16 = (iRadius << 4);
    int iXInset16 = iRadius16-iSqrt16(iRadius*iRadius-iYInRadius*iYInRadius);
    iXInset16 = __min(iXInset16, iDX * 8);
    int iDXOut16 = (iDX*16-iXInset16*2);

    if (iXSrc < 0)
    {
        iXDst -= iXSrc;
        iDX += iXSrc;
        iXSrc = 0;
    }

    if (iXDst < 0)
    {
        iXSrc += iXDst;
        iDX += iXDst;
        iXDst = 0;
    }

    iDX = __min(iDX, m_iImageDX-iXSrc);
    iDX = __min(iDX, pDIB->ImageDX()-iXDst);

    HL_PIXEL *pSrc = Pixel(iXSrc, iYSrc);
    
    int iXStart = (iXInset16>>4);
    HL_PIXEL *pDst = pDIB->Pixel(iXDst+iXStart, iYDst);
    int iXOff16 = (iXStart << 4)-iXInset16;

    int iX = iXOff16;

    while (iX < iDXOut16)
    {
        int iXSrc1024 = iX * (iDX << 10) / iDXOut16;
        int iXL = (iXSrc1024 >> 10);
        int iXR = iXL + 1;
        int iXRFact = iXSrc1024 - (iXL << 10);
        int iXLFact = 1024 - iXRFact;

        HL_PIXEL *pSrcLo = pSrc + iXL;
        HL_PIXEL *pSrcHi = pSrc + iXR;

        int iElevLo = __min(16, *pSrcLo & 0x00FF);
        int iElevHi = __min(16, *pSrcHi & 0x00FF);
        int iElev = ((iElevLo * iXLFact + iElevHi * iXRFact) >> 10);

        HL_PIXEL wTgt = *pDst;

#ifdef IPHONE
        if (iElev > 0)
        {
            unsigned int iR = wTgt & R_MASK;
            unsigned int iG = wTgt & G_MASK;
            unsigned int iB = wTgt & B_MASK;
            unsigned int iA = wTgt & 0xFF;

            unsigned int iAlphaThis = (iAlpha * iElev * iA) >> 12;
//            unsigned int iAlphaThis = (iAlpha * iA) >> 8;

            unsigned int iRAdd = (iAlphaThis << 8);
            unsigned int iGAdd = (iAlphaThis << 16);
            unsigned int iBAdd = (iAlphaThis << 24);

            unsigned int iRMask = (iA << 8);
            unsigned int iGMask = (iA << 16);
            unsigned int iBMask = (iA << 24);
            unsigned int iRMax = iRMask - iRAdd;
            unsigned int iGMax = iGMask - iGAdd;
            unsigned int iBMax = iBMask - iBAdd;

            if (iR >= iRMax) iR = iRMask; else { iR+=iRAdd; iR &= R_MASK;   }
            if (iG >= iGMax) iG = iGMask; else { iG+=iGAdd; iG &= G_MASK;   }
            if (iB >= iBMax) iB = iBMask; else { iB+=iBAdd; iB &= B_MASK;   }

            *pDst = (HL_PIXEL)(iR|iG|iB|iA);
        }
#else
        int iR = wTgt & 0xF800;
        int iG = wTgt & 0x07E0;
        int iB = wTgt & 0x001F;

        int iAlphaThis = (iElev * iAlpha) >> 4;

        int iRAdd = (iAlphaThis << 8) & 0xF800;
        int iGAdd = (iAlphaThis << 3) & 0x07E0;
        int iBAdd = (iAlphaThis >> 3);

        int iRMax = 0xF800 - iRAdd;
        int iGMax = 0x07E0 - iGAdd;
        int iBMax = 0x001F - iBAdd;

        if (iElev > 0)
        {
            if (iR >= iRMax) iR = 0xF800; else { iR+=iRAdd; iR &= 0xF800; }
            if (iG >= iGMax) iG = 0x07E0; else { iG+=iGAdd; iG &= 0x07E0; }
            if (iB >= iBMax) iB = 0x001F; else   iB+=iBAdd; 
        }

        *pDst = (HL_PIXEL)(iR|iG|iB);
#endif
        pDst++;
        iX += 16;
    }
}
/*========================================================================*/

    void                        CShadeDIB::ApplyToWithColor(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iMaxElev,
    COLORREF                    rgb )

/*========================================================================*/
{
    HL_PIXEL wRGB = RGBTOHLPIXEL(rgb);
    #ifdef IPHONE
        wRGB = wRGB | 0xFF;
    #endif
    SetFillColor(rgb);

    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        int i = 0;

        for (i = 0; i < m_Solid.NRects(); i++)
        {
            CHLRect rSolid = m_Solid.RectAtIndex(i);
            rSolid.Translate(iXDst, iYDst);
            pDIB->FillRect(rgb, rSolid);
        }

        for (i = 0; i < m_ROI.NRects(); i++)
        {
            int iX = m_ROI.X(i);
            int iY = m_ROI.Y(i);
            int iDX = m_ROI.DX(i);
            int iDY = m_ROI.DY(i);
            ApplyToWithColor(pDIB, iXDst+iX, iYDst+iY, iX, iY, iDX, iDY, iMaxElev, wRGB);
        }


        return;
    }

    ApplyToWithColor(pDIB, iXDst, iYDst, 0, 0, m_iImageDX, m_iDY, iMaxElev, wRGB);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyToWithColor(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iMaxElev,
    HL_PIXEL                    wRGB )

/*========================================================================*/
{
    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, pDIB))
        return;

    START_TIMER(GDI_SHADEPOLY);

#ifndef IPHONE
    int wR = 0;
    int wG = 0;
    int wB = 0;

    int wRsolid = wRGB & 0x0000F800;
    int wGsolid = wRGB & 0x000007E0;
    int wBsolid = wRGB & 0x0000001F;
#endif

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = pDIB->Pixel(iXDst, iY+iYDst);

        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;                
            if (wSrc & 0x00FF)
            {
                if (wSrc & 0xFF00)
                {
                    int iAlpha16 = wSrc & 0x00FF;

                    if (iAlpha16 < 16)
                    {
#ifdef IPHONE
                        int iAlpha256 = iAlpha16 << 4;
                        HL_PIXEL wColor = (g_FillMap[wSrc>>8]) >> 8;
                        DWORD dwRB = wColor & 0x00FF00FF;
                        DWORD dwG  = wColor & 0x0000FF00;
                        dwRB = (dwRB * iAlpha256) & 0xFF00FF00;
                        dwG  = (dwG  * iAlpha256) & 0x00FF0000;
                        *pTgt = dwRB|dwG|iAlpha256;
#else
                        int iAdder = (wSrc >> 8) - 128;
                        HL_PIXEL wTgt = *pTgt;
                        int iRBG = wTgt & 0x0000F800;
                        int iGBG = wTgt & 0x000007E0;
                        int iBBG = wTgt & 0x0000001F;
                        int _iAlpha16 = 16-iAlpha16;

                        wR = wRsolid;
                        wG = wGsolid;
                        wB = wBsolid;

                        if (iAdder < 0)
                        {
                            int iAddG     = -iAdder << 3;
                            int iAddB     = iAddG >> 6;;
                            int iAddR     = iAddB << 11;
                            if (wR <= iAddR)    wR = 0; else  { wR = wR - iAddR; wR &= 0x0000F800;   }
                            if (wG <= iAddG)    wG = 0; else  { wG = wG - iAddG; wG &= 0x000007E0;   }
                            if (wB <= iAddB)    wB = 0; else    wB = wB - iAddB;
                        }
                        else
                        {   
                            int iAddG     = iAdder << 3;
                            int iAddB     = iAddG  >> 6;;
                            int iAddR     = iAddB  << 11;
                            wR = wR+iAddR;
                            wG = wG+iAddG;
                            wB = wB+iAddB;
                            if (wR > 0x0000F800)    wR = 0x0000F800; else wR &= 0x0000F800;
                            if (wG > 0x000007E0)    wG = 0x000007E0; else wG &= 0x000007E0;
                            if (wB > 0x0000001F)    wB = 0x0000001F;
                        }


                        wR = ((wR * iAlpha16 + iRBG * _iAlpha16) >> 4) & 0xF800;
                        wG = ((wG * iAlpha16 + iGBG * _iAlpha16) >> 4) & 0x07E0;
                        wB = ((wB * iAlpha16 + iBBG * _iAlpha16) >> 4);

                        *pTgt = (HL_PIXEL)(wR|wG|wB);
#endif                        
                    }
                    else
                    {
                        *pTgt = g_FillMap[wSrc>>8];
                    }
                }
                else
                {
                    *pTgt = wRGB;
                }
            }

            pTgt++;
            pSrc++;
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyToWithColors(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iMaxElev,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iHints)

/*========================================================================*/
{
    CGradient1D gradient(rgb1, rgb2);

    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        int i = 0;
        for (i = 0; i < m_ROI.NRects(); i++)
        {
            int iX = m_ROI.X(i);
            int iY = m_ROI.Y(i);
            int iDX = m_ROI.DX(i);
            int iDY = m_ROI.DY(i);
            ApplyToWithColors(pDIB, iXDst+iX, iYDst+iY, iX, iY, iDX, iDY, iMaxElev, &gradient, iHints);
        }

        for (i = 0; i < m_Solid.NRects(); i++)
        {
            CHLRect rSolid = m_Solid.RectAtIndex(i);
            rSolid.Translate(iXDst, iYDst);
            COLORREF w1 = gradient.COLORREFAt(rSolid.YTop(), pDIB->DY());
            COLORREF w2 = gradient.COLORREFAt(rSolid.YBottom(), pDIB->DY());

            pDIB->GradientFill(rSolid, w1, w2, DIR_DOWN);
        }

        return;
    }

    ApplyToWithColors(pDIB, iXDst, iYDst, 0, 0, m_iImageDX, m_iDY, iMaxElev, &gradient, iHints);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyToWithColors(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iMaxElev,
    CGradient1D                 *pGradient,
    int                         iHints)

/*========================================================================*/
{
    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, pDIB))
        return;

    START_TIMER(GDI_SHADEPOLY);

    DWORD dwROut = 0;
    DWORD dwGOut = 0;
    DWORD dwBOut = 0;

#ifndef IPHONE
    CDitherMap map(0x0000);
#endif

    int iYMax = pDIB->DY();
    for (int iY = 0; iY < iDY; iY++)
    {
        // Source and dest pixels for this scan line
        HL_PIXEL *pSrc = Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = pDIB->Pixel(iXDst, iY+iYDst);

        // Get the color for this position in the vertical gradient

        // Pull out color components
#ifdef IPHONE
        HL_PIXEL wRGB = pGradient->PIXELAt(iY+iYDst, iYMax);
        DWORD dwR = wRGB & 0x0000FF00;
        DWORD dwG = wRGB & 0x00FF0000;
        DWORD dwB = wRGB & 0xFF000000;
#else
        COLORREF rgb = pGradient->COLORREFAt(iY+iYDst, iYMax);
        map.Set(GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
#endif

        // Run through the scan line
        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;

#ifndef IPHONE
            HL_PIXEL wRGB = map.NextColor();
#endif
                                                           
            if (wSrc & 0x00FF)
            {
#ifndef IPHONE
                DWORD dwR = wRGB & 0xF800;
                DWORD dwG = wRGB & 0x07E0;
                DWORD dwB = wRGB & 0x001F;
#endif

                //
                // Pixel is raised
                //
                if (wSrc & 0xFF00)
                {
                    //
                    // Pixel has shading component
                    //
                    int iAdder = (wSrc >> 8);
                    if (iAdder < 128)
                    {
#ifdef IPHONE
                        dwROut = ((dwR >> 7) * iAdder) & 0x0000FF00;
                        dwGOut = ((dwG >> 7) * iAdder) & 0x00FF0000;
                        dwBOut = ((dwB >> 7) * iAdder) & 0xFF000000;
#else
/*

                        dwROut = ((dwR * iAdder) >> 7) & R_MASK;
                        dwGOut = ((dwG * iAdder) >> 7) & G_MASK;
                        dwBOut = ((dwB * iAdder) >> 7) & B_MASK;
*/

                        iAdder = 128-iAdder;
                        DWORD iAddG     = iAdder << 3;
                        DWORD iAddB     = iAddG >> 6;
                        DWORD iAddR     = iAddB << 11;
                        if (dwR <= iAddR)    dwROut = 0; else  { dwROut = dwR - iAddR; dwROut &= 0x0000F800;   }
                        if (dwG <= iAddG)    dwGOut = 0; else  { dwGOut = dwG - iAddG; dwGOut &= 0x000007E0;   }
                        if (dwB <= iAddB)    dwBOut = 0; else    dwBOut = dwB - iAddB;
#endif
                    }
                    else
                    {
                        iAdder -= 128;

#ifdef IPHONE
                        DWORD dwRAdd = iAdder << 8;
                        DWORD dwGAdd = iAdder << 16;
                        DWORD dwBAdd = iAdder << 24;
                        DWORD dwRMax = 0x0000FF00 - dwRAdd;
                        DWORD dwGMax = 0x00FF0000 - dwGAdd;
                        DWORD dwBMax = 0xFF000000 - dwBAdd;
                        if (dwR < dwRMax) { dwROut = dwR + dwRAdd; dwROut &= 0x0000FF00; } else dwROut = 0x0000FF00;
                        if (dwG < dwGMax) { dwGOut = dwG + dwGAdd; dwGOut &= 0x00FF0000; } else dwGOut = 0x00FF0000;
                        if (dwB < dwBMax) { dwBOut = dwB + dwBAdd; dwBOut &= 0xFF000000; } else dwBOut = 0xFF000000;
#else
                        DWORD dwRAdd = (iAdder << 8) & 0xF800; DWORD dwRMax = 0xF800-dwRAdd;
                        DWORD dwGAdd = (iAdder << 3) & 0x07E0; DWORD dwGMax = 0x07E0-dwGAdd;
                        DWORD dwBAdd = (iAdder >> 3);          DWORD dwBMax = 0x001F-dwBAdd;
                        if (dwR < dwRMax) { dwROut = dwR + dwRAdd; dwROut &= 0xF800; } else dwROut = 0xF800;
                        if (dwG < dwGMax) { dwGOut = dwG + dwGAdd; dwGOut &= 0x07E0; } else dwGOut = 0x07E0;
                        if (dwB < dwBMax) { dwBOut = dwB + dwBAdd; dwBOut &= 0x001F; } else dwBOut = 0x001F;
#endif                        
                    }

                    int iAlpha16 = wSrc & 0x00FF;

                    if (iAlpha16 < 16)
                    {
                        //
                        // Border Pixel, not fully opaque  
                        //
#ifdef IPHONE
                        dwROut = dwROut >> 8;
                        dwGOut = dwGOut >> 8;
                        dwBOut = dwBOut >> 8;
#endif

                        if (iHints & SHADEDIB_HINT_INT_OVER_EXT)
                        {
                            //
                            // We are drawing the interior section of a region that has an sunken border around us,
                            // combine with existing color and make result fully opaque
                            //
#ifdef IPHONE
                            DWORD wIn = *pTgt;
                            wIn = wIn >> 8;
                            int iAlpha256 = iAlpha16 << 4;
                            int _iAlpha256 = 256-iAlpha256;
                            DWORD dwRIn = wIn & 0x000000FF;
                            DWORD dwGIn = wIn & 0x0000FF00;
                            DWORD dwBIn = wIn & 0x00FF0000;
                            dwROut = (dwROut * iAlpha256 + dwRIn * _iAlpha256) & 0x0000FF00;
                            dwGOut = (dwGOut * iAlpha256 + dwGIn * _iAlpha256) & 0x00FF0000;
                            dwBOut = (dwBOut * iAlpha256 + dwBIn * _iAlpha256) & 0xFF000000;
                            *pTgt = dwROut|dwGOut|dwBOut|iAlpha256;
#endif
                        }
                        else
                        {
                            // Make semi-transparent
#ifdef IPHONE
                            int iAlpha256 = iAlpha16 << 4;
                            dwROut = (dwROut * iAlpha256) & 0x0000FF00;
                            dwGOut = (dwGOut * iAlpha256) & 0x00FF0000;
                            dwBOut = (dwBOut * iAlpha256) & 0xFF000000;
                            *pTgt = dwROut|dwGOut|dwBOut|iAlpha256;
#else
                            DWORD dwIn = *pTgt;
                            DWORD dwRB_T    = dwIn & 0xF81F;
                            DWORD dwG_T     = dwIn & 0x07E0;
                            DWORD dwRB_S    = dwROut|dwBOut;                        
                            int _iAlpha16 = 16-iAlpha16;
                            dwRB_T = ((dwRB_T * _iAlpha16 + dwRB_S * iAlpha16) >> 4) & 0xF81F;
                            dwG_T  = ((dwG_T  * _iAlpha16 + dwGOut * iAlpha16) >> 4) & 0x07E0;
                            *pTgt = (HL_PIXEL)(dwRB_T|dwG_T);
#endif
                        }
                    }
                    else
                    {
                        // Full opaque, shaded
#ifdef IPHONE
                        *pTgt = dwROut|dwGOut|dwBOut|0xFF;
#else
                        *pTgt = (HL_PIXEL)(dwROut|dwGOut|dwBOut);
#endif
                    }
                }
                else
                {
                    // Fully opaque interior 
                    *pTgt = wRGB;
                }
            }

            pTgt++;
            pSrc++;
        }
    }

    END_TIMER(GDI_SHADEPOLY);
}
/*========================================================================*/

    void                        CShadeDIB::ApplyToElev(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    COLORREF                    rgbMax,
    COLORREF                    rgbMin,
    int                         iRange )

/*========================================================================*/
{
    if (m_iMode & SHADEDIB_TRACKRECT)
    {
        if (!(m_iMode & SHADEDIB_INIT))
        {
            m_iMode |= SHADEDIB_INIT;
            CDXYRect rROI(0, 0, m_iImageDX, m_iDY);
            m_ROI.AddRectRef(&rROI);
            m_Solid.SubtractFrom(&m_ROI);
            m_ROI.Consolidate();
        }

        for (int i = 0; i < m_ROI.NRects(); i++)
        {
            CHLRect rROI = m_ROI.RectAtIndex(i);
            ApplyToElev(pDIB, iXDst+rROI.XLeft(), iYDst+rROI.YTop(), rROI.XLeft(), rROI.YTop(), rROI.DX(), rROI.DY(), rgbMax, rgbMin, iRange);
        }
    }
    else
    {
        ApplyToElev(pDIB, iXDst, iYDst, 0, 0, m_iImageDX, m_iDY, rgbMax, rgbMin, iRange);
    }
}
/*========================================================================*/

    void                        CShadeDIB::ApplyToElev(
    CHLDibDC                    *pDIB,
    int                         iXDst,
    int                         iYDst,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    COLORREF                    rgbMax,
    COLORREF                    rgbMin,
    int                         iRange  )

/*========================================================================*/
{
    if (!GetSafeDims(&iXSrc, &iYSrc, &iDX, &iDY, &iXDst, &iYDst, pDIB))
        return;

    HL_PIXEL wLast = 0xFFFF;
    HL_PIXEL wLastIn = 0xFFFF;

    CDitherMap **ppMAP = new CDitherMap*[iRange+1];

    int i = 0;

    for (i = 0; i <= iRange; i++)
    {
        COLORREF rgb = CHLDC::BlendPCT(rgbMin, rgbMax, i * 100 / iRange);
        ppMAP[i] = new CDitherMap(rgb);
    }
    
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pSrc = Pixel(iXSrc, iY+iYSrc);
        HL_PIXEL *pTgt = pDIB->Pixel(iXDst, iY+iYDst);

        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wSrc = *pSrc;                
            int iIndex = wSrc & 0x00FF;
            CDitherMap *pMAP = ppMAP[iIndex];

            if (wSrc & PIXEL_ORTHO)
            {
                int iDXMax = iDX-iX;
                int iDYMax = iDY-iY;
                int iNDX = FillOrthoElev(pDIB, pSrc, pTgt, iX, iY, iDXMax, iDYMax, pDIB->DX(), pMAP);

                pTgt += iNDX;
                pSrc += iNDX;
                iX += (iNDX-1);
            }
            else
            {
                if (wSrc != 0)
                {
                    if (wSrc != wLastIn)
                    {
                        LOGASSERT(wSrc <= (HL_PIXEL)iRange);
                        wLast = pMAP->NextColor();
                        wLastIn = wSrc;
                    }
                    *pTgt = wLast;
                }

                pTgt++;
                pSrc++;
            }

        }
    }

    for (i = 0; i <= iRange; i++)
    {
        delete ppMAP[i];
    }

    delete [] ppMAP;
}
/*========================================================================*/

    int                         CShadeDIB::FillOrthoElev(
    CHLDibDC                    *pDIB,
    HL_PIXEL                    *pSrc,
    HL_PIXEL                    *pTgt,
    int                         iX,
    int                         iY,
    int                         iDXMax,
    int                         iDYMax,
    int                         iPitchTarget,
    CDitherMap                   *pMAP )

/*========================================================================*/
{
    static BOOL bOdd = TRUE;
    bOdd = !bOdd;

    HL_PIXEL wThisType = *pSrc;
    int iNCols = 0;
    int iN = 0;
    if (wThisType & PIXEL_VT)
    {
        iNCols = 1;
        iN = iDYMax;
    }
    else
    {
        iN = iDXMax;
    }

    HL_PIXEL wThisElev = wThisType & 0x0FFF;

    if (wThisType & PIXEL_VT)
    {
        while (iN > 0)
        {
            // Same elevation?
            if (wThisElev != (*pSrc & 0x0FFF))
                return 1;

            *pTgt = pMAP->NextColor();

            // Mark this source pixel so we don't hit it again
            *pSrc = 0;

            // Next Row
            pTgt += iPitchTarget;
            pSrc += m_iDX;
            iN--;
        }
    }
    else
    {
        while (iN > 0)
        {
            // Same elevation?
            if (wThisElev != (*pSrc & 0x0FFF))
            {
                pMAP->FillHZ(pTgt, iNCols);
                return iNCols;
            }

            // Next Col
            iNCols++;
            pSrc ++;
            iN--;
        }

        pMAP->FillHZ(pTgt, iNCols);
    }

    return iNCols;
}
/*========================================================================*/

    CEdgeList                   *CShadeDIB::GenEdgeListByElev(
    int                         iMinElev)

/*========================================================================*/
{
    CEdgeList *pEdges = new CEdgeList(CEdgeList::EDGE_HORIZONTAL);
    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pTest = Pixel(0, iY);
        BOOL bIn = *pTest & 0x00FF;
        int iXStart = 0;
        pTest++;
        for (int iX = 1; iX < m_iImageDX; iX++)
        {
            BOOL bInNow = (int)(*pTest & 0x00FF) > iMinElev;
            if (!bInNow && bIn)
            {
                pEdges->AddEdge(iXStart, iY, iX-iXStart);
            }
            if (bInNow && !bIn)
            {
                iXStart = iX;
            }

            bIn = bInNow;
            pTest++;
        }
        if (bIn)
        {
            pEdges->AddEdge(iXStart, iY, m_iImageDX-iXStart);
        }

    }

    return pEdges;
}
/*========================================================================*/

    CEdgeList                   *CShadeDIB::GenEdgeListByElevSunken()

/*========================================================================*/
{
    CEdgeList *pEdges = new CEdgeList(CEdgeList::EDGE_HORIZONTAL);
    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pTest = Pixel(0, iY);
        BOOL bIn = ((*pTest & 0x00FF) != 0x00FF);
        int iXStart = 0;
        pTest++;
        for (int iX = 1; iX < m_iImageDX; iX++)
        {
            BOOL bInNow = (*pTest & 0x00FF) != 0x00FF;
            if (!bInNow && bIn)
            {
                pEdges->AddEdge(iXStart, iY, iX-iXStart);
            }
            if (bInNow && !bIn)
            {
                iXStart = iX;
            }

            bIn = bInNow;
            pTest++;
        }
        if (bIn)
        {
            pEdges->AddEdge(iXStart, iY, m_iImageDX-iXStart);
        }

    }

    return pEdges;
}
/*========================================================================*/

    CEdgeList                   *CShadeDIB::GenEdgeListByFloor()

/*========================================================================*/
{
    CEdgeList *pEdges = new CEdgeList(CEdgeList::EDGE_HORIZONTAL);
    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pTest = Pixel(0, iY);
        BOOL bIn = *pTest == 0x0000;
        int iXStart = 0;
        pTest++;
        for (int iX = 1; iX < m_iImageDX; iX++)
        {
            BOOL bInNow = *pTest == 0;
            if (!bInNow && bIn)
            {
                pEdges->AddEdge(iXStart, iY, iX-iXStart);
            }
            if (bInNow && !bIn)
            {
                iXStart = iX;
            }

            bIn = bInNow;
            pTest++;
        }
        if (bIn)
        {
            pEdges->AddEdge(iXStart, iY, m_iImageDX-iXStart);
        }

    }

    return pEdges;
}
/*========================================================================*/

    CEdgeList                   *CShadeDIB::GenEdgeListByBoundingElevRaised()

/*========================================================================*/
{
    CEdgeList *pEdges = new CEdgeList(CEdgeList::EDGE_HORIZONTAL);
    for (int iY = 0; iY < m_iDY; iY++)
    {
        int iX0 = 0;
        int iX1 = ImageDX()-1;
        HL_PIXEL *pTest = Pixel(0, iY);
        while (!(*pTest & 0x00FF) && iX0 < m_iImageDX)
        {
            iX0++;
            pTest++;
        }
        pTest = Pixel(m_iImageDX-1, iY);
        while (!(*pTest & 0x00FF) && iX1 > 0)
        {
            iX1--;
            pTest--;
        }
        if (iX0 < iX1)
        {
            pEdges->AddEdge(iX0, iY, iX1-iX0);
        }
    }

    return pEdges;
}
/*========================================================================*/

    CEdgeList                   *CShadeDIB::GenEdgeListByBoundingElevSunken()

/*========================================================================*/
{
    CEdgeList *pEdges = new CEdgeList(CEdgeList::EDGE_HORIZONTAL);
    for (int iY = 0; iY < m_iDY; iY++)
    {
        int iX0 = 0;
        int iX1 = ImageDX()-1;
        HL_PIXEL *pTest = Pixel(0, iY);
        while (((*pTest & 0x00FF) == 0x00FF) && iX0 < m_iImageDX)
        {
            iX0++;
            pTest++;
        }
        pTest = Pixel(m_iImageDX-1, iY);
        while (iX1 > 0 && ((*pTest & 0x00FF) == 0x00FF))
        {
            iX1--;
            pTest--;
        }
        if (iX0 < iX1)
        {
            pEdges->AddEdge(iX0, iY, iX1-iX0+1);
        }
    }

    return pEdges;
}
/*========================================================================*/

    CHLRect                     CShadeDIB::FindContentRect(
    CEdgeList                   *pEdges )

/*========================================================================*/
{
    if (pEdges->NPoints() < 1)
        return CDXYRect(0, 0, DX(), DY());

    CHLRect rBounds = pEdges->GetBounds();
    int iXMin = DX();
    int iXMax = 0;
    int iYMin = DY();
    int iYMax = 0;
    int iDXMin = rBounds.DX() / 2;
    int iDYMin = rBounds.DY() / 2;
    for (int iX = 0; iX < m_iImageDX; iX++)
    {
        HL_PIXEL *pPIX = Pixel(iX, 0);
        int iY0 = 0;
        
        while (iY0 < m_iDY-1 && (*pPIX & 0x00FF) == 0)
        {
            iY0++;
            pPIX += m_iDX;
        }
        if (*pPIX & 0x00FF)
        {
            int iY1 = iY0;
            while (iY1 < m_iDY && (*pPIX & 0x00FF) != 0)
            {
                iY1++;
                pPIX += m_iDX;
            }
            int iDY = iY1-iY0;
            if (iDY > iDYMin)
            {
                iXMin = __min(iXMin, iX);
                iXMax = __max(iXMax, iX);
            }
        }
    }

    for (int iY = 0; iY < m_iDY; iY++)
    {
        HL_PIXEL *pPIX = Pixel(0, iY);
        int iX0 = 0;
        
        while (iX0 < (m_iImageDX-1) && (*pPIX & 0x00FF) == 0)
        {
            iX0++;
            pPIX++;
        }

        if (*pPIX & 0x00FF)
        {
            int iX1 = iX0;
            while (iX1 < m_iImageDX && (*pPIX & 0x00FF) != 0)
            {
                iX1++;
                pPIX++;
            }
            int iDX = iX1-iX0;
            if (iDX > iDXMin)
            {
                iYMin = __min(iYMin, iY);
                iYMax = __max(iYMax, iY);
            }
        }
    }


    return CDXYRect(iXMin, iYMin, iXMax-iXMin+1, iYMax-iYMin+1);
}
#ifdef DEBUG
/*========================================================================*/

    void                    CShadeDIB::DebugElev(
    CHLDC                   *pDC,
    int                     iMaxElev )

/*========================================================================*/
{
    if (iMaxElev == 0)
        iMaxElev = 0xFF;
    CHLDibDC dib(ImageDX(), DY(), 0, 0, MEMORY_SYSTEM);
    dib.FillRect(0x0000, 0, 0, ImageDX(), DY());
    HL_PIXEL *pOut = dib.Pixel(0,0);    
    HL_PIXEL *pSrc = Pixel(0, 0);
    int iNPIX = m_iDX * m_iDY;
    for (int i = 0; i < iNPIX; i++)
    {
        int iElev = (*pSrc & 0x00FF);
        int iPCT = 0;
        if (iElev < 16)
            iPCT = iElev * 100 / iMaxElev / 16;
        else
        {
            iElev -= 15;
            iPCT = iElev * 100 / iMaxElev;
        }

        COLORREF rgb = BlendPCT(RGB(0,0,255), RGB(255,0,0), iPCT);
//        *pOut = RGBTOHLPIXEL(rgb);
        if (iElev == 0x00FF)
        {
            *pOut = RGBTOHLPIXEL(RGB(255,0,0));
        }
        else if (iElev == 0)
        {
            *pOut = RGBTOHLPIXEL(RGB(0,0,255));
        }

        if (iElev != 0 && iElev != 0x00FF)
        {
            *pOut = RGBTOHLPIXEL(rgb);;
        }


        pOut++;
        pSrc++;
    }

    pDC->BltFromDibDC(&dib, 0, 0);
}
/*========================================================================*/

    void                    CShadeDIB::DebugShade(
    CHLDC                   *pDC,
    int                     iMaxElev )

/*========================================================================*/
{
    if (iMaxElev == 0)
        iMaxElev = 0xFF;
    CHLDibDC dib(ImageDX(), DY(), 0, 0, MEMORY_SYSTEM);
    dib.FillRect(RGB(0,0,255), 0, 0, ImageDX(), DY());
    ApplyTo(&dib, 0, 0);
    HL_PIXEL *pOut = dib.Pixel(0,0);    
    HL_PIXEL *pSrc = Pixel(0, 0);
    int iNPIX = m_iDX * m_iDY;
    for (int i = 0; i < iNPIX; i++)
    {
        int iElev = (*pSrc & 0x00FF);
        if (iElev == 0)
            // Top Level Surface
            *pOut = 0x0000;

        pOut++;
        pSrc++;
    }

    pDC->BltFromDibDC(&dib, 0, 0);
}
/*========================================================================*/

    void                    CShadeDIB::DebugROI(
    CHLDC                   *pDC )

/*========================================================================*/
{
    for (int i = 0; i < m_ROI.NRects(); i++)
    {
        CHLRect r = m_ROI.RectAtIndex(i);
        pDC->DrawRect(RGB(255,255,0), r);
    }
/*
    for (int i = 0; i < m_Solid.NRects(); i++)
    {
        CHLRect r = m_Solid.RectAtIndex(i);
        pDC->DrawRect(RGB(0,255,0), r);
    }
*/
}
/*========================================================================*/

    void                    CShadeDIB::DebugSolid(
    CHLDC                   *pDC )

/*========================================================================*/
{
    for (int i = 0; i < m_Solid.NRects(); i++)
    {
        CHLRect r = m_Solid.RectAtIndex(i);
        pDC->FillRect(RGB(0,255,0), r);
    }
}
#endif
/*========================================================================*/

    BOOL                        CShadeDIB::IsInterior(
    INIT_TYPE                   Type )

/*========================================================================*/
{
    switch (Type)
    {
    case INIT_RAISED_INT:
    case INIT_RAISED_INT_INV:
    case INIT_RAISED_INT_GRADIENT:
        return TRUE;
    default:
        break;
    }

    return FALSE;
}
/*========================================================================*/

    ShaderFunc                  *CShadeDIB::ShaderFor(
    int                         iQ,
    INIT_TYPE                   Type )

/*========================================================================*/
{
    if (m_iMode & SHADEDIB_DIRECT)
    {
        if (Type == CShadeDIB::INIT_RAISED_INT_GRADIENT)
        {
            switch (iQ)
            {
            case 1:
            case 2:
                return ShadeValueQ12_ELEVDirectColorGrad;
                
            case 3:
            case 4:
                return ShadeValueQ34_ELEVDirectColorGrad;
            }
        }

        if (IsInterior(Type))
        {
            switch (iQ)
            {
            case 1:
            case 2:
                return ShadeValueQ12_ELEVDirectColor;
                
            case 3:
            case 4:
                return ShadeValueQ34_ELEVDirectColor;
            }
        }

        switch (iQ)
        {
        case 1:
        case 2:
            return ShadeValueQ12_ELEVDirect;
            
        case 3:
        case 4:
            return ShadeValueQ34_ELEVDirect;
        }
    }

    switch (iQ)
    {
    case 1:
    case 2:
        return ShadeValueQ12_ELEV;
        
    case 3:
    case 4:
        return ShadeValueQ34_ELEV;
    }

    LOGASSERT(FALSE);
    return ShadeValueQ12_ELEV;
}
/*========================================================================*/

    int                         CShadeDIB::MaxShineInt(
    int                         iMaxElev )

/*========================================================================*/
{
    switch (iMaxElev)
    {   
    case 0:
        return 0;
    case 1:
        return (SHINE_INT * 128) >> 8;
    case 2:
        return (SHINE_INT * 175) >> 8;
    case 3:
        return (SHINE_INT * 200) >> 8;
    }

    return SHINE_INT;
}
/*========================================================================*/

    HLT_SCANLINE_ROI            CShadeDIB::GetScanlineROIQ2(
    int                         iY,
    int                         iRad2_Plus,
    int                         iRad,
    int                         iNShading )

/*========================================================================*/
{
//    int iRad2_Plus  = (iRad+2)*(iRad+1);

    // Get iDX to outer radius
    int iDY = iRad-iY;
    int iDY_2 = iDY*iDY;
    int iDXOuter = __min(iRad, iSqrt(iRad2_Plus-iDY_2));

    HLT_SCANLINE_ROI ret;
    ret.m_iX0 = iRad-iDXOuter;

    // Are we 
    int iRadInner = iRad-iNShading;
    if (iY < (iNShading+1))
    {
        ret.m_iX1 = iRad+1; // Exclusive
        return ret;
    }

    int iDXInner = iSqrt(iRadInner*iRadInner-iDY_2);
    iDXInner = __max(0, iDXInner-1);
    ret.m_iX1 = iRad + 1 - iDXInner;
    return ret;
}
/*========================================================================*/

    HL_PIXEL                    CShadeDIB::GradientAt(
    int                         iY,
    int                         iInsets )

/*========================================================================*/
{
    int iDYGrad = m_iDY - 2 * iInsets;
    #ifdef HL_PIXEL32
        int iY255 = (iY - iInsets) * 0xFF / iDYGrad;
        int _iY255 = 0xFF - iY255;
        DWORD dwRG = ((m_iRBT * _iY255) + (m_iRBB * iY255)) & 0xFF00FF00;
        DWORD dwG  = ((m_iGT * _iY255) + (m_iGB * iY255))   & 0x00FF0000;
    #else
        int iY64 = (iY - iInsets) * 64 / iDYGrad;
        int _iY64 = 64 - iY64;

        DWORD dwRG = (((m_iRBT * _iY64) + (m_iRBB * iY64)) >> 6) & 0xF81F;
        DWORD dwG  = (((m_iGT * _iY64)  + (m_iGB  * iY64)) >> 6) & 0x07E0;
    #endif

    HL_PIXEL wSet = (HL_PIXEL)(dwRG|dwG);

#ifdef IPHONE
    wSet |= 0x000000FF;
#endif

    return wSet;
}