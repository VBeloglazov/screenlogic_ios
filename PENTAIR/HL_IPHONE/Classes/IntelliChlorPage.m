//
//  IntelliChlorPage.m
//  XPAD_VIEW
//
//  Created by dmitriy on 9/1/14.
//
//

#import "IntelliChlorPage.h"
#import "crect.h"
#import "MainView.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "NSMSG.h"
#import "NSPoolConfig.h"
#import "CustomPage.h"
#import "hlbutton.h"


#define CMD_POOL_UP 1000
#define CMD_POOL_DN 1001
#define CMD_SPA_UP  1002
#define CMD_SPA_DN  1003

@implementation CStatusItem2


/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame Label:(NSString*)pLabel HasSubValue:(BOOL)bSubValue HasLevel:(BOOL)bHasLevel

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        int iSize = 32;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            iSize = 48;
        }
        
        m_pLabel = [self AddLabel:pLabel Size:iSize];
        m_pLabel.numberOfLines = 1;
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        
        m_pVal1  = [self AddLabel:@"" Size:iSize];
        m_pVal1.alpha = .6;
        m_pVal1.textAlignment = UITextAlignmentCenter;
        
        //if (bSubValue)
        //{
        //     UIColor* colorRed = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        //     m_pVal2  = [self AddLabel:@"" Size:[CMainView DEFAULT_TEXT_SIZE]];
        //     m_pVal2.alpha = .6;
        //     [m_pVal2 setTextColor:colorRed];
        //}
        
        //if (bHasLevel)
        //{
        //    m_pProgress  = [[UIProgressView alloc] initWithFrame:CGRectZero];
        //    [self addSubview:m_pProgress];
        //    [m_pProgress release];
        //}
        
        m_pLabel.textAlignment = UITextAlignmentLeft;
        
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(UILabel*)AddLabel:(NSString*)pText Size:(int)iSize

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [CCustomPage InitStandardLabel:pLabel Size:iSize];
    [pLabel setText:pText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(CGRect)getLabelFrame

/*============================================================================*/
{
    CGRect result = m_pLabel.frame;
    return result;
}
/*============================================================================*/

-(CGRect)getValFrame

/*============================================================================*/
{
    CGRect result = m_pVal1.frame;
    return result;
}

/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    CGRect rLabel = rBounds;
    [CRect BreakOffRight:&rLabel DX:100];
    [CRect BreakOffLeft:&rBounds DX:rBounds.size.width * 50 / 100];
    [m_pLabel setFrame:rLabel];
    
    if (m_pProgress != NULL)
    {
        CGRect rBoundsTemp = self.bounds;
        CGRect rLevel = [CRect BreakOffBottom:&rBoundsTemp DY:[CMainView DEFAULT_TEXT_SIZE]];
        
        int iDXSubVal = 60;
        int iDXProgress = 100;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            iDXSubVal = 100;
            iDXProgress = 200;
        }
        
        CGRect rSubVal2 = [CRect BreakOffLeft:&rLevel DX:iDXSubVal];
        CGRect rProgress = [CRect BreakOffLeft:&rLevel DX:iDXProgress];
        rProgress.origin.y += 4;
        [m_pVal2 setFrame:rSubVal2];
        [m_pProgress setFrame:rProgress];
    }
    else if (m_pVal2 != NULL && ![[m_pVal2 text]  isEqual: @""]);
    {
        CGRect rBoundsCopy = rBounds;
        
        [CRect CenterDownToY:&rBoundsCopy DY:([CMainView DEFAULT_TEXT_SIZE]+48)];
        CGRect rSubVal = [CRect BreakOffBottom:&rBoundsCopy DY:[CMainView DEFAULT_TEXT_SIZE]];
        rSubVal.origin.y += rSubVal.size.height;
        [m_pVal2 setFrame:rSubVal];

    }
    
    [m_pVal1 setFrame:rBounds];
}
/*============================================================================*/

-(void)SetData:(NSString*)pText

/*============================================================================*/
{
    [m_pVal1 setText:pText];
}

/*============================================================================*/

-(void)SetColor:(UIColor*)color

/*============================================================================*/
{
    [m_pVal1 setTextColor:color];
}

/*============================================================================*/

-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2

/*============================================================================*/
{
    [m_pVal1 setText:pText1];
    [m_pVal2 setText:pText2];
}
/*============================================================================*/

-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2 Level:(int)iLevel

/*============================================================================*/
{
    [m_pVal1 setText:pText1];
    [m_pVal2 setText:pText2];
    float fLevel = (float)iLevel / 100.f;
    [m_pProgress setProgress:fLevel];
}
@end

@implementation CIntelliChlorPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig IsActive:(BOOL)isActive;

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pConfig = pConfig;
        showValues = isActive;
        poolLevel = -1;
        spaLevel = -1;
        
        BOOL bORPTank = TRUE;
        isChlorinatorPresent = TRUE;
        if ([m_pConfig EquipPresent:POOL_CHLORPRESENT])
        {
            bORPTank = FALSE;
        }
        else
        {
            isChlorinatorPresent = FALSE;
        }
        
        
        m_pPoolOutput   = [[CStatusItem2 alloc] initWithFrame:CGRectZero Label:@"Pool Output" HasSubValue:FALSE HasLevel:FALSE];
        m_pSpaOutput  = [[CStatusItem2 alloc] initWithFrame:CGRectZero Label:@"Spa Output" HasSubValue:FALSE HasLevel:FALSE];
        m_pSalt = [[CStatusItem2 alloc] initWithFrame:CGRectZero Label:@"Salt Level" HasSubValue:FALSE HasLevel:FALSE];
        [self addSubview:m_pPoolOutput];
        [self addSubview:m_pSpaOutput];
        [self addSubview:m_pSalt];
        [m_pPoolOutput release];
        [m_pSpaOutput release];
        [m_pSalt release];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            m_pICControl1 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
            m_pICControl1.textAlignment = UITextAlignmentRight;
            m_pICControl1.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
            m_pICControl1.text = @"Intellichem in Control";
            [self addSubview:m_pICControl1];

            m_pICControl2 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
            m_pICControl2.textAlignment = UITextAlignmentRight;
            m_pICControl2.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
            m_pICControl2.text = @"Intellichem in Control";
            [self addSubview:m_pICControl2];
        }
        else
        {
            m_pICControl1 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
            m_pICControl1.textAlignment = UITextAlignmentLeft;
            m_pICControl1.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
            m_pICControl1.text = @"Intellichem";
            [self addSubview:m_pICControl1];
            
            m_pICControl1_Line2 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
            m_pICControl1_Line2.textAlignment = UITextAlignmentLeft;
            m_pICControl1_Line2.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
            m_pICControl1_Line2.text = @"in Control";
            [self addSubview:m_pICControl1_Line2];

            
            m_pICControl2 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
            m_pICControl2.textAlignment = UITextAlignmentLeft;
            m_pICControl2.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
            m_pICControl2.text = @"Intellichem";
            [self addSubview:m_pICControl2];

            m_pICControl2_Line2 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
            m_pICControl2_Line2.textAlignment = UITextAlignmentLeft;
            m_pICControl2_Line2.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
            m_pICControl2_Line2.text = @"in Control";
            [self addSubview:m_pICControl2_Line2];
        }

        
       if(showValues)
       {
           UIColor *pRGBTint = [UIColor colorWithRed:.1 green:0.0 blue:0.0 alpha:0.0];
        
           buttonPoolUp = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYUP Text:NULL TextSize:40 TextColor:NULL Color:pRGBTint Icon:NULL];
           buttonPoolDn = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYDN Text:NULL TextSize:40 TextColor:NULL Color:pRGBTint Icon:NULL];
           [buttonPoolUp SetCommandID:CMD_POOL_UP Responder:self];
           [buttonPoolDn SetCommandID:CMD_POOL_DN Responder:self];
           //[m_pPoolUp SetRepeatDwell:300 Rate:4];
           //[m_pPoolDn SetRepeatDwell:300 Rate:4];
        
           buttonSpaUp = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYUP Text:NULL TextSize:40 TextColor:NULL Color:pRGBTint Icon:NULL];
           buttonSpaDn = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYDN Text:NULL TextSize:40 TextColor:NULL Color:pRGBTint Icon:NULL];
           [buttonSpaUp SetCommandID:CMD_SPA_UP Responder:self];
           [buttonSpaDn SetCommandID:CMD_SPA_DN Responder:self];
           //[m_pPoolUp SetRepeatDwell:300 Rate:4];
           //[m_pPoolDn SetRepeatDwell:300 Rate:4];

        
           [self addSubview:buttonPoolUp];
           [self addSubview:buttonPoolDn];
           [self addSubview:buttonSpaUp];
           [self addSubview:buttonSpaDn];
       }

        
         m_pQueryTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(QueryChlorData) userInfo:nil repeats:YES] retain];
         
    }
    return self;
}
/*============================================================================*/

-(void)QueryChlorData

/*============================================================================*/
{
    int temPoolLevel;
    int tempSpaLevel;
    
    CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_POOL_GETSCGCONFIGQ] autorelease];
    [pQ1 PutInt:0];
    
    if ([pQ1 AskQuestion])
    {
        [pQ1 GetInt];
        [pQ1 GetInt];
        //poolRealOutput = [pQ1 GetInt];
        //spaRealOutput = [pQ1 GetInt];
        temPoolLevel = [pQ1 GetInt];
        tempSpaLevel = [pQ1 GetInt];
        saltLevel = [pQ1 GetInt];
        bSuper = [pQ1 GetInt];
        iSuper = [pQ1 GetInt];
    }
    
    NSString *pPoolPercent  = @"";
    NSString *pSpaPercent   = @"";
    NSString *pSalt         = @"";
    
    
    if(isPoolClick)
    {
        poolFeedbackCount++;
        if(poolFeedbackCount > 30)
        {
            poolRealOutput = temPoolLevel;
            poolLevel = temPoolLevel;
            poolFeedbackCount = 0;
            isPoolClick = false;
        }
        else
        {
            poolRealOutput = poolLevel;
        }
    }
    else
    {
        poolRealOutput = temPoolLevel;
        poolLevel = temPoolLevel;
        poolFeedbackCount = 0;
        isPoolClick = false;
    }
    poolLevel = poolLevel < 0?poolRealOutput:poolLevel;

    
    if(isSpaClick)
    {
        spaFeedbackCount++;
        if(spaFeedbackCount > 30)
        {
            spaRealOutput = tempSpaLevel;
            spaLevel = tempSpaLevel;
            spaFeedbackCount = 0;
            isSpaClick = false;
        }
        else
        {
            spaRealOutput = spaLevel;
        }
    }
    else
    {
        spaRealOutput = tempSpaLevel;
        spaLevel = tempSpaLevel;
        spaFeedbackCount = 0;
        isSpaClick = false;
    }
    spaLevel = spaLevel < 0?spaRealOutput:spaLevel;
  
    
    pPoolPercent = [NSString stringWithFormat:@"%d%s", poolRealOutput, "%"];
    pSpaPercent     = [NSString stringWithFormat:@"%d%s", spaLevel, "%"];
    pSalt           = [NSString stringWithFormat:@"%d", saltLevel * 50];
    
    if(!showValues)
    {
        pPoolPercent = @"";
        pSpaPercent = @"";
    }
    
    [m_pPoolOutput   SetData:pPoolPercent];
    [m_pSpaOutput  SetData:pSpaPercent];
    [m_pSalt SetData:pSalt];

}

/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
    [m_pConfig RemoveSink:self];
    [buttonPoolUp release];
    [buttonPoolDn release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    [CRect Inset:&rBounds DX:10 DY:0];
    [m_pPoolOutput   setFrame:[CRect CreateYSection:rBounds Index:0 Of:4]];
    [m_pSpaOutput  setFrame:[CRect CreateYSection:rBounds Index:1 Of:4]];
    [m_pSalt setFrame:[CRect CreateYSection:rBounds Index:2 Of:4]];
    
    CGRect r1 = [m_pPoolOutput frame];
    int y1 = r1.origin.y + r1.size.height - (r1.size.height/100) * 25;
    int x1 = r1.size.height/(2.5);//2;
    
    [m_pPoolOutput layoutSubviews];
    //int xLeft1 = (self.frame.size.width - x1 * 2)/1.2;///2;
    CGRect rVPool =[m_pPoolOutput getValFrame];
    int xLeft1 = rVPool.origin.x + (rVPool.size.width - x1 * 2)/2;
    
    CGRect rPoolUp      = CGRectMake(xLeft1 + x1, y1, x1, x1);
    [buttonPoolUp setFrame:rPoolUp];
    
    CGRect rPoolDn      = CGRectMake(xLeft1, y1, x1, x1);
    [buttonPoolDn setFrame:rPoolDn];
    
    CGRect r2 = [m_pSpaOutput frame];
    int y2 = r2.origin.y + r2.size.height - (r2.size.height/100) * 25;
    int x2 = r2.size.height/(2.5);
 
    [m_pSpaOutput layoutSubviews];
    //int xLeft2 = (self.frame.size.width - x2 * 2)/1.2;///2;
    CGRect rVSpa =[m_pSpaOutput getValFrame];
    int xLeft2 = rVSpa.origin.x + (rVSpa.size.width - x2 * 2)/2;
   
    CGRect rSpaUp      = CGRectMake(xLeft2 + x2, y2, x2, x2);
    [buttonSpaUp setFrame:rSpaUp];
    
    CGRect rSpaDn      = CGRectMake(xLeft2, y2, x2, x2);
    [buttonSpaDn setFrame:rSpaDn];
    
    if(!showValues)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGRect rLabel1;
            CGRect rLabel2;
            
            rLabel1 = [m_pPoolOutput frame];
            rLabel1.origin.x -= rLabel1.size.width/32;
            [m_pICControl1 setFont:[UIFont systemFontOfSize:[CMainView DEFAULT_TEXT_SIZE] * 1.6]];
        
            rLabel2 = [m_pSpaOutput frame];
            rLabel2.origin.x -= rLabel2.size.width/32;
            [m_pICControl2 setFont:[UIFont systemFontOfSize:[CMainView DEFAULT_TEXT_SIZE] * 1.6]];

            [m_pICControl1 setFrame:rLabel1];
            [m_pICControl2 setFrame:rLabel2];
        }
        else
        {
            CGRect rLabel1;
            CGRect rlabel1_Line2;
            
            CGRect rLabel2;
            CGRect rlabel2_Line2;
            
            rLabel1 = [m_pPoolOutput frame];
            rLabel1.origin.x = 200;
            rLabel1.size.height += 50;
            rLabel1.origin.y -=  30;
            [m_pICControl1 setFont:[UIFont systemFontOfSize:[CMainView DEFAULT_TEXT_SIZE]]];
            
            rlabel1_Line2 = [m_pPoolOutput frame];
            rlabel1_Line2.origin.x = 200;
            rlabel1_Line2.size.height += 50;
            rlabel1_Line2.origin.y -= 10;
            [m_pICControl1_Line2 setFont:[UIFont systemFontOfSize:[CMainView DEFAULT_TEXT_SIZE]]];
            
            [m_pICControl1 setFrame:rLabel1];
            [m_pICControl1_Line2 setFrame:rlabel1_Line2];
            
            
            rLabel2 = [m_pSpaOutput frame];
            rLabel2.origin.x = 200;
            rLabel2.size.height += 50;
            rLabel2.origin.y -=  30;
            [m_pICControl2 setFont:[UIFont systemFontOfSize:[CMainView DEFAULT_TEXT_SIZE]]];
            
            rlabel2_Line2 = [m_pSpaOutput frame];
            rlabel2_Line2.origin.x = 200;
            rlabel2_Line2.size.height += 50;
            rlabel2_Line2.origin.y -= 10;
            [m_pICControl2_Line2 setFont:[UIFont systemFontOfSize:[CMainView DEFAULT_TEXT_SIZE]]];
            
            [m_pICControl2 setFrame:rLabel2];
            [m_pICControl2_Line2 setFrame:rlabel2_Line2];
        }

    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}

/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch(iCommandID)
    {
        case CMD_POOL_UP:
        {
            poolLevel = poolLevel < 100? ++poolLevel: 100;
            poolFeedbackCount = 0;
            isPoolClick = true;
        }
            break;
        case CMD_POOL_DN:
        {
            poolLevel = poolLevel > 0? --poolLevel: 0;
            poolFeedbackCount = 0;
            isPoolClick = true;
        }
            break;
        case CMD_SPA_UP:
        {
            spaLevel = spaLevel < 100? ++spaLevel: 100;
            spaFeedbackCount = 0;
            isSpaClick = true;
        }
            break;
        case CMD_SPA_DN:
        {
            spaLevel = spaLevel > 0? --spaLevel: 0;
            spaFeedbackCount = 0;
            isSpaClick = true;
       }
            break;
        default:
            break;
    }

    [m_pPoolOutput SetData:[NSString stringWithFormat:@"%d%s", poolLevel, "%"]]; // Sub:[NSString stringWithFormat:@"%d%s", poolLevel, "%"]];
    [m_pSpaOutput SetData:[NSString stringWithFormat:@"%d%s", spaLevel, "%"]];
    
    CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_POOL_SETSCGCONFIGQ] autorelease];
    [pQ1 PutInt:0];
    [pQ1 PutInt:poolLevel];
    [pQ1 PutInt:spaLevel];
    [pQ1 PutInt:bSuper];
    [pQ1 PutInt:iSuper];
    
    [pQ1 SendMessage];
}

/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
}

@end
