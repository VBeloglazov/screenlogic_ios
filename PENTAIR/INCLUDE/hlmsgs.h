/*
--------------------------------------------------------------------------------

    HLMSGS.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

This file contains the port and message information used by the server and the
clients (tablets).  See also STRUCTS.H for the data structures used to pass
the parameters back and forth.
--------------------------------------------------------------------------------
*/
#ifndef HLMSGS_H
#define HLMSGS_H


//
// The messages are NOW all defined in HLM.H, in INCLUDE, HLM.JAVA will be generated dynamically
//
#include "hlm.h"

//
// Port info.
//
#define HL_BASE_PORT            80

#define HL_DEFAULT_PORT(x)      (x+0)
#define HL_PING_PORT(x)         444
#define HL_KEEPALIVE_PORT(x)    453

//
// HL defined custom Windows messages
//
#define WM_PING_GATEWAY         (WM_USER+1)
#define WM_KILL_GATEWAY         (WM_USER+2)
#define WM_TELEPHONE_SERV_EVENT (WM_USER+3)
#define WM_SYSTRAY              (WM_USER+4)
#define WM_GATEWAY_ENTERBUSY    (WM_USER+5)
#define WM_SOCKETINFO           (WM_USER+6)

//
// When a tablet wants to connect to a server, it pings the server on the
// "BroadcastPort".  The "ping packet" contains a packet which is 8 bytes
// long, and contains the message id (HL_ADD_CLIENTQ).
//
// The server picks up the broadcast, and determines the tablets IP address,
// and then sends back a response. The response includes the gateway IP address
// to work around a problem where recvfrom() does not get the correct address
// for two programs on the same machine.
//
// Data from tablet to gateway:
//  Byte0-3     HL_ADD_CLIENTQ
//  Byte4-7     Not used
//
// Data from gateway to tablet:
//  Byte0-3     HL_ADD_CLIENTA
//  Byte4-7     IP address of gateway
//
// Unlike the other HLM_ messages below, the HLM_ADD_CLIENTQ/A are sent from the
// tablet to the gateway and back over the broadcast port.
//
#define HL_SIZE_PINGPACKETQ                 8
#define HL_SIZE_PINGPACKETA                 40

#define HL_KEEPALIVE_PACKETSIZE             (sizeof(SYSTEMTIME)+(2*sizeof(int)))


//
// Default buffer sizes.
//
#ifdef HLSTART
    // Needs to be big here to accomidate old system file transfers
    #define HL_MAX_MESSAGE_SIZE                 2000000
#else
    #ifdef CRYSTALPAD
        // Should be more than enough for newer systems, right now this only needs
        // large to handle screen saver image transfers
        #define HL_MAX_MESSAGE_SIZE                 1000000
    #elif HLOSD
        // Some JPEGS are still pretty big so need some room here
        #define HL_MAX_MESSAGE_SIZE                 2000000
    #else
        #ifdef GATEWAY
            #define HL_MAX_MESSAGE_SIZE                 2000000
        #elif HLFILE
            #define HL_MAX_MESSAGE_SIZE                 2000000
        #else
            #ifdef HLSURVEY
                #define HL_MAX_MESSAGE_SIZE                 100000
            #else
                #define HL_MAX_MESSAGE_SIZE                 500000
            #endif
        #endif
    #endif
#endif

#define HL_MAX_NTABLETS                     32
#define HL_MAX_HOST_NAME                    256



//
// Timeouts: all in milliseconds (approx).
//
#define HL_TIMEOUT_FIND_SERVER              1000    // How often to look for the gateway
#define HL_TIMEOUT_CHECK_NEW_CLIENT         500     // How often to look for new clients
#define HL_TIMEOUT_PING                     250     // How long to wait for response from server
#ifdef HLCONFIG
#define HL_TIMEOUT_QUESTION                 30000   // How long to wait for an answer
#else
#define HL_TIMEOUT_QUESTION                 30000   // How long to wait for an answer
#endif
#define HL_TIMEOUT_IDLE                     50      // How long to wait after socket activity before allowing sleep
#define HL_TIMEOUT_TERMINATE                10000   // How long to wait for thread to terminate
#define HL_TIMEOUT_POST_INET                600000  // How long before updating our IP address to homelogic.com
#define HL_TIMEOUT_UPDATE_WEATHER           600000  // How long before updating the weather
#define HL_TIMEOUT_EVENTSERV                100     // How oftern to check for events
#define HL_TIMEOUT_REFRESH_AUDIO            1800000 // How often to scan audio share drives for changes
#define HL_TIMEOUT_KEEPALIVE_INIT           10000   // Initial keepalive timeout, until first packet recieved
#define HL_TIMEOUT_KEEPALIVE                5000    // Normal keepalive timeout
#define HL_TIMEOUT_LOGIN                    60000   // How long before we boot a client that hasn't logged in
#define HL_TIMEOUT_CHANGEMODE               20000   // How long to wait for all devices when changing house mode
#define HL_TIMEOUT_BUTTON                   100     // Min time between button clicks




#endif

