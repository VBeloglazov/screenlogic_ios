//
//  icon.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#include "NSSocketSink.h"
#import "hlview.h"

/*============================================================================*/

@protocol CIconContainer

/*============================================================================*/

-(void)NotifyIconReady;

@end

/*============================================================================*/

@interface CIcon : NSObject < CNSSocketSink >

/*============================================================================*/
{
    void                                *m_pData;
    BOOL                                m_bWaitImage;
    NSString                            *m_pName;
    CGImageRef                          m_pImage;
    NSMutableArray                      *m_pWaitList;
    int                                 m_iSourceType;
    
    CGImageRef                          m_pImageVirtualAlpha;
}

-(id)initWithName:(NSString*)pName;
-(BOOL)IsSameIcon:(NSString*)pName;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

-(int)SourceType;
-(CGImageRef)GetImage:(UIView*)pView;
-(CGImageRef)GetImageVirtualAlpha;
-(void)Post;
-(BOOL)LoadImage:(CNSMSG*)pMSG;

@end
