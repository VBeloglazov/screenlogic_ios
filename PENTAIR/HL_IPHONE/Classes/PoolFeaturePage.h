//
//  PoolFeaturePage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/10/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import "Sink.h"


@class CNSPoolConfig;
@class CNSPoolCircuit;

/*============================================================================*/

@interface CPoolFeaturePage : CSubSystemPage  < CSink >

/*============================================================================*/
{

    CNSPoolConfig                   *m_pConfig;
    UIScrollView                    *m_pScrollView;
    NSMutableArray                  *m_pControls;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig;

@end
