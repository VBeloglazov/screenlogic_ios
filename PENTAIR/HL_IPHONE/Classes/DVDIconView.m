//
//  DVDIconView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/11/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "DVDIconView.h"
#import "DVDSelectorView.h"


@implementation CDVDIconView


/*============================================================================*/

-(id)initWithDVD:(CDVD*)pDVD Cell:(CMovieTitleCell*)pCell

/*============================================================================*/
{
	if (self = [super initWithFrame:CGRectZero]) 
    {
		// Initialization code
        m_pCell = pCell;
        m_pDVD = pDVD;
        [self setExclusiveTouch:NO];
	}
	return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pDVD SetIconView:NULL];
    [super dealloc];
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    NSString *pText = [m_pDVD Name];
    UIImage *pImage = [m_pDVD GetImage];
    int iDXOut = rect.size.width - 20;
    int iDYOut = iDXOut * 4 / 3;
    CGRect rImage = CGRectMake(10, 0, iDXOut, iDYOut);
    [pImage drawInRect:rImage];

    CGRect rText = CGRectMake(0, iDYOut, rect.size.width, rect.size.height-iDYOut);

    [[UIColor whiteColor] set];
    [pText drawInRect:rText withFont:[UIFont boldSystemFontOfSize:8] lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentCenter];
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    if (m_pOverlay != NULL)
    {
        [m_pOverlay removeFromSuperview];
        m_pOverlay = NULL;
    }
    
    m_pOverlay = [[UIView alloc] initWithFrame:self.bounds];
    m_pOverlay.backgroundColor = [UIColor blueColor];
    [self addSubview:m_pOverlay];
    [m_pOverlay release];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [m_pOverlay setAlpha:0.0];
    [m_pOverlay setBackgroundColor:[UIColor whiteColor]];
    [UIView commitAnimations];

    [m_pCell SelectDisc:m_pDVD View:self];
    [super touchesBegan:touches withEvent:event];
}
/*============================================================================*/

-(void)SetDVD:(CDVD*)pDVD

/*============================================================================*/
{
    m_pDVD = pDVD;
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    [m_pOverlay removeFromSuperview];
    m_pOverlay = NULL;
}
@end
