//
//  ImageServer.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "ImageServer.h"
#import "icon.h"


@implementation CImageServer

static NSMutableArray                      *m_pImages;

/*============================================================================*/

+(void)Init

/*============================================================================*/
{
    if (m_pImages != NULL)
        return;
    m_pImages = [[NSMutableArray alloc] init];
}
/*============================================================================*/

+(void)Dealloc

/*============================================================================*/
{
    if (m_pImages == NULL)
        return;
    [m_pImages release];
}
/*============================================================================*/

+(CIcon*)IconByName:(NSString*)pName

/*============================================================================*/
{
    for (int i = 0; i < [m_pImages count]; i++)
    {
        CIcon *pIcon = (CIcon*)[m_pImages objectAtIndex:i];
        if ([pIcon IsSameIcon:pName])
            return pIcon;
    }

    CIcon *pIcon = [[CIcon alloc] initWithName:pName];
    [m_pImages addObject:pIcon];
    [pIcon release];
    return pIcon;
}
@end
