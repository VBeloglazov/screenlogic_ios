//
//  AudioButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlbutton.h"
#import "NSSocketSink.h"
#import "HLView.h"


/*============================================================================*/

@interface CUserButton : CHLButton < CNSSocketSink, CHLCommandResponder >

/*============================================================================*/
{
    int                                 m_iData;
    int                                 m_iType;
}

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Icon:(NSString*)sIcon 
                    Data:(int)iData
                    Type:(int)iType;

-(void)ButtonPress:(id)sender;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

-(void)OnCommand:(int)iCommandID;


@end
