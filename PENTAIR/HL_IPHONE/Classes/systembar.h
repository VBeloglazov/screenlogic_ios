//
//  systembar.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/20/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLToolBar.h"

@class CMainTab;
@class CHLSegmentHighlight;
@class IPAD_ViewController;
@class CSystemToolBar;


#define CURSOR_LEFTTORIGHT  0
#define CURSOR_RIGHTTOLEFT  1

/*============================================================================*/

@interface CSystemBarButton : UIControl

/*============================================================================*/
{
    UIImageView                 *m_pImageView;
    BOOL                        m_bFramed;
    int                         m_iImageDX;
    int                         m_iImageDY;
}
-(id)initWithFrame:(CGRect)rFrame IconName:(NSString*)psIcon Framed:(BOOL)bFramed ImageHeight:(int)iImageHeight;

@end

/*============================================================================*/

@interface CMainTabView : UIView

/*============================================================================*/
{
    CMainTab                        *m_pMainTab;
    UILabel                         *m_pLabel;
    UIImageView                     *m_pImageView;
}

-(id)initWithFrame:(CGRect)rFrame MainTab:(CMainTab*)pMainTab;
-(CMainTab*)MainTab;

@end

/*============================================================================*/

@interface CSystemToolBarClient : CToolBarClientView

/*============================================================================*/
{
    CSystemToolBar                  *m_pMenuBar;
}
-(id)initWithFrame:(CGRect)rFrame MainTabs:(NSMutableArray*)pMainTabs ViewController:(IPAD_ViewController*)pController SelectedIndex:(int)iIndex;

@end

/*============================================================================*/

@interface CSystemToolBar : UIControl 

/*============================================================================*/
{
    IPAD_ViewController             *m_pViewController;
    
    int                             m_iCursorDirection;
    int                             m_iSelectedIndex;
    CHLSegmentHighlight             *m_pHL;
    NSMutableArray                  *m_pViews;
}

-(id)initWithFrame:(CGRect)rFrame MainTabs:(NSMutableArray*)pMainTabs ViewController:(IPAD_ViewController*)pController SelectedIndex:(int)iIndex;
-(void)SetSelectedIndex:(int)iNew;
-(int)SelectedIndex;
-(int)CursorDirection;

@end
