//
//  AudioService.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "AudioService.h"
#import "NSMSG.h"


@implementation CAudioService

/*============================================================================*/

-(id)initWithMSG:(CNSMSG*)pMSG;

/*============================================================================*/
{
    if (self == [super init])
    {
        m_iID = [pMSG GetInt];
        m_pName = [[pMSG GetString] retain];
        m_iType = [pMSG GetInt];
    }
    return self;
}

/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [super dealloc];
}
/*============================================================================*/

-(int)ID

/*============================================================================*/
{
    return m_iID;
}
/*============================================================================*/

-(int)Type

/*============================================================================*/
{
    return m_iType;
}
/*============================================================================*/

-(NSString*)Name

/*============================================================================*/
{
    return m_pName;
}
/*============================================================================*/

-(NSString*)Icon

/*============================================================================*/
{
    switch (m_iType)
    {
    case AUDIO_SERVICE_SQUEEZENET_RHAPSODY:
    case AUDIO_SERVICE_INTERNET_RHAPSODY:
        return @"RHAPSODY.png";
    case AUDIO_SERVICE_SQUEEZENET_PANDORA:
    case AUDIO_SERVICE_INTERNET_PANDORA:
        return @"PANDORA.png";
    case AUDIO_SERVICE_INTERNET_SHOUTCAST:
        return @"SHOUTCAST.png";
    }
    return @"TRACK.png";
}
/*============================================================================*/

-(int)ServiceID

/*============================================================================*/
{
    return m_iID;
}
@end
