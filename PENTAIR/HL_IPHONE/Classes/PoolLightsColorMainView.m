//
//  PoolLightsColorMainView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolLightsColorMainView.h"
#import "NSPoolConfig.h"
#import "hlm.h"
#import "PoolColorLightsView.h"
#import "PoolIntelliBriteView.h"
#import "PoolMagicStreamView.h"

@implementation CPoolLightsColorMainView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_iFlags = TAB_RREGION_NOINSET;
        m_pConfig = pConfig;

        // Initialization code
        NSMutableArray *pItems = [[[NSMutableArray alloc]init] autorelease];

        int iNPages = 0;
        m_iSubPages[iNPages] = SUBPAGE_COLOR_LIGHTS; iNPages++;
        [pItems addObject:@"COLOR LIGHTS"];
                
        if ([pConfig EquipPresent:POOL_IBRITEPRESENT])
        {
            m_iSubPages[iNPages] = SUBPAGE_INTELLIBRITE; iNPages++;
            [pItems addObject:@"INTELLIBRITE"];
        }
        if ([pConfig EquipPresent:POOL_MAGICSTREAMPRESENT])
        {
            m_iSubPages[iNPages] = SUBPAGE_MAGICSTREAM; iNPages++;
            [pItems addObject:@"MAGICSTREAM"];
        }

        [self InitPageWithStrings:pItems];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(UIView*)CreatePageForIndex:(int)iIndex

/*============================================================================*/
{
    UIView *pNewPage = NULL;
    int iSubPageType = m_iSubPages[iIndex];
    switch (iSubPageType)
    {
    case SUBPAGE_COLOR_LIGHTS:
        pNewPage = [[CPoolColorLightsView alloc] initWithFrame:CGRectZero Config:m_pConfig];
        break;
    case SUBPAGE_INTELLIBRITE:
        pNewPage = [[CPoolIntelliBriteView alloc] initWithFrame:CGRectZero Config:m_pConfig];
        break;
    case SUBPAGE_MAGICSTREAM:
        pNewPage = [[CPoolMagicStreamView alloc] initWithFrame:CGRectZero];
        break;

    }

    return pNewPage;
}

@end
