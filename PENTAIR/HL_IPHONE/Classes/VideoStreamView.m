//
//  VideoStreamView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "VideoStreamView.h"
#import "SubTab.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "hlradio.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

#define MAX_JPEG_FRAMES         5

@implementation CVideoStreamView

@synthesize m_pPresetControl;
@synthesize m_pResControl;

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame CameraID:(int)iID Options:(int)iOptions

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        // Initialization code
        m_bFullScaleView = TRUE;
        m_iCameraID = iID;
        m_pLastImage = NULL;
        m_pData1 = malloc(150000);
        m_pData2 = malloc(150000);
        m_iStartupRes = -1;
        m_iOptions = iOptions;
        if (!(m_iOptions & VIDEO_OPTION_DEFAULTRES_AUTO))
        {
            if (m_iOptions & VIDEO_OPTION_DEFAULTRES_LOW)
                m_iStartupRes = 0;
            if (m_iOptions & VIDEO_OPTION_DEFAULTRES_MED)
                m_iStartupRes = 1;
            if (m_iOptions & VIDEO_OPTION_DEFAULTRES_HIGH)
                m_iStartupRes = 2;
        }

        m_pSinkServer = [[CSinkServer alloc] init];
    }
    return self;
}
/*============================================================================*/

-(void)drawRect:(CGRect) rect

/*============================================================================*/
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *pFill = [UIColor colorWithRed:0.2 green:0.2 blue:0.5 alpha:.5];
    [pFill set];
    CGRect rFill = rect;
    CGContextFillRect(context, rFill);
}
/*============================================================================*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
	UITouch *touch = [touches anyObject];
	
	// Only move the placard view if the touch was in the placard view
	if ([touch view] == self) 
    {
		// In case of a double tap outside the placard view, update the placard's display string
		if ([touch tapCount] == 1) 
        {
            CGPoint pt = [touch locationInView:self];
            int iDXThis = self.bounds.size.width;
            int iDYThis = self.bounds.size.height;
            int iDX100 = (pt.x * 100 / iDXThis)-50;
            int iDY100 = (pt.y * 100 / iDYThis)-50;
            if (m_b180)
            {
                iDX100 = -iDX100;
                iDY100 = -iDY100;
            }

            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_VIDEO_MOVEDXDYBYIDQ];
            [pQ autorelease];
            [pQ PutInt:m_iCameraID];
            [pQ PutInt:iDX100];
            [pQ PutInt:iDY100];
            [pQ PutInt:0];
            [pQ PutInt:0];
            [pQ SendMessageWithID:[CHLComm SenderID:self]];
		}
		return;
	}
}
/*============================================================================*/

-(void)StartStream
 
/*============================================================================*/
{
    if (m_bStreaming)
        return;

    [CHLComm AddSocketSink:self];

    int iThisRes = 2;

    if (!m_bFullScaleView)
    {
        iThisRes = 0;
    }
    else
    {
        if (m_iStartupRes >= 0)
        {
            iThisRes = m_iStartupRes;
        }
        else if (m_iOptions & VIDEO_OPTION_DEFAULTRES_AUTO)
        {
            if (self.frame.size.width <= 160)
                iThisRes = 0;
            else if (self.frame.size.width <= 320)
                iThisRes = 1;
        }
    }

    CNSQuery *pQRes = [[[CNSQuery alloc] initWithQ:HLM_VIDEO_SETRESMODEBYIDQ] autorelease];
    [pQRes PutInt:m_iCameraID];
    [pQRes PutInt:iThisRes]; // TRUE to Set
    [pQRes SendMessage];

    CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:HLM_VIDEO_CAMERASTARTBYIDQ];
    [pQ1 autorelease];
    [pQ1 PutInt:m_iCameraID];
    [pQ1 PutInt:[CHLComm SenderID:self]];
    [pQ1 PutInt:self.frame.size.width];
    [pQ1 PutInt:self.frame.size.height];
    [pQ1 PutInt:VCLIENT_FULLFRAME_JPEGOK];
    [pQ1 SendMessage];

    CNSQuery *pQ2 = [[CNSQuery alloc] initWithQ:HLM_VIDEO_GETCAMPOSQ];
    [pQ2 autorelease];
    [pQ2 PutInt:m_iCameraID];
    [pQ2 SendMessageWithID:[CHLComm SenderID:self]];
        
    m_bStreaming = TRUE;
}
/*============================================================================*/

-(void)StopStream
 
/*============================================================================*/
{
    if (!m_bStreaming)
        return;
        
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_VIDEO_CAMERASTOPBYIDQ];
    [pQ autorelease];
    [pQ PutInt:m_iCameraID];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessage];

    [CHLComm RemoveSocketSink:self];
        
    m_bStreaming = FALSE;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [self StopStream];
    m_bStreaming = FALSE;

    [self.layer setContents:NULL];

    if (m_pLastImage != NULL)
        CGImageRelease(m_pLastImage);

    free(m_pData1);
    free(m_pData2);
    [m_pSinkServer release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        if (m_bStreaming)
        {
            [self StopStream];
            [self StartStream];
        }
    }
    else 
    {
    }

}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iMSGID = [pMSG MessageID];
    switch(iMSGID)
    {
    case HLM_VIDEO_FULLFRAMEJPGDATA:
    case HLM_VIDEO_FULLFRAMEJPGDATA180:
        {
            // Process JPEG
            
            // Send Response
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_VIDEO_IMAGE_DONE];
            [pQ autorelease];
            [pQ PutInt:[pMSG MessageSize]];
            [pQ SendMessageWithID:[CHLComm SenderID:self]];


            int iImageSize = [pMSG DataSize];
            unsigned char *pData = [pMSG GetDataAtReadIndex];

            // Make a copy of the JPEG data, we don't want to overwrite while the rendering thread is doing its thing
            void *pLocalData = m_pData1;
            if (m_bData)
                pLocalData = m_pData2;
            memcpy(pLocalData, pData, iImageSize);
            m_bData = !m_bData;

            if (iMSGID == HLM_VIDEO_FULLFRAMEJPGDATA180)
            {
                if (!m_b180)
                {
                    CATransform3D transform = CATransform3DMakeScale(-1, -1, 1);
                    [self.layer setTransform:transform];
                    m_b180 = TRUE;
                }
            }
            else
            {
                if (m_b180)
                {
                    CATransform3D transform = CATransform3DMakeScale(1, 1, 1);
                    [self.layer setTransform:transform];
                    m_b180 = FALSE;
                }
            }

            CGDataProviderRef pSrc = CGDataProviderCreateWithData(NULL, pLocalData, iImageSize, NULL);
            CGImageRef pImage = CGImageCreateWithJPEGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
            CGDataProviderRelease(pSrc);
            if (m_pLastImage != NULL)
            {
                int iDX = CGImageGetWidth(m_pLastImage);
                int iRes = 0;
                if (iDX >= 640)
                    iRes = 2;
                else if (iDX >= 320)
                    iRes = 1;

                if (m_pResControl != NULL)
                {
                    m_iResMode = iRes;
                    if (iRes != [m_pResControl GetSelectedIndex])
                    {
                        [m_pResControl SetSelectedIndex:iRes];
                    }
                }
                CGImageRelease(m_pLastImage);
                m_pLastImage = NULL;
            }

            [self.layer setContents:(id)pImage];
            m_pLastImage = pImage;
            [m_pSinkServer NotifySinks:0 Data:0];
        }
        break;

    case HLM_VIDEO_GETCAMPOSA:
        {
            [pMSG GetInt];  // X
            [pMSG GetInt];  // Y
            [pMSG GetInt];  // Z
            int iPresetIndex = [pMSG GetInt]; 
            m_iActivePreset = iPresetIndex;
            if (m_pPresetControl != NULL)
            {
                [m_pPresetControl SetSelectedIndex:iPresetIndex];
            }
        }
        break;
    }
}
/*============================================================================*/

-(int)ResMode

/*============================================================================*/
{
    return m_iResMode;
}
/*============================================================================*/

-(int)ActivePreset

/*============================================================================*/
{
    return m_iActivePreset;
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    if (bVisible)
        [self StartStream];
    else
        [self StopStream];
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(void)StartupRes:(int)iStartupRes

/*============================================================================*/
{
    m_iStartupRes = iStartupRes;
}
/*============================================================================*/

-(BOOL)IsStreaming

/*============================================================================*/
{
    return m_bStreaming;
}
/*============================================================================*/

-(BOOL)Rotate180

/*============================================================================*/
{
    return m_b180;
}
/*============================================================================*/

-(void)SetFullScaleView:(BOOL)bFullScaleView

/*============================================================================*/
{
    if (m_bFullScaleView == bFullScaleView)
        return;

    m_bFullScaleView = bFullScaleView;

    if (!m_bStreaming)
        return;
        
    int iThisRes = 0;
    if (m_bFullScaleView)
    {
        iThisRes = 2;
        if (m_iStartupRes >= 0)
        {
            iThisRes = m_iStartupRes;
        }
        else if (m_iOptions & VIDEO_OPTION_DEFAULTRES_AUTO)
        {
            if (self.frame.size.width <= 160)
                iThisRes = 0;
            else if (self.frame.size.width <= 320)
                iThisRes = 1;
        }
    }

    CNSQuery *pQRes = [[[CNSQuery alloc] initWithQ:HLM_VIDEO_SETRESMODEBYIDQ] autorelease];
    [pQRes PutInt:m_iCameraID];
    [pQRes PutInt:iThisRes]; // TRUE to Set
    [pQRes SendMessage];
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
}
@end

