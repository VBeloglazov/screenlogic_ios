//
//  SubTab.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SubTab.h"


@implementation CSubTab

@synthesize m_pName;
@synthesize m_iID;
@synthesize m_iData1;
@synthesize m_iData2;

/*============================================================================*/

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int) iData1 Data2:(int)iData2

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pSubTabCell = NULL;
        m_pName = pName;
        [m_pName retain];
        m_iID = iID;
        m_iData1 = iData1;
        m_iData2 = iData2;
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [super dealloc];
}
/*============================================================================*/

-(CSubTabCell*)GetSubTabCell

/*============================================================================*/
{
    return m_pSubTabCell;
}
/*============================================================================*/

-(void)SetSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    m_pSubTabCell = pSubTabCell;
}
/*============================================================================*/

-(NSString*)Name

/*============================================================================*/
{
    return m_pName;
}
//*============================================================================*/
//-(void)SetMainViewController:(UIViewController*)mainViewController
//{
//    m_pMainViewController = mainViewController;
//}
//*============================================================================*/
@end
