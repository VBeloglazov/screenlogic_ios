//
//  hlsegment.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CHLSegmentHighlight;
@class CToolBarHighlight;

#define HLSEGMENT_PAGE_CTRL             0
#define HLSEGMENT_TOOLBAR               1
#define HLSEGMENT_BUTTON                2
#define HLSEGMENT_PAGE_CTRL_ROUNDREGION 3

/*============================================================================*/

@protocol CHLSegmentImageProvider

/*============================================================================*/

-(CGImageRef)GenerateSegmentHighlight;

@end

/*============================================================================*/

@interface CHLSegment : UIControl < CHLSegmentImageProvider >

/*============================================================================*/
{
    int                         m_iType;
    UIView                      *m_pHL;
    NSMutableArray              *m_pItems;
    int                         m_iSelectedIndex;
    int                         m_iData[10];
    int                         m_iNData;
    int                         m_iLayout;
    id                          m_pImageProvider;
    int                         m_iImageDX;
    int                         m_iImageDY;
}

-(id)initWithFrame:(CGRect)rFrame ImageProvider:(id)pImageProvider Items:(NSArray*)pItems Type:(int)iType Data:(BOOL)bData;
-(id)initWithFrame:(CGRect)rFrame ImageProvider:(id)pImageProvider Views:(NSArray*)pViews Type:(int)iType;

-(void)InitHighlight;
-(CGImageRef)GenerateImage:(int)iType;
-(int)NItems;
-(void)SetSelectedIndex:(int)iIndex;
-(int)SelectedIndex;
-(int)SelectedData;
-(void)SetSelectedIndexByData:(int)iData;

-(void)Layout:(int)iNew;

-(CGRect)SelectedItemRect:(int)iIndex;
-(CGRect)SelectedItemUnitRect:(CGRect)rSelectedItemRect;

@end

/*============================================================================*/

@interface CHLSegmentItem : NSObject

/*============================================================================*/
{
    NSString                    *m_pText;
    int                         m_iData;
}

-(id)initWithText:(NSString*)psText Data:(int)iData;
-(NSString*)Text;
-(int)Data;

@end
/*============================================================================*/

@interface CHLSegmentIconView : UIView

/*============================================================================*/
{
    UIImageView                 *m_pImage;
    UILabel                     *m_pLabel;
    int                         m_iImageDX;
    int                         m_iImageDY;
}

-(id)initWithFrame:(CGRect)rFrame Icon:(NSString*)psIcon Text:(NSString*)psText;

@end

