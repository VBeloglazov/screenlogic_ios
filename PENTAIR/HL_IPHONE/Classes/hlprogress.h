//
//  hlprogress.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

/*============================================================================*/

@interface CHLProgressRect : UIView

/*============================================================================*/
{

}
@end

/*============================================================================*/

@interface CHLProgress : UIView 

/*============================================================================*/
{
    NSMutableArray              *m_pPoints;
}

-(void)SetProgress:(int)iPCT;

@end
