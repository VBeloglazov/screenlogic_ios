//
//  SecurityHistoryDataView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecurityHistoryDataView.h"
#import "SecPartition.h"
#import "timeview.h"
#import "hlcomm.h"
#import "nsquery.h"
#import "rteview.h"
#import "nsmsg.h"
#import "SecurityHistory.h"

@implementation CSecurityHistoryDataView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView Partition:(CSecPartition*)pPartition LastView:(CSecurityHistoryDataView*)pLastView

/*============================================================================*/
{
    if (self = [super initWithFrame:frame TimeView:pTimeView]) 
    {
        [self setOpaque:YES];
        m_iXOrigin = frame.origin.x;
        int iXLeft = frame.origin.x;
        int iXRight = iXLeft + frame.size.width;
        NSDate *pStart = [[pTimeView GetTimeAtX:iXLeft] autorelease];
        NSDate *pEnd   = [[pTimeView GetTimeAtX:iXRight] autorelease];
        BOOL bNeedData = TRUE;

        // Initialization code
        if (pLastView != NULL)
        {
            if (([pStart laterDate:[pLastView StartDataDate]] == pStart) && ([pEnd earlierDate:[pLastView EndDataDate]] == pEnd))
            {
                [m_pDateDataStart release];
                [m_pDateDataEnd release];
                m_pDateDataStart = [pLastView StartDataDate];
                m_pDateDataEnd = [pLastView EndDataDate];
                [m_pDateDataStart retain];
                [m_pDateDataEnd retain];
                bNeedData = FALSE;
                m_bDataOK = TRUE;
            }

            m_pRTESeries = [pLastView RTESeries];
            [m_pRTESeries retain];

            if (![pLastView DataOK])
            {
                m_pDateDataStart = [pTimeView GetTimeAtX:frame.origin.x];
                m_pDateDataEnd = [pTimeView GetTimeAtX:frame.origin.x + frame.size.width];
                bNeedData = TRUE;
            }
        }
        m_pPartition = pPartition;

        if (bNeedData)
        {
            [m_pTimeView SetWorking:YES];
            if (pLastView == NULL || ![pLastView DataOK])
                [self PostQuery];
            else
                m_pQueryTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(PostQuery) userInfo:nil repeats:NO] retain];
        }
        else
        {
            [m_pTimeView SetWorking:NO];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    [CHLComm RemoveSocketSink:self];
    [m_pRTESeries release];
    [super dealloc];
}
/*============================================================================*/

-(NSMutableArray*)RTESeries;

/*============================================================================*/
{
    return m_pRTESeries;
}
/*============================================================================*/

-(void)PostQuery

/*============================================================================*/
{
    int iXLeft = m_iXOrigin;
    int iXRight = iXLeft + self.frame.size.width;
    NSDate *pStart = [[m_pTimeView GetTimeAtX:iXLeft] autorelease];
    NSDate *pEnd   = [[m_pTimeView GetTimeAtX:iXRight] autorelease];
    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_SECURITY_GETFAULTRPTBYPARTIDQ] autorelease];
    [pQ PutInt:m_pPartition.m_iID];
    [pQ PutTime:pStart];
    [pQ PutTime:pEnd];
    [pQ SendMessageWithMyID:self];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
}
/*============================================================================*/

-(void)StopQuery

/*============================================================================*/
{
    [m_pQueryTimer invalidate];
    [m_pQueryTimer release];
    m_pQueryTimer = NULL;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_SECURITY_GETFAULTRPTBYPARTIDA)
        return;

    NSCalendar *pCal = [NSCalendar currentCalendar];
    NSDateComponents *pComps = [[[NSDateComponents alloc] init] autorelease];

    [m_pRTESeries release];
    m_pRTESeries = [[NSMutableArray alloc]init];

    for (int i = 0; i < 4; i++)
    {
        UIColor *pRGB = NULL;
        switch (i)
        {
        case 0: { pRGB = [UIColor colorWithRed:.25 green:.25 blue:1.0 alpha:1.0]; }   break;
        case 1: { pRGB = [UIColor colorWithRed:1   green:1   blue:0   alpha:1.0]; }   break;
        case 2: { pRGB = [UIColor colorWithRed:1   green:0   blue:1.0 alpha:1.0]; }   break;
        case 3: { pRGB = [UIColor colorWithRed:1   green:0   blue:0   alpha:1.0]; }   break;
        }

        CRTESeries *pRTE = [[CRTESeries alloc] initWithColor:pRGB];
        [m_pRTESeries addObject:pRTE];
        [pRTE release];
    }

    CRTESeries *pArmed      = (CRTESeries*)[m_pRTESeries objectAtIndex:0];
    CRTESeries *pNotReady   = (CRTESeries*)[m_pRTESeries objectAtIndex:1];
    CRTESeries *pAlarm      = (CRTESeries*)[m_pRTESeries objectAtIndex:2];
    CRTESeries *pFire       = (CRTESeries*)[m_pRTESeries objectAtIndex:3];

    CDateHelper *pDateHelper = [[[CDateHelper alloc] init] autorelease];

    int iNPartStats = [pMSG GetInt];
    for (int i = 0; i < iNPartStats; i++)
    {
        NSDate *pDate = [pMSG GetTimeCompressedWith:pComps Calendar:pCal DateHelper:pDateHelper];
        int iStat     = [pMSG GetShort];
        BOOL bArmed     = FALSE;
        BOOL bNotReady  = FALSE;
        BOOL bAlarm     = FALSE;
        BOOL bFire      = FALSE;

        if (iStat & SEC_PARTITION_ARMED)        bArmed = TRUE;
        if (iStat & SEC_PARTITION_NORESPONSE)   bNotReady = TRUE;
        if (iStat & SEC_PARTITION_ALARMACT)     bAlarm = TRUE;
        if (iStat & SEC_PARTITION_FIRE)         bFire = TRUE;

        [pArmed     ProcessState:pDate True:bArmed];
        [pNotReady  ProcessState:pDate True:bNotReady];
        [pAlarm     ProcessState:pDate True:bAlarm];
        [pFire      ProcessState:pDate True:bFire];
    }

    int iNPartUsers = [pMSG GetInt];
    for (int i = 0; i < iNPartUsers; i++)
    {
        [pMSG GetTimeCompressedWith:pComps Calendar:pCal DateHelper:pDateHelper];
        [pMSG GetShort];
        [pMSG GetString];
    }

    UIColor *pColors[2];
    pColors[0] = [UIColor colorWithRed:0 green:0 blue:1 alpha:1.0];
    pColors[1] = [UIColor colorWithRed:0 green:.8 blue:0 alpha:1.0];

    int iRGBIndex = 0;
    int iNZones = [pMSG GetInt];
    for (int iZone = 0; iZone < iNZones; iZone++)
    {
        [pMSG GetInt];  //ID
        int iNRTE = [pMSG GetInt];

        CRTESeries *pSeries = [[CRTESeries alloc] initWithColor:pColors[iRGBIndex]];
        iRGBIndex++;
        if (iRGBIndex >= 2)
            iRGBIndex = 0;

        for (int iRTE = 0; iRTE < iNRTE; iRTE++)
        {
            NSDate *pD1 = [[pMSG GetTimeCompressedWith:pComps Calendar:pCal DateHelper:pDateHelper] retain];
            NSDate *pD2 = [[pMSG GetTimeCompressedWith:pComps Calendar:pCal DateHelper:pDateHelper] retain];
            [pSeries AddRTE:pD1 End:pD2];
            [pD1 release];
            [pD2 release];
        }

        [m_pRTESeries addObject:pSeries];
        [pSeries release];
    }
    

    m_bDataOK = TRUE;
    [self setNeedsDisplay];
    
    // Don't know why but if I don't do an animation here, the next rotation doesn't register
    [self setAlpha:0.5];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.50];
    [self setAlpha:1.0];
    [UIView commitAnimations];

}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();

    [self DrawDayRects:pRef];

    int iDYItem = [CSecurityHistoryView DYItem];

    for (int iPass = 0; iPass < 2; iPass++)
    {
        for (int iSeries = 0; iSeries < [m_pRTESeries count]; iSeries++)
        {
            int iXLast = -1;
            CRTESeries *pSeries = (CRTESeries*)[m_pRTESeries objectAtIndex:iSeries];
            int iY0 = iSeries * iDYItem + SECURITY_ITEM_INSET;
            int iDY = iDYItem - 2 * SECURITY_ITEM_INSET;

            UIColor *pRGB = NULL;
            switch (iPass)
            {
            case 0: pRGB = [[pSeries Color] colorWithAlphaComponent:.5];  break;
            case 1: pRGB = [pSeries Color];   break;
            }

            [pRGB set];

            NSMutableArray *pPoints = [pSeries PointList];
            for (int iPoint = 0; iPoint < [pPoints count]; iPoint++)
            {
                CRunTimeEvent *pEvent = (CRunTimeEvent*)[pPoints objectAtIndex:iPoint];
                int iX0 = [m_pTimeView XLogical:[pEvent StartDate]] - m_iXOrigin;
                int iX1 = [m_pTimeView XLogical:[pEvent EndDate]] - m_iXOrigin;

                int iDX = MAX(1, iX1-iX0);
                if (iDX > 1 || iX0 > iXLast)
                {
                    switch (iPass)
                    {
                    case 0:     CGContextFillRect(pRef, CGRectMake(iX0, iY0, iDX, iDY));               break;
                    case 1:     CGContextStrokeRect(pRef, CGRectMake(iX0, iY0, iDX, iDY));             break;
                    }
                    iXLast = iX1;
                }
            }
        }
    }
    if (m_bDataOK)
        [m_pTimeView SetWorking:NO];
}

@end
