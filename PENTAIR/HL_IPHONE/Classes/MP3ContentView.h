//
//  MP3ContentView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 7/12/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
@class CHLButton;
@class CUserTabIconView;
@class CMP3Menu;
@class CAudioZone;
@class CMP3ContentView;
@class CAudioService;
@class CRoundRegionSeparator;
@class CNSMSG;
@class CAudioServiceMenu;

/*============================================================================*/

@interface CMP3AddressHeader : UIView

/*============================================================================*/
{
    CMP3Menu                    *m_pMenu;
    CUserTabIconView            *m_pIconView;
    UILabel                     *m_pLabel;
}

-(id)initWithFrame:(CGRect)frame Menu:(CMP3Menu*)pMenu Icon:(NSString*)psIcon ContentView:(CMP3ContentView*)pContentView;
-(CMP3Menu*)Menu;
-(void)SetData:(int)iID Name:(NSString*)pName;

@end

/*============================================================================*/

@interface CMP3ContentView : UIView 

/*============================================================================*/
{
    NSString                    *m_psIcon;
    CHLButton                   *m_pBackButton;
    NSMutableArray              *m_pAddressButtons;
    NSMutableArray              *_m_pAddressButtons;
    CAudioZone                  *m_pZone;
    CRoundRegionSeparator       *m_pSeparator;
}

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Icon:(NSString*)pIcon;
-(void)PushView:(CMP3Menu*)pView;
-(void)PushView:(CMP3Menu*)pView Animated:(BOOL)bAnimated;
-(void)NavigationStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;
-(void)PushNewViewFrom:(int)iFrom To:(int)iTo Data:(NSString*)pData;

-(CGRect)ContentRect;
-(CGRect)HeaderRect:(BOOL)bRoot;
-(CGRect)ButtonRect;

@end


/*============================================================================*/

@interface CArtistContentView : CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone;

@end

/*============================================================================*/

@interface CAlbumContentView : CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone;

@end

/*============================================================================*/

@interface CTrackContentView : CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone;

@end

/*============================================================================*/

@interface CPlaylistContentView : CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone;

@end

/*============================================================================*/

@interface CGenreContentView : CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone;

@end


/*============================================================================*/

@interface CAudioServiceContentView : CMP3ContentView < CNSSocketSink >

/*============================================================================*/
{
    int                             m_iNAddress;
    int                             m_iAddress0;
    int                             m_iAddressB;

    CAudioServiceMenu               *m_pMenu;
    CAudioServiceMenu               *_m_pMenu;
    CMP3AddressHeader               *m_pHeader;
    CMP3AddressHeader               *_m_pHeader;
    

    CAudioService                   *m_pService;
    UIActivityIndicatorView         *m_pWaitGraphic;
    NSString                        *m_pNavString;
    int                             m_iLastID;
}
-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Service:(CAudioService*)pService;

@end

/*============================================================================*/

@interface CMyLibraryContentView : CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone;

@end