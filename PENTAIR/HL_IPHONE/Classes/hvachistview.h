//
//  hvachistview.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "timeview.h"

@class CTStat;




/*============================================================================*/

@interface CHVACHistoryView : CTimeView

/*============================================================================*/
{
    CTStat                              *m_pTStat;
}

-(id)initWithFrame:(CGRect)frame TStat:(CTStat*)pTStat;

-(int)DataViewDY;

-(void)NotifyRotation;

-(void)InitScaleView;

@end
