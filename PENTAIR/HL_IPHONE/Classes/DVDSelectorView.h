//
//  TVChannelsView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "HLView.h"

@class CDVDSelectorView;
@class CIconButton;
@class CDVDIconView;
@class CHLTableFrame;

/*============================================================================*/

@interface CDVD : NSObject < CNSSocketSink >

/*============================================================================*/
{
    CDVDIconView                        *m_pIconView;

    int                                 m_iGenreIndex;
    int                                 m_iDeviceID;
    int                                 m_iDiscIndex;
    NSString                            *m_pName;

    UIImage                             *m_pImage;
    BOOL                                m_bWaitImage;
}

-(id)initWithDeviceID:(int)iDeviceID GenreIndex:(int)iGenreIndex DiscIndex:(int)iDiscIndex Name:(NSString*)pName;
-(int)DeviceID;
-(int)GenreIndex;
-(int)DiscIndex;
-(NSString*)Name;
-(UIImage*)GetImage;
-(void)SetIconView:(CDVDIconView*)pView;

/*============================================================================*/
@end

/*============================================================================*/

@interface CMovieGenre : NSObject < CNSSocketSink >

/*============================================================================*/
{
    NSString                            *m_pName;
    int                                 m_iID;
    int                                 m_iDeviceID;
    NSMutableArray                      *m_pTitles;
    CDVDSelectorView                    *m_pView;
    BOOL                                m_bLoaded;
}

-(id)initWithID:(int)iID DeviceID:(int)iDeviceID Name:(NSString*)pName View:(CDVDSelectorView*)pView;
-(int)ID;
-(NSString*)Name;
-(int)NTitles;
-(CDVD*)Title:(int)iIndex;
-(BOOL)IsLoaded;

@end



/*============================================================================*/

@interface CMovieTitleCell : UITableViewCell 

/*============================================================================*/
{
    CMovieGenre                         *m_pGroup;
    NSMutableArray                      *m_pButtons;
    CDVDSelectorView                    *m_pView;
    BOOL                                m_bLoaded;
}


-(id)initWithFrame:(CGRect)aRect reuseIdentifier:(NSString *)identifier DXIndex:(int)iDXIndex;

-(void)SetGroup:(CMovieGenre*)pGroup View:(CDVDSelectorView*)pView;
-(void)SelectDisc:(CDVD*)pDVD View:(CDVDIconView*)pView;

@end



/*============================================================================*/

@interface CDVDSelectorView : UIView   < CNSSocketSink,
                                        UITableViewDelegate,
                                        UITableViewDataSource,
                                        UIAlertViewDelegate,
                                        CHLView >

/*============================================================================*/
{
    int                                 m_iData;
    int                                 m_iStyle;
    NSMutableArray                      *m_pGroups;
    NSMutableArray                      *m_pSectionTitles;
    NSMutableArray                      *m_pSectionTitlesTruncated;

    CHLTableFrame                       *m_pTableView;

    CDVD                                *m_pSelectedDVD;
    int                                 m_iSelectedSection;
    CGRect                              m_rSelectedStationRect;
}

-(id)initWithFrame:(CGRect)rFrame Data:(int)iData Style:(int)iStyle;

-(void)GroupLoaded;
-(void)SetSelectedDVD:(CDVD*)pDVD Section:(int)iSection Rect:(CGRect)rBtn;
-(NSString*)TruncateGenreTitle:(NSString*)pName;
-(int)MaxTitlesPerRow;
-(int)IndexOf:(CMovieGenre*)pGenre;

+(int)DXIcon;
+(int)DYIcon;

@end
