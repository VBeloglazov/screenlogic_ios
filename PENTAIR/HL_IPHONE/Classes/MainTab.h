//
//  MainTab.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

@class CNSQuery;
@class CSubTab;
@class CSubTabCell;
@class CAppDelegate;
@class CSubSystemPage;
@class CMainTabViewController;

#import "MainView.h"

/*============================================================================*/

@interface CMainTab : NSObject 

/*============================================================================*/
{
	NSMutableArray                      *m_pSubTabs;
    BOOL                                m_bConfigLoaded;

    NSString                            *m_pName;
    NSString                            *m_pSystemName;
    int                                 m_iTabID;

    int                                 m_iDefaultTab;
    
    BOOL                                m_bExpanded;
    
    UIViewController                    *m_pMainViewController;
}


@property (nonatomic, retain) NSString *m_pName;
@property (nonatomic, retain) NSMutableArray *m_pSubTabs;

-(id)initWithName:(NSString*)pName TabID:(int)iTabID;
-(id)initWithName:(NSString*)pName TabID:(int)iTabID SystemName:(NSString*)systemName;

-(NSString*)ImageName;
-(NSString*)SelectText;

-(int)TabID;

-(BOOL)LoadConfig;

-(void)LoadSubTab:(CNSQuery*)pQ;
-(void)AddSubTab:(NSString*) pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;
-(void)AddSubTab:(CSubTab*) pSubTab;
-(int)NSubTabs;
-(CSubTab*)SubTab:(int)iIndex;
-(UITableViewCell*)CellForSubTabIndex:(UITableView*)pTableView Index:(int)iSubTabIndex;
-(void)InitSubTabCell:(CSubTabCell*)pCell SubTab:(CSubTab*)pSubTab;

-(void)SetDefaultTab:(int)iIndex;
-(int)GetDefaultTab;

-(BOOL)IsExpanded;
-(void)SetExpanded:(BOOL)bNew;

-(CSubTab*)DefaultTab;

-(CSubSystemPage*)CreateUserPage:(CGRect) rFrame SubTab:(CSubTab*)pSubTab MainTabPage:(CMainTabViewController*)pMainPage;
-(int)IndexOf:(CSubTab*)pSubTab;

-(UIView*)CreateAccessoryView;
-(void)SetMainController:(UIViewController*)mainController;

@end
