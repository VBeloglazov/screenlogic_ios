//
//  TVChannelsView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "HLView.h"

@class CTVChannelsView;
@class CStation;
@class CIconButton;
@class CHLTableFrame;

/*============================================================================*/

@interface CTVChannelGroup : NSObject < CNSSocketSink >

/*============================================================================*/
{
    NSString                            *m_pName;
    int                                 m_iID;
    int                                 m_iDeviceID;
    NSMutableArray                      *m_pStations;
    CTVChannelsView                     *m_pView;
}

-(id)initWithID:(int)iID DeviceID:(int)iDeviceID Name:(NSString*)pName View:(CTVChannelsView*)pView;
-(int)ID;
-(NSString*)Name;
-(int)NStations;
-(CStation*)Station:(int)iIndex;

@end



/*============================================================================*/

@interface CChannelCell : UITableViewCell 

/*============================================================================*/
{
    CTVChannelGroup                     *m_pGroup;
    NSMutableArray                      *m_pButtons;

    int                                 m_iSection;
    int                                 m_iRow;
    CTVChannelsView                     *m_pView;
    
    int                                 m_iDXIndex;
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)identifier DXIndex:(int)iDXIndex;

-(void)SetGroup:(CTVChannelGroup*)pGroup Section:(int)iSection Row:(int)iRow View:(CTVChannelsView*)pView;
-(void)SelectChannel:(int)iIndex;

@end



/*============================================================================*/

@interface CTVChannelsView : UIView   < CNSSocketSink,
                                        UITableViewDelegate,
                                        UITableViewDataSource,
                                        CHLView >

/*============================================================================*/
{
    int                                 m_iData;
    int                                 m_iStyle;
    NSMutableArray                      *m_pGroups;
    NSMutableArray                      *m_pSectionTitles;
    NSMutableArray                      *m_pSectionTitlesTruncated;

    CHLTableFrame                       *m_pTableView;

    CStation                            *m_pSelectedStation;
    int                                 m_iSelectedSection;
    CGRect                              m_rSelectedStationRect;
}

-(id)initWithFrame:(CGRect)rFrame Data:(int)iData Style:(int)iStyle;

-(void)GroupLoaded;
-(void)SetSelectedChannel:(CStation*)pStation Section:(int)iSection Cell:(UITableViewCell*)pCell Rect:(CGRect)rBtn;
-(void)AddRect:(CGRect)rRect;
-(NSString*)TruncateGenreTitle:(NSString*)pName;

@end
