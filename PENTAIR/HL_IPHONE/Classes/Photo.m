//
//  Photo.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "Photo.h"
#import "PhotoAlbum.h"
#import "hlcomm.h"
#import "nsmsg.h"
#import "NSQuery.h"


@implementation CPhoto


/*============================================================================*/

-(id)initWithAlbum:(CPhotoAlbum*)pAlbum Index:(int)iIndex Name:(NSString*)pName

/*============================================================================*/
{
    m_pAlbum = pAlbum;
    m_iIndex = iIndex;
    m_pName  = pName;
    [m_pName retain];
    m_pLoResReceiver = [[CPhotoReceiver alloc] initWithPhoto:self HiRes:FALSE];
    m_pHiResReceiver = [[CPhotoReceiver alloc] initWithPhoto:self HiRes:TRUE];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [super dealloc];
}
/*============================================================================*/

-(CPhotoAlbum*)Album                    { return m_pAlbum;  }
-(int)Index                             { return m_iIndex;  }

/*============================================================================*/

-(UIImage*)GetImage:(BOOL)bHiRes View:(UIView*)pView

/*============================================================================*/
{
    if (bHiRes)
        return [m_pHiResReceiver GetImage:pView];
    return [m_pLoResReceiver GetImage:pView];
}
/*============================================================================*/

-(void)RemoveFromWaitList:(UIView*)pView

/*============================================================================*/
{
    [m_pHiResReceiver RemoveFromWaitList:pView];
    [m_pLoResReceiver RemoveFromWaitList:pView];
}
/*============================================================================*/

-(void)ReleaseHiRes

/*============================================================================*/
{
    [m_pHiResReceiver release];
    m_pHiResReceiver = [[CPhotoReceiver alloc] initWithPhoto:self HiRes:TRUE];
}

@end


@implementation CPhotoReceiver


/*============================================================================*/

-(id)initWithPhoto:(CPhoto*)pPhoto HiRes:(BOOL)bHiRes

/*============================================================================*/
{
    m_bWaiting      = FALSE;
    m_pPhoto        = pPhoto;
    m_bHiRes        = bHiRes;
    m_pData         = NULL;
    m_iNData        = 0;
    m_pWaitList     = [[NSMutableArray alloc] init];

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pWaitList release];
    if (m_pData != NULL)
        free(m_pData);
    if (m_bWaiting)
    {
        [CHLComm RemoveSocketSink:self];
    }

    [super dealloc];
}
/*============================================================================*/

-(UIImage*)GetImage:(UIView*)pView

/*============================================================================*/
{
    if (m_pData != NULL)
    {
        NSData *pData = [NSData dataWithBytes:m_pData length:m_iNData];
        UIImage *pImage = [UIImage imageWithData:pData];
        return pImage;
    }

    if ([m_pWaitList indexOfObject:pView] == NSNotFound)
        [m_pWaitList addObject:pView];

    if (m_bWaiting)
        return NULL;
        
    int iDY = 60;
    if (m_bHiRes)
        iDY = 320;
    int iDX = 4*iDY/3;

    [CHLComm AddSocketSink:self];

    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_PICSERVER_GETSLIDEDXDYINDEXQ] autorelease];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ PutInt:iDX];
    [pQ PutInt:iDY];
    [pQ PutInt:[[m_pPhoto Album] ID]];
    [pQ PutInt:[m_pPhoto Index]];
    [pQ PutInt:0];
    [pQ SendMessageWithMyID:self];
    

    m_bWaiting = TRUE;
    return NULL;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_PICSERVER_IMAGEDATA)
        return;
    [pMSG GetInt];  //DX
    [pMSG GetInt];  //DY

    if (m_pData != NULL)
        free(m_pData);

    m_iNData = [pMSG DataSize] - 8;
    m_pData = malloc(m_iNData);
    unsigned char *pData = [pMSG GetDataAtReadIndex];
    memcpy(m_pData, pData, m_iNData);

    m_bWaiting = FALSE;
    [CHLComm RemoveSocketSink:self];

    for (int i = 0; i < [m_pWaitList count]; i++)
    {
        UIView *pView = (UIView*)[m_pWaitList objectAtIndex:i];
        [pView setNeedsDisplay];
    }

    [m_pWaitList removeAllObjects];
}
/*============================================================================*/

-(void)RemoveFromWaitList:(UIView*)pView

/*============================================================================*/
{
    [m_pWaitList removeObject:pView];
}
@end
