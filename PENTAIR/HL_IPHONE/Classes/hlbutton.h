//
//  hlbutton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CUserTabIconView;
@class CShader;
@class CButtonHighlight;
@class CHLButton;
@class CButtonElementLayer;
@class CHatSwitch;
@class CNSEdgeList;

#define ICON_GRAYUP             50
#define ICON_GRAYDN             51
#define ICON_SEEKUP             52
#define ICON_SEEKDN             53
#define ICON_TUNEUP             54
#define ICON_TUNEDN             55


/*============================================================================*/

@interface CHLButton : UIControl

/*============================================================================*/
{
    int                             m_iFlags;
    int                             m_iShadeIn;
    int                             m_iShadeOut;
    int                             m_iRad;
    int                             m_iTextHeight;
    int                             m_iIconFormat;
    int                             m_iStyle;
    NSString                        *m_pText;
    NSString                        *m_pIcon;
    

    UILabel                         *m_pLabel;
    CUserTabIconView                *m_pIconView;

    UIColor                         *m_RGB;
    UIColor                         *m_RGBFlash;
    UIColor                         *m_RGBCheck;

    id                              m_pCommandResponder;
    int                             m_iCommandID;
    int                             m_iReleaseID;
    
    BOOL                            m_bChecked;

    int                             m_iFlashState;

    int                             m_iRepeatDwell;
    int                             m_iRepeatRate;
    
    NSTimer                         *m_pDwellTimer;
    NSTimer                         *m_pRepeatTimer;

    int                             m_iUserData;
    
    CHatSwitch                      *m_pHatSwitch;
    CNSEdgeList                     *m_pEdgeList;
    CGRect                          m_rContent;
    CGImageRef                      m_pImageStd;
    CGImageRef                      m_pImageFlash;
}

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconAlign
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Icon:(NSString*)sIcon;


-(id)initWithFrame:(CGRect)rFrame Text:(NSString*)pText;

-(void)InitShader;
-(void)InitEvents;
                    
-(CGRect)IconRect;
-(CGRect)TextRect;

CGRect                      SplitRectY(
CGRect                      rStart,
int                         iChunkSize,
int                         iNChunksTotal,
int                         iStartingIndex,
int                         iNChunks );

-(int)CommandID;
-(void)SetCommandID:(int)iCommandID ReleaseID:(int)iReleaseID Responder:(id)pResponder;
-(void)SetCommandID:(int)iCommandID Responder:(id)pResponder;
-(void)SetRepeatDwell:(int)iDwell Rate:(int)iRate;

-(void)ButtonPress:(id)sender;
-(void)ButtonRelease:(id)sender;

-(int)ShadeIn;
-(int)ShadeOut;
-(int)ROutside;
-(void)SetShadeIn:(int)iNew;
-(void)SetShadeOut:(int)iNew;
-(void)SetROutside:(int)iNew;
-(void)SetHatSwitch:(CHatSwitch*)pHatSwitch;
-(void)SetRenderDataStd:(CGImageRef)pRefStd Flash:(CGImageRef)pRefFlash Edges:(CNSEdgeList*)pEdges ContentRect:(CGRect) rContent;
-(UIColor*)RGBFace;
-(UIColor*)RGBFlash;
-(UIColor*)RGBCheck;

-(UILabel*)Label;

-(void)IconFormat:(int)iNew;
-(void)SetIcon:(NSString*)psIcon;
-(void)SetIconEmbedded:(NSString*)psIcon;
-(void)SetColor:(UIColor*)pRGB;
-(void)SetCheckedColor:(UIColor*)pRGB;
-(void)SetTextColor:(UIColor*)pRGB;
-(void)SetTextSize:(int)iTextSize;
-(void)SetMaxLinesText:(int)iNew;
-(void)SetText:(NSString*)pText;

-(BOOL)IsChecked;
-(void)SetChecked:(BOOL)bNew;
-(void)SetFlashing:(BOOL)bNew;
-(void)SetFlashingColor:(UIColor*)pRGB;
-(void)SetEnabled:(BOOL)bNew;
-(CGImageRef)ImageRefForState:(int)iState;

-(void)PressDwell;
-(void)PressRepeat;

-(int)GetData;
-(void)SetData:(int)iData;

-(BOOL)HasCapture;

+(NSString*)EmbeddedIconName:(int)iType;
@end

/*============================================================================*/

@interface CToolBarButton : CHLButton

/*============================================================================*/
{
    int                             m_iOrientation;
    int                             m_iFrameStyle;
}

-(id)initWithFrame:(CGRect)frame Text:(NSString*)pText Icon:(NSString*)psIcon;
-(void)Orientation:(int)iNew;
-(void)FrameStyle:(int)iNew;

@end
