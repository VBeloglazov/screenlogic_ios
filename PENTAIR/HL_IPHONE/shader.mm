/*
--------------------------------------------------------------------------------

    SHADER.MM

    Copyright 2011 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#import "shader.h"
#import "hlstd.h"
#import "hlcomm.h"
#import "hlbutton.h"
#import "crect.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

#include "dibdc.h"
#include "roundrect.h"
#include "shadedib.h"
#include "speed.h"
#include "colormap.h"


@implementation CShader

/*============================================================================*/

-(id)init

/*============================================================================*/
{
    self = [super init];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

+(CGImageRef)CreateRecessedImageDX:(int)iDX DY:(int)iDY Rad:(int)iRad SHOUT:(int)iSHOUT

/*============================================================================*/
{
    if (iDX <= 0)
        return NULL;
    if (iDY <= 0)
        return NULL;
    int iMinDim = MIN(iDX, iDY);
    iSHOUT = MIN(iSHOUT, iMinDim / 2);
    iSHOUT = MAX(iSHOUT, 0);
    iRad = MIN(iRad, iMinDim / 2);
    
    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    dib.Fill(0x0000);


    if (iSHOUT > 0)
    {
        int iDXOff = 0;
        int iDYOff = 0;
        CShadeDIB shader(&dib, 0, 0, iDX, iDY, &iDXOff, &iDYOff, SHADEDIB_3D);
        shader.InitStandard(iRad+iSHOUT, iSHOUT+1, CShadeDIB::INIT_RAISED_EXT, 0, 0x0000);
    }


    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDX, iDY, 8, iDX*4, pColorSpace, kCGImageAlphaPremultipliedFirst);


    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;
}
/*============================================================================*/

+(CGImageRef)CreateButtonImageDX:(int)iDX DY:(int)iDY Rad:(int)iRad SHIN:(int)iSHIN SHOUT:(int)iSHOUT ColorT:(unsigned int)rgbT ColorB:(unsigned int)rgbB;

/*============================================================================*/
{
    if (iDX <= 0)
        return NULL;
    if (iDY <= 0)
        return NULL;
    int iMinDim = MIN(iDX, iDY);
    iSHIN = MIN(iSHIN, iMinDim / 2);
    iSHOUT = MIN(iSHOUT, iMinDim / 2 - iSHIN);
    iSHOUT = MAX(iSHOUT, 0);
    iRad = MIN(iRad, iMinDim / 2);
    
    
    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    dib.Fill(0x0000);
    int iFlags = 0;

    if (iSHOUT == 1)
        iSHOUT = 2;

    if (iSHOUT > 0)
    {
//        if (iSHOUT > 1)
//            iFlags = SHADEDIB_HINT_INT_OVER_EXT;
        int iDXOff = 0;
        int iDYOff = 0;
        CShadeDIB shader(&dib, 0, 0, iDX, iDY, &iDXOff, &iDYOff, SHADEDIB_3D);
        shader.InitStandard(iRad+iSHOUT, iSHOUT, CShadeDIB::INIT_RAISED_EXT, 0, 0, 0);
    }


    int iDXOff = 0;
    int iDYOff = 0;
    CShadeDIB shader(&dib, iSHOUT, iSHOUT, iDX-2*iSHOUT, iDY-2*iSHOUT, &iDXOff, &iDYOff, SHADEDIB_3D);


    shader.ApplyDirectColor(iRad, iSHIN, 0, rgbT, rgbB, iFlags);
    dib.RenderButtonOverlay(iRad, 0xFFFFFFFF, iSHOUT, 0, 0);


    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDX, iDY, 8, iDX*4, pColorSpace, kCGImageAlphaPremultipliedFirst);


    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;
}
/*============================================================================*/

+(CGImageRef)CreateMainTabImageDX:(int)iDX DY:(int)iDY Selected:(int)bSelected

/*============================================================================*/
{
    if (iDX <= 0)
        return NULL;
    if (iDY <= 0)
        return NULL;
    int iRad = [CHLComm GetInt:INT_MAINTAB_ROUT];

    int iMinDim = MIN(iDX, iDY);
    iRad = MIN(iRad, iMinDim / 2);
    
    
    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    unsigned int rgbT = [CHLComm GetRGBWin32:RGB_MAINTAB_TOP];
    unsigned int rgbB = [CHLComm GetRGBWin32:RGB_MAINTAB_BTM];
    if (bSelected)
    {
        rgbT = [CHLComm GetRGBWin32:RGB_MAINTAB_SELECT_TOP];
        rgbB = [CHLComm GetRGBWin32:RGB_MAINTAB_SELECT_BTM];
    }


    int iA = [CHLComm GetInt:INT_MAINTAB_ALPHA];
    if (bSelected)
        iA = [CHLComm GetInt:INT_MAINTAB_SELECT_ALPHA];

    double dA = (double)iA / 100.f;

    rgbT = CHLDC::ScaleColor(dA, rgbT);
    rgbB = CHLDC::ScaleColor(dA, rgbB);

    CHLRect rGradient(0, 0, iDX, iDY);
    dib.GradientFill(rGradient, rgbT, rgbB, DIR_DOWN);
    
    CRoundRect roundRect(iDX, iDY, iRad, 1, 0x0000, 0x0000, 0x0000, 0xFFFFFFFF, 0);
    roundRect.ZeroOutsideRadius(&dib, 0xFFFFFFFF);
    
    iA = 255 * iA / 100;
    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pP = dib.Pixel(0, iY);
        for (int iX = 0; iX < iDX; iX++)
        {
            HL_PIXEL wP = *pP;
            int iThisA = MIN(iA, wP & 0x000000FF);
            wP = (wP & 0xFFFFFF00) | iThisA;
            *pP = wP;
            pP++;
        }
    }


    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDX, iDY, 8, iDX*4, pColorSpace, kCGImageAlphaPremultipliedFirst);


    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;

}
/*============================================================================*/

+(CGImageRef)CreateRegionImageDX:(int)iDX 
                              DY:(int)iDY 
                              Rad:(int)iRad 
                              RoundedEdges:(int)iEdges 
                              ColorT:(unsigned int*)prgbT 
                              ColorM:(unsigned int*)prgbM 
                              ColorB:(unsigned int)rgbB 
                              FillCenter:(int)bFill 
                              FillColor:(unsigned int)rgbFill 
                              Modifier:(id)pModifier 
                              SolidBG:(UIColor*)pSolidBG

/*============================================================================*/
{
    if (iDX <= 0)
        return NULL;
    if (iDY <= 0)
        return NULL;
    int iMinDim = MIN(iDX, iDY);
    iRad = MIN(iRad, iMinDim);
    
    iDX = __min(iDX, 3000);
    iDY = __min(iDY, 3000);


    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    dib.Fill(0x0000);

    CHLRect rGradient(0, 0, iDX, iDY);

    if (prgbT != NULL && prgbM != NULL)
    {
        CGradient2D gradient;
        gradient.Set(*prgbT, *prgbM, rgbB, dib.DY());
        gradient.BltTo(dib.Pixel(0,0), 0, 0, dib.ImageDX(), dib.DY(), dib.DX(), 100);
    }
    else if (prgbT != NULL)
    {
        dib.GradientFill(rGradient, *prgbT, rgbB, DIR_DOWN);
    }
    else
    {
        dib.FillRect(rgbB | 0xFF000000, rGradient);
    }

    [pModifier DoModifyStage1:&dib];

    unsigned int rgbEdge = [CShader MakeRGB:[CHLComm GetRGB:RGB_EDGE]];
    int iCorners = 0;
    if (iEdges & RREGION_EDGE_TOP)
    {
        iCorners |= (Bit(CORNER_UL)|Bit(CORNER_UR));
    }
    if (iEdges & RREGION_EDGE_BOTTOM)
    {
        iCorners |= (Bit(CORNER_LL)|Bit(CORNER_LR));
    }
    if (iEdges & RREGION_EDGE_LEFT)
    {
        iCorners |= (Bit(CORNER_LL)|Bit(CORNER_UL));
    }
    if (iEdges & RREGION_EDGE_RIGHT)
    {
        iCorners |= (Bit(CORNER_LR)|Bit(CORNER_UR));
    }


    int iFlags = ROUNDRECT_ADDMODE;
    if (bFill)
        iFlags |= ROUNDRECT_INVERT;

    CRoundRect roundRect(iDX, iDY, iRad, 1, rgbEdge, 0x0000, 0x0000, iCorners, iFlags);

    if (bFill)
    {
        roundRect.FillInterior(&dib, rgbFill);
    }

    if (pSolidBG != NULL)
    {
        CGColorRef cgColor = pSolidBG.CGColor;
        const CGFloat *fRGB = CGColorGetComponents(cgColor);
        int iR = fRGB[0] * 255.0f;
        int iG = fRGB[1] * 255.0f;
        int iB = fRGB[2] * 255.0f;
        unsigned int rgb = (iB<<24)|(iG<<16)|(iR<<8);
        roundRect.ZeroOutsideRadiusWith(&dib, iCorners, rgb);
    }

    roundRect.DrawX(&dib, 0, 0);

    if (pSolidBG == NULL)
        roundRect.ZeroOutsideRadius(&dib, iCorners);

    [pModifier DoModifyStage2:&dib];

    CGBitmapInfo info = kCGImageAlphaPremultipliedFirst;
    if (iRad <= 0 || iCorners == 0 || pSolidBG != NULL)
    {
        info = kCGImageAlphaNoneSkipFirst;
    }

    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDX, iDY, 8, iDX*4, pColorSpace, info);
    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;
}
/*============================================================================*/

+(CGImageRef)CreateRegionSeparatorDX:(int)iDX Position:(int)iPosition

/*============================================================================*/
{
    double dPos = (double)(100-iPosition) / 100.f;
    UIColor *pT = [CHLComm GetRGB:RGB_PAGETAB_TOP];
    UIColor *pB = [CHLComm GetRGB:RGB_PAGETAB_BTM];
    UIColor *pThis = [self Blend:dPos RGB1:pT RGB2:pB];
    unsigned int rgbBase = [self MakeRGB:pThis];
    int iR = GetRValue(rgbBase);
    int iG = GetGValue(rgbBase);
    int iB = GetBValue(rgbBase);
    unsigned int rgbEdge = [CHLComm GetRGBWin32:RGB_EDGE];
    int iRAdd = GetRValue(rgbEdge);
    int iGAdd = GetRValue(rgbEdge);
    int iBAdd = GetRValue(rgbEdge);
    int iRT = MIN(255, iR + iRAdd * DARK_SIDE_PCT / 100);
    int iGT = MIN(255, iG + iGAdd * DARK_SIDE_PCT / 100);
    int iBT = MIN(255, iB + iBAdd * DARK_SIDE_PCT / 100);
    int iRB = MIN(255, iR + iRAdd * LIGHT_SIDE_PCT / 100);
    int iGB = MIN(255, iG + iGAdd * LIGHT_SIDE_PCT / 100);
    int iBB = MIN(255, iB + iBAdd * LIGHT_SIDE_PCT / 100);
    unsigned int rgbT = (unsigned int)((iBT << 24)|(iGT << 16)|(iRT << 8));
    unsigned int rgbB = (unsigned int)((iBB << 24)|(iGB << 16)|(iRB << 8));
    
    CHLDibDC dib(iDX, 3, 0, 0, MEMORY_SYSTEM);
    HL_PIXEL *pPixT = dib.Pixel(0, 0);
    HL_PIXEL *pPixM = dib.Pixel(0, 1);
    HL_PIXEL *pPixB = dib.Pixel(0, 2);

    for (int i = 0; i < iDX; i++)
    {
        *pPixM = 0;
        *pPixT = rgbT;
        *pPixB = rgbB;
        pPixT++;
        pPixB++;
        pPixM++;
    }
    
    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDX, dib.DY(), 8, iDX*4, pColorSpace, kCGImageAlphaNoneSkipFirst);
    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;
}
/*============================================================================*/

+(CGImageRef)CreateToolBarButtonImage:(int)iDX DY:(int)iDY ColorT:(unsigned int)rgbT ColorB:(unsigned int) rgbB Edge:(unsigned int)rgbEdge Orientation:(int)iOrientation

/*============================================================================*/
{
    CHLDibDC dib(iDX, iDY, 0, 0, MEMORY_SYSTEM);
    dib.Fill(0x0000);

    CHLRect rGradient(0, 0, iDX, iDY);
    dib.GradientFill(rGradient, rgbT, rgbB, DIR_DOWN);

    int iOriginX = 0;
    int iDXRect = iDX;

    if (iOrientation == TOOLBAR_BTM_RIGHT)
    {
        iDXRect++;
        iOriginX--;
    }

    if (iOrientation == TOOLBAR_BTM_LEFT)
    {
        iDXRect++;
    }

    CRoundRect roundRect(iDXRect, iDY, 0, 1, rgbEdge, 0x0000, 0x0000, 0xFFFFFFFF, ROUNDRECT_ADDMODE);
    roundRect.DrawX(&dib, iOriginX, 0);
    
    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDX, iDY, 8, iDX*4, pColorSpace, kCGImageAlphaNoneSkipFirst);
    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;
}
/*============================================================================*/

+(void)Init

/*============================================================================*/
{
    g_pSpeedServer = new CSpeedServer();
}
/*============================================================================*/

+(void)Release

/*============================================================================*/
{
    delete g_pSpeedServer;
    g_pSetServer = NULL;
}
/*============================================================================*/

+(unsigned int)BlendPCT:(int)iPCT RGB1:(unsigned int)rgb1 RGB2:(unsigned int)rgb2

/*============================================================================*/
{
    int iR1 = (rgb1 & 0x00FF0000) >> 16;
    int iG1 = (rgb1 & 0x0000FF00) >> 8;
    int iB1 = (rgb1 & 0x000000FF);
    int iR2 = (rgb2 & 0x00FF0000) >> 16;
    int iG2 = (rgb2 & 0x0000FF00) >> 8;
    int iB2 = (rgb2 & 0x000000FF);
    int _iPCT = 100 - iPCT;
    int iR = (iR1 * iPCT + iR2 * _iPCT) / 100;
    int iG = (iG1 * iPCT + iG2 * _iPCT) / 100;
    int iB = (iB1 * iPCT + iB2 * _iPCT) / 100;
    unsigned int iRet = ((iR << 16) | (iG << 8) | iB);
    return iRet;
}
/*============================================================================*/

+(unsigned int)MakeRGB:(int)iR G:(int)iG B:(int)iB

/*============================================================================*/
{
    unsigned int iRet = ((iR << 16) | (iG << 8) | iB);
    return iRet;
}
/*============================================================================*/

+(unsigned int)MakeRGB:(UIColor*)pColor

/*============================================================================*/
{
    if (pColor == NULL)
        return 0;
    CGColorRef cgColor = pColor.CGColor;
    const CGFloat *fRGB = CGColorGetComponents(cgColor);
    int iR = fRGB[0] * 255.0f;
    int iG = fRGB[1] * 255.0f;
    int iB = fRGB[2] * 255.0f;
    return [self MakeRGB:iR G:iG B:iB];
    
}
/*============================================================================*/

+(UIColor*)Blend:(float)f1 RGB1:(UIColor*)rgb1 RGB2:(UIColor*)rgb2

/*============================================================================*/
{
    CGColorRef cgColor1 = rgb1.CGColor;
    CGColorRef cgColor2 = rgb2.CGColor;

    const CGFloat *fRGB1 = CGColorGetComponents(cgColor1);
    const CGFloat *fRGB2 = CGColorGetComponents(cgColor2);

    float fR = fRGB1[0] * f1 + fRGB2[0] * (1.0-f1);
    float fG = fRGB1[1] * f1 + fRGB2[1] * (1.0-f1);
    float fB = fRGB1[2] * f1 + fRGB2[02] * (1.0-f1);
    return [UIColor colorWithRed:fR green:fG blue:fB alpha:1.0];
}
/*============================================================================*/

+(UIColor*)Add:(UIColor*)rgb1 Val:(int)iVal

/*============================================================================*/
{
    CGColorRef cgColor1 = rgb1.CGColor;
    const CGFloat *fRGB1 = CGColorGetComponents(cgColor1);

    float fAdd = (float) iVal / 255.0f;

    float fR = __min(1.0, fRGB1[0] +fAdd);
    float fG = __min(1.0, fRGB1[1] +fAdd);
    float fB = __min(1.0, fRGB1[2] +fAdd);
    return [UIColor colorWithRed:fR green:fG blue:fB alpha:1.0];
}
/*============================================================================*/

+(unsigned int)AddRGB:(unsigned int)rgb1 Val:(int)iVal

/*============================================================================*/
{
    int iB = rgb1 & 0x000000FF;
    int iG = (rgb1 & 0x0000FF00) >> 8;
    int iR = (rgb1 & 0x00FF0000) >> 16;
    iB = MIN(iB+iVal, 255);
    iG = MIN(iG+iVal, 255);
    iR = MIN(iR+iVal, 255);
    unsigned int iRes = (iR<<16)|(iG<<8)|iB;
    return iRes;
}
/*============================================================================*/

+(void)MakeSpread:(int)iSpread Color1:(unsigned int*)p1 Color2:(unsigned int*)p2

/*============================================================================*/
{
    CHLDC::CreateGradient(*p1,iSpread, p1, p2);
/*
    int iR1 = GetRValue(*p1);
    int iG1 = GetGValue(*p1);
    int iB1 = GetBValue(*p1);
    int iR2 = iR1;
    int iG2 = iG1;
    int iB2 = iB1;
    int iR = iR1;
    int iG = iG1;
    int iB = iB1;
    iR2 = MIN(0xFF, iR+iSpread);
    iG2 = MIN(0xFF, iG+iSpread);
    iB2 = MIN(0xFF, iB+iSpread);
    int iSubR = MAX(0, iR+iSpread-255);
    int iSubG = MAX(0, iG+iSpread-255);
    int iSubB = MAX(0, iB+iSpread-255);
    iR1 -= iSubR;
    iG1 -= iSubG;
    iB1 -= iSubB;
    *p1 = (iR1 << 16)|(iG1 << 8)|iB1;
    *p2 = (iR2 << 16)|(iG2 << 8)|iB2;
*/
}
@end

@implementation CNSShader
/*============================================================================*/

-(id)initWithDX:(int)iDX DY:(int)iDY;

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_pDIB = NULL;
        if (iDX > 0 && iDY > 0)
        {
            m_pDIB = new CShadeDIB(iDX, iDY);
        }
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    delete m_pDIB;
    [super dealloc];
}
/*============================================================================*/

-(CShadeDIB*)DIB

/*============================================================================*/
{
    return m_pDIB;
}
/*============================================================================*/

-(int)DX

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return 0;
    return m_pDIB->DX();
}
/*============================================================================*/

-(int)DY

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return 0;
    return m_pDIB->DY();
}
/*============================================================================*/

-(int)GetUserData                   { return m_iUserData;   }
-(void)SetUserData:(int)iNew        { m_iUserData = iNew;   }

/*============================================================================*/

-(void)InitStandard:(int)iRad Shading:(int)iShading Type:(int)iType Inset:(int)iInset

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return;

    CShadeDIB::INIT_TYPE Type = CShadeDIB::INIT_RAISED_INT;
    switch (iType)
    {
    case NS_INIT_RAISED_INT:            Type = CShadeDIB::INIT_RAISED_INT; break;
    case NS_INIT_RAISED_EXT:            Type = CShadeDIB::INIT_RAISED_EXT; break;
    case NS_INIT_RAISED_INT_INV:        Type = CShadeDIB::INIT_RAISED_INT_INV; break;
    case NS_INIT_RAISED_EXT_INV:        Type = CShadeDIB::INIT_RAISED_EXT_INV; break;
    case NS_INIT_RAISED_INT_GRADIENT:   Type = CShadeDIB::INIT_RAISED_INT_GRADIENT; break;
    }

    m_pDIB->InitStandard(iRad, iShading, Type, iInset, 0x0000, SHADEDIB_DITHER_HIGH_EDGE|SHADEDIB_DITHER_LOW_EDGE);
}
/*============================================================================*/

-(void)Subtract:(CNSShader*)pOther X:(int)iX Y:(int)iY

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return;
    CShadeDIB *pDIB = [pOther DIB];
    if (pDIB == NULL)
        return;

    m_pDIB->Subtract(pDIB, iX, iY);
}
/*============================================================================*/

-(void)AndWith:(CNSShader*)pOther X:(int)iX Y:(int)iY Flip:(int)iFlip

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return;
    CShadeDIB *pDIB = [pOther DIB];
    if (pDIB == NULL)
        return;

    m_pDIB->And(pDIB, iX, iY, iFlip);
}
/*============================================================================*/

-(void)InitWedgeDX:(int)iDYL YR:(int)iDYR Shading:(int)iNShading SHOUT:(int)iSHOUT Raised:(int)bRaised

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return;
    m_pDIB->InitWedgeDX(iDYL, iDYR, iNShading, iSHOUT, bRaised);
}
/*============================================================================*/

-(void)InitWedgeDY:(int)iDXT XB:(int)iDXB Shading:(int)iNShading SHOUT:(int)iSHOUT Raised:(int)bRaised

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return;
    m_pDIB->InitWedgeDY(iDXT, iDXB, iNShading, iSHOUT, bRaised);
}
/*============================================================================*/

-(void)BltFromX:(int)iX Y:(int)iY Source:(CNSShader*)pOther XSrc:(int)iXSrc YSrc:(int)iYSrc DX:(int)iDX DY:(int)iDY

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return;
    CShadeDIB *pDIB = [pOther DIB];
    if (pDIB == NULL)
        return;
    m_pDIB->BltFromDibDC(iX, iY, pDIB, iXSrc, iYSrc, iDX, iDY);
}
/*============================================================================*/

-(CGImageRef)CreateImageWithColor1:(unsigned int)rgb1 Color2:(unsigned int)rgb2 ShadeIn:(int)iShadeIn ShadeOut:(CNSShader*)pShadeOut Bounds:(CGRect)bounds

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return NULL;

    int iDXOut = bounds.size.width;
    int iDYOut = bounds.size.height;

    CHLDibDC dib(iDXOut, iDYOut, 0, 0, MEMORY_SYSTEM);
    dib.Fill(0x0000);
    
    int iXOffset = -bounds.origin.x;
    int iYOffset = -bounds.origin.y;

    int iDX = m_pDIB->DX();
    int iDY = m_pDIB->DY();

    CShadeDIB *pShaderOut = [pShadeOut DIB];
    int iHints = 0;

    if (pShaderOut != NULL)
    {
        iHints = SHADEDIB_HINT_INT_OVER_EXT;
        pShaderOut->ApplyTo(&dib, iXOffset, iYOffset, 0, 0, iDX, iDY);
    }

    m_pDIB->ApplyToWithColors(&dib, iXOffset, iYOffset, iShadeIn, rgb1, rgb2, iHints);
    m_pDIB->ApplyShineyGradient(&dib, iXOffset, iYOffset);

    BYTE *pBits = (BYTE*)dib.Pixel(0,0);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pDIBContext = CGBitmapContextCreate(pBits, iDXOut, iDYOut, 8, iDXOut*4, pColorSpace, kCGImageAlphaPremultipliedFirst);
    CGImageRef pRef = CGBitmapContextCreateImage(pDIBContext);
    CGContextRelease(pDIBContext);
    CGColorSpaceRelease(pColorSpace);
    return pRef;
}
/*============================================================================*/

-(CNSEdgeList*)GenEdgeListByElevSunken

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return NULL;
    CEdgeList *pEdgeList = m_pDIB->GenEdgeListByElevSunken();
    CNSEdgeList *pRet = [[CNSEdgeList alloc] initWithEdgeList:pEdgeList];
    return pRet;
}
/*============================================================================*/

-(CNSEdgeList*)GenEdgeListByElev:(int)iElev

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return NULL;
    CEdgeList *pEdgeList = m_pDIB->GenEdgeListByElev(iElev);
    CNSEdgeList *pRet = [[CNSEdgeList alloc] initWithEdgeList:pEdgeList];
    return pRet;
}
/*============================================================================*/

-(CGRect)FindContentRect:(CNSEdgeList*)pEdgeList

/*============================================================================*/
{
    if (m_pDIB == NULL)
        return CGRectZero;
    CEdgeList *pEdges = [pEdgeList EdgeList];
    if (pEdges == NULL)
        return CGRectZero;

    CHLRect rect = m_pDIB->FindContentRect(pEdges);
    return CGRectMake(rect.XLeft(), rect.YTop(), rect.DX(), rect.DY());
}

@end 

@implementation CNSEdgeList
/*============================================================================*/

-(id)initWithEdgeList:(CEdgeList*)pEdgeList

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_pEdgeList = pEdgeList;
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    delete m_pEdgeList;
    [super dealloc];
}
/*============================================================================*/

-(void)TranslateDX:(int)iDX DY:(int)iDY

/*============================================================================*/
{
    if (m_pEdgeList == NULL)
        return;
    m_pEdgeList->Translate(iDX, iDY);
}
/*============================================================================*/

-(CGRect)GetBounds

/*============================================================================*/
{
    if (m_pEdgeList == NULL)
        return CGRectZero;
    CHLRect rect = m_pEdgeList->GetBounds();
    return CGRectMake(rect.XLeft(), rect.YTop(), rect.DX(), rect.DY());
}
/*============================================================================*/

-(CEdgeList*)EdgeList

/*============================================================================*/
{
    return m_pEdgeList;
}
/*============================================================================*/

-(int)IsPointInside:(CGPoint)pt

/*============================================================================*/
{
    if (m_pEdgeList == NULL)
        return FALSE;
    return m_pEdgeList->ContainsPoint(pt.x, pt.y);
}
@end

@implementation CRegionModifierHZBar
/*============================================================================*/

-(id)initWithDir:(int)iDir Size:(int)iSize

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_iDir = iDir;
        m_iSize = iSize;
    }

    return self;
}
/*============================================================================*/

-(void)DoModifyStage1:(CHLDibDC*)pDIB

/*============================================================================*/
{
    int iY = m_iSize;
    if (m_iDir == DIR_DOWN)
        iY = pDIB->DY() - m_iSize-1;
    HL_PIXEL *pSet = pDIB->Pixel(0, iY);
    memset(pSet, 0, pDIB->ImageDX()*sizeof(HL_PIXEL));
}
/*============================================================================*/

-(void)DoModifyStage2:(CHLDibDC*)pDIB

/*============================================================================*/
{
}
@end