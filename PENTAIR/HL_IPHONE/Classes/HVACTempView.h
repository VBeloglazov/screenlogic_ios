//
//  HVACTempView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "hlview.h"

#define TEMP_ROOM                       0
#define TEMP_COOL                       1
#define TEMP_HEAT                       2
#define TEMP_HUMIDITY                   3
#define TEMP_HUMSPLO                    4
#define TEMP_HUMSPHI                    5

@class CTStat;
@class CHLButton;
@class CHVACModePage_IPOD;

/*============================================================================*/

@interface CHVACTempView : UIView  < CSink, CHLCommandResponder, CHLView >

/*============================================================================*/
{
    BOOL                                m_bReadOnly;
    int                                 m_iMode;
    CTStat                              *m_pTStat;
    UILabel                             *m_pLabel;
    UILabel                             *m_pTempText;

    CHLButton                           *m_pUp;
    CHLButton                           *m_pDn;
    
    BOOL                                m_bFlashEnable;
    BOOL                                m_bFlashing;
    BOOL                                m_bFlashOn;
    
    int                                 m_iLocalTemp;
    
    int                                 m_iDXLast;
    int                                 m_iDYLast;

}

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat Mode:(int)iMode ReadOnly:(BOOL)bReadOnly;
-(void)Notify:(int)iID Data:(int)iData;
-(BOOL)ShouldFlash;
-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;
-(void)SetVisible:(BOOL)bVisible;
@end
