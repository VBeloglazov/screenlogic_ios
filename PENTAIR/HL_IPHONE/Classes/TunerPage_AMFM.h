//
//  TunerPage_AMFM.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/9/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "HLView.h"

@class CAudioZone;
@class CTunerFavsPage;
@class CHLButton;
@class NSTunerControl;
@class CTunerHeaderView;

/*============================================================================*/

@interface CTunerPageAMFM : UIView      < CSink, CHLCommandResponder >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    
    CHLButton                           *m_pDigits[12];

    UILabel                             *m_pTuneLabel;
    UILabel                             *m_pSeekLabel;
    UILabel                             *m_pHDTuneLabel;

    CHLButton                           *m_pTuneBtns[2];
    CHLButton                           *m_pSeekBtns[2];
    CHLButton                           *m_pHDTuneBtns[2];
    
    // Scanning Vars
    int                                 m_iDTune;

    NSTunerControl                      *m_pTunerControl;
    CTunerHeaderView                    *m_pStatus;
}

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone StatusView:(CTunerHeaderView*)pStatusView;

-(CHLButton*)AddTextButton:(NSString*)pText;
-(CHLButton*)AddIconButton:(int)iIcon Command:(int)iCmd;
-(UILabel*)AddLabel:(NSString*)pText;

-(IBAction)Dig0:(id)sender;
-(IBAction)Dig1:(id)sender;
-(IBAction)Dig2:(id)sender;
-(IBAction)Dig3:(id)sender;
-(IBAction)Dig4:(id)sender;
-(IBAction)Dig5:(id)sender;
-(IBAction)Dig6:(id)sender;
-(IBAction)Dig7:(id)sender;
-(IBAction)Dig8:(id)sender;
-(IBAction)Dig9:(id)sender;

-(void)ProcessDigit:(int)iKey;
-(void)UpdateStationText;

@end
