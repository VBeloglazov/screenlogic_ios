//
//  AudioSource.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNSQuery;

/*============================================================================*/

@interface CAudioSource : NSObject

/*============================================================================*/
{
    NSString                            *m_pSourceName;
    NSString                            *m_pIconName;
    int                                 m_iSourceID;
    int                                 m_iRemoteID;
}

@property (nonatomic, retain) NSString *m_pSourceName;
@property (nonatomic, retain) NSString *m_pIconName;
@property (nonatomic) int m_iSourceID;
@property (nonatomic) int m_iRemoteID;

-(id)initFromQ:(CNSQuery*)pQ;
-(NSString*)Name;

@end
