//
//  NSPoolCircuit.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "NSPoolCircuit.h"
#include "hlstd.h"
#include "poolconfig.h"


@implementation CNSPoolCircuit

/*============================================================================*/

-(id)initWithCircuit:(CPoolCircuit*)pCircuit

/*============================================================================*/
{
    m_pCircuit = pCircuit;
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(NSString*)Name

/*============================================================================*/
{
    CHLString sText = m_pCircuit->Name();
    int iLen = sText.GetLength();
    char *pBuf = sText.GetBufferCHAR(iLen);
    NSString *pRet = [NSString stringWithUTF8String:pBuf];
    return pRet;
}
/*============================================================================*/

-(int)ID

/*============================================================================*/
{
    return m_pCircuit->CircuitID();
}
/*============================================================================*/

-(int)State

/*============================================================================*/
{
    return m_pCircuit->State();
}
/*============================================================================*/

-(int)Function

/*============================================================================*/
{
    return m_pCircuit->Function();
}
@end
