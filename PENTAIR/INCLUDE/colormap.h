/*
--------------------------------------------------------------------------------

    COLORMAP.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/


#ifndef COLORMAP_H
#define COLORMAP_H

#ifdef HL_PIXEL32
    #define MAX_PATTERN             1
#else
    #define MAX_PATTERN             64
#endif

#define MODE_LINEAR             0
#define MODE_PARA_UP            1
#define MODE_PARA_DN            2

class CHLDibDC;

/*============================================================================*/

    class CGradient1D

/*============================================================================*/
{
protected:
    int                         m_iRT;
    int                         m_iGT;
    int                         m_iBT;

    int                         m_iRB;
    int                         m_iGB;
    int                         m_iBB;

    int                         m_iMode;

public:
    CGradient1D(
    COLORREF                    rgbT,
    COLORREF                    rgbB );
    
    inline int                  Mode()          { return m_iMode;   }       
    inline void                 Mode(int iNew)  { m_iMode = iNew;   }

    HL_PIXEL                    PIXELAt(
    int                         iY,
    int                         iYMax );

    COLORREF                    COLORREFAt(
    int                         iY,
    int                         iYMax );

protected:
    int                         InitMapWithColor(
    HL_PIXEL                    *pDst,
    COLORREF                    rgb );
};
/*============================================================================*/

    class CGradient2D

/*============================================================================*/
{
protected:
    int                         m_iNColors;
    int                         *m_piSizes;
    int                         m_iNSetPoints;
    HL_PIXEL                    *m_pDitherMap;

    COLORREF                    m_rgb1;
    COLORREF                    m_rgb2;
    COLORREF                    m_rgb3;

    CHLDibDC                    *m_pRenderDIB;

public:
    CGradient2D();
    virtual ~CGradient2D();

    inline int                  Dimension()     { return m_iNColors;            }

    void                        Set(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iDim );

    void                        Set(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    COLORREF                    rgb3,
    int                         iDim );

    void                        BltTo(
    HL_PIXEL                    *pTgt,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iPitch,
    int                         iAlpha );

    BOOL                        IsSolidColor();

    inline COLORREF             SolidColor()    { return m_rgb1;                }

    CHLDibDC                    *GetGradientRenderDIB();
    void                        ReleaseRenderingResources();

protected:
    void                        Init(
    int                         iDim );
};
/*============================================================================*/

    class CColorMap

/*============================================================================*/
{
public:
    virtual COLORREF            ColorAtPosition(int iPos);

protected:
    COLORREF                    Interpolate(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iPos,
    int                         iDim );
};
/*============================================================================*/

    class CNonLinearColorMap : public CColorMap

/*============================================================================*/
{
protected:
    int                         m_iDim;
    int                         m_iMidPoint;
    int                         m_iR1;
    int                         m_iG1;
    int                         m_iB1;
    int                         m_iR2;
    int                         m_iG2;
    int                         m_iB2;

public:
    CNonLinearColorMap(
    int                         iDim,
    int                         iMidPoint,
    COLORREF                    rgbEnd,
    COLORREF                    rgbMid );

    virtual COLORREF            ColorAtPosition(
    int                         iPos );
};
/*========================================================================*/

    class C3PosColorMap : public CColorMap

/*========================================================================*/
{
protected:
    int                         m_iDim;
    int                         m_iMidPoint;
    
    COLORREF                    m_rgb1;
    COLORREF                    m_rgb2;
    COLORREF                    m_rgb3;

public:
    C3PosColorMap(
    int                         iDim,
    COLORREF                    rgbFace );

    C3PosColorMap(
    int                         iDim,
    int                         iMidPoint,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    COLORREF                    rgb3 );

    virtual COLORREF            ColorAtPosition(
    int                         iPos );
};
/*========================================================================*/

    class CDitherMap

/*========================================================================*/
{
protected:
    HL_PIXEL                    m_wColors[MAX_PATTERN];
    int                         m_iNColors;
    BOOL                        m_iIndex;
    BOOL                        m_bOffset;
    int                         m_iR; 
    int                         m_iG;
    int                         m_iB;

public:
    CDitherMap();

    CDitherMap(
    COLORREF                    rgb );

    inline int                  NColors()   { return m_iNColors;    }
    inline HL_PIXEL             *Map()      { return m_wColors;     }

    void                        Set(
    int                         iR,
    int                         iG,
    int                         iB );

    void                        FillHZ(
    HL_PIXEL                    *pTgt,
    int                         iN,
    int                         iXGeo = 0 );

    void                        AddHZ(
    HL_PIXEL                    *pTgt,
    int                         iN,
    int                         iXGeo = 0 );

    inline HL_PIXEL             NextColor()
    {
        if (m_iIndex >= m_iNColors)
            m_iIndex = 0;
        HL_PIXEL wRet = m_wColors[m_iIndex];
        m_iIndex++;
        return wRet;
    };

protected:
    int                         RError(int iR16Bit, int iR256);
    int                         GError(int iG16Bit, int iG256);
    int                         BError(int iB16Bit, int iB256);
};
#endif

