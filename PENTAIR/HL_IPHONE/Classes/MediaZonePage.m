//
//  MediaZonePage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MediaZonePage.h"
#import "AudioZone.h"
#import "AudioSource.h"
#import "SourceIconView.h"
#import "SourceSelectPage.h"
#import "tmp3.h"
#import "VolumeControl.h"
#import "TunerPage_AMFM.h"
#import "TunerPage_XM.h"
#import "MainTab.h"
#import "MainTabVC.h" 
#import "hlm.h"
#import "CustomMediaPage.h"
#import "hlview.h"
#import "CustomPage.h"
#import "mainview.h"
#import "crect.h"
#import "HLToolBar.h"
#import "hlbutton.h"
#import "SourceIndicator.h"
#import "zoneselector.h"
#import "TunerContainerPage.h"
#import "hlcomm.h"
#import "shader.h"
#import "iconview.h"
#import "AllZonesOffView.h"
#import "MediaKeypadView.h"
#import "MediaZoneBar.h"

#define CMD_ZONE_OFF            1000
#define CMD_BACK                1001
#define CMD_DONE                1002

@implementation CMediaModalViewHeaderPage
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Title:(NSString*)pTitle Icon:(NSString*)psIcon;

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView TEXT_SIZE_TITLE]];
        m_pLabel.textAlignment = UITextAlignmentLeft;
        [m_pLabel setText:pTitle];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
        m_pIconView = [[CUserTabIconView alloc] initWithFrame:CGRectZero];
        [m_pIconView SetEmbeddedIcon:psIcon];
        [self addSubview:m_pIconView];
        [m_pIconView release];
    }
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDXIcon = rThis.size.height * 125/100;
    CGRect rIcon = [CRect BreakOffLeft:&rThis DX:iDXIcon];
    [CRect Inset:&rIcon DX:0 DY:2];
    [m_pIconView setFrame:rIcon];
    [m_pLabel setFrame:rThis];
}

@end

@implementation CMediaModalViewClientPage
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Title:(NSString*)pTitle

/*============================================================================*/
{

    self = [super initWithFrame:rFrame];
    if (self)
    {
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
}

@end


@implementation CMediaModalView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MediaZonePage:(CMediaZonePage*)pPage Type:(int)iType

/*============================================================================*/
{
    self = [super initWithFrame:rFrame Style:SRREGION_DEFAULT];
    if (self)
    {
        m_iType = iType;
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pPages release];
    [super dealloc]; 
}
/*============================================================================*/

-(int)Type

/*============================================================================*/
{
    return m_iType;
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch(iCommandID)
    {
    case CMD_BACK:
        {
            if ([m_pPages count] < 2)
                return;

            CMediaModalViewClientPage *pTopView = (CMediaModalViewClientPage*)[m_pPages objectAtIndex:[m_pPages count]-1];
            CMediaModalViewClientPage *pPrevView = (CMediaModalViewClientPage*)[m_pPages objectAtIndex:[m_pPages count]-2];

            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.25];

            CGRect rThis = self.bounds;
            int iDXTrans = rThis.size.width;
            [pPrevView setFrame:rThis];
            rThis.origin.x += iDXTrans;
            [pTopView setFrame:rThis];
            [pPrevView setAlpha:1.0];
            [pTopView setAlpha:0.0];

            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(PopView:finished:context:)];

            [UIView commitAnimations];
            
            [m_pBackBtn setHidden:[m_pPages count] <= 2];
//            [m_pLabel setText:[pPrevView Title]];
        }
        break;
    case CMD_DONE:
        [m_pMediaZonePage ReleaseModalView];
        break;
    }
}
/*============================================================================*/

-(void)PopView:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    UIView *pTopView = (UIView*)[m_pPages objectAtIndex:[m_pPages count]-1];
    [pTopView removeFromSuperview];
    [m_pPages removeLastObject];
}

@end


@implementation CMediaZonePage
/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    self = [super initWithFrame:rFrame  MainTabPage:pMainTabPage  SubTab:pSubTab];
    if (self)
    {
        m_rBaseFrame = rFrame;
        [self Init:pSubTab];
    }
    return self;
}
/*============================================================================*/

-(void)Init:(CSubTab*)pSubTab

/*============================================================================*/
{
    [self RemoveSubViews];

    if (m_pZone != NULL)
    {
        [m_pZone RemoveSink:self];
    }

    [super Init:pSubTab];
    m_pZone = (CAudioZone*)pSubTab;
    [m_pZone AddSink:self];
    [self InitToolBar];

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [self SetActiveSubPage];
        m_bInit = TRUE;
    }
}
/*============================================================================*/

-(void)UIInit

/*============================================================================*/
{
    if (!m_bInit)
    {
        [self SetActiveSubPage];
        m_bInit = TRUE;
    }
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    if (m_pZone != NULL)
    {
        [m_pZone RemoveSink:self];
    }

    [super dealloc];
}
/*============================================================================*/

- (void)InitToolBar

/*============================================================================*/
{
    [m_pToolBar removeFromSuperview];
    m_pToolBar = [[CHLToolBar alloc] initWithFrame:CGRectZero Orientation:TOOLBAR_BTM];
    [self addSubview:m_pToolBar];
    [m_pToolBar release];
    m_pToolBarClient = [[CMediaZoneToolBarClientView alloc] initWithFrame:CGRectZero Zone:m_pZone Page:self];
    [m_pToolBar SetClientView:m_pToolBarClient AnimationType:TOOLBAR_NONE];
    [m_pToolBarClient release];
    [m_pToolBarClient Notify:m_pZone.m_iID Data:SINK_VOLCHANGED];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_SOURCECHANGED)
    {
        // Remove Any old pages
        [self RemoveSubViews];

        BOOL bReleaseModal = TRUE;
        if ([m_pZone ActiveSourceIndex] == 0)
        {
            if (m_pModalPage  != NULL)
            {
                if ([m_pModalPage Type] == MODAL_SELECTSOURCE)
                    bReleaseModal = FALSE;
            }
        }


        if (bReleaseModal)
        {
            [[m_pToolBarClient SourceButton] SetChecked:NO];
            [[m_pToolBarClient SettingsButton] SetChecked:NO];
            [self ReleaseModalView];
        }

        if (m_bInit)
        {
            [self SetActiveSubPage];
            [CCustomPage SetVisible:m_pPage Visible:[self IsVisible]];
        }

        [self InitSubTabViewOverlay];
        [self UpdateOverlay];
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (iCommandID == CMD_ZONE_OFF)
    {
        [m_pZone SetActiveSourceIndex:0];
    }
}
/*============================================================================*/

-(void)SelectSource

/*============================================================================*/
{
    [self SelectSourceAnimated:YES];
}
/*============================================================================*/

-(void)SelectSourceAnimated:(BOOL)bAnimate

/*============================================================================*/
{
    if (m_pModalPage != NULL)
    {
        if ([m_pZone ActiveSourceIndex] == 0)
        {
            if ([m_pModalPage Type] == MODAL_SELECTSOURCE)
                return;
        }

        BOOL bWasSelect = FALSE;
        if ([m_pModalPage Type] == MODAL_SELECTSOURCE)
            bWasSelect = TRUE;

        [[m_pToolBarClient SourceButton] SetChecked:NO];
        [[m_pToolBarClient SettingsButton] SetChecked:NO];
        [self ReleaseModalView];

        if (bWasSelect)
            return;
    }

    CGRect rFrame = [self ContentPageRect];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInset = [CHLComm GetInt:INT_STD_INSET];
        [CRect Inset:&rFrame DX:iInset DY:iInset];
    }
    CMediaModalView *pView = [[CMediaModalView alloc] initWithFrame:rFrame MediaZonePage:self Type:MODAL_SELECTSOURCE];
    pView.autoresizingMask = 0xFFFFFFFF;

    CSourceSelectPageIPAD *pSources = [[CSourceSelectPageIPAD alloc] initWithFrame:CGRectZero Zone:m_pZone ZonePage:self];
    [[pView LowerRegion] SetClientView:pSources];
    [pSources release];
    
    CMediaModalViewHeaderPage *pHeader = [[CMediaModalViewHeaderPage alloc] initWithFrame:CGRectZero Title:@"Sources" Icon:@"SOURCE.png"];
    [[pView UpperRegion] SetClientView:pHeader];
    [pHeader release];

    [self PresentModalView:pView Animated:bAnimate];
    [pView release];

    [[m_pToolBarClient SourceButton] SetChecked:YES];
}
/*============================================================================*/

-(void)ShowSettings

/*============================================================================*/
{
    if (m_pModalPage != NULL)
    {
        BOOL bWasSettings = FALSE;
        if ([m_pModalPage Type] == MODAL_CONFIG)
            bWasSettings = TRUE;

        [[m_pToolBarClient SourceButton] SetChecked:NO];
        [[m_pToolBarClient SettingsButton] SetChecked:NO];
        [self ReleaseModalView];

        if (bWasSettings)
            return;
    }

    CGRect rFrame = [self ContentPageRect];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInset = [CHLComm GetInt:INT_STD_INSET];
        [CRect Inset:&rFrame DX:iInset DY:iInset];
    }
    CMediaModalView *pView = [[CMediaModalView alloc] initWithFrame:rFrame MediaZonePage:self Type:MODAL_CONFIG];
    pView.autoresizingMask = 0xFFFFFFFF;

    CCustomMediaPage *pSettings = [[CCustomMediaPage alloc] initWithFrame:CGRectZero ID:[m_pZone SettingsID] ZoneID:m_pZone.m_iID ZonePage:NULL];
    [[pView LowerRegion] SetClientView:pSettings];
    [pSettings release];
    
    CMediaModalViewHeaderPage *pHeader = [[CMediaModalViewHeaderPage alloc] initWithFrame:CGRectZero Title:@"Settings" Icon:@"SETTINGS.png"];
    [[pView UpperRegion] SetClientView:pHeader];
    [pHeader release];

    [self PresentModalView:pView Animated:YES];
    [pView release];

    [[m_pToolBarClient SettingsButton] SetChecked:YES];
}
/*============================================================================*/

-(void)InitSubTabViewOverlay

/*============================================================================*/
{
    CSubTabViewOverlay *pOverlay = [self Overlay];

    if (pOverlay == NULL)
        return;

    [pOverlay FreeClientView];

    CGRect rBounds = [pOverlay ClientRect];

    CSourceIconView *pIconView = NULL;

    int iSourceIndex = [m_pZone ActiveSourceIndex];
    if (iSourceIndex > 0)
    {
        CAudioSource *pSrc = [m_pZone Source:(iSourceIndex-1)];
        pIconView = [[CSourceIconView alloc] initWithFrame:rBounds IconName:pSrc.m_pIconName SourceName:pSrc.m_pSourceName];
        [pOverlay SetHighlightMode:TRUE];
    }
    else 
    {
        pIconView = [[CSourceIconView alloc] initWithFrame:rBounds IconName:NULL SourceName:@"Off"];
        [pOverlay SetHighlightMode:FALSE];
    }

    [pOverlay SetClientView:pIconView];
    [pIconView release];
}
/*============================================================================*/

-(void)RemoveSubViews

/*============================================================================*/
{
    NSArray *pArray = self.subviews;
    for (int i = 0; i < [pArray count]; i++)
    {
        UIView *pView = (UIView*)[pArray objectAtIndex:i];
        BOOL bOK = TRUE;
        
        if (pView == m_pToolBar) bOK = FALSE;
        if (pView == m_pModalPage) bOK = FALSE;
        if (pView == _m_pModalPage) bOK = FALSE;
        if (pView == m_pAllZonesOffView) bOK = FALSE;
        if (pView == _m_pAllZonesOffView) bOK = FALSE;
        
        if (bOK)
        {
            [pView removeFromSuperview];
        }
    }

    m_pPage = NULL;
}
/*============================================================================*/

-(void)SetActiveSubPage

/*============================================================================*/
{
    BOOL bAnimate = (m_pPage != NULL);

    CGRect rPage = self.bounds;

    int iSubSource = [m_pZone ActiveSubSource];
    int iSubSourceBasic = iSubSource & 0x0000FFFF;
    int iControlMode = MEDIABAR_DEFAULT;
    if (iSubSourceBasic == AUX_SOURCE_KEYPAD_NEXT_PREV)
        iControlMode = MEDIABAR_PREVNEXT;

    if ([m_pZone ActiveSourceIndex] == 0)
        iControlMode = MEDIABAR_DEFAULT;

    [m_pToolBarClient SetConfig:iControlMode];



    int iSizeToolBar = [m_pToolBarClient RequiredHeight:rPage.size.width];
    [CRect BreakOffBottom:&rPage DY:iSizeToolBar];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInset = [CHLComm GetInt:INT_STD_INSET];
        [CRect Inset:&rPage DX:iInset DY:iInset];
    }

    [m_pPage removeFromSuperview];
    m_pPage     = NULL;

    if ([m_pZone ActiveSourceIndex] == 0)
    {
        // Zone is off
        if (m_pModalPage != NULL)
        {
            return;
        }

        [self SelectSourceAnimated:bAnimate];

        return;
    }

    
    if (m_pPage == NULL)
    {
        if ([m_pZone ActiveMP3])
        {
            m_pPage = [[CMP3InfoPage alloc] initWithFrame:rPage Zone:m_pZone ZonePage:self];
        }
    }

    if (m_pPage == NULL)
    {
        if ([m_pZone ActiveTunerAMFM] || [m_pZone ActiveTunerXM])
        {
            CTunerContainerPage *pPage = [[CTunerContainerPage alloc] initWithFrame:rPage Zone:m_pZone MediaZonePage:self];
            m_pPage = pPage;
        }
    }

    if (m_pPage == NULL)
    {
        switch (iSubSourceBasic)
        {
        case AUX_SOURCE_USERDEF:
            m_pPage = [[CCustomMediaPage alloc] initWithFrame:rPage ID:[m_pZone ActiveUserData] ZoneID:m_pZone.m_iID ZonePage:self];
            break;


        case AUX_SOURCE_KEYPAD_NEXT_PREV:
        case AUX_SOURCE_KEYPAD:
            m_pPage = [[CMediaKeypadView alloc] initWithFrame:rPage ID:[m_pZone ActiveUserData] ZoneID:m_pZone.m_iID Flags:0 TextSize:0 Type:CONTROL_KEYPADKEYPAD];
            break;
        }
    }
    


    if (m_pPage != NULL)
    {
        if (m_pToolBar == NULL)
            [self addSubview:m_pPage];
        else
            [self insertSubview:m_pPage belowSubview:m_pToolBar];
            
        [m_pPage release];
    }
    if ([self IsVisible])
        [self SetVisible:TRUE];
}
/*============================================================================*/

-(void)SourceSelectFinished
    
/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [self setFrame:m_rBaseFrame];
        [[m_pMainPage ToolBar] setAlpha:1.0];
    }

    [[m_pToolBarClient SourceButton] SetChecked:NO];
    [[m_pToolBarClient SettingsButton] SetChecked:NO];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [m_pZone StopVolume];
    [super SetVisible:bVisible];
    [CCustomPage SetVisible:m_pPage Visible:bVisible];


    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
//        if (bVisible)
//        {
//            BOOL bLockScroll = FALSE;
//            if (m_pMP3Page != NULL)
//                bLockScroll = [m_pMP3Page PlaylistActive];

//            [self LockHorizontalScroll:bLockScroll];
//        }
    }
}
/*============================================================================*/

-(void)CSubTabViewOverlay

/*============================================================================*/
{
    [self ReleaseModalView];
}
/*============================================================================*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    [m_pZone StopVolume];
}
/*============================================================================*/

-(void)PresentModalView:(CMediaModalView*)pView Animated:(BOOL)bAnimate

/*============================================================================*/
{
    if (m_pModalPage != NULL)
    {
        [m_pModalPage removeFromSuperview];
        m_pModalPage = NULL;
    }

    CGRect rEnd = [self ContentPageRect];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInset = [CHLComm GetInt:INT_STD_INSET];
        [CRect Inset:&rEnd DX:iInset DY:iInset];
    }

    [self addSubview:pView];
    [pView setAlpha:0];
    int iDYTrans = rEnd.size.height / 2;
    CGRect rFrameStart = rEnd;
    rFrameStart.origin.y += iDYTrans;
    [pView setFrame:rFrameStart];
    if (bAnimate)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
    }
    [m_pPage setAlpha:0.0];
    [pView setAlpha:1.0];
    [pView setFrame:rEnd];
    rEnd.origin.y -= iDYTrans;
    [m_pPage setFrame:rEnd];

    if (bAnimate)
    {
        [UIView commitAnimations];
    }

    m_pModalPage = pView;
}
/*============================================================================*/

-(void)ReleaseSelectPage

/*============================================================================*/
{
    [[m_pToolBarClient SourceButton] SetChecked:NO];
    [[m_pToolBarClient SettingsButton] SetChecked:NO];
    [self ReleaseModalView];
}
/*============================================================================*/

-(void)ReleaseModalView

/*============================================================================*/
{
    if (m_pModalPage == NULL)
        return;

    if (_m_pModalPage != NULL)
    {
        [_m_pModalPage removeFromSuperview];
        _m_pModalPage = NULL;
    }

    _m_pModalPage = m_pModalPage;
    m_pModalPage = NULL;

    CGRect rThis = [self ContentPageRect];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInset = [CHLComm GetInt:INT_STD_INSET];
        [CRect Inset:&rThis DX:iInset DY:iInset];
    }

    int iDYTrans = rThis.size.height / 2;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [m_pPage setAlpha:1.0];
    [_m_pModalPage setAlpha:0.0];
    [m_pPage setFrame:rThis];
    rThis.origin.y += iDYTrans;
    [_m_pModalPage setFrame:rThis];

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(ModalRelease:finished:context:)];
    [UIView commitAnimations];
}
/*============================================================================*/

-(CGRect)ContentPageRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iSizeToolBar = [m_pToolBarClient RequiredHeight:rThis.size.width];
    [CRect BreakOffBottom:&rThis DY:iSizeToolBar];
    return rThis;
}
/*============================================================================*/

+(UILabel*)CreateLabel:(CGRect)rLabel View:(UIView*)pView

/*============================================================================*/
{
    UILabel *pText = [[UILabel alloc] initWithFrame:rLabel];
    pText.textAlignment = UITextAlignmentLeft;
    [CCustomPage InitStandardLabel:pText Size:rLabel.size.height * 6 / 10];
    [pView addSubview:pText];
    return pText;
}
/*============================================================================*/

-(void)layoutSubviews    

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rToolBar = CGRectZero;
    m_rBaseFrame = self.frame;

    printf("MediaZonePage layout %f %f\r\n", rThis.size.width, rThis.size.height);

    int iSizeToolBar = [m_pToolBarClient RequiredHeight:rThis.size.width];

    rToolBar = [CRect BreakOffBottom:&rThis DY:iSizeToolBar];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        int iInsets = [CHLComm GetInt:INT_STD_INSET];
        [CRect Inset:&rThis DX:iInsets DY:iInsets];
    }

    if (m_pPage != NULL)
    {
        if (m_pPage.superview == self)
            printf("OK\n");
        else
            printf("ERROR\n");

        CGRect rSub = m_pPage.frame;
        printf("Sub layout %f %f\r\n", rSub.size.width, rSub.size.height);
    }

    [m_pPage setFrame:rThis];
    [m_pPage setNeedsLayout];
    [m_pModalPage setFrame:rThis];
    [m_pToolBar setFrame:rToolBar];
    [m_pToolBarClient setFrame:m_pToolBar.bounds];
    
    CGRect rAllOff = rThis;
    int iDXAllOff = 300;
    int iDYAllOff = 200;
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iDXAllOff = 280;
        iDYAllOff = 140;
    }

    [CRect CenterDownToX:&rAllOff DX:iDXAllOff];
    [CRect CenterDownToY:&rAllOff DY:iDYAllOff];
    [m_pAllZonesOffView setFrame:rAllOff];
    
}
/*============================================================================*/

-(void)ModalRelease:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    [_m_pModalPage removeFromSuperview];
    _m_pModalPage = NULL;
    
    if ([m_pZone ActiveSourceIndex] == 0)
    {
        if (m_pModalPage  == NULL)
        {
            [self SelectSource];
        }
    }
}
/*============================================================================*/

-(void)BeginAllOffWarning

/*============================================================================*/
{
    if (m_pAllZonesOffView != NULL)
        return;
    

    m_pAllZonesOffView = [[CAllZonesOffView alloc] initWithFrame:CGRectZero Page:self ZoneID:m_pZone.m_iID];
    [self addSubview:m_pAllZonesOffView];
    [m_pAllZonesOffView release];
    [m_pAllZonesOffView setAlpha:0];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    [m_pPage setAlpha:0.25];
    [m_pAllZonesOffView setAlpha:1.0];

    [UIView commitAnimations];
}
/*============================================================================*/

-(void)CancelAllOffWarning

/*============================================================================*/
{
    if (m_pAllZonesOffView == NULL)
        return;

    [m_pAllZonesOffView Dismiss];

    if (_m_pAllZonesOffView != NULL)
        [_m_pAllZonesOffView removeFromSuperview];
    _m_pAllZonesOffView = m_pAllZonesOffView;
    m_pAllZonesOffView = NULL;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    [m_pPage setAlpha:1.0];
    [_m_pAllZonesOffView setAlpha:0.0];

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AllZonesOffRelease:finished:context:)];

    [UIView commitAnimations];
}
/*============================================================================*/

-(void)AllZonesOffRelease:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [_m_pAllZonesOffView removeFromSuperview];
    _m_pAllZonesOffView = NULL;
}
@end
