/*
--------------------------------------------------------------------------------

    ASSERTTRACE.CPP : CRYSTALPAD

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlmsg.h"
#include "version.h"
#include "hlserver.h"

#include "asserttrace.h"


/*============================================================================*/

    void                        AssertDlg(
    const char                  *psFile,
    int                         iLine,
    BOOL                        bLogAssert)
/*
    NOTE:  bLogAssert defines whether a message is sent to the server or not.  It is the sole
    difference between ASSERT and LOGASSERT. When TRUE bLogAssert will send a message to the server.
*/

/*============================================================================*/
{
#ifndef IPHONE
    // Get the file name in UNICODE.
    int iNChars = strlen(psFile);
    CHLString sWFile(psFile);
    CHLString sVersion = _T(HL_VERSION);
    sWFile.ConvertTowchar_t();

    // Construct the message text: chuck any path prior to the file name.
    int iIndex = sWFile.ReverseFind('\\');
    if (iIndex >= 0)
        sWFile = sWFile.Right(iNChars - iIndex - 1);

    //  1) create user message, abort = exit program, retry = break into debugger, ignore = continue
    //     This is created as model
    CHLString sText;
    sText.Format(_T("A: %s(line %d)"), (const TCHAR*) sWFile, iLine, (const TCHAR*) sVersion);
    #ifdef DEBUG
        // Message Box
        UINT iUserCommand = IDRETRY;
        HWND hWnd = ::GetDesktopWindow();
        iUserCommand = MessageBox(hWnd,(const TCHAR*)sText,_T("Program Warning")
        ,(MB_ICONEXCLAMATION | MB_ABORTRETRYIGNORE | MB_DEFBUTTON3 | MB_APPLMODAL | MB_SETFOREGROUND));

        //  2)  Output to debug windows.  This does nothing if there is no debugger
        sText.Format(_T("A: %s(line %i)"), (const TCHAR*) sWFile, iLine, (const TCHAR*) sVersion);
        OutputDebugString((const TCHAR*)sText);

        // Handle user input
        if (iUserCommand == IDABORT)
        {
            PostQuitMessage(-1);
            if(g_pMainWindow == NULL)
                exit(-1);
        }
        else if (iUserCommand == IDRETRY)
        {
            DebugBreak();
        }
    #endif

    //  3) Are we telling the server?
    if(bLogAssert)
    {
        sText.Format(_T("%s(line %d: %s)"), (const TCHAR*) sWFile, iLine, (const TCHAR*) sVersion);
        if (g_pHLServer != NULL)
        {
            CHLMSG msg(0, HLM_SYSLOG_ASSERT);
            msg.PutString(sText);
            g_pHLServer->SendMSG(&msg);
        }
    }
#endif
}
/*============================================================================*/

    void                        TraceWrap(
    const TCHAR                 *pString )

/*============================================================================*/
{
    // Should only get here in debug builds, also only in UNICODE.
    printf("%s", pString);

    // 1) Send a message to the gateway, unless there is a debugger present.
    if (g_pHLServer != NULL)
    {
        CHLMSG msg(0, HLM_SYSLOG_TRACE);
        msg.PutString(pString);
        g_pHLServer->SendMSG(&msg);
    }
}

