//
//  PhotoFullFrameView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 8/31/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPhoto;
@class CPhotoView;
@class CPhotoAlbum;
@class CPhotosPage;
@class CHLButton;

/*============================================================================*/

@interface CPhotoFullFrameView : UIView < UIScrollViewDelegate >

/*============================================================================*/
{
    CPhotosPage                 *m_pPhotosPage;
    CHLButton                   *m_pGButton;
    CHLButton                   *m_pNextButton;
    CHLButton                   *m_pPrevButton;
    UIScrollView                *m_pScrollView;
    CPhotoView                  *m_pL;
    CPhotoView                  *m_pC;
    CPhotoView                  *m_pR;

    CGSize                      m_SizeLast;
}

- (id)initWithFrame:(CGRect)frame PhotosPage:(CPhotosPage*)pPhotosPage PhotoView:(CPhotoView*)pView;

-(void)OnIdleVisible;
-(void)UpdateButtons;

@end
