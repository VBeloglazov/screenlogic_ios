//
//  Weather.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/28/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitRoundRegion.h"
#import "HLTableFrame.h"
#import "NSSocketSink.h"
#import "SinkServer.h"
#import "Sink.h"

@class CDateHelper;
@class CDayIconView;

#define ORIENTATION_VT          0
#define ORIENTATION_HZ          1

/*============================================================================*/

@interface CDayForecast : NSObject

/*============================================================================*/
{
    int                         m_iIndex;
    NSString                    *m_psDate;
    int                         m_iHigh;
    int                         m_iLow;
    NSString                    *m_psImageRef;
    NSString                    *m_psText;
}

-(id) initWithMSG:(CNSMSG*)pMSG Index:(int)iIndex Comp:(NSDateComponents*)pComps Calendar:(NSCalendar*)pCal Helper:(CDateHelper*)pHelper;
-(int)Index;
-(NSString*)DayOfWeekText;
-(NSString*)HighText;
-(NSString*)LowText;
-(NSString*)Description;

@end

/*============================================================================*/

@interface CForecast : NSObject < CNSSocketSink >

/*============================================================================*/
{
    CSinkServer                 *m_pSinkServer;

    NSString                    *m_psText;
    int                         m_iTemp;
    int                         m_iHumidity;
    NSString                    *m_psWind;
    int                         m_iBaro;
    int                         m_iDewPoint;
    int                         m_iWindChill;
    int                         m_iVisibility;
    
    NSMutableArray              *m_pDays;
}

-(id)init;

-(int)CurrentTemp;
-(NSString*)Conditions;

-(void)ReadFrom:(CNSMSG*)pMSG;
-(int)NDays;
-(CDayForecast*)Day:(int)iIndex;
-(void)Post;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;

@end

/*============================================================================*/

@interface CWeatherForecastView : CSplitRoundRegion  

/*============================================================================*/
{
}
@end


/*============================================================================*/

@interface CWeatherScrollView : UIView <  CSink >

/*============================================================================*/
{
    UIScrollView                *m_pScrollView;
    CForecast                   *m_pForecast;
    NSMutableArray              *m_pDayViews;
}

@end

/*============================================================================*/

@interface CDayForecastCell : UIView 

/*============================================================================*/
{
    CDayForecast            *m_pDay;
    
    UILabel                 *m_pDayOfWeek;
    UILabel                 *m_pHi;
    UILabel                 *m_pLo;
    UILabel                 *m_pDesc;
    CDayIconView            *m_pIconView;
    int                     m_iOrientation;
}

-(id)initWithFrame:(CGRect)rect Day:(CDayForecast*)pDay;
-(void)SetDay:(CDayForecast*)pDay;
-(void)Orientation:(int)iOrientation;


-(UILabel*)AddText:(NSString*)psText Size:(int)iSize Color:(UIColor*)pColor;

@end

/*============================================================================*/

@interface CDayIconView : UIView < CNSSocketSink >

/*============================================================================*/
{
    void                                *m_pData;
    CGImageRef                          m_pImage;
}

-(id)initWithFrame:(CGRect)rect DayIndex:(int)iIndex Day:(CDayForecast*)pDay;

@end