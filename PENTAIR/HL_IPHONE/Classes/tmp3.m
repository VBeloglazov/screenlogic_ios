//
//  tmp3.m
//  XPAD_VIEW
//

//  Created by Andrew Watzke on 10/1/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "tmp3.h"
#import "tnowplaying.h"
#import "tplaylist.h"
#import "mp3menu.h"
#import "MediaZonePage.h"
#import "hlm.h"
#import "AudioZone.h"
#import "custompage.h"
#import "MainView.h"
#import "crect.h"
#import "hlsegment.h"
#import "AudioService.h"
#import "MP3ContentView.h"
#import "RoundRegion.h"
#import "shader.h"

@implementation CMP3InfoPage

#define BUTTON_SIZE_IPAD            45
#define SPACING_IPAD                5
#define TRANSPORT_INSET_IPAD        32
#define DY_TRANSPORT_IPAD           60

#define BUTTON_SIZE_IPOD            32
#define SPACING_IPOD                3
#define TRANSPORT_INSET_IPOD        8
#define DY_TRANSPORT_IPOD           38

#define PAGE_NOWPLAYING             0
#define PAGE_ARTIST                 1
#define PAGE_ALBUMS                 2
#define PAGE_TRACKS                 3
#define PAGE_PLAYLIST               4
#define PAGE_GENRE                  5
#define PAGE_MYLIBRARY              6
#define PAGE_SERVICE_START          1000
#define PAGE_SERVICE_END            2000

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone ZonePage:(CMediaZonePage*)pZonePage

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame Style:SRREGION_MAIN_WINDOW])
    {
        m_iFlags = SRREGION_FLAG_AUTOLAYOUT;


        for (int i = 0; i < 32; i++)
        {
            m_pPages[i] = NULL;
        }

        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        m_pZone = pZone;
    
        NSMutableArray *pItems = [[[NSMutableArray alloc] init] autorelease];
        [self AddItem:pItems Text:@"NOW PLAYING" Icon:@"NOWPLAYING.png" Data:PAGE_NOWPLAYING];


        if ([m_pZone NServices] > 0)
        {
            [self AddItem:pItems Text:@"MY LIBRARY" Icon:@"TRACK.png" Data:PAGE_MYLIBRARY];
        }
        else
        {
            if ([m_pZone HasSort:SORT_ARTIST])      [self AddItem:pItems Text:@"ARTISTS"    Icon:@"ARTIST.png" Data:PAGE_ARTIST];
            if ([m_pZone HasSort:SORT_ALBUM])       [self AddItem:pItems Text:@"ALBUMS"     Icon:@"ALBUM.png" Data:PAGE_ALBUMS];
            if ([m_pZone HasSort:SORT_TRACK])       [self AddItem:pItems Text:@"TRACKS"     Icon:@"TRACK.png" Data:PAGE_TRACKS];
            if ([m_pZone HasSort:SORT_PLAYLIST])    [self AddItem:pItems Text:@"PLAYLISTS"  Icon:@"PLAYLIST.png" Data:PAGE_PLAYLIST];
            if ([m_pZone HasSort:SORT_GENRE])       [self AddItem:pItems Text:@"GENRES"     Icon:@"GENRE.png" Data:PAGE_GENRE];
        }

        for (int i = 0; i < [m_pZone NServices]; i++)
        {
            CAudioService *pService = [m_pZone Service:i];
            NSString *pName = [pService Name];
            pName = [pName uppercaseString];
            NSString *pIcon = [pService Icon];
            [self AddItem:pItems Text:pName Icon:pIcon Data:PAGE_SERVICE_START+i];
        }


        [self InitPageWithViews:pItems];
        [self SetFlags:RREGION_FLAG_BASELEVEL];
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    for (int i = 0; i < 32; i++)
    {
        UIView *pView = m_pPages[i];
        [pView release];
    }

    [m_pZone RemoveSink:self];

    [super dealloc];
}
/*============================================================================*/

-(void)AddItem:(NSMutableArray*)pList Text:(NSString*)pText Icon:(NSString*)pIcon Data:(int)iData

/*============================================================================*/
{
    CHLSegmentIconView *pObj = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:pIcon Text:pText];
    [pList addObject:pObj];
    [pObj release];
    m_iPageMap[m_iNPageMap] = iData;
    m_iNPageMap++;
}
/*============================================================================*/

-(UIView*)CreatePageForIndex:(int)iIndex

/*============================================================================*/
{
    if (m_pPages[iIndex] != NULL)
    {
        return m_pPages[iIndex];
    }

    UIView *pView = NULL;

    int iType = m_iPageMap[iIndex];
    switch (iType)
    {
    case PAGE_NOWPLAYING:   pView = [[CNowPlayingPage alloc] initWithFrame:CGRectZero Zone:m_pZone];    break;
    case PAGE_ARTIST:       pView = [[CArtistContentView alloc] initWithFrame:CGRectZero Zone:m_pZone]; break;
    case PAGE_ALBUMS:       pView = [[CAlbumContentView alloc] initWithFrame:CGRectZero Zone:m_pZone]; break;
    case PAGE_TRACKS:       pView = [[CTrackContentView alloc] initWithFrame:CGRectZero Zone:m_pZone]; break;
    case PAGE_PLAYLIST:     pView = [[CPlaylistContentView alloc] initWithFrame:CGRectZero Zone:m_pZone]; break;
    case PAGE_GENRE:        pView = [[CGenreContentView alloc] initWithFrame:CGRectZero Zone:m_pZone]; break;
    case PAGE_MYLIBRARY:    pView = [[CMyLibraryContentView alloc] initWithFrame:CGRectZero Zone:m_pZone]; break;
    }
    
    if (pView == NULL)
    {
        // Service?
        if (iType >= PAGE_SERVICE_START && iType <= PAGE_SERVICE_END)
        {
            int iIndex = iType - PAGE_SERVICE_START;
            CAudioService *pService = [m_pZone Service:iIndex];
            pView = [[CAudioServiceContentView alloc] initWithFrame:CGRectZero Zone:m_pZone Service:pService];
        }
    }

    if (iType == PAGE_NOWPLAYING)
        [m_pRgn2 ContentMode:CONTENT_MODE_FULLWIDTH];
    else
        [m_pRgn2 ContentMode:CONTENT_MODE_FULLHEIGHT_FULLWIDTH];

    m_pPages[iIndex] = pView;
    [pView retain];

    return pView;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    printf("tmp %f %f\r\n", self.bounds.size.width, self.bounds.size.height);

    [super layoutSubviews];
}
/*============================================================================*/

+(int)BUTTON_SIZE       {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return BUTTON_SIZE_IPAD; return BUTTON_SIZE_IPOD; }
+(int)TRANSPORT_INSET   {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return TRANSPORT_INSET_IPAD; return TRANSPORT_INSET_IPOD; }
+(int)SPACING           {   if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) return SPACING_IPAD; return SPACING_IPOD; }

/*============================================================================*/

@end
