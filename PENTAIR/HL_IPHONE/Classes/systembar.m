//
//  systembar.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/20/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "systembar.h"
#import "MainTab.h"
#import "crect.h"
#import "CustomPage.h"
#import "HLToolBar.h"
#import "hlcomm.h"
#import "hlm.h"
#import "IPAD_ViewController.h"
#import "AppDelegate.h"

#define TEXT_SIZE                   20

@implementation CSystemBarButton;
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame IconName:(NSString*)psIcon Framed:(BOOL)bFramed ImageHeight:(int)iImageHeight

/*============================================================================*/
{
    if ((self = [super initWithFrame:rFrame])) 
    {
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        UIImage *pImage = [CCustomPage ScaleImage:[UIImage imageNamed:psIcon] Height:iImageHeight];
        CGSize size = pImage.size;
        m_iImageDX = size.width;
        m_iImageDY = size.height;

        m_pImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [m_pImageView setImage:pImage];
        [self addSubview:m_pImageView];
        [self addTarget:self action:@selector(ButtonPress:) forControlEvents:UIControlEventTouchDown];
        [m_pImageView release];

        m_bFramed = bFramed;
    }
    return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rIcon = CGRectMake(rThis.size.width/2-m_iImageDX/2, rThis.size.height/2-m_iImageDY/2, m_iImageDX, m_iImageDY);

    #ifdef PENTAIR
        [CRect CenterDownToX:&rIcon DX:60];
        [CRect CenterDownToY:&rIcon DY:60];
    #endif

    [m_pImageView setFrame:rIcon];
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    if (!m_bFramed)
        return;

    printf("SYSBAR DRARECT1\n");

    CGContextRef context = UIGraphicsGetCurrentContext();

    UIColor *pLo = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:.25];
    [pLo set];

    int iX = rect.origin.x+rect.size.width;
    int iY0 = rect.origin.y;
    int iY1 = rect.origin.y + rect.size.height;

    CGContextBeginPath(context);
    CGContextMoveToPoint(context, iX-3, iY0);
    CGContextAddLineToPoint(context, iX-3, iY1);
    CGContextStrokePath(context); 

    UIColor *pMid = [CHLComm GetRGB:RGB_APP_BACKGROUND];
    [pMid set];

    CGContextBeginPath(context);
    CGContextMoveToPoint(context, iX-2, iY0);
    CGContextAddLineToPoint(context, iX-2, iY1);
    CGContextStrokePath(context); 

    UIColor *pHi = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:.25];
    [pHi set];


    CGContextBeginPath(context);
    CGContextMoveToPoint(context, iX-1, iY0);
    CGContextAddLineToPoint(context, iX-1, iY1);
    CGContextStrokePath(context); 

    printf("SYSBAR DRARECT2\n");
}
/*============================================================================*/

-(void)ButtonPress:(id)sender

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    [pDelegate PlaySoundButtonPress];
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    [self sendActionsForControlEvents:UIControlEventTouchDown];
}
@end

@implementation CMainTabView;
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MainTab:(CMainTab*)pMainTab

/*============================================================================*/
{
    if ((self = [super initWithFrame:rFrame])) 
    {
        m_pMainTab = pMainTab;
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        UIImage *pImage = [UIImage imageNamed:[pMainTab ImageName]];

        int iTextSize = TEXT_SIZE;
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            iTextSize = [CMainView TEXT_SIZE_SIDEBAR];
            pImage = [CCustomPage ScaleImage:pImage Height:52];
        }

        m_pImageView = [[UIImageView alloc] initWithImage:pImage];
        [self addSubview:m_pImageView];
        [m_pImageView release];

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        NSString *pString = [m_pMainTab.m_pName uppercaseString];
        [m_pLabel setText:pString];
        [self addSubview:m_pLabel];
        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];
        [m_pLabel release];
        [self setUserInteractionEnabled:NO];
    }
    return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDYLabel = 0;
    @try
    {
        iDYLabel = m_pLabel.font.lineHeight * 150 / 100;
    }
    @catch (NSException *ex)
    {
        iDYLabel = m_pLabel.font.pointSize * 175 / 100;
    }
    CGRect rText = [CRect BreakOffBottom:&rThis DY:iDYLabel];
 
    [CRect ConfineToAspect:&rThis DXDY:1.0];
    //DL
    rText.origin.y += 20;
    [m_pLabel setFrame:rText];
    //DL - this controls icons and test top margins on main (very first) tab slider
    rThis.origin.y += 20;
    [m_pImageView setFrame:rThis];
}
/*============================================================================*/

-(CMainTab*)MainTab

/*============================================================================*/
{
    return m_pMainTab;
}
@end

@implementation CSystemToolBarClient;

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MainTabs:(NSMutableArray*)pMainTabs ViewController:(IPAD_ViewController*)pController SelectedIndex:(int)iIndex;

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pMenuBar = [[CSystemToolBar alloc] initWithFrame:self.bounds MainTabs:pMainTabs ViewController:pController SelectedIndex:iIndex];
        [self addSubview:m_pMenuBar];
        [m_pMenuBar release];
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pMenuBar setFrame:self.bounds];
}
/*============================================================================*/

-(BOOL)IsSystemMenu

/*============================================================================*/
{
    return TRUE;
}

@end

@implementation CSystemToolBar;

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MainTabs:(NSMutableArray*)pMainTabs ViewController:(IPAD_ViewController*)pController SelectedIndex:(int)iIndex;

/*============================================================================*/
{
    if ((self = [super initWithFrame:rFrame])) 
    {
        m_pViews = [[NSMutableArray alloc]init];

        m_iSelectedIndex = iIndex;
        m_pViewController = pController;
        
        // Initialization code
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        int iN = [pMainTabs count];
        int iDX = rFrame.size.width / iN;

        CGRect rHL = CGRectMake(iDX*iIndex, 0, iDX, rFrame.size.height);
        m_pHL = [[CHLSegmentHighlight alloc] InitWithFrame:rHL Color1:RGB_PAGETAB_SELECT_TOP_LS Color2:RGB_PAGETAB_SELECT_BTM_LS];
        [self addSubview:m_pHL];
        [m_pHL release];

        for (int i = 0; i < iN; i++)
        {
            CMainTab *pMain = (CMainTab*)[pMainTabs objectAtIndex:i];
            CGRect rTab = CGRectMake(i * iDX, 0, iDX, rFrame.size.height);
            CMainTabView *pView = [[CMainTabView alloc] initWithFrame:rTab MainTab:pMain];
            [self addSubview:pView];
            [m_pViews addObject:pView];
            [pView release];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pViews release];
    [super dealloc];
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    int iN = [m_pViews count];
    int iDX = self.frame.size.width / iN;
    for (int i = 0; i < iN; i++)
    {
        CMainTabView *pView = (CMainTabView*)[m_pViews objectAtIndex:i];
        int iX = iDX * i;
        CGRect rFrame = CGRectMake(iX, 0, iDX, self.frame.size.height);
        [pView setFrame:rFrame];
    }

    CGRect rHL = CGRectMake(iDX*m_iSelectedIndex, 0, iDX, self.bounds.size.height);
    //DL - this controls top margin for the very first tab slider - iPad
    rHL.origin.y += 20;
    [CRect Inset:&rHL DX:1 DY:1];
    [m_pHL setFrame:rHL];
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    int iNItems = [m_pViews count];
    NSArray *pTouches = [touches allObjects];
    for (int i = 0; i < [pTouches count]; i++)
    {
        UITouch *pTouch = [pTouches objectAtIndex:i];
        if (pTouch.view == self)
        {
            int iX = [pTouch locationInView:self].x;
            int iDXSeg = self.bounds.size.width / iNItems;
            int iSeg = iX / iDXSeg;
            if (iSeg != m_iSelectedIndex)
            {
                CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
                [pDelegate PlaySoundButtonPress];
                [self SetSelectedIndex:iSeg];
                [self sendActionsForControlEvents:UIControlEventValueChanged];
                CMainTabView *pView = [m_pViews objectAtIndex:iSeg];
                CMainTab *pMainTab = [pView MainTab];
                [pMainTab LoadConfig];
                [m_pViewController SetActiveMainTab:pMainTab CursorDirection:m_iCursorDirection];
            }
        }
    }
}
/*============================================================================*/

-(void)SetSelectedIndex:(int)iIndex

/*============================================================================*/
{
    if (m_iSelectedIndex == iIndex)
        return;

    if (iIndex > m_iSelectedIndex)
        m_iCursorDirection = CURSOR_LEFTTORIGHT;
    else 
        m_iCursorDirection = CURSOR_RIGHTTOLEFT;


    m_iSelectedIndex = iIndex;
    
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    int iNItems = [m_pViews count];
    int iDXSeg = self.bounds.size.width / iNItems;
    CGRect rFrame = m_pHL.frame;
    rFrame.origin.x = iDXSeg * iIndex;
    [CRect Inset:&rFrame DX:1 DY:1];
    [m_pHL setFrame:rFrame];

	[UIView commitAnimations];
}
/*============================================================================*/

-(int)SelectedIndex

/*============================================================================*/
{
    return m_iSelectedIndex;
}
/*============================================================================*/

-(int)CursorDirection

/*============================================================================*/
{
    return m_iCursorDirection;
}

@end
