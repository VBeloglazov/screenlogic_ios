//
//  PoolTempPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PoolTempPage.h"
#import "PoolCircuitButton.h"
#import "PoolSubSystemPage.h"
#import "hlm.h"
#import "NSPoolConfig.h"
#import "NSPoolCircuit.h"
#import "PoolCircuitButton.h"
#import "PoolTempCtrlView.h"
#import "crect.h"
#import "hlradio.h"
#import "hlsegment.h"
#import "ButtonBar.h"

#define DY_CIRCUIT_BUTTON           40
#define DY_MODES_IPAD               54
#define DY_MODES_IPOD               40

@implementation CPoolTempPage
/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame Type:(int)iType Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        // Initialization code
        m_pConfig = pConfig;
        [m_pConfig AddSink:self];
        m_iType = iType;
//       self.autoresizingMask = 0xFFFFFFFF;

        NSMutableArray *pCircuits = [[[NSMutableArray alloc] init] autorelease];
        int iIType = 0;
        int iBodyType = 0;
        switch (iType)
        {
        case PAGE_POOL:     {   iIType = POOLINT_POOL;      iBodyType = BODYTYPE_POOL;  } break;
        case PAGE_SPA:      {   iIType = POOLINT_SPA;       iBodyType = BODYTYPE_SPA;   } break;
        case PAGE_CIRCUITS: {   iIType = POOLINT_GENERAL;   iBodyType = -1;             } break;
        }

        [m_pConfig LoadCircuitsByInterface:pCircuits Interface:iIType];

        if ([pCircuits count] > 0)
        {
            m_pCircuitButtons = [[NSMutableArray alloc] init];

            for (int iC = 0; iC < [pCircuits count]; iC++)
            {
                CNSPoolCircuit *pCircuit = (CNSPoolCircuit*)[pCircuits objectAtIndex:iC];
                CPoolCircuitButton *pBtn = [[CPoolCircuitButton alloc] initWithFrame:CGRectZero Circuit:pCircuit Config:m_pConfig];
                [self addSubview:pBtn];
                [m_pCircuitButtons addObject:pBtn];
                [pBtn release];
            }
        }

        if (iBodyType != -1)
        {
            m_pTempCtrl = [[CPoolTempCtrlView alloc] initWithFrame:CGRectZero Type:iBodyType Config:m_pConfig];
            [self addSubview:m_pTempCtrl];
            [m_pTempCtrl release];
            [self InitModeControl];
            [self Notify:0 Data:0];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pConfig RemoveSink:self];
    [m_pCircuitButtons release];

    [super dealloc];
    
    //DL - looks like it is not necessary since it is NIL here (before super dealloc)
    //#ifdef PENTAIR
    //[m_pTempText release];
    //#endif
}
/*============================================================================*/

-(void)InitModeControl

/*============================================================================*/
{
    if (m_pMode != NULL)
    {
        [m_pMode removeFromSuperview];
        [m_pMode release];
        m_pMode = NULL;
    }

//    NSMutableArray *pModes = [[NSMutableArray alloc] init];
    m_pMode = [[CButtonBar alloc] initWithFrame:CGRectZero ];
    
#ifdef PENTAIR
    [m_pMode SetCheckedColor:[UIColor redColor]];
#endif
    
    [self addSubview:m_pMode];
    [m_pMode release];

    BOOL bSolar = [m_pConfig EquipPresent:POOL_SOLARPRESENT|POOL_SOLARHEATPUMP];

    m_pModeOff = [m_pMode AddButtonText:@"Off" Icon:NULL];
    m_pModeHeat = [m_pMode AddButtonText:[m_pConfig HeatModeText:POOLHEAT_HEAT] Icon:NULL];

    if (bSolar)
    {
        m_pModeSolarPref = [m_pMode AddButtonText:[m_pConfig HeatModeText:POOLHEAT_SOLARPREF] Icon:NULL];
        m_pModeSolar = [m_pMode AddButtonText:[m_pConfig HeatModeText:POOLHEAT_SOLAR] Icon:NULL];
    }


    [m_pModeOff         addTarget:self action:@selector(ChangeMode:) forControlEvents:UIControlEventTouchDown];
    [m_pModeSolar       addTarget:self action:@selector(ChangeMode:) forControlEvents:UIControlEventTouchDown];
    [m_pModeSolarPref   addTarget:self action:@selector(ChangeMode:) forControlEvents:UIControlEventTouchDown];
    [m_pModeHeat        addTarget:self action:@selector(ChangeMode:) forControlEvents:UIControlEventTouchDown];
}
/*============================================================================*/

-(IBAction)ChangeMode:(id)sender

/*============================================================================*/
{
    int iHeatMode = 0;
    if (sender == m_pModeOff)
        iHeatMode = POOLHEAT_OFF;
    else if (sender == m_pModeHeat)
        iHeatMode = POOLHEAT_HEAT;
    else if (sender == m_pModeSolar)
        iHeatMode = POOLHEAT_SOLAR;
    else if (sender == m_pModeSolarPref)
        iHeatMode = POOLHEAT_SOLARPREF;
        
    [m_pConfig SetHeatMode:m_iType Mode:iHeatMode];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    #ifdef PENTAIR
    int iTemp = [m_pConfig AirTemp];
    NSString *pText = [NSString stringWithFormat:@"Air Temperature %d°", iTemp];
    [m_pTempText setText:pText];
    #endif

    int iHeatMode = [m_pConfig HeatMode:m_iType];
    [m_pModeOff         SetChecked:(iHeatMode == POOLHEAT_OFF)];
    [m_pModeHeat        SetChecked:(iHeatMode == POOLHEAT_HEAT)];
    [m_pModeSolar       SetChecked:(iHeatMode == POOLHEAT_SOLAR)];
    [m_pModeSolarPref   SetChecked:(iHeatMode == POOLHEAT_SOLARPREF)];
    
    int iHeatState = [m_pConfig HeatStatus:m_iType];

    BOOL bHeater = FALSE;
    BOOL bSolar  = FALSE;

    switch (iHeatState)
    {
    case HEAT_SOLAR_ACTIVE: { bSolar = TRUE;                    } break;
    case HEAT_HEATER_ACTIVE:{ bHeater = TRUE;                   } break;
    case HEAT_BOTH_ACTIVE:  { bSolar = TRUE; bHeater = TRUE;    } break;
    }

    [m_pModeHeat SetFlashing:bHeater];
    [m_pModeSolar SetFlashing:bSolar];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    int iNCircuits = [m_pCircuitButtons count];
    int iDYModes = DY_MODES_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iDYModes = DY_MODES_IPAD;
    }
    else
    {
        [CRect Inset:&rThis DX:0 DY:4];
    }
    

    CGRect rCircuits = [CRect BreakOffTop:&rThis DY:iDYModes];
    CGRect rModes    = [CRect BreakOffBottom:&rThis DY:iDYModes];
        
    [CRect CenterDownToX:&rModes DX:[m_pMode NItems] * 125];

    [CRect BreakOffTop:&rThis DY:10];

    [m_pTempCtrl setFrame:rThis];
    [m_pMode setFrame:rModes];

    [CRect CenterDownToX:&rCircuits DX:iNCircuits * 125];

    for (int iC = 0; iC < iNCircuits; iC++)
    {
        CPoolCircuitButton *pBtn = (CPoolCircuitButton*)[m_pCircuitButtons objectAtIndex:iC];
        CGRect rBtn = [CRect CreateXSection:rCircuits Index:iC Of:iNCircuits];
        [pBtn setFrame:rBtn];
    }

    

}
@end
