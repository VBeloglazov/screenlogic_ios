//
//  MediaKeypadView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "MediaKeypadView.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "hlcomm.h"
#import "ControlDef.h"
#import "MainView.h"

@implementation CMediaKeypadView

/*============================================================================*/

-(id)initWithFrame: (CGRect)rView ID:(int)iID ZoneID:(int)iZoneID Flags:(int)iFlags TextSize:(int)iTextSize Type:(int)iType

/*============================================================================*/
{
    self = [super initWithFrame:rView];
    if (self)
    {
        self.autoresizingMask = 0xFFFFFFFF;
        m_iZoneID = iZoneID;
        m_iID = iID;
        m_iType = iType;

        m_iTextSize = [CMainView DEFAULT_TEXT_SIZE];
        if ((iFlags & CONTROL_FLAG_DEFAULT_TEXT_SIZE) != 0)
            m_iTextSize = iTextSize;

        [CHLComm AddSocketSink:self];

        [self InitComm];

    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_iType == CONTROL_ZONEKEYPAD)
    {
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_REMOVEZONECLIENTQ] autorelease];
        [pQ PutInt:m_iID];
        [pQ PutInt:[CHLComm SenderID:self]];
        [pQ SendMessageWithMyID:self];
    }
    else
    {
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_REMOVEKEYPADSINKQ] autorelease];
        [pQ PutInt:m_iID];
        [pQ PutInt:[CHLComm SenderID:self]];
        [pQ SendMessageWithMyID:self];
    }

    [CHLComm RemoveSocketSink:self];

    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        [self InitComm];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    BOOL bProc = FALSE;
    if (m_iType == CONTROL_ZONEKEYPAD && [pMSG MessageID] == HLM_AUDIO_GETZONEKEYPADA)
        bProc = TRUE;
    if (m_iType == CONTROL_KEYPADKEYPAD && [pMSG MessageID] == HLM_AUDIO_GETKEYPADSTATUSA)
        bProc = TRUE;

    if (!bProc)
        return;

    if (m_iType == CONTROL_ZONEKEYPAD)
    {
        int iID = [pMSG GetInt];
        if (iID != m_iID)
        {
            [pMSG ResetRead];
            return;
        }
    }
    else
    {
        int iType = [pMSG GetInt];
        if (iType != AUDIO_STATUS_KEYPADINFO)
        {
            [pMSG ResetRead];
            return;
        }
    }

    for (int i = 0; i < [m_pControls count]; i++)
    {
        CControlDef *pDef = (CControlDef*)[m_pControls objectAtIndex:i];
        UIView *pView = [pDef GetView];
        [pView removeFromSuperview];
    }
    [m_pControls removeAllObjects];

    [self LoadControls:pMSG ZoneID:m_iZoneID Flags:CONTROLDEF_FLAG_SCALETEXT];
}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    if (m_iType == CONTROL_ZONEKEYPAD)
    {
        CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_GETZONEKEYPADQ] autorelease];
        [pQ1 PutInt:m_iZoneID];
        [pQ1 SendMessageWithMyID:self];

        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDZONECLIENTQ] autorelease];
        [pQ2 PutInt:m_iZoneID];
        [pQ2 PutInt:[CHLComm SenderID:self]];
        [pQ2 PutInt:1];
        [pQ2 SendMessageWithMyID:self];

        m_iID = m_iZoneID;
    }   
    else
    {
        CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDKEYPADSINKQ] autorelease];
        [pQ1 PutInt:m_iID];
        [pQ1 PutInt:[CHLComm SenderID:self]];
        [pQ1 SendMessageWithMyID:self];
    
        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_GETKEYPADSTATUSQ] autorelease];
        [pQ2 PutInt:m_iID];
        [pQ2 SendMessageWithMyID:self];
    }
}
@end
