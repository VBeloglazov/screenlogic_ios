//
//  PoolSpaPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "PoolSpaPage.h"
#import "PoolTempPage.h"
#import "PoolSubSystemPage.h"
#import "NSPoolConfig.h"
#import "hlm.h"
#import "crect.h"
#import "RoundRegion.h"

@implementation CPoolSpaPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    if ((self = [super initWithFrame:rFrame])) 
    {
        // Initialization code
        self.opaque = NO;
        self.autoresizingMask = 0xFFFFFFFF;
        m_pConfig = pConfig;
        NSMutableArray *pPoolCircuits = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray *pSpaCircuits = [[[NSMutableArray alloc] init] autorelease];
        [m_pConfig LoadCircuitsByInterface:pPoolCircuits Interface:POOLINT_POOL];
        [m_pConfig LoadCircuitsByInterface:pSpaCircuits Interface:POOLINT_SPA];

        if ([pPoolCircuits count]>0)
        {
            m_pPage1 = [[CPoolTempPage alloc] initWithFrame:CGRectZero Type:PAGE_POOL Config:m_pConfig];

            if ([pSpaCircuits count] > 0)
            {
                CRoundRegion *pRgn = [[CRoundRegion alloc] initWithFrame:CGRectZero];
                [pRgn SetClientView:m_pPage1];
                [self addSubview:pRgn];
                [pRgn release];
                m_pPage1 = pRgn;
            }
            else 
            {
                [self addSubview:m_pPage1];
                [m_pPage1 release];
            }
        }

        if ([pSpaCircuits count]>0)
        {
            m_pPage2 = [[CPoolTempPage alloc] initWithFrame:CGRectZero Type:PAGE_SPA Config:m_pConfig];

            if (m_pPage1 == NULL)
            {
                [self addSubview:m_pPage2];
                [m_pPage2 release];
                m_pPage1 = m_pPage2;
                m_pPage2 = NULL;
            }
            else 
            {
                CRoundRegion *pRgn = [[CRoundRegion alloc] initWithFrame:CGRectZero];
                [pRgn SetClientView:m_pPage2];
                [self addSubview:pRgn];
                [pRgn release];
                m_pPage2 = pRgn;
            }



        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    if (m_pPage2 == NULL)
    {
        [m_pPage1 setFrame:rBounds];
        return;
    }

    CGRect r1 = rBounds;
    CGRect r2 = rBounds;
    
    if (rBounds.size.width > rBounds.size.height)
    {
        r1 = [CRect BreakOffLeft:&r2 DX:r2.size.width/2];
    }
    else 
    {
        r1 = [CRect BreakOffTop:&r2 DY:r2.size.height/2];
    }

    [CRect Inset:&r1 DX:2 DY:2];
    [CRect Inset:&r2 DX:2 DY:2];

    [m_pPage1 setFrame:r1];
    [m_pPage2 setFrame:r2];
}


@end
