//
//  AudioSource.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "AudioSource.h"
#import "NSQuery.h"

@implementation CAudioSource

@synthesize m_pSourceName;
@synthesize m_pIconName;
@synthesize m_iSourceID;
@synthesize m_iRemoteID;

/*============================================================================*/

-(id)initFromQ:(CNSQuery*)pQ

/*============================================================================*/
{
    if (self == [super init])
    {
        m_iSourceID =   [pQ GetInt];
        m_iRemoteID =   [pQ GetInt];
        m_pSourceName   = [[pQ GetString] retain];
        m_pIconName     = [[pQ GetStringNoFilter] retain];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pSourceName release];
    [m_pIconName release];
    [super dealloc];
}
/*============================================================================*/

-(NSString*)Name

/*============================================================================*/
{
    return m_pSourceName;
}
@end
