/*
--------------------------------------------------------------------------------

    HTTPHELP.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifndef HTTPHELP_H
#define HTTPHELP_H

#include "hlapi.h"

#define HTML_FONT               "Arial"
#define HTML_TABLEWIDTH         500
#define HTML_SPLIT              50

class CHTMLPage;
class CHLFile;

/*============================================================================*/

    HL_API CHTMLPage            *DoHTTPExchange(
    CHLString                   sAddress,
    CHLString                   sSendText,
    char                        *pEndTag    = NULL,
    int                         iNTag       = 0,
    WORD                        nPort       = 80 );

    HL_API CHTMLPage            *DoHTTPExchange(
    SOCKADDR_IN                 *pAddress,
    CHLString                   sSendText,
    char                        *pEndTag    = NULL,
    int                         iNTag       = 0  );

    HL_API CHTMLPage            *DoHTTPExchangeSocket(
    SOCKET                      hSocket, 
    CHLString                   sSendText,
    char                        *pEndTag    = NULL,
    int                         iNTag       = 0  );

    HL_API CHTMLPage            *DoHTTPExchange(
    CHLString                   sAddress,
    CHLString                   sSendText,
    BOOL                        *pbAbort,
    WORD                        nPort       = 80 );

    HL_API CHLString            FormatHTTPString(
    CHLString                   sIn );

    HL_API void                 UnformatHTTPString(
    CHLString                   *pString );

    HL_API int                  UnformatHTTPString(
    char                        *pszString );

    CHLString                   RemoveFormatting(
    CHLString                   sIn,
    BOOL                        bInsertSpaces = FALSE );

    CHLString                   RemoveFormattingSpace(
    CHLString                   sIn );

    HL_API CHLString            FindParam(
    CHLString                   sIn,
    CHLString                   sParam );

    HL_API CHLString            FindStringBetweenKeyRight(
    CHLString                   sIn,
    CHLString                   sLeft,
    CHLString                   sRight ); 

    HL_API CHLString            FindStringBetweenKeyLeft(
    CHLString                   sIn,
    CHLString                   sLeft,
    CHLString                   sRight );

    HL_API CHLString            EverythingAfter(
    CHLString                   sIn,
    CHLString                   sKey ); 

    HL_API CHLString            EverythingBefore(
    CHLString                   sIn,
    CHLString                   sKey ); 

    CHLString                   TrimLeftToDigit(
    CHLString                   sIn );

    HL_API BOOL                 HTTPFindEOF(
    BYTE                        *pBuffer,
    int                         iSizeTotal,
    int                         iSearchSize,
    int                         *piLastChar,
    BYTE                        *pSearchStr,
    int                         iSearchStrSize );

    BOOL                        FileWriteString(
    CHLFile                     *pFile,
    CHLString                   sOut );

    BOOL                        FileWriteLine(
    CHLFile                     *pFile,
    CHLString                   sOut );

    BOOL                        WriteFormattedLine(
    CHLFile                     *pFile,
    CHLString                   sLine,
    CHLString                   sFont,
    CHLString                   sFontSize,
    CHLString                   sFontColor,
    BOOL                        bUnderLine  = FALSE,
    BOOL                        bBold = FALSE );

    BOOL                        WriteHeader(CHLFile *pFile);

    BOOL                        BeginStyle(CHLFile *pFile);
    BOOL                        EndStyle(CHLFile *pFile);

    BOOL                        AddStyle(
    CHLFile                     *pFile,
    CHLString                   sStyleName,
    CHLString                   sFont,
    CHLString                   sFontSize,
    CHLString                   sColor,
    CHLString                   sWeight );

    BOOL                        BeginTable(
    CHLFile                     *pFile,
    int                         iWidth );

    BOOL                        WriteTableHeader(
    CHLFile                     *pFile,
    CHLString                   sClass,
    CHLString                   sHeader,
    int                         iColsWidth );

    BOOL                        EndTable(CHLFile *pFile);

    BOOL                        BeginTableRow(CHLFile *pFile);
    BOOL                        EndTableRow(CHLFile *pFile);

    BOOL                        WriteTableData(
    CHLFile                     *pFile,
    CHLString                   sClass,
    CHLString                   sData,
    int                         iWidth = 0 );

    BOOL                        WriteTableRow2(
    CHLFile                     *pFile,
    CHLString                   sClass1,
    CHLString                   sData1,
    CHLString                   sClass2,
    CHLString                   sData2,
    int                         iWidthCol1 = 0);

    BOOL                        WriteTableRow3(
    CHLFile                     *pFile,
    CHLString                   sClass1,
    CHLString                   sData1,
    CHLString                   sClass2,
    CHLString                   sData2,
    CHLString                   sClass3,
    CHLString                   sData3,
    int                         iWidthCol1 = 0,
    int                         iWidthCol2 = 0);

    BOOL                        WriteTableRow4(
    CHLFile                     *pFile,
    CHLString                   sClass1,
    CHLString                   sData1,
    CHLString                   sClass2,
    CHLString                   sData2,
    CHLString                   sClass3,
    CHLString                   sData3,
    CHLString                   sClass4,
    CHLString                   sData4);

    HL_API BOOL                 ResolveAddress(
    CHLString                   sAddress,
    SOCKADDR_IN                 *pAddress,
    WORD                        nSpecPort = 80 );

/*============================================================================*/

#endif

