//
//  SecPartition.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecPartition.h"
#import "nsquery.h"
#import "ArmingMode.h"
#import "SecZone.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "SinkServer.h"
#import "hlcomm.h"
#import "SubTabCell.h"

@implementation CSecPartition


/*============================================================================*/

-(id)initFromQ:(CNSQuery*)pQ

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pSinkServer = [[CSinkServer alloc] init];

        NSString *pName = [pQ GetString];
        int iID         = [pQ GetInt];
        int iData       = [pQ GetInt];
        [super initWithName:pName ID:iID Data1:iData Data2:0];

        m_bHasPanic     = [pQ GetInt];
        m_bArmAllowed   = [pQ GetInt];
        m_bGetBypass    = [pQ GetInt];

        m_pZones        = [[NSMutableArray alloc] init];
        m_pArmingModes  = [[NSMutableArray alloc] init];
        m_pFaults       = [[NSMutableArray alloc] init];

        int iNModes = [pQ GetInt];
        for (int iMode = 0; iMode < iNModes; iMode++)
        {
            CArmingMode *pMode = [[CArmingMode alloc] initFromQ:pQ];
            [m_pArmingModes addObject:pMode];
            [pMode release];
        }

        int iNZones = [pQ GetInt];
        for (int iZone = 0; iZone < iNZones; iZone++)
        {
            CSecZone *pZone = [[CSecZone alloc] initFromQ:pQ];
            [m_pZones addObject:pZone];
            [pZone release];
        }

    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [self ReleaseComm];
    [m_pSinkServer release];
    [m_pArmingModes release];
    [m_pZones release];
    [m_pFaults release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (!bNowConnected)
    {
        [self ReleaseComm];
    }
    else 
    {
        if ([m_pSinkServer NSinks] > 0)
            [self InitComm];
    }

}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_SECURITY_STATUSCHANGED:
        [self PostGetStatus];
        break;

    case HLM_SECURITY_GETALLPARTSTATBYIDA:
        {
            // Faults
            int iNFaults = [pMSG GetInt];
            int i = 0;
            [m_pFaults removeAllObjects];
            for (i = 0; i < iNFaults; i++)
            {
                NSString *psFault = [pMSG GetString];
                [m_pFaults addObject:psFault];
            }

            int iZoneStates = [pMSG GetInt];
            for (i = 0; i < iZoneStates; i++)
            {
                int iID = [pMSG GetInt];
                int iState = [pMSG GetInt];
                [self SetZoneState:iID State:iState];
            }
            
            m_iPartStatus = [pMSG GetInt];
            [m_pSinkServer NotifySinks:m_iID Data:0];

            CSubTabCell *pSubTabCell = [self GetSubTabCell];
            if (pSubTabCell != NULL)
                [self UpdateSubTabCell:pSubTabCell];
        }
        break;
    }
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
    [self InitComm];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
    if ([m_pSinkServer NSinks] == 0 && m_pSubTabCell == NULL)
        [self ReleaseComm];
}
/*============================================================================*/

-(int)NArmingModes

/*============================================================================*/
{
    return [m_pArmingModes count];
}
/*============================================================================*/

-(CArmingMode*)ArmingMode:(int)iIndex

/*============================================================================*/
{
    CArmingMode *pMode = (CArmingMode*)[m_pArmingModes objectAtIndex:iIndex];
    return pMode;
}
/*============================================================================*/

-(int)NZones

/*============================================================================*/
{
    return [m_pZones count];
}
/*============================================================================*/

-(CSecZone*)Zone:(int)iIndex

/*============================================================================*/
{
    CSecZone *pZone = (CSecZone*)[m_pZones objectAtIndex:iIndex];
    return pZone;
}
/*============================================================================*/

-(int)NZonesNotReady

/*============================================================================*/
{
    int iCount = 0;
    int iNZones = [m_pZones count];
    for (int i = 0; i < iNZones; i++)
    {
        CSecZone *pZ = (CSecZone*)[m_pZones objectAtIndex:i];

        if (![pZ ReadyState])
        {
            iCount++;
        }
    }
    return iCount;
}
/*============================================================================*/

-(CSecZone*)ZoneNotReady:(int)iIndex

/*============================================================================*/
{
    int iCount = 0;
    int iNZones = [m_pZones count];
    for (int i = 0; i < iNZones; i++)
    {
        CSecZone *pZ = (CSecZone*)[m_pZones objectAtIndex:i];
        if (![pZ ReadyState])
        {
            if (iCount == iIndex)
                return pZ;
            iCount++;
        }
    }
    return NULL;
}
/*============================================================================*/

-(int)NFaults

/*============================================================================*/
{
    return [m_pFaults count];
}
/*============================================================================*/

-(NSString*)Fault:(int)iIndex

/*============================================================================*/
{
    NSString *psFault = (NSString*)[m_pFaults objectAtIndex:iIndex];
    return psFault;
}
/*============================================================================*/

-(int)PartitionStatus

/*============================================================================*/
{
    return m_iPartStatus;
}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    if (m_bCommInit)
        return;
        
    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_SECURITY_ADDCLIENTBYPARTIDQ];
    [pQ autorelease];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ PutInt:m_iID];
    [pQ SendMessageWithMyID:self];
    [self PostGetStatus];
    
    m_bCommInit = TRUE;
}
/*============================================================================*/

-(void)ReleaseComm

/*============================================================================*/
{
    if (!m_bCommInit)
        return;

    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_SECURITY_REMOVECLIENTQ];
    [pQ autorelease];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];
    [CHLComm RemoveSocketSink:self];

    m_bCommInit = FALSE;
}
/*============================================================================*/

-(void)PostGetStatus

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_SECURITY_GETALLPARTSTATBYIDQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)SetZoneState:(int)iZoneID State:(int)iState

/*============================================================================*/
{
    for (int i = 0; i < [m_pZones count]; i++)
    {
        CSecZone *pZ = (CSecZone*)[m_pZones objectAtIndex:i];
        if ([pZ ID] == iZoneID)
            [pZ SetState:iState];
    }
}
/*============================================================================*/

-(BOOL)StatusArmed          { return ((m_iPartStatus & SEC_PARTITION_ARMED) != 0);      }
-(BOOL)StatusArmable        { return ((m_iPartStatus & SEC_PARTITION_ARMABLE) != 0);    }
-(BOOL)StatusBypassable     { return ((m_iPartStatus & SEC_PARTITION_BYPASSABLE) != 0); }
-(BOOL)StatusAlarmActive    { return ((m_iPartStatus & SEC_PARTITION_ALARMACT) != 0);   }
-(BOOL)StatusEntryDel       { return ((m_iPartStatus & SEC_PARTITION_ENTRY_DEL) != 0);  }
-(BOOL)StatusExitDel        { return ((m_iPartStatus & SEC_PARTITION_EXIT_DEL) != 0);   }
-(BOOL)StatusNoResponse     { return ((m_iPartStatus & SEC_PARTITION_NORESPONSE) != 0); }
-(BOOL)StatusFire           { return ((m_iPartStatus & SEC_PARTITION_FIRE) != 0);       }

/*============================================================================*/

-(int)UserModeIndex

/*============================================================================*/
{
    int iArmingMode = [self ArmingMode];
    int iNVisible = [self NVisibleArmingModes];
    for (int i = 0; i < iNVisible; i++)
    {
        CArmingMode *pTest = [self UserArmingMode:i];
        if ([pTest Mode] == iArmingMode)
            return i;
    }

    return -1;
}
/*========================================================================*/

-(int)ArmingMode

/*========================================================================*/
{
    return ((m_iPartStatus & 0xFFFF0000) >> 16);
}
/*========================================================================*/

-(CArmingMode*)UserArmingMode:(int)iIndex

/*========================================================================*/
{
    int iCount = 0;
    for (int i = 0; i < [m_pArmingModes count]; i++)
    {
        CArmingMode *pMode = [m_pArmingModes objectAtIndex:i];
        if ([pMode Visible])
        {
            if (iCount >= iIndex)
                return pMode;
            iCount++;
        }
    }
    
    return NULL;
}
/*========================================================================*/

-(int)NVisibleArmingModes

/*========================================================================*/
{
    int iCount = 0;
    for (int i = 0; i < [m_pArmingModes count]; i++)
    {
        CArmingMode *pMode = [m_pArmingModes objectAtIndex:i];
        if ([pMode Visible])
        {
            iCount++;
        }
    }
    
    return iCount;
}
/*========================================================================*/

-(NSString*)ArmingModeText

/*========================================================================*/
{
    int iArmingMode = [self ArmingMode];
    int iNVisible = [self NVisibleArmingModes];
    for (int i = 0; i < iNVisible; i++)
    {
        CArmingMode *pTest = [self UserArmingMode:i];
        if ([pTest Mode] == iArmingMode)
            return [pTest Name];
    }
    
    return @"Error";
}
/*========================================================================*/

-(NSString*)StatusText

/*========================================================================*/
{
    if ([self StatusNoResponse])
    {
        return @"Connecting, please wait...";
    }
    else if ([self StatusAlarmActive])
    {
        if ([self StatusFire])
            return  @"Fire Alarm";
        else
            return @"Alarm Active";
    }
    else if ([self StatusArmed] && !([self StatusExitDel] || [self StatusEntryDel]))
    {
        if ([self NArmingModes] <= 2)
            return @"System is Armed";
        else
        {
            NSString *pModeText = [self ArmingModeText];
            NSString *pRet = [NSString stringWithFormat:@"Armed in %s Mode", [pModeText UTF8String]];
            return pRet;
        }
    }
    else if ([self StatusExitDel])
    {
        if ([self ArmingMode]  == 0)
            return @"Arming...";
        else
        {
            NSString *pModeText = [self ArmingModeText];
            NSString *pRet = [NSString stringWithFormat:@"Arming in %s Mode", [pModeText UTF8String]];
            return pRet;
        }
    }
    else if ([self StatusEntryDel])
    {
        return @"Entry Delay";
    }
    else if ([self StatusArmable])
        return @"Disarmed, Ready to Arm";
    else if ([self StatusBypassable])
        return @"Disarmed, Ready to Bypass and Arm";
    else
        return @"Disarmed, Not Ready to Arm";

    return @"";
}
/*============================================================================*/

-(void)SetSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    [super SetSubTabCell:pSubTabCell];
    [self UpdateSubTabCell:pSubTabCell];

    if (pSubTabCell != NULL)
        [self InitComm];
    else if ([m_pSinkServer NSinks] == 0)
        [self ReleaseComm];
}
/*============================================================================*/

-(void)UpdateSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    NSString *pIconName = @"SECURITY_ICON.png";
    if ([self StatusArmed] || [self StatusAlarmActive])
        pIconName = @"SEC_SECURE.png";
    else if ([self StatusArmable])
        pIconName = @"SEC_READY.png";

    [pSubTabCell SetEmbeddedIcon:pIconName];
}

@end
