/*
--------------------------------------------------------------------------------

    NSSOCKETSINK.MM

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "NSSocketSink.h"
#include "nsmsg.h"

extern "C"
{

class CHLMSG;

/*============================================================================*/

    void                        HLConnectionChanged(
    void                        *pNSSink,
    int                         iSenderID,
    BOOL                        bNowConnected)

/*============================================================================*/
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    id pIDSink = (id)pNSSink;
    //NSLog(@"**** ABOUT TO RELEASE");
    [pIDSink ConnectionChanged:bNowConnected];
    //NSLog(@"**** POOL RELEASED");
    [pPool release];
}
/*============================================================================*/

    void                        HLSinkMessage(
    void                        *pNSSink,
    int                         iSenderID,
    CHLMSG                      *pMSG )

/*============================================================================*/
{


    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
    id pIDSink = (id)pNSSink;
    
    CNSMSG *pNSMSG = [[CNSMSG alloc] initWithMSG:pMSG];

    [pIDSink SinkMessage:pNSMSG];
    [pNSMSG release];
    [pPool release];
}
}