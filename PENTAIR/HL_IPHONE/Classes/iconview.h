//
//  iconview.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "icon.h"

#define ICONVIEW_TVICON             0x00000001

/*============================================================================*/

    @interface CMainTabIconView : UIView

/*============================================================================*/
{
    UIImage                         *m_pImage;
}

-(void)SetImage:(UIImage*) pNew;
@end

/*============================================================================*/

    @interface CUserTabIconView : UIImageView < CIconContainer >

/*============================================================================*/
{
    UIImage                         *m_pImage;
    NSString                        *m_pIconName;
    int                             m_iOptions;
}

-(void)InitImage;
-(void)SetUserIcon:(NSString*) pNew;
-(void)SetEmbeddedIcon:(NSString*) pNew;
-(void)SetOptions:(int)iOptions;

@end

/*============================================================================*/

    @interface CAccesoryView : UIView

/*============================================================================*/
{
}

@end
