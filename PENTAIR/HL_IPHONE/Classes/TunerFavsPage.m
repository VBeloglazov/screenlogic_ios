//
//  TunerFavsPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "AudioZone.h"
#import "NSSocketSink.h"
#import "hlcomm.h"
#import "hlm.h"
#import "NSMSG.h"
#import "NSQuery.h"
#import "TableTitleHeaderView.h"
#import "HLTableFrame.h"
#import "TunerContainerPage.h"

#import "TunerFavsPage.h"

@implementation CStation


/*============================================================================*/

-(id)initWithID:(int)iID Name:(NSString*)pName

/*============================================================================*/
{
    m_pName = pName;
    [m_pName retain];
    m_iID = iID;
    
    return self;
}
/*============================================================================*/

-(id)initWithID:(int)iID Icon:(NSString*)pIcon Name:(NSString*)pName

/*============================================================================*/
{
    m_pName = pName;
    m_pIcon = pIcon;

    [m_pName retain];
    [m_pIcon retain];
    
    m_iID = iID;
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [m_pIcon release];
    [super dealloc];
}
/*============================================================================*/

-(int)ID                    { return m_iID;     }
-(NSString*)Name            { return m_pName;   }
-(NSString*)IconName        { return m_pIcon;   }

/*============================================================================*/

@end

@implementation CTunerFavsPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone Container:(CTunerContainerPage*)pContainer

/*============================================================================*/
{
    if (self == [super initWithFrame:rect])
    {
        m_pContainer = pContainer;
        m_pZone = pZone;
        [m_pZone AddSink:self];

        self.backgroundColor = [UIColor clearColor];

        //
        // Table View
        //
        CGRect rTable = CGRectMake(0, DY_HEADER_SPACE, rect.size.width, rect.size.height-DY_HEADER_SPACE);
//        m_pTableView = [[CHLTableFrame alloc] initWithFrame:(CGRect)rTable Title:@"Station Presets" DataSource:self Delegate:self];
        m_pTableView = [[CHLTableFrame alloc] initWithFrame:(CGRect)rTable DataSource:self Delegate:self];
        
        // set the autoresizing mask so that the table will always fill the view
        m_pTableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
        
        // set the tableview delegate to this object and the datasource to the datasource which has already been set
        [[m_pTableView TableView] setTag:999 ];
        
        self.backgroundColor = [UIColor clearColor];
        
        // set the tableview as the controller view
        [self addSubview:m_pTableView];

        m_pStations = [[NSMutableArray alloc] init];

        [CHLComm AddSocketSink:self];
        int iTunerType = [m_pZone ActiveSubSource];
        BOOL bXM = FALSE;
        if ((iTunerType & 0x0000FFFF) == AUX_SOURCE_TUNER_XM)
            bXM = TRUE;
        if ((iTunerType & 0x0000FFFF) == AUX_SOURCE_TUNER_SIRIUS)
            bXM = TRUE;

        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETALLTSTATIONSQ];
        [pQ autorelease];
        [pQ PutInt:bXM];
        [pQ PutInt:iTunerType];
        [pQ PutInt:m_pZone.m_iID];
        [pQ SendMessageWithMyID:self];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pZone RemoveSink:self];
    [m_pTableView release];
    [m_pStations release];
    [super dealloc];
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    int iTunerType = [m_pZone ActiveSubSource];
    int iBasicType = (iTunerType & 0x0000FFFF);

    if (iBasicType == AUX_SOURCE_TUNER_DEVICEPRESETS )
    {
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_TUNEZONETOFAVBYIDQ];
        [pQ autorelease];
        [pQ PutInt:m_pZone.m_iID];
        [pQ PutInt:indexPath.row];
        [pQ SendMessageWithMyID:self];
    }
    else
    {
        CStation *pStation = (CStation*)[m_pStations objectAtIndex:indexPath.row];

        if ((iBasicType == AUX_SOURCE_TUNER_XM) || (iBasicType == AUX_SOURCE_TUNER_SIRIUS))
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_TUNEZONETOXMFAVBYIDQ];
            [pQ autorelease];
            [pQ PutInt:m_pZone.m_iID];
            [pQ PutInt:[pStation ID]];
            [pQ SendMessageWithMyID:self];
        }
        else
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_TUNEZONETOFAVBYIDQ];
            [pQ autorelease];
            [pQ PutInt:m_pZone.m_iID];
            [pQ PutInt:[pStation ID]];
            [pQ SendMessageWithMyID:self];
        }
    }
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return 1;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    return [m_pStations count];
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }

    CStation *pStation = (CStation*)[m_pStations objectAtIndex:indexPath.row];

    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setText:[pStation Name]];

    // Configure the cell
    return cell;
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iStationID = [m_pZone TunerFavoriteID];
    int iSelIndex = -1;
    for (int i = 0; i < [m_pStations count] ; i++)
    {
        CStation *pS = (CStation*)[m_pStations objectAtIndex:i];
    
        if ([pS ID] == iStationID)
        {
            iSelIndex = i;
        }
        else
        {
            NSIndexPath *pPath = [NSIndexPath indexPathForRow:i inSection:0];
            [[m_pTableView TableView] deselectRowAtIndexPath:pPath animated:YES];
        }
    } 

    if (iSelIndex >= 0)
    {
        NSIndexPath *pPath = [NSIndexPath indexPathForRow:iSelIndex inSection:0];
        [[m_pTableView TableView] selectRowAtIndexPath:pPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iMSGID = [pMSG MessageID];
    if (iMSGID != HLM_AUDIO_GETALLTSTATIONSA)
        return;

    [m_pStations removeAllObjects];
    int iNStations = [pMSG GetInt];
    for (int i = 0; i < iNStations; i++)
    {
        int iID = [pMSG GetInt];
        NSString *pName = [pMSG GetString];
        [pMSG GetInt]; // TRUE for FM
        [pMSG GetInt]; // The Hz
        CStation *pStation = [[CStation alloc] initWithID:iID Name:pName];
        [m_pStations addObject:pStation];
        [pStation release];
    }
    
    if (iNStations < 1)
    {
        [m_pContainer NotifyEmptyFavorites];
    }
    
    [m_pTableView ReloadData];
}


@end
