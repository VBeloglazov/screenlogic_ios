//
//  SubTabheader.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/19/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMainTab;
@class CMainTabViewController;

/*============================================================================*/

@interface CSubTabHeader : UIView 

/*============================================================================*/
{
	UILabel                             *m_pLabel;
    UIImage                             *m_pImage;
}

-(id)initWithFrame:(CGRect)frame MainTab:(CMainTab*)pMainTab;

@end
