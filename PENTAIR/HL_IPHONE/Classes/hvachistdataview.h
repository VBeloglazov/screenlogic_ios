//
//  hvachistdataview.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "timedataview.h"
#import "NSSocketSink.h"

@class CTStat;
@class CNSMSG;
@class CRTEView;
@class CRTESeries;
@class CTimeBarLabel;

#define MIN_TEMP                55
#define MAX_TEMP                75





/*============================================================================*/

@interface CHVACHistoryDataView : CTimeDataView <CNSSocketSink>

/*============================================================================*/
{
    CTStat                          *m_pTStat;
    NSMutableArray                  *m_pTempSeries;
    NSMutableArray                  *m_pRTESeries;
    NSTimer                         *m_pQueryTimer;
    
    CRTEView                        *m_pRTEHeatView;
    CRTEView                        *m_pRTECoolView;
    int                             m_iTempMin;
    int                             m_iTempMax;

    BOOL                            m_bCheckScale;
    int                             m_iFrameResizeOld;
    int                             m_iFrameResizeNew;
}

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView TStat:(CTStat*)pTStat LastView:(CHVACHistoryDataView*)pLastView;

-(int)YLogical:(int)iTemp;
-(void)DrawSetPoints:(CGContextRef)pRef Color:(UIColor*)pRGB Series:(CTempSeries*)pSeries Dir:(BOOL)bTopDown;

-(NSMutableArray*)TempSeries;
-(NSMutableArray*)RTESeries;
-(void)PostQuery;
-(void)AddRTEViews;
-(void)UpdateRTEViews;
-(void)UpdateTimeBar;
-(void)UpdateLabel:(CTimeBarLabel*)pLabel;
-(void)AddInfoTextToView:(UIView*)pView Rect:(CGRect)rect PCT:(int)iPCT Color:(UIColor*)pColor;
-(int)CalcUsage:(CRTESeries*)pSeries StartDate:(NSDate*)pStartDate EndDate:(NSDate*)pEndDate;

-(int)MinTemp;
-(int)MaxTemp;
-(BOOL)SetScaleMin:(int)iMin Max:(int)iMax;
-(BOOL)ResizeFrameFrom:(int)iFrom To:(int)iTo;
-(void)SetCheckScale;
-(void)SetFrameResizeOld:(int)iDYDataOld New:(int)iDYDataNew;

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;

@end
