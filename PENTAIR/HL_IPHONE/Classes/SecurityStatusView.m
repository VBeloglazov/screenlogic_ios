//
//  SecurityStatusView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "SecurityStatusView.h"
#import "SecPartition.h"
#import "crect.h"
#import "hltoolbar.h"
#import "hlsegment.h"
#import "SecZoneCell.h"
#import "TableTitleHeaderView.h"
#import "HLTableFrame.h"
#import "hlm.h"
#import "ArmingMode.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "hlbutton.h"
#import "CustomPage.h"

#define HEADER_SIZE             40

@implementation CSecurityStatusView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Partition:(CSecPartition*)pPartition

/*============================================================================*/
{
    if ((self = [super initWithFrame:frame])) 
    {
        m_pPartition = pPartition;
        [m_pPartition AddSink:self];
        self.opaque = NO;

        //
        // Status Text at top
        //
        m_pStatusText = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pStatusText Size:40];
        m_pStatusText.autoresizingMask = 0xFFFFFFFF;
        [m_pStatusText setText:@"Status"];
        [m_pStatusText setAdjustsFontSizeToFitWidth:YES];
        [m_pStatusText setMinimumFontSize:8];
        [self addSubview:m_pStatusText];
        [m_pStatusText release];

        //
        // Arming Buttons
        //

        
        NSMutableArray *pModes = [[NSMutableArray alloc] init];
        int iNModes = [m_pPartition NVisibleArmingModes];
        for (int i = 0; i < iNModes; i++)
        {
            CArmingMode *pMode = [m_pPartition UserArmingMode:i];
            [pModes addObject:[pMode Name]];
        }

        m_pModes = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Items:pModes Type:HLSEGMENT_BUTTON Data:NO];
        [self addSubview:m_pModes];
        [pModes release];
        [m_pModes addTarget:self action:@selector(SetArmMode:) forControlEvents:UIControlEventValueChanged];

        [self Notify:0 Data:0];
        
        //
        // List Boxes
        //
        m_pZones = [[CSecurityListBox alloc] initWithFrame:CGRectZero Partition:m_pPartition Type:LISTBOX_ZONES];
        m_pFaults = [[CSecurityListBox alloc] initWithFrame:CGRectZero Partition:m_pPartition Type:LISTBOX_FAULTS];
        [self addSubview:m_pZones];
        [self addSubview:m_pFaults];
        [m_pZones release];
        [m_pFaults release];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pPartition RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    [m_pStatusText setText:[m_pPartition StatusText]];

    int iCheckedIndex = 0;
    if ([m_pPartition StatusArmed])
    {
        int iUserMode = [m_pPartition UserModeIndex];
        if (iUserMode >= 1 && iUserMode < [m_pModes NItems])
            iCheckedIndex = iUserMode;
    }

    [m_pModes SetSelectedIndex:iCheckedIndex];
}
/*============================================================================*/

-(void)SetArmMode:(id)sender

/*============================================================================*/
{
    if (m_pKeypad != NULL)
        return;

    CGRect rKeypad = [self KeypadRect];
    rKeypad.origin.y += self.bounds.size.height;
    m_pKeypad = [[CKeypadView alloc] initWithFrame:rKeypad Partition:m_pPartition View:self Mode:[m_pModes SelectedIndex]];
    m_pKeypad.alpha = 0;
    [self addSubview:m_pKeypad];
    [m_pKeypad release];


    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    
    m_iDXLast = 0;
    [self layoutSubviews];

    [m_pStatusText setAlpha:0.0];
    [m_pZones setAlpha:0.0];
    [m_pFaults setAlpha:0.0];
    [m_pModes setAlpha:0.25];

    [m_pKeypad setAlpha:1.0];
    [m_pModes setUserInteractionEnabled:NO];
    
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)DismissKeypad

/*============================================================================*/
{
    _m_pKeypad = m_pKeypad;
    m_pKeypad = NULL;

    CGRect rKeypad = [self KeypadRect];
    rKeypad.origin.y += self.bounds.size.height / 4;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [UIView setAnimationDuration:0.5];

    m_iDXLast = 0;
    [self layoutSubviews];

    [_m_pKeypad setFrame:rKeypad];
    [_m_pKeypad setAlpha:0];

    [m_pStatusText setAlpha:1.0];
    [m_pZones setAlpha:1.0];
    [m_pFaults setAlpha:1.0];
    [m_pModes setAlpha:1.0];

    [m_pKeypad setAlpha:1.0];
    [m_pModes setUserInteractionEnabled:YES];

    [UIView commitAnimations];
    [self Notify:0 Data:0];
}
/*============================================================================*/

-(CGRect)KeypadRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    int iDYMode = [CMainView DY_TOOLBAR];

    if (rThis.size.width > rThis.size.height)
        rThis.origin.y -= iDYMode / 2;

    [CRect Inset:&rThis DX:0 DY:iDYMode];
    [CRect CenterDownToX:&rThis DX:350];
    [CRect ConfineToAspect:&rThis DXDY:3.0/4.0];
    return rThis;
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    [_m_pKeypad removeFromSuperview];
    _m_pKeypad = NULL;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    if (rThis.size.width == m_iDXLast && rThis.size.height == m_iDYLast)
        return;
    m_iDXLast = rThis.size.width;
    m_iDYLast = rThis.size.height;

    int iDYMode = [CMainView DY_TOOLBAR];

    int iYOffset = 0;
    if (m_pKeypad != NULL)
        iYOffset = rThis.size.height / 4;

    CGRect rStatus = [CRect BreakOffTop:&rThis DY:iDYMode];
    rStatus.origin.y -= iYOffset;
    [m_pStatusText setFrame:rStatus];

    CGRect rMode = [CRect BreakOffBottom:&rThis DY:iDYMode];
    [CRect BreakOffBottom:&rMode DY:8];
    [CRect CenterDownToX:&rMode DX:[m_pModes NItems] * 150];
    [m_pModes setFrame:rMode];

    CGRect rZones = CGRectZero;
    CGRect rFaults = CGRectZero;

    if (self.bounds.size.width > self.bounds.size.height)
    {
        rZones = [CRect BreakOffLeft:&rThis DX:rThis.size.width*65/100];
        rFaults = rThis;        
    }
    else
    {
        rZones = [CRect BreakOffTop:&rThis DY:rThis.size.height/2];
        rFaults = rThis;        
    }

    [CRect Inset:&rZones DX:8 DY:8];
    [CRect Inset:&rFaults DX:8 DY:8];

    rZones.origin.y -= iYOffset;
    rFaults.origin.y -= iYOffset;
    
    [m_pZones setFrame:rZones];
    [m_pFaults setFrame:rFaults];

    // Should have to do this, but we do
    [m_pZones layoutSubviews];
    [m_pFaults layoutSubviews];
    
    if (m_pKeypad != NULL)
    {
        [m_pKeypad setFrame:[self KeypadRect]];
    }
}
@end

@implementation CSecurityListBox

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition Type:(int)iType

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        m_pPartition = pPartition;
        [m_pPartition AddSink:self];

        m_iType = iType;

        NSString *psTitle = NULL;
        switch (m_iType)
        {
        case LISTBOX_ZONES:  psTitle = @"Zone Status";     break;
        case LISTBOX_FAULTS: psTitle = @"System Faults";   break;
        }

//        m_pTableView = [[CHLTableFrame alloc] initWithFrame:self.bounds Title:psTitle DataSource:self Delegate:self];
        m_pTableView = [[CHLTableFrame alloc] initWithFrame:self.bounds DataSource:self Delegate:self];
        [self addSubview:m_pTableView];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pPartition RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    [m_pTableView ReloadData];

    switch (m_iType)
    {
    case LISTBOX_ZONES:
        {
            if ([m_pPartition NZonesNotReady] > 0)
                [m_pTableView ClearEmpty];
            else
                [m_pTableView SetEmpty:@"All Zones Ready"];
        }
        break;
    case LISTBOX_FAULTS:
        {
            if ([m_pPartition NFaults] > 0)
                [m_pTableView ClearEmpty];
            else
                [m_pTableView SetEmpty:@"No System Faults"];
        }
        break;
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    [m_pTableView setFrame:rThis];
    [m_pTableView layoutSubviews];
    
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return 1;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    switch (m_iType)
    {
    case LISTBOX_ZONES:     return [m_pPartition NZonesNotReady];
    case LISTBOX_FAULTS:    return [m_pPartition NFaults];
    }
    return 0;
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    static NSString *CellIdentifier = @"Cell";

    if (indexPath == NULL)
        return NULL;

    switch (m_iType)
    {
    case LISTBOX_ZONES:     
        {
            CSecZoneCell *cell = (CSecZoneCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) 
            {
                cell = [[[CSecZoneCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier Partition:m_pPartition] autorelease];
            }

            CSecZone *pZone = [m_pPartition ZoneNotReady:indexPath.row];

            [cell SetZone:pZone];
            return cell;
        }
        break;
    case LISTBOX_FAULTS:
        {
            UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) 
            {
                cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
            }


            [cell.textLabel setText:[m_pPartition Fault:indexPath.row]];
            [CCustomPage InitStandardLabel:cell.textLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
            [cell.textLabel setTextAlignment:UITextAlignmentLeft];
            return cell;
        }
        break;
    }

    return NULL;
}


@end

@implementation CKeypadView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition View:(CSecurityStatusView*)pView Mode:(int)iIndex

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        m_iModeIndex    = iIndex;
        m_pView         = pView;
        m_pPartition    = pPartition;
        m_pBtns         = [[NSMutableArray alloc] init];
        for (int i = 0; i < 12; i++)
        {
            NSString *psText = NULL;
            int iCmdID = 0;
            UIColor *pColor = NULL;

            if (i < 9)
            {
                psText = [NSString stringWithFormat:@"%d", i+1];
                iCmdID = CMD_DIG0 + i + 1;
            }
            else 
            {
                switch (i)
                {
                case 9:     {   psText = @"Cancel"; iCmdID = CMD_KEY_CLEAR;                                 }  break;
                case 10:    {   psText = @"0";      iCmdID = CMD_DIG0;                                      }  break;
                case 11:    {   psText = @"Enter";  iCmdID = CMD_KEY_ENTER; pColor = [UIColor blueColor];   }  break;

                }
            }


            CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:psText];
            [pBtn SetCommandID:iCmdID Responder:self];
            if (pColor != NULL)
                [pBtn SetColor:pColor];
                
            [self addSubview:pBtn];
            [m_pBtns addObject:pBtn];
            [pBtn release];
        }

        CArmingMode *pMode = [m_pPartition UserArmingMode:m_iModeIndex];
        m_nKeysWanted = [pMode NKeysEntry];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pBtns release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDX = rThis.size.width / 3;
    int iDY = rThis.size.height / 4;
    
    int iCol = 0;
    int iRow = 0;
    for (int i = 0; i < 12; i++)
    {
        CGRect rThis = CGRectMake(iCol * iDX, iRow * iDY, iDX, iDY);
        [CRect Inset:&rThis DX:4 DY:4];
        CHLButton *pBtn = (CHLButton*)[m_pBtns objectAtIndex:i];
        [pBtn setFrame:rThis];

        iCol++;
        if (iCol >= 3)  { iRow++; iCol = 0; }
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (iCommandID == CMD_KEY_ENTER)
    {
        // Figure out what they're trying to do
        short ushMessage = HLM_SECURITY_KEYPAD_DISARMQ;
        int iArmMode = 0;
        if (m_iModeIndex != 0)
        {
            ushMessage = HLM_SECURITY_KEYPAD_ARMQ;
            CArmingMode *pMode = [m_pPartition UserArmingMode:m_iModeIndex];
            if (pMode != NULL)
                iArmMode = [pMode Mode];
        }

        NSString *pPIN = @"";
        for (int i = 0; i < m_nKeysEntered; i++)
        {
            pPIN = [NSString stringWithFormat:@"%s%d", [pPIN UTF8String], m_Keys[i]];
        }

        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:ushMessage];
        [pQ autorelease];
        [pQ PutInt:m_pPartition.m_iID];
        [pQ PutInt:iArmMode];
        [pQ PutString:pPIN];
        [pQ SendMessage];
        
        [m_pView DismissKeypad];
    }
    else if ((iCommandID >= CMD_DIG0) && (iCommandID <= CMD_DIG9))
    {
//        m_hltUIMode.Restart();
        if (m_nKeysEntered >= 10)
            return;
        int iDig = iCommandID - CMD_DIG0;
        m_Keys[m_nKeysEntered] = iDig;
        m_nKeysEntered++;

        if (m_nKeysWanted > 0 && m_nKeysEntered >= m_nKeysWanted)
        {
            [self OnCommand:CMD_KEY_ENTER];
        }
    }
    else if (iCommandID == CMD_KEY_CLEAR)
    {
        [m_pView DismissKeypad];
    }
}

@end
