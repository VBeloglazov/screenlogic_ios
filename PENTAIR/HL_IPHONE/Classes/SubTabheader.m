//
//  SubTabheader.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/19/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SubTabheader.h"
#import "MainTabVC.h"
#import "MainTab.h"
#import "SubTab.h"
#import "CustomPage.h"

@implementation CSubTabHeader


/*============================================================================*/

-(id)initWithFrame:(CGRect)frame MainTab:(CMainTab*)pMainTab

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        m_pImage = [UIImage imageNamed:[pMainTab ImageName]];
        [m_pImage retain];

        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:16];
        [m_pLabel setText:pMainTab.m_pName];
        [m_pLabel setAdjustsFontSizeToFitWidth:YES];
        [m_pLabel setMinimumFontSize:11];
        [self addSubview:m_pLabel];
    }
    return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iDXIcon = rBounds.size.height;
    rBounds.origin.x  += iDXIcon;
    rBounds.size.width -= iDXIcon;
    m_pLabel.frame = rBounds;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{

    int iDY = rect.size.height;
    int iDYImage = iDY - 8;
    CGRect rImage = CGRectMake(0, (iDY-iDYImage)/2, iDYImage, iDYImage);
    [m_pImage drawInRect:rImage];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pImage release];
    [super dealloc];
}


@end
