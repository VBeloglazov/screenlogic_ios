//
//  AudioKeypadListControl.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/26/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


/*============================================================================*/

@interface CAudioKeypadListControl : UIView 

/*============================================================================*/
{
    
}

-(id)initWithFrame:(CGRect)rFrame ID:(int)iID TextSize:(int)iTextSize TextColor:(UIColor*)pColor Style:(int)iStyle;

@end
