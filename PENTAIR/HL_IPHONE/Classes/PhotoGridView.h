//
//  PhotoGridView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlview.h"

@class CPhotoAlbum;
@class CPhotosPage;

/*============================================================================*/

@interface CPhotoGridView : UIView < UIScrollViewDelegate >

/*============================================================================*/
{
    CPhotosPage                         *m_pPhotosPage;
    CPhotoAlbum                         *m_pAlbum;
    NSMutableArray                      *m_pPictures;
    NSTimer                             *m_pTimerIO;
    UIScrollView                        *m_pScrollView;
    int                                 m_iNPacketsInFlight;
}

-(id)initWithFrame:(CGRect)rFrame MainPage:(CPhotosPage*)pMainPage Album:(CPhotoAlbum*)pAlbum;
-(void)StartTimer;

-(void)IncPacketsInFlight;
-(void)DecPacketsInFlight;


@end
