//
//  DBManager.m
//  XPAD_VIEW
//
//  Created by dmitriy on 3/16/14.
//
//

#import "DBManager.h"
static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager

+(DBManager*)getSharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB
{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"connections.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS PREVCONTACTS (       ID              INTEGER PRIMARY KEY AUTOINCREMENT,\
                                                                                    SYSTEMNAME      TEXT,\
                                                                                    NICKNAME        TEXT,\
                                                                                    PASSWORD        TEXT)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

-(void)addController:(ControllerDescriptor*) descriptor
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO PREVCONTACTS \
                                    (SYSTEMNAME, NICKNAME, PASSWORD) VALUES ( \"%@\",\"%@\", \"%@\")",
                                    [descriptor systemName],
                                    [descriptor nickName],
                                    [descriptor password]];
        
        
                                const char *insert_stmt = [insertSQL UTF8String];
                                sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
                                if (sqlite3_step(statement) == SQLITE_DONE)
                                {
                                    //return YES;
                                } 
                                else
                                {
                                    //return NO;
                                }
                                sqlite3_reset(statement);
    }
}
-(BOOL)ifExists:(NSString*) name
{
    BOOL result = FALSE;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT SYSTEMNAME FROM PREVCONTACTS WHERE SYSTEMNAME =\"%@\"",name];
        const char *query_stmt = [querySQL UTF8String];
        int r;
        if ((r = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL)) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = TRUE;
            }
            sqlite3_reset(statement);
        }
    }
    return result;
}

//systemName field must have the name, the descriptor structure will be filled out with found data
-(BOOL)getControllerByName:(ControllerDescriptor*) descriptor
{
    BOOL result = FALSE;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT * FROM PREVCONTACTS WHERE SYSTEMNAME =\"%@\"", [descriptor systemName]];
        const char *query_stmt = [querySQL UTF8String];
        int r;
        if ((r = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL)) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *nickName = [[NSString alloc]initWithUTF8String:(const char*)sqlite3_column_text(statement, 2)];
                [descriptor setNickName:nickName];
                NSString *password = [[NSString alloc]initWithUTF8String:(const char*)sqlite3_column_text(statement, 3)];
                [descriptor setPassword:password];
                result = TRUE;
            }
            sqlite3_reset(statement);
        }
    }
    return result;
}

//systemName field must have the name
-(BOOL)deleteControllerByName:(ControllerDescriptor*)descriptor
{
    BOOL result = FALSE;
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM PREVCONTACTS WHERE SYSTEMNAME =\"%@\"", [descriptor systemName]];
        const char *query_stmt = [querySQL UTF8String];
        int r;
        if((r = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL)) == SQLITE_OK)
        {
            if(sqlite3_step(statement) == SQLITE_DONE)
            {
                result = TRUE;
            }
        }
        sqlite3_finalize(statement);
        //sqlite3_close(database);
    }
    return result;
}

-(NSMutableArray*)getAllControllers
{
    NSSortDescriptor* sortRule = [NSSortDescriptor sortDescriptorWithKey: @"nickName" ascending:YES];
    
    NSMutableArray *controllerList = [[[NSMutableArray alloc] init] autorelease];

    
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM PREVCONTACTS WHERE SYSTEMNAME <> ''"];
        const char *query_stmt = [querySQL UTF8String];
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                ControllerDescriptor *cd = [[ControllerDescriptor alloc] init];
                cd.systemName = [[NSString alloc]initWithUTF8String:(const char*)sqlite3_column_text(statement, 1)];
                cd.nickName = [[NSString alloc]initWithUTF8String:(const char*)sqlite3_column_text(statement, 2)];
                cd.password = [[NSString alloc]initWithUTF8String:(const char*)sqlite3_column_text(statement, 3)];
                [controllerList addObject:cd];
            }
        }
    }
    //DL to make it sorted ascending
    return (NSMutableArray*)[controllerList sortedArrayUsingDescriptors: [NSArray arrayWithObject:sortRule]];;
}

-(BOOL)updateNickname:(ControllerDescriptor*)descriptor
{
    BOOL result = FALSE;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        NSString *querySQL = [NSString stringWithFormat:
                              @"UPDATE PREVCONTACTS SET NICKNAME =\"%@\" WHERE SYSTEMNAME =\"%@\"", [descriptor nickName], [descriptor systemName]];
        const char *query_stmt = [querySQL UTF8String];
        int r;
        if ((r = sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL)) == SQLITE_OK)
        {
            if ((r = sqlite3_step(statement)) == SQLITE_DONE)
            {
                result = TRUE;
            }
            sqlite3_reset(statement);
        }
    }
    return result;

    
}
@end
