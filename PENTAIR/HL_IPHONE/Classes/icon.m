//
//  icon.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "icon.h"
#import "hlcomm.h"
#import "nsmsg.h"
#import "nsquery.h"


/*============================================================================*/
@implementation CIcon

/*============================================================================*/

-(id)initWithName:(NSString*)pName

/*============================================================================*/
{
    if (self == [super init])
    {
        m_bWaitImage = FALSE;
        m_pName = pName;
        [m_pName retain];
        m_pImage = NULL;
        m_pImageVirtualAlpha = NULL;
        m_pWaitList = [[NSMutableArray alloc] init];
        [CHLComm AddSocketSink:self];
        [self Post];
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_bWaitImage)
    {
        [CHLComm RemoveSocketSink:self];
    }

    [m_pWaitList release];
    if (m_pImage != NULL)
        CGImageRelease(m_pImage);
    if (m_pImageVirtualAlpha != NULL)
        CGImageRelease(m_pImageVirtualAlpha);
    [m_pName release];
    [super dealloc];

    if (m_pData != NULL)
        free(m_pData);
}
/*============================================================================*/

-(BOOL)IsSameIcon:(NSString*)pName

/*============================================================================*/
{
    return ([m_pName isEqual:pName]);
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        if (m_pImage == NULL)
        {
            [self Post];
        }
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG SenderID] != [CHLComm SenderID:self])
        return;

    if ([pMSG MessageID] == HLM_IMAGESERV_GETICONBYNAMEA)
    {
        if ([self LoadImage:pMSG])
        {
            for (int iWnd = 0; iWnd < [m_pWaitList count]; iWnd++)
            {
                UIView *pView = (UIView*)[m_pWaitList objectAtIndex:iWnd];

                if ([pView conformsToProtocol:@protocol(CIconContainer)])
                {
                    id pID = pView;
                    [pID NotifyIconReady];
                }

                [pView setNeedsDisplay];

                UIView *pSuperView = pView.superview;
                while (pSuperView != NULL)
                {
                    if ([pSuperView conformsToProtocol:@protocol(CHLView)])
                    {
                        UIView <CHLView> *pHLView = (UIView < CHLView> *) pSuperView;
                        [pHLView NotifySubViewChanged];
                    }
                    pSuperView = pSuperView.superview;
                }
            }
        }
        else
        {
            [pMSG ResetRead];
            [self LoadImage:pMSG];
        }

        [m_pWaitList release];
        m_pWaitList = NULL;
        [CHLComm RemoveSocketSink:self];
    }
    else if ([pMSG MessageID] == HLM_BAD_PARAMETERS)
    {
        [m_pWaitList release];
        m_pWaitList = NULL;
        [CHLComm RemoveSocketSink:self];
    }
}
/*============================================================================*/

-(int)SourceType

/*============================================================================*/
{
    return m_iSourceType;
}
/*============================================================================*/

-(CGImageRef)GetImage:(UIView*)pView

/*============================================================================*/
{
    if (m_pImage != NULL)
        return m_pImage;
    if (pView != NULL)
    {
        if (m_pWaitList == NULL)
            return NULL;

        [m_pWaitList addObject:pView];
    }
    
    return NULL;
}
/*============================================================================*/

-(CGImageRef)GetImageVirtualAlpha

/*============================================================================*/
{
    if (m_pImageVirtualAlpha != NULL)
        return m_pImageVirtualAlpha;

    if (m_pImage == NULL)
        return NULL;

    int iDX = CGImageGetWidth(m_pImage);
    int iDY = CGImageGetHeight(m_pImage);
    int iSizeData = iDX * iDY * 4;
    void *pImageData = malloc(iSizeData);
    memset(pImageData, 0, iSizeData);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pRef = CGBitmapContextCreate(pImageData, iDX, iDY, 8, iDX*4, pColorSpace, kCGImageAlphaPremultipliedFirst);

    // Draw Here
    CGRect rOutIcon = CGRectMake(0, 0, iDX, iDY);
    CGContextDrawImage(pRef, rOutIcon, m_pImage);

    int iNPIX = iDX * iDY;
    unsigned char *pAlpha = pImageData;
    unsigned char *pR = pImageData+1;
    unsigned char *pG = pImageData+2;
    unsigned char *pB = pImageData+3;
    for (int i = 0; i < iNPIX; i++)
    {
        unsigned char byR = *pR;
        unsigned char byG = *pG;
        unsigned char byB = *pB;

        int iAVG = (byR + byG + byG) / 3;
        int iGrayOff = abs(iAVG - byR) + abs(iAVG - byG) + abs(iAVG - byB);
        if (iGrayOff < 128 && iAVG > 128)
        {
            unsigned char byAlpha = (255-iAVG)+iGrayOff;
            *pAlpha = byAlpha;
            *pR = (byR * byAlpha) / 255;
            *pG = (byG * byAlpha) / 255;
            *pB = (byB * byAlpha) / 255;

        }

        pAlpha += 4;
        pR += 4;
        pG += 4;
        pB += 4;
    }
    
    CGContextRef pMod = CGBitmapContextCreate(pImageData, iDX, iDY, 8, iDX*4, pColorSpace, kCGImageAlphaPremultipliedFirst);
    m_pImageVirtualAlpha = CGBitmapContextCreateImage(pMod);
    CGContextRelease(pMod);

    CGColorSpaceRelease(pColorSpace);

    CGContextRelease(pRef);
    free(pImageData);

    return m_pImageVirtualAlpha;
}
/*============================================================================*/

-(void)Post

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_IMAGESERV_GETICONBYNAMEQ];
    [pQ autorelease];
    [pQ PutString:m_pName];
    [pQ SendMessageWithID:[CHLComm SenderID:self]];
    
}
/*============================================================================*/

-(BOOL)LoadImage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iType = [pMSG GetInt];
    if (iType == ENCODE_HLICON)
        return FALSE;
    int iSize = [pMSG GetInt];
    
    void *pData = [pMSG GetDataAtReadIndex];
    m_pData = malloc(iSize);
    memcpy(m_pData, pData, iSize);

    CGDataProviderRef pSrc = CGDataProviderCreateWithData(NULL, m_pData, iSize, NULL);
    switch (iType)
    {
    case ENCODE_JPEG:
        m_pImage = CGImageCreateWithJPEGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
        break;

    case ENCODE_GIF:
        {
            NSData *pNSData = [NSData dataWithBytes:pData length:iSize];
            UIImage *pImage = [UIImage imageWithData:pNSData];
            m_pImage = [pImage CGImage];
            CGImageRetain(m_pImage);
        }
        break;
    
    case ENCODE_PNG:
        m_pImage = CGImageCreateWithPNGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
        break;
    }

    m_iSourceType = iType;

    CGDataProviderRelease(pSrc);
    return (m_pImage != NULL);
}
@end
