//
//  MainTabCell.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MainTabCell.h"
#import "maintab.h"
#import "iconview.h"
#import "GradientView.h"
#import "hlm.h"
#import "mainview.h"
#import "crect.h"
#import "CustomPage.h"


@implementation CMainTabCell

@synthesize m_pLabel;
@synthesize m_pMainTab;

#define CONTENTS_INSET                  20
#define DX_ACCESSORY                    80

/*============================================================================*/

- (id)initWithFrame:(CGRect)aRect reuseIdentifier:(NSString *)identifier

/*============================================================================*/
{
	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

	if (self)
	{
		// you can do this here specifically or at the table level for all cells
        self.textLabel.textColor = [UIColor whiteColor];
        self.accessoryType = UITableViewCellAccessoryNone;

        m_pGradientView1 = [[CGradientView alloc] initWithFrame:CGRectZero Selected:FALSE];
        m_pGradientView2 = [[CGradientView alloc] initWithFrame:CGRectZero Selected:TRUE];
   
		// Create label views to contain the various pieces of text that make up the cell.
		// Add these as subviews.
		m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
		m_pLabel.highlightedTextColor = [UIColor whiteColor];
        //defines text size on big buttons
        int iTextSize = 22;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            iTextSize = 40;
            
        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];
        [m_pLabel setTextAlignment:UITextAlignmentLeft];

        CGRect frameIcon = CGRectMake(0, 0, 32, 32);
		m_pIcon = [[CMainTabIconView alloc] initWithFrame:frameIcon];	// layoutSubViews will decide the final frame
		m_pIcon.backgroundColor = [UIColor clearColor];
		m_pLabel.opaque = NO;

        
        [self.contentView setBackgroundColor:[UIColor blackColor]];
        [self.contentView addSubview:m_pGradientView1];
        [self.contentView addSubview:m_pGradientView2];
		[self.contentView addSubview:m_pLabel];
		[self.contentView addSubview:m_pIcon];
    
        m_pGradientView2.alpha = 0;


        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
	
	return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    //DL - controls top margin for big buttons background - iPhone
    //contentRect.origin.y += 20;
    
    contentRect.origin.x += [CMainView MAINTABCELL_INSET_X];
    contentRect.size.width -= 2 * [CMainView MAINTABCELL_INSET_X];

    int iDYRect = CGRectGetHeight(contentRect);
    int iDXIcon = iDYRect;
    
    CGRect rGradient = contentRect;
    //DL
    //rGradient.origin.y += 20;
    
    rGradient.origin.y += 2;
    rGradient.size.height -= 4;
    m_pGradientView1.frame = rGradient;
    m_pGradientView2.frame = rGradient;

    int iDXIconSpacing = iDXIcon + 2 * CONTENTS_INSET + 35;
	
#ifdef PENTAIR
    iDXIcon = MIN(iDXIcon, 64);
    iDXIconSpacing = iDXIcon + 2 * CONTENTS_INSET;
    
#endif

    CGRect frameIcon = CGRectMake(contentRect.origin.x + CONTENTS_INSET, iDYRect/2-iDXIcon/2, iDXIcon, iDXIcon);
    //DL
    //frameIcon.origin.y += 20;
    
    CGRect frameText = CGRectMake(contentRect.origin.x + iDXIconSpacing, 0, contentRect.size.width-iDXIconSpacing-CONTENTS_INSET, iDYRect);
    
    m_pIcon.frame = frameIcon;
	m_pLabel.frame = frameText;
    
    if (m_pAccessoryView != NULL)
    {
        CGRect contentRect = [self.contentView bounds];
        //DL
        //contentRect.origin.y += 20;
        CGRect rAccesssory = [CRect BreakOffRight:&contentRect DX:DX_ACCESSORY];
        //DL
        //rAccesssory.origin.y += 20;
        [m_pAccessoryView setFrame:rAccesssory];
    }
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pGradientView1 release];
    [m_pGradientView2 release];
	[m_pIcon release];
	[m_pLabel release];
    [super dealloc];
}

/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated

/*============================================================================*/
{
	[super setSelected:selected animated:animated];
   
	// when the selected state changes, set the highlighted state of the lables accordingly
	m_pLabel.highlighted = selected;
    
  	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    
    if (selected)
    {
        [UIView setAnimationDuration:0.1];
        [m_pGradientView2 setAlpha:1.0];
    }
    else
    {
        [UIView setAnimationDuration:1.0];
        [m_pGradientView2 setAlpha:0.0];
    }
    
    //DL - A
    [UIView commitAnimations];

    //[super setSelected:selected animated:animated];

}
/*============================================================================*/

-(void)SetMainTab:(CMainTab*)pTab

/*============================================================================*/
{
    if (m_pMainTab == pTab)
        return;
    [m_pMainTab release];
    m_pMainTab = pTab;
    [m_pMainTab retain];
    m_pLabel.text = pTab.m_pName;

    UIImage *pImage = [UIImage imageNamed:[pTab ImageName]];
    [m_pIcon SetImage:pImage];
}
/*============================================================================*/

-(void)SetHighlighted:(BOOL)bHighlighted

/*============================================================================*/
{
    //DL - A
    float fAlpha = 0.0;
    if (bHighlighted)
        fAlpha = 1.0;

	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.25];
    [m_pGradientView2 setAlpha:fAlpha];
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)SetAccessoryView:(UIView*)pView

/*============================================================================*/
{
    if (m_pAccessoryView != NULL)
    {
        [m_pAccessoryView removeFromSuperview];
        m_pAccessoryView = NULL;
    }

    if (pView == NULL)
        return;

    [self.contentView addSubview:pView];
    m_pAccessoryView = pView;
}
@end

