//
//  MediaDigitKeypad.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/15/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlview.h"

@class CHLButton;

/*============================================================================*/

@interface CMediaDigitKeypad : UIView < CHLCommandResponder >

/*============================================================================*/
{
    int                                 m_iData;
    int                                 m_iZoneID;
    CHLButton                           *m_pBtns[12];
}

-(id)initWithFrame: (CGRect)rView
                    Style:(int)iStyle
                    TextColor:(UIColor*)pRGBText
                    Color:(UIColor*)pRGBFace
                    TextSize:(int)iTextSize
                    Data:(int)iData
                    ZoneID:(int)iZoneID;

@end
