//
//  PoolMainPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/19/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolMainPage.h"
#import "RoundRegion.h"
#import "hlsegment.h"
#import "hlcomm.h"
#import "hlm.h"
#import "crect.h"
#import "MainView.h"
#import "NSPoolConfig.h"
#import "PoolSpaPage.h"
#import "PoolFeaturePage.h"
#import "PoolLightsMainView.h"
#import "IntelliChemPage.h"
#import "IntelliChlorPage.h"

@implementation CPoolMainPage
/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pConfig = pConfig;

        // Initialization code
        NSMutableArray *pItems = [[[NSMutableArray alloc] init] autorelease];

        BOOL bPool = ([pConfig NCircuitsByInterface:POOLINT_POOL] > 0);
        BOOL bSpa  = ([pConfig NCircuitsByInterface:POOLINT_SPA] > 0);
        NSString *pPool = [m_pConfig PoolText];
        NSString *pSpa  = [m_pConfig SpaText];

        pPool = [pPool uppercaseString];
        pSpa  = [pSpa  uppercaseString];

        m_iNSubPages = 0;

        if (bPool && bSpa)
        {
            NSString *pItem = [NSString stringWithFormat:@"%s/%s", [pPool UTF8String], [pSpa UTF8String]];
            [pItems addObject:pItem];
            m_iSubPages[m_iNSubPages] = SUBPAGE_POOLSPA; m_iNSubPages++;
        }
        else if (bPool)
        {
            [pItems addObject:pPool];
            m_iSubPages[m_iNSubPages] = SUBPAGE_POOLSPA; m_iNSubPages++;
        }
        else if (bSpa)
        {
            [pItems addObject:pSpa];
            m_iSubPages[m_iNSubPages] = SUBPAGE_POOLSPA; m_iNSubPages++;
        }

        if ([pConfig NCircuitsByInterface:POOLINT_GENERAL] > 0)
        {
            [pItems addObject:@"FEATURES"];
            m_iSubPages[m_iNSubPages] = SUBPAGE_FEATURES; m_iNSubPages++;
        }

        [pItems addObject:@"LIGHTS"];
        m_iSubPages[m_iNSubPages] = SUBPAGE_LIGHTS; m_iNSubPages++;


        if ([m_pConfig EquipPresent:POOL_ICHEMPRESENT])
        {
            [pItems addObject:@"INTELLICHEM"];
            m_iSubPages[m_iNSubPages] = SUBPAGE_INTELLICHEM; m_iNSubPages++;
        }
        
        //DL do it independently from IntelliChlor
        if([m_pConfig EquipPresent:POOL_CHLORPRESENT] && ![m_pConfig EquipPresent:POOL_ICHEMPRESENT])
        {
            [pItems addObject:@"INTELLICHLOR"];
            m_iSubPages[m_iNSubPages] = SUBPAGE_CHLORINATOR; m_iNSubPages++;
        }

        [self InitPageWithStrings:pItems];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(UIView*)CreatePageForIndex:(int)iIndex

/*============================================================================*/
{
    UIView *pNewPage = NULL;
    int iSubPageType = m_iSubPages[iIndex];
    switch (iSubPageType)
    {
    case SUBPAGE_POOLSPA:
        {
            pNewPage = [[CPoolSpaPage alloc] initWithFrame:CGRectZero Config:m_pConfig];
        }
        break;

    case SUBPAGE_FEATURES:
        {
            pNewPage = [[CPoolFeaturePage alloc] initWithFrame:CGRectZero MainTabPage:NULL Config:m_pConfig];
        }
        break;

    case SUBPAGE_LIGHTS:
        {
            pNewPage = [[CPoolLightsMainView alloc] initWithFrame:CGRectZero Config:m_pConfig];
        }
        break;

    case SUBPAGE_INTELLICHEM:
        {
            pNewPage = [[CIntelliChemPage alloc] initWithFrame:CGRectZero MainTabPage:NULL Config:m_pConfig];
        }
        break;
            
    case SUBPAGE_CHLORINATOR:
        {
            if(![m_pConfig EquipPresent:POOL_ICHEMPRESENT])
            {
                pNewPage = [[CIntelliChlorPage alloc] initWithFrame:CGRectZero MainTabPage:NULL Config:m_pConfig IsActive:true];
            }
            else
            {
                pNewPage = [[CIntelliChlorPage alloc] initWithFrame:CGRectZero MainTabPage:NULL Config:m_pConfig IsActive:false];
            }
        }
        break;

    }

    return pNewPage;
}


@end
