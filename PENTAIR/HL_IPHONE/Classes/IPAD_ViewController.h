//
//  IPAD_ViewController.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/20/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "Sink.h"

@class CConnectPage;
@class CHLToolBar;
@class CMainTab;
@class CSubSystemPage;
@class CZonePageZoomDelegate;
@class CSubTabViewOverlay;
@class CForecast;
@class CToolBarClientView;
@class IPAD_ViewController;

#define LAYOUT_DEFAULT                  0
#define LAYOUT_LARGESYSBAR_TOP          1
#define LAYOUT_LARGESYSBAR_TOP_OFFSET   2
#define LAYOUT_SIDESYSBAR               3
#define LAYOUT_SIDESYSBAR_OFFSET        4

/*============================================================================*/

@interface CIPADMainView : UIView

/*============================================================================*/
{
    UIView                          *m_pToolBar;
    CSubSystemPage                  *m_pContent;
    IPAD_ViewController             *m_pController;
}

-(void)SetController:(IPAD_ViewController*)pController;
-(void)SetToolBar:(UIView*)pView;
-(void)SetContent:(CSubSystemPage*)pView;
-(void)DoLayout;

@end

/*============================================================================*/

@interface IPAD_ViewController : UIViewController         < CNSSocketSink,
                                                            CSink  >

/*============================================================================*/
{
    BOOL                            m_bHideNavBar;
    int                             m_iMainTabIndex;
    NSTimer                         *m_pFlipTimer;
    NSTimer                         *m_pTimer;
    BOOL                            m_bInit;

    UIAlertView                     *m_pLockView;
	CConnectPage                    *m_pConnectPage;

	NSMutableArray                  *m_pMainTabs;
    NSString                        *m_pSysName;
    NSString                        *m_pNickName;

    UIImageView                     *m_pBackground;

    CHLToolBar                      *m_pToolBar;

    CSubSystemPage                   *m_pContentPage;
    CSubSystemPage                   *_m_pContentPage;
    
    CZonePageZoomDelegate           *m_pZoomDelegate;
    
    CSubTabViewOverlay              *_m_pOverlay;

}

-(void)ShowConnectPage;
-(void)HideConnectPage;

-(void)SetBackgroundImage:(int)iImage;

-(void)LoadSystemToolBarForAnimation:(BOOL)bAnimate;
-(void)SetActiveMainTab:(CMainTab*)pMainTab CursorDirection:(int)iCursorDirection;
-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;

-(void)SetFlipTimer:(int)iMSEC;
-(void)CancelFlipTimer;
-(void)RestoreSubSysMenu;
-(void)ShowMainMenu:(id)sender;
-(void)PageLeft:(id)sender;
-(void)PageRight:(id)sender;
-(void)ShiftPage:(int)iDir;
-(void)ShiftMainTabTo:(int)iNewIndex;
-(void)ZoomToSubPageView:(id)sender;
+(IPAD_ViewController*)MainViewController;
-(int)GetMenuFlags;
-(int)Layout;

-(void)HideNavBar:(BOOL)bHide;
-(BOOL)HideNavBar;
-(void)ShowConnectPage2;

@end
