//
//  LightDimmerControl.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/27/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "LightDimmerControl.h"
#import "LightControl.h"
#import "hlbutton.h"
#import "HLSlider.h"
#import "crect.h"
#import "hlm.h"
#import "hlcomm.h"

/*============================================================================*/

@implementation CLightDimmerControl

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Style:(int)iStyle Control:(CLightControl*)pControl;

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_iStyle = iStyle;
        m_pControl = pControl;
        
        m_pOff = [[CHLButton alloc] initWithFrame:CGRectZero Text:NULL];
        m_pOn = [[CHLButton alloc] initWithFrame:CGRectZero Text:NULL];
        BOOL bVertical = FALSE;

        switch (m_iStyle)
        {
        case DIMMER_STYLE_VT_BULB:
        case DIMMER_STYLE_VT_ARROW:
            {
                [m_pOff SetIconEmbedded:@"DOWN"];
                [m_pOn SetIconEmbedded:@"UP"];
                bVertical = TRUE;
            }
            break;
            
        default:
            {
                [m_pOff SetIconEmbedded:@"LEFT"];
                [m_pOn SetIconEmbedded:@"RIGHT"];
            }
            break;

        }

        m_pSlider = [[CHLSlider alloc] initWithFrame:CGRectZero Vertical:bVertical];
        [self addSubview:m_pOn];
        [self addSubview:m_pOff];
        [self addSubview:m_pSlider];
        [m_pOn release];
        [m_pOff release];
        [m_pSlider release];

        [m_pOn addTarget:m_pControl action:@selector(DeviceOn:) forControlEvents: UIControlEventTouchDown];
        [m_pOff addTarget:m_pControl action:@selector(DeviceOff:) forControlEvents: UIControlEventTouchDown];
        [m_pSlider addTarget:self action:@selector(SliderChanged:) forControlEvents: UIControlEventValueChanged];
        [m_pSlider addTarget:self action:@selector(SliderRelease:) forControlEvents: UIControlEventTouchUpInside];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iDXBtnMax = 50;
    int iDYBtnMax = 50;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iDXBtnMax = 110;
        iDYBtnMax = 80;
    }

    CGRect rOff = CGRectZero;
    CGRect rOn = CGRectZero;


    switch (m_iStyle)
    {
    case DIMMER_STYLE_VT_BULB:
    case DIMMER_STYLE_VT_ARROW:
        {
            int iDYBtn = rBounds.size.width;
            iDYBtn = MIN(iDYBtn, rBounds.size.height / 4);
            iDYBtn = MIN(iDYBtn, iDYBtnMax);
            rOff = [CRect BreakOffBottom:&rBounds DY:iDYBtn];
            rOn = [CRect BreakOffTop:&rBounds DY:iDYBtn];
        }
        break;
        
    default:
        {
            int iDXBtn = rBounds.size.height;
            iDXBtn = MIN(iDXBtn, rBounds.size.width / 4);
            iDXBtn = MIN(iDXBtn, iDXBtnMax);
            rOff = [CRect BreakOffLeft:&rBounds DX:iDXBtn];
            rOn = [CRect BreakOffRight:&rBounds DX:iDXBtn];
        }
        break;

    }

    [m_pOn setFrame:rOn];
    [m_pOff setFrame:rOff];
    [m_pSlider setFrame:rBounds];
}
/*============================================================================*/

-(void)NotifyState:(int)iState

/*============================================================================*/
{
    if (iState == STATE_OFF || iState == STATE_DIM_START)
    {
        [m_pOn SetChecked:FALSE];
        [m_pOff SetChecked:TRUE];
        [m_pSlider SetPositionPCT:0];
        return;
    }

    [m_pOn SetChecked:TRUE];
    [m_pOff SetChecked:FALSE];

    int iPCT = 100;
    if (iState >= STATE_DIM_START && iState <= STATE_DIM_END)
    {
        iPCT = iState - STATE_DIM_START;
    }

    if ([m_pSlider HasCapture])
        return;

    [m_pSlider SetPositionPCT:iPCT];
}
/*============================================================================*/

-(void)SliderChanged:(id)sender

/*============================================================================*/
{
    if (sender != m_pSlider)
        return;

    if (m_pTimer != NULL)
    {
        [m_pTimer invalidate];
        [m_pTimer release];
    }

    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(UpdateFromSlider) userInfo:nil repeats:NO] retain];
}
/*============================================================================*/

-(void)UpdateFromSlider

/*============================================================================*/
{
    if (m_pTimer != NULL)
    {
        [m_pTimer invalidate];
        [m_pTimer release];
        m_pTimer = NULL;
    }

    int iPCT = [m_pSlider GetPositionPCT];
    
    [m_pOn SetChecked:iPCT > 0];
    [m_pOff SetChecked:iPCT <= 0];

    [m_pControl DeviceControl:ACTION_DIM_START+iPCT];
}
/*============================================================================*/

-(void)SliderRelease:(id)sender

/*============================================================================*/
{
    [self UpdateFromSlider];
}

@end
