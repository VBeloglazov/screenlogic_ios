//
//  RoundRegion.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlsegment.h"

@class CTableTitleHeaderView;
@class CToolBarHighlight;

#define RECT_FULL           0
#define RECT_STDINSET       1

#define FRAMESTYLE_DEFAULT      0
#define FRAMESTYLE_UPPER        1
#define FRAMESTYLE_LOWER        2
#define FRAMESTYLE_LEFT         3
#define FRAMESTYLE_RIGHT        4

#define RADIUSSTYLE_DEFAULT     0
#define RADIUSSTYLE_NORADIUS    1

#define CONTENT_MODE_DEFAULT                0
#define CONTENT_MODE_FULLWIDTH              1
#define CONTENT_MODE_FULLHEIGHT_FULLWIDTH   2

#define RREGION_FLAG_BASELEVEL              0x0000001

/*============================================================================*/

@interface CRoundRegion : UIView < CHLSegmentImageProvider >

/*============================================================================*/
{
    int                     m_iFlags;
    int                     m_iFrameStyle;
    int                     m_iContentMode;
    int                     m_iRad;
    
    UIView                  *m_pClientView;
    UIView                  *_m_pClientView;

    CToolBarHighlight       *m_pHighlight;
    id                      m_pModifier;
}

-(id)initWithFrame:(CGRect)rFrame FrameStyle:(int)iFrameStyle RadiusStyle:(int)iRadiusStyle;
-(void)SetClientView:(UIView*)pView;
-(void)SetClientViewAnimated:(UIView*)pView Direction:(int)iDir;
-(CGRect)ClientRect:(int)iType;
-(UIView*)ClientView;
-(int)FrameStyle;
-(void)FrameStyle:(int)iNew;
-(int)ContentMode;
-(void)ContentMode:(int)iNew;
-(int)Radius;
-(void)HasHighlight:(BOOL)bHighlight;
-(CGImageRef)GenerateImage:(int)iType;
-(void)Modifier:(id)pModifier;
-(id)Modifier;
-(int)Flags;
-(void)Flags:(int)iNew;

@end


/*============================================================================*/

@interface CRoundRegionSeparator : UIView

/*============================================================================*/
{
    int                             m_iPositionPCT;
}

-(id)initWithFrame:(CGRect)frame;
-(CGImageRef)GenerateImage;
-(void)SetPosition:(int)iPosition;

@end
