/*
--------------------------------------------------------------------------------

    HLCOMM.MM

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"
#import "hlcomm.h"

#include "hlserver.h"
#include "setserver.h"
#include "hlsocket.h"
#include "hlmsg.h"
#include "hlquery.h"
#include "hlsock.h"
#include "ConnectionGDiscoveryMgr.h"
#include "ConnectionGatewayMgr.h"
#include "ServerDataRetriever.h"
#include "SocketHL.h"
#include "hlwav.h"
#include "platform.h"
#import "NSStatusSink.h"
#import "NSSocketSink.h"
#import "NSMSG.h"
#import <SystemConfiguration/SCNetworkReachability.h>


@implementation CHLComm


    static int                      m_iVersionMajor = 0;
    static int                      m_iVersionMinor = 0;

/*============================================================================*/

- (void) InitComm

/*============================================================================*/
{
#ifdef PENTAIR
    g_iConnectionType = CONNECTION_TYPE_IOS;
#else
    g_iConnectionType = CONNECTION_TYPE_IPHONE;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        g_iConnectionType = CONNECTION_TYPE_IPAD;
#endif
    
    g_pHLServer = new CHLServer();
    g_pSetServer = new CSetServer();
    
//    g_pHLServer->SetGatewayName((char*)"cd-hc8");
//    g_pHLServer->SetGatewayName((char*)"cd-hc12");
//    g_pHLServer->SetGatewayName((char*)"EV-HC4");
//    g_pHLServer->SetGatewayName((char*)"ev-hc6");
//    g_pHLServer->SetGatewayName((char*)"ANDY");
//    g_pHLServer->SetGatewayName((char*)"ANDY HC-6");
//    g_pHLServer->SetGatewayName((char*)"HC6B");
//    g_pHLServer->SetGatewayName((char*)"Jack");
//    g_pHLServer->SetGatewayName((char*)"Jack HC6");
//    g_pHLServer->SetGatewayName((char*)"BensHC12");
//    g_pHLServer->SetGatewayName((char*)"ELAN RACK");
//    g_pHLServer->SetGatewayName((char*)"PoolBrick 01-23");

    UIDevice *pDevice = [UIDevice currentDevice];
//    NSString *pName = [NSString stringWithFormat:@"Andy's Simulator"];

    NSString *pName = [pDevice name];
    NSString *pOSVer = [pDevice systemVersion];

    CHLString sName;
    CHLString sOSVer;

    [CNSMSG MakeCHLString:&sName From:pName];
    [CNSMSG MakeCHLString:&sOSVer From:pOSVer];
    
    g_pHLServer->MachineName(sName);
    g_pHLServer->OSVersion(sOSVer);
    
    m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:0.04 target:self selector:@selector(TimerEvent) userInfo:nil repeats:YES] retain];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate:) name:nil object:nil];
}
/*============================================================================*/

+ (BOOL)IsInit

/*============================================================================*/
{
    return (g_pHLServer != NULL);
}
/*============================================================================*/

- (void) ShutDownComm

/*============================================================================*/
{
    [m_pTimer invalidate];

    delete g_pHLServer;
    g_pHLServer = NULL;
}
/*============================================================================*/

-(void) TimerEvent

/*============================================================================*/
{
    if (g_pHLServer == NULL)
        return;
    if (g_pHLServer->SuspendIO())
        return;

      
    int iRes = RECEIVE_BUSY;
    CHLTimer tm;
    while ((iRes == RECEIVE_BUSY || iRes == RECEIVE_MSGREADY) && tm.HLTickCount() < 1)
    {
        iRes = g_pHLServer->PumpSocketMessages();
    }
}
/*============================================================================*/

- (void) applicationWillTerminate:(NSNotification*)aNotification

/*============================================================================*/
{
    if ([aNotification.name isEqualToString:@"_UIApplicationWillSuspendNotification"])
    {
        g_pHLServer->Socket()->Disconnect();
    }
}

/*============================================================================*/

+ (void) Disconnect

/*============================================================================*/
{

    NSLog(@"Disconnecting...");
    g_pHLServer->Socket()->Disconnect();
}
/*============================================================================*/

+ (BOOL) Connected

/*============================================================================*/
{
    if (g_pHLServer == NULL)
        return FALSE;
    return g_pHLServer->IsConnected();
}
/*============================================================================*/

+ (BOOL) IsConnecting

/*============================================================================*/
{
    if (g_pHLServer == NULL)
        return FALSE;
    return g_pHLServer->LoggingIn();
}
/*============================================================================*/

+ (BOOL) CreateRemoteConnection:(NSObject*)pSink Name:(const char*)pUserName Password:(const char*)pPassword

/*============================================================================*/
{
#ifdef PENTAIR
    int iType = CONNECTION_TYPE_IOS;
#else
   int iType = CONNECTION_TYPE_IPHONE;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iType = CONNECTION_TYPE_IPAD;

    SCNetworkReachabilityRef pRef =  SCNetworkReachabilityCreateWithName (NULL, "homelogic.com");    
    SCNetworkReachabilityFlags reachabilityFlags;
    BOOL gotFlags = SCNetworkReachabilityGetFlags(pRef, &reachabilityFlags);
    if (gotFlags)
    {
        if (reachabilityFlags & kSCNetworkReachabilityFlagsIsWWAN)
        {
            iType = CONNECTION_TYPE_IPHONE_3G;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                iType = CONNECTION_TYPE_IPAD_3G;
        }
    }
#endif

    CNSStatusSink *pStatusSink = NULL;
    if ([pSink conformsToProtocol:@protocol(CNSStatusSink)])
    {
        pStatusSink = (CNSStatusSink*)pSink;
    }

    g_pHLServer->LoggingIn(TRUE);

    CStatusSink sink(pStatusSink);

    BOOL bReturn = FALSE;
    if (bReturn == FALSE) bReturn = [CHLComm ConnectViaDiscoveryService:iType Server:0 Name:pUserName Password:pPassword Sink:&sink];
    if (bReturn == FALSE) bReturn = [CHLComm ConnectViaDiscoveryService:iType Server:1 Name:pUserName Password:pPassword Sink:&sink];
    if (bReturn == FALSE) bReturn = g_pHLServer->CreateCrystalpadConnection(pUserName, pPassword, &sink, iType);


    if (bReturn)
    {
        CHLMSG msgQ(0, HLM_SYSCONFIG_GETUSERINFOQ);
        CHLQuery q;
        CHLMSG *pMSGA = NULL;
        if (q.AskQuestion(&pMSGA, &msgQ))
        {
            int iJunk = 0;
            pMSGA->GetInt(&iJunk);
            pMSGA->GetInt(&iJunk);
            CHLString sSysName;
            pMSGA->GetString(&sSysName);
            if (sSysName.Compare(pUserName))
            {
                g_pHLServer->Socket()->Disconnect();
                return FALSE;
            }
        }
    }
    

    g_pHLServer->LoggingIn(FALSE);

    return bReturn;
}
/*============================================================================*/

+ (BOOL) ConnectViaDiscoveryService:(int)iConType Server:(int)iServer Name:(const char*)pUserName Password:(const char*)pPassword Sink:(CStatusSink*)pSink

/*============================================================================*/
{
    //
    // Get Serversoft Dispatcher IP:Port
    //
    CHLString sServersoftAddress;
    WORD  wServersoftPort;

    if ( ![CHLComm GetServersoftData:iServer Address:&sServersoftAddress Port:&wServersoftPort] )
    {
        return FALSE;
    }

    CHLString sTrace;
    sTrace.Format( _HLTEXT("Discovery Service: %s:%d"),
        (const HLTCHAR*) sServersoftAddress,
        wServersoftPort
    );
    pSink->SetStatus(sTrace);

    BYTE  pServersoftAddress[4];

    int iLen = sServersoftAddress.GetLength();

    if ( !ParseAddress(
        sServersoftAddress.GetBufferCHAR(iLen), "",
        pServersoftAddress, pServersoftAddress+1, pServersoftAddress+2, pServersoftAddress+3,
        NULL ) )
    {
        return FALSE;
    }

    //
    // Connect to the Discovery Service, pass authorization,
    // get the GATEWAY's data needed to connect to the GATEWAY
    //
    if ( !CConnectionGDiscoveryMgr::GetGatewayData(
            CHLString(pUserName),
            CHLString(pUserName),
            pServersoftAddress,
            wServersoftPort,
            pSink ) )
    {
        // This call will close the socket
        CConnectionGDiscoveryMgr::Dispose();

        return FALSE;
    }

    CHLString sGatewayAddress     ( CConnectionGDiscoveryMgr::GatewayAddress()      );
    WORD      wGatewayPort        ( CConnectionGDiscoveryMgr::GatewayPort()         );
    BOOL      bRelayOn            ( CConnectionGDiscoveryMgr::RelayOn()             );

    // This call will close the socket
    CConnectionGDiscoveryMgr::Dispose();

    //
    // Create socket, connect to the GATEWAY, log in
    //
    if ( !CConnectionGatewayMgr::GetGatewayData(
            CHLString(pUserName),
            CHLString(pPassword),
            sGatewayAddress,
            wGatewayPort,
            bRelayOn,
            iConType,
            pSink ) )
    {
        // This call will close the socket.
        CConnectionGatewayMgr::Dispose(TRUE);

        return FALSE;
    }

    /* If we needed them...
    CHLString sGatewayVersion = CConnectionGatewayMgr::GatewayVersion();
    int       iSystemType     = CConnectionGatewayMgr::SystemType();
    */

    g_pHLServer->Socket()->Socket( CConnectionGatewayMgr::Socket()->Socket() );
    g_pHLServer->SetConnectionChanged();

    // This call will NOT close the socket.
    // g_pHLServer is responsible now.
    CConnectionGatewayMgr::Dispose();

    return TRUE;
}
/*============================================================================*/

+ (BOOL) GetServersoftData:(int)iServer Address:(CHLString*)psAddress Port:(unsigned short*)pwPort

/*============================================================================*/
{
	BOOL bRet(FALSE);

    CServerDataRetriever dataServersoft(iServer);
    if ( dataServersoft.IsValid() )
    {
        *psAddress = dataServersoft.AddressString();
        *pwPort    = dataServersoft.Port();

        bRet = TRUE;
    }
    else
    {
        *psAddress = CHLString( SERVER_DISPATCHER_DEFAULT_ADDRESS );
        *pwPort    = SERVER_DISPATCHER_DEFAULT_PORT;

        bRet = TRUE;
    }

	return bRet;
}
/*============================================================================*/

+ (void) AddSocketSink:(id)pSink

/*============================================================================*/
{
    if (![pSink conformsToProtocol:@protocol(CNSSocketSink)])
        return;
    CSocketSink *pHLSink = new CSocketSink(pSink);
    g_pHLServer->AddSocketSink(pHLSink);
}
/*============================================================================*/

+ (void) RemoveSocketSink:(id)pSink

/*============================================================================*/
{
    if (![pSink conformsToProtocol:@protocol(CNSSocketSink)])
        return;
    CSocketSink *pHLSink = g_pHLServer->FindSink(pSink);
    if (pHLSink == NULL)
        return;
    g_pHLServer->RemoveSocketSink(pHLSink);
    delete pHLSink;
}
/*============================================================================*/

+ (void) RemoveAllSocketSinks

/*============================================================================*/
{
    g_pHLServer->RemoveAllSocketSinks();
}

/*============================================================================*/

+ (int) SenderID:(id)pSink

/*============================================================================*/
{
    if (![pSink conformsToProtocol:@protocol(CNSSocketSink)])
        return 0;
    CSocketSink *pHLSink = g_pHLServer->FindSink(pSink);
    if (pHLSink == NULL)
        return 0;
    return pHLSink->SenderID();
}
/*============================================================================*/

+ (void) SuspendIO:(BOOL)bSuspend

/*============================================================================*/
{
    if (g_pHLServer != NULL)
        g_pHLServer->SuspendIO(bSuspend);
}
/*============================================================================*/

+ (void) RunIO:(int)iMSEC

/*============================================================================*/
{
    int iRes = RECEIVE_BUSY;
    CHLTimer tm;
    while (iRes == RECEIVE_BUSY)
    {
        iRes = g_pHLServer->PumpSocketMessages();
        if (tm.HLTickCount() > iMSEC)
            return;
    }
}
/*============================================================================*/

+ (BOOL) WirelessOK

/*============================================================================*/
{
    return g_pHLServer->Socket()->WirelessOK();
}
/*============================================================================*/

+ (void) SetVersion:(int)iMajor Sub:(int)iMinor

/*============================================================================*/
{
    m_iVersionMajor = iMajor;
    m_iVersionMinor = iMinor;
}
/*============================================================================*/

+ (int) VersionMajor;

/*============================================================================*/
{
    return m_iVersionMajor;
}
/*============================================================================*/

+ (int) VersionMinor

/*============================================================================*/
{
    return m_iVersionMinor;
}
/*============================================================================*/

+ (BOOL) LoadSettings

/*============================================================================*/
{
    g_pSetServer->LoadSettings();
    return TRUE;
}
/*============================================================================*/

+ (void) LoadGradientTo:(CGFloat*)pArray G1:(int)iIndex1 G2:(int)iIndex2

/*============================================================================*/
{
    COLORREF rgb1 = g_pSetServer->GetRGB(iIndex1);
    COLORREF rgb2 = g_pSetServer->GetRGB(iIndex2);
    pArray[0] = (float)(GetRValue(rgb1)) / 255.f;
    pArray[1] = (float)(GetGValue(rgb1)) / 255.f;
    pArray[2] = (float)(GetBValue(rgb1)) / 255.f;
    pArray[3] = 1.f;
    pArray[4] = (float)(GetRValue(rgb2)) / 255.f;
    pArray[5] = (float)(GetGValue(rgb2)) / 255.f;
    pArray[6] = (float)(GetBValue(rgb2)) / 255.f;
    pArray[7] = 1.f;
}
/*============================================================================*/

+ (UIColor*)GetRGB:(int)iIndex

/*============================================================================*/
{
    COLORREF rgb = g_pSetServer->GetRGB(iIndex);
    float fR = (float)(GetRValue(rgb)) / 255.f;
    float fG = (float)(GetGValue(rgb)) / 255.f;
    float fB = (float)(GetBValue(rgb)) / 255.f;

    UIColor *pRet = [UIColor colorWithRed:fR green:fG blue:fB alpha:1.0];
    return pRet;
}
/*============================================================================*/

+ (UIColor*)GetRGB:(int)iIndex Adjust:(float)fAdjust

/*============================================================================*/
{
    COLORREF rgb = g_pSetServer->GetRGB(iIndex);
    float fR = (float)(GetRValue(rgb)) / 255.f * fAdjust;
    float fG = (float)(GetGValue(rgb)) / 255.f * fAdjust;
    float fB = (float)(GetBValue(rgb)) / 255.f * fAdjust;

    UIColor *pRet = [UIColor colorWithRed:fR green:fG blue:fB alpha:1.0];
    return pRet;
}
/*============================================================================*/

+ (void) GetRGB:(int)iIndex To:(float*)pResult

/*============================================================================*/
{
    COLORREF rgb = g_pSetServer->GetRGB(iIndex);
    float fR = (float)(GetRValue(rgb)) / 255.f;
    float fG = (float)(GetGValue(rgb)) / 255.f;
    float fB = (float)(GetBValue(rgb)) / 255.f;
    *(pResult+0) = fR;
    *(pResult+1) = fG;
    *(pResult+2) = fB;
    *(pResult+3) = 1.0;
}
/*============================================================================*/

+ (unsigned int)GetRGBWin32:(int)iIndex

/*============================================================================*/
{
    if (g_pSetServer == NULL)
        return 0;

    COLORREF rgb = g_pSetServer->GetRGB(iIndex);
    return rgb;
}
/*============================================================================*/

+ (int)GetInt:(int)iIndex

/*============================================================================*/
{
    if (g_pSetServer == NULL)
        return 0;

#ifdef PENTAIR
    if (iIndex == INT_BUTTON_GRADIENT)
        return 0;
#endif


    int iRet = g_pSetServer->GetInt(iIndex);


    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        switch (iIndex)
        {
        case INT_BUTTON_ROUT:
        case INT_BUTTON_SHADEIN:
        case INT_BUTTON_SHADEOUT:
        case INT_STD_INSET:
        case INT_PAGE_INSET:
        case INT_ALPHA_RMINOR:
        case INT_MAINTAB_ROUT:
            {
                return iRet / 2;
            }
            break;

        case INT_APP_TEXTHEIGHT:
        case INT_BUTTON_TEXTHEIGHT:
            {
                return iRet * 75 / 100;
            }
            break;

        }
    }

    return iRet;
}
/*============================================================================*/

+ (NSString*)TranslateString:(NSString*)pText Context:(NSString*)pContext

/*============================================================================*/
{
    return pText;
}
/*============================================================================*/

+ (AVAudioPlayer*)CreateButtonSoundPlayer

/*============================================================================*/
{
#ifdef PENTAIR
    return NULL;
#else
    CHLWAV *pWAV = g_pSetServer->ButtonWAV();
    if (pWAV == NULL)
        return NULL;
    if (!pWAV->DataOK())
        return NULL;
    NSData *pData = [NSData dataWithBytes:pWAV->Data() length:pWAV->NBytesData()];
    AVAudioPlayer *pPlayer = [[AVAudioPlayer alloc] initWithData:pData error:NULL];
    return pPlayer;
#endif
}
@end
