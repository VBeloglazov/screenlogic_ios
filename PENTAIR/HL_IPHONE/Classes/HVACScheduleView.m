//
//  HVACScheduleView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/20/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HVACScheduleView.h"
#import "tstat.h"
#import "hlbutton.h"
#import "hlm.h"
#import "MainView.h"

@implementation CHVACScheduleView

#define PAGE_INSET                      5

#define CMD_RUN                         1000
#define CMD_PHOLD                       1001
#define CMD_THOLD1                      1002
#define CMD_THOLD2                      1003
#define CMD_TIMEUP                      1004
#define CMD_TIMEDN                      1005

#define DX_INSET_IPAD                   250
#define DX_INSET_IPOD                   10

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat

/*============================================================================*/
{
    if (self = [super initWithFrame:rect])
    {
        self.autoresizingMask = 0xFFFFFFFF;

        m_pTStat = pTStat;
        [m_pTStat AddSink:self];

        int iDXThis = rect.size.width;
        int iDYThis = rect.size.height;
        int iDYBtn = iDYThis / 5;
        int iDXInset = DX_INSET_IPOD;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            iDXInset = DX_INSET_IPAD;

        for (int i = 0; i < 4; i++)
        {
            CGRect rBtn = CGRectMake(iDXInset, i * iDYBtn + PAGE_INSET, iDXThis-2*iDXInset, iDYBtn - 2 * PAGE_INSET);
            NSString *pText = NULL;
            switch (i)
            {
            case 0:     pText = @"Run Program";             break;
            case 1:     pText = @"Permanent Hold";          break;
            case 2:     pText = @"Hold Until Next Period";  break;
            case 3:     pText = @"Hold For Time...";        break;
            }

            CHLButton *pBtn = [[CHLButton alloc] initWithFrame:rBtn Text:pText];
            [pBtn SetCommandID:CMD_RUN+i Responder:self];
            pBtn.autoresizingMask = 0xFFFFFFFF;
            [self addSubview:pBtn];
            m_pModes[i] = pBtn;
        }

        CGRect rUp = CGRectMake(iDXThis-2*iDXInset, iDYBtn * 3 + PAGE_INSET, iDYBtn, iDYBtn-2*PAGE_INSET);
        CGRect rDn = rUp;
        rDn.origin.y += iDYBtn-PAGE_INSET;
        m_pUp = [[CHLButton alloc] initWithFrame:rUp IconFormat:HL_ICONCENTER Style:ICON_GRAYUP Text:NULL TextSize:20 TextColor:NULL Color:NULL Icon:NULL];
        m_pDn = [[CHLButton alloc] initWithFrame:rDn IconFormat:HL_ICONCENTER Style:ICON_GRAYDN Text:NULL TextSize:20 TextColor:NULL Color:NULL Icon:NULL];
        [m_pUp SetCommandID:CMD_TIMEUP Responder:self];
        [m_pDn SetCommandID:CMD_TIMEDN Responder:self];
        [m_pUp SetRepeatDwell:300 Rate:4];
        [m_pDn SetRepeatDwell:300 Rate:4];
        [m_pUp setAlpha:0.0];
        [m_pDn setAlpha:0.0];
        [m_pUp setUserInteractionEnabled:NO];
        [m_pDn setUserInteractionEnabled:NO];
        [self addSubview:m_pUp];
        [self addSubview:m_pDn];
        m_pUp.autoresizingMask = 0xFFFFFFFF;
        m_pDn.autoresizingMask = 0xFFFFFFFF;

        m_iLastProgramState = -1;

        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    for (int i = 0; i < 4; i++)
    {
        [m_pModes[i] release];
    }

    [m_pTStat RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iProgramState = [m_pTStat ProgramState];
    int iIndex = -1;
    switch (iProgramState)
    {
    case PRG_STATE_RUN:         iIndex = 0; break;
    case PRG_STATE_PHOLD:       iIndex = 1; break;
    case PRG_STATE_THOLDNEXT:   iIndex = 2; break;
    case PRG_STATE_THOLDTIME:   iIndex = 3; break;
    }

    if (iProgramState != m_iLastProgramState)
    {
        m_iLastProgramState = iProgramState;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.75];

        for (int i = 0; i < 4; i++)
        {
            [m_pModes[i] SetChecked:(i == iIndex)];
        }

        CGRect rBounds3 = m_pModes[2].frame;
        CGRect rBounds4 = m_pModes[3].frame;
        CGRect rUp = m_pUp.frame;
        CGRect rDn = m_pDn.frame;

        if (iProgramState == PRG_STATE_THOLDTIME)
        {
            rUp.origin.x = rBounds3.origin.x + rBounds3.size.width - m_pUp.bounds.size.width;
            rDn.origin.x = rUp.origin.x;

            rBounds4.size.width = rBounds3.size.width - (rUp.size.width + PAGE_INSET);

            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(HoldButtonFirstAnimationDidStop:finished:context:)];


            [m_pUp setAlpha:1.0];
            [m_pDn setAlpha:1.0];
            [m_pUp setUserInteractionEnabled:YES];
            [m_pDn setUserInteractionEnabled:YES];
            [self UpdateHoldTime];
        }
        else
        {
            rUp.origin.x = self.bounds.size.width;
            rDn.origin.x = self.bounds.size.width;
            rBounds4.size.width = rBounds3.size.width;
            rBounds4.size.height = rBounds3.size.height;
                    
            [m_pUp setAlpha:0.0];
            [m_pDn setAlpha:0.0];
            [m_pUp setUserInteractionEnabled:NO];
            [m_pDn setUserInteractionEnabled:NO];
            [m_pModes[3] SetText:@"Hold for Time ..."];
        }

        [m_pUp setFrame:rUp];
        [m_pDn setFrame:rDn];
        [m_pModes[3] setFrame:rBounds4];

        [UIView commitAnimations];
    }
    else
    {
       if (iProgramState == PRG_STATE_THOLDTIME)
        {
            [self UpdateHoldTime];

        }
        return;
    }

}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_RUN:      [m_pTStat SetProgramState:PRG_STATE_RUN]; break;
    case CMD_PHOLD:    [m_pTStat SetProgramState:PRG_STATE_PHOLD]; break;
    case CMD_THOLD1:   [m_pTStat SetProgramState:PRG_STATE_THOLDNEXT]; break;
    case CMD_THOLD2:   [m_pTStat SetProgramState:PRG_STATE_THOLDTIME]; break;


    case CMD_TIMEUP:
    case CMD_TIMEDN:
        {
            int iMinutesHold = (([m_pTStat HoldTimeMinutes] + 3) / 5) * 5;
            iMinutesHold = MAX(5, iMinutesHold);
            if (iCommandID == CMD_TIMEDN)
            {
                if (iMinutesHold <= 5)
                    return;
                iMinutesHold -= 5;
            }
            if (iCommandID == CMD_TIMEUP)
            {
                if (iMinutesHold >= 240)
                    return;
                iMinutesHold += 5;
            }

            [m_pTStat SetHoldTimeMinutes:iMinutesHold];
            [self UpdateHoldTime];
        }
        break;
    }
}
/*============================================================================*/

-(void)UpdateHoldTime

/*============================================================================*/
{
    int iMinutesHold = [m_pTStat HoldTimeMinutes];
    NSString *pTimeText = NULL;
    if (iMinutesHold < 1)
    {
        pTimeText = @"Less than a minute";
    }
    else if (iMinutesHold < 60)
    {
        pTimeText = [NSString stringWithFormat:@"%d Minutes", iMinutesHold];
    }
    else
    {
        int iHours = iMinutesHold / 60;
        int iMin = iMinutesHold - iHours * 60;
        pTimeText = [NSString stringWithFormat:@"%d:%02d", iHours, iMin];
    }

    [m_pModes[3] SetText:[ NSString stringWithFormat:@"Hold for %s", [pTimeText UTF8String]]];
}
/*============================================================================*/

-(void)HoldButtonFirstAnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rBounds3 = m_pModes[2].frame;
    CGRect rBounds4 = m_pModes[3].frame;
    rBounds4.size.height = 2*rBounds3.size.height + PAGE_INSET;
    [m_pModes[3] setFrame:rBounds4];
    [UIView commitAnimations];
}

@end
