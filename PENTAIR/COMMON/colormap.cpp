/*
--------------------------------------------------------------------------------

    COLORMAP.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"
#include "helpers.h"
#include "setserver.h"
#include "main.h"
#include "dibdc.h"

#include "colormap.h"

class CSetServer;

#ifdef HLOSD
    #define R_MASK              0x000000FF
    #define G_MASK              0x0000FF00
    #define B_MASK              0x00FF0000
    #define A_MASK              0xFF000000
#elif IPHONE
    #define R_MASK              0x0000FF00
    #define G_MASK              0x00FF0000
    #define B_MASK              0xFF000000
    #define A_MASK              0x000000FF
#else
    #define R_MASK              0xF800
    #define G_MASK              0x07E0
    #define B_MASK              0x001F
#endif

/*============================================================================*/

    CGradient1D::CGradient1D(
    COLORREF                    rgbT,
    COLORREF                    rgbB )

/*============================================================================*/
{
    m_iRT = GetRValue(rgbT);
    m_iGT = GetGValue(rgbT);
    m_iBT = GetBValue(rgbT);
    m_iRB = GetRValue(rgbB);
    m_iGB = GetGValue(rgbB);
    m_iBB = GetBValue(rgbB);
    m_iMode = MODE_LINEAR;
}
/*============================================================================*/

    HL_PIXEL                    CGradient1D::PIXELAt(
    int                         iY,
    int                         iYMax )

/*============================================================================*/
{
    int iY255 = 0;

    switch (m_iMode)
    {
    case MODE_LINEAR:
        iY255 = iY * 0xFF / iYMax;
        break;

    case MODE_PARA_UP:
        {
            int iRX = iYMax - iY;
            int iRY = iSqrt(iYMax*iYMax - iRX * iRX);
            iY255 = iRY * 255 / iYMax;
        }
        break;
    case MODE_PARA_DN:
        {
            int iRX = iY;
            int iRY = iSqrt(iYMax*iYMax - iRX * iRX);
            iY255 = 255-iRY * 255 / iYMax;
        }
        break;
    }
    

    int _iY255 = 0xFF - iY255;
    int iR = (m_iRT * _iY255 + m_iRB * iY255) >> 8;
    int iG = (m_iGT * _iY255 + m_iGB * iY255) >> 8;
    int iB = (m_iBT * _iY255 + m_iBB * iY255) >> 8;

    #ifdef IPHONE
        return (iR << 8)|(iG << 16)|(iB << 24)|0xFF;
    #else
        iR = iR * 31 / 255;
        iG = iG * 63 / 255;
        iB = iB * 31 / 255;

        return (HL_PIXEL)(iR << 11)|(iG << 5)|iB;
        
    #endif
}
/*============================================================================*/

    COLORREF                    CGradient1D::COLORREFAt(
    int                         iY,
    int                         iYMax )

/*============================================================================*/
{
    int iY255 = iY * 0xFF / iYMax;

    switch (m_iMode)
    {
    case MODE_LINEAR:
        iY255 = iY * 0xFF / iYMax;
        break;

    case MODE_PARA_UP:
        {
            int iRX = iYMax - iY;
            int iRY = iSqrt(iYMax*iYMax - iRX * iRX);
            iY255 = iRY * 255 / iYMax;
            LOGASSERT(iY255 <= 255);
        }
        break;
    case MODE_PARA_DN:
        {
            int iRX = iY;
            int iRY = iSqrt(iYMax*iYMax - iRX * iRX);
            iY255 = iRY * 255 / iYMax;
            iY255 = 255-iY255;    
            LOGASSERT(iY255 <= 255);
        }
        break;
    }

    int _iY255 = 0xFF - iY255;
    int iR = (m_iRT * _iY255 + m_iRB * iY255) >> 8;
    int iG = (m_iGT * _iY255 + m_iGB * iY255) >> 8;
    int iB = (m_iBT * _iY255 + m_iBB * iY255) >> 8;
    return RGB(iR,iG,iB);
}
/*============================================================================*/

    CGradient2D::CGradient2D()

/*============================================================================*/
{
    m_iNSetPoints   = 0;
    m_piSizes       = NULL;
    m_pDitherMap    = NULL;
    m_pRenderDIB    = NULL;
}
/*============================================================================*/

    CGradient2D::~CGradient2D()

/*============================================================================*/
{
    delete [] m_piSizes;
    delete [] m_pDitherMap;
    delete m_pRenderDIB;
    m_iNColors = 0;
}
/*============================================================================*/

    void                        CGradient2D::Set(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iDim )

/*============================================================================*/
{
    if (m_iNColors == iDim && m_rgb1 == rgb1 && m_rgb2 == rgb2 && m_iNSetPoints == 2)
        return;

    m_rgb1 = rgb1;
    m_rgb2 = rgb2;
    m_rgb3 = 0x0000;
    m_iNSetPoints = 2;
    Init(iDim);
}
/*============================================================================*/

    void                        CGradient2D::Set(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    COLORREF                    rgb3,
    int                         iDim )

/*============================================================================*/
{
    if (m_iNColors == iDim && m_rgb1 == rgb1 && m_rgb2 == rgb2 && m_rgb3 == rgb3 && m_iNSetPoints == 3)
        return;
    
    m_rgb1 = rgb1;
    m_rgb2 = rgb2;
    m_rgb3 = rgb3;
    m_iNSetPoints = 3;
    Init(iDim);
}
/*============================================================================*/

    void                        CGradient2D::BltTo(
    HL_PIXEL                    *pTgt,
    int                         iXSrc,
    int                         iYSrc,
    int                         iDX,
    int                         iDY,
    int                         iPitch,
    int                         iAlpha )

/*============================================================================*/
{
    iYSrc = MAX(0, iYSrc);
    iDY = MIN(iDY, m_iNColors - iYSrc);
    if (iDY <= 0)
        return;


    if (iAlpha >= 100)
    {
        //
        // Fully opaque
        //
        for (int iY = 0; iY < iDY; iY++)
        {
            HL_PIXEL *pThisTgt = pTgt;

            int iColorIndex = iY+iYSrc;
            HL_PIXEL *pSrc = m_pDitherMap + MAX_PATTERN*iColorIndex;
            int iNColorsInRow = m_piSizes[iColorIndex];

            if (iNColorsInRow == 1)
            {
                HL_PIXEL wSet = *pSrc;                
                int iN = iDX;
                while (iN > 0)
                {
                    *pThisTgt = wSet;
                    pThisTgt++;
                    iN--;
                }
            }
            else
            {
                int iStartOffset = iXSrc % iNColorsInRow;
                int iNCopyStart = MIN(iDX, iNColorsInRow - iStartOffset);
                memcpy(pThisTgt, pSrc + iStartOffset, iNCopyStart * sizeof(HL_PIXEL));
                pThisTgt += iNCopyStart;
                int iNDone = iNCopyStart;
                while (iNDone < iDX)    
                {
                    int iNCopy = MIN(iNColorsInRow, iDX-iNDone);
                    memcpy(pThisTgt, pSrc, iNCopy * sizeof(HL_PIXEL));
                    pThisTgt += iNCopy;
                    iNDone += iNCopy;
                }
            }

            pTgt += iPitch;
        }

        return;
    }

    int iA16 = iAlpha * 16 / 100;
    int _iA16 = 16-iA16;

    for (int iY = 0; iY < iDY; iY++)
    {
        HL_PIXEL *pThisTgt = pTgt;

        int iColorIndex = iY+iYSrc;
        int iNColorsInRow = m_piSizes[iColorIndex];

        if (iNColorsInRow == 1)
        {
            HL_PIXEL *pSrc = m_pDitherMap + MAX_PATTERN*iColorIndex;
            HL_PIXEL wSet = *pSrc;                
            DWORD dwRBS = wSet & 0xF81F;
            DWORD dwGS  = wSet & 0x07E0;

            int iN = iDX;
            while (iN > 0)
            {
                DWORD wTgt = *pThisTgt;
                DWORD dwRBT = wTgt & 0xF81F;
                DWORD dwGT  = wTgt & 0x07E0;
                DWORD wRBOut = (((dwRBS * iA16 + _iA16 * dwRBT) >> 4) & 0x0000F81F);
                DWORD wGOut  = (((dwGS  * iA16 + _iA16 * dwGT ) >> 4) & 0x000007E0);
                *pThisTgt = (HL_PIXEL)(wRBOut|wGOut);
                pThisTgt++;
                iN--;
            }
        }
        else
        {
            HL_PIXEL *pSrcRow = m_pDitherMap + MAX_PATTERN*iColorIndex;                
            HL_PIXEL *pSrc = pSrcRow;
            int iN = iDX;
            int iNSrc = iNColorsInRow;
            while (iN > 0)
            {
                DWORD wSet = *pSrc;                
                DWORD wTgt = *pThisTgt;

                DWORD dwRBS = wSet & 0xF81F;
                DWORD dwGS  = wSet & 0x07E0;

                DWORD dwRBT = wTgt & 0xF81F;
                DWORD dwGT  = wTgt & 0x07E0;

                DWORD wRBOut = (((dwRBS * iA16 + _iA16 * dwRBT) >> 4) & 0x0000F81F);
                DWORD wGOut  = (((dwGS  * iA16 + _iA16 * dwGT ) >> 4) & 0x000007E0);

                *pThisTgt = (HL_PIXEL)(wRBOut|wGOut);

                pThisTgt++;

                iN--;
                pSrc++;
                iNSrc--;

                if (iNSrc <= 0)
                {
                    pSrc = pSrcRow;
                    iNSrc = iNColorsInRow;
                }
            }
        }

        pTgt += iPitch;
    }

}
/*============================================================================*/

    BOOL                        CGradient2D::IsSolidColor()

/*============================================================================*/
{
    if (m_iNSetPoints == 2)
        return (m_rgb1 == m_rgb2);
    
    if (m_iNSetPoints == 3)
    {
        if (m_rgb1 != m_rgb2)
            return FALSE;
        if (m_rgb1 != m_rgb3)
            return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    CHLDibDC                    *CGradient2D::GetGradientRenderDIB()

/*============================================================================*/
{
    if (m_pRenderDIB != NULL)   
        return m_pRenderDIB;

    m_pRenderDIB = new CHLDibDC(256, m_iNColors, 0, 0, MEMORY_SYSTEM);
    m_pRenderDIB->BltFromGradient(this, 0, 0, 0, 0, 256, m_iNColors);
    m_pRenderDIB->DDConvertToVRAM();
    return m_pRenderDIB; 
}
/*============================================================================*/

    void                        CGradient2D::ReleaseRenderingResources()

/*============================================================================*/
{
    delete m_pRenderDIB;
    m_pRenderDIB = NULL;
}
/*============================================================================*/

    void                        CGradient2D::Init(
    int                         iDim )

/*============================================================================*/
{
    if (iDim != m_iNColors)
    {
        delete [] m_piSizes;
        delete [] m_pDitherMap;
        m_piSizes = NULL;
        m_pDitherMap = NULL;
    }

    if (iDim <= 0)
    {
        m_iNColors = 0;
        return;    
    }

    if (m_piSizes == NULL)
        m_piSizes = new int[iDim];
    if (m_pDitherMap == NULL)
        m_pDitherMap = new HL_PIXEL[MAX_PATTERN * iDim];

    m_iNColors = iDim;
    COLORREF sp2 = m_rgb2;  
    if (m_iNSetPoints == 3)
        sp2 = m_rgb3;

    CGradient1D gradient1(m_rgb1, m_rgb2);
    CGradient1D gradient2(m_rgb2, m_rgb3);
    int iDim_2 = iDim/2;

    if (m_iNSetPoints == 3)
    {
        gradient1.Mode(MODE_PARA_UP);
        gradient2.Mode(MODE_PARA_DN);
    }

    CDitherMap map(m_rgb1);

    COLORREF rgbLastLine = 0xFFFFFFFF;
    int iNColorsLast     = 0;
    int iRepeat         = 0;

    for (int i = 0; i < iDim; i++)
    {
        COLORREF rgbThisLine;
        if (m_iNSetPoints == 2)
        {
            rgbThisLine = gradient1.COLORREFAt(i, iDim);
        }
        else
        {
            if (i < iDim_2)
                rgbThisLine = gradient1.COLORREFAt(i, iDim_2);
            else
                rgbThisLine = gradient2.COLORREFAt(i-iDim_2, iDim-iDim_2);
        }

        HL_PIXEL *pDst = m_pDitherMap + MAX_PATTERN * i;
        int iR = GetRValue(rgbThisLine);
        int iG = GetGValue(rgbThisLine);
        int iB = GetBValue(rgbThisLine);
        int iNColors = 0;

        if (rgbThisLine == rgbLastLine)
        {
            HL_PIXEL *pLast = pDst - MAX_PATTERN;
            iNColors = m_piSizes[i-1];
            int iN1 = 0;
            if ((iRepeat % 2) == 0)
                iN1 = iNColorsLast/2;
            int iN2 = iNColors-iN1;
            memcpy(pDst, pLast+iN1, iN2 * sizeof(HL_PIXEL));
            memcpy(pDst+iN2, pLast, iN1 * sizeof(HL_PIXEL));
            iRepeat++;
        }
        else
        {
            iRepeat = 0;
            map.Set(iR,iG,iB);
            iNColors = map.NColors();
            iNColorsLast = iNColors;
            int iFactor = 1;
            if (iNColors > 1 && iNColors < 32)
            {
                iFactor = 32 / iNColors;
            }

            for (int i = 0; i < iFactor; i++)
            {
                memcpy(pDst, map.Map(), iNColors * sizeof(HL_PIXEL));
                pDst += iNColors;            
            }

            iNColors = iNColors * iFactor;
        }
        rgbLastLine = rgbThisLine;

        m_piSizes[i] = iNColors;
    }
}
/*============================================================================*/

    COLORREF                    CColorMap::ColorAtPosition(int iPos)

/*============================================================================*/
{
    return 0x000;
}
/*============================================================================*/

    COLORREF                    CColorMap::Interpolate(
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    int                         iPos,
    int                         iDim )

/*============================================================================*/
{
    int _iPos = iDim-iPos;
    int iR = (GetRValue(rgb1) * iPos + GetRValue(rgb2) * _iPos) / iDim;
    int iG = (GetGValue(rgb1) * iPos + GetGValue(rgb2) * _iPos) / iDim;
    int iB = (GetBValue(rgb1) * iPos + GetBValue(rgb2) * _iPos) / iDim;
    return RGB(iR, iG, iB);
}
/*============================================================================*/

    CNonLinearColorMap::CNonLinearColorMap(
    int                         iDim,
    int                         iMidPoint,
    COLORREF                    rgbEnd,
    COLORREF                    rgbMid )

/*============================================================================*/
{
    m_iDim = iDim;
    m_iMidPoint = iMidPoint;
    m_iR1 = GetRValue(rgbEnd);
    m_iG1 = GetGValue(rgbEnd);
    m_iB1 = GetBValue(rgbEnd);
    m_iR2 = GetRValue(rgbMid);
    m_iG2 = GetGValue(rgbMid);
    m_iB2 = GetBValue(rgbMid);
}
/*============================================================================*/

    COLORREF                    CNonLinearColorMap::ColorAtPosition(
    int                         iPos )

/*============================================================================*/
{
    int iRangePCT = 50;
    int iSat = 256;
    if (iPos < m_iMidPoint)
    {
        int iDMax = m_iMidPoint * iRangePCT / 100;
        int iYMin = iSqrt(m_iMidPoint*m_iMidPoint-iDMax*iDMax);
        int iD = (m_iMidPoint - iPos) * iRangePCT / 100;
        int iY = iSqrt(m_iMidPoint*m_iMidPoint-iD*iD) - iYMin;
        iSat = 256 * iY / (m_iMidPoint-iYMin);
    }
    else
    {
        int iD = (iPos - m_iMidPoint) * iRangePCT / 100;
        int iRange = m_iDim - m_iMidPoint;
        int iDMax = iRange * iRangePCT / 100;
        int iYMin = iSqrt(iRange*iRange-iDMax*iDMax);
        int iY = iSqrt(iRange*iRange-iD*iD)-iYMin;
        iSat = 256 * iY / (iRange-iYMin);
    }

    int _iSat = 256-iSat;
    int iR = ((m_iR1 * _iSat + m_iR2 * iSat) >> 8);
    int iG = ((m_iG1 * _iSat + m_iG2 * iSat) >> 8);
    int iB = ((m_iB1 * _iSat + m_iB2 * iSat) >> 8);
    return RGB(iR,iG,iB);
}
/*========================================================================*/

    C3PosColorMap::C3PosColorMap(
    int                         iDim,
    COLORREF                    rgbFace )

/*========================================================================*/
{
    m_iDim = iDim;
    m_iMidPoint = iDim * 2 / 3;
    m_rgb1 = rgbFace;
    m_rgb2 = rgbFace;

    int iAlphaBottom = g_pSetServer->GetInt(INT_ALPHA_BOTTOM_EDGE);
    int iR = __min(0xFF, GetRValue(rgbFace)+iAlphaBottom);
    int iG = __min(0xFF, GetGValue(rgbFace)+iAlphaBottom);
    int iB = __min(0xFF, GetBValue(rgbFace)+iAlphaBottom);
    m_rgb3 = RGB(iR, iG, iB);    
}
/*========================================================================*/

    C3PosColorMap::C3PosColorMap(
    int                         iDim,
    int                         iMidPoint,
    COLORREF                    rgb1,
    COLORREF                    rgb2,
    COLORREF                    rgb3 )

/*========================================================================*/
{
    m_iDim = iDim;
    m_iMidPoint = iMidPoint;
    m_rgb1      = rgb1;
    m_rgb2      = rgb2;
    m_rgb3      = rgb3;
}
/*========================================================================*/

    COLORREF                    C3PosColorMap::ColorAtPosition(
    int                         iPos )

/*========================================================================*/
{
    iPos = __max(iPos, 0);
    iPos = __min(iPos, m_iDim-1);
    if (iPos < m_iMidPoint)
        return Interpolate(m_rgb2, m_rgb1, iPos, m_iMidPoint);
    return Interpolate(m_rgb3, m_rgb2, iPos-m_iMidPoint, m_iDim-m_iMidPoint);
}

/*========================================================================*/

    CDitherMap::CDitherMap()

/*========================================================================*/
{
    m_iR = -1;
    m_iG = -1;
    m_iB = -1;
    m_iNColors = 0;
    m_iIndex = 0;
    m_bOffset = FALSE;
}
/*========================================================================*/

    CDitherMap::CDitherMap(
    COLORREF                    rgb )

/*========================================================================*/
{
    m_iR = -1;
    m_iG = -1;
    m_iB = -1;
    m_iNColors = 0;
    m_iIndex = 0;
    Set(GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
}
/*========================================================================*/

    void                        CDitherMap::Set(
    int                         iR,
    int                         iG,
    int                         iB )

/*========================================================================*/
{
#ifdef IPHONE
    m_iR = iR;
    m_iG = iG;
    m_iB = iB;
    m_wColors[0] = (iB << 24) | (iG << 16) | (iR << 8) | 0x000000FF;
    m_iNColors = 1;
#else
    BOOL bSame = TRUE;
    if (m_iR != iR) bSame = FALSE;
    if (m_iG != iG) bSame = FALSE;
    if (m_iB != iB) bSame = FALSE;

    if (bSame)
    {
        m_bOffset = !m_bOffset;
    }
    else
    {
        m_bOffset = FALSE;
    }

    m_iR = iR;  
    m_iG = iG;
    m_iB = iB;

    int iR16Lo = iR * 31 / 255;
    int iR16Hi = iR16Lo + 1;
    int iG16Lo = iG * 63 / 255;
    int iG16Hi = iG16Lo + 1;
    int iB16Lo = iB * 31 / 255;
    int iB16Hi = iB16Lo + 1;

    int iRErrorLo  = RError(iR16Lo, m_iR);
    int iGErrorLo  = GError(iG16Lo, m_iG);
    int iBErrorLo  = BError(iB16Lo, m_iB);
    int iRErrorHi  = RError(iR16Hi, m_iR);
    int iGErrorHi  = GError(iG16Hi, m_iG);
    int iBErrorHi  = BError(iB16Hi, m_iB);
    
    m_iNColors = 0;
    m_iIndex = 0;

    BOOL bPatDone = FALSE;

    iR16Lo = (iR16Lo << 11);
    iR16Hi = (iR16Hi << 11);
    iG16Lo = (iG16Lo << 5);
    iG16Hi = (iG16Hi << 5);

    int iR16 = iR16Lo;
    int iG16 = iG16Lo;
    int iB16 = iB16Lo;
    int iRError = iRErrorLo;
    int iGError = iGErrorLo;
    int iBError = iBErrorLo;

    if (iRErrorHi < -iRErrorLo)
    {
        iR16 = iR16Hi;
        iRError = iRErrorHi;
    }
    if (iGErrorHi < -iGErrorLo)
    {
        iG16 = iG16Hi;
        iGError = iGErrorHi;
    }
    if (iBErrorHi < -iBErrorLo)
    {
        iB16 = iB16Hi;
        iBError = iBErrorHi;
    }

    int iRErrorSum = iRError;
    int iGErrorSum = iGError;
    int iBErrorSum = iBError;
    int iRErrorStart = iRError;
    int iGErrorStart = iGError;
    int iBErrorStart = iBError;

    while (m_iNColors < MAX_PATTERN && !bPatDone)
    {
        if (iRErrorSum > 0) {iR16  = iR16Lo; iRError = iRErrorLo; } else if (iRErrorSum < 0)   { iR16 = iR16Hi; iRError = iRErrorHi; } 
        if (iGErrorSum > 0) {iG16  = iG16Lo; iGError = iGErrorLo; } else if (iGErrorSum < 0)   { iG16 = iG16Hi; iGError = iGErrorHi; }
        if (iBErrorSum > 0) {iB16  = iB16Lo; iBError = iBErrorLo; } else if (iBErrorSum < 0)   { iB16 = iB16Hi; iBError = iBErrorHi; }

        iRErrorSum += iRError;
        iGErrorSum += iGError;
        iBErrorSum += iBError;

        m_wColors[m_iNColors] = (WORD)(iR16 | iG16 | iB16);
        m_iNColors++;
        
        if (iRErrorSum == iRErrorStart && iGErrorSum == iGErrorStart && iBErrorSum == iBErrorStart)
            bPatDone = TRUE;
    }
#endif
}
/*========================================================================*/

    void                        CDitherMap::FillHZ(
    HL_PIXEL                    *pTgt,
    int                         iN,
    int                         iXGeo )

/*========================================================================*/
{
    if (m_bOffset)
        iXGeo += m_iNColors / 2;
    m_iIndex = iXGeo % m_iNColors;

    if (m_iNColors == 0)
        return;

    if (m_iNColors == 1)
    {
        HL_PIXEL wSet = m_wColors[0];
        while (iN > 0)
        {
            *pTgt = wSet;
            pTgt++;
            iN--;
        }
        return;
    }
    
    int iNDone = 0;
    while (iNDone < iN)
    {
        if (m_iIndex >= m_iNColors)
            m_iIndex = 0;

        int iNNow = iN-iNDone;
        iNNow = __min(iNNow, m_iNColors-m_iIndex);
        memcpy(pTgt, m_wColors + m_iIndex, iNNow * sizeof(HL_PIXEL));
        pTgt += iNNow;
        m_iIndex += iNNow;
        iNDone += iNNow;
    }
}
/*========================================================================*/

    void                        CDitherMap::AddHZ(
    HL_PIXEL                    *pTgt,
    int                         iN,
    int                         iXGeo )

/*========================================================================*/
{
#ifdef HLOSD
    unsigned int iRAdd = m_iR;
    unsigned int iGAdd = m_iG << 8;
    unsigned int iBAdd = m_iB << 16;

    HL_PIXEL iRMax = R_MASK - iRAdd;
    HL_PIXEL iGMax = G_MASK - iGAdd;
    HL_PIXEL iBMax = B_MASK - iBAdd;

    for (int i = 0 ; i < iN ; i++)
    {
        HL_PIXEL wSet = *pTgt;
        HL_PIXEL iR = wSet & R_MASK;
        HL_PIXEL iG = wSet & G_MASK;
        HL_PIXEL iB = wSet & B_MASK;
        HL_PIXEL iA = wSet & A_MASK;

        if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
        if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
        if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   }
        *pTgt = iR|iG|iB|iA;
        pTgt ++;
    }
#elif IPHONE
    unsigned int iRAdd = m_iR << 8;
    unsigned int iGAdd = m_iG << 16;
    unsigned int iBAdd = m_iB << 24;

    HL_PIXEL iRMax = R_MASK - iRAdd;
    HL_PIXEL iGMax = G_MASK - iGAdd;
    HL_PIXEL iBMax = B_MASK - iBAdd;

    for (int i = 0 ; i < iN ; i++)
    {
        HL_PIXEL wSet = *pTgt;
        HL_PIXEL iR = wSet & R_MASK;
        HL_PIXEL iG = wSet & G_MASK;
        HL_PIXEL iB = wSet & B_MASK;
        HL_PIXEL iA = wSet & A_MASK;

        if (iA == 0xFF)
        {
            if (iR >= iRMax) iR = R_MASK; else { iR+=iRAdd; iR &= R_MASK;   }
            if (iG >= iGMax) iG = G_MASK; else { iG+=iGAdd; iG &= G_MASK;   }
            if (iB >= iBMax) iB = B_MASK; else { iB+=iBAdd; iB &= B_MASK;   }
            *pTgt = iR|iG|iB|0xFF;
        }
        else
        {
            unsigned int iRAddThis = (m_iR * iA)        & R_MASK;
            unsigned int iGAddThis = ((m_iG * iA) << 8)  & G_MASK;
            unsigned int iBAddThis = ((m_iB * iA) << 16) & B_MASK;

            HL_PIXEL iRMaxThis = R_MASK - iRAddThis;
            HL_PIXEL iGMaxThis = G_MASK - iGAddThis;
            HL_PIXEL iBMaxThis = B_MASK - iBAddThis;

            if (iR >= iRMaxThis) iR = R_MASK; else { iR+=iRAddThis; iR &= R_MASK;   }
            if (iG >= iGMaxThis) iG = G_MASK; else { iG+=iGAddThis; iG &= G_MASK;   }
            if (iB >= iBMaxThis) iB = B_MASK; else { iB+=iBAddThis; iB &= B_MASK;   }
            *pTgt = iR|iG|iB|iA;
        }
        pTgt ++;
    }
#else
    if (m_iNColors == 0)
        return;

    if (m_bOffset)
        iXGeo += m_iNColors / 2;
    m_iIndex = iXGeo % m_iNColors;

    if (m_iNColors == 1)
    {
        HL_PIXEL wSet = m_wColors[0];
        int wR = wSet & 0xF800;
        int wG = wSet & 0x07E0;
        int wB = wSet & 0x001F;
 
        while (iN > 0)
        {
            HL_PIXEL wTgt = *pTgt;
            int wROut = ((wTgt & 0xF800) + wR);
            if (wROut > 0xF800) wROut = 0xF800; else wROut &= 0xF800;
            int wGOut = ((wTgt & 0x07E0) + wG);
            if (wGOut > 0x07E0) wGOut = 0x07E0; else wGOut &= 0x07E0;
            int wBOut = ((wTgt & 0x001F) + wB);
            if (wBOut > 0x001F) wBOut = 0x001F;

            *pTgt = (HL_PIXEL)(wROut|wGOut|wBOut);
            pTgt++;
            iN--;
        }
        return;
    }
    
    int iNDone = 0;
    while (iNDone < iN)
    {
        if (m_iIndex >= m_iNColors)
            m_iIndex = 0;

        HL_PIXEL wSet = m_wColors[m_iIndex];
        m_iIndex++;
        int wR = wSet & 0xF800;
        int wG = wSet & 0x07E0;
        int wB = wSet & 0x001F;
 
        HL_PIXEL wTgt = *pTgt;
        int wROut = ((wTgt & 0xF800) + wR);
        if (wROut > 0xF800) wROut = 0xF800; else wROut &= 0xF800;
        int wGOut = ((wTgt & 0x07E0) + wG);
        if (wGOut > 0x07E0) wGOut = 0x07E0; else wGOut &= 0x07E0;
        int wBOut = ((wTgt & 0x001F) + wB);
        if (wBOut > 0x001F) wBOut = 0x001F;

        *pTgt = (HL_PIXEL)(wROut|wGOut|wBOut);
        pTgt++;
        iNDone++;
    }
#endif
}
/*========================================================================*/

    int                         CDitherMap::RError(int iR16Bit, int iR256)

/*========================================================================*/
{
    int iROut = iR16Bit * 255 / 31;
    return iROut-iR256;
}
/*========================================================================*/

    int                         CDitherMap::GError(int iG16Bit, int iG256)

/*========================================================================*/
{
    int iGOut = iG16Bit * 255 / 63;
    return iGOut-iG256;
}
/*========================================================================*/

    int                         CDitherMap::BError(int iB16Bit, int iB256)

/*========================================================================*/
{
    int iBOut = iB16Bit * 255 / 31;
    return iBOut-iB256;
}