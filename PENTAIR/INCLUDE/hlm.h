/*
--------------------------------------------------------------------------------

    HLM.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifndef HLM_H
#define HLM_H

// General Errors
enum CONFIG_ERROR
{
    HL_ERROR_FAIL							= 0,
    HL_ERROR_PASS							= 1,
    HL_ERROR_NOT_CONNECTED					= 2,
    HL_ERROR_TIMED_OUT						= 3,
    HL_ERROR_INCORRECT_OBJECT				= 4,
    HL_ERROR_ELANINTERFACE_NULL				= 5,
    HL_ERROR_CAUDIIOZONE_NULL				= 6,
    HL_ERROR_DUMP							= 7,
    HL_CONFIG_TIMEOUT						= 8,
    HL_VERSION_ERROR						= 9,
    HL_LONG_DOWNLOAD						= 10,
    HL_ERROR_XML_INCORRECT_FORMAT			= 11
};


#define HL_COMPRESS_NONE                            -1
#define HL_COMPRESS_2BIT                            0
#define HL_COMPRESS_RLE                             1

#define SIZE_CPCLIENT_PING                          32

#define TERMTYPE_NONE                               0
#define TERMTYPE_CR                                 1
#define TERMTYPE_LF                                 2
#define TERMTYPE_CRLF                               3

#define KEY_DOWN                                    0x00004000
#define KEY_UP                                      0x00008000

#define BUTTON_OPT_NODRAWFACE                       0x00000001
#define BUTTON_OPT_NODRAWPRESSED                    0x00000002
#define BUTTON_OPT_NOANIMATE                        0x00000004
#define BUTTON_OPT_NOSHINE                          0x00000008
#define BUTTON_OPT_SIMPLEROUNDRECT                  0x00000010
#define BUTTON_OPT_HALO                             0x00000020
#define BUTTON_OPT_TOOLBAR                          0x00000040
#define BUTTON_OPT_CONFIRM                          0x00000080
#define BUTTON_OPT_QUICKCONFIRM                     0x00000180
#define BUTTON_OPT_CONFIRM_SET                      0x00000280
#define BUTTON_OPT_TINTICON1                        0x00004000
#define BUTTON_OPT_GOONDOWN                         0x00008000
#define BUTTON_OPT_TINTICON2                        0x00010000
#define BUTTON_OPT_INVERT_ICON                      0x00020000
#define BUTTON_OPT_SPLITTEXT_2                      0x00040000
#define BUTTON_OPT_SPLITTEXT_3                      0x00080000
#define BUTTON_OPT_SPLITTEXT_4                      0x00100000
#define BUTTON_OPT_NOSHINE_UPPER                    0x00200000
#define BUTTON_OPT_NOGRADIENT                       0x00400000

#define DIBATTRIB_RENDERED                          0x00000001
#define DIBATTRIB_DONTRENDER                        0x00000002
#define DIBATTRIB_NOXBITS                           0x00000004
#define DIBATTRIB_VERTICAL                          0x00000008
#define DIBATTRIB_DRAWFAST                          0x00000020
#define DIBATTRIB_DRAWFASTHEADER                    0x00000040
#define DIBATTRIB_ITEMSEMBEDDED                     0x00000080
#define DIBATTRIB_HIRESALPHA                        0x00000100
#define DIBATTRIB_BACKGROUNDEMBEDDED                0x00000200
#define DIBATTRIB_FORCESYSMEM                       0x00000400
#define DIBATTRIB_HIRESBLEND                        0x00000800
#define DIBATTRIB_ALPHAADDMODE                      0x00001000
#define DIBATTRIB_RLEENCODED                        0x00002000
#define DIBATTRIB_ONESHOT                           0x00008000
#define DIBATTRIB_DRAWOVERLAY                       0x00010000
#define DIBATTRIB_TRACKUPDATES                      0x00020000
#define DIBATTRIB_ODDSCANLINE                       0x00040000
#define DIBATTRIB_EVENSCANLINE                      0x00080000
#define DDFLAG_DISCARDABLE                          0x00400000
#define DIBATTRIB_SCALE_ANY                         0x00800000
#define DIBATTRIB_CUSTOMFONT                        0x01000000
#define DIBATTRIB_LOCALDRAW                         0x10000000
#define DIBATTRIB_WAITVSYNC                         0x20000000
#define DIBATTRIB_PERSISTANT                        0x40000000

#define HL3DBUTTON_CLEARTYPEOK                      0x00000001
#define HL3DBUTTON_MASK                             0x00000002
#define HL3DBUTTON_RAISEDLEVEL                      0x00000004
#define HL3DBUTTON_FORCEZEROLEVEL                   0x00000008
#define HL3DBUTTON_NOTEXTURIZEDIB                   0x00000020
#define HL3DBUTTON_FLAT_ADDITIVE                    0x00000040
#define HL3DBUTTON_SUBTLENAV                        0x00000400
#define HL3DBUTTON_TRANSPARENT                      0x00000800
#define HL3DBUTTON_ADDMODE                          0x00001000
#define HL3DBUTTON_COMPLEX                          0x00002000
#define HL3DBUTTON_DISABLECHECKDRAW                 0x00008000
#define HL3DBUTTON_DRAWSCALE                        0x00010000
#define HL3DBUTTON_TRANSPARENTFACE                  0x00020000
#define HL3DBUTTON_CLIENTEDGE                       0x00040000
#define HL3DBUTTON_FORCECAPS                        0x00080000
#define HL3DBUTTON_ROUNDRECT                        0x00100000

//
// CHLWindow types
//
#define WND_START                                   0
#define WND_NONE                                        0
#define WND_APP_LABEL                                   1
#define WND_PAGE_TAB                                    2
#define WND_LIST_CONTROL                                3
#define WND_BUTTON                                      4
#define WND_END                                     4

//
// SetServer int types
//
#define INT_OSD_HEADER_OPAC1                            0
#define INT_OSD_HEADER_OPAC2                            1
#define INT_OSD_GROUP_OPAC1                             2
#define INT_OSD_GROUP_OPAC2                             3
#define INT_OSD_CURSOR_HALO                             4
#define INT_OSD_CURSOR_RAD                              5
#define INT_OSD_SCROLL_SIZE                             6
#define INT_OSD_GROUP_PAD                               7
#define INT_OSD_SUMMARY_YPOS                            8
#define INT_OSD_MAIN_YPOS                               9
#define INT_OSD_HEADER_SIZE                             10
#define INT_OSD_HEADER_INSET                            11
#define INT_OSD_OSCAN_LEFT                              24
#define INT_OSD_OSCAN_RIGHT                             25
#define INT_OSD_OSCAN_TOP                               26
#define INT_OSD_OSCAN_BOTTOM                            28
#define INT_OSD_CURSOR_SHINE                            13


#define INT_START                                   0
#define INT_BUTTON_SHADEIN                              0
#define INT_BUTTON_SHADEOUT                             1
#define INT_BUTTON_TEXTHEIGHT                           2
#define INT_BUTTON_ROUT                                 3
#define INT_APP_TEXTHEIGHT                              4
#define INT_APP_HEADERSIZE                              5
#define INT_APP_HEADERINSET                             6
#define INT_LIST_TEXTHEIGHT                             7
#define INT_LABEL_TEXTHEIGHT                            8
#define INT_TAB_ROUT                                    9
#define INT_SCROLL_WIDTH                                10
#define INT_FONT_WEIGHT                                 11
#define INT_ALPHA_RMINOR                                12
#define INT_ALPHAMAX                                    13
#define INT_ALPHA_UPPER_RATIO                           14
#define INT_ALPHA_BOTTOM_EDGE                           15
#define INT_MAINTAB_ROUT                                16
#define INT_MAINTAB_ALPHAMAX                            17
#define INT_MAINTAB_ALPHA_UPPER_RATIO                   18
#define INT_MAINTAB_INSET                               19
#define INT_MAINTAB_FLAGS                               20
#define INT_ALPHA_SUBTLE_SELECT_TOP                     21
#define INT_ALPHA_SUBTLE_SELECT_BTM                     22
#define INT_NAVBAR_INSET                                23
#define INT_PAGETAB_INSET                               24
#define INT_MAINTAB_RMINOR                              25
#define INT_EDGESIZE                                    26
#define INT_TOPBARFLAGS                                 27
#define INT_BUTTONGROUP_SHADEOUT                        28
#define INT_SCROLL_ALPHAMIN                             29
#define INT_SCROLL_ALPHAMAX                             30
#define INT_TOPBAR_SIZEPCT                              31
#define INT_BTMBAR_SIZEPCT                              32
#define INT_BUTTON_GRADIENT                             33
#define INT_DISABLE_MAIN_ALPHA                          34
#define INT_MAINTAB_ALPHA                               35
#define INT_MAINTAB_SELECT_ALPHA                        36
#define INT_HORIZON_DY                                  37
#define INT_LIST_EDGEFADE                               38
#define INT_STD_INSET                                   39
#define INT_PAGE_INSET                                  40
#define INT_TABBUTTON_TEXTHEIGHT                        41
#define INT_LIST_SELRAD                                 42
#define INT_ZONEICON_ALPHAOVERRIDE                      43
#define INT_DEFAULT_ICONSIZE                            44
#define INT_GLOWMODE_SIZE1                              45
#define INT_GLOWMODE_SIZE2                              46

#define INT_END                                     46

#define TOPBAR_FLAG_SYSGEN                          0x00000001
#define TOPBAR_FLAG_ENABLE_PAGETAB_RGB2             0x00000008
#define TOPBAR_FLAG_ICONCOLOR                       0x00000010
#define TOPBAR_FLAG_ICONCOLOR_ABS                   0x00000020
#define TOPBAR_FLAG_ZONEICONOVERRIDE                0x00000040
#define TOPBAR_FLAG_OSDSHIELDMODE                   0x00000080
#define TOPBAR_FLAG_DYNAMICCURSOR                   0x00000100
#define TOPBAR_FLAG_EXITMAIN                        0x00000200
#define TOPBAR_FLAG_OSDHORIZON                      0x00000400
#define TOPBAR_FLAG_SOLIDCURSOR                     0x00000800
#define TOPBAR_FLAG_ENABLE_PAGETAB_RGB3             0x00001000

#define INTDEF_OSD_HEADER_OPAC1                     60
#define INTDEF_OSD_HEADER_OPAC2                     93
#define INTDEF_OSD_GROUP_OPAC1                      30
#define INTDEF_OSD_GROUP_OPAC2                      50
#define INTDEF_OSD_CURSOR_HALO                      20
#define INTDEF_OSD_CURSOR_RAD                       10
#define INTDEF_OSD_SCROLL_SIZE                      50
#define INTDEF_OSD_GROUP_PAD                        4
#define INTDEF_OSD_SUMMARY_YPOS                     80
#define INTDEF_OSD_MAIN_YPOS                        52
#define INTDEF_OSD_HEADER_SIZE                      150
#define INTDEF_OSD_HEADER_INSET                     25
#define INTDEF_OSD_OSCAN_LEFT                       0
#define INTDEF_OSD_OSCAN_RIGHT                      0
#define INTDEF_OSD_OSCAN_TOP                        0
#define INTDEF_OSD_OSCAN_BOTTOM                     0

#define INTDEF_BUTTON_SHADEIN                       6
#define INTDEF_BUTTON_SHADEOUT                      0
#define INTDEF_BUTTON_TEXTHEIGHT                    20
#define INTDEF_BUTTON_ROUT                          20
#define INTDEF_APP_TEXTHEIGHT                       28
#define INTDEF_APP_HEADERSIZE                       60
#define INTDEF_APP_HEADERINSET                      4
#define INTDEF_LIST_TEXTHEIGHT                      20
#define INTDEF_LABEL_TEXTHEIGHT                     24
#define INTDEF_TAB_ROUT                             20
#define INTDEF_FONT_WEIGHT                          700
#define INTDEF_SCROLL_WIDTH                         32
#define INTDEF_ALPHA_RMINOR                         8
#define INTDEF_ALPHAMAX                             150
#define INTDEF_ALPHA_UPPER_RATIO                    75
#define INTDEF_ALPHA_BOTTOM_EDGE                    50
#define INTDEF_MAINTAB_ROUT                         20
#define INTDEF_MAINTAB_ALPHAMAX                     150
#define INTDEF_MAINTAB_ALPHA_UPPER_RATIO            90
#define INTDEF_MAINTAB_INSET                        3
#define INTDEF_MAINTAB_FLAGS                        0
#define INTDEF_ALPHA_SUBTLE_SELECT_TOP              50
#define INTDEF_ALPHA_SUBTLE_SELECT_BTM              40
#define INTDEF_PAGETAB_INSET                        3
#define INTDEF_NAVBAR_INSET                         4
#define INTDEF_MAINTAB_RMINOR                       8
#define INTDEF_EDGESIZE                             2
#define INTDEF_TOPBARFLAGS                          0
#define INTDEF_BUTTONGROUP_SHADEOUT                 4
#define INTDEF_SCROLL_ALPHAMIN                      25
#define INTDEF_SCROLL_ALPHAMAX                      100
#define INTDEF_TOPBAR_SIZEPCT                       50
#define INTDEF_BTMBAR_SIZEPCT                       50
#define INTDEF_BUTTON_GRADIENT                      64
#define INTDEF_DISABLE_MAIN_ALPHA                   32
#define INTDEF_MAINTAB_ALPHA                        95
#define INTDEF_MAINTAB_SELECT_ALPHA                 100
#define INTDEF_HORIZON_DY                           175
#define INTDEF_LIST_EDGEFADE                        1
#define INTDEF_STD_INSET                            8
#define INTDEF_PAGE_INSET                           10
#define INTDEF_TABBUTTON_TEXTHEIGHT                 18
#define INTDEF_LIST_SELRAD                          5
#define INTDEF_ZONEICON_ALPHAOVERRIDE               75
#define INTDEF_DEFAULT_ICONSIZE                     32
#define INTDEF_GLOWMODE_SIZE1                       32
#define INTDEF_GLOWMODE_SIZE2                       16

#define N_INT_TYPES                                 (INT_END-INT_START+1)


//
// SetServer rgb types
//
#define RGB_OSD_BG_UL                                   0
#define RGB_OSD_BG_UR                                   1
#define RGB_OSD_BG_LL                                   2
#define RGB_OSD_BG_LR                                   3
#define RGB_OSD_HDR_UL                                  4
#define RGB_OSD_HDR_UR                                  5
#define RGB_OSD_HDR_LL                                  6
#define RGB_OSD_HDR_LR                                  7
#define RGB_OSD_TEXT                                    8
#define RGB_OSD_GROUP1                                  9
#define RGB_OSD_GROUP2                                  10
#define RGB_OSD_CURSOR_HALO                             21
#define RGB_OSD_CURSOR_FILL                             22

#define RGB_START                                   0
#define RGB_BUTTON                                      0
#define RGB_BUTTON_TEXT                                 1
#define RGB_APP_BACKGROUND                              2
#define RGB_APP_TEXT                                    3
#define RGB_EDGE                                        4
#define RGB_LEGEND_HEAT                                 5
#define RGB_LEGEND_COOL                                 6
#define RGB_LEGEND_ROOM                                 7
#define RGB_LEGEND_OUTSIDE                              8
#define RGB_LEGEND_ARMED                                9
#define RGB_LEGEND_NOTREADY                             10
#define RGB_LEGEND_ALARM                                11
#define RGB_LEGEND_FIRE                                 12
#define RGB_LEGEND_ZONE1                                13
#define RGB_LEGEND_ZONE2                                14
#define RGB_SELECT                                      15
#define RGB_SELECT_TEXT                                 16
#define RGB_SELECT_SUBTLE                               17
#define RGB_MAINTAB_TOP                                 18
#define RGB_MAINTAB_BTM                                 19
#define RGB_MAINTAB_EDGE                                20
#define RGB_PAGETAB_TOP                                 21
#define RGB_PAGETAB_BTM                                 22
#define RGB_LIST_AREA                                   23
#define RGB_PAGETAB_SELECT_TOP_LS                       24
#define RGB_PAGETAB_SELECT_BTM_LS                       25
#define RGB_VPANEL_1                                    26
#define RGB_VPANEL_2                                    27
#define RGB_MAINTAB_SELECT_TOP                          28
#define RGB_MAINTAB_SELECT_BTM                          29
#define RGB_TOPBAR_TOP                                  30
#define RGB_TOPBAR_BTM                                  31
#define RGB_SCROLL                                      32
#define RGB_BTMBAR_TOP                                  33
#define RGB_BTMBAR_BTM                                  34
#define RGB_LCD_TOP                                     35
#define RGB_LCD_BTM                                     36
#define RGB_LCD_TEXT                                    37
#define RGB_UNUSED3                                     38
#define RGB_ZONEPAGE_LOWER                              39
#define RGB_ICONOVERRIDE                                40
#define RGB_BAR_EDGE                                    41
#define RGB_PAGETAB_UNSELECT_TOP                        42
#define RGB_PAGETAB_UNSELECT_BTM                        43
#define RGB_HEAT_STD                                    44
#define RGB_HEAT_MAINTAB                                45
#define RGB_COOL_STD                                    46
#define RGB_COOL_MAINTAB                                47
#define RGB_LIST_SELECTITEMT                            48
#define RGB_LEGEND_MORNING                              49
#define RGB_LEGEND_DAY                                  50
#define RGB_LEGEND_EVENING                              51
#define RGB_LEGEND_NIGHT                                52
#define RGB_LEGEND_POOL                                 53
#define RGB_LEGEND_SPA                                  54
#define RGB_LEGEND_SOLAR                                55
#define RGB_LEGEND_LIGHTS                               56
#define RGB_LEGEND_OFF                                  57
#define RGB_LEGEND_RUNPROG                              58
#define RGB_LEGEND_MANUAL                               59
#define RGB_LEGEND_FAN                                  60
#define RGB_ZONEICON_OVERRIDE                           61
#define RGB_LIST_SELECTITEMB                            62
#define RGB_LIST_PLAYABLE                               63
#define RGB_LIST_SELECTABLE                             64
#define RGB_LIST_AUTO                                   65
#define RGB_LIST_PLAY1                                  66
#define RGB_LIST_PLAY2                                  67
#define RGB_LIST_IDLE                                   68
#define RGB_LIST_PAUSED                                 69
#define RGB_LIST_WORKING                                70
#define RGB_LIST_NEWMSG                                 71
#define RGB_LIST_OLDMSG                                 72
#define RGB_LEGEND_RAIN                                 73
#define	RGB_ERROR										74
#define	RGB_ERROR_2										75
#define RGB_PAGETAB_SELECT_TOP_PT                       76
#define RGB_PAGETAB_SELECT_BTM_PT                       77
#define RGB_PAGETAB_MID                                 78

#define RGB_END                                     78

#define N_RGB_TYPES                                 (RGB_END-RGB_START+1)

//
// RGB DEFAULTS
//
#define RGBDEF_OSD_BG_UL                            RGB(0,0,32)
#define RGBDEF_OSD_BG_UR                            RGB(20,20,200)
#define RGBDEF_OSD_BG_LL                            RGB(0,0,55)
#define RGBDEF_OSD_BG_LR                            RGB(0,0,55)
#define RGBDEF_OSD_HDR_UL                           RGB(200,200,200)
#define RGBDEF_OSD_HDR_UR                           RGB(128,128,128)
#define RGBDEF_OSD_HDR_LL                           RGB(200,200,200)
#define RGBDEF_OSD_HDR_LR                           RGB(128,128,128)
#define RGBDEF_OSD_TEXT                             RGB(190,190,190)
#define RGBDEF_OSD_GROUP1                           RGB(255,255,255)
#define RGBDEF_OSD_GROUP2                           RGB(255,255,255)

#define RGBDEF_BUTTON                               RGB(0,0,0)
#define RGBDEF_BUTTON_TEXT                          RGB(255,255,255)
#define RGBDEF_APP_BACKGROUND                       RGB(0,0,0)
#define RGBDEF_APP_TEXT                             RGB(255,255,255)
#define RGBDEF_EDGE                                 RGB(100,100,100)
#define RGBDEF_LEGEND_HEAT                          RGB(200,0,0)
#define RGBDEF_LEGEND_COOL                          RGB(150,150,0)
#define RGBDEF_LEGEND_ROOM                          RGB(0,0,200)
#define RGBDEF_LEGEND_OUTSIDE                       RGB(0,200,0)
#define RGBDEF_LEGEND_ARMED                         RGB(0,0,150)
#define RGBDEF_LEGEND_NOTREADY                      RGB(175,0,175)
#define RGBDEF_LEGEND_ALARM                         RGB(128,128,128)
#define RGBDEF_LEGEND_FIRE                          RGB(150,150,75)
#define RGBDEF_LEGEND_ZONE1                         RGB(0,150,150)
#define RGBDEF_LEGEND_ZONE2                         RGB(0,0,200)
#define RGBDEF_SELECT                               RGB(200,200,255)
#define RGBDEF_SELECT_TEXT                          RGB(255,255,255)
#define RGBDEF_SELECT_SUBTLE                        RGB(0,0,255)
#define RGBDEF_MAINTAB_TOP                          RGB(0,0,0)
#define RGBDEF_MAINTAB_BTM                          RGB(50,50,50)
#define RGBDEF_MAINTAB_EDGE                         RGB(75,75,75)
#define RGBDEF_PAGETAB_TOP                          RGB(35,35,35)
#define RGBDEF_PAGETAB_BTM                          RGB(50,50,50)
#define RGBDEF_LIST_AREA                            RGB(50,50,50)
#define RGBDEF_PAGETAB_SELECT_TOP_LS                RGB(0,0,200)
#define RGBDEF_PAGETAB_SELECT_BTM_LS                RGB(0,0,0)
#define RGBDEF_PAGETAB_SELECT_TOP_PT                RGB(10,10,50)
#define RGBDEF_PAGETAB_SELECT_BTM_PT                RGB(60,60,255)
#define RGBDEF_VPANEL_1                             RGB(80,80,80)
#define RGBDEF_VPANEL_2                             RGB(30,30,30)
#define RGBDEF_MAINTAB_SELECT_TOP                   RGB(100,100,100)
#define RGBDEF_MAINTAB_SELECT_BTM                   RGB(100,100,150)
#define RGBDEF_TOPBAR_TOP                           RGB(20,20,20)
#define RGBDEF_TOPBAR_BTM                           RGB(30,30,30)
#define RGBDEF_SCROLL                               RGB(200,200,200)
#define RGBDEF_BTMBAR_TOP                           RGB(20,20,20)
#define RGBDEF_BTMBAR_BTM                           RGB(30,30,30)
#define RGBDEF_LCD_TOP                              RGB(0,0,0)
#define RGBDEF_LCD_BTM                              RGB(0,0,100)
#define RGBDEF_LCD_TEXT                             RGB(150,150,255)
#define RGBDEF_UNUSED3                              RGB(0,0,0)
#define RGBDEF_ZONEPAGE_LOWER                       RGB(0,0,200)
#define RGBDEF_ICONOVERRIDE                         RGB(128,128,255)
#define RGBDEF_BAR_EDGE                             RGB(50,50,50)
#define RGBDEF_PAGETAB_UNSELECT_TOP                 RGB(50,50,50)
#define RGBDEF_PAGETAB_UNSELECT_BTM                 RGB(0,0,0)
#define RGBDEF_HEAT_STD                             RGB(255,0,0)
#define RGBDEF_HEAT_MAINTAB                         RGB(255,0,0)
#define RGBDEF_COOL_STD                             RGB(0,0,255)
#define RGBDEF_COOL_MAINTAB                         RGB(0,0,255)
#define RGBDEF_LIST_SELECTITEMT                     RGB(90,90,90)
#define RGBDEF_LEGEND_MORNING                       RGB(100,100,150)
#define RGBDEF_LEGEND_DAY                           RGB(100,100,150)
#define RGBDEF_LEGEND_EVENING                       RGB(100,100,150)
#define RGBDEF_LEGEND_NIGHT                         RGB(100,100,150)
#define RGBDEF_LEGEND_POOL                          RGB(100,100,255)
#define RGBDEF_LEGEND_SPA                           RGB(0,0,255)
#define RGBDEF_LEGEND_SOLAR                         RGB(200,200,0)
#define RGBDEF_LEGEND_LIGHTS                        RGB(200,200,200)
#define RGBDEF_LEGEND_OFF                           RGB(50,50,50)
#define RGBDEF_LEGEND_RUNPROG                       RGB(0,255,0)
#define RGBDEF_LEGEND_MANUAL                        RGB(0,175,0)
#define RGBDEF_LEGEND_FAN                           RGB(0,255,0)
#define RGBDEF_ZONEICON_OVERRIDE                    RGB(200,200,200)
#define RGBDEF_LIST_SELECTITEMB                     RGB(120,120,120)
#define RGBDEF_LIST_PLAYABLE                        RGB(0,50,0)
#define RGBDEF_LIST_SELECTABLE                      RGB(0,0,50)
#define RGBDEF_LIST_AUTO                            RGB(200,0,200)
#define RGBDEF_LIST_PLAY1                           RGB(0,100,0)
#define RGBDEF_LIST_PLAY2                           RGB(0,150,0)
#define RGBDEF_LIST_IDLE                            RGB(50,50,50)
#define RGBDEF_LIST_PAUSED                          RGB(200,200,0)
#define RGBDEF_LIST_WORKING                         RGB(200,0,0)
#define RGBDEF_LIST_NEWMSG                          RGB(100,100,255)
#define RGBDEF_LIST_OLDMSG                          RGB(200,200,200)
#define RGBDEF_LEGEND_RAIN                          RGB(100,100,255)
#define RGBDEF_ERROR								RGB(255,0,0)
#define	RGBDEF_ERROR_2								RGB(255,255,0)
#define	RGBDEF_PAGETAB_MID                          RGB(0,0,0)

#define IRDEVICE_RC5                                0x00000001
#define IRDEVICE_WANTRECEIVE                        0x00000002
#define IRDEVICE_LEARNMODE                          0x00000004

#define ACCEL_WHOLE                                 0
#define ACCEL_TOPHALF                               1
#define ACCEL_BOTTOMHALF                            2

#define VOLUME_TYPE_SOURCE                          0
#define VOLUME_TYPE_RAMP                            1
#define VOLUME_TYPE_DISPLAY                         2
#define VOLUME_TYPE_CONTROLLED                      3
#define VOLUME_TYPE_NONE                            4

#define WEATHER_NONE                                -1
#define WEATHER_USA                                 0
#define WEATHER_CANADA_MSC                          1

#define TYPE_SYSCOMP                                0
#define TYPE_BRIDGE                                 1

#define TRACK_TYPE_NONE                             0
#define TRACK_TYPE_VT                               1
#define TRACK_TYPE_HZ                               2
#define TRACK_TYPE_LOCAL                            3
#define TRACK_TYPE_EXCLUSIVE                        4

#define OWNERID_SYSTEM                              100
#define OWNERID_HOMEPAGE                            101
#define OWNERID_SERVHOST                            102
#define OWNERID_VCTRL                               103
#define OWNERID_LIGHTSERV                           104
#define OWNERID_AUDIOSERVER                         105

#define LIGHT_OBJECT_KEYPAD                         0
#define LIGHT_OBJECT_INTERFACE                      1
#define LIGHT_OBJECT_INTERFACE_GROUP                2

#define TUNER_TYPE_ANTENNA                          0
#define TUNER_TYPE_CABLE1                           1
#define TUNER_TYPE_SAT1                             2
#define TUNER_TYPE_CABLE2                           3
#define TUNER_TYPE_SAT2                             4

#define CPCLIENT_PORT                               5500
#define CPCLIENT_AUDIO_PORT                         5501
#define CPCLIENT_BROADCAST_PORT                     5502

#define ENCODE_RGB565                               0
#define ENCODE_GIF                                  1
#define ENCODE_JPEG                                 2
#define ENCODE_CACHED                               3
#define ENCODE_ARGB                                 4
#define ENCODE_PNG                                  5
#define ENCODE_HLICON                               6

#define PAGEOP_ADDNEW                               0
#define PAGEOP_DELETE                               1
#define PAGEOP_RENAME                               2
#define PAGEOP_MOVEUP                               3
#define PAGEOP_MOVEDOWN                             4

#define READ_FULL                                   0
#define READ_DIMENSIONS                             1

#define EMAIL_UNREAD                                0
#define EMAIL_READ                                  1
#define EMAIL_DELETED                               2

#define EMAIL_PART_MULTI                            0
#define EMAIL_PART_TEXT                             1
#define EMAIL_PART_GIF                              2
#define EMAIL_PART_JPEG                             3
#define EMAIL_PART_HTML                             4
#define EMAIL_PART_FILE                             5

#define PLAYITEMOP_DELETE                           0
#define PLAYITEMOP_MOVEUP                           1
#define PLAYITEMOP_MOVEDN                           2

#define AUDIO_TRACK_MP3                             0x0001
#define AUDIO_TRACK_WAV                             0x0002
#define AUDIO_TRACK_WMA                             0x0004
#define AUDIO_TRACK_WMA_DRM                         0x0008
#define AUDIO_TRACK_IMERGE                          0x0010
#define AUDIO_TRACK_FLAC                            0x0020
#define AUDIO_TRACK_XPERINET                        0x0080
#define AUDIO_TRACK_AXONIX                          0x0100
#define AUDIO_TRACK_ESCIENT                         0x0200
#define AUDIO_TRACK_REQUEST4                        0x0400
#define AUDIO_TRACK_AAC                             0x0800

#define AUDIO_STATUS_TEXTINFO                       0
#define AUDIO_STATUS_KEYPADINFO                     1

// Audio Service Type ID
#define AUDIO_SERVICE_SQUEEZENET_RHAPSODY           1000
#define AUDIO_SERVICE_SQUEEZENET_PANDORA            1001
#define AUDIO_SERVICE_SQUEEZENET_MP3TUNES           1002
#define AUDIO_SERVICE_SMS3_MODES_NOTSUPPORTED       1003
//#define AUDIO_SERVICE_INTERNET_AUDIO                1004
#define AUDIO_SERVICE_ESCIENT_RADIO                 1005
#define AUDIO_SERVICE_INTERNET_LASTFM               1006
#define AUDIO_SERVICE_INTERNET_RHAPSODY             1007
#define AUDIO_SERVICE_INTERNET_PANDORA              1008
#define AUDIO_SERVICE_INTERNET_IHEART               1009
#define AUDIO_SERVICE_INTERNET_SHOUTCAST            1010
#define AUDIO_SERVICE_INTERNET_TUNEIN               1011

#define TS2_FLAG_FORECAST1                          0x00000001
#define TS2_FLAG_FORECAST2                          0x00000002
#define TS2_FLAG_SYSMODES                           0x00000004

#define ITEM_TYPE_TEXT                              0x00000000
#define ITEM_TYPE_PAGE                              0x00000001
#define ITEM_TYPE_PLAYABLE                          0x00000002
#define ITEM_TYPE_ADDABLE                           0x00000004
#define ITEM_TYPE_SEARCH                            0x00000008
#define ITEM_TYPE_IMAGEAVAIL                        0x00000010
#define ITEM_TYPE_BOUNDARY1                         0x00000020
#define ITEM_TYPE_BOUNDARY2                         0x00000040
#define ITEM_TYPE_PASSWORD                          0x00000080
#define ITEM_TYPE_SEARCH_DIRECT                     0x00000100
#define ITEM_TYPE_PAGE_DIRECT                       0x00000200

#define ITEM_OPERATION_GO                           0
#define ITEM_OPERATION_PLAY                         1
#define ITEM_OPERATION_ADD                          2
#define ITEM_OPERATION_GETIMAGE                     3

#define AUDIO_SHARE_NETDIR                          1
#define AUDIO_SHARE_IMERGE_NOTSUPPORTED             2
#define AUDIO_SHARE_SMS3_NOTSUPPORTED               3
#define AUDIO_SHARE_XPERINET                        4
#define AUDIO_SHARE_AXONIX                          5
#define AUDIO_SHARE_REQUEST4                        6
#define AUDIO_SHARE_IMERGE_S2000                    7

#define AUTO_ADDSUBCOMPONENT_MSG                    0
#define AUTO_ADDSUBCOMPONENT_DONE                   1
#define AUTO_ADDSUBCOMPONENT_FAIL                   2

#define AUDIO_ATTRIB_BASS                           0
#define AUDIO_ATTRIB_TREBLE                         1
#define AUDIO_ATTRIB_MODE_0                         2
#define AUDIO_ATTRIB_MODE_1                         3
#define AUDIO_ATTRIB_MODE_2                         4
#define AUDIO_ATTRIB_MODE_3                         5
#define AUDIO_ATTRIB_MODE_4                         6
#define AUDIO_ATTRIB_MODE_5                         7
#define AUDIO_ATTRIB_MODE_6                         8
#define AUDIO_ATTRIB_MODE_7                         9

#define HLM_AUTH_ENCRYPTEDKEY                       0
#define HLM_AUTH_NOLOGIN                            1

// Gateway/Brick defines: Check agains CPU list above
#define HLM_GATEWAY_FULL                            1
#define HLM_GATEWAY_BRICK_MASTER                    2
#define HLM_GATEWAY_BRICK_SLAVE                     3
#define HLM_GATEWAY_BRICK_APRILAIRE                 4
#define HLM_GATEWAY_BRICK_PASSTHROUGH               5
#define HLM_GATEWAY_FULL_X86C                       6
#define HLM_GATEWAY_FULL_ARMV4IA                    7

#define HC_12                                       12
#define HC_8                                        8
#define HC_6                                        6
#define HC_4                                        4

// Special case ID
#define HLID_NONE                                   0x7FFF

#define CAPTURE_NONE                                0
#define CAPTURE_PAGEUP                              1
#define CAPTURE_PAGEDN                              2
#define CAPTURE_SLIDER                              3

#define VIEW_FULL                                   0
#define VIEW_BOTTOM                                 1
#define VIEW_TOP                                    2

#define DIR_NONE                                    -1
#define DIR_UP                                      0
#define DIR_DOWN                                    1
#define DIR_LEFT                                    2
#define DIR_RIGHT                                   3
#define CORNER_LL                                   0
#define CORNER_UL                                   1
#define CORNER_UR                                   2
#define CORNER_LR                                   3
#define FILL_UP                                     0
#define FILL_DOWN                                   1
#define FILL_NONE                                   2

#define CORNER_LL_OUTSIDE                           0
#define CORNER_UL_OUTSIDE                           1
#define CORNER_UR_OUTSIDE                           2
#define CORNER_LR_OUTSIDE                           3
#define CORNER_LL_INSIDE                            4
#define CORNER_UL_INSIDE                            5
#define CORNER_UR_INSIDE                            6
#define CORNER_LR_INSIDE                            7

#define HL_TEXTBOLD                                 0x00000001
#define HL_TEXTBKSOLID                              0x00000002
#define HL_TEXTMULTILINE                            0x00000004
#define HL_TEXTPOS90                                0x00000008
#define HL_TEXTNEG90                                0x00000010

#define EDGE_TOP                                    0
#define EDGE_LEFT                                   1
#define EDGE_BOTTOM                                 2
#define EDGE_RIGHT                                  3

#define OMIT_TOPEDGE                                0x00000001
#define OMIT_ULCORNER                               0x00000010
#define OMIT_URCORNER                               0x00000020
#define OMIT_LLCORNER                               0x00000040
#define OMIT_LRCORNER                               0x00000080
#define OMIT_FACEFILL                               0x00000100

#define HR2_CODE_BOOT                               1
#define HR2_CODE_CODE                               2
#define HR2_CODE_FACTTEST                           3
#define HR2_CODE_RADIOCFG                           4
#define HR2_CODE_USBMOD                             5

#define HR2_POWER_TOGGLE                            0x00000001
#define HR2_MEDIASNAP_10SEC                         0x00000080
#define HR2_MEDIASNAP_30SEC                         0x00000100
#define HR2_MEDIASNAP_60SEC                         0x00000200
#define HR2_MEDIASNAP_120SEC                        0x00000400
#define HR2_TIMEOUT_MASK                            0xFF000000

#define IRBINDING_USER                              0x00000001
#define IRBINDING_READONLY                          0x00000002

#define TABLET_NOLIGHTSCHED                         0x00000002
#define TABLET_NOHVACSCHED                          0x00000004
#define TABLET_ENABLEPHOTOS                         0x00000080
#define TABLET_DEFAULTVOLUME50                      0x00000000
#define TABLET_DEFAULTVOLUME0                       0x00000800
#define TABLET_DEFAULTVOLUME25                      0x00001000
#define TABLET_DEFAULTVOLUME75                      0x00002000
#define TABLET_DEFAULTVOLUME100                     0x00004000
#define TABLET_ANNOUNCEVOLUME50                     0x00000000
#define TABLET_ANNOUNCEVOLUME0                      0x00008000
#define TABLET_ANNOUNCEVOLUME25                     0x00010000
#define TABLET_ANNOUNCEVOLUME75                     0x00020000
#define TABLET_ANNOUNCEVOLUME100                    0x00040000
#define TABLET_ANNOUNCESECURITY                     0x00080000
#define TABLET_VIDEO_DDRAW                          0x00400000
#define TABLET_VIDEO_NULL                           0x00800000
#define TABLET_ENABLEPIN                            0x01000000
#define TABLET_ENABLESCALEXFORMS                    0x02000000

#define TABLAYOUT_SHOWGROUPS                        0x00000001
#define TABLAYOUT_NAVDEFAULTPAGE                    0x00000002
#define TABLAYOUT_SHOWARROWS                        0x00000004
#define TABLAYOUT_PAGEFLIP                          0x00000008

#define CHAMFER_LL                                  0x00000001
#define CHAMFER_LR                                  0x00000002
#define CHAMFER_UL                                  0x00000004
#define CHAMFER_UR                                  0x00000008
#define CHAMFER_NONE                                0x00000000

#define CONTROL_CLASS_GENERAL                       0x00000001
#define CONTROL_CLASS_HOMEPAGE                      0x00000002
#define CONTROL_CLASS_AUDIO                         0x00000004
#define CONTROL_CLASS_ADV_GENERAL                   0x00000008
#define CONTROL_CLASS_EXCLUDE_AUDIO                 0x00000010
#define CONTROL_CLASS_OLE                           0x00000020
#define CONTROL_CLASS_OSD                           0x00000040
#define CONTROL_CLASS_HR2                           0x00000080
#define CONTROL_CLASS_HR2_HEADER                    0x00000100

#define CONTROL_PAGE_SECTION                        99

#define CONTROL_FLAG_FORCETRANSPARENT               0x00000001
#define CONTROL_FLAG_VERTICAL                       0x00000002
#define CONTROL_FLAG_DEFAULT_FACE_RGB               0x00000004
#define CONTROL_FLAG_DEFAULT_TEXT_RGB               0x00000008
#define CONTROL_FLAG_DEFAULT_BEHAVIOUR              0x00000010
#define CONTROL_FLAG_DEFAULT_RADIUS                 0x00000020
#define CONTROL_FLAG_DEFAULT_SHIN                   0x00000040
#define CONTROL_FLAG_DEFAULT_SHOUT                  0x00000080
#define CONTROL_FLAG_DEFAULT_TEXT_SIZE              0x00000100
#define CONTROL_FLAG_SCROLLABLE                     0x00000200
#define CONTROL_FLAG_IOS_COORDS                     0x00000400
#define CONTROL_FLAG_MULTILINE                      0x00000800

#define CONTROL_START                           100
#define CONTROL_STATICTEXT                          100

#define CONTROL_SYSTEMMODE_0                        101
#define CONTROL_SYSTEMMODE_1                        102
#define CONTROL_SYSTEMMODE_2                        103
#define CONTROL_SYSTEMMODE_TEXT                     104
#define CONTROL_WEATHER_FORECAST                    106
#define CONTROL_CLOCKWITHDATE                       107
#define CONTROL_CLOCKDATEONLY                       108
#define CONTROL_CLOCKTIMEONLY                       109
#define CONTROL_PICTURE                             110
#define CONTROL_3DTEXT                              111
#define CONTROL_LOGO                                113
#define CONTROL_LOGO_TRANS_OBSOLETE                 114
#define CONTROL_VTHUMB_OBSOLETE                     115
#define CONTROL_WEBPIC                              116
#define CONTROL_POOL_SUMMARY                        117
#define CONTROL_POOL_CIRSUMMARY                     118
#define CONTROL_HVAC_SUMMARY1                       119
#define CONTROL_HVAC_SUMMARY2                       120
#define CONTROL_HOMEPAGE_BUTTON                     121
#define CONTROL_SECPANEL                            122
#define CONTROL_HOMEPAGE_BUTTON_TOGGLE              123
#define CONTROL_AUDIO_BUTTON                        124
#define CONTROL_AUDIO_BUTTON_TOGGLE                 125
#define CONTROL_REGION                              126
#define CONTROL_REGION_OBSOLETE                     127
#define CONTROL_VSTREAM                             128
#define CONTROL_LIGHT_KEYPAD                        129
#define CONTROL_VSTREAM_VOLUME                      130
#define CONTROL_VSTREAM_RES                         132
#define CONTROL_SYSTEMMODE_3                        133
#define CONTROL_SYSTEMMODE_4                        134
#define CONTROL_SYSTEMMODE_5                        135
#define CONTROL_SYSTEMMODE_6                        136
#define CONTROL_SYSTEMMODE_7                        137
#define CONTROL_SYSTEMMODE_8                        138
#define CONTROL_SYSTEMMODE_9                        139
#define CONTROL_LIGHT_SWITCH                        140
#define CONTROL_LIGHT_DIMMER                        141
#define CONTROL_LIGHT_PRESET                        142
#define CONTROL_LIGHT_ROCKER1                       143
#define CONTROL_LIGHT_ROCKER2                       144
#define CONTROL_LIGHT_TOGGLE                        145
#define CONTROL_DVD_SELECTOR                        146
#define CONTROL_DIGITKEYPAD                         147
#define CONTROL_DVD_IMAGE_SELECTOR                  148
#define CONTROL_NUMERIC_TEXT                        149
#define CONTROL_HOMEPAGE_BUTTON_MOMENTARY           150
#define CONTROL_AUDIO_ADJUST                        151
#define CONTROL_AUDIO_MODECTRL                      152
#define CONTROL_HATSWITCH                           153
#define CONTROL_LOCALMIC_OBSOLETE                   155
#define CONTROL_HATSWITCH_COMPLEX                   156
#define CONTROL_HATSWITCH_COMPLEX_RECESSED_OBSOLETE 157
#define CONTROL_HATSWITCH_RECESSED_OBSOLETE         158
#define CONTROL_AUDIO_BUTTON_RECESSED_OBSOLETE      159
#define CONTROL_AUDIO_BUTTON_LOGO                   160
#define CONTROL_PERIP_TEXTVIEW                      161
#define CONTROL_VSTREAM_ABSPOS                      162
#define CONTROL_VSTREAM_ABSZOOM                     163
#define CONTROL_EMAIL_VIEWER                        166
#define CONTROL_EMAIL_VIEWER_OBSOLETE               167
#define CONTROL_ZONEKEYPAD                          168
#define CONTROL_AUDIO_BUTTON_INTERNAL_ZONE          169
#define CONTROL_GRAPHVIEW                           170
#define CONTROL_AUDIO_BUTTON_IMAGE                  171
#define CONTROL_HOMEPAGE_BUTTON_IMAGE               172
#define CONTROL_OUTPUT_TOGGLE                       173
#define CONTROL_OUTPUT_MOMENTARY                    174
#define CONTROL_INPUT_TOGGLE                        175
#define CONTROL_WEBPICLINK                          176
#define CONTROL_SHADE_PRESET                        177
#define CONTROL_SHADE_CONTROL                       178
#define CONTROL_LIGHT_GLOBALGRID                    179
#define CONTROL_CALENDAR                            180
#define CONTROL_VSTREAM_FULL                        182
#define CONTROL_TVCHANNELS1                         183
#define CONTROL_KEYPADKEYPAD                        184
#define CONTROL_AUDIO_BUTTON_INTERNAL_KEYPAD        185
#define CONTROL_GLOBALMIC_OBSOLETE                  186
#define CONTROL_STATUS_TEXT                         187
#define CONTROL_LIGHT_USERSCENEBUTTON               188
#define CONTROL_LIGHT_KEYPADSCENEBUTTON             189
#define CONTROL_AUDIO_LISTBOX_INTERNAL_KEYPAD       190
#define CONTROL_OUTPUT_VARIABLE                     191
#define CONTROL_TS2_CHANFAV                         192
#define CONTROL_TVCHANNELS2                         193
#define CONTROL_COMPOSITE_VIDEO                     194
#define CONTROL_TS2_JUMP_OBSOLETE                   195
#define CONTROL_TS2_BATTERY                         196
#define CONTROL_TS2_SOURCEICON                      197
#define CONTROL_TS2_CLOCK                           198
#define CONTROL_AUDIO_BUTTON_INTERNAL_KEYPAD_ABC    199
#define CONTROL_END                             199

#define CONTROL_TEXTVALID                           0x00000001
#define CONTROL_TEXTSIZEVALID                       0x00000002
#define CONTROL_TEXTCOLORVALID                      0x00000004
#define CONTROL_TEXTQUALVALID                       0x00000008
#define CONTROL_JPGVALID                            0x00000010
#define CONTROL_GIFVALID                            0x00000020
#define CONTROL_TEXTALIGNVALID                      0x00000040
#define CONTROL_LOCKCOLORBARS                       0x00000080
#define CONTROL_BORDERVALID                         0x00000100
#define CONTROL_SHADINGVALID                        0x00000200
#define CONTROL_STYLEVALID                          0x00000400
#define CONTROL_FUNCTIONVALID                       0x00000800
#define CONTROL_FACECOLORVALID                      0x00001000
#define CONTROL_ICONALIGNVALID                      0x00002000
#define CONTROL_TEXTQUALVALID_EXT                   0x00004000
#define CONTROL_ICONVALID                           0x00008000
#define CONTROL_FLAGSVALID                          0x00010000
#define CONTROL_ADVSHADINGVALID                     0x00020000
#define CONTROL_STYLEVARIABLE                       0x00040000
#define CONTROL_DEFBEHAVEVALID                      0x00080000

#define CONTROL_BORDER_NONE                         0
#define CONTROL_BORDER_DEFAULT                      1

#define DIMMER_STYLE_HZ_ARROW                       0
#define DIMMER_STYLE_HZ_BULB                        1
#define DIMMER_STYLE_VT_ARROW                       2
#define DIMMER_STYLE_VT_BULB                        3

#define DIGITKEYPAD_ENTER                           0x00000001
#define DIGITKEYPAD_CANCEL                          0x00000002
#define DIGITKEYPAD_POUND                           0x00000004
#define DIGITKEYPAD_ASTERIX                         0x00000008

#define DRAWICON_DISABLED                           0x00000001
#define DRAWICON_NOCACHE                            0x00000002
#define DRAWICON_NOBLUR                             0x00000004
#define DRAWICON_SOLIDBG                            0x00000008
#define DRAWICON_INVERTALPHA                        0x00000010
#define DRAWICON_COPYALPHA                          0x00000020
#define DRAWICON_GHOSTMODE                          0x00000040
#define DRAWICON_INVERTED                           0x00000080

#define AUDIO_ICON_START                        0
#define AUDIO_ICON_TEXT                             0
#define AUDIO_ICON_PLAY                             1
#define AUDIO_ICON_STOP                             2
#define AUDIO_ICON_PAUSE                            3
#define AUDIO_ICON_RIGHT                            4
#define AUDIO_ICON_LEFT                             5
#define AUDIO_ICON_UP                               6
#define AUDIO_ICON_DOWN                             7
#define AUDIO_ICON_FFWD                             8
#define AUDIO_ICON_RWND                             9
#define AUDIO_ICON_SKIP_FWD                         10
#define AUDIO_ICON_SKIP_BACK                        11
#define AUDIO_ICON_RECORD                           12
#define AUDIO_ICON_POWER                            13
#define AUDIO_ICON_EJECT                            14
#define AUDIO_ICON_SKIP_UP                          15
#define AUDIO_ICON_SKIP_DOWN                        16
#define AUDIO_ICON_UPDOWN                           17
#define AUDIO_ICON_LEFTRIGHT                        18
#define AUDIO_ICON_PLUS                             19
#define AUDIO_ICON_MINUS                            20
#define AUDIO_ICON_CHECK                            21
#define AUDIO_ICON_FROMFILE                         22
#define AUDIO_ICON_SHUFFLE                          23
#define AUDIO_ICON_REPEAT                           24
#define AUDIO_ICON_XPORT_HOME                       25
#define AUDIO_ICON_REPLAY                           26
#define AUDIO_ICON_ADVANCE                          27

#define AUDIO_ICON_END                          27

#define PRIM_START                              0
#define PRIM_RECT                                   0
#define PRIM_LARROW                                 1
#define PRIM_RARROW                                 2
#define PRIM_UARROW                                 3
#define PRIM_DARROW                                 4
#define PRIM_CIRCLE                                 5
#define PRIM_PGRAM_HZ1                              6
#define PRIM_PGRAM_HZ2                              7
#define PRIM_PGRAM_VT1                              8
#define PRIM_PGRAM_VT2                              9
#define PRIM_NOP                                    10
#define PRIM_INV_CIRCLE                             11
#define PRIM_END                                11

// Hatswitch options
#define HATSWITCH_NOCORNERS                         0x00000001
#define HATSWITCH_RECESSED                          0x00000002

#define HATBUTTON_START                             0
#define HATBUTTON_UP                                0
#define HATBUTTON_DOWN                              1
#define HATBUTTON_LEFT                              2
#define HATBUTTON_RIGHT                             3
#define HATBUTTON_ENTER                             4
#define HATBUTTON_UL                                5
#define HATBUTTON_UR                                6
#define HATBUTTON_LL                                7
#define HATBUTTON_LR                                8
#define HATBUTTON_PLAY                              100
#define HATBUTTON_ADD                               101
#define HATBUTTON_PGUP                              102
#define HATBUTTON_PGDN                              103
#define HATBUTTON_ESCAPE                            104
#define HATBUTTON_RELEASE                           105
#define HATBUTTON_STOP                              106
#define HATBUTTON_PAUSE                             107
#define HATBUTTON_DIGIT0                            110
#define HATBUTTON_DIGIT1                            111
#define HATBUTTON_DIGIT2                            112
#define HATBUTTON_DIGIT3                            113
#define HATBUTTON_DIGIT4                            114
#define HATBUTTON_DIGIT5                            115
#define HATBUTTON_DIGIT6                            116
#define HATBUTTON_DIGIT7                            117
#define HATBUTTON_DIGIT8                            118
#define HATBUTTON_DIGIT9                            119
#define HATBUTTON_VOLUP                             120
#define HATBUTTON_VOLDN                             121
#define HATBUTTON_MENU                              122
#define HATBUTTON_END                               122

#define SHADEPRIM_RECT                              0
#define SHADEPRIM_CORNER                            1
#define SHADEPRIM_EDGE                              2
#define SHADEPRIM_OTHER                             3

// Video Client Flags
#define VCLIENT_MPEG1_OK                            0x00000001
#define VCLIENT_MPEG2_OK                            0x00000002
#define VCLIENT_FULLFRAME_JPEGOK                    0x00000004
#define VCLIENT_FULLFRAME_YUY2OK                    0x00000008
#define VCLIENT_MAXPUSHRATE                         0x00000010


// Forecast unknown
#define FC_VALUE_UNKNOWN                            10000
#define TRACK_LEN_CALLBACK                          0x0000FFFF

#define TIMEOUT_QA_RESPONSE                         15000
#define REPEAT_DELAY                                500
#define INIT_REPEAT                                 250
#define REPEAT_RAMP_TIME                            1000
#define FAST_REPEAT                                 10
#define SLOW_REPEAT                                 100


// Commands used in multiple windows
#define CMD_DIG0                                    11000
#define CMD_DIG1                                    11001
#define CMD_DIG2                                    11002
#define CMD_DIG3                                    11003
#define CMD_DIG4                                    11004
#define CMD_DIG5                                    11005
#define CMD_DIG6                                    11006
#define CMD_DIG7                                    11007
#define CMD_DIG8                                    11008
#define CMD_DIG9                                    11009
#define CMD_KPCANCEL                                11010
#define CMD_KPENTER                                 11011
#define CMD_CLOSE                                   11012
#define CMD_REFRESH_FB                              11013
#define CMD_REFRESH_BB                              11014
#define CMD_NONE                                    11015
#define CMD_PAGE_VOICEMSG                           11016
#define CMD_PAGE_INCOMING                           11017
#define CMD_PAGE_GREETING                           11018
#define CMD_PAGE_ADDRBOOK                           11019
#define CMD_MAILBOX_HOUSE                           11020
#define CMD_MAILBOX_USER1                           11021
#define CMD_MAILBOX_USER2                           11022    // these are placeholders,
#define CMD_MAILBOX_USER3                           11023    // must be consecutive following CMD_MAILBOX_USER1
#define CMD_MAILBOX_USER4                           11024    //
#define CMD_MAILBOX_USER5                           11025    //
#define CMD_MAILBOX_USER6                           11026    //
#define CMD_MAILBOX_USER7                           11027    //
#define CMD_MAILBOX_USER8                           11028    //
#define NMAX_USER_MAILBOXES                         8
#define CMD_LIGHTS_EXECUTE                          11029
#define CMD_LIGHTS_RESTORE                          11030
#define CMD_ADD                                     11031
#define CMD_REMOVE                                  11032
#define CMD_TREE                                    11033
#define CMD_DROPLIST                                11034
#define CMD_CHANGE                                  11035
#define CMD_RESET_DEVICE                            11036
#define CMD_STATUSCHANGED                           11037
#define CMD_SETWORKING                              11038
#define CMD_SCAN                                    11039
#define CMD_CANCEL                                  11040
#define CMD_OK                                      11041
#define CMD_TIMECHANGED                             11042


#define NODE_COM_BASE                               0
#define NODE_COM                                    1
#define NODE_SYSCOMP_BASE                           2
#define NODE_SYSCOMP                                3
#define NODE_TRIG_BASE                              100
#define NODE_TRIG                                   101
#define NODE_CMD_BASE                               102
#define NODE_CMD                                    103
#define NODE_IFACE_BASE                             200
#define NODE_IFACE                                  201
#define NODE_OPTIONS                                202
#define NODE_TABLAYOUT                              203
#define NODE_CUSTOMTAB                              204
#define NODE_IFACE1                                 205
#define NODE_IFACE2                                 206

// Messaging modes
#define MODE_PHONE                                  0
#define MODE_EMAIL                                  1

// System log types
#define LOG_SYSTEM                                  0
#define LOG_EXCEPTIONS                              1

// Web site image indexes
#define IMG_CATEGORY                                0
#define IMG_SITE                                    1
#define IMG_CONN                                    2

// Window tracking
#define TRACK_TOP_ABS                               0
#define TRACK_LEFT_ABS                              1
#define TRACK_RIGHT_ABS                             2
#define TRACK_BOTTOM_ABS                            3
#define TRACK_LEFTRIGHT_PROP                        4
#define TRACK_TOPBOTTOM_PROP                        5
#define TRACK_FIXED_WIDTH                           6
#define TRACK_FIXED_HEIGHT                          7

#define TYPE_STRUCTURE                              0
#define TYPE_LEVEL                                  1
#define TYPE_AREA                                   2

// IO Numeric States
#define NUMERIC_TRIG_LESSTHAN                       0
#define NUMERIC_TRIG_GREATERTHAN                    1
#define NUMERIC_TRIG_INRANGE                        2
#define NUMERIC_TRIG_OUTRANGE                       3
#define NUMERIC_TRIG_EQUALS                         4

#ifndef HL_PLATFORM_SMALL
    #define PHONE_BTN_BORDER                        5
    #define PHONE_BUTTON_BORDER                     5
    #define PHONE_DY_BUTTONS                        50
    #define PHONE_DY_INFO                           65
    #define PHONE_TEXT_INFO                         25
    #define PHONE_DX_UPDOWN                         30
#endif

#define SERVICE_DY_TITLE                            25

#define AUDIO_KP_TEXTSIZE                           30

#define LIGHT_TYPE_KEYPAD                           0
#define LIGHT_TYPE_SWITCH                           1

// HVAC defines
#define HVAC_MAX_TEMP                               200
#define HVAC_MIN_TEMP                               -30
#define HVAC_TYPE_OFF                               0
#define HVAC_TYPE_NOTREADY                          1
#define HVAC_TYPE_HEAT                              2
#define HVAC_TYPE_COOL                              3
#define HVAC_TYPE_SINGLESET                         4

#define HVAC_SCALE_F                                0
#define HVAC_SCALE_C                                1

#define HVAC_REMOTE_PRESENT                         0x0001
#define HVAC_REMOTE_PCT                             0x0002
#define HVAC_REMOTE_ALARM                           0x0004
#define HVAC_REMOTE_CONTROL                         0x0008

// Pool interface flags
#define POOL_TAB_LIGHTS                             0x00000001
#define POOL_TAB_CONFIGCOLORS                       0x00000002
#define POOL_TAB_SCHEDULE                           0x00000004
#define POOL_TAB_RUNONCE                            0x00000008
#define POOL_TAB_EGGTIMER                           0x00000010
#define POOL_TAB_HISTORY                            0x00000020
#define POOL_TAB_EQUIPMENT                          0x00000040
#define POOL_TAB_ALL                                0x0000007F

// Pool equip flags
#define POOL_SOLARPRESENT                           0x00000001
#define POOL_SOLARHEATPUMP                          0x00000002
#define POOL_CHLORPRESENT                           0x00000004
#define POOL_IBRITEPRESENT                          0x00000008
#define POOL_IFLOWPRESENT0                          0x00000010
#define POOL_IFLOWPRESENT1                          0x00000020
#define POOL_IFLOWPRESENT2                          0x00000040
#define POOL_IFLOWPRESENT3                          0x00000080
#define POOL_IFLOWPRESENT4                          0x00000100
#define POOL_IFLOWPRESENT5                          0x00000200
#define POOL_IFLOWPRESENT6                          0x00000400
#define POOL_IFLOWPRESENT7                          0x00000800
#define POOL_NO_SPECIAL_LIGHTS                      0x00001000
#define POOL_HEATPUMPHASCOOL                        0x00002000
#define POOL_MAGICSTREAMPRESENT                     0x00004000
#define POOL_ICHEMPRESENT                           0x00008000

// Pool Circuit Types
#define POOLCIRCUIT_GENERIC                         0
#define POOLCIRCUIT_SPA                             1
#define POOLCIRCUIT_POOL                            2
#define POOLCIRCUIT_SPA_SECOND                      3
#define POOLCIRCUIT_POOL_SECOND                     4
#define POOLCIRCUIT_CLEANER                         5
#define POOLCIRCUIT_CLEANER_SECOND                  6
#define POOLCIRCUIT_LIGHT                           7
#define POOLCIRCUIT_DIMMER                          8     // dimmer steps= 0,30,40..90,100%
#define POOLCIRCUIT_SAM                             9
#define POOLCIRCUIT_SAL                             10
#define POOLCIRCUIT_PHOTON                          11
#define POOLCIRCUIT_COLOR_WHEEL                     12
#define POOLCIRCUIT_VALVE                           13
#define POOLCIRCUIT_SPILLWAY                        14
#define POOLCIRCUIT_FLOORCLEANER                    15
#define POOLCIRCUIT_INTELLIBRITE                    16
#define POOLCIRCUIT_MAGICSTREAM                     17
#define POOLCIRCUIT_DIMMER_25                       18    // dimmer steps= 0,25,50,75,100%
#define POOLCIRCUIT_UNUSED                          19
#define POOLCIRCUIT_LAST_ID                         19


// Pool Circuit Flags
#define POOLCIRCUITFLAG_FREEZE                      0x00000001
#define POOLCIRCUITFLAG_DARK                        0x00000002

// Pool 2-Speed Triggers
#define POOL_2SPDTRIG_SOLAR                         128
#define POOL_2SPDTRIG_HEATER                        129
#define POOL_2SPDTRIG_POOLHEATER                    130
#define POOL_2SPDTRIG_SPAHEATER                     131
#define POOL_2SPDTRIG_FREEZE                        132

// Pool Remote
#define POOL_REMOTE_HEATBOOST                       133
#define POOL_REMOTE_HEATENABLE                      134
#define POOL_REMOTE_INC_PUMPSPD                     135
#define POOL_REMOTE_DEC_PUMPSPD                     136

// Special IntelliFlo circuits
#define POOL_POOL_HEATER                            155
#define POOL_SPA_HEATER                             156
#define POOL_EITHER_HEATER                          157
#define POOL_SOLAR                                  158
#define POOL_FREEZE                                 159

#define POOL_4SPEEDIFLOW                            0x80

// Pool heater modes
#define BODYTYPE_POOL                               0
#define BODYTYPE_SPA                                1

// Pool Heat Modes
#define POOLHEAT_OFF                                0
#define POOLHEAT_SOLAR                              1
#define POOLHEAT_SOLARPREF                          2
#define POOLHEAT_HEAT                               3
#define POOLHEAT_DONTCHANGE                         4

#define POOLSCHED_RUNONCE                           1   // Run-Once schedule item
#define POOLSCHED_HEATSP                            2   // Change setpoint when run
#define POOLSCHED_SMARTSTART                        4   // Stupid Smart-Start

// Pool Heat Active States
#define HEAT_NONE_ACTIVE                            0
#define HEAT_SOLAR_ACTIVE                           1
#define HEAT_HEATER_ACTIVE                          2
#define HEAT_BOTH_ACTIVE                            3

#define POOLINT_POOL                                0
#define POOLINT_SPA                                 1
#define POOLINT_GENERAL                             2
#define POOLINT_SYNCSWIM                            3
#define POOLINT_LIGHT                               4
#define POOLINT_DONTSHOW                            5
#define POOLINT_INVALID                             6

#define POOLCMD_ALLOFF                              0
#define POOLCMD_ALLON                               1
#define POOLCMD_SET                                 2
#define POOLCMD_SYNC                                3
#define POOLCMD_SWIM                                4
#define POOLCMD_PARTY                               5     // IntelliBrite
#define POOLCMD_ROMANCE                             6     //    "
#define POOLCMD_CARIBBEAN                           7     //    "
#define POOLCMD_AMERICAN                            8     //    "
#define POOLCMD_SUNSET                              9     //    "
#define POOLCMD_ROYALTY                             10    //    "
#define POOLCMD_SAVE                                11    //    "
#define POOLCMD_RECALL                              12    //    "
#define POOLCMD_BLUE                                13    //    "
#define POOLCMD_GREEN                               14    //    "
#define POOLCMD_RED                                 15    //    "
#define POOLCMD_WHITE                               16    //    "
#define POOLCMD_MAGENTA                             17    //    "
#define POOLCMD_MS_THUMPER                          18    // Magic Stream (Laminar LED)
#define POOLCMD_MS_NEXT_MODE                        19    //    "
#define POOLCMD_MS_RESET                            20    //    "
#define POOLCMD_MS_HOLD                             21    //    "

#define IRCODE_CANCEL_LEARNMODE                     0
#define IRCODE_ENTER_LEARNMODE                      1
#define IRCODE_ACCEPT_LEARNMODE                     2
#define IRCODE_ENTER_AUTOLEARNMODE                  3

#define DISC_UNKNOWN                                0
#define DISC_CD                                     1
#define DISC_VCD                                    2
#define DISC_DVD                                    3
#define DISC_BD										4
#define DISC_NODISC									0xFF

#define HLKEY_VOLUP                                 1000
#define HLKEY_VOLDOWN                               1001
#define HLKEY_PLAYPAUSE                             1002
#define HLKEY_MENU                                  1003
#define HLKEY_INFO                                  1004


#define ICON_SEARCH                                 _T("MAIN\\96PT\\MEDIALIB\\SEARCH")
#define ICON_FAVS                                   _T("MAIN\\96PT\\MEDIALIB\\FAVORITES")
#define ICON_FAVS_ADD                               _T("MAIN\\96PT\\MEDIALIB\\FAVORITES_ADD")

#define ICON_LIST_PLAY                              _T("MAIN\\96PT\\TRANSPORT\\LIST_PLAY")
#define ICON_LIST_ARTIST_ALBUM                      _T("MAIN\\96PT\\TRANSPORT\\LIST_ARTIST_ALBUM")
#define ICON_LIST_TRACK                             _T("MAIN\\96PT\\TRANSPORT\\LIST_TRACK")
#define ICON_LIST_PLUS                              _T("MAIN\\96PT\\TRANSPORT\\LIST_PLUS")
#define ICON_LIST_MINUS                             _T("MAIN\\96PT\\TRANSPORT\\LIST_MINUS")
#define ICON_FFWD                                   _T("MAIN\\96PT\\TRANSPORT\\FFWD")
#define ICON_RWND                                   _T("MAIN\\96PT\\TRANSPORT\\RWD")
#define ICON_RECORD                                 _T("MAIN\\96PT\\TRANSPORT\\RECORD")
#define ICON_NEXT                                   _T("MAIN\\96PT\\TRANSPORT\\TRACK_FORWARD")
#define ICON_PREV                                   _T("MAIN\\96PT\\TRANSPORT\\TRACK_BACK")

#define ICON_KEYPAD                                 _T("MAIN\\96PT\\MEDIALIB\\KEYPAD")
#define ICON_ALBUM                                  _T("MAIN\\96PT\\MEDIALIB\\ALBUM")
#define ICON_ARTIST                                 _T("MAIN\\96PT\\MEDIALIB\\ARTIST")
#define ICON_ARTIST_ALBUM                           _T("MAIN\\96PT\\MEDIALIB\\ARTIST_ALBUM")
#define ICON_GENRE                                  _T("MAIN\\96PT\\MEDIALIB\\GENRE")
#define ICON_TRACK                                  _T("MAIN\\96PT\\MEDIALIB\\TRACK")
#define ICON_SHOUTCAST                              _T("MAIN\\96PT\\MEDIALIB\\SHOUTCAST")
#define ICON_PANDORA                                _T("MAIN\\96PT\\MEDIALIB\\PANDORA")
#define ICON_RHAPSODY                               _T("MAIN\\96PT\\MEDIALIB\\RHAPSODY")
#define ICON_TUNEIN                                 _T("MAIN\\96PT\\MEDIALIB\\TUNEIN")
#define ICON_NOWPLAYING                             _T("MAIN\\96PT\\MEDIALIB\\NOWPLAYING")
#define ICON_PLAYLIST                               _T("MAIN\\96PT\\MEDIALIB\\PLAYLIST")
#define ICON_VOL_MUTE                               _T("MAIN\\96PT\\SIDEBAR\\VOL_MUTE")
#define ICON_VOL_UP                                 _T("MAIN\\96PT\\SIDEBAR\\VOL_UP")
#define ICON_VOL_DN                                 _T("MAIN\\96PT\\SIDEBAR\\VOL_DN")
#define ICON_SOURCE                                 _T("MAIN\\96PT\\SIDEBAR\\SOURCE")
#define ICON_PUMPS                                  _T("MAIN\\96PT\\SIDEBAR\\PUMPS")
#define ICON_SETTINGS                               _T("MAIN\\96PT\\SIDEBAR\\SETTINGS")
#define ICON_HISTORY                                _T("MAIN\\96PT\\SIDEBAR\\HISTORY")
#define ICON_HVACTAB                                _T("MAIN\\96PT\\SIDEBAR\\TEMP")
#define ICON_STATUS                                 _T("MAIN\\96PT\\SIDEBAR\\STATUS")
#define ICON_SCHEDULE                               _T("MAIN\\96PT\\SIDEBAR\\SCHEDULE")
#define ICON_EXIT                                   _T("MAIN\\96PT\\SIDEBAR\\EXIT")
#define ICON_DELETE_MSG                             _T("MAIN\\96PT\\SIDEBAR\\DELETE MSSG")
#define ICON_MARK_MSG                               _T("MAIN\\96PT\\SIDEBAR\\MARK MSSG")
#define ICON_READ_MSG                               _T("MAIN\\96PT\\SIDEBAR\\READ MSSG")
#define ICON_SUMMARY                                _T("MAIN\\96PT\\SIDEBAR\\SUMMARY")
#define ICON_ZONES                                  _T("MAIN\\96PT\\SIDEBAR\\ZONES")
#define ICON_SIDEBAR_LIVE                           _T("MAIN\\96PT\\SIDEBAR\\LIVE")
#define ICON_SIDEBAR_OFF                            _T("MAIN\\96PT\\SIDEBAR\\SIDEBAR_OFF")
#define ICON_SIDEBAR_DVR                            _T("MAIN\\96PT\\SIDEBAR\\DVR")
#define ICON_RECORD                                 _T("MAIN\\96PT\\TRANSPORT\\RECORD")
#define ICON_REWIND                                 _T("MAIN\\96PT\\TRANSPORT\\RWD")
#define ICON_PLAY                                   _T("MAIN\\96PT\\TRANSPORT\\PLAY")
#define ICON_DOWN                                   _T("MAIN\\96PT\\TRANSPORT\\DOWN")
#define ICON_UP                                     _T("MAIN\\96PT\\TRANSPORT\\UP")
#define ICON_DELETE_ALL                             _T("MAIN\\96PT\\TRANSPORT\\DELETE ALL")
#define ICON_DELETE                                 _T("MAIN\\96PT\\TRANSPORT\\DELETE")
#define ICON_PAUSE                                  _T("MAIN\\96PT\\TRANSPORT\\PAUSE")
#define ICON_RANDOM                                 _T("MAIN\\96PT\\TRANSPORT\\RANDOM")
#define ICON_REPEAT                                 _T("MAIN\\96PT\\TRANSPORT\\REPEAT")
#define ICON_REPLAY                                 _T("MAIN\\96PT\\TRANSPORT\\REPLAY")
#define ICON_STOP                                   _T("MAIN\\96PT\\TRANSPORT\\STOP")
#define ICON_LEFT                                   _T("MAIN\\96PT\\TRANSPORT\\LEFT")
#define ICON_RIGHT                                  _T("MAIN\\96PT\\TRANSPORT\\RIGHT")
#define ICON_XPORT_HOME                             _T("MAIN\\96PT\\TRANSPORT\\HOME")
#define ICON_ADVANCE                                _T("MAIN\\96PT\\TRANSPORT\\ADVANCE")
#define ICON_CHECK                                  _T("MAIN\\96PT\\TRANSPORT\\CHECK")
#define ICON_EJECT                                  _T("MAIN\\96PT\\TRANSPORT\\EJECT")
#define ICON_POWER                                  _T("MAIN\\96PT\\TRANSPORT\\POWER")
#define ICON_SKIP_UP                                _T("MAIN\\96PT\\TRANSPORT\\UP")
#define ICON_SKIP_DOWN                              _T("MAIN\\96PT\\TRANSPORT\\DOWN")
#define ICON_UPDOWN                                 _T("MAIN\\96PT\\TRANSPORT\\UP_DOWN")
#define ICON_LEFTRIGHT                              _T("MAIN\\96PT\\TRANSPORT\\LEFT_RIGHT")

#define ICON_TOPBAR                                 _T("MAIN\\96PT\\MISC\\TOPBAR")
#define ICON_TOPBAR_LG                              _T("MAIN\\96PT\\MISC\\TOPBAR_LG")
#define ICON_SIDEBAR                                _T("MAIN\\96PT\\MISC\\SIDEBAR")
#define ICON_G                                      _T("MAIN\\96PT\\MISC\\G")
#define ICON_PFISH                                  _T("MAIN\\96PT\\MISC\\PFISH")    // Pentair Fish


#define ICON_HOME                                   _T("MAIN\\128PT\\SYS\\HOME_MAIN")
#define ICON_AUDIO                                  _T("MAIN\\128PT\\SYS\\MEDIA")
#define ICON_HVAC                                   _T("MAIN\\128PT\\SYS\\CLIMATE")
#define ICON_IRRIGATION                             _T("MAIN\\128PT\\SYS\\IRRIGATION")
#define ICON_LIGHTING                               _T("MAIN\\128PT\\SYS\\LIGHTING")
#define ICON_MESSAGING                              _T("MAIN\\128PT\\SYS\\MESSAGING")
#define ICON_SECURITY                               _T("MAIN\\128PT\\SYS\\SECURITY")
#define ICON_VIDEO                                  _T("MAIN\\128PT\\SYS\\VIDEO")
#define ICON_INTERNET                               _T("MAIN\\128PT\\SYS\\HOME_MAIN")
#define ICON_POOL                                   _T("MAIN\\128PT\\SYS\\POOL_SPA")
#define ICON_PHOTO                                  _T("MAIN\\128PT\\SYS\\PHOTOS")

#define ICON_SIMPLE_LIGHTING                        _T("MAIN\\96PT\\OVERVIEW\\LIGHTING")
#define ICON_SIMPLE_SCHEDULE                        _T("MAIN\\96PT\\OVERVIEW\\SCHEDULE")
#define ICON_SIMPLE_PHONE                           _T("MAIN\\96PT\\OVERVIEW\\CALL")
#define ICON_SIMPLE_EMAIL                           _T("MAIN\\96PT\\OVERVIEW\\EMAIL")
#define ICON_SIMPLE_PHONECONFIG                     _T("MAIN\\96PT\\OVERVIEW\\CALLSETTINGS")
#define ICON_SIMPLE_CALLLOG                         _T("MAIN\\96PT\\OVERVIEW\\CALLLOG")
#define ICON_SIMPLE_VIDEO                           _T("MAIN\\96PT\\OVERVIEW\\CAMERA")
#define ICON_SIMPLE_SECURITY                        _T("MAIN\\96PT\\OVERVIEW\\SECURITY")
#define ICON_SIMPLE_HVAC                            _T("MAIN\\96PT\\OVERVIEW\\CLIMATE")
#define ICON_SIMPLE_IRRIGATION                      _T("MAIN\\96PT\\OVERVIEW\\IRRIGATION")

#define ICON_SIRIUS                                 _T("MEDIA\\SIRIUS")
#define ICON_XM                                     _T("MEDIA\\XM")
#define ICON_MOVIES                                 _T("MEDIA\\MOVIE")

//
// Variants
//
#define ICON_MESSAGING_EMAIL                        _T("MAIN\\128PT\\SYS\\MESSAGING_EMAIL")
#define ICON_MESSAGING_VOICEMAIL                    _T("MAIN\\128PT\\SYS\\MESSAGING_VOICEMAIL")

#define ICON_CLIMATE_OFF                            _T("MAIN\\128PT\\SYS\\CLIMATE_OFF")
#define ICON_CLIMATE_HEAT                           _T("MAIN\\128PT\\SYS\\CLIMATE_HEAT")
#define ICON_CLIMATE_EHEAT                          _T("MAIN\\128PT\\SYS\\CLIMATE_HEAT")  // temp, need a unique icon
#define ICON_CLIMATE_COOL                           _T("MAIN\\128PT\\SYS\\CLIMATE_COOL")
#define ICON_CLIMATE_AUTO                           _T("MAIN\\128PT\\SYS\\CLIMATE_AUTO")
#define ICON_CLIMATE_VENT                           _T("MAIN\\128PT\\SYS\\CLIMATE_AUTO")  // temp, need a unique icon

#ifndef HL_PLATFORM_SMALL
    #define GIF_LBADD                               _T("add_now.gif")
    #define GIF_LBPLAY                              _T("play_now.gif")
    #define GIF_LBLISTEN                            _T("playrad.gif")
    #define SEARCHING_TEXT                          _T("Searching...")
#endif

// Audio list box defs
#define ALB_DRAW_CACHE_SIZE                         10
#define ALB_MAX_PENDING                             50
#define ALB_SEARCHING_TEXT_SIZE                     12
#define ALB_HL_TIME                                 2000
#define ALB_AUDIOLB_HEIGHT                          22
#define ALB_DX_VSCROLL_BIG                          32
#define ALB_DX_VSCROLL_SM                           20

#define PHONE_RGB_NEXTPREV                          RGB(100,100,255)
#define PHONE_RGB_D                                 RGB(20,20,20)
#define PHONE_RGB_R                                 RGB(150,0,0)
#define PHONE_RGB_G                                 RGB(0,150,0)
#define PHONE_RGB_B                                 RGB(0,0,150)
#define RGB_NONE                                    RGB(1,1,1)


// Socket defs
#define MSG_IDLE                                    0
#define MSG_READY                                   1
#define MSG_PROCESSING                              2
#define MSG_ERROR                                   3

// Audio lib wnd defs
#define CALW_CMD_ARTIST                             1000
#define CALW_CMD_ARTIST_ADD                         1001
#define CALW_CMD_ARTIST_PLAY                        1002
#define CALW_CMD_ALBUM                              1003
#define CALW_CMD_ALBUM_ADD                          1004
#define CALW_CMD_ALBUM_PLAY                         1005
#define CALW_CMD_TRACK                              1006
#define CALW_CMD_TRACK_ADD                          1007
#define CALW_CMD_TRACK_PLAY                         1008
#define CALW_CMD_PLAYLIST_ADD                       1009
#define CALW_CMD_PLAYLIST_PLAY                      1010
#define CALW_CMD_GENRE                              1011
#define CALW_CMD_STATION                            1012
#define CALW_CMD_STATION_PLAY                       1013
#define CALW_CMD_ARTIST_DATADONE                    2000
#define CALW_CMD_ALBUM_DATADONE                     2001
#define CALW_CMD_TRACK_DATADONE                     2002
#define CALW_CMD_PLAYLIST_DATADONE                  2003
#define CALW_CMD_GENRE_DATADONE                     2004
#define CALW_CMD_STATION_DATADONE                   2005
#define CALW_CMD_FAV                                3000
#define CALW_CMD_PLAYLIST                           3001
#define CALW_CMD_SAVENEW                            3002
#define CALW_CMD_OVERWRITE                          3003
#define CALW_CMD_DELETE                             3004
#define CALW_CMD_NAME                               3005
#define CALW_CMD_RENAME                             3006
#define CALW_CMD_RENAME_DONE                        3007
#define CALW_BORDER                                 5
#define CALW_DY_TEXT                                25
#define CALW_DY_BUTTON                              25
#define CALW_TEXT_SIZE                              18


//
// Days.
//
#define HL_DAY_INVALID                          -1
#define HL_DAYS_START                           0
#define HL_MONDAY                                   0
#define HL_TUESDAY                                  1
#define HL_WEDNESDAY                                2
#define HL_THURSDAY                                 3
#define HL_FRIDAY                                   4
#define HL_SATURDAY                                 5
#define HL_SUNDAY                                   6
#define HL_HOLIDAY                                  7
#define HL_DAYS_END                             7

// Irrigation days
#define HL_DAYS_DAYOFWEEK                           0x00000001L
#define HL_DAYS_EVENMONTHDAYS                       0x00000002L
#define HL_DAYS_ODDMONTHDAYS                        0x00000003L
#define HL_DAYS_SKIP2DAYS                           0x00000004L
#define HL_DAYS_SKIP3DAYS                           0x00000005L
#define HL_DAYS_SKIP4DAYS                           0x00000006L
#define HL_DAYS_SKIP5DAYS                           0x00000007L
#define HL_DAYS_MONDAY                              0x00010000L
#define HL_DAYS_TUESDAY                             0x00020000L
#define HL_DAYS_WEDNESDAY                           0x00040000L
#define HL_DAYS_THURSDAY                            0x00080000L
#define HL_DAYS_FRIDAY                              0x00100000L
#define HL_DAYS_SATURDAY                            0x00200000L
#define HL_DAYS_SUNDAY                              0x00400000L
#define HL_DAYS_EVERYDAY                            0x007F0000L

// IO Templates
#define IRTEMPLATE_START                    1000
#define IRTEMPLATE_HLUNIVERSAL                      1000
#define IRTEMPLATE_AMFMTUNER                        1001
#define IRTEMPLATE_CABLE                            1002
#define IRTEMPLATE_CDPLAYER                         1003
#define IRTEMPLATE_DVD                              1004
#define IRTEMPLATE_DVD_CHANGER                      1005
#define IRTEMPLATE_IRRECEIVER                       1006
#define IRTEMPLATE_IRTV                             1007
#define IRTEMPLATE_TIVO                             1008
#define IRTEMPLATE_VCR                              1009
#define IRTEMPLATE_MEDIACENTER                      1010
#define IRTEMPLATE_HLUI                             1011
#define IRTEMPLATE_DISH                             1012
#define IRTEMPLATE_DIRECTV                          1013
#define IRTEMPLATE_FIOS                             1014
#define IRTEMPLATE_BLU_RAY                          1015
#define IRTEMPLATE_APPLETV                          1016
#define IRTEMPLATE_END                      1016


//
// Schedule types: used to define the type for a CScheduleOwner.
//
#define HLSCHED_NORMAL                              1
#define HLSCHED_HVAC                                2

//
// Connections, interfaces and bridges:
//

#define WEB_CONNECT_BROADBAND                       0
#define WEB_CONNECT_DIALUP                          1

// - Device ConnectionTypes
#define DEVICE_CONNECT_UNDEFINED                    0
#define DEVICE_CONNECT_SERIAL                       1
#define DEVICE_CONNECT_NETWORK                      2
#define DEVICE_CONNECT_USB                          3
#define DEVICE_CONNECT_NETWORKBRIDGE                4
#define DEVICE_CONNECT_GATEWAY                      5
#define DEVICE_CONNECT_INTERNAL                     6

// - Bridge types
#define BRIDGE_COMPORT                              0
#define BRIDGE_UDS10                                1
#define BRIDGE_HOMELOGIC                            2
#define BRIDGE_ETHERNET                             3
#define BRIDGE_GLOBALCACHE                          4
#define BRIDGE_GATEWAY_REMOTE                       5
#define BRIDGE_MOXA_5610                            6

// Passthrough port
#define HL_PASSTHROUGH_PORT                         10001


//
// Serial port defines
//
#define HLPROTOCOL_RS232                            0
#define HLPROTOCOL_RS485                            1
#define HLPROTOCOL_RS485_HALF                       2

#define HLFLOWC_NONE                                0
#define HLFLOWC_XONXOFF                             1
#define HLFLOWC_HARDWARE                            2

#define HLPARITY_NONE                               0
#define HLPARITY_ODD                                1
#define HLPARITY_EVEN                               2

#define IFACE_CANCHANGEPROTO                        0x00000001
#define IFACE_CANCHANGEBAUD                         0x00000002
#define IFACE_HASDISCOVER                           0x00000004
#define IFACE_NOCHANGEPORT                          0x00000008
#define IFACE_USERDROPLIST                          0x00000010
#define IFACE_CANCHANGEANY                          0x00000020
#define IFACE_NOUSERADD                             0x00000040
#define IFACE_NOUSERDEL                             0x00000080
#define IFACE_NOCHANGEIP                            0x00000100
#define IFACE_HASRESET                              0x00000200

#define DEVICETYPE_AUTODETECT                       0x00000001
#define DEVICETYPE_CLASS_REPLACENONE                0x00010000
#define DEVICETYPE_CLASS_REPLACEANY                 0x00020000
#define DEVICETYPE_CLASS_REPLACE_X10                0x00030000
#define DEVICETYPE_CLASS_REPLACE_UPB                0x00040000
#define DEVICETYPE_CLASS_REPLACE_INSTEON            0x00050000
#define DEVICETYPE_NOUSERADD                        0x00100000

#define SYSCOMP_NEEDID                              0x00000001
#define SYSCOMP_SPLITID                             0x00000002
#define SYSCOMP_HASRESET                            0x00000004
#define SYSCOMP_ZEROBASEDID                         0x00000008
#define SYSCOMP_DISABLEID                           0x00000010
#define SYSCOMP_HUMIDISTAT                          0x00000020
#define SYSCOMP_CUSTOMPAGE                          0x00000040
#define SYSCOMP_BASEEDIT                            0x00000080
#define SYSCOMP_READONLYNAME                        0x00000100
#define SYSCOMP_READONLYSUBDEVID                    0x00000200
#define SYSCOMP_ALTDEVICE                           0x00000400
#define SYSCOMP_NOUSERADDSUBDEV                     0x00000800
#define SYSCOMP_EXTERNCONFIGFILE                    0x00001000
#define SYSCOMP_USERSELAUTO                         0x00002000
#define SYSCOMP_CONFIGDROPLIST                      0x00004000
#define SYSCOMP_USERDROPLIST                        0x00008000
#define SYSCOMP_HASUSEROVERRIDE                     0x00010000
#define SYSCOMP_DISALBEUSEROVERRIDE                 0x00020000
#define SYSCOMP_CANDETECTSUBCOMPONENT               0x00040000
#define SYSCOMP_NOUSERADDKEYPADS                    0x00080000
#define SYSCOMP_ACCEPTTRANSPORTCOMMANDS             0x00100000
#define SYSCOMP_HASCONFIGPROC                       0x00200000
#define SYSCOMP_GROUP                               0x00400000
#define SYSCOMP_SCHEDULE                            0x00800000
#define SYSCOMP_CONFIG1                             0x01000000
#define SYSCOMP_CONFIG2                             0x02000000
#define SYSCOMP_NOUSERSETID                         0x04000000
#define SYSCOMP_CONFIG3                             0x08000000
#define SYSCOMP_READONLYCOMDEV                      0x10000000
#define SYSCOMP_NOUSERDEL                           0x20000000
#define SYSCOMP_IFACEREQUIRED                       0x40000000

#define DEVTYPE_ALL                                 0
#define DEVTYPE_DIMMERS                             1
#define DEVTYPE_NONDIMMERS                          2

#define LIGHTDEV_CANDIM                             0x00000001
#define LIGHTDEV_CANRAMP                            0x00000002
#define LIGHTDEV_REMOVEABLE                         0x00000004
#define LIGHTDEV_HASLEDCONTROL                      0x00000008
#define LIGHTDEV_X10ADDRESS                         0x00000010
#define LIGHTDEV_ADDRESS1                           0x00000020
#define LIGHTDEV_ADDRESS2                           0x00000030
#define LIGHTDEV_ADDRESS3                           0x00000040
#define LIGHTDEV_ADDRESS4                           0x00000050
#define LIGHTDEV_ADDRESS5                           0x00000060
#define LIGHTDEV_ADDRESS1_EDIT                      0x00000100
#define LIGHTDEV_ADDRESS2_EDIT                      0x00000200
#define LIGHTDEV_HASSLAVEDEVICE                     0x00000800
#define LIGHTDEV_SLAVEDEVICE                        0x00001000
#define LIGHTDEV_ADDRESS_10BIT                      0x00002000
#define LIGHTDEV_HEXADDRESS                         0x00004000
#define LIGHTDEV_ZEROBASEDADDR                      0x00010000
#define LIGHTDEV_KEYPADHASLOADS                     0x00020000
#define LIGHTDEV_NOUSERADD                          0x00040000
#define LIGHTDEV_NOUSERMODADDRESS                   0x00080000
#define LIGHTDEV_CANTURNON                          0x00100000
#define LIGHTDEV_NOSCHEDULE                         0x00200000
#define LIGHTDEV_RESETCONFIG                        0x00400000
#define LIGHTDEV_ISLINK                             0x00800000
#define LIGHTDEV_READONLY                           0x01000000
#define LIGHTDEV_NOUSERDEL                          0x02000000
#define LIGHTDEV_2BASEDADDR                         0x04000000
#define LIGHTDEV_LINKNOSTATE                        0x08000000
#define LIGHTDEV_REALTIMEDIMMER                     0x10000000
#define LIGHTDEV_HIDEFROMSCHEDULER                  0x20000000
#define LIGHTDEV_NOLEDCONTROL                       0x40000000
#define LIGHTDEV_NOCHANGETEMPLATE                   0x80000000

#define LIGHTDEV_USERSETTABLEBITS                   (LIGHTDEV_NOLEDCONTROL|LIGHTDEV_HIDEFROMSCHEDULER)
/* tags for icon bitmaps */
#define LIGHTDEV_ICON_LOAD                          0x00000001
#define LIGHTDEV_ICON_LAMP                          0x00000002
#define LIGHTDEV_ICON_DIMMER                        0x00000003
#define LIGHTDEV_ICON_APPLIANCE                     0x00000004
#define LIGHTDEV_ICON_KEYPADDIMMER                  0x00000005
#define LIGHTDEV_ICON_KEYPAD                        0x00000006
#define LIGHTDEV_ICON_OUTPUT                        0x00000007
#define LIGHTDEV_ICON_INPUT                         0x00000008


#define VIDEO_NEEDLOGIN                             0x00000001
#define VIDEO_HASPANTILT                            0x00000002
#define VIDEO_HASPANTILTSCALE                       0x00000004
#define VIDEO_HASAUDIO                              0x00000008
#define VIDEO_CUSTOMPAGE                            0x00000010
#define VIDEO_MPEG                                  0x00000020
#define VIDEO_16_9                                  0x00000040
#define VIDEO_HASZOOM                               0x00000080
#define VIDEO_HASPRESETS8                           0x00000100
#define VIDEO_HASABSPANTILT                         0x00000200
#define VIDEO_HASABSZOOM                            0x00000400
#define VIDEO_CANHAVECTLR                           0x00000800
#define VIDEO_MOTIONINSTREAM                        0x00001000
#define VIDEO_USERSETOPTS                           0x00002000

#define HVAC_HASAUTO                                0x00000001
#define HVAC_HASEHEAT                               0x00000002
#define HVAC_SINGLESETPOINT                         0x00000004
#define HVAC_FIXEDAUTOMODE                          0x00000008
#define HVAC_DEGC                                   0x00000010
#define HVAC_3SPEEDFAN                              0x00000020
#define HVAC_HASSYNC                                0x00000040
#define HVAC_HASVENT                                0x00000080
#define HVAC_HASOFF                                 0x00000100
#define HVAC_HAS_HUMIDITY_CONTROL					0x00000200
#define HVAC_USER_SHOWUSAGE                         0x00010000
#define HVAC_USER_CFG_HUMIDITY_CTRL                 0x00020000

#define HVAC_RELAY_FAN                              0x00000001
#define HVAC_RELAY_HEAT                             0x00000002
#define HVAC_RELAY_COOL                             0x00000004
#define HVAC_RELAY_EHEAT                            0x00000008

#define VIDEO_OPTION_FLIP180                        0x00000001
#define VIDEO_OPTION_FULLFRAME                      0x00000002
#define VIDEO_OPTION_DEFAULTRES_HIGH                0x00000000
#define VIDEO_OPTION_DEFAULTRES_MED                 0x00000004
#define VIDEO_OPTION_DEFAULTRES_LOW                 0x00000008
#define VIDEO_OPTION_NOSHOW_RES                     0x00000010
#define VIDEO_OPTION_NOSHOW_FS                      0x00000020
#define VIDEO_OPTION_DEFAULTRES_AUTO                0x00000040
#define VIDEO_OPTION_DVR                            0x00000080
#define VIDEO_OPTION_MDETECT_FIXED                  0x00000100
#define VIDEO_OPTION_MDETECT_AUTO_HISENSE           0x00000200
#define VIDEO_OPTION_MDETECT_AUTO_LOSENSE           0x00000400

#define TABLET_HASBATTERY                           0x00000001
#define TABLET_HASWEB                               0x00000002

#define SECURITY_NEEDUSERNUM                        0x00000200
#define SECURITY_BYPASSINFOAUTO                     0x00000400

//
// System Families
//
#define FAMILY_SECURITY                             0
#define FAMILY_LIGHTDEVCTLR                         1
#define FAMILY_AUDIO                                3
#define FAMILY_HVAC                                 4
#define FAMILY_HVACU                                5
#define FAMILY_TELEPHONE                            6
#define FAMILY_IRRIGATION                           7
#define FAMILY_VIDEO                                8
#define FAMILY_UPS                                  9
#define FAMILY_AUDIO_ZONECTLR                       10
#define FAMILY_AUDIO_TUNER                          11
#define FAMILY_POOL_CTLR                            12
#define FAMILY_AUDIO_KEYPAD                         13
#define FAMILY_IR_RECEIVER                          14
#define FAMILY_IR_SENDER                            15
#define FAMILY_GEN_OUTPUT                           16
#define FAMILY_GEN_INPUT                            17
#define FAMILY_DVD                                  18
#define FAMILY_DISPLAY                              19
#define FAMILY_VIDEO_CTRL                           20
#define FAMILY_GENSERIAL                            21
#define FAMILY_DVR                                  22
#define FAMILY_INTERFACE                            23
#define FAMILY_ENERGY_MONITOR						24
#define	FAMILY_LAST_FAMILY							24  // IF YOU ADD FAMILY, ENSURE TO MAKE THIS THAT SAME

#define FAMILY_SYSTEM                               100
#define FAMILY_SERIAL                               101

//
// Tabs
//

#define TAB_HOME                                    0
#define TAB_SECURITY                                1
#define TAB_HVAC                                    2
#define TAB_HVAC_GLOBALSTAT                         3
#define TAB_HVAC_GLOBALSCHED                        4
#define TAB_LIGHTING                                5
#define TAB_AUDIO                                   6
#define TAB_TELEPHONE                               7
#define TAB_IRRIGATION                              8
#define TAB_VIDEO                                   9
#define TAB_POOL                                    10
#define TAB_PHOTO                                   11
#define TAB_WEB                                     12
#define TAB_EXIT                                    14

#define MAX_TABS                                    15

#define MODULE_NULL                                 0
#define MODULE_SECURITY                             1
#define MODULE_HVAC                                 2
#define MODULE_LIGHTING                             3
#define MODULE_AUDIO                                4
#define MODULE_PHONE                                5
#define MODULE_VIDEO                                6
#define MODULE_IRRIGATION                           7
#define MODULE_POOL                                 8
#define MODULE_PHOTO                                9
#define MODULE_THEATER                              30
#define MODULE_DVR                                  31

#define MODULE_SECURITY_LT                          101
#define MODULE_HVAC_LT                              102
#define MODULE_LIGHTING_LT                          103
#define MODULE_AUDIO_LT                             104
#define MODULE_PHONE_LT                             105
#define MODULE_VIDEO_LT                             106
#define MODULE_IRRIGATION_LT                        107
#define MODULE_POOL_LT                              108
#define MODULE_PHOTO_LT                             109
#define MODULE_HLWEB                                110
#define MODULE_DVR_LT                               111

#define MODULE_BRICK_START                      152
#define MODULE_HVAC_BRICK                           152
#define MODULE_LIGHTING_BRICK                       153
#define MODULE_IRRIGATION_BRICK                     157
#define MODULE_BRICK_END                        157

#define MODULE_INSTALLTRIAL_DAYS                    30
#define MODULE_INSTALLSTATE_NEVERINSTALLED          0
#define MODULE_INSTALLSTATE_PERMANENT               1
#define MODULE_INSTALLSTATE_TRIALACTIVE             2
#define MODULE_INSTALLSTATE_TRIALEXPIRED            3
#define MODULE_INSTALLSTATE_KEYASSIGNED             4
#define MODULE_INSTALLSTATE_UNINSTALLED             5

#define MODULE_WIN32_SEAT1                          10
#define MODULE_WIN32_SEAT2                          11
#define MODULE_WIN32_SEAT3                          12
#define MODULE_WIN32_SEAT4                          13
#define MODULE_WIN32_SEAT5                          14
#define MODULE_WIN32_SEATX                          15

#define MODULE_VIEWER_SEATS                         16

#define MODULE_PDA_SEAT1                            20
#define MODULE_PDA_SEAT2                            21
#define MODULE_PDA_SEAT3                            22
#define MODULE_PDA_SEAT4                            23
#define MODULE_PDA_SEAT5                            24
#define MODULE_PDA_SEATX                            25

#define MODULE_5_0_CORE                             55

#define MODULE_PENTAIRMON                           50

// Client connection types
#define CONNECTION_TYPE_UNKNOWN                     -1  // Not known yet
#define CONNECTION_TYPE_CP_800X600                  0   // Crystalpad 800x600 mode Win32
#define CONNECTION_TYPE_CP_240X320                  1   // Crystalpad 240x320 mode PDA
#define CONNECTION_TYPE_JAVAPAD                     2   // Javapad
#define CONNECTION_TYPE_CONFIG                      3   // Configurator
#define CONNECTION_TYPE_SERVICE                     4   // Service connection HLSCRIBE / HLTRACE
#define CONNECTION_TYPE_MINIPAD                     5   // Minipad PDA/Win32
#define CONNECTION_TYPE_CP_800X480                  6   // Crystalpad 800x480 mode Win32
#define CONNECTION_TYPE_CP_320X240                  7   // Crystalpad 320x240 mode Win32 (Q-Phone)
#define CONNECTION_TYPE_CP_240X240                  8   // Crystalpad 320x240 mode Win32 (Q-Phone)
#define CONNECTION_TYPE_CLIENTGATEWAY               9   // Client Gateway
#define CONNECTION_TYPE_CP_320X320                  10  // Crystalpad 320x320 mode Win32 (Q-Phone)
#define CONNECTION_TYPE_IOS                         11
#define CONNECTION_TYPE_IOS_3G                      12
#define CONNECTION_TYPE_OSD                         13
#define CONNECTION_TYPE_TS2                         14
#define CONNECTION_TYPE_HR2                         17

#define HLCMOS_480P60                               1
#define HLCMOS_576P50                               2
#define HLCMOS_720P60                               3
#define HLCMOS_720P50                               4
#define HLCMOS_1080I60                              5
#define HLCMOS_1080I50                              6
#define HLCMOS_1080P60                              7
#define HLCMOS_1080P50                              8


// Editor defines
#define OP_NONE                                     0
#define OP_TRANS_LEFT                               1
#define OP_TRANS_RIGHT                              2
#define OP_TRANS_UP                                 3
#define OP_TRANS_DOWN                               4
#define OP_ALIGN_LEFT                               5
#define OP_ALIGN_RIGHT                              6
#define OP_ALIGN_TOP                                7
#define OP_ALIGN_BOTTOM                             8
#define OP_MATCH_X                                  9
#define OP_MATCH_Y                                  10
#define OP_SPACEX                                   11
#define OP_SPACEY                                   12
#define OP_SAMESTYLE                                13
#define OP_SAMECOLORS                               14

// Rendering image types
#define RENDIMG_FILE_SIMPLE                         1
#define RENDIMG_TEXTURE                             4
#define RENDIMG_PICTURE                             5
#define RENDIMG_PICTURE_FROMARTWORK                 6
#define RENDIMG_PICTURE_FROMICONS                   7
#define RENDIMG_24BITDOWNCONVERT                    8

// Rendering image opts
#define RENDHINT_EXACTFILL                          0x00000001
#define RENDHINT_BIGSHADE                           0x00000002
#define RENDHINT_NEEDDISABLED                       0x00000004
#define RENDHINT_KEEPGIFCOLORS                      0x00000008
#define RENDHINT_DELAYEDPOST                        0x00000010
#define RENDHINT_DONTFIXBOUNDS                      0x00000020
#define RENDHINT_RAISED                             0x00000040
#define RENDHINT_BOLD                               0x00000080
#define RENDHINT_NOTRANSPARENCY                     0x00000100

//
// AudioPage sort types
#define SORT_ARTIST                                 0x00000001
#define SORT_ALBUM                                  0x00000002
#define SORT_TRACK                                  0x00000004
#define SORT_ARTISTALBUM                            0x00000008
#define SORT_PLAYLIST                               0x00000010
#define SORT_GENRE                                  0x00000020

// Key Types
#define CMD_KEY_START                           1844
#define CMD_KEY_SEARCH_MEDIA                        1844
#define CMD_KEY_CLEAR                               1845
#define CMD_KEY_DISC_PLUS                           1846
#define CMD_KEY_DISC_MINUS                          1847
#define CMD_KEY_FOLDER                              1848
#define CMD_KEY_LOADDIRECT                          1849
#define CMD_KEY_POWERON                             1850
#define CMD_KEY_POWEROFF                            1851
#define CMD_KEY_PLAY                                1852
#define CMD_KEY_STOP                                1853
#define CMD_KEY_SKIPF                               1854
#define CMD_KEY_SKIPR                               1855
#define CMD_KEY_FF                                  1856
#define CMD_KEY_REW                                 1857
#define CMD_KEY_PAUSE                               1858
#define CMD_KEY_LASTPLAY                            1859
#define CMD_KEY_SUBTITLE                            1860
#define CMD_KEY_SETUP                               1861
#define CMD_KEY_TOPMENU                             1862
#define CMD_KEY_MENU                                1863
#define CMD_KEY_UP                                  1864
#define CMD_KEY_DOWN                                1865
#define CMD_KEY_LEFT                                1866
#define CMD_KEY_RIGHT                               1867
#define CMD_KEY_ENTER                               1868
#define CMD_KEY_RETURN                              1869
#define CMD_KEY_AUDIO                               1870
#define CMD_KEY_RANDOM                              1871
#define CMD_KEY_OPEN_CLOSE                          1872
#define CMD_KEY_ANGLE                               1873
#define CMD_KEY_0                                   1874
#define CMD_KEY_1                                   1875
#define CMD_KEY_2                                   1876
#define CMD_KEY_3                                   1877
#define CMD_KEY_4                                   1878
#define CMD_KEY_5                                   1879
#define CMD_KEY_6                                   1880
#define CMD_KEY_7                                   1881
#define CMD_KEY_8                                   1882
#define CMD_KEY_9                                   1883
#define CMD_KEY_10                                  1884
#define CMD_KEY_SEARCH                              1885
#define CMD_KEY_DISPLAY                             1886
#define CMD_KEY_REPEAT                              1887
#define CMD_KEY_STEP                                1888
#define CMD_KEY_STEPBACK                            1889
#define CMD_KEY_SLOW                                1890
#define CMD_KEY_SLOWBACK                            1891
#define CMD_KEY_ZOOMUP                              1892
#define CMD_KEY_ZOOMDOWN                            1893
#define CMD_KEY_PROGRESSIVE                         1894
#define CMD_KEY_VIDEO_ONOFF                         1895
#define CMD_KEY_SEARCH_BACK                         1896
#define CMD_KEY_VRQ_MODE                            1897
#define CMD_KEY_DVD_MODE                            1898
#define CMD_KEY_SELECT                              1899
#define CMD_KEY_DISC1                               1900
#define CMD_KEY_DISC12                              1912
#define CMD_KEY_GUIDE                               1913
#define CMD_KEY_OPTIONS                             1914
#define CMD_KEY_MODE                                1915
#define CMD_KEY_MUSIC                               1916
#define CMD_KEY_IRADIO                              1917    // TODO: ?
#define CMD_KEY_MOVIES                              1918
#define CMD_KEY_ADDFAVORITES                        1919
#define CMD_KEY_PLAYFAVORITES                       1920
#define CMD_KEY_RECORD                              1921
#define CMD_KEY_BACK                                1922
#define CMD_KEY_STEREO                              1923
#define CMD_KEY_SURROUND                            1924
#define CMD_KEY_LIBRARY                             1925
#define CMD_KEY_NOW_PLAYING                         1926
#define CMD_KEY_PAGE_UP                             1927
#define CMD_KEY_PAGE_DOWN                           1928
#define CMD_KEY_SAVE                                1929
#define CMD_KEY_CANCEL                              1930
#define CMD_KEY_VOL_UP                              1931
#define CMD_KEY_VOL_DOWN                            1932
#define CMD_KEY_MUTE                                1933
#define CMD_DVP_SET									1934
#define CMD_DVP_DIMMER								1935
#define CMD_DVP_LOAD								1936
#define CMD_DVP_HOME								1937
#define CMD_DVP_RED									1938
#define CMD_DVP_GREEN								1939
#define CMD_DVP_BLUE								1940
#define CMD_DVP_YELLOW								1941
#define CMD_DVP_SORT								1942
#define CMD_DVP_GROUP								1943
#define CMD_DVP_START_SCAN							1944
#define CMD_DVP_STOP_SCAN							1945
#define CMD_DVP_START_SCAN_UPDATE					1946
#define CMD_KEY_CDSACD								1947
#define CMD_KEY_INFO								1948
#define CMD_KEY_INSTANT_REPLAY						1949
#define CMD_KEY_ADVANCE								1950
//#define CMD_DVP_DISC_LOCK_RLS						1944
//#define CMD_DVP_DISC_INFO_RQST					1945
//#define CMD_DVP_DISC_ID_RQST						1946
//#define CMD_DVP_PAREN_CTRL_RLS					1947
#define CMD_KEY_END                             1950


//
// System Command Types
//
#define CMD_SET_GENERAL                             0x00000001
#define CMD_SET_AUDIO                               0x00000002

#define CMD_OPTIONS_DELAYTIME                       0x00000001
#define CMD_OPTIONS_IOMAP                           0x00000002

#define CMD_START                               1000
#define CMD_LIGHTING_ACTIVATESCENE                  1000
#define CMD_LIGHTING_ACTIVATESWITCH                 1001
#define CMD_LIGHTING_DEACTIVATESWITCH               1002
#define CMD_LIGHTING_TOGGLESWITCH                   1003
#define CMD_LIGHTING_DIMDEVICE                      1004
#define CMD_LIGHTING_BRIGHTDEVICE                   1005
#define CMD_LIGHTING_BEGINRAISE                     1006
#define CMD_LIGHTING_BEGINLOWER                     1007
#define CMD_LIGHTING_ENDRAISELOWER                  1008
#define CMD_LIGHTING_DIMDEVICETO                    1009

/*
#define CMD_LIGHTING_SAVEALL1                       1010
#define CMD_LIGHTING_SAVEALL2                       1011
#define CMD_LIGHTING_SAVEALL3                       1012
#define CMD_LIGHTING_SAVEALL4                       1013
#define CMD_LIGHTING_SAVEALL5                       1014

#define CMD_LIGHTING_RESTOREALL1                    1020
#define CMD_LIGHTING_RESTOREALL2                    1021
#define CMD_LIGHTING_RESTOREALL3                    1022
#define CMD_LIGHTING_RESTOREALL4                    1023
#define CMD_LIGHTING_RESTOREALL5                    1024
*/
#define CMD_SYSTIMER_STARTUSER                      1104
#define CMD_SYSTIMER_STARTX1                        1105
#define CMD_SYSTIMER_STARTX2                        1106
#define CMD_SYSTIMER_STARTX5                        1107
#define CMD_SYSTIMER_STARTX10                       1108
#define CMD_SYSTIMER_CANCEL                         1109
#define CMD_SYSTEM_TBUTTONON                        1110
#define CMD_SYSTEM_TBUTTONOFF                       1111
#define CMD_SYSTEM_DELAY                            1112

#define CMD_SYSTEMMODE0                             1120
#define CMD_SYSTEMMODE1                             1121
#define CMD_SYSTEMMODE2                             1122
#define CMD_SYSTEMMODE3                             1123
#define CMD_SYSTEMMODE4                             1124
#define CMD_SYSTEMMODE5                             1125
#define CMD_SYSTEMMODE6                             1126
#define CMD_SYSTEMMODE7                             1127
#define CMD_SYSTEMMODE8                             1128
#define CMD_SYSTEMMODE9                             1129

#define CMD_SYSTEM_TABLETDISPLAYON                  1130
#define CMD_SYSTEM_TABLETDISPLAYOFF                 1131
#define CMD_SYSTEM_TABLETKEYDOWN                    1132
#define CMD_SYSTEM_TABLETKEYUP                      1134
#define CMD_SYSTEM_TABLETKEY                        1135
#define CMD_SYSTEM_TABELETLED1ONOFF                 1136
#define CMD_SYSTEM_TABELETLED2ONOFF                 1137
#define CMD_SYSTEM_TABLETJUMP_HOME                  1138
#define CMD_SYSTEM_TABLETJUMP_SECURITY              1139
#define CMD_SYSTEM_TABLETJUMP_HVAC                  1140
#define CMD_SYSTEM_TABLETJUMP_LIGHTING              1141
#define CMD_SYSTEM_TABLETJUMP_VIDEO                 1142
#define CMD_SYSTEM_TABLETJUMP_MEDIA                 1143
#define CMD_SYSTEM_TABLETJUMP_WEB                   1144
#define CMD_SYSTEM_TABLETJUMP_WEB_NONAV             1145
#define CMD_SYSTEM_TABLETJUMP_MESSAGING             1146
#define CMD_SYSTEM_TABLET_QUERYDISPLAY              1147
#define CMD_SYSTEM_TABLET_SCREENSAVERSTART          1148
#define CMD_SYSTEM_TABLET_POPMESSAGE                1149
#define CMD_SYSTEM_TABLETVIDCAPON                   1160
#define CMD_SYSTEM_TABLETVIDCAPOFF                  1161
#define CMD_SYSTEM_TS7_LIGHTLEDON                   1162
#define CMD_SYSTEM_OSD_ON                           1163
#define CMD_SYSTEM_OSD_OFF                          1164

#define CMD_EMAIL_SEND                              1150
#define CMD_PHONE_CALL                              1151
#define CMD_TTS_ANNOUNCE                            1152
#define CMD_TTS_ANNOUNCE_GATEWAY                    1153
#define CMD_WAV_ANNOUNCE                            1154
#define CMD_WAV_ANNOUNCE_GATEWAY                    1155
#define CMD_TTS_ANNOUNCE_GATEWAY_2                  1156
#define CMD_WAV_ANNOUNCE_GATEWAY_2                  1157
#define CMD_PHONE_CALL_LINE1                        1158
#define CMD_PHONE_CALL_LINE2                        1159

#define CMD_SECURITY_AUTOARM_MODE1                  1201
#define CMD_SECURITY_AUTOARM_MODE2                  1202
#define CMD_SECURITY_AUTOARM_MODE3                  1203
#define CMD_SECURITY_AUTOARM_MODE4                  1204
#define CMD_SECURITY_AUTOARM_MODE5                  1205
#define CMD_SECURITY_AUTOARM_MODE6                  1206
#define CMD_SECURITY_AUTOARM_MODE7                  1207
#define CMD_SECURITY_MACRO                          1208
#define CMD_SECURITY_SYSCMD                         1209

#define CMD_UPS_OUTLET_ON                           1280
#define CMD_UPS_OUTLET_OFF                          1281
#define CMD_UPS_OUTLET_POWERCYCLE                   1282
#define CMD_UPS_MAIN_ON								1283
#define CMD_UPS_MAIN_OFF							1284	// In battery mode
#define CMD_UPS_NON_CRITICAL_LOAD_ON				1285	// Battery falls below critical load threshold
#define CMD_UPS_NON_CRITICAL_LOAD_OFF				1286
#define CMD_UPS_ALL_ON								1287
#define CMD_UPS_ALL_OFF								1288
#define CMD_UPS_BANK_STATE							1289

#define CMD_AUDIO_PAUSE                             1290
#define CMD_AUDIO_PLAY                              1300
#define CMD_AUDIO_STOP                              1301
#define CMD_AUDIO_NEXT                              1302
#define CMD_AUDIO_PREV                              1303
#define CMD_AUDIO_MUTE                              1304
#define CMD_AUDIO_VOLUP5                            1305
#define CMD_AUDIO_VOLUP10                           1306
#define CMD_AUDIO_VOLUP15                           1307
#define CMD_AUDIO_VOLDN5                            1308
#define CMD_AUDIO_VOLDN10                           1309
#define CMD_AUDIO_VOLDN15                           1310
#define CMD_AUDIO_SELPLAYLIST_START_OBSOLETE        1311
#define CMD_AUDIO_SELPLAYLIST_END_OBSOLETE          1319
#define CMD_AUDIO_TBUTTONON                         1339
#define CMD_AUDIO_TBUTTONOFF                        1340
#define CMD_AUDIO_SCANALLMEDIA                      1341
#define CMD_AUDIO_POWERON                           1342
#define CMD_AUDIO_POWEROFF                          1343
#define CMD_AUDIO_UP                                1344
#define CMD_AUDIO_DOWN                              1345
#define CMD_AUDIO_LEFT                              1346
#define CMD_AUDIO_RIGHT                             1347
#define CMD_AUDIO_ENTER                             1348
#define CMD_AUDIO_HOME                              1349
#define CMD_AUDIO_PLAYPAUSE                         1370
#define CMD_AUDIO_FASTFWD                           1371
#define CMD_AUDIO_REWIND                            1372
#define CMD_AUDIO_RANDOM_ON                         1373
#define CMD_AUDIO_RANDOM_OFF                        1374
#define CMD_AUDIO_RANDOM_TOG                        1375
#define CMD_AUDIO_REPEAT_TOG                        1376
#define CMD_AUDIO_GUI_ON                            1377
#define CMD_AUDIO_GUI_OFF                           1378
#define CMD_AUDIO_GUI_TOG                           1379
#define CMD_AUDIO_SCRNSAV_ON                        1380
#define CMD_AUDIO_SCRNSAV_OFF                       1381
#define CMD_AUDIO_SCRNSAV_TOG                       1382
#define CMD_AUDIO_PAGEUP                            1383
#define CMD_AUDIO_PAGEDOWN                          1384
#define CMD_AUDIO_SHUFFLE                           1385
#define CMD_AUDIO_REPEAT                            1386
#define CMD_AUDIO_SCANMEDIA                         1387
#define CMD_AUDIO_STEREO                            1388
#define CMD_AUDIO_SURROUND                          1389

#define CMD_AUDIO_MENU                              1420
#define CMD_AUDIO_CLEAR                             1421
#define CMD_AUDIO_LIBRARY                           1422
#define CMD_AUDIO_NOW_PLAYING                       1423
#define CMD_AUDIO_OPTIONS                           1424
#define CMD_AUDIO_SAVE                              1425
#define CMD_AUDIO_DIGIT_0                           1430
#define CMD_AUDIO_DIGIT_1                           1431
#define CMD_AUDIO_DIGIT_2                           1432
#define CMD_AUDIO_DIGIT_3                           1433
#define CMD_AUDIO_DIGIT_4                           1434
#define CMD_AUDIO_DIGIT_5                           1435
#define CMD_AUDIO_DIGIT_6                           1436
#define CMD_AUDIO_DIGIT_7                           1437
#define CMD_AUDIO_DIGIT_8                           1438
#define CMD_AUDIO_DIGIT_9                           1439
#define CMD_AUDIO_VOL_UP                            1440
#define CMD_AUDIO_VOL_DOWN                          1441
#define CMD_AUDIO_MUTE_TOGGLE                       1442

#define CMD_AUDIOZONE_PLAY                          1320
#define CMD_AUDIOZONE_STOP                          1321
#define CMD_AUDIOZONE_NEXT                          1322
#define CMD_AUDIOZONE_PREV                          1323
#define CMD_AUDIOZONE_MUTE                          1324
#define CMD_AUDIOZONE_VOLUP5                        1325
#define CMD_AUDIOZONE_VOLUP10                       1326
#define CMD_AUDIOZONE_VOLUP                         1327
#define CMD_AUDIOZONE_VOLDN5                        1328
#define CMD_AUDIOZONE_VOLDN10                       1329
#define CMD_AUDIOZONE_VOLDN                         1330
#define CMD_AUDIOZONE_ON                            1331
#define CMD_AUDIOZONE_OFF                           1332
#define CMD_AUDIOZONE_SETSOURCE                     1333
#define CMD_AUDIOZONE_SETLISTENMODEOPT              1334
#define CMD_AUDIOZONE_DOZONECOMMAND                 1335
#define CMD_AUDIOZONE_UNMUTE                        1336
#define CMD_AUDIOZONE_PAUSE                         1337
#define CMD_AUDIOZONE_VOLTO                         1338
#define CMD_AUDIOZONE_TREBLETO						1413
#define CMD_AUDIOZONE_BASSTO						1414

#define CMD_AUDIOZONE_ZONE_DIGIT0                   1450
#define CMD_AUDIOZONE_ZONE_DIGIT1                   1451
#define CMD_AUDIOZONE_ZONE_DIGIT2                   1452
#define CMD_AUDIOZONE_ZONE_DIGIT3                   1453
#define CMD_AUDIOZONE_ZONE_DIGIT4                   1454
#define CMD_AUDIOZONE_ZONE_DIGIT5                   1455
#define CMD_AUDIOZONE_ZONE_DIGIT6                   1456
#define CMD_AUDIOZONE_ZONE_DIGIT7                   1457
#define CMD_AUDIOZONE_ZONE_DIGIT8                   1458
#define CMD_AUDIOZONE_ZONE_DIGIT9                   1459
#define CMD_AUDIOZONE_ZONE_CHUP                     1460
#define CMD_AUDIOZONE_ZONE_CHDN                     1461
#define CMD_AUDIOZONE_ZONE_ENTER                    1462
#define CMD_AUDIOZONE_ZONE_PREV_CHANNEL             1463
#define CMD_AUDIOZONE_ZONE_LEFT                     1464
#define CMD_AUDIOZONE_ZONE_RIGHT                    1465
#define CMD_AUDIOZONE_ZONE_UP                       1466
#define CMD_AUDIOZONE_ZONE_DOWN                     1467
#define CMD_AUDIOZONE_ZONE_EXIT                     1468
#define CMD_AUDIOZONE_ZONE_MUTE                     1469
#define CMD_AUDIOZONE_ZONE_POWER                    1470
#define CMD_AUDIOZONE_ZONE_TVVIDEO                  1471
#define CMD_AUDIOZONE_ZONE_TVVCR                    1472
#define CMD_AUDIOZONE_ZONE_AB                       1473
#define CMD_AUDIOZONE_ZONE_TVDVD                    1474
#define CMD_AUDIOZONE_ZONE_TVLD                     1475
#define CMD_AUDIOZONE_ZONE_INPUT                    1476
#define CMD_AUDIOZONE_ZONE_TVDSS                    1477
#define CMD_AUDIOZONE_ZONE_SRCPLAY                  1478
#define CMD_AUDIOZONE_ZONE_SRCSTOP                  1479
#define CMD_AUDIOZONE_ZONE_SEARCHFW                 1480
#define CMD_AUDIOZONE_ZONE_SEARCHRW                 1481
#define CMD_AUDIOZONE_ZONE_SRCPAUSE                 1482
#define CMD_AUDIOZONE_ZONE_RECORD                   1483
#define CMD_AUDIOZONE_ZONE_MENU                     1484
#define CMD_AUDIOZONE_ZONE_SELECT                   1485
#define CMD_AUDIOZONE_ZONE_DISPLAY                  1486
#define CMD_AUDIOZONE_ZONE_GUIDE                    1487
#define CMD_AUDIOZONE_ZONE_PAGEUP                   1488
#define CMD_AUDIOZONE_ZONE_PAGEDOWN                 1489
#define CMD_AUDIOZONE_ZONE_DISK                     1490
#define CMD_AUDIOZONE_ZONE_PLUS10                   1491
#define CMD_AUDIOZONE_ZONE_OPENCLOSE                1492
#define CMD_AUDIOZONE_ZONE_RANDOM                   1493
#define CMD_AUDIOZONE_ZONE_TRACKFORWARD             1494
#define CMD_AUDIOZONE_ZONE_TRACKREVERSE             1495
#define CMD_AUDIOZONE_ZONE_SURROUNDONOFF            1496
#define CMD_AUDIOZONE_ZONE_SURROUNDMODE             1497
#define CMD_AUDIOZONE_ZONE_SURROUNDUP               1498
#define CMD_AUDIOZONE_ZONE_SURROUNDDOWN             1499
//note  CMD_IRRIGATION_AUTOMODE =                   1500
//      CMD_IRRIGATION_OFF =                        1501

#define CMD_AUDIOZONE_ZONE_CONTINUE                 1537
#define CMD_AUDIOZONE_ZONE_SHUFFLE                  1538
#define CMD_AUDIOZONE_ZONE_GROUP                    1539
#define CMD_AUDIOZONE_ZONE_SBUTTON0                 1540
#define CMD_AUDIOZONE_ZONE_SBUTTON1                 1541
#define CMD_AUDIOZONE_ZONE_SBUTTON2                 1542
#define CMD_AUDIOZONE_ZONE_SBUTTON3                 1543
#define CMD_AUDIOZONE_ZONE_SBUTTON4                 1544
#define CMD_AUDIOZONE_ZONE_SBUTTON5                 1545
#define CMD_AUDIOZONE_ZONE_SBUTTON6                 1546
#define CMD_AUDIOZONE_ZONE_SBUTTON7                 1547
#define CMD_AUDIOZONE_ZONE_SBUTTON8                 1548
#define CMD_AUDIOZONE_ZONE_SBUTTON9                 1549
#define CMD_AUDIOZONE_ZONE_PIP                      1550
#define CMD_AUDIOZONE_ZONE_PIPMOVE                  1551
#define CMD_AUDIOZONE_ZONE_PIPSWAP                  1552
#define CMD_AUDIOZONE_ZONE_PROGRAM                  1553
#define CMD_AUDIOZONE_ZONE_SLEEP                    1554
#define CMD_AUDIOZONE_ZONE_ON                       1555
#define CMD_AUDIOZONE_ZONE_OFF                      1556
#define CMD_AUDIOZONE_ZONE_NUM11                    1557
#define CMD_AUDIOZONE_ZONE_NUM12                    1558
#define CMD_AUDIOZONE_ZONE_NUM13                    1559
#define CMD_AUDIOZONE_ZONE_NUM14                    1560
#define CMD_AUDIOZONE_ZONE_NUM15                    1561
#define CMD_AUDIOZONE_ZONE_NUM16                    1562
#define CMD_AUDIOZONE_ZONE_BRIGHT                   1563
#define CMD_AUDIOZONE_ZONE_DIM                      1564
#define CMD_AUDIOZONE_ZONE_CLOSE                    1565
#define CMD_AUDIOZONE_ZONE_OPEN                     1566
#define CMD_AUDIOZONE_ZONE_STOP2                    1567
#define CMD_AUDIOZONE_ZONE_AMFM                     1568
#define CMD_AUDIOZONE_ZONE_CUE                      1569
#define CMD_AUDIOZONE_ZONE_DISKUP                   1570
#define CMD_AUDIOZONE_ZONE_DISKDOWN                 1571
#define CMD_AUDIOZONE_ZONE_INFO                     1572
#define CMD_AUDIOZONE_ZONE_ROUTE_IO                 1574

#define CMD_AZCTLR_DOGLOBALCOMMAND                  1573

#define CMD_AUDIOZONE_ZONE_TUNER_AM                 1575
#define CMD_AUDIOZONE_ZONE_TUNER_FM                 1576
#define CMD_AUDIOZONE_ZONE_TUNEUP                   1577
#define CMD_AUDIOZONE_ZONE_TUNEDOWN                 1578
#define CMD_AUDIOZONE_ZONE_NEXT_PRESET              1579
#define CMD_AUDIOZONE_ZONE_PREV_PRESET              1580
#define CMD_AUDIOZONE_ZONE_TUNER_XM                 1581

#define CMD_AUDIOZONE_ZONE_PREVIOUS                 1350
#define CMD_AUDIOZONE_ZONE_REWIND                   1351
#define CMD_AUDIOZONE_ZONE_STOP                     1352
#define CMD_AUDIOZONE_ZONE_PAUSE                    1353
#define CMD_AUDIOZONE_ZONE_PLAY                     1354
#define CMD_AUDIOZONE_ZONE_NEXT                     1355
#define CMD_AUDIOZONE_ZONE_FORWARD                  1356

#define CMD_TUNER_ON                                1360
#define CMD_TUNER_OFF                               1361
#define CMD_TUNER_AMMODE                            1362
#define CMD_TUNER_FMMODE                            1363
#define CMD_TUNER_SEEKUP                            1364
#define CMD_TUNER_SEEKDOWN                          1365
#define CMD_TUNER_EXCOMMAND                         1366

#define CMD_VIDEO_TAKEPICTURE                       1390
#define CMD_VIDEO_SETRES                            1391
#define CMD_VIDEO_GOTOPRESET                        1392
#define CMD_VIDEO_SETLOCKOUT                        1393
#define CMD_VIDEO_RECORD_MAXFPS                     1394
#define CMD_VIDEO_RECORD_1FPS                       1395
#define CMD_VIDEO_RECORD_2FPS                       1396
#define CMD_VIDEO_RECORD_5FPS                       1397
#define CMD_VIDEO_RECORD_10FPS                      1398

#define CMD_HVAC_HVACMODEOFF                        1400
#define CMD_HVAC_HVACMODEHEAT                       1401
#define CMD_HVAC_HVACMODECOOL                       1402
#define CMD_HVAC_HVACMODEAUTO                       1403
#define CMD_HVAC_HVACMODEEHEAT                      1404
#define CMD_HVAC_PRGMODERUN                         1405
#define CMD_HVAC_PRGMODETHOLD                       1406
#define CMD_HVAC_PRGMODEPHOLD                       1407
#define CMD_HVAC_FANMODEAUTO                        1408
#define CMD_HVAC_FANMODECONST                       1409
#define CMD_HVAC_SETPOINTHEAT                       1410
#define CMD_HVAC_SETPOINTCOOL                       1411
#define CMD_HVAC_HVACMODEVENT                       1412

#define CMD_IRRIGATION_AUTOMODE                     1500
#define CMD_IRRIGATION_OFF                          1501

#define CMD_SECURITY_JUMPTAB                        1600
#define CMD_LIGHTING_JUMPTAB                        1601
#define CMD_HVAC_JUMPTAB                            1602
#define CMD_VIDEO_JUMPTAB                           1603
#define CMD_AUDIO_JUMPTAB                           1604
#define CMD_AUDIOZONE_JUMPTAB                       1605
#define CMD_MESSAGING_JUMPTAB                       1606
#define CMD_HOME_JUMPTAB                            1610

#define CMD_POOL_RESTART_COM                        1699
#define CMD_POOLCIRCUIT_ACTIVATE                    1700
#define CMD_POOLCIRCUIT_DEACTIVATE                  1701

#define CMD_IR_SEND                                 1702
#define CMD_IR_SEND_VIA                             1703
#define CMD_IR_SEND_CONST                           1704
#define CMD_IR_STOP_CONST                           1705
#define CMD_IR_SLEEP                                1706

#define CMD_OUTPUT_ON                               1800
#define CMD_OUTPUT_OFF                              1801

#define CMD_BACKUP_DOBACKUP                         1810

#define CMD_TVTUNER_TUNEANTENNA                     1948
#define CMD_TVTUNER_TUNECATV                        1949

#define CMD_DISPLAY_ON                              1950
#define CMD_DISPLAY_OFF                             1951
#define CMD_DISPLAY_SOURCE                          1952
#define CMD_DISPLAY_MODE                            1953
#define CMD_DISPLAY_SAVESTATE                       1954
#define CMD_DISPLAY_RESTORESTATE                    1955
#define CMD_DISPLAY_PICTURE_MUTE_ON                 1956
#define CMD_DISPLAY_PICTURE_MUTE_OFF                1957

#define CMD_KEY_VTS_START                           1960    // VIRTUAL TOUCHSCREEN
#define CMD_KEY_VTS_END                             1990    // VIRTUAL TOUCHSCREEN

#define CMD_SERIAL_SENDDEFAULT                      2000
#define CMD_SERIAL_SENDVIA                          2001
#define CMD_SERIAL_SLEEP                            2002

#define CMD_CLIENTGATEWAY_TX                        2100
#define CMD_HOSTGATEWAY_TX                          2101

#define CMD_END                               2101

//
// System Event Types
//
#define EVENT_START                           2000
#define EVENT_LIGHTING_SCENEACTIVATE                2000
#define EVENT_LIGHTING_SCENEDEACTIVATE              2001
#define EVENT_LIGHTING_SWITCHACTIVATE               2002
#define EVENT_LIGHTING_SWITCHDEACTIVATE             2003
#define EVENT_LIGHTING_SWITCHACTIVATE_REPEAT        2004
#define EVENT_LIGHTING_SWITCHDEACTIVATE_REPEAT      2005
#define EVENT_LIGHTING_IRCODE_0                     2006
#define EVENT_LIGHTING_IRCODE_1                     2007
#define EVENT_LIGHTING_IRCODE_2                     2008
#define EVENT_LIGHTING_IRCODE_3                     2009
#define EVENT_LIGHTING_IRCODE_4                     2010
#define EVENT_LIGHTING_IRCODE_5                     2011
#define EVENT_LIGHTING_IRCODE_6                     2012
#define EVENT_LIGHTING_IRCODE_7                     2013
#define EVENT_LIGHTING_IRCODE_8                     2014
#define EVENT_LIGHTING_IRCODE_9                     2015
#define EVENT_LIGHTING_IRCODE_VOLUP                 2016
#define EVENT_LIGHTING_IRCODE_VOLDN                 2017
#define EVENT_LIGHTING_IRCODE_CHUP                  2018
#define EVENT_LIGHTING_IRCODE_CHDN                  2019
#define EVENT_LIGHTING_BUTTONPRESS                  2020
#define EVENT_LIGHTING_BUTTONRELEASE                2021
#define EVENT_LIGHTING_KPBUTTONPRESS                2022
#define EVENT_LIGHTING_KPBUTTONRELEASE              2023
#define EVENT_LIGHTING_BUTTONTAP                    2024
#define EVENT_LIGHTING_BUTTONDOUBLETAP              2025

#define EVENT_SYSTEM_HOMEPAGEBUTTON                 2103
#define EVENT_SYSTEM_REJECTEDLOGIN                  2104
#define EVENT_SYSTEM_REMOTELOGIN                    2105
#define EVENT_SYSTEM_DOWNLOADBEGIN                  2106
#define EVENT_SYSTEM_DOWNLOADFAILED                 2107
#define EVENT_SYSTEM_DOWNLOADDONE                   2108
#define EVENT_SYSTEM_UPGRADEDONE                    2109
#define EVENT_SYSTIMER_EXPIRED                      2110
#define EVENT_TIMEDEVENT                            2111
#define EVENT_SYSTEM_TABLETUSE                      2112
#define EVENT_SYSTEM_TBUTTONON                      2114
#define EVENT_SYSTEM_TBUTTONOFF                     2115
#define EVENT_SYSTEM_HOMPAGEMOMENTARYDOWN           2116
#define EVENT_SYSTEM_HOMPAGEMOMENTARYUP             2117
#define EVENT_SYSTEM_INTERNETDISCONNECT             2220
#define EVENT_SYSTEM_INTERNETRECONNECT              2221

#define EVENT_SYSTEMMODE0                           2150
#define EVENT_SYSTEMMODE1                           2151
#define EVENT_SYSTEMMODE2                           2152
#define EVENT_SYSTEMMODE3                           2153
#define EVENT_SYSTEMMODE4                           2154
#define EVENT_SYSTEMMODE5                           2155
#define EVENT_SYSTEMMODE6                           2156
#define EVENT_SYSTEMMODE7                           2157
#define EVENT_SYSTEMMODE8                           2158
#define EVENT_SYSTEMMODE9                           2159

#define EVENT_SYSTEM_TABLETEXITSLEEP                2120
#define EVENT_SYSTEM_TABLETBUTTON1DOWN              2121
#define EVENT_SYSTEM_TABLETBUTTON1UP                2122
#define EVENT_SYSTEM_TABLETBUTTON2DOWN              2123
#define EVENT_SYSTEM_TABLETBUTTON2UP                2124
#define EVENT_SYSTEM_FTPTRIGGER                     2125
#define EVENT_SYSTEM_TS7_LIGHTBTNDOWN               2126
#define EVENT_SYSTEM_TS7_LIGHTBTNUP                 2127
#define EVENT_SYSTEM_TS7_POWERBTNDOWN               2128
#define EVENT_SYSTEM_TS7_POWERBTNUP                 2129

#define EVENT_SECURITY_PARTITION_ARMEDANY           2200
#define EVENT_SECURITY_PARTITION_DISARM             2202
#define EVENT_SECURITY_PARTITION_FIRE               2203
#define EVENT_SECURITY_PARTITION_ARMEDMODE1         2210
#define EVENT_SECURITY_PARTITION_ARMEDMODE2         2211
#define EVENT_SECURITY_PARTITION_ARMEDMODE3         2212
#define EVENT_SECURITY_PARTITION_ARMEDMODE4         2213
#define EVENT_SECURITY_PARTITION_ARMEDMODE5         2214
#define EVENT_SECURITY_PARTITION_ARMEDMODE6         2215
#define EVENT_SECURITY_PARTITION_ARMEDMODE7         2216

#define EVENT_SECURITY_ZONE_FAULT                   2204
#define EVENT_SECURITY_ZONE_FAULTCLEAR              2205

#define EVENT_SECURITY_PARTITION_ALARM              2206
#define EVENT_SECURITY_PARTITION_ENTRYDELAY         2207
#define EVENT_SECURITY_PARTITION_EXITDELAY          2208

#define EVENT_SECURITY_MACRO                        2209

#define EVENT_AUDIOZONE_ON                          2240
#define EVENT_AUDIOZONE_OFF                         2241
#define EVENT_AUDIOZONE_SELECT_SOURCE               2242
#define EVENT_AUDIOZONE_SELECT_SPECSOURCE           2243
#define EVENT_AUDIOZONE_UNSELECT_SPECSOURCE         2244

#define EVENT_AUDIO_ENTERIDLE10_OBSOLETE            2250
#define EVENT_AUDIO_ENTERIDLE30_OBSOLETE            2251
#define EVENT_AUDIO_ENTERIDLE60_OBSOLETE            2252
#define EVENT_AUDIO_EXITIDLE_OBSOLETE               2253
#define EVENT_AUDIO_IRCODE_0                        2254
#define EVENT_AUDIO_IRCODE_1                        2255
#define EVENT_AUDIO_IRCODE_2                        2256
#define EVENT_AUDIO_IRCODE_3                        2257
#define EVENT_AUDIO_IRCODE_4                        2258
#define EVENT_AUDIO_IRCODE_5                        2259
#define EVENT_AUDIO_IRCODE_6                        2260
#define EVENT_AUDIO_IRCODE_7                        2261
#define EVENT_AUDIO_IRCODE_8                        2262
#define EVENT_AUDIO_IRCODE_9                        2263
#define EVENT_AUDIO_IRCODE_VOLUP                    2264
#define EVENT_AUDIO_IRCODE_VOLDN                    2265
#define EVENT_AUDIO_IRCODE_CHUP                     2266
#define EVENT_AUDIO_IRCODE_CHDN                     2267
#define EVENT_AUDIO_BUTTON                          2268
#define EVENT_AUDIO_TBUTTONON                       2269
#define EVENT_AUDIO_TBUTTONOFF                      2270

#define EVENT_AUDIO_ACTIVATE                        2271
#define EVENT_AUDIO_DEACTIVATE                      2272

#define EVENT_AUDIO_PLAYERON                        2273
#define EVENT_AUDIO_PLAYEROFF                       2274

#define EVENT_MEDIA_DB_CHANGED                      2276

#define EVENT_VIDEO_MOTIONDETECT                    2275
#define EVENT_VIDEO_NOMOTION1MIN                    2278
#define EVENT_VIDEO_NOMOTION5MIN                    2279
#define EVENT_VIDEO_NOMOTION10MIN                   2280
#define EVENT_VIDEO_STARTUP                         2281
#define EVENT_VIDEO_SHUTDOWN                        2282
#define EVENT_VIDEO_SIGNALLOST_OBSOLETE             2283
#define EVENT_VIDEO_SIGNALRESTORE_OBSOLETE          2284

#define EVENT_DVD_ASPECT_133                        2290
#define EVENT_DVD_ASPECT_178                        2291
#define EVENT_DVD_ASPECT_185                        2292
#define EVENT_DVD_ASPECT_235                        2293
#define EVENT_DVD_ASPECT_240                        2294
#define EVENT_DVD_LOADDIRECT                        2295

#define EVENT_HVAC_HEATRELAYON                      2300
#define EVENT_HVAC_HEATRELAYOFF                     2301
#define EVENT_HVAC_COOLRELAYON                      2302
#define EVENT_HVAC_COOLRELAYOFF                     2303
#define EVENT_HVAC_FANRELAYON                       2304
#define EVENT_HVAC_FANRELAYOFF                      2305
#define EVENT_HVAC_HVACMODECHANGEOFF                2306
#define EVENT_HVAC_HVACMODECHANGEHEAT               2307
#define EVENT_HVAC_HVACMODECHANGECOOL               2308
#define EVENT_HVAC_HVACMODECHANGEAUTO               2309
#define EVENT_HVAC_FANMODECHANGEAUTO                2310
#define EVENT_HVAC_FANMODECHANGECONST               2311
#define EVENT_HVAC_PRGMODECHANGERUN                 2312
#define EVENT_HVAC_PRGMODECHANGETHOLD               2313
#define EVENT_HVAC_PRGMODECHANGEPHOLD               2314
#define EVENT_HVAC_HVACMODENOTOK                    2315
#define OBSOLETE_EVENT_HVAC_ALARMLO                 2321
#define OBSOLETE_EVENT_HVAC_ALARMLO_CLEAR           2322
#define OBSOLETE_EVENT_HVAC_ALARMHI                 2323
#define OBSOLETE_EVENT_HVAC_ALARMHI_CLEAR           2324
#define EVENT_HVAC_HVACMODECHANGEVENT               2325
#define EVENT_HVAC_HVACMODECHANGEEHEAT              2326
#define EVENT_HVAC_EHEATRELAYON                     2327
#define EVENT_HVAC_EHEATRELAYOFF                    2328

#define EVENT_TELEPHONE_RING1                       2350
#define EVENT_TELEPHONE_RING2                       2351
#define EVENT_TELEPHONE_RING3                       2352
#define EVENT_TELEPHONE_RING4                       2353
#define EVENT_TELEPHONE_RING5                       2354
#define EVENT_TELEPHONE_RING6                       2355
#define EVENT_TELEPHONE_NEWMESSAGE                  2356
#define EVENT_TELEPHONE_ANSWERCALL                  2357
#define EVENT_TELEPHONE_REJECTEDLOGIN               2358
#define EVENT_TELEPHONE_CALLERID                    2359
#define EVENT_TELEPHONE_NEWMESSAGEUSER1             2360
#define EVENT_TELEPHONE_NEWMESSAGEUSER2             2361
#define EVENT_TELEPHONE_NEWMESSAGEUSER3             2362
#define EVENT_TELEPHONE_NEWMESSAGEUSER4             2363
#define EVENT_TELEPHONE_NEWMESSAGEUSER5             2364
#define EVENT_TELEPHONE_NEWMESSAGEUSER6             2365
#define EVENT_TELEPHONE_NEWMESSAGEUSER7             2366
#define EVENT_TELEPHONE_NEWMESSAGEUSER8             2367
#define EVENT_TELEPHONE_USERCID                     2368

#define EVENT_TELEPHONE_L2_RING1                    2369
#define EVENT_TELEPHONE_L2_RING2                    2370
#define EVENT_TELEPHONE_L2_RING3                    2371
#define EVENT_TELEPHONE_L2_RING4                    2372
#define EVENT_TELEPHONE_L2_RING5                    2373
#define EVENT_TELEPHONE_L2_RING6                    2374
#define EVENT_TELEPHONE_L2_ANSWERCALL               2375
#define EVENT_TELEPHONE_L2_CALLERID                 2376

#define EVENT_UPS_POWER_LOST                        2400
//#define EVENT_UPS_SHUTDOWN                          2401 // Should not be used
#define EVENT_UPS_POWER_RESTORED                    2402 // Was UPS_SHUTDOWN_CANCEL
#define EVENT_UPS_POWER_RESTART                     2403
#define EVENT_UPS_LOW_BATTERY                       2404

#define EVENT_IRRIGATION_WATERING                   2500
#define EVENT_IRRIGATION_IDLE                       2501

#define EVENT_POOLCIRCUIT_ACTIVATE                  2600
#define EVENT_POOLCIRCUIT_DEACTIVATE                2601

#define EVENT_IO_IRCODE                             2700
#define EVENT_NUMERIC_TRIGGER                       2701
#define EVENT_IO_IRCODE_END                         2702

#define EVENT_OUTPUT_ON                             2800
#define EVENT_OUTPUT_OFF                            2801

#define EVENT_INPUT_ON                              2810
#define EVENT_INPUT_OFF                             2811

#define EVENT_AVSOURCE_SELECT                       2900
#define EVENT_AVSOURCE_DESELECT                     2901

#define EVENT_DISPLAY_SOURCESELECTED                2950
#define EVENT_DISPLAY_ACTIVATED                     2951
#define EVENT_DISPLAY_DEACTIVATED                   2952
#define EVENT_DISPLAY_POWER_UP_DONE                 2953
#define EVENT_DISPLAY_POWER_DOWN_DONE               2954

#define EVENT_AZCTLR_VOLRAMPUP                      2960
#define EVENT_AZCTLR_VOLRAMPUPEND                   2961
#define EVENT_AZCTLR_VOLRAMPDN                      2962
#define EVENT_AZCTLR_VOLRAMPDNEND                   2963
#define EVENT_AZCTLR_POWERON                        2964
#define EVENT_AZCTLR_POWEROFF                       2965
#define EVENT_AZCTLR_SOURCESELECTED                 2966
#define EVENT_AZCTLR_ANYZONEON                      2967
#define EVENT_AZCTLR_ALLZONESOFF                    2968
#define EVENT_AZCTLR_MUTE                           2969

#define EVENT_SERIAL_CODERECV                       3000

#define EVENT_CLIENTGATEWAY_RX                      3100
#define EVENT_HOSTGATEWAY_RX                        3101

#define EVENT_END                             3101

#define CONDITION_START                       3000

#define CONDITION_SYSTEM_DAYTIME                    3103
#define CONDITION_SYSTIMER_ACTIVE                   3104
#define CONDITION_SYSTEM_TBUTTONON                  3105

#define CONDITION_SYSTEMMODE0                       3120
#define CONDITION_SYSTEMMODE1                       3121
#define CONDITION_SYSTEMMODE2                       3122
#define CONDITION_SYSTEMMODE3                       3123
#define CONDITION_SYSTEMMODE4                       3124
#define CONDITION_SYSTEMMODE5                       3125
#define CONDITION_SYSTEMMODE6                       3126
#define CONDITION_SYSTEMMODE7                       3127
#define CONDITION_SYSTEMMODE8                       3128
#define CONDITION_SYSTEMMODE9                       3129

#define CONDITION_SYSTEMTABLETLED1ON                3150
#define CONDITION_SYSTEMTABLETLED2ON                3151
#define CONDITION_SYSTEM_TS7_LIGHTLEDON             3152

#define CONDITION_LIGHTING_SWITCHISON               3200
#define CONDITION_LIGHTING_SCENEISON                3201

#define CONDITION_SECURITY_ZONE_FAULT               3300
#define CONDITION_SECURITY_PART_ISARMEDANY          3301
#define CONDITION_SECURITY_PART_ISALARM             3303
#define CONDITION_SECURITY_PART_ISENTRYDELAY        3304
#define CONDITION_SECURITY_PART_ISEXITDELAY         3305
#define CONDITION_SECURITY_PART_ARM_MODE1           3307
#define CONDITION_SECURITY_PART_ARM_MODE2           3308
#define CONDITION_SECURITY_PART_ARM_MODE3           3309
#define CONDITION_SECURITY_PART_ARM_MODE4           3310
#define CONDITION_SECURITY_PART_ARM_MODE5           3311
#define CONDITION_SECURITY_PART_ARM_MODE6           3312
#define CONDITION_SECURITY_PART_ARM_MODE7           3313

#define CONDITION_PHONE_NEWVOICEMAILS               3400
#define CONDITION_PHONE_NEWVOICEMAILSINBOX          3401

#define CONDITION_HVAC_OFFMODE                      3500
#define CONDITION_HVAC_HEATMODE                     3501
#define CONDITION_HVAC_COOLMODE                     3502
#define CONDITION_HVAC_AUTOMODE                     3503
#define CONDITION_HVAC_HEATRELAYON                  3504
#define CONDITION_HVAC_COOLRELAYON                  3505
#define CONDITION_HVAC_FANRELAYON                   3506
#define CONDITION_HVAC_ROOMTEMPABOVE                3507
#define CONDITION_HVAC_ROOMTEMPBELOW                3508
#define CONDITION_HVAC_HSETPOINTABOVE               3509
#define CONDITION_HVAC_HSETPOINTBELOW               3510
#define CONDITION_HVAC_CSETPOINTABOVE               3511
#define CONDITION_HVAC_CSETPOINTBELOW               3512
#define CONDITION_HVAC_VENTMODE                     3513
#define CONDITION_HVAC_EHEATMODE                    3514

#define CONDITION_OUTPUT_ON                         3600

#define CONDITION_INPUT_ON                          3610

#define CONDITION_NUMERIC_TRIGGER                   3611

#define CONDITION_AUDIO_AVSOURCEACTIVE              3700
#define CONDITION_AUDIO_ZONEISON                    3701
#define CONDITION_AUDIO_ANYZONEON                   3702

#define CONDITION_DISPLAY_ISON                      3800
#define CONDITION_DISPLAY_SOURCEISACTIVE            3801

#define CONDITION_END                         3801

//
// Audio Server Sort Types
//
#define SORT_ALLTRACKS                              0
#define SORT_ALLARTISTS                             1
#define SORT_ALLALBUMS                              2
#define SORT_ALLTRACKSBYARTIST                      3
#define SORT_ALLALBUMSBYARTIST                      4
#define SORT_TRACKSBYARTISTALBUM                    5
#define SORT_ALLPLAYLISTS                           6
#define SORT_TRACKSINPLAYLIST                       7
#define SORT_TRACKSBYALBUM                          12
#define SORT_ALLGENRES                              13
#define SORT_ALBUMSBYGENRE                          14
#define SORT_ALLALBUMS_COVER                        15
#define SORT_ALLALBUMSBYARTIST_COVER                16
#define SORT_ALLALBUMSBYGENRE_COVER                 17
#define SORT_ALLARTISTS_CHUNK                       18
#define SORT_ALLALBUMS_COVER_CHUNK                  19

//
// Tablet screen size (resolution in pixels).
//
#define HL_FORMAT_START                             0
#define HL_800x600                                  0
#define HL_240x320                                  1
#define HL_800x480                                  2
#define HL_320x240                                  3
#define HL_240x240                                  4
#define HL_320x320                                  5
#define HL_FORMAT_END                               5


#define HL_400x300                                  6
#define HL_OLE_FORMAT                               7
#define HL_OSD_RES                                  8
#define HL_HR2_FORMAT                               9

//
// Processors needed at run-time.
// See also platform.h.
//
//  Processor int           Build Define    Device / Platform
//  -------------           ------------    -----------------
//  HL_PROC_X86             N/A             Old, not valid
//  HL_PROC_XSCALE          N/A             Old, not valid
//
//  HL_PROC_WIN32           WIN32           WIN32: XP
//
//  HL_PROC_X86A            HL_X86A         In Wall
//  HL_PROC_X86B            HL_X86B         DT366 etc
//  HL_PROC_X86C            HL_X86C         HC6, 8, 12
//
//  HL_PROC_ARM             HL_ARM          PPC2002
//  HL_PROC_ARMV4A          HL_ARMV4A       DT370, etc
//  HL_PROC_ARMV4B          HL_ARMV4B       DT375, etc
//  HL_PROC_ARMV4C          HL_ARMV4C       PPC2003
//
//  HL_PROC_ARMV4IA         HL_ARMV4IA      CM-X255 MultiBrick
//  HL_PROC_ARMV4IB         HL_ARMV4IB      CM-X255 Touch Screen
//  HL_PROC_ARMV4IC         HL_ARMV4IC      CM-X300 Touch Screen
//
//  HL_PROC_CLIENTGATEWAY   N/A             Gateway connecting to other gateway
//  HL_PROC_IPHONE          IPHONE          Gateway connecting to other gateway
//
#define HL_PROC_UNKNOWN                             -1  // JAVAPAD

#define HL_PROC_ARM                                 0
#define HL_PROC_X86                                 1
#define HL_PROC_WIN32                               2
#define HL_PROC_XSCALE                              3
#define HL_PROC_ARMV4A                              4
#define HL_PROC_ARMV4B                              5
#define HL_PROC_X86A                                6
#define HL_PROC_X86B                                7
#define HL_PROC_ARMV4C                              8
#define HL_PROC_ARMV4IB                             9
#define HL_PROC_CLIENTGATEWAY                       10
#define HL_PROC_IPHONE                              11
#define HL_PROC_ARMV4IC                             12
#define HL_PROC_ARMV4IA                             13
#define HL_PROC_X86C                                14
#define HL_PROC_IPAD                                15

//
// Options
//
#define TEXT_FORMAT_MASK                            0x000007FF

#define HL_HLEFT                                    0x00000001
#define HL_HCENTER                                  0x00000002
#define HL_HRIGHT                                   0x00000004    // Mask = 0x00000111
#define HL_VBOTTOM                                  0x00000008
#define HL_VCENTER                                  0x00000010
#define HL_VTOP                                     0x00000020    // Mask = 0x00111000
#define HL_TEXTURE                                  0x00000100
#define HL_ARROWRIGHT                               0x00000200
#define HL_ARROWLEFT                                0x00000400
#define HL_TRUNCATE                                 0x00000800


#define HL_QUICKSYSMENU                             0x00000400
#define HL_POPUP_ALPHASCROLL                        0x00000800
#define HL_HZ_SIDEBAR                               0x00001000

#define HL_ICONDEFAULT                              0x00000000
#define HL_ICONCENTER                               0x00000001
#define HL_ICONLEFTTEXT1                            0x00000002
#define HL_ICONRIGHTTEXT1                           0x00000003
#define HL_ICONLEFTTEXT2                            0x00000004
#define HL_ICONRIGHTTEXT2                           0x00000005
#define HL_ICONLEFTTEXT4                            0x00000006
#define HL_ICONLEFTTEXTMAX                          0x00000007
#define HL_ICONRIGHTTEXT4                           0x00000008
#define HL_ICONRIGHTTEXTMAX                         0x00000009
#define HL_ICONTOPTEXT                              0x00000010
#define HL_ICONBOTTOMTEXT                           0x00000011
#define HL_ICONTOPHALF                              0x00000014
#define HL_ICONBOTTOMHALF                           0x00000015
#define HL_ICONLEFTHALF                             0x00000016
#define HL_ICONRIGHTHALF                            0x00000017
#define HL_ICONTOPTHIRD                             0x00000018
#define HL_ICONBOTTOMTHIRD                          0x00000019
#define HL_ICONLEFTTHIRD                            0x00000020
#define HL_ICONRIGHTTHIRD                           0x00000021
#define HL_ICON75PCTHT                              0x00000022
#define HL_ICONTOP1X                                0x00000023
#define HL_ICONTOP2X                                0x00000024
#define HL_ICONTOP3X                                0x00000025
#define HL_ICONTOP4X                                0x00000026
#define HL_ICONBOTTOM1X                             0x00000027
#define HL_ICONBOTTOM2X                             0x00000028
#define HL_ICONBOTTOM3X                             0x00000029
#define HL_ICONBOTTOM4X                             0x00000030
#define HL_ICONRIGHTTEXT_150PCT                     0x00000031
#define HL_ICONLEFTTEXT_150PCT                      0x00000032
#define HL_ICONCENTERMAX                            0x00000033
#define HL_ICONTEXTOVER                             0x00000034
#define HL_ICONCENTER2X                             0x00000035
#define HL_ICONTOP150PCT                            0x00000036
#define HL_ICONCENTER1X                             0x00000037
#define HL_ICONTOP125PCT                            0x00000038

//
// UPS settings.
//
#define UPS_STATE_UNKNOWN                           0
#define UPS_STATE_ACTIVE                            2
//#define UPS_STATE_SHUTDOWN_PENDING                  1 No longer used
#define UPS_STATE_LOW_BATTERY                       4

//
// HVAC settings.
//
// Note: The Aprilaire 8870 has a fixed range of 40-90, with a 2 degree separation.
//
//      The Honeywell T8635L can be configured: our documentation sets the T8635L
//      to match the Aprilaire as closely as we can: 43-90, 2 degree separation.
//      On the T8635L we set the max for heating to 88, the min for cooling to 45
//      (it won't go lower) and the separation to 2.
//
//      The HAI RC-80 thermostats can be configured: so we set its cool setpoint
//      limit to 42 and heat setpoint limit to 88.
//
#define HVAC_DEGS_SEPARATION                        2
#define HVAC_MINS_SEPARATION                        60
#define HVAC_MIN_SPDEG                              40
#define HVAC_MAX_SPDEG                              90
#define HVAC_HEATSP_IGNORE                          -100
#define HVAC_COOLSP_IGNORE                          200
#define HVAC_TYPE_UI                                0
#define HVAC_TYPE_TSTATS                            1
#define HVAC_TYPE_HSTATS                            2

#define HVAC_STATE_OFF                              0x00  // State of equip, lower 4 bits
#define HVAC_STATE_HEAT                             0x01
#define HVAC_STATE_EHEAT                            0x02
#define HVAC_STATE_COOL                             0x03
#define HVAC_STATE_AUTO                             0x04
#define HVAC_STATE_NOTOK                            0x05
#define HVAC_STATE_VENT                             0x06
#define HVAC_STATE_DEHUMID							HVAC_STATE_HEAT
#define HVAC_STATE_HUMID							HVAC_STATE_COOL

#define HVAC_STATE_COOLACTIVE                       0x10  // Heat Active bit, can be combine with above state
#define HVAC_STATE_HEATACTIVE                       0x20  // Cool Active bit, can be combine with above state
#define HVAC_STATE_ALARM                            0x40  // Alarm State
#define HVAC_STATE_OVERRIDE                         0x80  // Network override bit
#define HVAC_STATE_HUMIDACTIVE						HVAC_STATE_COOLACTIVE
#define HVAC_STATE_DEHUMIDACTIVE					HVAC_STATE_HEATACTIVE

#define FAN_STATE_AUTO                              0
#define FAN_STATE_CONT                              1
#define FAN_STATE_DONTCARE                          2
#define FAN_STATE_HI                                3
#define FAN_STATE_LOW                               4

#define PRG_STATE_RUN                               0
#define PRG_STATE_PHOLD                             1
#define PRG_STATE_THOLDNEXT                         2
#define PRG_STATE_THOLDTIME                         3

//
// Voice message constants.
//
#define HL_VMSG_NEW                                 1    // New message
#define HL_VMSG_OLD                                 2    // Old message
#define HL_VMSGTAG_OLD                              4    // New, but tagged to become old
#define HL_VMSG_ALL                                 7    // All messages

#define HL_MAILBOX_ALL                              0
#define HL_MAILBOX_HOUSE                            1

//
// AudioTrack file status
//
#define AUDIO_TRACK_OK                              0
#define AUDIO_TRACK_BAD_FILE                        1
#define AUDIO_TRACK_BAD_FORMAT                      2

//
// Irrigation Program States
//
#define IRRIGATION_STATE_NOTREADY                   0
#define IRRIGATION_STATE_OFF                        1
#define IRRIGATION_STATE_RUNPROGRAM                 2
#define IRRIGATION_STATE_MANUAL                     3


/**/
/*                                                                    */
/*                                                                    */
/* Lighting Scene/Device Control                                      */
/*                                                                    */
/*                                                                    */
/**/

#define TIMETYPE_SUNRISE                            0
#define TIMETYPE_SUNSET                             1
#define TIMETYPE_CLOCK                              2

#define IGNORE_STATE                                0xF0000000

// Lighting Controller Types
#define LIGHTING_DEVICE_CONTROLLER                  0
#define LIGHTING_SCENE_CONTROLLER                   1

// Device Actions
#define ACTION_ACTIVATE                             0
#define ACTION_DEACTIVATE                           1
#define ACTION_DIM                                  2
#define ACTION_BRIGHT                               3
#define ACTION_BLINK                                20
#define ACTION_PRESET_START                         100
#define ACTION_PRESET_END                           131
#define ACTION_RAISE                                132
#define ACTION_LOWER                                133
#define ACTION_BEGIN_RAISE                          134
#define ACTION_BEGIN_LOWER                          135
#define ACTION_END_RAISELOWER                       136

#define ACTION_DIM_START                            150  // 0%
#define ACTION_DIM_END                              250  // 100%
#define ACTION_DIM_START_FAST                       251  // 0%
#define ACTION_DIM_END_FAST                         351  // 100%

#define ACTION_MOMENTARYDN_START                    400
#define ACTION_MOMENTARYDN_END                      499
#define ACTION_MOMENTARYUP_START                    500
#define ACTION_MOMENTARYUP_END                      599
#define ACTION_MOMENTARYTAP_START                   600
#define ACTION_MOMENTARYTAP_END                     699


// Special scene ids
#define SCENE_RAISELOWER                            0
#define SCENE_ALL_ON                                1
#define SCENE_ALL_OFF                               2
#define SCENE_BASE                                  3
#define SCENE_FADE                                  100

// Scene Actions
#define ACTION_FADE_SCENE                           0
#define ACTION_INSTANT_SCENE                        1
#define ACTION_FADE_UP                              2
#define ACTION_FADE_DOWN                            3
#define ACTION_FADE_STOP                            4

#define EXECUTE_ONBUTTONUP                          0
#define EXECUTE_ONBUTTONDOWN                        1
#define EXECUTE_ONCLICK                             2
#define EXECUTE_ANY                                 3

// Scene Types
#define SCENETYPE_SCENE                             0
#define SCENETYPE_DIM                               1
#define SCENETYPE_BRIGHT                            2
#define SCENETYPE_TOGGLE                            3
#define SCENETYPE_PANIC                             4
#define SCENETYPE_MOMENTARY                         5
#define SCENETYPE_SCENE_MOMENTARY                   6

// Device states
#define STATE_ON                                    0
#define STATE_OFF                                   1
#define STATE_PRESET_START                          100
#define STATE_PRESET_END                            131
#define STATE_DIM_START                             150
#define STATE_DIM_END                               250
#define STATE_BLINK                                 20

/**/
/*                                                                      */
/*                                                                      */
/* Audio System                                                         */
/*                                                                      */
/*                                                                      */
/**/

// Player States
#define PLAYER_STATE_NOTREADY                       1000
#define PLAYER_STATE_IDLE                           1001
#define PLAYER_STATE_PLAYING                        1002
#define PLAYER_STATE_BUFFERING                      1003
#define PLAYER_STATE_PAUSED                         1004

// Player Commands
#define HL_PLAYER_STOP                              2000
#define HL_PLAYER_PAUSE                             2001
#define HL_PLAYER_RESUME                            2002
#define HL_PLAYER_BEGINSTREAM                       2003
#define HL_PLAYER_ENDSTREAM                         2004
#define HL_PLAYER_POWERON                           2005
#define HL_PLAYER_POWEROFF                          2006

// Player Button Disabled Bitmasks
#define PLAYER_DISABLE_REMOVE                       0x80
#define PLAYER_DISABLE_REPEAT                       0x40
#define PLAYER_DISABLE_SHUFFLE                      0x20
#define PLAYER_DISABLE_PREV                         0x10
#define PLAYER_DISABLE_STOP                         0x08
#define PLAYER_DISABLE_PLAY                         0x04
#define PLAYER_DISABLE_PAUSE                        0x02
#define PLAYER_DISABLE_NEXT                         0x01

// Roku UDP Commands
#define ROKU_STOP                                   10
#define ROKU_PAUSE                                  11
#define ROKU_RESUME                                 12
#define ROKU_SETVOLUME                              13
#define ROKU_NOTIFYVOLUME                           14

// MP3 Source States
#define MP3SOURCE_NONMP3                            0
#define MP3SOURCE_PLAYLIST                          3000
#define MP3SOURCE_IRADIO                            3001
#define MP3SOURCE_OFFLINE                           3002

// MP3 Source State Flags
#define MP3SOURCE_SERVICEACTIVE                     0x00020000
#define MP3SOURCE_REALTIMELIB                       0x00040000
#define MP3SOURCE_NODELPLAYLISTITEMS                0x00080000
#define MP3SOURCE_NOADDPLAYLISTITEMS                0x00100000
#define MP3SOURCE_GENREOK                           0x00200000
#define MP3SOURCE_AUTOPILOTMODE                     0x00400000
#define MP3SOURCE_NOSORTARTIST                      0x00800000
#define MP3SOURCE_NOPLAYARTIST                      0x01000000
#define MP3SOURCE_NOSORTTRACKS                      0x02000000
#define MP3SOURCE_HASGENRESORT                      0x04000000
#define MP3SOURCE_NOEDITPLAYLISTS                   0x08000000
#define MP3SOURCE_NORANDREPEAT                      0x10000000
#define MP3SOURCE_COVERSAVAIL                       0x20000000
#define MP3SOURCE_NOSTOP                            0x40000000
#define MP3SOURCE_NODELPLAYQUEUE                    0x80000000

#define AUX_SOURCE_TUNER                            3100      // basic Tuner type in lower 16 bits
#define AUX_SOURCE_TUNER_XM                         3101
#define AUX_SOURCE_CK12KEYPAD                       3102
#define AUX_SOURCE_USERDEF                          3103
#define AUX_SOURCE_TUNER_AMFM_EURO50                3104         // 50kHz FM step size
#define AUX_SOURCE_TUNER_SIRIUS                     3105
#define AUX_SOURCE_TUNER_HDAMFM                     3106
#define AUX_SOURCE_TUNER_AMFMXM                     3107
#define AUX_SOURCE_TUNER_AMFMSIRIUS                 3108
#define AUX_SOURCE_TUNER_AMFM_EURO100               3109         // 100kHz FM step size
#define AUX_SOURCE_DVDCHANGER                       3120
#define AUX_SOURCE_KEYPAD                           3121
#define AUX_SOURCE_KEYPAD_NEXT_PREV                 3122
#define AUX_SOURCE_TUNER_DEVICEPRESETS              0x00010000   // flags in upper bits
#define AUX_SOURCE_TUNER_MCAST_CHANNELS             0x00020000
#define AUX_SOURCE_TUNER_NODIRECTTUNE               0x00040000
#define AUX_SOURCE_TUNER_NORENAMEPRESETS            0x00080000
#define AUX_SOURCE_TUNER_LONG_RDS_RT                0x00100000
#define AUX_SOURCE_TUNER_SETDEVICEPRESETS           0x00200000
#define AUX_SOURCE_TUNER_MCAST_UPDOWN               0x00400000
#define AUX_SOURCE_NULL_INTERFACE                   0x10000000

// Tuner States
#define TUNER_OFF                                   3101
#define TUNER_AM                                    3102
#define TUNER_FM                                    3103
#define TUNER_XM                                    3104
#define TUNER_SIRIUS                                3105

// Display Source Control
#define DISPLAY_CYCLING                             0   // Open-loop cycling
#define DISPLAY_DISCRETE                            1   // Discrete, but when only switch when we think we need to
#define DISPLAY_DISCRETE_VERIFY                     2   // Discrete, but we ALWAYS send commands
#define DISPLAY_CYCLING_NOTRACK                     3   // Open-loop cycling, we don't try to track on/off (like a TV remote)

// PlayList States
#define PLAYLIST_PAUSED                             4000
#define PLAYLIST_IDLE                               4001
#define PLAYLIST_READY                              4002
#define PLAYLIST_STOPPED                            4003
#define PLAYLIST_PLAYING                            4004
#define PLAYLIST_WORKING                            4005

// DVR Commands
#define DVR_GOTOTIME                                0
#define DVR_PLAY                                    1
#define DVR_STOP                                    2
#define DVR_PAUSE                                   3
#define DVR_PLAYBACKSPEED                           4
#define DVR_NEXTCLIP                                5
#define DVR_PREVCLIP                                6
#define DVR_SAVECLIP                                7

// DVR Flags
#define DVR_FILEFRAGMENT_PREV                       0x00000001 // Clip is a framgment owned by previous clip
#define DVR_AVI_FOUND                               0x00000002 // We have a saved AVI for this clip

// DVR Playback status
#define DVR_STOPPEDVIDEO                            0
#define DVR_STOPPEDNOVIDEO                          1
#define DVR_PLAYINGVIDEO                            2
#define DVR_PLAYINGNOVIDEO                          3
#define DVR_STOPPEDSAVEDVIDEO                       4

// PlayList/Zone Commands
#define HL_PLAYLIST_SHUFFLE                         5002
#define HL_PLAYLIST_REPEAT                          5003
#define HL_PLAYLIST_REWIND                          5004
#define HL_PLAYLIST_PREVIOUS                        5005
#define HL_PLAYLIST_STOP                            5006
#define HL_PLAYLIST_PAUSE                           5007
#define HL_PLAYLIST_PLAY                            5008
#define HL_PLAYLIST_NEXT                            5009
#define HL_PLAYLIST_PLAYLAST                        5010
#define HL_PLAYLIST_FAST_FORWARD                    5011
#define HL_PLAYLIST_SETFREQUENCY                    5013
#define HL_PLAYLIST_SETTUNERMODE                    5014
#define HL_PLAYLIST_BEGINRAMPVOLUP                  5015
#define HL_PLAYLIST_BEGINRAMPVOLDN                  5016
#define HL_PLAYLIST_ENDRAMPVOLUP                    5017
#define HL_PLAYLIST_ENDRAMPVOLDN                    5018
#define HL_PLAYLIST_TOGGLEMUTE                      5019

// Audio Keypad/Zone Commands
#define HL_KEYPAD_BTN1                              5020
#define HL_KEYPAD_BTN2                              5021
#define HL_KEYPAD_BTN3                              5022
#define HL_KEYPAD_BTN4                              5023
#define HL_KEYPAD_BTN5                              5024
#define HL_KEYPAD_BTN6                              5025
#define HL_KEYPAD_BTN7                              5026
#define HL_KEYPAD_BTN8                              5027
#define HL_KEYPAD_BTN9                              5028
#define HL_KEYPAD_BTN10                             5029
#define HL_KEYPAD_NEXTPAGE                          5030
#define HL_KEYPAD_MAINMENU                          5031
#define HL_KEYPAD_FAV                               5032
#define HL_KEYPAD_MUTE                              5033
#define HL_KEYPAD_CH_PLUS                           5034
#define HL_KEYPAD_CH_MINUS                          5035
#define HL_KEYPAD_OFF                               5036
#define HL_KEYPAD_ON                                5037
#define HL_KEYPAD_CURRENT_DEVICE                    5038
#define HL_KEYPAD_ACTIVE_SOURCE                     5039

#define HL_PLAYLIST_ON                              5050
#define HL_PLAYLIST_OFF                             5051

/**/
/*                                                                      */
/*                                                                      */
/* Telephone System                                                     */
/*                                                                      */
/*                                                                      */
/**/

#define TTS_VOICE_MALE                              0
#define TTS_VOICE_FEMALE                            1

#define TTS_SAY_DATETIME                            0x00000001
#define TTS_SAY_NAME                                0x00000002
#define TTS_SAY_NUMBER                              0x00000004

#define HANDSHAKE_LIMIT_MSEC                        30000   // limit handshaking to 30 seconds

/**/
/*                                                                      */
/*                                                                      */
/* Event Log Types / Sub-Types                                          */
/*                                                                      */
/*                                                                      */
/**/

//
// Program errors
//
#define ERROR_TYPE_GATEWAY_EXCEPTION                0x00000001
#define ERROR_TYPE_GATEWAY_ASSERT                   0x00000002
#define ERROR_TYPE_TABLET_EXCEPTION                 0x00000004
#define ERROR_TYPE_TABLET_ASSERT                    0x00000008
#define ERROR_TYPE_GATEWAY_STARTUP                  0x00000010
#define ERROR_TYPE_GATEWAY_SHUTDOWN                 0x00000020
#define ERROR_TYPE_MONITOR_MESSAGE                  0x00000040


//
// System
//
#define SYSTEM_TYPE_SERVERHOST                      0x00000001 // Connections coming/going
#define SYSTEM_TYPE_STARTUP                         0x00000002 // System startup/shutdown
#define SYSTEM_TYPE_LAYOUT                          0x00000004 // Layout server changes
#define SYSTEM_TYPE_COMPONENT                       0x00000008 // Component add/remove
#define SYSTEM_TYPE_ERROR                           0x00000010 // Program errors / asserts
#define SYSTEM_TYPE_MODE                            0x00000020 // House mode changes
#define SYSTEM_TYPE_UPS                             0x00000040 // UPS message
#define SYSTEM_TYPE_PHONELOGIN                      0x00000080 // Connection established via DTMF login

// Subtypes for SYSTEM_SERVHOST
#define SYSTEM_STYPE_SERVHOST_CONNECTED             1
#define SYSTEM_STYPE_SERVHOST_CONNECTIONREFUSED     2
#define SYSTEM_STYPE_SERVHOST_DISCONNECTED          3
#define SYSTEM_STYPE_SERVHOST_LOGINREJECTED         4


//
// Security
//
#define SECURITY_PARTSTAT_CHANGE_DETECTED           0x00000001      // Partition Status changed
#define SECURITY_ZONESTAT_CHANGE_DETECTED           0x00000002      // Zone status changed
#define SECURITY_USER_COMMAND                       0x00000003      // User tried to arm/disarm/etc through us

// Partition state bits
#define SEC_PARTITION_ARMED       0x00000001
#define SEC_PARTITION_ARMABLE     0x00000004
#define SEC_PARTITION_BYPASSABLE  0x00000008
#define SEC_PARTITION_ALARMACT    0x00000010
#define SEC_PARTITION_FIRE        0x00000020
#define SEC_PARTITION_ENTRY_DEL   0x00000040
#define SEC_PARTITION_EXIT_DEL    0x00000080
#define SEC_PARTITION_NORESPONSE  0x00000100

// Partition caps 
#define SEC_PARTITION_CAPS_HASPANIC     0x00000001
#define SEC_PARTITION_CAPS_CANARM       0x00000002
#define SEC_PARTITION_CAPS_BYPASSDATA   0x00000004
#define SEC_PARTITION_CAPS_CANREARM     0x00000008


// Zone state bits
#define SEC_ZONE_BYPASSABLE       0x00000001
#define SEC_ZONE_BYPASSED         0x00000002
#define SEC_ZONE_FAULTED          0x00000004
#define SEC_ZONE_TROUBLE          0x00000008
#define SEC_ZONE_UNBYPASSABLE     0x00000010

// User commands
#define SEC_USER_ARM              0x00000001
#define SEC_USER_DISARM           0x00000003
#define SEC_USER_PANIC            0x00000004
#define SECU


//
// HVAC
//
#define HVAC_TYPE_STATE                             0x00000001      // State change, setpoint, program mode etc
#define HVAC_TYPE_USER_CHANGE                       0x00000002      // User changed something, program mode, program, setoint etc

// Sub-types for user change data
#define HVAC_STYPE_USER_CHANGE_SETPOINT             0
#define HVAC_STYPE_USER_CHANGE_RUNMODE              1
#define HVAC_STYPE_USER_CHANGE_PROGRAM              2

// Numeric Sensors
#define SENSOR_TYPE_VALUE                           0x00000001

// Variable State
#define STATE_TYPE_SET                              0x00000001
#define STATE_TYPE_UNSET                            0x00000002

//
// PHONE
//
#define PHONE_TYPE_ANSWER                           0x00000001

//
// AUDIO
//
#define CT610_TYPE_CONFIG                           0x00000001
#define AZCTLR_TYPE_INFO                            0x00000002

//
// Irrigation
//
#define IRRIGATION_TYPE_ZONESTATECHANGE             0x00000001
#define IRRIGATION_TYPE_MODECHANGE                  0x00000002

// Pool
#define POOL_TYPE_STATECHANGE                       0x00000001

// Pool status integer defines
#define POOLINT_AIRTEMP                             0
#define POOLINT_POOLTEMP                            1
#define POOLINT_SPATEMP                             2
#define POOLINT_POOLSP                              3
#define POOLINT_SPASP                               4
#define POOLINT_POOLHEATMODE                        5
#define POOLINT_SPAHEATMODE                         6
#define POOLINT_MINSPPOOL                           7
#define POOLINT_MAXSPPOOL                           8
#define POOLINT_MINSPSPA                            9
#define POOLINT_MAXSPSPA                            10
#define POOLINT_POOLHEATACT                         11
#define POOLINT_SPAHEATACT                          12
#define POOLINT_POOLFILTER                          13
#define POOLINT_SPAFILTER                           14
#define POOLINT_POOLLIGHTS                          15
#define POOLINT_POOLCOOLSP                          16
#define POOLINT_PH                                  17
#define POOLINT_ORP                                 18
#define POOLINT_PHSP                                19
#define POOLINT_ORPSP                               20
#define POOLINT_PH_DOSING                           21
#define POOLINT_ORP_DOSING                          22
#define POOLINT_ALARMS                              23
#define POOLINT_SATURATION                          24
#define POOLINT_SALTPPM                             25
#define POOLINT_PH_TANK                             26
#define POOLINT_ORP_TANK                            27
#define POOLINT_NSTATUS_INTS                        28

/**/
/*                                                                    */
/*                                                                    */
/* Messages                                                           */
/*                                                                    */
/*                                                                    */
/**/

// First message: see end of file below for last one. Used to reject
// junk messages.
#define    HLM_MESSAGES_FIRST                       1
#define    HLM_GENERAL_START                        1

//
// General Communications.
//
// Note that there is no HLM_REMOVE_CLIENTA, since the socket is closed.
//
#define    HLM_ADD_CLIENTQ                          1       // Sent over "broadcast" port
#define    HLM_ADD_CLIENTA                          2       // Sent over "broadcast" port
#define    HLM_KEEPALIVE                            3
#define    HLM_PINGBRICKQ                           4
#define    HLM_PINGBRICKA                           5
#define    HLM_SETBRICKCONFIGQ                      6
#define    HLM_SETBRICKCONFIGBCQ                    7
#define    HLM_NONE                                 10
#define    HLM_LEGACY_CLIENTLOGINQ                  11      // Old Login
#define    HLM_LEGACY_CLIENTLOGINA                  12
#define    HLM_CLIENTLOGINREJECTED                  13
#define    HLM_CHALLENGESTRINGQ                     14
#define    HLM_CHALLENGESTRINGA                     15
#define    HLM_PING_SERVERQ                         16
#define    HLM_PING_SERVERA                         17
#define    HLM_REMOVE_CLIENTQ                       18
#define    HLM_SECURITY_PARTITION_ENTRYBRCST        19
#define    HLM_SECURITY_PARTITION_EXITBRCST         20
#define    HLM_SECURITY_ZONENAME_QUERY_DATA         21
#define    HLM_DEBUG_TRACE                          22
    #define    HLM_SERVHOST_AMILOCALQ                   24
    #define    HLM_SERVHOST_AMILOCALA                   25
    #define    HLM_TABLET_REPORTSTATS                   26
    #define    HLM_CLIENTLOGINQ                         27
    #define    HLM_CLIENTLOGINA                         28

    //
    // Normally, messages go from a client on the tablet to the gateway: the sender
    // ID is used to keep track of which client on the tablet sent the message.
    //
    // If the gateway wants to send a message but not in response to a specific question,
    // it will use the following m_ushSenderID to indicate that the message is targeted at
    // all listeners.
    //
    #define    HLM_ALL_SENDERS_ID               0x7FFF


    //
    // General unsolicited messages: specific system unsolicited messages are
    // listed with each system.
    //
    #define    HLM_ERRORS_START                 30
    #define    HLM_UNKNOWN                              30
    #define    HLM_BAD_PARAMETERS                       31
    #define    HLM_BAD_LOCK                             32
    #define    HLM_FORWARDFAILED                        33
    #define    HLM_ERRORS_END                   33

    //
    // Dummy / test data and messages.
    //
    #define    HLM_GENERIC_DATAQ                40
    #define    HLM_GENERIC_DATAA                41
    #define    HLM_GENERIC_STREAM               42
#define    HLM_GENERIC_STREAMQ              43
#define    HLM_GENERIC_STREAMA              44
#define    HLM_DROP_CONNECTION              45

// Tablet connection/lock messages
#define    HLM_TABLETLOCKOUTQ                       50
#define    HLM_TABLETLOCKOUTA                       51

/**/
/*                                                                    */
/* House Mode messages                                                */
/*                                                                    */
/**/
#define    HLM_MODE_START                   100

// Unsolicited
#define    HLM_MODE_CHANGED                         100

// Q/A
#define    HLM_MODE_GETMODEQ                        110
#define    HLM_MODE_GETMODEA                        111
#define    HLM_MODE_SETMODEQ                        112
#define    HLM_MODE_SETMODEA                        113
#define    HLM_MODE_END                     113

#define    HLM_SYSTEM_NOTIFYTABLETUSEQ              114 + 2
#define    HLM_SYSTEM_NOTIFYTABLETUSEA              115 + 2
#define    HLM_SYSTEM_TOGGLEBUTTONSTATE             116 + 2

#define    HLM_AUDIO_TOGGLEBUTTONSTATE              117 + 2

#define    HLM_SYSTEM_NOTIFYTABLETEXITSLEEP         118 + 2
#define    HLM_SYSTEM_TABLETSETDISPLAYPOWERSTATE    119 + 2
#define    HLM_SYSTEM_IDTABLET                      120 + 2
#define    HLM_SYSTEM_TABLETREMOTEKEY               121 + 2
#define    HLM_SYSTEM_NOTIFYTABLETBUTTON            122 + 2
#define    HLM_SYSTEM_SETTABLETLED                  124 + 2
#define    HLM_SYSTEM_NOTIFYCLEARQACACHE            125 + 2
#define    HLM_EMAIL_ACCOUNTCHANGE                  126 + 2
#define    HLM_SYSTEM_TABLETSCREENSAVERSTART        127 + 2
#define    HLM_SYSTEM_NOTIFYACTIVEPAGE              128 + 2

#define    HLM_GENERAL_END                  129 + 2


// Global Unsolicited
#define    HLM_TABLET_JUMPTAB                       5490
#define    HLM_TABLET_POPMESSAGE                    5491
#define    HLM_CONFIG_POPMESSAGE                    5492


//                                                                    */
// Security messages                                                  */
//                                                                    */
#define    HLM_SECURITY_START               1000

// Unsolicted
#define    HLM_SECURITY_COMMANDFAILED               1001
#define    HLM_SECURITY_STATUSCHANGED               1002
#define    HLM_SECURITY_SETALARMWAVINFOQ            1003

// Client add/remove
#define    HLM_SECURITY_GETALLPARTSQ                1005
#define    HLM_SECURITY_GETALLPARTSA                1006
#define    HLM_SECURITY_ADDCLIENTBYINDEXQ           1007
#define    HLM_SECURITY_ADDCLIENTBYINDEXA           1008
#define    HLM_SECURITY_ADDCLIENTBYPARTIDQ          1009
#define    HLM_SECURITY_ADDCLIENTBYPARTIDA          1010
#define    HLM_SECURITY_REMOVECLIENTQ               1011
#define    HLM_SECURITY_REMOVECLIENTA               1012
#define    HLM_SECURITY_GETALLPARTSTATBYIDQ         1013
#define    HLM_SECURITY_GETALLPARTSTATBYIDA         1014

// Config
#define    HLM_SECURITY_GETALLSYSTEMDATAQ           1020 // Get all security systems, partitions, and zones
#define    HLM_SECURITY_GETALLSYSTEMDATAA           1021

#define    HLM_SECURITY_GETALLBYCOMPIDQ             1022 // Get all partition and zone ids and names for a system
#define    HLM_SECURITY_GETALLBYCOMPIDA             1023

// System config
#define    HLM_SECURITY_SETLOGINPINGQ               1024 // Sets login key for system
#define    HLM_SECURITY_SETLOGINPINGA               1025

// Partition config
#define    HLM_SECURITY_GETALLPARTNDATABYIDQ        1027 // Get all partition data for a single system
#define    HLM_SECURITY_GETALLPARTNDATABYIDA        1028
#define    HLM_SECURITY_ADDNEWPARTITIONQ            1029 // Add a new partition to a system
#define    HLM_SECURITY_ADDNEWPARTITIONA            1030
#define    HLM_SECURITY_SETPARTCONFIGBYIDQ          1031 // Config a partition
#define    HLM_SECURITY_SETPARTCONFIGBYIDA          1032
#define    HLM_SECURITY_DELPARTITIONBYIDQ           1037 // Remove whole partition from system
#define    HLM_SECURITY_DELPARTITIONBYIDA           1038
#define    HLM_SECURITY_SETPARTAREASBYIDQ           1039 // Set all areas in a system partitino
#define    HLM_SECURITY_SETPARTAREASBYIDA           1040

// Zone config
#define    HLM_SECURITY_ADDNEWZONEBYIDQ             1044 // Add a new zone to a system
#define    HLM_SECURITY_ADDNEWZONEBYIDA             1045
#define    HLM_SECURITY_SETZONENAMEBYIDQ            1046 // Rename a partition
#define    HLM_SECURITY_SETZONENAMEBYIDA            1047
#define    HLM_SECURITY_DELZONEBYIDQ                1048 // Remove whole partition from system
#define    HLM_SECURITY_DELZONEBYIDA                1049
#define    HLM_SECURITY_SETZONEPARTSBYIDQ           1050 // Set which partitions a zone exists in
#define    HLM_SECURITY_SETZONEPARTSBYIDA           1051
#define    HLM_SECURITY_SETZONECONFIGBYIDQ          1052 // Force zone to always bypassable
#define    HLM_SECURITY_SETZONECONFIGBYIDA          1053 // Force zone to always bypassable
#define    HLM_SECURITY_MOVEZONEBYIDQ               1054
#define    HLM_SECURITY_MOVEZONEBYIDA               1055


// Security System Commands
#define    HLM_SECURITY_KEYPAD_DISARMQ              1060 // Disarm
#define    HLM_SECURITY_KEYPAD_DISARMA              1061
#define    HLM_SECURITY_KEYPAD_ARMQ                 1062 // Arm in away mode
#define    HLM_SECURITY_KEYPAD_ARMA                 1063

#define    HLM_SECURITY_GETZONEOPTSQ                1070
#define    HLM_SECURITY_GETZONEOPTSA                1071

#define    HLM_SECURITY_GETFAULTRPTBYPARTIDQ        1076
#define    HLM_SECURITY_GETFAULTRPTBYPARTIDA        1077

#define    HLM_SECURITY_BYPASSZONEBYIDQ             1080
#define    HLM_SECURITY_BYPASSZONEBYIDA             1081
#define    HLM_SECURITY_UNBYPASSZONEBYIDQ           1082
#define    HLM_SECURITY_UNBYPASSZONEBYIDA           1083

#define    HLM_SECURITY_GETARMMODECONFIGQ           1084
#define    HLM_SECURITY_GETARMMODECONFIGA           1085
#define    HLM_SECURITY_SETARMMODECONFIGQ           1086
#define    HLM_SECURITY_SETARMMODECONFIGA           1087

#define    HLM_SECURITY_GETALLPARTINFOBYINDEXQ      1088
#define    HLM_SECURITY_GETALLPARTINFOBYINDEXA      1089
#define    HLM_SECURITY_GETALLPARTINFOBYIDQ         1090
#define    HLM_SECURITY_GETALLPARTINFOBYIDA         1091

#define    HLM_SECURITY_END                 1091


/**/
/*                                                                    */
/* HVAC messages                                                      */
/*                                                                    */
/**/
#define    HLM_HVAC_START                   2000

// Unsolicted messages
#define    HLM_HVAC_STATECHANGED                    2000
#define    HLM_HVAC_PROGRAMCHANGED                  2001
#define    HLM_HVAC_HISTORYDATA                     2002
#define    HLM_HVAC_USAGEDATA                       2003

// General Q+A messages
#define    HLM_HVAC_GETLOCALINFOQ                   2004
#define    HLM_HVAC_GETLOCALINFOA                   2005
#define    HLM_HVAC_GETSCALEQ                       2006
#define    HLM_HVAC_GETSCALEA                       2007
#define    HLM_HVAC_SETSCALEQ                       2008
#define    HLM_HVAC_SETSCALEA                       2009
#define    HLM_HVAC_GETALLTSTATSBYTYPEQ             2010
#define    HLM_HVAC_GETALLTSTATSBYTYPEA             2011
#define    HLM_HVAC_GETALLTSTATSINFOQ               2012
#define    HLM_HVAC_GETALLTSTATSINFOA               2013
#define    HLM_HVAC_GETTSTATINFOBYIDQ               2014
#define    HLM_HVAC_GETTSTATINFOBYIDA               2015
#define    HLM_HVAC_SETTSTATINFOBYIDQ               2018
#define    HLM_HVAC_SETTSTATINFOBYIDA               2019
#define    HLM_HVAC_GETALLUNITSQ                    2020
#define    HLM_HVAC_GETALLUNITSA                    2021
#define    HLM_HVAC_GETALLUNITSINFOQ                2022
#define    HLM_HVAC_GETALLUNITSINFOA                2023
#define    HLM_HVAC_GETUNITINFOBYIDQ                2024
#define    HLM_HVAC_GETUNITINFOBYIDA                2025
#define    HLM_HVAC_SETUNITINFOBYIDQ                2026
#define    HLM_HVAC_SETUNITINFOBYIDA                2027
#define    HLM_HVAC_ADDCLIENTQ                      2028
#define    HLM_HVAC_ADDCLIENTA                      2029
#define    HLM_HVAC_REMOVECLIENTQ                   2030
#define    HLM_HVAC_REMOVECLIENTA                   2031

// Global hvac schedules
#define    HLM_HVAC_GETALLGLOBALSCHEDULESQ          2032
#define    HLM_HVAC_GETALLGLOBALSCHEDULESA          2033
#define    HLM_HVAC_ADDNEWGLOBALSCHEDULEQ           2034
#define    HLM_HVAC_ADDNEWGLOBALSCHEDULEA           2035
#define    HLM_HVAC_DELETEGLOBALSCHEDULEQ           2036
#define    HLM_HVAC_DELETEGLOBALSCHEDULEA           2037
#define    HLM_HVAC_GETGLOBALSCHEDULELAYOUTQ        2038
#define    HLM_HVAC_GETGLOBALSCHEDULELAYOUTA        2039
#define    HLM_HVAC_SETGLOBALSCHEDULELAYOUTQ        2040
#define    HLM_HVAC_SETGLOBALSCHEDULELAYOUTA        2041
#define    HLM_HVAC_GETGLOBALSCHEDULEPROGRAMQ       2042
#define    HLM_HVAC_GETGLOBALSCHEDULEPROGRAMA       2043
#define    HLM_HVAC_SETGLOBALSCHEDULEPROGRAMQ       2044
#define    HLM_HVAC_SETGLOBALSCHEDULEPROGRAMA       2045

#define    HLM_HVAC_GETALLHOLIDAYSQ                 2046
#define    HLM_HVAC_GETALLHOLIDAYSA                 2047
#define    HLM_HVAC_ADDNEWHOLIDAYQ                  2048
#define    HLM_HVAC_ADDNEWHOLIDAYA                  2049
#define    HLM_HVAC_DELHOLIDAYBYINDEXQ              2050
#define    HLM_HVAC_DELHOLIDAYBYINDEXA              2051
#define    HLM_HVAC_DELETEALLHOLIDAYSQ              2052
#define    HLM_HVAC_DELETEALLHOLIDAYSA              2053

#define    HLM_HVAC_NHVACQ                          2060
#define    HLM_HVAC_NHVACA                          2061
#define    HLM_HVAC_GETSTATEQ                       2062
#define    HLM_HVAC_GETSTATEA                       2063
#define    HLM_HVAC_SETCOOLQ                        2064
#define    HLM_HVAC_SETCOOLA                        2065
#define    HLM_HVAC_SETHEATQ                        2066
#define    HLM_HVAC_SETHEATA                        2067
#define    HLM_HVAC_SETTHOLDTIMEQ                   2068
#define    HLM_HVAC_SETTHOLDTIMEA                   2069
#define    HLM_HVAC_COMMANDQ                        2070
#define    HLM_HVAC_COMMANDA                        2071
#define    HLM_HVAC_UNITCOMMANDQ                    2072
#define    HLM_HVAC_UNITCOMMANDA                    2073
#define    HLM_HVAC_GETSCHEDLAYOUTBYIDQ             2074
#define    HLM_HVAC_GETSCHEDLAYOUTBYIDA             2075
#define    HLM_HVAC_SETSCHEDLAYOUTBYIDQ             2076
#define    HLM_HVAC_SETSCHEDLAYOUTBYIDA             2077
#define    HLM_HVAC_GETALLSCHEDULESFORTSTATQ        2078
#define    HLM_HVAC_GETALLSCHEDULESFORTSTATA        2079
#define    HLM_HVAC_SETALLSCHEDULESFORTSTATQ        2080
#define    HLM_HVAC_SETALLSCHEDULESFORTSTATA        2081
#define    HLM_HVAC_SETTSTATSCHEDIDQ                2082
#define    HLM_HVAC_SETTSTATSCHEDIDA                2083
#define    HLM_HVAC_GETHISTORYBYTSTATQ              2084
#define    HLM_HVAC_GETHISTORYBYTSTATA              2085
#define    HLM_HVAC_GETALLTSTATSTATUSQ              2088
#define    HLM_HVAC_GETALLTSTATSTATUSA              2089
#define    HLM_HVAC_COPYGLOBALTOTSTATLOCALQ         2090
#define    HLM_HVAC_COPYGLOBALTOTSTATLOCALA         2091
#define    HLM_HVAC_GETHVACIVARSQ                   2092
#define    HLM_HVAC_GETHVACIVARSA                   2093
#define    HLM_HVAC_SETHVACIVARSQ                   2094
#define    HLM_HVAC_SETHVACIVARSA                   2095
#define    HLM_HVAC_GETALLHSTATSTATUSQ              2096
#define    HLM_HVAC_GETALLHSTATSTATUSA              2097
#define    HLM_HVAC_GETTSTATSENSORINFOQ             2098
#define    HLM_HVAC_GETTSTATSENSORINFOA             2099
#define    HLM_HVAC_SETTSTATSENSORINFOQ             2100
#define    HLM_HVAC_SETTSTATSENSORINFOA             2101
#define    HLM_HVAC_GETALLSENSORSTATSQ              2102
#define    HLM_HVAC_GETALLSENSORSTATSA              2103
#define    HLM_HVAC_GETUSAGESTATSBYTSTATIDQ         2104
#define    HLM_HVAC_GETUSAGESTATSBYTSTATIDA         2105

#define	   HLM_HVAC_SETHUMIDQ						2106
#define	   HLM_HVAC_SETHUMIDA						2107
#define	   HLM_HVAC_SETDEHUMIDQ						2108
#define	   HLM_HVAC_SETDEHUMIDA						2109

// Other /Units
#define    HLM_HVAC_GETALLSYSCOMPUNITSQ             2110
#define    HLM_HVAC_GETALLSYSCOMPUNITSA             2111

// Global Settings
#define    HLM_HVAC_GETUSEROPTSQ                    2150
#define    HLM_HVAC_GETUSEROPTSA                    2151
#define    HLM_HVAC_SETUSEROPTSQ                    2152
#define    HLM_HVAC_SETUSEROPTSA                    2153
#define    HLM_HVAC_GETTEMPSENSORSQ                 2154
#define    HLM_HVAC_GETTEMPSENSORSA                 2155
#define    HLM_HVAC_GETHUMSENSORSQ                  2156
#define    HLM_HVAC_GETHUMSENSORSA                  2157
#define    HLM_HVAC_GETSTATICONSQ                   2158
#define    HLM_HVAC_GETSTATICONSA                   2159

#define    HLM_HVAC_END                     2159


/**/
/*                                                                    */
/* Lighting messages                                                  */
/*                                                                    */
/**/
#define    HLM_LIGHTING_START               3000
#define    HLM_LIGHTING_GENERAL_START       3000

// Unsolicited
#define    HLM_LIGHTING_KEYPADSCENECHANGE           3000
#define    HLM_LIGHTING_SNIFFERDATA                 3001
#define    HLM_LIGHTING_LOADSTATUSCHANGE            3002
#define    HLM_LIGHTING_USERSCENESTATECHANGED       3003
#define    HLM_LIGHTING_LOADCHANGE                  3004

// Keypads: get/add/delete
#define    HLM_LIGHTING_GETALLUIKEYPADSBYIDQ        3010 // Get all the keypad names
#define    HLM_LIGHTING_GETALLUIKEYPADSBYIDA        3011
#define    HLM_LIGHTING_GETALLKEYPADSQ              3012 // Get all the keypad names
#define    HLM_LIGHTING_GETALLKEYPADSA              3013
#define    HLM_LIGHTING_ADDNEWKEYPADQ               3014 // Add a new
#define    HLM_LIGHTING_ADDNEWKEYPADA               3015
#define    HLM_LIGHTING_GETKEYPADLAYOUTBYIDQ        3018
#define    HLM_LIGHTING_GETKEYPADLAYOUTBYIDA        3019
#define    HLM_LIGHTING_SETKEYPADLAYOUTBYIDQ        3020
#define    HLM_LIGHTING_SETKEYPADLAYOUTBYIDA        3021
#define    HLM_LIGHTING_DELETEKEYPADBYIDQ           3022 // Delete
#define    HLM_LIGHTING_DELETEKEYPADBYIDA           3023
#define    HLM_LIGHTING_SETKEYPADINFOBYIDQ          3024 // Edit basic keypad attribs
#define    HLM_LIGHTING_SETKEYPADINFOBYIDA          3025
#define    HLM_LIGHTING_SETKEYPADNAMEBYIDQ          3026 // Rename
#define    HLM_LIGHTING_SETKEYPADNAMEBYIDA          3027

// Scenes: execute/get info/add to-from keypad
#define    HLM_LIGHTING_GETALLSCENEINFOBYKEYPADIDQ  3030 // Get all scenes in a keypad
#define    HLM_LIGHTING_GETALLSCENEINFOBYKEYPADIDA  3031
#define    HLM_LIGHTING_ADDSCENETOKEYPADBYIDQ       3032 // Add a new scene to a keypad
#define    HLM_LIGHTING_ADDSCENETOKEYPADBYIDA       3033
#define    HLM_LIGHTING_DELETESCENEFROMKEYPADBYIDQ  3034 // Delete a scene from a keypad
#define    HLM_LIGHTING_DELETESCENEFROMKEYPADBYIDA  3035
#define    HLM_LIGHTING_DELETEALLSCENESKEYPADBYIDQ  3036 // Remove everything from keypad
#define    HLM_LIGHTING_DELETEALLSCENESKEYPADBYIDA  3037
#define    HLM_LIGHTING_EXECUTESCENEBYIDQ           3038 // Execute a pre-defined scene
#define    HLM_LIGHTING_EXECUTESCENEBYIDA           3039
#define    HLM_LIGHTING_EXECUTESCENEBYIDMDQ         3040 // Execute a pre-defined scene MOMENTARY DOWN
#define    HLM_LIGHTING_EXECUTESCENEBYIDMDA         3041
#define    HLM_LIGHTING_EXECUTESCENEBYIDMUQ         3042 // Execute a pre-defined scene MOMENTARY UP
#define    HLM_LIGHTING_EXECUTESCENEBYIDMUA         3043
#define    HLM_LIGHTING_EXECUTESCENEBYIDTAPQ        3046 // Execute a pre-defined scene MOMENTARY UP
#define    HLM_LIGHTING_EXECUTESCENEBYIDTAPA        3047

#define    HLM_LIGHTING_SETALLSCENECOMPSBYIDQ       3044 // Scene definition from crystalpad (teach scene from device)
#define    HLM_LIGHTING_SETALLSCENECOMPSBYIDA       3045
#define    HLM_LIGHTING_GETALLLINKCOMPSBYIDQ        3050 // New Links stuff
#define    HLM_LIGHTING_GETALLLINKCOMPSBYIDA        3051
#define    HLM_LIGHTING_SETALLLINKCOMPSBYIDQ        3052
#define    HLM_LIGHTING_SETALLLINKCOMPSBYIDA        3053
#define    HLM_LIGHTING_GETOBJECTLOADLEVELQ         3054
#define    HLM_LIGHTING_GETOBJECTLOADLEVELA         3055

// Scene Components: get/add/remove
#define    HLM_LIGHTING_ADDCOMPSCENEBYKEYPADIDQ     3072 // Add a new component to a scene
#define    HLM_LIGHTING_ADDCOMPSCENEBYKEYPADIDA     3073
#define    HLM_LIGHTING_DELCOMPSCENEBYKEYPADIDQ     3074 // Delete a component from a scene
#define    HLM_LIGHTING_DELCOMPSCENEBYKEYPADIDA     3075
#define    HLM_LIGHTING_CLEARSCENEBYKEYPADIDQ       3076 // Delete ALL components from a scene
#define    HLM_LIGHTING_CLEARSCENEBYKEYPADIDA       3077

// Component Sniffer
#define    HLM_LIGHTING_ADDSNIFFERQ                 3080 // Attach a sniffer to the server
#define    HLM_LIGHTING_ADDSNIFFERA                 3081
#define    HLM_LIGHTING_REMOVESNIFFERQ              3082 // Detach sniffer from server
#define    HLM_LIGHTING_REMOVESNIFFERA              3083

//
// Keypad/Scene execution
//
#define    HLM_LIGHTING_KEYPAD_ADDCLIENTBYIDQ       3090 // Add client to keypad to know when scene changes
#define    HLM_LIGHTING_KEYPAD_ADDCLIENTBYIDA       3091
#define    HLM_LIGHTING_KEYPAD_REMOVECLIENTBYIDQ    3092 // See above
#define    HLM_LIGHTING_KEYPAD_REMOVECLIENTBYIDA    3093
#define    HLM_LIGHTING_KEYPAD_GETSTATESBYIDQ       3094 // Get scene states now
#define    HLM_LIGHTING_KEYPAD_GETSTATESBYIDA       3095
#define    HLM_LIGHTING_KEYPAD_ADDCLIENTBYSCENEIDQ  3096
#define    HLM_LIGHTING_KEYPAD_ADDCLIENTBYSCENEIDA  3097

// Keypad template manipulation
#define    HLM_LIGHTING_GETTEMPLATESQ               3200 // Get all templates
#define    HLM_LIGHTING_GETTEMPLATESA               3201
#define    HLM_LIGHTING_GETTEMPLATEINFOBYIDQ        3202
#define    HLM_LIGHTING_GETTEMPLATEINFOBYIDA        3203
#define    HLM_LIGHTING_SETTEMPLATEINFOBYIDQ        3204
#define    HLM_LIGHTING_SETTEMPLATEINFOBYIDA        3205
#define    HLM_LIGHTING_ADDNEWTEMPLATEQ             3206
#define    HLM_LIGHTING_ADDNEWTEMPLATEA             3207
#define    HLM_LIGHTING_DELTEMPLATEBYIDQ            3208
#define    HLM_LIGHTING_DELTEMPLATEBYIDA            3209
#define    HLM_LIGHTING_GETSCHEDLAYOUTQ             3210
#define    HLM_LIGHTING_GETSCHEDLAYOUTA             3211
#define    HLM_LIGHTING_SETSCHEDLAYOUTQ             3212
#define    HLM_LIGHTING_SETSCHEDLAYOUTA             3213
#define    HLM_LIGHTING_GETALLPERIODSBYSCHEDIDXQ    3214
#define    HLM_LIGHTING_GETALLPERIODSBYSCHEDIDXA    3215
#define    HLM_LIGHTING_ADDNEWPERIODBYSCHEDIDXQ     3216
#define    HLM_LIGHTING_ADDNEWPERIODBYSCHEDIDXA     3217
#define    HLM_LIGHTING_DELPERIODBYSCHEDIDXQ        3218
#define    HLM_LIGHTING_DELPERIODBYSCHEDIDXA        3219
#define    HLM_LIGHTING_SETSCHEDTIMEBYIDXQ          3220
#define    HLM_LIGHTING_SETSCHEDTIMEBYIDXA          3221
#define    HLM_LIGHTING_GETALLSCHEDCOMPSBYINDEXQ    3222 // Get all the stuff that happens for a lighting period
#define    HLM_LIGHTING_GETALLSCHEDCOMPSBYINDEXA    3223
#define    HLM_LIGHTING_DELSCHEDCOMPBYINDEXQ        3224 // Delete a component from a schedule period
#define    HLM_LIGHTING_DELSCHEDCOMPBYINDEXA        3225
#define    HLM_LIGHTING_DELALLSCHEDCOMPSBYINDEXQ    3226 // Delete all components from a schedule period
#define    HLM_LIGHTING_DELALLSCHEDCOMPSBYINDEXA    3227
#define    HLM_LIGHTING_GETALLKEYPADSBYIDQ          3228 // List all keypads with ID
#define    HLM_LIGHTING_GETALLKEYPADSBYIDA          3229
#define    HLM_LIGHTING_GETALLNTSCENESBYKEYPADIDQ   3230 // Get all scenes for a keypad, NON-toggles only
#define    HLM_LIGHTING_GETALLNTSCENESBYKEYPADIDA   3231
#define    HLM_LIGHTING_GETALLLIGHTDEVSBYIDQ        3232 // Get all lighting devices with ID
#define    HLM_LIGHTING_GETALLLIGHTDEVSBYIDA        3233
#define    HLM_LIGHTING_GETALLDEVACTIONSBYIDQ       3234 // Get all actions for device
#define    HLM_LIGHTING_GETALLDEVACTIONSBYIDA       3235
#define    HLM_LIGHTING_ADDSCHEDSCENECMDBYINDEXQ    3236 // Add a keypad button to a schedule period
#define    HLM_LIGHTING_ADDSCHEDSCENECMDBYINDEXA    3237
#define    HLM_LIGHTING_ADDDEVICECMDBYINDEXQ        3238 // Add a device command to schedule period
#define    HLM_LIGHTING_ADDDEVICECMDBYINDEXA        3239
#define    HLM_LIGHTING_EXECUTESCHEDPERIODBYINDEXQ  3240
#define    HLM_LIGHTING_EXECUTESCHEDPERIODBYINDEXA  3241
#define    HLM_LIGHTING_SETNEWKPTEMPLATEBYIDQ       3246 // Remap esisting keypad to a new template
#define    HLM_LIGHTING_SETNEWKPTEMPLATEBYIDA       3247 // Remap esisting keypad to a new template
#define    HLM_LIGHTING_MOVEPERIODINDEXSCHEDQ       3248
#define    HLM_LIGHTING_MOVEPERIODINDEXSCHEDA       3249
#define    HLM_LIGHTING_GETACTIVESCHEDULEDATAQ      3250
#define    HLM_LIGHTING_GETACTIVESCHEDULEDATAA      3251

#define    HLM_LIGHTING_GENERAL_END         3251
#define    HLM_LIGHTING_DEVICECTLR_START    3320

// Device Controllers
#define    HLM_LIGHTING_GETALLDEVCTLRSQ             3320 // Get all device controller syscomps
#define    HLM_LIGHTING_GETALLDEVCTLRSA             3321
#define    HLM_LIGHTING_GETALLSUBDEVICETYPESBYIDQ   3322 // Get all supported sub-device types
#define    HLM_LIGHTING_GETALLSUBDEVICETYPESBYIDA   3323
#define    HLM_LIGHTING_GETALLSUBDEVICESBYIDQ       3324 // Get all sub-devices attached to this device
#define    HLM_LIGHTING_GETALLSUBDEVICESBYIDA       3325
#define    HLM_LIGHTING_ADDSUBDEVICEBYIDQ           3326 // Add new sub-device to device
#define    HLM_LIGHTING_ADDSUBDEVICEBYIDA           3327
#define    HLM_LIGHTING_REMOVESUBDEVICEBYIDQ        3328 // Remove sub-device from device
#define    HLM_LIGHTING_REMOVESUBDEVICEBYIDA        3329
#define    HLM_LIGHTING_SETSUBDEVCONFIGBYIDQ        3332 // Set sub-device specific options
#define    HLM_LIGHTING_SETSUBDEVCONFIGBYIDA        3333
#define    HLM_LIGHTING_SETSUBDEVOPTSBYIDQ          3334 // Set sub-device specific options
#define    HLM_LIGHTING_SETSUBDEVOPTSBYIDA          3335
#define    HLM_LIGHTING_TESTDEVICEBYIDQ             3336 // Test device, as with configurator
#define    HLM_LIGHTING_TESTDEVICEBYIDA             3337
#define    HLM_LIGHTING_GETDEVGROUPBYIDQ            3338 // Get devices in group based on input device
#define    HLM_LIGHTING_GETDEVGROUPBYIDA            3339
#define    HLM_LIGHTING_GETNSUBDEVICESBYIDQ         3340 // Get the count of subdevices by ctlr id
#define    HLM_LIGHTING_GETNSUBDEVICESBYIDA         3341 // Get the count of subdevices by ctlr id
#define    HLM_LIGHTING_GETSUBDEVICESBYIDQ          3342 // Get the count of subdevices by ctlr id
#define    HLM_LIGHTING_GETSUBDEVICESBYIDA          3343 // Get the count of subdevices by ctlr id

#define    HLM_LIGHTING_ADDLOADCLIENTQ              3344
#define    HLM_LIGHTING_ADDLOADCLIENTA              3345
#define    HLM_LIGHTING_REMOVELOADCLIENTQ           3346
#define    HLM_LIGHTING_REMOVELOADCLIENTA           3347
#define    HLM_LIGHTING_GETLOADSTATUSQ              3348
#define    HLM_LIGHTING_GETLOADSTATUSA              3349
#define    HLM_LIGHTING_EXECUTELOADCOMMANDQ         3350
#define    HLM_LIGHTING_EXECUTELOADCOMMANDA         3351
#define    HLM_LIGHTING_RESETCONFIGQ                3352
#define    HLM_LIGHTING_RESETCONFIGA                3353
#define    HLM_LIGHTING_AUTOADDSUBDEVICEQ           3354
#define    HLM_LIGHTING_AUTOADDSUBDEVICEA           3355
#define    HLM_LIGHTING_GETGLOBALCONTROLSETQ        3356
#define    HLM_LIGHTING_GETGLOBALCONTROLSETA        3357
#define    HLM_LIGHTING_GETNEWADDRESSRANGEQ         3358
#define    HLM_LIGHTING_GETNEWADDRESSRANGEA         3359
#define	   HLM_LIGHTING_SETDEVICE_KNX				3360

#define    HLM_LIGHTING_DEVICECTLR_END      3360
#define    HLM_LIGHTING_USERSCENESTART      3400

#define    HLM_LIGHTING_GETALLUSERSCENESQ           3400
#define    HLM_LIGHTING_GETALLUSERSCENESA           3401
#define    HLM_LIGHTING_ADDNEWUSERSCENEQ            3402
#define    HLM_LIGHTING_ADDNEWUSERSCENEA            3403
#define    HLM_LIGHTING_DELUSERSCENEQ               3404
#define    HLM_LIGHTING_DELUSERSCENEA               3405
#define    HLM_LIGHTING_CONFIGUSERSCENEQ            3406
#define    HLM_LIGHTING_CONFIGUSERSCENEA            3407
#define    HLM_LIGHTING_GETUSERSCENESTATEQ          3408
#define    HLM_LIGHTING_GETUSERSCENESTATEA          3409
#define    HLM_LIGHTING_EXECUTEUSERSCENEQ           3410
#define    HLM_LIGHTING_EXECUTEUSERSCENEA           3411
#define    HLM_LIGHTING_GETUSERSCENECOMPSQ          3412
#define    HLM_LIGHTING_GETUSERSCENECOMPSA          3413
#define    HLM_LIGHTING_ADDUSERSCENECOMPQ           3414
#define    HLM_LIGHTING_ADDUSERSCENECOMPA           3415
#define    HLM_LIGHTING_DELUSERSCENECOMPQ           3416
#define    HLM_LIGHTING_DELUSERSCENECOMPA           3417
#define    HLM_LIGHTING_SETUSERSCENECOMPQ           3418
#define    HLM_LIGHTING_SETUSERSCENECOMPA           3419

#define    HLM_LIGHTING_USERSCENEEND        3419

#define    HLM_LIGHTING_END      3419


/**/
/*                                                                    */
/* Audio messages                                                     */
/*                                                                    */
/**/
#define    HLM_AUDIO_START                  4000

// Unsolicited messages
#define    HLM_AUDIO_ACTIVESTATECHANGED             4000
#define    HLM_AUDIO_DATABASECHANGED                4001
#define    HLM_AUDIO_STATECHANGED                   4002
#define    HLM_AUDIO_ITEMSCHANGED                   4003
#define    HLM_AUDIO_ZONESOURCECHANGED              4004
#define    HLM_AUDIO_ZONEVOLCHANGED                 4005
#define    HLM_AUDIO_ZONEARTCHANGED                 4006
#define    HLM_AUDIO_ZONETEXTCHANGED                4007
#define    HLM_AUDIO_ZONEICONSCHANGED               4008
#define    HLM_AUDIO_NEWTUNERFAVS                   4009
#define    HLM_AUDIO_SEARCHUPDATE                   4010
#define    HLM_AUDIO_DVDLISTINVALIDATE              4015
#define    HLM_AUDIO_ZONESOUNDATTRIBS               4016
#define    HLM_AUDIO_PLAYLISTITEMSCHANGED           4017

// Q+A messages
#define    HLM_AUDIO_ADDLIBRARYCLIENTQ              4010
#define    HLM_AUDIO_ADDLIBRARYCLIENTA              4011
#define    HLM_AUDIO_REMOVELIBRARYCLIENTQ           4012
#define    HLM_AUDIO_REMOVELIBRARYCLIENTA           4013

// Player config
#define    HLM_AUDIO_GETPLAYERFIXEDVOLQ             4020
#define    HLM_AUDIO_GETPLAYERFIXEDVOLA             4021
#define    HLM_AUDIO_SETPLAYERFIXEDVOLQ             4022
#define    HLM_AUDIO_SETPLAYERFIXEDVOLA             4023
#define    HLM_AUDIO_GETPLAYERDEVICECONFIGQ         4024
#define    HLM_AUDIO_GETPLAYERDEVICECONFIGA         4025
#define    HLM_AUDIO_SETPLAYERDEVICECONFIGQ         4026
#define    HLM_AUDIO_SETPLAYERDEVICECONFIGA         4027

#define    HLM_AUDIO_ARTISTQ                        4030
#define    HLM_AUDIO_ARTISTA                        4031
#define    HLM_AUDIO_ALBUMBYARTISTALBUMQ            4032
#define    HLM_AUDIO_ALBUMBYARTISTALBUMA            4033

// More Player config
#define    HLM_AUDIO_GETPLAYERSERVICESQ             4040
#define    HLM_AUDIO_GETPLAYERSERVICESA             4041
#define    HLM_AUDIO_SETPLAYERSERVICECONFIGQ        4042
#define    HLM_AUDIO_SETPLAYERSERVICECONFIGA        4043
#define    HLM_AUDIO_ADDSERVICEQ                    4044
#define    HLM_AUDIO_ADDSERVICEA                    4045
#define    HLM_AUDIO_DELSERVICEQ                    4046
#define    HLM_AUDIO_DELSERVICEA                    4047
#define    HLM_AUDIO_SERVICE_ADDCLIENTQ             4048
#define    HLM_AUDIO_SERVICE_ADDCLIENTA             4049
#define    HLM_AUDIO_SERVICE_REMOVECLIENTQ          4050
#define    HLM_AUDIO_SERVICE_REMOVECLIENTA          4051
#define    HLM_AUDIO_SERVICE_DOSERVICEOBJECTQ       4052
#define    HLM_AUDIO_SERVICE_DOSERVICEOBJECTA       4053
#define    HLM_AUDIO_GETALLSERVICESQ                4054
#define    HLM_AUDIO_GETALLSERVICESA                4055

// Sorted list messages
#define    HLM_AUDIO_GETPLAYLISTITEMQ               4062
#define    HLM_AUDIO_GETPLAYLISTITEMA               4063
#define    HLM_AUDIO_NSORTITEMSQ                    4064
#define    HLM_AUDIO_NSORTITEMSA                    4065
#define    HLM_AUDIO_GETSORTITEMQ                   4066
#define    HLM_AUDIO_GETSORTITEMA                   4067
#define    HLM_AUDIO_GETQUICKSORTITEMA              4068
#define    HLM_AUDIO_NSORTITEMSA_CHUNK              4069

// Zone messages.
#define    HLM_AUDIO_GETALLZONESQ                   4070
#define    HLM_AUDIO_GETALLZONESA                   4071
#define    HLM_AUDIO_GETALLZCQ                      4072
#define    HLM_AUDIO_GETALLZCA                      4073
#define    HLM_AUDIO_SETZONECONFIGBYIDQ             4074
#define    HLM_AUDIO_SETZONECONFIGBYIDA             4075
#define    HLM_AUDIO_SETSOURCECONFIGBYIDQ           4076
#define    HLM_AUDIO_SETSOURCECONFIGBYIDA           4077
#define    HLM_AUDIO_GETZONESOURCEIDQ               4078
#define    HLM_AUDIO_GETZONESOURCEIDA               4079
#define    HLM_AUDIO_SETZONESOURCEIDQ               4080
#define    HLM_AUDIO_SETZONESOURCEIDA               4081
#define    HLM_AUDIO_GETZONEVOLUMEQ                 4082
#define    HLM_AUDIO_GETZONEVOLUMEA                 4083
#define    HLM_AUDIO_SETZONEVOLUMEQ                 4084
#define    HLM_AUDIO_SETZONEVOLUMEA                 4085
#define    HLM_AUDIO_ADDZONECLIENTQ                 4086
#define    HLM_AUDIO_ADDZONECLIENTA                 4087
#define    HLM_AUDIO_REMOVEZONECLIENTQ              4088
#define    HLM_AUDIO_REMOVEZONECLIENTA              4089
#define    HLM_AUDIO_GETZONEARTQ                    4099
#define    HLM_AUDIO_GETZONEARTA                    4100
#define    HLM_AUDIO_SOURCECOMPSCHANGEDQ            4105
#define    HLM_AUDIO_SOURCECOMPSCHANGEDA            4106
#define    HLM_AUDIO_GETZONEICONSQ                  4107
#define    HLM_AUDIO_GETZONEICONSA                  4108
#define    HLM_AUDIO_GETZONECONFIGBYIDQ             4109
#define    HLM_AUDIO_GETZONECONFIGBYIDA             4110
#define    HLM_AUDIO_ADDSOURCEQ                     4111
#define    HLM_AUDIO_ADDSOURCEA                     4112
#define    HLM_AUDIO_DELSOURCEBYIDQ                 4113
#define    HLM_AUDIO_DELSOURCEBYIDA                 4114
#define    HLM_AUDIO_SETZCOPTSQ                     4115
#define    HLM_AUDIO_SETZCOPTSA                     4116
#define    HLM_AUDIO_GETDVDARTQ                     4117
#define    HLM_AUDIO_GETDVDARTA                     4118
#define    HLM_AUDIO_GETZONESOUNDATTRIBSQ           4119
#define    HLM_AUDIO_GETZONESOUNDATTRIBSA           4120
#define    HLM_AUDIO_SETZONESOUNDATTRIBSQ           4121
#define    HLM_AUDIO_SETZONESOUNDATTRIBSA           4122
#define    HLM_AUDIO_GETZONEMODESQ                  4123
#define    HLM_AUDIO_GETZONEMODESA                  4124
#define    HLM_AUDIO_GETZONESOURCECONFIGQ           4125
#define    HLM_AUDIO_GETZONESOURCECONFIGA           4126
#define    HLM_AUDIO_SETZONESOURCECONFIGQ           4127
#define    HLM_AUDIO_SETZONESOURCECONFIGA           4128
#define    HLM_AUDIO_GETZONESOURCEINDEXQ            4129
#define    HLM_AUDIO_GETZONESOURCEINDEXA            4130
#define    HLM_AUDIO_SETZONESOURCEINDEXQ            4131
#define    HLM_AUDIO_SETZONESOURCEINDEXA            4132
#define    HLM_AUDIO_GETMODECONFIGQ                 4133
#define    HLM_AUDIO_GETMODECONFIGA                 4134

#define	   HLM_AUDIO_SETSOURCEAUDIOIRBYIDQ			4135
#define	   HLM_AUDIO_SETSOURCEAUDIOIRBYIDA			4136

#define	   HLM_AUDIO_GET_CUR_SRC_SETTINGSQ			4137
#define	   HLM_AUDIO_GET_CUR_SRC_SETTINGSA			4138

#define	   HLM_AUDIO_GET_CUR_ZN_SETTINGSQ			4139
#define	   HLM_AUDIO_GET_CUR_ZN_SETTINGSA			4140

#define	   HLM_AUDIO_GET_CUR_LED_SETTINGSQ			4141
#define	   HLM_AUDIO_GET_CUR_LED_SETTINGSA			4142

#define	   HLM_AUDIO_SETZONEBYIDQ					4143
#define	   HLM_AUDIO_SETZONEBYIDA					4144

#define    HLM_AUDIO_ALLZONESOFFQ                   4145
#define    HLM_AUDIO_ALLZONESOFFA                   4146

#define	   HLM_AUDIO_SETLOCALSOURCE_IRQ				4147
#define	   HLM_AUDIO_SETLOCALSOURCE_IRA				4148

#define	   HLM_AUDIO_GET_CUR_LOCAL_AUDIO_IN_VALUESQ 4149
#define	   HLM_AUDIO_GET_CUR_LOCAL_AUDIO_IN_VALUESA 4150

#define	   HLM_AUDIO_SET_LOCAL_AUDIO_INPUTQ			4151
#define	   HLM_AUDIO_SET_LOCAL_AUDIO_INPUTA			4152

#define	   HLM_AUDIO_TRANSFERQ						4153
#define	   HLM_AUDIO_TRANSFERA						4154

#define	   HLM_AUDIO_SETLEDQ						4155
#define	   HLM_AUDIO_SETLEDA						4156


// Share messages
#define    HLM_AUDIO_GETALLSHARESQ                  4200
#define    HLM_AUDIO_GETALLSHARESA                  4201
#define    HLM_AUDIO_REFRESHAUDIONOWQ               4202
#define    HLM_AUDIO_REFRESHAUDIONOWA               4203
#define    HLM_AUDIO_ADDSHAREQ                      4204
#define    HLM_AUDIO_ADDSHAREA                      4205
#define    HLM_AUDIO_REMOVESHAREBYINDEXQ            4206
#define    HLM_AUDIO_REMOVESHAREBYINDEXA            4207
#define    HLM_AUDIO_GETSCANSTATUSQ                 4208
#define    HLM_AUDIO_GETSCANSTATUSA                 4209
#define    HLM_AUDIO_GETSHAREOPTSQ                  4210
#define    HLM_AUDIO_GETSHAREOPTSA                  4211
#define    HLM_AUDIO_SETSHAREOPTSQ                  4212
#define    HLM_AUDIO_SETSHAREOPTSA                  4213

// Playlist messages.
#define    HLM_AUDIO_GETALLITEMSQ                   4314
#define    HLM_AUDIO_GETALLITEMSA                   4315
#define    HLM_AUDIO_ADDITEMQ                       4316
#define    HLM_AUDIO_ADDITEMA                       4317
#define    HLM_AUDIO_ADDSORTITEMQ                   4318
#define    HLM_AUDIO_ADDSORTITEMA                   4319
#define    HLM_AUDIO_ADDALLSORTITEMSQ               4320
#define    HLM_AUDIO_ADDALLSORTITEMSA               4321
#define    HLM_AUDIO_REMOVEITEMQ                    4322
#define    HLM_AUDIO_REMOVEITEMA                    4323
#define    HLM_AUDIO_REMOVEALLQ                     4324
#define    HLM_AUDIO_REMOVEALLA                     4325
#define    HLM_AUDIO_GETSTATEQ                      4326
#define    HLM_AUDIO_GETSTATEA                      4327
#define    HLM_AUDIO_PLAYTRACKQ                     4330
#define    HLM_AUDIO_PLAYTRACKA                     4331
#define    HLM_AUDIO_CREATENEWPLAYLISTQ             4334
#define    HLM_AUDIO_CREATENEWPLAYLISTA             4335
#define    HLM_AUDIO_OVERWRITEPLAYLISTQ             4336
#define    HLM_AUDIO_OVERWRITEPLAYLISTA             4337
#define    HLM_AUDIO_DELETEPLAYLISTQ                4338
#define    HLM_AUDIO_DELETEPLAYLISTA                4339
#define    HLM_AUDIO_RENAMEPLAYLISTQ                4340
#define    HLM_AUDIO_RENAMEPLAYLISTA                4341
#define    HLM_AUDIO_XFERPLAYLISTQ                  4342
#define    HLM_AUDIO_XFERPLAYLISTA                  4343
#define    HLM_AUDIO_SETNEXTARTQ                    4344
#define    HLM_AUDIO_SETNEXTARTA                    4345
#define    HLM_AUDIO_SETPREVARTQ                    4346
#define    HLM_AUDIO_SETPREVARTA                    4347
#define    HLM_AUDIO_DELCURARTQ                     4348
#define    HLM_AUDIO_DELCURARTA                     4349
#define    HLM_AUDIO_GETALLGENRESQ                  4350
#define    HLM_AUDIO_GETALLGENRESA                  4351
#define    HLM_AUDIO_SETGENRESQ                     4352
#define    HLM_AUDIO_SETGENRESA                     4353
#define    HLM_AUDIO_SETGENREAUTOPILOTQ             4354
#define    HLM_AUDIO_SETGENREAUTOPILOTA             4355
#define    HLM_AUDIO_ADDSORTITEMTOPLAYLISTQ         4356
#define    HLM_AUDIO_ADDSORTITEMTOPLAYLISTA         4357
#define    HLM_AUDIO_PLAYLISTITEMOPERATIONQ         4358
#define    HLM_AUDIO_PLAYLISTITEMOPERATIONA         4359
#define    HLM_AUDIO_PLAYQUEUETOPLAYLISTQ           4360
#define    HLM_AUDIO_PLAYQUEUETOPLAYLISTA           4361

// Zone Commands
#define    HLM_AUDIO_ZONECOMMANDQ                   4445
#define    HLM_AUDIO_ZONECOMMANDA                   4446
#define    HLM_AUDIO_GETZONEKEYPADQ                 4447
#define    HLM_AUDIO_GETZONEKEYPADA                 4448

// General Config
#define    HLM_AUDIO_GETUSEROPTSQ                   4460
#define    HLM_AUDIO_GETUSEROPTSA                   4461
#define    HLM_AUDIO_SETUSEROPTSQ                   4462
#define    HLM_AUDIO_SETUSEROPTSA                   4463

// Player commands.
#define    HLM_PLAYER_GETAREAIDQ                    4565
#define    HLM_PLAYER_GETAREAIDA                    4566
#define    HLM_PLAYER_SETAREAIDQ                    4567
#define    HLM_PLAYER_SETAREAIDA                    4568

// Tuner Station commands
#define    HLM_AUDIO_GETALLTSTATIONSQ               4580
#define    HLM_AUDIO_GETALLTSTATIONSA               4581
#define    HLM_AUDIO_ADDNEWTSTATIONBYZONEIDQ        4582
#define    HLM_AUDIO_ADDNEWTSTATIONBYZONEIDA        4583
#define    HLM_AUDIO_RENAMEFAVSTATIONBYIDQ          4584
#define    HLM_AUDIO_RENAMEFAVSTATIONBYIDA          4585
#define    HLM_AUDIO_DELTSTATIONBYIDQ               4586
#define    HLM_AUDIO_DELTSTATIONBYIDA               4587
#define    HLM_AUDIO_TUNEZONETOFAVBYIDQ             4588
#define    HLM_AUDIO_TUNEZONETOFAVBYIDA             4589
#define    HLM_AUDIO_SAVEZONEPRESETQ                4590
#define    HLM_AUDIO_SAVEZONEPRESETA                4591

// XM Tuner Station Commands
#define    HLM_AUDIO_GETALLXMCATSQ                  4600
#define    HLM_AUDIO_GETALLXMCATSA                  4601
#define    HLM_AUDIO_GETXMSTATIONSBYCATIDQ          4602
#define    HLM_AUDIO_GETXMSTATIONSBYCATIDA          4603
#define    HLM_AUDIO_TUNEZONETOXMBYIDQ              4604
#define    HLM_AUDIO_TUNEZONETOXMBYIDA              4605
#define    HLM_AUDIO_TUNEZONETOXMFAVBYIDQ           4606
#define    HLM_AUDIO_TUNEZONETOXMFAVBYIDA           4607

// Audio Keypad commands
#define    HLM_AUDIO_GETCK12KEYPADTEXTQ             4610
#define    HLM_AUDIO_GETCK12KEYPADTEXTA             4611

// Custom interfaces
#define    HLM_AUDIO_GETALLINTERACESQ               4612
#define    HLM_AUDIO_GETALLINTERACESA               4613
#define    HLM_AUDIO_ADDNEWINTERFACEQ               4614
#define    HLM_AUDIO_ADDNEWINTERFACEA               4615
#define    HLM_AUDIO_DELINTERFACEQ                  4616
#define    HLM_AUDIO_DELINTERFACEA                  4617
#define    HLM_AUDIO_GETINTERFACELAYOUTBYIDQ        4618
#define    HLM_AUDIO_GETINTERFACELAYOUTBYIDA        4619
#define    HLM_AUDIO_SETINTERFACELAYOUTBYIDQ        4620
#define    HLM_AUDIO_SETINTERFACELAYOUTBYIDA        4621
#define    HLM_AUDIO_SETINTERFACELAYOUTCFGBYIDQ     4622
#define    HLM_AUDIO_SETINTERFACELAYOUTCFGBYIDA     4623

// Custom UI
#define    HLM_AUDIO_GETAUDIOTOGGLEBUTTONQ          4624
#define    HLM_AUDIO_GETAUDIOTOGGLEBUTTONA          4625
#define    HLM_AUDIO_EXECUTEAUDIOBUTTONQ            4626
#define    HLM_AUDIO_EXECUTEAUDIOBUTTONA            4627

// Misc
#define    HLM_AUDIO_GETTVTUNERINFOQ                4628
#define    HLM_AUDIO_GETTVTUNERINFOA                4629
#define    HLM_AUDIO_SETTVTUNERINFOQ                4630
#define    HLM_AUDIO_SETTVTUNERINFOA                4631
#define    HLM_AUDIO_EXECDEVICEKEYQ                 4632
#define    HLM_AUDIO_EXECDEVICEKEYA                 4633
#define    HLM_AUDIO_GETKEYPADLISTBOXINFOQ          4634
#define    HLM_AUDIO_GETKEYPADLISTBOXINFOA          4635
#define    HLM_AUDIO_SCROLLKEYPADLISTBOXQ           4636
#define    HLM_AUDIO_SCROLLKEYPADLISTBOXA           4637
#define    HLM_AUDIO_GOTOKEYPADLISTITEMQ            4638
#define    HLM_AUDIO_GOTOKEYPADLISTITEMA            4639
#define    HLM_AUDIO_EXECUNIVERSALQ                 4640
#define    HLM_AUDIO_EXECUNIVERSALA                 4641
#define    HLM_AUDIO_EXECHATQ                       4642
#define    HLM_AUDIO_EXECHATA                       4643

// Templates
#define    HLM_AUDIO_MODIFYIFACEPAGEQ               4646
#define    HLM_AUDIO_MODIFYIFACEPAGEA               4647
#define    HLM_AUDIO_MAPINTERFACEBYIDQ              4648
#define    HLM_AUDIO_MAPINTERFACEBYIDA              4649
#define    HLM_AUDIO_GETALLTEMPLATESQ               4650
#define    HLM_AUDIO_GETALLTEMPLATESA               4651
#define    HLM_AUDIO_ADDNEWTEMPLATEQ                4652
#define    HLM_AUDIO_ADDNEWTEMPLATEA                4653
#define    HLM_AUDIO_DELTEMPLATEQ                   4654
#define    HLM_AUDIO_DELTEMPLATEA                   4655
#define    HLM_AUDIO_GETTEMPLATELAYOUTBYIDQ         4656
#define    HLM_AUDIO_GETTEMPLATELAYOUTBYIDA         4657
#define    HLM_AUDIO_SETTEMPLATELAYOUTBYIDQ         4658
#define    HLM_AUDIO_SETTEMPLATELAYOUTBYIDA         4659
#define    HLM_AUDIO_SETTEMPLATELAYOUTCFGBYIDQ      4660
#define    HLM_AUDIO_SETTEMPLATELAYOUTCFGBYIDA      4661
#define    HLM_AUDIO_SAVEIFACEASTEMPLATEQ           4662
#define    HLM_AUDIO_SAVEIFACEASTEMPLATEA           4663
#define    HLM_AUDIO_ADDCHANPAGESQ                  4664
#define    HLM_AUDIO_ADDCHANPAGESA                  4665

#define    HLM_AUDIO_GETALLIFACEDEVICESQ            4670
#define    HLM_AUDIO_GETALLIFACEDEVICESA            4671

// Displays
#define    HLM_AUDIO_GETALLDISPLAYSQ                4672
#define    HLM_AUDIO_GETALLDISPLAYSA                4673
#define    HLM_AUDIO_SETDISPLAYCONFIGQ              4674
#define    HLM_AUDIO_SETDISPLAYCONFIGA              4675
#define    HLM_AUDIO_ADDDISPLAYSOURCEQ              4676
#define    HLM_AUDIO_ADDDISPLAYSOURCEA              4677
#define    HLM_AUDIO_DELDISPLAYSOURCEQ              4678
#define    HLM_AUDIO_DELDISPLAYSOURCEA              4679

#define    HLM_AUDIO_GETPREVIEWCONFIGQ              4680
#define    HLM_AUDIO_GETPREVIEWCONFIGA              4681
#define    HLM_AUDIO_SETPREVIEWCONFIGQ              4682
#define    HLM_AUDIO_SETPREVIEWCONFIGA              4683

#define    HLM_AUDIO_GETDISPLAYSOURCEINDEXQ         4184
#define    HLM_AUDIO_GETDISPLAYSOURCEINDEXA         4185
#define    HLM_AUDIO_SETDISPLAYSOURCEINDEXQ         4186
#define    HLM_AUDIO_SETDISPLAYSOURCEINDEXA         4187

#define    HLM_AUDIO_DVDGETLISTQ                    4690
#define    HLM_AUDIO_DVDGETLISTA                    4691
#define    HLM_AUDIO_DVDGETSTATUSQ                  4692
#define    HLM_AUDIO_DVDGETSTATUSA                  4693
#define    HLM_AUDIO_DVDCOMMANDQ                    4694
#define    HLM_AUDIO_DVDCOMMANDA                    4695
#define    HLM_AUDIO_DVDADDCLIENTQ                  4696
#define    HLM_AUDIO_DVDADDCLIENTA                  4697
#define    HLM_AUDIO_DVDREMOVECLIENTQ               4698
#define    HLM_AUDIO_DVDREMOVECLIENTA               4699
#define    HLM_AUDIO_DVDGETGENRESQ                  4700
#define    HLM_AUDIO_DVDGETGENRESA                  4701
#define    HLM_AUDIO_DVDGETCOMMANDSETQ              4702
#define    HLM_AUDIO_DVDGETCOMMANDSETA              4703

#define    HLM_AUDIO_GETMP3MODESQ                   4710
#define    HLM_AUDIO_GETMP3MODESA                   4711
#define    HLM_AUDIO_GETMP3MODEQ                    4712
#define    HLM_AUDIO_GETMP3MODEA                    4713
#define    HLM_AUDIO_SETMP3MODEQ                    4714
#define    HLM_AUDIO_SETMP3MODEA                    4715

#define    HLM_AUDIO_ADDKEYPADSINKQ                 4720
#define    HLM_AUDIO_ADDKEYPADSINKA                 4721
#define    HLM_AUDIO_REMOVEKEYPADSINKQ              4722
#define    HLM_AUDIO_REMOVEKEYPADSINKA              4723
#define    HLM_AUDIO_GETKEYPADSTATUSQ               4724
#define    HLM_AUDIO_GETKEYPADSTATUSA               4725

#define    HLM_AUDIO_CUSTOMZONECOMMANDQ             4726
#define    HLM_AUDIO_CUSTOMZONECOMMANDA             4727
#define    HLM_AUDIO_CUSTOMKEYPADCOMMANDQ           4728
#define    HLM_AUDIO_CUSTOMKEYPADCOMMANDA           4729
#define    HLM_AUDIO_GETCUSTOMLISTCONTENTSQ         4730
#define    HLM_AUDIO_GETCUSTOMLISTCONTENTSA         4731
#define    HLM_AUDIO_CUSTOMKEYPADCOMMAND_STRINGQ    4732
#define    HLM_AUDIO_CUSTOMKEYPADCOMMAND_STRINGA    4733

// Displays Continued
#define    HLM_AUDIO_MAPDISPLAYBYIDQ                4750
#define    HLM_AUDIO_MAPDISPLAYBYIDA                4751

// Zone Headers More custom UI
#define    HLM_AUDIO_GETALLZONEHEADERSQ             4760
#define    HLM_AUDIO_GETALLZONEHEADERSA             4761
#define    HLM_AUDIO_ADDNEWZONEHEADERQ              4762
#define    HLM_AUDIO_ADDNEWZONEHEADERA              4763
#define    HLM_AUDIO_DELZONEHEADERQ                 4764
#define    HLM_AUDIO_DELZONEHEADERA                 4765
#define    HLM_AUDIO_GETZONEHEADERLAYOUTBYIDQ       4766
#define    HLM_AUDIO_GETZONEHEADERLAYOUTBYIDA       4767
#define    HLM_AUDIO_SETZONEHEADERLAYOUTBYIDQ       4768
#define    HLM_AUDIO_SETZONEHEADERLAYOUTBYIDA       4769
#define    HLM_AUDIO_SETZONEHEADERLAYOUTCFGBYIDQ    4770
#define    HLM_AUDIO_SETZONEHEADERLAYOUTCFGBYIDA    4771
#define    HLM_AUDIO_GETIRBINDINGSQ                 4772
#define    HLM_AUDIO_GETIRBINDINGSA                 4773
#define    HLM_AUDIO_SETIRBINDINGSQ                 4774
#define    HLM_AUDIO_SETIRBINDINGSA                 4775
#define    HLM_AUDIO_GETDEFAULTBINDINGFORDEVICEQ    4776
#define    HLM_AUDIO_GETDEFAULTBINDINGFORDEVICEA    4777

#define    HLM_AUDIO_END                    4777


/**/
/*                                                                    */
/* Telephone messages                                                 */
/*                                                                    */
/**/
#define    HLM_PHONE_START                  5000

// Unsolicited messages.

// !***! these really only relate to vmsgs - put in that group...
// !***! renumber next time in here...
#define    HLM_PHONE_DATACHANGED                    5000
#define    HLM_PHONE_STATUSCHANGED                  5001

#define    HLM_PHONE_RING_CID                       5010
#define    HLM_PHONE_CALLEND                        5011
#define    HLM_VOICEMSG_RECSTART                    5012
#define    HLM_VOICEMSG_PACKET                      5013
#define    HLM_VOICEMSG_END                         5014
#define    HLM_GREETING_MSGCHANGED                  5015
#define    HLM_GREETING_RINGCHANGED                 5016
#define    HLM_GREETING_SCREENCALLCHANGED           5017
#define    HLM_PHONE_OUTGOING_CONNECT               5018
#define    HLM_PHONE_OUTGOING_FAILCONNECT           5019
#define    HLM_INET_DIAL_STATUS                     5020
#define    HLM_INET_DIAL_CONNECTED                  5021

// Q+A messages.
#define    HLM_PHONE_CIDNUMBERQ                     5032
#define    HLM_PHONE_CIDNUMBERA                     5033
#define    HLM_PHONE_CIDNAMEQ                       5034
#define    HLM_PHONE_CIDNAMEA                       5035

#define    HLM_PHONE_GETMAILBOXESQ                  5040
#define    HLM_PHONE_GETMAILBOXESA                  5041
#define    HLM_PHONE_GETALLMAILBOXESQ               5042
#define    HLM_PHONE_GETALLMAILBOXESA               5043

#define    HLM_VOICEMSG_NMSGSQ                      5050
#define    HLM_VOICEMSG_NMSGSA                      5051
#define    HLM_VOICEMSG_NNEWMSGSQ                   5052
#define    HLM_VOICEMSG_NNEWMSGSA                   5053

#define    HLM_VOICEMSG_GETDATABYINDEXQ             5060
#define    HLM_VOICEMSG_GETDATABYINDEXA             5061
#define    HLM_VOICEMSG_GETNEXTDATAQ                5062
#define    HLM_VOICEMSG_GETNEXTDATAA                5063
#define    HLM_VOICEMSG_GETALLDATAQ                 5064
#define    HLM_VOICEMSG_GETALLDATAA                 5065
#define    HLM_VOICEMSG_GETWAVFILEQ                 5066
#define    HLM_VOICEMSG_GETWAVFILEA                 5067
#define    HLM_VOICEMSG_GETAUFILEQ                  5068
#define    HLM_VOICEMSG_GETAUFILEA                  5069
#define    HLM_VOICEMSG_STOPQ                       5070
#define    HLM_VOICEMSG_STOPA                       5071
#define    HLM_VOICEMSG_DELETEQ                     5072
#define    HLM_VOICEMSG_DELETEA                     5073
#define    HLM_VOICEMSG_GETRECSTREAMQ               5074
#define    HLM_VOICEMSG_GETRECSTREAMA               5075
#define    HLM_VOICEMSG_STOPRECSTREAMQ              5076
#define    HLM_VOICEMSG_STOPRECSTREAMA              5077
#define    HLM_VOICEMSG_MARKNEWQ                    5078
#define    HLM_VOICEMSG_MARKNEWA                    5079
#define    HLM_VOICEMSG_PUTWFXQ                     5080
#define    HLM_VOICEMSG_PUTWXFA                     5081
#define    HLM_VOICEMSG_PUTPACKETQ                  5082
#define    HLM_VOICEMSG_PUTPACKETA                  5083
#define    HLM_VOICEMSG_PUTENDQ                     5084
#define    HLM_VOICEMSG_PUTENDA                     5085

#define    HLM_PHONE_DIALQ                          5090
#define    HLM_PHONE_DIALA                          5091
#define    HLM_PHONE_OUTGOING_DICONNECTQ            5092
#define    HLM_PHONE_OUTGOING_DICONNECTA            5093

#define    HLM_INCOMING_GETALLINFOQ                 5094
#define    HLM_INCOMING_GETALLINFOA                 5095
#define    HLM_INCOMING_GETALLINFOSTATSQ            5096
#define    HLM_INCOMING_GETALLINFOSTATSA            5097

#define    HLM_GREETING_GETINFOQ                    5100
#define    HLM_GREETING_GETINFOA                    5101
#define    HLM_GREETING_GETAUDIOFILEQ               5102
#define    HLM_GREETING_GETAUDIOFILEA               5103
#define    HLM_GREETING_GETAUAUDIOFILEQ             5104
#define    HLM_GREETING_GETAUAUDIOFILEA             5105
#define    HLM_GREETING_GETBEEPQ                    5106
#define    HLM_GREETING_GETBEEPA                    5107
#define    HLM_GREETING_PUTWFXQ                     5108
#define    HLM_GREETING_PUTWXFA                     5109
#define    HLM_GREETING_PUTPACKETQ                  5110
#define    HLM_GREETING_PUTPACKETA                  5111
#define    HLM_GREETING_PUTENDQ                     5112
#define    HLM_GREETING_PUTENDA                     5113
#define    HLM_GREETING_DELETEQ                     5114
#define    HLM_GREETING_DELETEA                     5115
#define    HLM_GREETING_GETRINGQ                    5116
#define    HLM_GREETING_GETRINGA                    5117
#define    HLM_GREETING_SETRINGQ                    5118
#define    HLM_GREETING_SETRINGA                    5119
#define    HLM_GREETING_GETSCREENCALLQ              5120
#define    HLM_GREETING_GETSCREENCALLA              5121
#define    HLM_GREETING_SETSCREENCALLQ              5122
#define    HLM_GREETING_SETSCREENCALLA              5123

#define    HLM_INTERCOM_BEGINBROADCASTQ             5125
#define    HLM_INTERCOM_PUTPACKETQ                  5126
#define    HLM_INTERCOM_ENDBROADCASTQ               5127
#define    HLM_INTERCOM_BEGINPLAYQ                  5128
#define    HLM_INTERCOM_ENDPLAYQ                    5129

#define    HLM_INET_DIALQ                           5130
#define    HLM_INET_DIALA                           5131
#define    HLM_MODEM_GETCONFIGJARQ                  5132
#define    HLM_MODEM_GETCONFIGJARA                  5133
#define    HLM_MODEM_GETJAVAPADJARQ                 5134
#define    HLM_MODEM_GETJAVAPADJARA                 5135
#define    HLM_MODEM_LOGOUTCLIENTQ                  5136
#define    HLM_MODEM_LOGOUTCLIENTA                  5137

#define    HLM_INTERCOM_ISTABLETAVAILQ              5140
#define    HLM_INTERCOM_ISTABLETAVAILA              5141
#define    HLM_INTERCOM_REQUESTTABLETSTREAMSTARTQ   5142
#define    HLM_INTERCOM_REQUESTTABLETSTREAMSTARTA   5143
#define    HLM_INTERCOM_REQUESTTABLETSTREAMSTOPQ    5144
#define    HLM_INTERCOM_REQUESTTABLETSTREAMSTOPA    5145
#define    HLM_INTERCOM_GETALLCLIENTSQ              5146
#define    HLM_INTERCOM_GETALLCLIENTSA              5147

// Config messages
#define    HLM_PHONE_GETTTSOPTSQ                    5150
#define    HLM_PHONE_GETTTSOPTSA                    5151
#define    HLM_PHONE_SETTTSOPTSQ                    5152
#define    HLM_PHONE_SETTTSOPTSA                    5153
#define    HLM_PHONE_ADDNEWMAILBOXQ                 5154
#define    HLM_PHONE_ADDNEWMAILBOXA                 5155
#define    HLM_PHONE_DELMAILBOXBYIDQ                5156
#define    HLM_PHONE_DELMAILBOXBYIDA                5157
#define    HLM_PHONE_SETMAILBOXNAMEBYIDQ            5158
#define    HLM_PHONE_SETMAILBOXNAMEBYIDA            5159

#define    HLM_PHONE_END                    5159


#define    HLM_TTS_START                    5500

// Unsolicited
#define    HLM_TTS_NEWTTSMESSAGE                    5500

#define    HLM_TTS_ADDNEWMESSAGEQ                   5502
#define    HLM_TTS_ADDNEWMESSAGEA                   5503
#define    HLM_TTS_DELMESSAGEBYIDQ                  5504
#define    HLM_TTS_DELMESSAGEBYIDA                  5505
#define    HLM_TTS_GETALLMESSAGESQ                  5506
#define    HLM_TTS_GETALLMESSAGESA                  5507
#define    HLM_TTS_SETMESSAGETEXTQ                  5508
#define    HLM_TTS_SETMESSAGETEXTA                  5509
#define    HLM_TTS_GETTTSMESSAGEQ                   5510
#define    HLM_TTS_GETTTSMESSAGEA                   5511
#define    HLM_TTS_GETTTSMESSAGEAUQ                 5512
#define    HLM_TTS_GETTTSMESSAGEAUA                 5513
#define    HLM_TTS_GETREFERENCEQ                    5514
#define    HLM_TTS_GETREFERENCEA                    5515
#define    HLM_TTS_GETALLWAVSQ                      5516
#define    HLM_TTS_GETALLWAVSA                      5517

#define    HLM_TTS_END                     5517


/**/
/*                                                                    */
/* Video messages                                                     */
/*                                                                    */
/**/
#define    HLM_VIDEO_START                  6000

// Unsolicited
#define    HLM_VIDEO_IMAGE                          6000
#define    HLM_VIDEO_IMAGE_DONE                     6001
#define    HLM_VIDEO_STARTDEBUG                     6002
#define    HLM_VIDEO_ENDDEBUG                       6003
#define    HLM_VIDEO_AUDIODATA                      6004
#define    HLM_VIDEO_MP3AUDIODATA                   6005
#define    HLM_VIDEO_ULAWAUDIODATA                  6006
#define    HLM_VIDEO_MPEG1DATA                      6007
#define    HLM_VIDEO_FULLFRAMEJPGDATA               6080
#define    HLM_VIDEO_FULLFRAMEYUY2DATA              6081
#define    HLM_VIDEO_MPEG_BUFFERSIZE                6075
#define    HLM_VIDEO_OFFLINE                        6076
#define    HLM_VIDEO_FULLFRAMEJPGDATA180            6077
#define    HLM_VIDEO_MPEG1DATA180                   6078

// Q/A
#define    HLM_VIDEO_GETALLCAMERASQ                 6008
#define    HLM_VIDEO_GETALLCAMERASA                 6009
#define    HLM_VIDEO_NCAMERASQ                      6010
#define    HLM_VIDEO_NCAMERASA                      6011
#define    HLM_VIDEO_CAMERANAMEBYIDQ                6012
#define    HLM_VIDEO_CAMERANAMEBYIDA                6013
#define    HLM_VIDEO_GETCAMUSERINFOBYIDQ            6014
#define    HLM_VIDEO_GETCAMUSERINFOBYIDA            6015
#define    HLM_VIDEO_SETCAMUSERINFOBYIDQ            6016
#define    HLM_VIDEO_SETCAMUSERINFOBYIDA            6017
#define    HLM_VIDEO_CAMERASTARTBYIDQ               6018
#define    HLM_VIDEO_CAMERASTARTBYIDA               6019
#define    HLM_VIDEO_CAMERASTOPBYIDQ                6022
#define    HLM_VIDEO_CAMERASTOPBYIDA                6023
#define    HLM_VIDEO_CAMERATRACKBYIDQ               6024
#define    HLM_VIDEO_CAMERATRACKBYIDA               6025
#define    HLM_VIDEO_CAMERASTOPTRACKBYIDQ           6026
#define    HLM_VIDEO_CAMERASTOPTRACKBYIDA           6027
#define    HLM_VIDEO_SETRESMODEBYIDQ                6031
#define    HLM_VIDEO_SETRESMODEBYIDA                6032
#define    HLM_VIDEO_GETRESMODEBYIDQ                6033
#define    HLM_VIDEO_GETRESMODEBYIDA                6034
#define    HLM_VIDEO_SETTEMPLOWRESQ                 6035
#define    HLM_VIDEO_SETTEMPLOWRESA                 6036
#define    HLM_VIDEO_ENDTEMPLOWRESQ                 6037
#define    HLM_VIDEO_ENDTEMPLOWRESA                 6038
#define    HLM_VIDEO_GETAUDIOCAPSBYIDQ              6039
#define    HLM_VIDEO_GETAUDIOCAPSBYIDA              6040
#define    HLM_VIDEO_ADDAUDIOCLIENTBYIDQ            6041
#define    HLM_VIDEO_ADDAUDIOCLIENTBYIDA            6042
#define    HLM_VIDEO_ADDMP3AUDIOCLIENTBYIDQ         6043
#define    HLM_VIDEO_ADDMP3AUDIOCLIENTBYIDA         6044
#define    HLM_VIDEO_ADDULAWAUDIOCLIENTBYIDQ        6045
#define    HLM_VIDEO_ADDULAWAUDIOCLIENTBYIDA        6046
#define    HLM_VIDEO_REMOVEAUDIOCLIENTBYIDQ         6047
#define    HLM_VIDEO_REMOVEAUDIOCLIENTBYIDA         6048
#define    HLM_VIDEO_MOVELEFTBYIDQ                  6049
#define    HLM_VIDEO_MOVELEFTBYIDA                  6050
#define    HLM_VIDEO_MOVERIGHTBYIDQ                 6051
#define    HLM_VIDEO_MOVERIGHTBYIDA                 6052
#define    HLM_VIDEO_MOVEUPBYIDQ                    6053
#define    HLM_VIDEO_MOVEUPBYIDA                    6054
#define    HLM_VIDEO_MOVEDOWNBYIDQ                  6055
#define    HLM_VIDEO_MOVEDOWNBYIDA                  6056
#define    HLM_VIDEO_GETSTATSBYIDQ                  6057
#define    HLM_VIDEO_GETSTATSBYIDA                  6058
#define    HLM_VIDEO_MOVEDXDYBYIDQ                  6061
#define    HLM_VIDEO_MOVEDXDYBYIDA                  6062
#define    HLM_VIDEO_ZOOMCONTROLQ                   6065
#define    HLM_VIDEO_ZOOMCONTROLA                   6066
#define    HLM_VIDEO_PRESETCONTROLQ                 6067
#define    HLM_VIDEO_PRESETCONTROLA                 6068
#define    HLM_VIDEO_MOVEABSQ                       6069
#define    HLM_VIDEO_MOVEABSA                       6070
#define    HLM_VIDEO_ZOOMABSQ                       6071
#define    HLM_VIDEO_ZOOMABSA                       6072
#define    HLM_VIDEO_GETCAMPOSQ                     6073
#define    HLM_VIDEO_GETCAMPOSA                     6074


#define    HLM_VIDEO_END                    6100


#define    HLM_DVR_START                    6500

// Unsolicited
#define    HLM_DVR_NOTIFYLISTCHANGED                6500
#define    HLM_DVR_NOTIFYPLAYBACKSTATUS             6501
#define    HLM_DVR_NOTIFYAVICOMPILESTATUS           6524

// QA
#define    HLM_DVR_GETGLOBALOPTSQ                   6502
#define    HLM_DVR_GETGLOBALOPTSA                   6503
#define    HLM_DVR_SETGLOBALOPTSQ                   6504
#define    HLM_DVR_SETGLOBALOPTSA                   6505
#define    HLM_DVR_GETALLDVRBYIDQ                   6506
#define    HLM_DVR_GETALLDVRBYIDA                   6507
#define    HLM_DVR_DELDVRBYIDPATHQ                  6510
#define    HLM_DVR_DELDVRBYIDPATHA                  6511
#define    HLM_DVR_BEGINPLAYBACKSESSIONQ            6514
#define    HLM_DVR_BEGINPLAYBACKSESSIONA            6515
#define    HLM_DVR_ENDPLAYBACKSESSIONQ              6516
#define    HLM_DVR_ENDPLAYBACKSESSIONA              6517
#define    HLM_DVR_SESSIONCONTROLQ                  6518
#define    HLM_DVR_SESSIONCONTROLA                  6519
#define    HLM_DVR_GETDVRBITMAPQ                    6520
#define    HLM_DVR_GETDVRBITMAPA                    6521
#define    HLM_DVR_GETAVICLIPQ                      6522
#define    HLM_DVR_GETAVICLIPA                      6523
#define    HLM_DVR_GETAVICLIPA                      6523

#define    HLM_DVR_END                      6651

/**/
/*                                                                    */
/* Configureation and setup messages                                  */
/*                                                                    */
/**/
#define    HLM_SYSCONFIG_START                  8000

// Unsolicited
#define    HLM_SYSCONFIG_UICHANGED                  8000
#define    HLM_SYSCONFIG_CONFIGCHANGED              8001
#define    HLM_SYSCONFIG_REMOTESCHANGED             8002
#define    HLM_SYSCONFIG_CONFIGSTATUS               8003
#define    HLM_SYSCONFIG_NOTIFYRESTART              8004
#define    HLM_SYSCONFIG_CLIENTSCHANGED             8005
#define    HLM_SYSCONFIG_NOTIFYNEEDWRITEFLASH       8300
#define    HLM_SYSCONFIG_FILEDATA                   8301

#define    HLM_SYSCONFIG_SETCONFIGDROPLISTQ         8003
#define    HLM_SYSCONFIG_SETCONFIGDROPLISTA         8004
#define    HLM_SYSCONFIG_GETALLCOMPORTSQ            8005
#define    HLM_SYSCONFIG_GETALLCOMPORTSA            8006
#define    HLM_SYSCONFIG_GETCONFIGDROPLISTQ         8007
#define    HLM_SYSCONFIG_GETCONFIGDROPLISTA         8008
#define    HLM_SYSCONFIG_GETCOMMSTATSQ              8010
#define    HLM_SYSCONFIG_GETCOMMSTATSA              8011
#define    HLM_SYSCONFIG_GETALLNETWORKDEVSQ         8012
#define    HLM_SYSCONFIG_GETALLNETWORKDEVSA         8013
#define    HLM_SYSCONFIG_GETBRIDGESTATSBYIDQ        8014
#define    HLM_SYSCONFIG_GETBRIDGESTATSBYIDA        8015
#define    HLM_SYSCONFIG_RESETBRIDGESTATSBYIDQ      8016
#define    HLM_SYSCONFIG_RESETBRIDGESTATSBYIDA      8017
#define    HLM_SYSCONFIG_GETCOMPSTATSBYFAMIDQ       8018
#define    HLM_SYSCONFIG_GETCOMPSTATSBYFAMIDA       8019
#define    HLM_SYSCONFIG_RESETCOMPSTATSBYFAMIDQ     8020
#define    HLM_SYSCONFIG_RESETCOMPSTATSBYFAMIDA     8021
#define    HLM_SYSCONFIG_GETCLIENTSTATSQ            8022
#define    HLM_SYSCONFIG_GETCLIENTSTATSA            8023
#define    HLM_SYSCONFIG_GETALLFAMTYPESQ            8024
#define    HLM_SYSCONFIG_GETALLFAMTYPESA            8025
#define    HLM_SYSCONFIG_GETALLDEVTYPESBYFAMQ       8026
#define    HLM_SYSCONFIG_GETALLDEVTYPESBYFAMA       8027
#define    HLM_SYSCONFIG_GETCOMPCOUNTSBYFAMQ        8028
#define    HLM_SYSCONFIG_GETCOMPCOUNTSBYFAMA        8029
#define    HLM_SYSCONFIG_GETALLCOMPSALLFAMQ         8030
#define    HLM_SYSCONFIG_GETALLCOMPSALLFAMA         8031
#define    HLM_SYSCONFIG_GETALLCOMPBYFAMILYQ        8032
#define    HLM_SYSCONFIG_GETALLCOMPBYFAMILYA        8033
#define    HLM_SYSCONFIG_ADDCOMPBYFAMILYQ           8034
#define    HLM_SYSCONFIG_ADDCOMPBYFAMILYA           8035
#define    HLM_SYSCONFIG_REMOVECOMPBYIDQ            8036
#define    HLM_SYSCONFIG_REMOVECOMPBYIDA            8037
#define    HLM_SYSCONFIG_GETCOMPCONFIGBYIDQ         8038
#define    HLM_SYSCONFIG_GETCOMPCONFIGBYIDA         8039
#define    HLM_SYSCONFIG_SETCOMPCONFIGBYIDQ         8040
#define    HLM_SYSCONFIG_SETCOMPCONFIGBYIDA         8041
#define    HLM_SYSCONFIG_COMPSTATUSBYIDQ            8042
#define    HLM_SYSCONFIG_COMPSTATUSBYIDA            8043
#define    HLM_SYSCONFIG_COMPSTRINGSBYIDQ           8044
#define    HLM_SYSCONFIG_COMPSTRINGSBYIDA           8045
#define    HLM_SYSCONFIG_GETALLBRIDGESQ             8046
#define    HLM_SYSCONFIG_GETALLBRIDGESA             8047
#define    HLM_SYSCONFIG_GETALLIFACETYPESQ          8048
#define    HLM_SYSCONFIG_GETALLIFACETYPESA          8049
#define    HLM_SYSCONFIG_ADDBRIDGEQ                 8050
#define    HLM_SYSCONFIG_ADDBRIDGEA                 8051
#define    HLM_SYSCONFIG_REMBRIDGEQ                 8052
#define    HLM_SYSCONFIG_REMBRIDGEA                 8053
#define    HLM_SYSCONFIG_GETBRIDGECONFIGQ           8054
#define    HLM_SYSCONFIG_GETBRIDGECONFIGA           8055
#define    HLM_SYSCONFIG_SETBRIDGECONFIGQ           8056
#define    HLM_SYSCONFIG_SETBRIDGECONFIGA           8057
#define    HLM_SYSCONFIG_GETUSERINFOQ               8058
#define    HLM_SYSCONFIG_GETUSERINFOA               8059
#define    HLM_SYSCONFIG_SETUSERINFOQ               8060
#define    HLM_SYSCONFIG_SETUSERINFOA               8061
#define    HLM_SYSCONFIG_GETSTRINGSSYSTEMQ          8062
#define    HLM_SYSCONFIG_GETSTRINGSSYSTEMA          8063
#define    HLM_SYSCONFIG_GETREMOTEUSERSQ            8064
#define    HLM_SYSCONFIG_GETREMOTEUSERSA            8065
#define    HLM_SYSCONFIG_BOOTUSERBYINDEXQ           8066
#define    HLM_SYSCONFIG_BOOTUSERBYINDEXA           8067
#define    HLM_SYSCONFIG_GETFAILURLSQ               8068
#define    HLM_SYSCONFIG_GETFAILURLSA               8069
#define    HLM_SYSCONFIG_SETFAILURLSQ               8070
#define    HLM_SYSCONFIG_SETFAILURLSA               8071
#define    HLM_SYSCONFIG_RESETSYSCOMPBYIDQ          8072
#define    HLM_SYSCONFIG_RESETSYSCOMPBYIDA          8073
#define    HLM_SYSCONFIG_GETALLORPHANBRICKSQ        8074
#define    HLM_SYSCONFIG_GETALLORPHANBRICKSA        8075
#define    HLM_SYSCONFIG_GETRESETWARNINGMSGQ        8076
#define    HLM_SYSCONFIG_GETRESETWARNINGMSGA        8077
#define    HLM_SYSCONFIG_FORCEUSERUPBYINDEXQ        8078
#define    HLM_SYSCONFIG_FORCEUSERUPBYINDEXA        8079
#define    HLM_SYSCONFIG_GETALLGCPORTSQ             8085
#define    HLM_SYSCONFIG_GETALLGCPORTSA             8086
#define    HLM_SYSCONFIG_SETUSINGEXTERNCONFIGQ      8087
#define    HLM_SYSCONFIG_SETUSINGEXTERNCONFIGA      8088

// Scheduling
#define    HLM_SYSCONFIG_GETSCHEDLAYOUTBYIDQ        8080
#define    HLM_SYSCONFIG_GETSCHEDLAYOUTBYIDA        8081
#define    HLM_SYSCONFIG_SETSCHEDLAYOUTBYIDQ        8082
#define    HLM_SYSCONFIG_SETSCHEDLAYOUTBYIDA        8083

#define    HLM_SYSCONFIG_SETCOMPNAMEBYIDQ           8090
#define    HLM_SYSCONFIG_SETCOMPNAMEBYIDA           8091
#define    HLM_SYSCONFIG_SETBRIDGENAMEBYIDQ         8092
#define    HLM_SYSCONFIG_SETBRIDGENAMEBYIDA         8093

#define    HLM_SYSCONFIG_ADDCOMPBYFAMILYEXQ         8094
#define    HLM_SYSCONFIG_ADDCOMPBYFAMILYEXA         8095

#define    HLM_SYSCONFIG_FINDNEWCOMPSBYFAMQ         8100
#define    HLM_SYSCONFIG_FINDNEWCOMPSBYFAMA         8101

#define    HLM_SYSCONFIG_GETSYSTIMEQ                8110
#define    HLM_SYSCONFIG_GETSYSTIMEA                8111
#define    HLM_SYSCONFIG_SETSYSTIMEQ                8112
#define    HLM_SYSCONFIG_SETSYSTIMEA                8113

#define    HLM_SYSCONFIG_GET_OSUPDATERQ             8116
#define    HLM_SYSCONFIG_GET_OSUPDATERA             8117
#define    HLM_SYSCONFIG_GET_NKBINQ                 8118
#define    HLM_SYSCONFIG_GET_NKBINA                 8119

#define    HLM_SYSCONFIG_GETVERSIONQ                8120
#define    HLM_SYSCONFIG_GETVERSIONA                8121
#define    HLM_SYSCONFIG_RESETWEBVERSIONSQ          8122
#define    HLM_SYSCONFIG_RESETWEBVERSIONSA          8123
#define    HLM_SYSCONFIG_GETWEBVERSIONSQ            8124
#define    HLM_SYSCONFIG_GETWEBVERSIONSA            8125
#define    HLM_SYSCONFIG_DOUPDATENOWQ               8126
#define    HLM_SYSCONFIG_DOUPDATENOWA               8127
#define    HLM_SYSCONFIG_GETCRYSTALPADVERSIONQ      8128
#define    HLM_SYSCONFIG_GETCRYSTALPADVERSIONA      8129
#define    HLM_SYSCONFIG_GETCRYSTALPADQ             8130
#define    HLM_SYSCONFIG_GETCRYSTALPADA             8131
#define    HLM_SYSCONFIG_GETMINIPADQ                8132
#define    HLM_SYSCONFIG_GETMINIPADA                8133
#define    HLM_SYSCONFIG_DOBACKUPQ                  8134
#define    HLM_SYSCONFIG_DOBACKUPA                  8135
#define    HLM_SYSCONFIG_DORESTOREQ                 8136
#define    HLM_SYSCONFIG_DORESTOREA                 8137
#define    HLM_SYSCONFIG_GETBACKUPINFOQ             8138
#define    HLM_SYSCONFIG_GETBACKUPINFOA             8139
#define    HLM_SYSCONFIG_GETHLIEQ                   8140
#define    HLM_SYSCONFIG_GETHLIEA                   8141
#define    HLM_SYSCONFIG_GETHLFAVSDIRQ              8142
#define    HLM_SYSCONFIG_GETHLFAVSDIRA              8143
#define    HLM_SYSCONFIG_GETHLFAVSFILEQ             8144
#define    HLM_SYSCONFIG_GETHLFAVSFILEA             8145
#define    HLM_SYSCONFIG_GETTABCONFIGQ              8146
#define    HLM_SYSCONFIG_GETTABCONFIGA              8147
#define    HLM_SYSCONFIG_GETHLCONFIGQ               8148
#define    HLM_SYSCONFIG_GETHLCONFIGA               8149
#define    HLM_SYSCONFIG_GETPOOLCONFIGQ             8150
#define    HLM_SYSCONFIG_GETPOOLCONFIGA             8151
#define    HLM_SYSCONFIG_GETHLOSDQ                  8152
#define    HLM_SYSCONFIG_GETHLOSDA                  8153
#define    HLM_SYSCONFIG_GETHLOSDVERSIONQ           8154
#define    HLM_SYSCONFIG_GETHLOSDVERSIONA           8155
#define    HLM_SYSCONFIG_GETHR2CODEQ                8156
#define    HLM_SYSCONFIG_GETHR2CODEA                8157


#define    HLM_SYSCONFIG_GETALLUSERSQ               8160
#define    HLM_SYSCONFIG_GETALLUSERSA               8161
#define    HLM_SYSCONFIG_SETUSERPASSWORDBYIDQ       8162
#define    HLM_SYSCONFIG_SETUSERPASSWORDBYIDA       8163
#define    HLM_SYSCONFIG_SETUSERCONFIGBYIDQ         8164
#define    HLM_SYSCONFIG_SETUSERCONFIGBYIDA         8165
#define    HLM_SYSCONFIG_ADDNEWUSERQ                8166
#define    HLM_SYSCONFIG_ADDNEWUSERA                8167
#define    HLM_SYSCONFIG_DELUSERBYIDQ               8168
#define    HLM_SYSCONFIG_DELUSERBYIDA               8169
#define    HLM_SYSCONFIG_RESTARTGATEWAYQ            8170
#define    HLM_SYSCONFIG_RESTARTGATEWAYA            8171

#define    HLM_SYSCONFIG_SETSYSTEMIDQ               8172
#define    HLM_SYSCONFIG_SETSYSTEMIDA               8173
#define    HLM_SYSCONFIG_GETIPCONFIGQ               8174
#define    HLM_SYSCONFIG_GETIPCONFIGA               8175
#define    HLM_SYSCONFIG_SETIPCONFIGQ               8176
#define    HLM_SYSCONFIG_SETIPCONFIGA               8177
#define    HLM_SYSCONFIG_GETCPUUSAGEQ               8178
#define    HLM_SYSCONFIG_GETCPUUSAGEA               8179
#define    HLM_SYSCONFIG_WRITEFLASHNOWQ             8180
#define    HLM_SYSCONFIG_WRITEFLASHNOWA             8181

#define    HLM_SYSCONFIG_GETUSERCONFIGBYIDQ         8182
#define    HLM_SYSCONFIG_GETUSERCONFIGBYIDA         8183

#define    HLM_SYSCONFIG_REBOOTMACHINEQ             8184
#define    HLM_SYSCONFIG_REBOOTMACHINEA             8185
#define    HLM_SYSCONFIG_SHUTDOWNMONITORQ           8186
#define    HLM_SYSCONFIG_SHUTDOWNMONITORA           8187

#define    HLM_SYSCONFIG_GETSAFEMODESTATUSQ         8188
#define    HLM_SYSCONFIG_GETSAFEMODESTATUSA         8189

#define    HLM_SYSCONFIG_GETALLSYSTEMTIMERSQ        8190
#define    HLM_SYSCONFIG_GETALLSYSTEMTIMERSA        8191
#define    HLM_SYSCONFIG_ADDNEWSYSTEMTIMERQ         8192
#define    HLM_SYSCONFIG_ADDNEWSYSTEMTIMERA         8193
#define    HLM_SYSCONFIG_DELETESYSTEMTIMERQ         8194
#define    HLM_SYSCONFIG_DELETESYSTEMTIMERA         8195
#define    HLM_SYSCONFIG_GETSYSTEMTIMERINFOQ        8196
#define    HLM_SYSCONFIG_GETSYSTEMTIMERINFOA        8197
#define    HLM_SYSCONFIG_SETSYSTEMTIMERINFOQ        8198
#define    HLM_SYSCONFIG_SETSYSTEMTIMERINFOA        8199


#define    HLM_SYSCONFIG_ONSYSTEMEXITQ              8200
#define    HLM_SYSCONFIG_ONSYSTEMEXITA              8201
#define    HLM_SYSCONFIG_ONSYSTEMSTARTUPQ           8202
#define    HLM_SYSCONFIG_ONSYSTEMSTARTUPA           8203
#define    HLM_SYSCONFIG_CLEANFILESYSTEMQ           8204
#define    HLM_SYSCONFIG_CLEANFILESYSTEMA           8205

#define    HLM_SYSCONFIG_GETALLTIMEDEVENTSQ         8210
#define    HLM_SYSCONFIG_GETALLTIMEDEVENTSA         8211
#define    HLM_SYSCONFIG_ADDNEWTIMEDEVENTQ          8212
#define    HLM_SYSCONFIG_ADDNEWTIMEDEVENTA          8213
#define    HLM_SYSCONFIG_DELETETIMEDEVENTQ          8214
#define    HLM_SYSCONFIG_DELETETIMEDEVENTA          8215
#define    HLM_SYSCONFIG_GETTIMEDEVENTINFOQ         8216
#define    HLM_SYSCONFIG_GETTIMEDEVENTINFOA         8217
#define    HLM_SYSCONFIG_SETTIMEDEVENTINFOQ         8218
#define    HLM_SYSCONFIG_SETTIMEDEVENTINFOA         8219

#define    HLM_SYSCONFIG_GETBRICKSTATSQ             8220
#define    HLM_SYSCONFIG_GETBRICKSTATSA             8221

#define    HLM_SYSCONFIG_GETTABOPTIONSBYFAMILYQ     8222
#define    HLM_SYSCONFIG_GETTABOPTIONSBYFAMILYA     8223
#define    HLM_SYSCONFIG_GETTABCONFIGBYFAMILYQ      8224
#define    HLM_SYSCONFIG_GETTABCONFIGBYFAMILYA      8225
#define    HLM_SYSCONFIG_SETTABCONFIGBYFAMILYIDQ    8226
#define    HLM_SYSCONFIG_SETTABCONFIGBYFAMILYIDA    8227

#define    HLM_SYSCONFIG_GETSYSTEMMODESQ            8228
#define    HLM_SYSCONFIG_GETSYSTEMMODESA            8229
#define    HLM_SYSCONFIG_SETSYSTEMMODESQ            8230
#define    HLM_SYSCONFIG_SETSYSTEMMODESA            8231

#define    HLM_SYSCONFIG_GETMAINTABCONFIGQ          8240
#define    HLM_SYSCONFIG_GETMAINTABCONFIGA          8241
#define    HLM_SYSCONFIG_SETMAINTABCONFIGQ          8242
#define    HLM_SYSCONFIG_SETMAINTABCONFIGA          8243

#define    HLM_SYSCONFIG_GETTABCONFIGBYFAMILYIDQ    8244
#define    HLM_SYSCONFIG_GETTABCONFIGBYFAMILYIDA    8245

#define    HLM_SYSCONFIG_GETHELPERDLLQ              8248
#define    HLM_SYSCONFIG_GETHELPERDLLA              8249

#define    HLM_SYSCONFIG_RESETINTERFACEQ            8250
#define    HLM_SYSCONFIG_RESETINTERFACEA            8251

#define    HLM_SYSCONFIG_EXECONFIGPROCQ             8252
#define    HLM_SYSCONFIG_EXECONFIGPROCA             8253

#define	   HLM_SYSCONFIG_SET_DEVICES				8254
#define	   HLM_SYSCONFIG_GET_DEVICES				8255
#define	   HLM_SYSCONFIG_GET_GATEWAYS				8256
#define	   HLM_SYSCONFIG_SET_GATEWAY				8257

#define	   HLM_SYSCONFIG_SET_DEFAULT_PORT_SETTINGS	8268

#define    HLM_SYSCONFIG_GETCONFIGIDQ               8267
#define    HLM_SYSCONFIG_GETCONFIGIDA               8268


#define    HLM_SYSCONFIG_REFRESH_MEDIATREEQ         8270
#define    HLM_SYSCONFIG_REFRESH_MEDIATREEA         8271

#define    HLM_SYSCONFIG_END                        8400

/**/
/*                                                                    */
/* System layout messages                                             */
/*                                                                    */
/**/
#define    HLM_SYSLAYOUT_START              9000

#define    HLM_SYSLAYOUT_GETLAYOUTQ                 9001
#define    HLM_SYSLAYOUT_GETLAYOUTA                 9002
#define    HLM_SYSLAYOUT_ADDSTRUCTUREQ              9003
#define    HLM_SYSLAYOUT_ADDSTRUCTUREA              9004
#define    HLM_SYSLAYOUT_DELSTRUCTUREBYIDQ          9005
#define    HLM_SYSLAYOUT_DELSTRUCTUREBYIDA          9006
#define    HLM_SYSLAYOUT_SETSTRUCTNAMEBYIDQ         9007
#define    HLM_SYSLAYOUT_SETSTRUCTNAMEBYIDA         9008
#define    HLM_SYSLAYOUT_ADDLEVELTOSTRUCTUREBYIDQ   9009
#define    HLM_SYSLAYOUT_ADDLEVELTOSTRUCTUREBYIDA   9010
#define    HLM_SYSLAYOUT_MOVELEVELTOSTRUCTBYIDQ     9011
#define    HLM_SYSLAYOUT_MOVELEVELTOSTRUCTBYIDA     9012
#define    HLM_SYSLAYOUT_DELLEVELBYIDQ              9013
#define    HLM_SYSLAYOUT_DELLEVELBYIDA              9014
#define    HLM_SYSLAYOUT_SETLEVELNAMEBYIDQ          9015
#define    HLM_SYSLAYOUT_SETLEVELNAMEBYIDA          9016
#define    HLM_SYSLAYOUT_ADDAREATOLEVELBYIDQ        9017
#define    HLM_SYSLAYOUT_ADDAREATOLEVELBYIDA        9018
#define    HLM_SYSLAYOUT_MOVEAREATOLEVELBYIDQ       9019
#define    HLM_SYSLAYOUT_MOVEAREATOLEVELBYIDA       9020
#define    HLM_SYSLAYOUT_DELAREABYIDQ               9021
#define    HLM_SYSLAYOUT_DELAREABYIDA               9022
#define    HLM_SYSLAYOUT_SETAREANAMEBYIDQ           9023
#define    HLM_SYSLAYOUT_SETAREANAMEBYIDA           9024
#define    HLM_SYSLAYOUT_GETALLAREASQ               9025
#define    HLM_SYSLAYOUT_GETALLAREASA               9026

#define    HLM_SYSLAYOUT_END                9026

/**/
/*                                                                    */
/* Lock Config Messages                                               */
/*                                                                    */
/**/

#define    HLM_LOCK_START                   9050
#define    HLM_LOCK_GETCURCONFIGQ                   9050
#define    HLM_LOCK_GETCURCONFIGA                   9051
#define    HLM_LOCK_GETINSTALLCODEBYTYPEQ           9052
#define    HLM_LOCK_GETINSTALLCODEBYTYPEA           9053
#define    HLM_LOCK_INSTALLBYTYPEQ                  9054
#define    HLM_LOCK_INSTALLBYTYPEA                  9055
#define    HLM_LOCK_UNINSTALLBYTYPEQ                9056
#define    HLM_LOCK_UNINSTALLBYTYPEA                9057
#define    HLM_LOCK_GETMACCONFIGQ                   9058
#define    HLM_LOCK_GETMACCONFIGA                   9059
#define    HLM_LOCK_SETMACINDEXQ                    9060
#define    HLM_LOCK_SETMACINDEXA                    9061
#define    HLM_LOCK_INSTALLKEY_Q                    9062
#define    HLM_LOCK_INSTALLKEY_A                    9063
#define    HLM_LOCK_GETINSTALLEDKEYS_Q              9064
#define    HLM_LOCK_GETINSTALLEDKEYS_A              9065
#define    HLM_LOCK_GETCURCONFIG51Q                 9066
#define    HLM_LOCK_GETCURCONFIG51A                 9067
#define    HLM_LOCK_ASSIGNAPPKEYBYTYPEQ             9068
#define    HLM_LOCK_ASSIGNAPPKEYBYTYPEA             9069
#define    HLM_LOCK_UNASSIGNAPPKEYBYTYPEQ           9070
#define    HLM_LOCK_UNASSIGNAPPKEYBYTYPEA           9071
#define    HLM_LOCK_INSTALLTIMERBYTYPEQ             9072
#define    HLM_LOCK_INSTALLTIMERBYTYPEA             9073

#define    HLM_LOCK_END                     9073

/**/
/*                                                                    */
/* Image Server Messages                                              */
/*                                                                    */
/**/

#define    HLM_IMAGESERV_START              9100

// Unsolicited
#define    HLM_IMAGESERV_TABICONCHANGED             9100
#define    HLM_IMAGESERV_TABTEXTCHANGED             9101

// Q/A
#define    HLM_IMAGESERV_GETIMAGEFILE_CLEARQ        9102
#define    HLM_IMAGESERV_GETIMAGEFILE_CLEARA        9103
#define    HLM_IMAGESERV_GET3DTEXTQ                 9106
#define    HLM_IMAGESERV_GET3DTEXTA                 9107
#define    HLM_IMAGESERV_INVALIDATEICONSQ           9110
#define    HLM_IMAGESERV_INVALIDATEICONSA           9111

#define    HLM_IMAGESERV_GETALLICONSQ               9112
#define    HLM_IMAGESERV_GETALLICONSA               9113
#define    HLM_IMAGESERV_ADDNEWICONQ                9114
#define    HLM_IMAGESERV_ADDNEWICONA                9115
#define    HLM_IMAGESERV_GETICONBYNAMEQ             9116
#define    HLM_IMAGESERV_GETICONBYNAMEA             9117
#define    HLM_IMAGESERV_SETICONBYNAMEQ             9118
#define    HLM_IMAGESERV_SETICONBYNAMEA             9119
#define    HLM_IMAGESERV_GETALLSOURCEICONSQ         9120
#define    HLM_IMAGESERV_GETALLSOURCEICONSA         9121
#define    HLM_IMAGESERV_GETALLTEXTURESQ            9122
#define    HLM_IMAGESERV_GETALLTEXTURESA            9123
#define    HLM_IMAGESERV_GETTEXTUREQ                9124
#define    HLM_IMAGESERV_GETTEXTUREA                9125
#define    HLM_IMAGESERV_GETPICTUREQ                9126
#define    HLM_IMAGESERV_GETPICTUREA                9127
#define    HLM_IMAGESERV_GETPICTUREFROMARTWORKQ     9128
#define    HLM_IMAGESERV_GETPICTUREFROMARTWORKA     9129

#define    HLM_IMAGESERV_END                9129

/**/
/*                                                                    */
/* Wave Server Messages                                               */
/*                                                                    */
/**/

#define    HLM_WAVSERV_START                9200
#define    HLM_WAVSERV_GETWAVFILEQ                  9200
#define    HLM_WAVSERV_GETWAVFILEA                  9201
#define    HLM_WAVSERV_GETWAVFILEAUQ                9202
#define    HLM_WAVSERV_GETWAVFILEAUA                9203
#define    HLM_WAVSERV_END                  9203

/**/
/*                                                                    */
/* Event Log messages                                                 */
/*                                                                    */
/**/

#define    HLM_SYSLOG_START                 9500

// Unsolicited
#define    HLM_SYSLOG_LOGDATA                       9500
#define    HLM_SYSLOG_ASSERT                        9501
#define    HLM_SYSLOG_TRACE                         9502
#define    HLM_SYSLOG_LOGTRACE                      9503
#define    HLM_SYSLOG_CANCELQUERY                   9504

#define    HLM_SYSLOG_GETEVENT_SYSTEMQ              9510
#define    HLM_SYSLOG_GETEVENT_SYSTEMA              9511
#define    HLM_SYSLOG_GETEVENT_SYSCOMPQ             9512
#define    HLM_SYSLOG_GETEVENT_SYSCOMPA             9513
#define    HLM_SYSLOG_GETEVENT_SYSCOMPBYIDQ         9514
#define    HLM_SYSLOG_GETEVENT_SYSCOMPBYIDA         9515
#define    HLM_SYSLOG_GETEVENT_ERRORLOGQ            9516
#define    HLM_SYSLOG_GETEVENT_ERRORLOGA            9517
#define    HLM_SYSLOG_END                   9517


/**/
/*                                                                    */
/* CSetServer Messages                                                 */
/*                                                                    */
/**/

#define    HLM_SETSERVER_START                  9600

// Unsolicited
#define    HLM_SETSERVER_SETTINGSCHANGED            9600

#define    HLM_SETSERVER_GETALLSCHEMESQ             9601
#define    HLM_SETSERVER_GETALLSCHEMESA             9602
#define    HLM_SETSERVER_ADDSCHEMEQ                 9603
#define    HLM_SETSERVER_ADDSCHEMEA                 9604
#define    HLM_SETSERVER_REMOVESCHEMEBYIDQ          9605
#define    HLM_SETSERVER_REMOVESCHEMEBYIDA          9606
#define    HLM_SETSERVER_GETSCHEMEBYIDQ             9607
#define    HLM_SETSERVER_GETSCHEMEBYIDA             9608
#define    HLM_SETSERVER_SETSCHEMEBYIDQ             9609
#define    HLM_SETSERVER_SETSCHEMEBYIDA             9610
#define    HLM_SETSERVER_GETDEFAULTSCHEMEQ          9611
#define    HLM_SETSERVER_GETDEFAULTSCHEMEA          9612
#define    HLM_SETSERVER_LOADHOMEPAGEDEFAULTSQ      9615
#define    HLM_SETSERVER_LOADHOMEPAGEDEFAULTSA      9616
#define    HLM_SETSERVER_GETSECTIONLAYOUTQ          9617
#define    HLM_SETSERVER_GETSECTIONLAYOUTA          9618
#define    HLM_SETSERVER_SETSECTIONLAYOUTBYIDQ      9619
#define    HLM_SETSERVER_SETSECTIONLAYOUTBYIDA      9620
#define    HLM_SETSERVER_GETHOMEPAGEWINDOWSQ        9621
#define    HLM_SETSERVER_GETHOMEPAGEWINDOWSA        9622
#define    HLM_SETSERVER_SETHOMEPAGEWINDOWSBYIDQ    9623
#define    HLM_SETSERVER_SETHOMEPAGEWINDOWSBYIDA    9624
#define    HLM_SETSERVER_GETALLCONTROLTYPESQ        9625
#define    HLM_SETSERVER_GETALLCONTROLTYPESA        9626
#define    HLM_SETSERVER_GETHOMEPAGEINFOQ           9627
#define    HLM_SETSERVER_GETHOMEPAGEINFOA           9628
#define    HLM_SETSERVER_SETHOMEPAGEINFOQ           9629
#define    HLM_SETSERVER_SETHOMEPAGEINFOA           9630
#define    HLM_SETSERVER_GETCONTROLOPTSBYTYPEQ      9631
#define    HLM_SETSERVER_GETCONTROLOPTSBYTYPEA      9632
#define    HLM_SETSERVER_GETTOGGLEBUTTONQ           9633
#define    HLM_SETSERVER_GETTOGGLEBUTTONA           9634
#define    HLM_SETSERVER_GETCONTROLSTYLESBYTYPEQ    9635
#define    HLM_SETSERVER_GETCONTROLSTYLESBYTYPEA    9636
#define    HLM_SETSERVER_GETALLIFACESBYFAMILYQ      9637
#define    HLM_SETSERVER_GETALLIFACESBYFAMILYA      9638
#define    HLM_SETSERVER_ADDNEWINTERFACEQ           9639
#define    HLM_SETSERVER_ADDNEWINTERFACEA           9640
#define    HLM_SETSERVER_DELINTERFACEQ              9641
#define    HLM_SETSERVER_DELINTERFACEA              9642
#define    HLM_SETSERVER_GETINTERFACELAYOUTBYIDQ    9643
#define    HLM_SETSERVER_GETINTERFACELAYOUTBYIDA    9644
#define    HLM_SETSERVER_SETINTERFACELAYOUTBYIDQ    9645
#define    HLM_SETSERVER_SETINTERFACELAYOUTBYIDA    9646
#define    HLM_SETSERVER_SETINTERFACELAYOUTCFGBYIDQ 9647
#define    HLM_SETSERVER_SETINTERFACELAYOUTCFGBYIDA 9648
#define    HLM_SETSERVER_GETCONTROLFUNCTIONSBYTYPEQ 9649
#define    HLM_SETSERVER_GETCONTROLFUNCTIONSBYTYPEA 9650
#define    HLM_SETSERVER_GETALLHOMEPAGESQ           9651
#define    HLM_SETSERVER_GETALLHOMEPAGESA           9652
#define    HLM_SETSERVER_GETHOMEPAGEWINDOWSBYIDQ    9653
#define    HLM_SETSERVER_GETHOMEPAGEWINDOWSBYIDA    9654
#define    HLM_SETSERVER_GETSECTIONLAYOUTBYIDQ      9655
#define    HLM_SETSERVER_GETSECTIONLAYOUTBYIDA      9656
#define    HLM_SETSERVER_ADDNEWHOMEPAGEQ            9657
#define    HLM_SETSERVER_ADDNEWHOMEPAGEA            9658
#define    HLM_SETSERVER_DELHOMEPAGEQ               9659
#define    HLM_SETSERVER_DELHOMEPAGEA               9660
#define    HLM_SETSERVER_GETASPECTLOCKQ             9661
#define    HLM_SETSERVER_GETASPECTLOCKA             9662
#define    HLM_SETSERVER_RENAMESCHEMEBYIDQ          9663
#define    HLM_SETSERVER_RENAMESCHEMEBYIDA          9664
#define    HLM_SETSERVER_RENAMEHOMEPAGEBYIDQ        9665
#define    HLM_SETSERVER_RENAMEHOMEPAGEBYIDA        9666
#define    HLM_SETSERVER_GETTABGROUPSBYFAMQ         9671
#define    HLM_SETSERVER_GETTABGROUPSBYFAMA         9672
#define    HLM_SETSERVER_CREATETABGROUPBYFAMQ       9673
#define    HLM_SETSERVER_CREATETABGROUPBYFAMA       9674
#define    HLM_SETSERVER_DELETETABGROUPBYFAMQ       9675
#define    HLM_SETSERVER_DELETETABGROUPBYFAMA       9676
#define    HLM_SETSERVER_CONFIGTABGROUPBYFAMQ       9677
#define    HLM_SETSERVER_CONFIGTABGROUPBYFAMA       9678
#define    HLM_SETSERVER_GETALLFONTSQ               9679
#define    HLM_SETSERVER_GETALLFONTSA               9680
#define    HLM_SETSERVER_GETFONTBYNAMEQ             9681
#define    HLM_SETSERVER_GETFONTBYNAMEA             9682

#define    HLM_SETSERVER_END                    9682

/**/
/*                                                                    */
/* CEventServer Messages                                              */
/*                                                                    */
/**/

#define    HLM_EVENTSERVER_START                10000

// Unsolicited
#define    HLM_EVENTSERVER_DEBUGSTRING              10000

#define    HLM_EVENTSERVER_GETALLEVENTSBYFAMILYQ    10001
#define    HLM_EVENTSERVER_GETALLEVENTSBYFAMILYA    10002
#define    HLM_EVENTSERVER_GETALLCOMMANDSBYFAMILYQ  10003
#define    HLM_EVENTSERVER_GETALLCOMMANDSBYFAMILYA  10004
#define    HLM_EVENTSERVER_ADDCOMMANDTOMAPBYIDQ     10005
#define    HLM_EVENTSERVER_ADDCOMMANDTOMAPBYIDA     10006
#define    HLM_EVENTSERVER_DELCOMMANDFROMMAPBYIDQ   10007
#define    HLM_EVENTSERVER_DELCOMMANDFROMMAPBYIDA   10008
#define    HLM_EVENTSERVER_ADDEVENTTOMAPBYIDQ       10009
#define    HLM_EVENTSERVER_ADDEVENTTOMAPBYIDA       10010
#define    HLM_EVENTSERVER_DELEVENTFROMMAPBYIDQ     10011
#define    HLM_EVENTSERVER_DELEVENTFROMMAPBYIDA     10012
#define    HLM_EVENTSERVER_RENAMEMAPBYIDQ           10013
#define    HLM_EVENTSERVER_RENAMEMAPBYIDA           10014
#define    HLM_EVENTSERVER_GETALLMAPSQ              10015
#define    HLM_EVENTSERVER_GETALLMAPSA              10016
#define    HLM_EVENTSERVER_ADDNEWMAPQ               10017
#define    HLM_EVENTSERVER_ADDNEWMAPA               10018
#define    HLM_EVENTSERVER_DELMAPBYIDQ              10019
#define    HLM_EVENTSERVER_DELMAPBYIDA              10020
#define    HLM_EVENTSERVER_GETMAPDATABYIDQ          10021
#define    HLM_EVENTSERVER_GETMAPDATABYIDA          10022
#define    HLM_EVENTSERVER_EXECUTEHPBUTTONQ         10023
#define    HLM_EVENTSERVER_EXECUTEHPBUTTONA         10024
#define    HLM_EVENTSERVER_ISENABLEDQ               10033
#define    HLM_EVENTSERVER_ISENABLEDA               10034
#define    HLM_EVENTSERVER_SETENABLEDQ              10035
#define    HLM_EVENTSERVER_SETENABLEDA              10036
#define    HLM_EVENTSERVER_ADDDEBUGCLIENTQ          10037
#define    HLM_EVENTSERVER_ADDDEBUGCLIENTA          10038
#define    HLM_EVENTSERVER_REMDEBUGCLIENTQ          10039
#define    HLM_EVENTSERVER_REMDEBUGCLIENTA          10040
#define    HLM_EVENTSERVER_MOVECMDUPINMAPQ          10041
#define    HLM_EVENTSERVER_MOVECMDUPINMAPA          10042
#define    HLM_EVENTSERVER_MOVECMDDNINMAPQ          10043
#define    HLM_EVENTSERVER_MOVECMDDNINMAPA          10044
#define    HLM_EVENTSERVER_GETALLCONDSBYFAMILYQ     10045
#define    HLM_EVENTSERVER_GETALLCONDSBYFAMILYA     10046
#define    HLM_EVENTSERVER_ADDCONDTOMAPBYIDQ        10047
#define    HLM_EVENTSERVER_ADDCONDTOMAPBYIDA        10048
#define    HLM_EVENTSERVER_DELCONDFROMMAPBYIDQ      10049
#define    HLM_EVENTSERVER_DELCONDFROMMAPBYIDA      10050
#define    HLM_EVENTSERVER_EXECMAPBYIDQ             10051
#define    HLM_EVENTSERVER_EXECMAPBYIDA             10052
#define    HLM_EVENTSERVER_EXECTRIGGERNOWQ          10053
#define    HLM_EVENTSERVER_EXECTRIGGERNOWA          10054

// Audio Maps
#define    HLM_EVENTSERVER_GETAMAPDATAQ             10055
#define    HLM_EVENTSERVER_GETAMAPDATAA             10056
#define    HLM_EVENTSERVER_MOVECMDUPINAMAPQ         10057
#define    HLM_EVENTSERVER_MOVECMDUPINAMAPA         10058
#define    HLM_EVENTSERVER_MOVECMDDNINAMAPQ         10059
#define    HLM_EVENTSERVER_MOVECMDDNINAMAPA         10060
#define    HLM_EVENTSERVER_ADDCOMMANDTOAMAPBYIDQ    10061
#define    HLM_EVENTSERVER_ADDCOMMANDTOAMAPBYIDA    10062
#define    HLM_EVENTSERVER_DELCOMMANDFROMAMAPBYIDQ  10063
#define    HLM_EVENTSERVER_DELCOMMANDFROMAMAPBYIDA  10064

#define    HLM_EVENTSERVER_GETCMDOPTSBYIDQ          10070
#define    HLM_EVENTSERVER_GETCMDOPTSBYIDA          10071
#define    HLM_EVENTSERVER_GETCONDOPTSBYIDQ         10072
#define    HLM_EVENTSERVER_GETCONDOPTSBYIDA         10073
#define    HLM_EVENTSERVER_GETEVENTOPTSBYIDQ        10074
#define    HLM_EVENTSERVER_GETEVENTOPTSBYIDA        10075

// Display Maps
#define    HLM_EVENTSERVER_GETDMAPDATAQ             10080
#define    HLM_EVENTSERVER_GETDMAPDATAA             10081
#define    HLM_EVENTSERVER_MOVECMDUPINDMAPQ         10082
#define    HLM_EVENTSERVER_MOVECMDUPINDMAPA         10083
#define    HLM_EVENTSERVER_MOVECMDDNINDMAPQ         10084
#define    HLM_EVENTSERVER_MOVECMDDNINDMAPA         10085
#define    HLM_EVENTSERVER_ADDCOMMANDTODMAPBYIDQ    10086
#define    HLM_EVENTSERVER_ADDCOMMANDTODMAPBYIDA    10087
#define    HLM_EVENTSERVER_DELCOMMANDFROMDMAPBYIDQ  10088
#define    HLM_EVENTSERVER_DELCOMMANDFROMDMAPBYIDA  10089

#define    HLM_EVENTSERVER_EXECUTEHPMOMENTARYQ      10090
#define    HLM_EVENTSERVER_EXECUTEHPMOMENTARYA      10091

// New Config Helpers

#define    HLM_EVENTSERVER_GETALLEVENTSBYIDQ        10100
#define    HLM_EVENTSERVER_GETALLEVENTSBYIDA        10101
#define    HLM_EVENTSERVER_GETALLMAPSBYEVENTIDQ     10102
#define    HLM_EVENTSERVER_GETALLMAPSBYEVENTIDA     10103

#define    HLM_EVENTSERVER_GETALLFTPTRIGGERSQ       10104
#define    HLM_EVENTSERVER_GETALLFTPTRIGGERSA       10105
#define    HLM_EVENTSERVER_ADDFTPTRIGGERQ           10106
#define    HLM_EVENTSERVER_ADDFTPTRIGGERA           10107
#define    HLM_EVENTSERVER_DELFTPTRIGGERQ           10108
#define    HLM_EVENTSERVER_DELFTPTRIGGERA           10109
#define    HLM_EVENTSERVER_SETFTPTRIGGERCONFIGQ     10110
#define    HLM_EVENTSERVER_SETFTPTRIGGERCONFIGA     10111

#define    HLM_EVENTSERVER_MODIFYMAPCOMMANDQ        10200
#define    HLM_EVENTSERVER_MODIFYMAPCOMMANDA        10201
#define    HLM_EVENTSERVER_MODIFYMAPEVENTQ          10202
#define    HLM_EVENTSERVER_MODIFYMAPEVENTA          10203
#define    HLM_EVENTSERVER_MODIFYMAPCONDQ           10204
#define    HLM_EVENTSERVER_MODIFYMAPCONDA           10205

#define    HLM_EVENTSERVER_END                  10205

/**/
/*                                                                    */
/* CWeatherServer Messages                                            */
/*                                                                    */
/**/

#define    HLM_WEATHER_START                    9800

// Gateway/Weather server messages
#define    HLM_WEATHER_GETFORECASTBYZIPQ            9800
#define    HLM_WEATHER_GETFORECASTBYZIPA            9801
#define    HLM_WEATHER_GETFORECASTBYZIPIDQ          9802
#define    HLM_WEATHER_GETFORECASTBYZIPIDA          9803
#define    HLM_WEATHER_GETFORECASTIMAGEZIPINDEXQ    9804
#define    HLM_WEATHER_GETFORECASTIMAGEZIPINDEXA    9805
#define    HLM_WEATHER_GETFORECASTIMAGEDXDYSIZEQ    9820
#define    HLM_WEATHER_GETFORECASTIMAGEDXDYSIZEA    9821
#define    HLM_WEATHER_GETFORECASTCCIMAGEZIPQ       9822
#define    HLM_WEATHER_GETFORECASTCCIMAGEZIPA       9823


// Unsolicited
#define    HLM_WEATHER_FORECASTCHANGED          9806

// Tablet/Gateway message
#define    HLM_WEATHER_GETFORECASTQ                 9807
#define    HLM_WEATHER_GETFORECASTA                 9808
#define    HLM_WEATHER_GETIMAGEBYDAYINDEXQ          9809
#define    HLM_WEATHER_GETIMAGEBYDAYINDEXA          9810
#define    HLM_WEATHER_GETFORECASTDATESTRINGQ       9811
#define    HLM_WEATHER_GETFORECASTDATESTRINGA       9812
#define    HLM_WEATHER_GETCCIMAGEQ                  9813
#define    HLM_WEATHER_GETCCIMAGEA                  9814

#define    HLM_WEATHER_END                      9820

/**/
/*                                                                    */
/* CRadioServer Messages                                              */
/*                                                                    */
/**/

#define    HLM_RADSERVER_START                  9850
#define    HLM_RADSERVER_GETDBINFOQ                 9850
#define    HLM_RADSERVER_GETDBINFOA                 9851
#define    HLM_RADSERVER_GETGENREBYINDEXQ           9852
#define    HLM_RADSERVER_GETGENREBYINDEXA           9853
#define    HLM_RADSERVER_END                    9851

// PentairMon Email Server
#define    HLM_MAILSERV_GETEMAILCONFIGQ             9860
#define    HLM_MAILSERV_GETEMAILCONFIGA             9861
#define    HLM_MAILSERV_SETEMAILCONFIGQ             9862
#define    HLM_MAILSERV_SETEMAILCONFIGA             9863

// Relayserver Lock Manager
#define    HLM_RELAYSERVER_GETALLLOCKCONFIGQ        9880
#define    HLM_RELAYSERVER_GETALLLOCKCONFIGA        9881
#define    HLM_RELAYSERVER_SETLOCKCONFIGQ           9882
#define    HLM_RELAYSERVER_SETLOCKCONFIGA           9883
#define    HLM_RELAYSERVER_GETLOCKMODULEQ           9884
#define    HLM_RELAYSERVER_GETLOCKMODULEA           9885
#define    HLM_RELAYSERVER_SETLOCKUSERINFOQ         9886
#define    HLM_RELAYSERVER_SETLOCKUSERINFOA         9887

/**/
/*                                                                    */
/* CWebServer Messages                                                */
/*                                                                    */
/**/

#define    HLM_WEB_START                        9900
#define    HLM_WEB_GETALLSITESQ                     9900
#define    HLM_WEB_GETALLSITESA                     9901
#define    HLM_WEB_ADDGROUPQ                        9902
#define    HLM_WEB_ADDGROUPA                        9903
#define    HLM_WEB_SETGROUPENAMEBYIDQ               9904
#define    HLM_WEB_SETGROUPENAMEBYIDA               9905
#define    HLM_WEB_DELGROUPBYIDQ                    9906
#define    HLM_WEB_DELGROUPBYIDA                    9907
#define    HLM_WEB_ADDSITETOGROUPBYIDQ              9908
#define    HLM_WEB_ADDSITETOGROUPBYIDA              9909
#define    HLM_WEB_SETSITEINFOBYIDQ                 9910
#define    HLM_WEB_SETSITEINFOBYIDA                 9911
#define    HLM_WEB_DELSITEBYIDQ                     9912
#define    HLM_WEB_DELSITEBYIDA                     9913
#define    HLM_WEB_GETINETCONNECTQ                  9914
#define    HLM_WEB_GETINETCONNECTA                  9915
#define    HLM_WEB_SETINETCONNECTQ                  9916
#define    HLM_WEB_SETINETCONNECTA                  9917
#define    HLM_WEB_ISINETDIALUPCONNECTEDQ           9918
#define    HLM_WEB_ISINETDIALUPCONNECTEDA           9919
#define    HLM_WEB_GETSITEURLBYIDQ                  9920
#define    HLM_WEB_GETSITEURLBYIDA                  9921
#define    HLM_WEB_END                          9921

/**/
/*                                                                    */
/* Email Contact Config Messages                                      */
/*                                                                    */
/**/

#define    HLM_EMAIL_START                      9940
#define    HLM_EMAIL_GETALLMESSAGESQ                9940
#define    HLM_EMAIL_GETALLMESSAGESA                9941
#define    HLM_EMAIL_ADDNEWMESSAGEQ                 9942
#define    HLM_EMAIL_ADDNEWMESSAGEA                 9943
#define    HLM_EMAIL_DELMESSAGEQ                    9944
#define    HLM_EMAIL_DELMESSAGEA                    9945
#define    HLM_EMAIL_CONFIGMESSAGEQ                 9946
#define    HLM_EMAIL_CONFIGMESSAGEA                 9947
#define    HLM_EMAIL_GETGENCONFIGQ                  9950
#define    HLM_EMAIL_GETGENCONFIGA                  9951
#define    HLM_EMAIL_SETGENCONFIGQ                  9952
#define    HLM_EMAIL_SETGENCONFIGA                  9953
#define    HLM_EMAIL_GETALLACOUNTSQ                 9954
#define    HLM_EMAIL_GETALLACOUNTSA                 9955
#define    HLM_EMAIL_SETACCOUNTCONFIGQ              9956
#define    HLM_EMAIL_SETACCOUNTCONFIGA              9957
#define    HLM_EMAIL_ADDNEWACCOUNTQ                 9958
#define    HLM_EMAIL_ADDNEWACCOUNTA                 9959
#define    HLM_EMAIL_DELACCOUNTQ                    9960
#define    HLM_EMAIL_DELACCOUNTA                    9961
#define    HLM_EMAIL_GETACCOUNTLISTQ                9962
#define    HLM_EMAIL_GETACCOUNTLISTA                9963
#define    HLM_EMAIL_GETACCOUNTEMAILQ               9964
#define    HLM_EMAIL_GETACCOUNTEMAILA               9965
#define    HLM_EMAIL_MODIFYACCOUNTEMAILQ            9966
#define    HLM_EMAIL_MODIFYACCOUNTEMAILA            9967
#define    HLM_EMAIL_ACCOUNTSTATUSQ                 9968
#define    HLM_EMAIL_ACCOUNTSTATUSA                 9969
#define    HLM_EMAIL_CHECKACCOUNTNOWQ               9970
#define    HLM_EMAIL_CHECKACCOUNTNOWA               9971
#define    HLM_EMAIL_END                        9971

/**/
/*                                                                    */
/* CTraceServer Messages                                              */
/*                                                                    */
/**/

#define    HLM_TRACESERVER_START                9975
#define    HLM_TRACESERVER_GETCONFIGQ               9975
#define    HLM_TRACESERVER_GETCONFIGA               9976
#define    HLM_TRACESERVER_SETCONFIGQ               9977
#define    HLM_TRACESERVER_SETCONFIGA               9978
#define    HLM_TRACESERVER_ADDCLIENTQ               9979
#define    HLM_TRACESERVER_ADDCLIENTA               9980
#define    HLM_TRACESERVER_REMOVECLIENTQ            9981
#define    HLM_TRACESERVER_REMOVECLIENTA            9982
#define    HLM_TRACESERVER_END                  9982

/**/
/*                                                                    */
/* CHLPlayer Messages                                                 */
/*                                                                    */
/**/

#define    HLM_HLPLAYER_START                   10000

#define    HLM_HLPLAYER_VOLUME                      10000
#define    HLM_HLPLAYER_PLAY                        10001
#define    HLM_HLPLAYER_STOP                        10002
#define    HLM_HLPLAYER_WAVDATA                     10003
#define    HLM_HLPLAYER_MP3DATA                     10004
#define    HLM_HLPLAYER_TRACKINFO                   10005
#define    HLM_HLPLAYER_GETVOLUME                   10006
#define    HLM_HLPLAYER_GETDEVICECONFIGQ            10007
#define    HLM_HLPLAYER_GETDEVICECONFIGA            10008
#define    HLM_HLPLAYER_SETDEVICECONFIGQ            10009
#define    HLM_HLPLAYER_SETDEVICECONFIGA            10010

#define    HLM_HLPLAYER_END                     10010

//
// Hunter Brick Passthrough Messages
// NOTE: These don't go in a regular HLM_STRUCT
//

#define    HLM_HUNTER_GETRAINSENSORQ                10100
#define    HLM_HUNTER_GETRAINSENSORA                10101
#define    HLM_HUNTER_SETZONEWITHTIMEQ              10102
#define    HLM_HUNTER_SETZONEWITHTIMEA              10103
#define    HLM_HUNTER_SETZONEOFFQ                   10104
#define    HLM_HUNTER_SETZONEOFFA                   10105

//
// Serial Brick line control
//
#define    HLM_SERIALBRICK_GETCTS                   10110
#define    HLM_SERIALBRICK_SETRTS                   10111


//
//                                                                    */
// CPictureServer Messages                                            */
//                                                                    */
//

#define    HLM_PICSERVER_START                  11000

// Unsolicited
#define    HLM_PICSERVER_IMAGEDATA                  11000
#define    HLM_PICSERVER_BADIMAGE                   11001
#define    HLM_PICSERVER_WEBPICUPDATE               11002
#define    HLM_PICSERVER_CONTENTSCHANGED            11003

#define    HLM_PICSERVER_GETALLSHARESQ              11010
#define    HLM_PICSERVER_GETALLSHARESA              11011
#define    HLM_PICSERVER_ADDSHAREQ                  11012
#define    HLM_PICSERVER_ADDSHAREA                  11013
#define    HLM_PICSERVER_DELSHAREBYINDEXQ           11014
#define    HLM_PICSERVER_DELSHAREBYINDEXA           11015
#define    HLM_PICSERVER_GETALLALBUMSQ              11016
#define    HLM_PICSERVER_GETALLALBUMSA              11017
#define    HLM_PICSERVER_GETPICBYALBUMQ             11018
#define    HLM_PICSERVER_GETPICBYALBUMA             11019
#define    HLM_PICSERVER_GETUSEROPTSQ               11020
#define    HLM_PICSERVER_GETUSEROPTSA               11021
#define    HLM_PICSERVER_SETUSEROPTSQ               11022
#define    HLM_PICSERVER_SETUSEROPTSA               11023
#define    HLM_PICSERVER_GETVIDEOFILEQ              11024
#define    HLM_PICSERVER_GETVIDEOFILEA              11025
#define    HLM_PICSERVER_GETARTWORKFILEQ            11026
#define    HLM_PICSERVER_GETARTWORKFILEA            11027
#define    HLM_PICSERVER_GETPATHNAMEBYALBUMQ        11028
#define    HLM_PICSERVER_GETPATHNAMEBYALBUMA        11029
#define    HLM_PICSERVER_GETARTWORKFILEDXDYQ        11030
#define    HLM_PICSERVER_GETARTWORKFILEDXDYA        11031
#define    HLM_PICSERVER_GETALLWEBPICSQ             11032
#define    HLM_PICSERVER_GETALLWEBPICSA             11033
#define    HLM_PICSERVER_ADDNEWWEBPICQ              11034
#define    HLM_PICSERVER_ADDNEWWEBPICA              11035
#define    HLM_PICSERVER_DELWEBPICBYIDQ             11036
#define    HLM_PICSERVER_DELWEBPICBYIDA             11037
#define    HLM_PICSERVER_SETWEBPICINFOBYIDQ         11038
#define    HLM_PICSERVER_SETWEBPICINFOBYIDA         11039
#define    HLM_PICSERVER_GETWEBPICBYINDEXQ          11040
#define    HLM_PICSERVER_GETWEBPICBYINDEXA          11041
#define    HLM_PICSERVER_GETWEBPICDXDYBYINDEXQ      11042
#define    HLM_PICSERVER_GETWEBPICDXDYBYINDEXA      11043
#define    HLM_PICSERVER_GETALLIMAGESQ              11044
#define    HLM_PICSERVER_GETALLIMAGESA              11045
#define    HLM_PICSERVER_REFRESHNOWQ                11046
#define    HLM_PICSERVER_REFRESHNOWA                11047
#define    HLM_PICSERVER_GETSLIDEDXDYQ              11048
#define    HLM_PICSERVER_GETSLIDEDXDYA              11049
#define    HLM_PICSERVER_GETWEBPICBYIDQ             11050
#define    HLM_PICSERVER_GETWEBPICBYIDA             11051
#define    HLM_PICSERVER_GETWEBPICDXDYBYIDQ         11052
#define    HLM_PICSERVER_GETWEBPICDXDYBYIDA         11053
#define    HLM_PICSERVER_GETSLIDEDXDYINDEXQ         11054
#define    HLM_PICSERVER_GETSLIDEDXDYINDEXA         11055
#define    HLM_PICSERVER_GETLINKPICBYIDQ            11056
#define    HLM_PICSERVER_GETLINKPICBYIDA            11057
#define    HLM_PICSERVER_CANCELIMAGEQ               11058
#define    HLM_PICSERVER_CANCELIMAGEA               11059

#define    HLM_PICSERVER_END                    11059

/**/
/*                                                                    */
/* Irrigation                                                         */
/*                                                                    */
/**/

#define    HLM_IRRIGATION_START                 12000

// Unsolicited
#define     HLM_IRRIGATION_STATUSCHANGED                12000

#define     HLM_IRRIGATION_GETALLZONESQ                 12001    // Get list off irrigation zones
#define     HLM_IRRIGATION_GETALLZONESA                 12002
#define     HLM_IRRIGATION_GETALLZONESBYDEVICEIDQ       12003    // Get zones controlled by a gigiven device
#define     HLM_IRRIGATION_GETALLZONESBYDEVICEIDA       12004
#define     HLM_IRRIGATION_GETGROUPSQ                   12005    // Get all irrigation zone groups
#define     HLM_IRRIGATION_GETGROUPSA                   12006
#define     HLM_IRRIGATION_ADDZONETOCTLRQ               12007    // Add a new zone to a ir controller
#define     HLM_IRRIGATION_ADDZONETOCTLRA               12008
#define     HLM_IRRIGATION_SETZONEGROUPQ                12009    // Assign a zone to a group (exclusive)
#define     HLM_IRRIGATION_SETZONEGROUPA                12010
#define     HLM_IRRIGATION_DELZONEBYIDQ                 12011    // Delete a zone
#define     HLM_IRRIGATION_DELZONEBYIDA                 12012
#define     HLM_IRRIGATION_ADDNEWGROUPQ                 12013    // Create a new zone group
#define     HLM_IRRIGATION_ADDNEWGROUPA                 12014
#define     HLM_IRRIGATION_DELGROUPBYIDQ                12015    // Delete a zone group
#define     HLM_IRRIGATION_DELGROUPBYIDA                12016
#define     HLM_IRRIGATION_SETZONENAMEQ                 12017    // Set name of zone
#define     HLM_IRRIGATION_SETZONENAMEA                 12018
#define     HLM_IRRIGATION_SETGROUPNAMEQ                12019    // Set name of group
#define     HLM_IRRIGATION_SETGROUPNAMEA                12020
#define     HLM_IRRIGATION_GETPERIODSQ                  12021    // Get all periods in a group
#define     HLM_IRRIGATION_GETPERIODSA                  12022
#define     HLM_IRRIGATION_ADDPERIODQ                   12023    // Add a new period to a group
#define     HLM_IRRIGATION_ADDPERIODA                   12024
#define     HLM_IRRIGATION_DELPERIODBYIDQ               12025    // Delete a period from a group
#define     HLM_IRRIGATION_DELPERIODBYIDA               12026
#define     HLM_IRRIGATION_SETPERIODINFOBYIDQ           12027    // Set time type / name etc
#define     HLM_IRRIGATION_SETPERIODINFOBYIDA           12028
#define     HLM_IRRIGATION_GETZONESBYGROUPIDQ           12029    // Get all zones/times for in a group
#define     HLM_IRRIGATION_GETZONESBYGROUPIDA           12030
#define     HLM_IRRIGATION_SETZONETIMESBYIDQ            12031    // Set the times for a zone
#define     HLM_IRRIGATION_SETZONETIMESBYIDA            12032
#define     HLM_IRRIGATION_SETGROUPDAYSBYIDQ            12033    // Days when group should go
#define     HLM_IRRIGATION_SETGROUPDAYSBYIDA            12034
#define     HLM_IRRIGATION_SETPERIODTIMEBYIDQ           12035    // When does period start in day?
#define     HLM_IRRIGATION_SETPERIODTIMEBYIDA           12036
#define     HLM_IRRIGATION_SETRUNMODEQ                  12037    // Set the system mode on ALL controllers
#define     HLM_IRRIGATION_SETRUNMODEA                  12038
#define     HLM_IRRIGATION_SETOFFMODEQ                  12039    // Set the system mode on ALL controllers
#define     HLM_IRRIGATION_SETOFFMODEA                  12040
#define     HLM_IRRIGATION_SETMANUALMODEBYZONEIDQ       12041    // Start manual mode with this zone for x time
#define     HLM_IRRIGATION_SETMANUALMODEBYZONEIDA       12042
#define     HLM_IRRIGATION_GETALLZONESTATEQ             12043    // Get whole system zone state summary, including names
#define     HLM_IRRIGATION_GETALLZONESTATEA             12044
#define     HLM_IRRIGATION_SETMANUALTIMEBYZONEIDQ       12045    // Set the manual runtime for a zone (may or maynot be running)
#define     HLM_IRRIGATION_SETMANUALTIMEBYZONEIDA       12046
#define     HLM_IRRIGATION_GETGROUPREPORTBYIDQ          12049
#define     HLM_IRRIGATION_GETGROUPREPORTBYIDA          12050
#define     HLM_IRRIGATION_GETGLOBALREPORTQ             12051
#define     HLM_IRRIGATION_GETGLOBALREPORTA             12052
#define     HLM_IRRIGATION_GETMINSUNTILRUNBYGRPIDQ      12055
#define     HLM_IRRIGATION_GETMINSUNTILRUNBYGRPIDA      12056
#define     HLM_IRRIGATION_GETSYSTEMMODEQ               12057
#define     HLM_IRRIGATION_GETSYSTEMMODEA               12058
#define     HLM_IRRIGATION_GETSEASONALADJUSTMENTSQ      12059
#define     HLM_IRRIGATION_GETSEASONALADJUSTMENTSA      12060
#define     HLM_IRRIGATION_SETSEASONALADJUSTMENTQ       12061
#define     HLM_IRRIGATION_SETSEASONALADJUSTMENTA       12062
#define     HLM_IRRIGATION_GETMAXNCTRLSQ                12063
#define     HLM_IRRIGATION_GETMAXNCTRLSA                12064
#define     HLM_IRRIGATION_GETALLGROUPSQ                12065    // Get all irrigation zone groups
#define     HLM_IRRIGATION_GETALLGROUPSA                12066
#define     HLM_IRRIGATION_MOVEZONEBYIDQ                12067
#define     HLM_IRRIGATION_MOVEZONEBYIDA                12068

#define     HLM_IRRIGATION_END                          12068


#define     HLM_POOL_START                      12500

#define     HLM_POOL_STATUSCHANGED                      12500
#define     HLM_POOL_SCHEDULECHANGED                    12501
#define     HLM_POOL_HISTORYDATA                        12502
#define     HLM_POOL_RUNTIMECHANGED                     12503
#define     HLM_POOL_COLORUPDATE                        12504
#define     HLM_POOL_CHEMDATACHANGED                    12505
#define     HLM_POOL_CHEMHISTORYDATA                    12506

#define     HLM_POOL_GETCIRCUITDEFSQ                    12510
#define     HLM_POOL_GETCIRCUITDEFSA                    12511
#define     HLM_POOL_GETCIRCUITINFOBYIDQ                12518
#define     HLM_POOL_GETCIRCUITINFOBYIDA                12519
#define     HLM_POOL_SETCIRCUITINFOBYIDQ                12520
#define     HLM_POOL_SETCIRCUITINFOBYIDA                12521
#define     HLM_POOL_ADDCLIENTQ                         12522
#define     HLM_POOL_ADDCLIENTA                         12523
#define     HLM_POOL_REMOVECLIENTQ                      12524
#define     HLM_POOL_REMOVECLIENTA                      12525
#define     HLM_POOL_GETSTATUSQ                         12526
#define     HLM_POOL_GETSTATUSA                         12527
#define     HLM_POOL_SETHEATSPQ                         12528
#define     HLM_POOL_SETHEATSPA                         12529
#define     HLM_POOL_BUTTONPRESSQ                       12530
#define     HLM_POOL_BUTTONPRESSA                       12531
#define     HLM_POOL_GETCTLRCONFIGQ                     12532
#define     HLM_POOL_GETCTLRCONFIGA                     12533
#define     HLM_POOL_GETHISTORYDATAQ                    12534
#define     HLM_POOL_GETHISTORYDATAA                    12535
#define     HLM_POOL_SETHEATMODEQ                       12538
#define     HLM_POOL_SETHEATMODEA                       12539
#define     HLM_POOL_GETSCHEDULEDATAQ                   12542
#define     HLM_POOL_GETSCHEDULEDATAA                   12543
#define     HLM_POOL_ADDNEWSCHEDEVENTQ                  12544
#define     HLM_POOL_ADDNEWSCHEDEVENTA                  12545
#define     HLM_POOL_DELSCHEDEVENTBYIDQ                 12546
#define     HLM_POOL_DELSCHEDEVENTBYIDA                 12547
#define     HLM_POOL_SETSCHEDEVENTBYIDQ                 12548
#define     HLM_POOL_SETSCHEDEVENTBYIDA                 12549
#define     HLM_POOL_SETCIRCUITRUNTIMEBYIDQ             12550
#define     HLM_POOL_SETCIRCUITRUNTIMEBYIDA             12551
#define     HLM_POOL_CONFIGLIGHTQ                       12554
#define     HLM_POOL_CONFIGLIGHTA                       12555
#define     HLM_POOL_COLORLIGHTSCMDQ                    12556
#define     HLM_POOL_COLORLIGHTSCMDA                    12557
#define     HLM_POOL_GETNCIRCUITNAMESQ                  12558
#define     HLM_POOL_GETNCIRCUITNAMESA                  12559
#define     HLM_POOL_GETCIRCUITNAMESA                   12560
#define     HLM_POOL_GETCIRCUITNAMESQ                   12561
#define     HLM_POOL_GETALLCUSTOMNAMESQ                 12562
#define     HLM_POOL_GETALLCUSTOMNAMESA                 12563
#define     HLM_POOL_SETCUSTOMNAMEQ                     12564
#define     HLM_POOL_SETCUSTOMNAMEA                     12565
#define     HLM_POOL_GETEQUIPCONFIGQ                    12566
#define     HLM_POOL_GETEQUIPCONFIGA                    12567
#define     HLM_POOL_SETEQUIPCONFIGQ                    12568
#define     HLM_POOL_SETEQUIPCONFIGA                    12569
#define     HLM_POOL_SETCALQ                            12570
#define     HLM_POOL_SETCALA                            12571
#define     HLM_POOL_GETSCGCONFIGQ                      12572
#define     HLM_POOL_GETSCGCONFIGA                      12573
#define     HLM_POOL_SETSCGENABLEQ                      12574
#define     HLM_POOL_SETSCGENABLEA                      12575
#define     HLM_POOL_SETSCGCONFIGQ                      12576
#define     HLM_POOL_SETSCGCONFIGA                      12577
#define     HLM_POOL_ENABLEREMOTESQ                     12578
#define     HLM_POOL_ENABLEREMOTESA                     12579
#define     HLM_POOL_CANCELDELAYSQ                      12580
#define     HLM_POOL_CANCELDELAYSA                      12581
#define     HLM_POOL_GETALLERRORSQ                      12582
#define     HLM_POOL_GETALLERRORSA                      12583
#define     HLM_POOL_GETPUMPSTATUSQ                     12584
#define     HLM_POOL_GETPUMPSTATUSA                     12585
#define     HLM_POOL_SETPUMPFLOWQ                       12586
#define     HLM_POOL_SETPUMPFLOWA                       12587
#define     HLM_POOL_RESETHOUSECODEQ                    12588
#define     HLM_POOL_RESETHOUSECODEA                    12589
#define     HLM_POOL_SETCOOLSPQ                         12590
#define     HLM_POOL_SETCOOLSPA                         12591
#define     HLM_POOL_GETCHEMDATAQ                       12592
#define     HLM_POOL_GETCHEMDATAA                       12593
#define     HLM_POOL_SETCHEMDATAQ                       12594
#define     HLM_POOL_SETCHEMDATAA                       12595
#define     HLM_POOL_GETCHEMHISTORYDATAQ                12596
#define     HLM_POOL_GETCHEMISTORYDATAA                 12597

#define     HLM_POOL_END                        12597

/**/
/*                                                                     */
/* Radio                                                               */
/*                                                                     */
/**/

#define     HLM_RADIO_START                     13000

#define     HLM_RADIO_GETFAVGENRELISTQ               13000
#define     HLM_RADIO_GETFAVGENRELISTA               13001
#define     HLM_RADIO_ADDNEWFAVQ                     13002
#define     HLM_RADIO_ADDNEWFAVA                     13003
#define     HLM_RADIO_GETNFAVGENRESQ                 13004
#define     HLM_RADIO_GETNFAVGENRESA                 13005
#define     HLM_RADIO_GETFAVGENREBYINDEXQ            13006
#define     HLM_RADIO_GETFAVGENREBYINDEXA            13007
#define     HLM_RADIO_ADDNEWFAVGENREQ                13008
#define     HLM_RADIO_ADDNEWFAVGENREA                13009
#define     HLM_RADIO_SETSTATIONPLSDATAQ             13010
#define     HLM_RADIO_SETSTATIONPLSDATAA             13011
#define     HLM_RADIO_DELFAVGENREBYNAMEQ             13012
#define     HLM_RADIO_DELFAVGENREBYNAMEA             13013
#define     HLM_RADIO_DELFAVSTATIONBYNAMEQ           13014
#define     HLM_RADIO_DELFAVSTATIONBYNAMEA           13015

#define     HLM_RADIO_END                        13015

#define     HLM_GRAPHPIPE_START                  13100
#define     HLM_GRAPHPIPE_FILLRECT                  13100
#define     HLM_GRAPHPIPE_BLTFROMDIBDC              13101
#define     HLM_GRAPHPIPE_BLTTRANSFROMDIBDC         13102
#define     HLM_GRAPHPIPE_BLTJPEGDATA               13103
#define     HLM_GRAPHPIPE_DRAWLINE                  13104
#define     HLM_GRAPHPIPE_DRAWRECT                  13105
#define     HLM_GRAPHPIPE_MOVEBLT                   13106
#define     HLM_GRAPHPIPE_SCREENSPECS               13107
#define     HLM_GRAPHPIPE_AUDIOSPECS                13108

#define     HLM_GRAPHPIPE_PAINT                     13200
#define     HLM_GRAPHPIPE_MOUSEMESSAGE              13201
#define     HLM_GRAPHPIPE_KEYMESSAGE                13202

#define     HLM_GRAPHPIPE_END                    13210

#define     HLM_AUDIOCLIENT_WAVEFORMAT              13300
#define     HLM_AUDIOCLIENT_AUDIODATA               13301
#define     HLM_AUDIOCLIENT_VOLUME                  13302
#define     HLM_AUDIOCLIENT_PLAYOUT                 13303

//
// File Transfer
//
#define     HLM_FILE_START                      14000

// Unsolicted
#define     HLM_FILEDATAQ                           14000
#define     HLM_FILEDATAA                           14001
#define     HLM_FILE_GETCURDIRQ                     14010
#define     HLM_FILE_GETCURDIRA                     14011
#define     HLM_FILE_GOTODIRBYNAMEQ                 14012
#define     HLM_FILE_GOTODIRBYNAMEA                 14013
#define     HLM_FILE_GETFILEINFOBYINDEXQ            14016
#define     HLM_FILE_GETFILEINFOBYINDEXA            14017
#define     HLM_FILE_FILETOCLIENTBYINDEXQ           14018
#define     HLM_FILE_FILETOCLIENTBYINDEXA           14019
#define     HLM_FILE_FILETOGATEWAYBYNAMEQ           14020
#define     HLM_FILE_FILETOGATEWAYBYNAMEA           14021
#define     HLM_FILE_DELETEFILEBYINDEXQ             14022
#define     HLM_FILE_DELETEFILEBYINDEXA             14023
#define     HLM_FILE_FORMATFLASHFILESYSTEMQ         14024
#define     HLM_FILE_FORMATFLASHFILESYSTEMA         14025
#define     HLM_FILE_CHECKHOMELOGICFOLDERQ          14028
#define     HLM_FILE_CHECKHOMELOGICFOLDERA          14029
#define     HLM_FILE_GETHOMELOGICFILESTATSQ         14030
#define     HLM_FILE_GETHOMELOGICFILESTATSA         14031
#define     HLM_FILE_HOMELOGICFILETOGATEWAYBYNAMEQ  14032
#define     HLM_FILE_HOMELOGICFILETOGATEWAYBYNAMEA  14033
#define     HLM_FILE_INITHISTORYFILESQ              14034
#define     HLM_FILE_INITHISTORYFILESA              14035
#define     HLM_FILE_CREATEFOLDERQ                  14036
#define     HLM_FILE_CREATEFOLDERA                  14037
#define     HLM_FILE_DELETEFOLDERQ                  14038
#define     HLM_FILE_DELETEFOLDERA                  14039
#define     HLM_FILE_GETFLASHQ                      14040
#define     HLM_FILE_GETFLASHA                      14041
#define     HLM_FILE_GETFILESTATSQ                  14042
#define     HLM_FILE_GETFILESTATSA                  14043
#define     HLM_FILE_RENAMEFILEQ                    14044
#define     HLM_FILE_RENAMEFILEA                    14045

#define     HLM_FILE_END                        14045


//
// General IO config
//
#define     HLM_IO_START                        15000

// Unsolicited
#define     HLM_IO_IRCODEUPDATEFULLQ                15000
#define     HLM_IO_GRAPHOBJECTHISTDATA              15001
#define     HLM_IO_OUTPUTSTATECHANGED               15002
#define     HLM_IO_INPUTSTATECHANGED                15003
#define     HLM_IO_IRCODEUPDATEPARTIALQ             15004
#define     HLM_IO_CLIENTGATEWAYEVENT               15005
#define     HLM_IO_CLIENTGATEWAYDATACHANGED         15006

#define     HLM_IO_GETALLIRCODESQ                   15010
#define     HLM_IO_GETALLIRCODESA                   15011
#define     HLM_IO_ADDNEWIRCODEQ                    15012
#define     HLM_IO_ADDNEWIRCODEA                    15013
#define     HLM_IO_DELIRCODEQ                       15014
#define     HLM_IO_DELIRCODEA                       15015
#define     HLM_IO_GETIRCODECONFIGQ                 15016
#define     HLM_IO_GETIRCODECONFIGA                 15017
#define     HLM_IO_SETIRCODECONFIGQ                 15018
#define     HLM_IO_SETIRCODECONFIGA                 15019
#define     HLM_IO_SETIRCODELEARNMODEQ              15020
#define     HLM_IO_SETIRCODELEARNMODEA              15021
#define     HLM_IO_GETALLIOQ                        15024
#define     HLM_IO_GETALLIOA                        15025
#define     HLM_IO_ADDNEWIOQ                        15026
#define     HLM_IO_ADDNEWIOA                        15027
#define     HLM_IO_DELIOIOQ                         15028
#define     HLM_IO_DELIOIOA                         15029
#define     HLM_IO_SETIOCONFIGQ                     15030
#define     HLM_IO_SETIOCONFIGA                     15031
#define     HLM_IO_PASTECCFQ                        15032
#define     HLM_IO_PASTECCFA                        15033
#define     HLM_IO_GETCODECONFIGSQ                  15034
#define     HLM_IO_GETCODECONFIGSA                  15035
#define     HLM_IO_ADDNEWIRDEVICEQ                  15036
#define     HLM_IO_ADDNEWIRDEVICEA                  15037
#define     HLM_IO_SETIRDEVCONFIGQ                  15038
#define     HLM_IO_SETIRDEVCONFIGA                  15039
#define     HLM_IO_DELIRDEVBYIDQ                    15040
#define     HLM_IO_DELIRDEVBYIDA                    15041
#define     HLM_IO_GETNUMERICVALUEQ                 15042
#define     HLM_IO_GETNUMERICVALUEA                 15043
#define     HLM_IO_TESTIRCODEQ                      15044
#define     HLM_IO_TESTIRCODEA                      15045
#define     HLM_IO_GETALLNUMERICSQ                  15046
#define     HLM_IO_GETALLNUMERICSA                  15047
#define     HLM_IO_GETALLNUMERICTRIGGERSQ           15048
#define     HLM_IO_GETALLNUMERICTRIGGERSA           15049
#define     HLM_IO_ADDNEWNUMERICTRIGGERQ            15050
#define     HLM_IO_ADDNEWNUMERICTRIGGERA            15051
#define     HLM_IO_DELNUMERICTRIGGERQ               15052
#define     HLM_IO_DELNUMERICTRIGGERA               15053
#define     HLM_IO_SETNUMERICTRIGCONFIGQ            15054
#define     HLM_IO_SETNUMERICTRIGCONFIGA            15055
#define     HLM_IO_PASTECCFTOCODEQ                  15056
#define     HLM_IO_PASTECCFTOCODEA                  15057
#define     HLM_IO_REMOTEDEVICEINPUTQ               15058
#define     HLM_IO_REMOTEDEVICEINPUTA               15059

#define     HLM_IO_GETALLGRAPHOBJECTSQ              15060
#define     HLM_IO_GETALLGRAPHOBJECTSA              15061
#define     HLM_IO_ADDNEWGRAPHOBJECTQ               15062
#define     HLM_IO_ADDNEWGRAPHOBJECTA               15063
#define     HLM_IO_DELGRAPHOBJECTQ                  15064
#define     HLM_IO_DELGRAPHOBJECTA                  15065
#define     HLM_IO_GETGRAPHOBJECTCONFIGQ            15066
#define     HLM_IO_GETGRAPHOBJECTCONFIGA            15067
#define     HLM_IO_SETGRAPHOBJECTCONFIGQ            15068
#define     HLM_IO_SETGRAPHOBJECTCONFIGA            15069
#define     HLM_IO_GETGRAPHOBJECTHISTORYQ           15070
#define     HLM_IO_GETGRAPHOBJECTHISTORYA           15071

#define     HLM_IO_GETALLSTATEINPUTSQ               15072
#define     HLM_IO_GETALLSTATEINPUTSA               15073

#define     HLM_IO_SETOUTPUTSTATEQ                  15074
#define     HLM_IO_SETOUTPUTSTATEA                  15075
#define     HLM_IO_GETOUTPUTSTATEQ                  15076
#define     HLM_IO_GETOUTPUTSTATEA                  15077
#define     HLM_IO_GETINPUTSTATEQ                   15078
#define     HLM_IO_GETINPUTSTATEA                   15079

#define     HLM_IO_GETALLBACKUPPROCSQ               15090
#define     HLM_IO_GETALLBACKUPPROCSA               15091
#define     HLM_IO_ADDNEWBACKUPPROCQ                15092
#define     HLM_IO_ADDNEWBACKUPPROCA                15093
#define     HLM_IO_DELBACKUPPROCQ                   15094
#define     HLM_IO_DELBACKUPPROCA                   15095
#define     HLM_IO_SETBACKUPPROCCONFIGQ             15096
#define     HLM_IO_SETBACKUPPROCCONFIGA             15097

#define     HLM_IO_ADDSERIALCODEQ                   15100
#define     HLM_IO_ADDSERIALCODEA                   15101
#define     HLM_IO_DELSERIALCODEQ                   15102
#define     HLM_IO_DELSERIALCODEA                   15103
#define     HLM_IO_SETSERIALCONFIGQ                 15104
#define     HLM_IO_SETSERIALCONFIGA                 15105
#define     HLM_IO_GETALLCODESFORDEVICEQ            15106
#define     HLM_IO_GETALLCODESFORDEVICEA            15107

#define     HLM_IO_SIMPLIFYIRCODEQ                  15108
#define     HLM_IO_SIMPLIFYIRCODEA                  15109

#define     HLM_IO_TESTSERIALCODEQ                  15110
#define     HLM_IO_TESTSERIALCODEA                  15111

#define     HLM_IO_GETALLMOXAPORTSQ                 15112
#define     HLM_IO_GETALLMOXAPORTSA                 15113

#define     HLM_IO_GETSERIALDEVCONFIGQ              15150
#define     HLM_IO_GETSERIALDEVCONFIGA              15151
#define     HLM_IO_SETSERIALDEVCONFIGQ              15152
#define     HLM_IO_SETSERIALDEVCONFIGA              15153
#define     HLM_IO_IRCODESETWAVEFORMQ               15154
#define     HLM_IO_IRCODESETWAVEFORMA               15155
#define     HLM_IO_END                          15155


#define     HLM_TABLETSERVER_START              15500
#define     HLM_TABLETSERVER_IRREFLECT              15500
#define     HLM_TABLETSERVER_GETALLTABLETSQ         15501
#define     HLM_TABLETSERVER_GETALLTABLETSA         15502
#define     HLM_TABLETSERVER_GETTABLETOPTSQ         15503
#define     HLM_TABLETSERVER_GETTABLETOPTSA         15504
#define     HLM_TABLETSERVER_GETTABLETOPTSBYIDQ     15505
#define     HLM_TABLETSERVER_GETTABLETOPTSBYIDA     15506
#define     HLM_TABLETSERVER_SETTABLETOPTSBYIDQ     15507
#define     HLM_TABLETSERVER_SETTABLETOPTSBYIDA     15508
#define     HLM_TABLETSERVER_IDTABLETQ              15509
#define     HLM_TABLETSERVER_IDTABLETA              15510
#define     HLM_TABLETSERVER_DELTABLETBYIDQ         15511
#define     HLM_TABLETSERVER_DELTABLETBYIDA         15512
#define     HLM_TABLETSERVER_COPYALLQ               15513
#define     HLM_TABLETSERVER_COPYALLA               15514
#define     HLM_TABLETSERVER_ADDTABLETQ             15515
#define     HLM_TABLETSERVER_ADDTABLETA             15516
#define     HLM_TABLETSERVER_WHATSMYIDQ             15517
#define     HLM_TABLETSERVER_WHATSMYIDA             15518
#define     HLM_TABLETSERVER_GETOBJSQ               15519
#define     HLM_TABLETSERVER_GETOBJSA               15520
#define     HLM_TABLETSERVER_ADDOBJQ                15521
#define     HLM_TABLETSERVER_ADDOBJA                15522
#define     HLM_TABLETSERVER_DELOBJQ                15523
#define     HLM_TABLETSERVER_DELOBJA                15524
#define     HLM_TABLETSERVER_SETOBJDATAQ            15525
#define     HLM_TABLETSERVER_SETOBJDATAA            15526
#define     HLM_TABLETSERVER_REPLACETABLETQ         15527
#define     HLM_TABLETSERVER_REPLACETABLETA         15528
#define     HLM_TABLETSERVER_SETDEVICEDEBUGQ        15529
#define     HLM_TABLETSERVER_SETDEVICEDEBUGA        15530
#define     HLM_TABLETSERVER_GETDEFAULTPAGEQ        15531
#define     HLM_TABLETSERVER_GETDEFAULTPAGEA        15532
#define     HLM_TABLETSERVER_GETEXITOPTIONSQ        15537
#define     HLM_TABLETSERVER_GETEXITOPTIONSA        15538
#define     HLM_TABLETSERVER_RELEASEDISPLAYQ        15539
#define     HLM_TABLETSERVER_RELEASEDISPLAYA        15540
#define     HLM_TABLETSERVER_GETOSDCONFIGQ          15541
#define     HLM_TABLETSERVER_GETOSDCONFIGA          15542
#define     HLM_TABLETSERVER_GETOLEDCONFIGQ         15543
#define     HLM_TABLETSERVER_GETOLEDCONFIGA         15544
#define     HLM_TABLETSERVER_SETOLEDCONFIGQ         15545
#define     HLM_TABLETSERVER_SETOLEDCONFIGA         15546
#define     HLM_TABLETSERVER_SETOSDCONFIGQ          15547
#define     HLM_TABLETSERVER_SETOSDCONFIGA          15548
#define     HLM_TABLETSERVER_OSDSETLOCKQ            15549
#define     HLM_TABLETSERVER_OSDSETLOCKA            15550

#define     HLM_TABLETSERVER_END                15550

#define     HLM_TUNERSERVER_START                   16000
#define     HLM_TUNERSERVER_GETALLTVSTATIONSQ           16000
#define     HLM_TUNERSERVER_GETALLTVSTATIONSA           16001
#define     HLM_TUNERSERVER_GETALLTVSTATIONGROUPSQ      16002
#define     HLM_TUNERSERVER_GETALLTVSTATIONGROUPSA      16003
#define     HLM_TUNERSERVER_ADDNEWTVSTATIONQ            16004
#define     HLM_TUNERSERVER_ADDNEWTVSTATIONA            16005
#define     HLM_TUNERSERVER_ADDNEWTVSTATIONGROUPQ       16006
#define     HLM_TUNERSERVER_ADDNEWTVSTATIONGROUPA       16007
#define     HLM_TUNERSERVER_DELTVSTATIONQ               16008
#define     HLM_TUNERSERVER_DELTVSTATIONA               16009
#define     HLM_TUNERSERVER_DELTVSTATIONGROUPQ          16010
#define     HLM_TUNERSERVER_DELTVSTATIONGROUPA          16011
#define     HLM_TUNERSERVER_SETTVSTATIONCONFIGQ         16012
#define     HLM_TUNERSERVER_SETTVSTATIONCONFIGA         16013
#define     HLM_TUNERSERVER_GETTVSTATIONGROUPCONFIGQ    16014
#define     HLM_TUNERSERVER_GETTVSTATIONGROUPCONFIGA    16015
#define     HLM_TUNERSERVER_SETTVSTATIONGROUPCONFIGQ    16016
#define     HLM_TUNERSERVER_SETTVSTATIONGROUPCONFIGA    16017
#define     HLM_TUNERSERVER_TUNEDEVICETOSTATIONQ        16018
#define     HLM_TUNERSERVER_TUNEDEVICETOSTATIONA        16019
#define     HLM_TUNERSERVER_GETTVGROUPINFOQ             16020
#define     HLM_TUNERSERVER_GETTVGROUPINFOA             16021
#define     HLM_TUNERSERVER_MOVECHANGROUPQ              16022
#define     HLM_TUNERSERVER_MOVECHANGROUPA              16023
#define     HLM_TUNERSERVER_END                     16023

#define     HLM_CALSERV_START                       17000
#define     HLM_CALSERV_GETTAGSFORMONTHQ                17000
#define     HLM_CALSERV_GETTAGSFORMONTHA                17001
#define     HLM_CALSERV_GETASSEMFORDAYQ                 17002
#define     HLM_CALSERV_GETASSEMFORDAYA                 17003
#define     HLM_CALSERV_SETASSEMFORDAYQ                 17004
#define     HLM_CALSERV_SETASSEMFORDAYA                 17005
#define     HLM_CALSERV_END                         17005

#define     HLM_STRINGLIB_START                     17500
#define     HLM_STRINGLIB_GETALLSTRINGSQ                17500
#define     HLM_STRINGLIB_GETALLSTRINGSA                17501
#define     HLM_STRINGLIB_SETALLSTRINGSQ                17502
#define     HLM_STRINGLIB_SETALLSTRINGSA                17503
#define     HLM_STRINGLIB_GETUISTRINGSQ                 17504
#define     HLM_STRINGLIB_GETUISTRINGSA                 17505
#define     HLM_STRINGLIB_GETDEFAULTSTRINGSQ            17506
#define     HLM_STRINGLIB_GETDEFAULTSTRINGSA            17507
#define     HLM_STRINGLIB_END                       17507

//
// SERVERSOFT
//

#define SERVER_NAME_WEATHER                         _T("WeatherServer")
#define SERVER_NAME_RADIO                           _T("RadioServer")

//Fidel's PRODUCTION LOAD BALANCER
#define   SERVER_DISPATCHER_URL_0                     _T("screenlogicserver.pentair.com")//_T("166.78.45.67")
#define   SERVER_DISPATCHER_URL_1                     _T("screenlogicserver.pentair.com")//_T("166.78.45.67")
#define   SERVER_DISPATCHER_DEFAULT_ADDRESS           _T("screenlogicserver.pentair.com")//_T("166.78.45.67")

//Fidel's QA LAOD BALANCER (162.242.142.77)
//#define     SERVER_DISPATCHER_URL_0                     _T("screenlogiclb.pentair.com")//_T("162.242.141.77")
//#define     SERVER_DISPATCHER_URL_1                     _T("screenlogiclb.pentair.com")//_T("162.242.141.77")
//#define     SERVER_DISPATCHER_DEFAULT_ADDRESS           _T("screenlogiclb.pentair.com")//_T("162.242.141.77")

//Production settings  (Andy's)
//#define   SERVER_DISPATCHER_URL_0                     _T("screenlogicserver.pentair.com")
//#define   SERVER_DISPATCHER_URL_1                     _T("hlloginserver.getmyip.com")
//#define	SERVER_DISPATCHER_DEFAULT_ADDRESS           _T("166.78.22.251")

#define SERVER_DISPATCHER_DEFAULT_PORT              500

// Gateway Discovery Service messages
#define     GDISCOVERY_SCHEMA_1                     1
#define     GDISCOVERY_SCHEMA_2                     2 // Relay Server list
#define     GDISCOVERY_SCHEMA_LATEST                2

// Relay Service posts registration request using this interval.
// Discovery service makes the relay inactive, if it haven't
// posted for 2 * interval.
#define     RELAY_REGISTER_POST_INTERVAL_MS         60000 // 1 min

#define     HLM_SERVERSOFT_START                    18000

#define     HLM_SERVERSOFT_ERROR                        18000

#define     HLM_SERVICE_GDISCOVERY_START                18001

#define		HLM_SERVICE_GDISCOVERY_POSTGATEWAYDATA_Q	    18001
#define     HLM_SERVICE_GDISCOVERY_POSTGATEWAYDATA_A	    18002
#define		HLM_SERVICE_GDISCOVERY_GETGATEWAYDATA_Q         18003
#define		HLM_SERVICE_GDISCOVERY_GETGATEWAYDATA_A         18004
#define     HLM_SERVICE_GDISCOVERY_POSTWSERVERDATA_Q        18005
#define     HLM_SERVICE_GDISCOVERY_POSTWSERVERDATA_A        18006
#define     HLM_SERVICE_GDISCOVERY_POSTRADSERVERDATA_Q      18007
#define     HLM_SERVICE_GDISCOVERY_POSTRADSERVERDATA_A      18008
#define     HLM_SERVICE_GDISCOVERY_GETGATEWAYRECORDSET_Q    18009
#define     HLM_SERVICE_GDISCOVERY_GETGATEWAYRECORDSET_A    18010
#define     HLM_SERVICE_GDISCOVERY_GATEWAYRELAYON_Q         18011
#define     HLM_SERVICE_GDISCOVERY_GATEWAYRELAYON_A         18012
#define     HLM_SERVICE_GDISCOVERY_GATEWAYRELAYOFF_Q        18013
#define     HLM_SERVICE_GDISCOVERY_GATEWAYRELAYOFF_A        18014
#define     HLM_SERVICE_GDISCOVERY_UPDATEEXPIRATION_Q       18015
#define     HLM_SERVICE_GDISCOVERY_UPDATEEXPIRATION_A       18016
#define     HLM_SERVICE_GDISCOVERY_RELAYREGISTER_Q			18017
#define     HLM_SERVICE_GDISCOVERY_RELAYREGISTER_A			18018
#define     HLM_SERVICE_GDISCOVERY_RELAYUNREGISTER_Q		18019
#define     HLM_SERVICE_GDISCOVERY_RELAYUNREGISTER_A		18020
#define     HLM_SERVICE_GDISCOVERY_RELAYCONNECTIONOPEN_Q	18021
#define     HLM_SERVICE_GDISCOVERY_RELAYCONNECTIONOPEN_A	18022
#define     HLM_SERVICE_GDISCOVERY_RELAYCONNECTIONCLOSED_Q	18023
#define     HLM_SERVICE_GDISCOVERY_RELAYCONNECTIONCLOSED_A	18024
#define     HLM_SERVICE_GDISCOVERY_RELAYLIST_Q              18025
#define     HLM_SERVICE_GDISCOVERY_RELAYLIST_A              18026
#define     HLM_SERVICE_GDISCOVERY_RELAYUPDATEOPENALL_Q     18027
#define     HLM_SERVICE_GDISCOVERY_RELAYUPDATEOPENALL_A     18028

#define     HLM_SERVICE_GDISCOVERY_END                  18050

// DVD Meta Data Retriever Service messages
#define     DVD_METADATA_SCHEMA_1                   1   // Version 5.0 Build 526
#define     DVD_METADATA_SCHEMA_2                   2   // Version 5.3 Build 11
#define     DVD_METADATA_SCHEMA_LATEST              2

#define     HLM_SERVICE_DVD_METADATA_START              18051
#define     HLM_SERVICE_DVD_METADATA_GETART_Q               18051
#define     HLM_SERVICE_DVD_METADATA_GETART_A               18052
#define     HLM_SERVICE_DVD_METADATA_END                18100

// Internet Audio Service messages
#define     INTERNET_AUDIO_SCHEMA_1                 1   // Version 5.1 Build 35
#define     INTERNET_AUDIO_SCHEMA_LATEST            1

#define     HLM_SERVICE_INTERNET_AUDIO_START            18101

#define     HLM_SERVICE_INTERNET_AUDIO_GET_SESSION_Q                18101
#define     HLM_SERVICE_INTERNET_AUDIO_GET_SESSION_A                18102
#define     HLM_SERVICE_INTERNET_AUDIO_LOGIN_Q                      18103
#define     HLM_SERVICE_INTERNET_AUDIO_LOGIN_A                      18104
#define     HLM_SERVICE_INTERNET_AUDIO_GET_PAGE_Q                   18105
#define     HLM_SERVICE_INTERNET_AUDIO_GET_PAGE_A                   18106
#define     HLM_SERVICE_INTERNET_AUDIO_PLAY_ITEM_Q                  18107
#define     HLM_SERVICE_INTERNET_AUDIO_PLAY_TRACKS_A                18108
#define     HLM_SERVICE_INTERNET_AUDIO_PLAY_STATION_A               18109
#define     HLM_SERVICE_INTERNET_AUDIO_GET_ITEM_IMAGE_Q             18110
#define     HLM_SERVICE_INTERNET_AUDIO_GET_ITEM_IMAGE_A             18111
#define     HLM_SERVICE_INTERNET_AUDIO_ACCOUNT_CHANGED_Q            18112
#define     HLM_SERVICE_INTERNET_AUDIO_ACCOUNT_CHANGED_A            18113

#define     HLM_SERVICE_INTERNET_AUDIO_END              18150

// Key Manager Service messages
#define     KEYMANAGER_SCHEMA_1                     1   // Version 5.1 Build 185
#define     KEYMANAGER_SCHEMA_2                     2   // Version 5.1 Build 311
#define     KEYMANAGER_SCHEMA_3                     3   // Version 5.1 Build 360
#define     KEYMANAGER_SCHEMA_4                     4   // Version 5.1 Build 386
#define     KEYMANAGER_SCHEMA_5                     5   // Version 5.2 Build 580
#define     KEYMANAGER_SCHEMA_LATEST                5

#define     HLM_SERVICE_KEYMANAGER_START                18151

#define     HLM_SERVICE_KEYMANAGER_ACTIVATEKEY_Q                    18151
#define     HLM_SERVICE_KEYMANAGER_ACTIVATEKEY_A                    18152
#define     HLM_SERVICE_KEYMANAGER_KEYINSTALLED_Q                   18153
#define     HLM_SERVICE_KEYMANAGER_KEYINSTALLED_A                   18154
#define     HLM_SERVICE_KEYMANAGER_EXISTINGKEYSDATA_Q               18155
#define     HLM_SERVICE_KEYMANAGER_EXISTINGKEYSDATA_A               18156
#define     HLM_SERVICE_KEYMANAGER_ADDNEWKEYS_Q                     18157
#define     HLM_SERVICE_KEYMANAGER_ADDNEWKEYS_A                     18158
#define     HLM_SERVICE_KEYMANAGER_GETSYSTEMKEYSDATA_Q              18159
#define     HLM_SERVICE_KEYMANAGER_GETSYSTEMKEYSDATA_A              18160
#define     HLM_SERVICE_KEYMANAGER_KEYUNINSTALLED_Q                 18161
#define     HLM_SERVICE_KEYMANAGER_KEYUNINSTALLED_A                 18162
#define     HLM_SERVICE_KEYMANAGER_DEACTIVATEKEY_Q                  18163
#define     HLM_SERVICE_KEYMANAGER_DEACTIVATEKEY_A                  18164
#define     HLM_SERVICE_KEYMANAGER_REGISTERUSER_Q                   18165
#define     HLM_SERVICE_KEYMANAGER_REGISTERUSER_A                   18166
#define     HLM_SERVICE_KEYMANAGER_GETUSERREGISTRATION_Q            18167
#define     HLM_SERVICE_KEYMANAGER_GETUSERREGISTRATION_A            18168
#define     HLM_SERVICE_KEYMANAGER_GETLISTREGIONS_Q                 18169
#define     HLM_SERVICE_KEYMANAGER_GETLISTREGIONS_A                 18170
#define     HLM_SERVICE_KEYMANAGER_GETLISTCOUNTRIES_Q               18171
#define     HLM_SERVICE_KEYMANAGER_GETLISTCOUNTRIES_A               18172
#define     HLM_SERVICE_KEYMANAGER_GETLISTSTATES_Q                  18173
#define     HLM_SERVICE_KEYMANAGER_GETLISTSTATES_A                  18174

#define     HLM_SERVICE_KEYMANAGER_END                  18250

#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_TRACKDATA_Q         18251
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_TRACKDATA_A         18252
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_STATIONDATA_Q       18253
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_GET_EA_TRACK_Q      18254
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_GET_EA_STATION_Q    18255
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_GET_EA_A            18256
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_LOGMETER_Q          18257
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_LOGMETER_A          18258
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_LOGMETER_SKIP_Q     18259
#define     HLM_SERVICE_INTERNET_AUDIO_RHAPSODY_LOGMETER_SKIP_A     18260

#define     HLM_SERVERSOFT_END                      18500

// End SERVERSOFT

// Key Manager
#define     HLM_KEYMANAGER_GETSYSTEMID_Q            18501
#define     HLM_KEYMANAGER_GETSYSTEMID_A            18502

// g!ConnectPro 5.1
#define     HLM_ADD_CLIENT51Q                       18503       // Sent over "broadcast" port
#define     HLM_ADD_CLIENT51A                       18504       // Sent over "broadcast" port

// Last message.
#define     HLM_MESSAGES_LAST                18504

// audio zone controller bit flags
#define     AZCTLR_CANROUTEIRPORTS			0x00000001
#define		AZCTLR_CANSETAUDIOINVALUES		0x00000002
#define     AZCTLR_CANSETMAXZNVOL			0x00000004
#define		AZCTLR_CANSETMINZNTURNONVOL		0x00000008
#define		AZCTLR_CANSETMAXZNTURNONVOL		0x00000010
#define     AZCTLR_CANSETZNPGVOL			0x00000020
#define     AZCTLR_CANSETWHM				0x00000040
#define     AZCTLR_CANSETSRCNAMES			0x00000080
#define     AZCTLR_CANSETLEDBRIGHTNESS		0x00000100
#define     AZCTLR_CANSETLEDTIMEOUT			0x00000200
#define     AZCTLR_CANSETZNGROUPING			0x00000400
#define     AZCTLR_CANSETZNBALANCE			0x00000800
#define     AZCTLR_CANROUTELOCALIRPORTS		0x00001000
#define     AZCTLR_CANSETLOCALAUDIOINVALUES 0x00002000
#define     AZCTLR_CANSETZNPAGEGROUPING		0x00004000
#define     AZCTLR_CANSETVIDEOPROGRAMMING	0x00008000
#define     AZCTLR_CANSETZNDB				0x00010000
#define     AZCTLR_OVERRIDESETVOLUME		0x00020000
#define     AZCTLR_CANSETTRIGGERS           0x00040000

#define		XML_OPEN_TAG_SYMBOL				'<'
#define		XML_CLOSE_TAG_SYMBOL			'>'

////#define		KNX_DATA_TYPE_UNDEFINED			NULL
////#define		KNX_DATA_TYPE_UNDEFINED_NAME	""
////#define		KNX_DATA_TYPE_1BIT				0x1
////#define		KNX_DATA_TYPE_1BIT_NAME			"1 bit"
////#define		KNX_DATA_TYPE_4BIT				0x2
////#define		KNX_DATA_TYPE_4BIT_NAME			"4 bit"
////#define		KNX_DATA_TYPE_1BYTE				0x4
////#define		KNX_DATA_TYPE_1BYTE_NAME		"1 Byte"
////#define		KNX_DATA_TYPE_2BYTE				0x8
////#define		KNX_DATA_TYPE_2BYTE_NAME		"2 Byte"
////#define		KNX_DATA_TYPE_3BYTE				0x10
////#define		KNX_DATA_TYPE_3BYTE_NAME		"3 Byte"
////#define		KNX_DATA_TYPE_4BYTE				0x20
////#define		KNX_DATA_TYPE_4BYTE_NAME		"4 Byte"
////#define		KNX_DATA_TYPE_14BYTE			0x40
////#define		KNX_DATA_TYPE_14BYTE_NAME		"14 Byte"
////
////#define		KNX_PRIORITY_UNDEFINED			NULL
////#define		KNX_PRIORITY_UNDEFINED_NAME		""
////#define		KNX_PRIORITY_ALARM				0x1
////#define		KNX_PRIORITY_ALARM_NAME			"Alarm"
////#define		KNX_PRIORITY_HIGH				0x2
////#define		KNX_PRIORITY_HIGH_NAME			"High"
////#define		KNX_PRIORITY_LOW				0x4
////#define		KNX_PRIORITY_LOW_NAME			"Low"
////
////#define		KNX_FLAGS_UNDEFINED				NULL
////#define		KNX_FLAGS_UNDEFINED_NAME		""
////#define		KNX_FLAGS_ACK					0x1
////#define		KNX_FLAGS_ACK_NAME				"ACK"
////#define		KNX_FLAGS_C						0x2
////#define		KNX_FLAGS_C_NAME				"C"
////#define		KNX_FLAGS_R						0x4
////#define		KNX_FLAGS_R_NAME				"R"
////#define		KNX_FLAGS_W						0x8
////#define		KNX_FLAGS_W_NAME				"W"
////#define		KNX_FLAGS_T						0x10
////#define		KNX_FLAGS_T_NAME				"T"
////#define		KNX_FLAGS_U						0x20
////#define		KNX_FLAGS_U_NAME				"U"
////
////#define		KNX_FLAGS_UPDATE				0x1
////#define		KNX_FLAGS_ADD					0x2
////#define		KNX_FLAGS_DELETE				0x4
////
////#define		KNX_GATEWAY_COM_TECH			0x1
////#define		KNX_GATEWAY_COM_TECH_NAME		"Com Tech"
////
////#define		KNX_GATEWAY_TAPCO				0x2
////#define		KNX_GATEWAY_TAPCO_NAME			"Tapco"

#endif // HLM_H
