//
//  tconnect.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSStatusSink.h"
#import "DBManager.h"
#import "connectionEditor.h"
#import "DLSegmentedControl.h"

#define CONNECTION_STATE_IDLE           0
#define CONNECTION_STATE_WORKING        1
#define CONNECTION_STATE_FAILED         2

/*============================================================================*/

@interface CConnectPage : UIViewController < CNSStatusSink, ConnectionEditorDelegate > 

/*============================================================================*/
{
    IBOutlet UISegmentedControl         *m_pMode;
    IBOutlet UILabel                    *m_pConnectLabel;
    IBOutlet UILabel                    *m_pNameLabel;
    IBOutlet UILabel                    *m_pPassLabel;
    IBOutlet UITextField                *m_pName;
    IBOutlet UITextField                *m_pPass;
    IBOutlet UILabel                    *m_pStatus;
    IBOutlet UILabel                    *m_pVersion;
    IBOutlet UIButton                   *m_pConnectBtn;
    IBOutlet UIActivityIndicatorView    *m_pActivity;
    IBOutlet UIImageView                *m_pLogo;
    NSTimer                             *m_pEnableIOTimer;
    NSTimer                             *m_pConnectTimer;
    NSTimer                             *m_pWiFiTimer;
    int                                 m_iConnectionState;
    int                                 m_iYConnectLabel;
    int                                 m_iYMode;
    int                                 m_iYActivity;
    int                                 m_iYStatus;
    UIInterfaceOrientation              m_Orientation;
    
    IBOutlet UILabel *m_LabelPreviousConnection;
    IBOutlet UIScrollView *m_ConnectionTableScroller;
    ConnectionEditor* editConnectionView;
    NSMutableArray *controllerList;
    
}

-(void)checkEditStatus:(ConnectionEditor *)controller isFinished:(int)status;
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Orientation:(UIInterfaceOrientation) orientation;

-(void)SetStatusString:(NSString*)sStatus;
-(IBAction)RemoteConnect:(id)sender;
-(IBAction)ToggleAuto:(id)sender;

-(void)ConnectThreadProc;
-(void)CheckConnectTimer;
-(void)WiFiTimer;
-(void)EnableIOAfterTimeout;

-(void)LoadSettings;
-(void)SaveSettings;

-(IBAction)GoEvent:(id)sender;

-(void)OffsetView:(UIView*)pView Pos:(int)iY;

-(void)ReleaseAll;

-(UIImage*)scaledImage:(UIImage*)image scaledToSize:(CGSize)newSize;
-(void)segmentButtonHandler:(UISegmentedControl*)segment;
-(void)refreshControllerList;
-(void)addController:(ControllerDescriptor *)cd coounter:(int*)count scrollerHeight:(int*)scrollerHeight width:(float*)dx height:(float*)dy;

@end
