//
//  ArmingMode.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNSQuery;

/*============================================================================*/

@interface CArmingMode : NSObject

/*============================================================================*/
{
    NSString                            *m_pName;
    int                                 m_iMode;
    BOOL                                m_bAutoEntry;
    int                                 m_iKeysEntry;
    BOOL                                m_bVisible;
}
-(id)initFromQ:(CNSQuery*)pQ;

-(NSString*)Name;
-(int)Mode;
-(int)NKeysEntry;
-(BOOL)Visible;

-(BOOL)AutoEntry;

@end