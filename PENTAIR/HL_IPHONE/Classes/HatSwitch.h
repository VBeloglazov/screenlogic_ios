//
//  HatSwitch.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLView.h"
#import "hlbutton.h"
#import "shader.h"

@class CHLButton;
@class CNSShader;
@class CControlDef;

/*============================================================================*/

@interface CPrimitiveSet : NSObject

/*============================================================================*/
{
    int                                 m_iDX;
    int                                 m_iDY;
    int                                 m_iSHIN;
    int                                 m_iSHOUT;

    CNSShader                           *m_pMainShader;
    CNSShader                           *m_pLOut;
    CNSShader                           *m_pROut;
    CNSShader                           *m_pTOut;
    CNSShader                           *m_pBOut;
    CNSShader                           *m_pLWedge;
    CNSShader                           *m_pRWedge;
    CNSShader                           *m_pTWedge;
    CNSShader                           *m_pBWedge;
    CNSShader                           *m_pShaderOut;
    CNSShader                           *m_pIntShader1;
    CNSShader                           *m_pIntShader2;
}

-(id)initWithDX:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT;
-(BOOL)IsSameSet:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT;
-(void)InitializeButton:(CHLButton*)pButton Position:(int)iPosition;
-(void)InitializeSatelliteButton:(CHLButton *)pButton HatSwitch:(UIView*)pThis;
-(CNSShader*)GenIntersectShader:(int)iNShading Padding:(int)iPadding Inset:(int)iShadeInset;

@end

/*============================================================================*/

@interface CHatSwitch : UIView < CHLCommandResponder >

/*============================================================================*/
{
    int                                 m_iData;
    int                                 m_iZoneID;
    CHLButton                           *m_pBtns[12];

    int                                 m_iSHIN;
    int                                 m_iSHOUT;
    UIColor                             *m_pColor;

    CPrimitiveSet                       *m_pPrimitives;
}

-(id)initWithFrame:(CGRect)rFrame 
        Style:(int)iStyle 
        Data:(int)iData 
        Text:(NSString*)pText 
        TextColor:(UIColor*)pRGBText 
        Type:(int)iType 
        Flags:(int)iFlags
        ZoneID:(int)iZoneID
        ControlDef:(CControlDef*)pControlDef;

-(void)InitializeSatelliteButton:(CHLButton*)pButton;

-(int)Radius;

@end
