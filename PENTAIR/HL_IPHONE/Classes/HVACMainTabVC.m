//
//  HVACMainTabViewController.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/29/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "HVACMainTabVC.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "MainView.h"
#import "CustomPage.h"

@implementation CHVACMainTabViewController


/*============================================================================*/

-(id)initWithMainTab:(CMainTab*)pMainTab Background:(UIImageView*)pBackground

/*============================================================================*/
{
    if (self == [super initWithMainTab:pMainTab Background:pBackground])
    {
        [CHLComm AddSocketSink:self];
        [self PostWeather];
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_WEATHER_FORECASTCHANGED:
        [self PostWeather];
        break;
        
    case HLM_WEATHER_GETFORECASTA:
        {
            [pMSG GetInt];      // Version
            [pMSG GetString];   // ZIP

            // Skip over 2 time structures (32 Bytes)
            for (int i = 0; i < 8; i++)
            {
                [pMSG GetInt];
            }

            [pMSG GetString];   // Date Text
            [pMSG GetString];   // Conditions

            int iTemp = [pMSG GetInt];
            if (iTemp == FC_VALUE_UNKNOWN)
                [m_pTempText setText:@""];
            else
            {
                NSString *pText = [NSString stringWithFormat:@"Outside Temperature %d°", iTemp];
                [m_pTempText setText:pText];
            }
        }
        break;
    }
}
/*============================================================================*/

-(void)PostWeather

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_WEATHER_GETFORECASTQ] autorelease];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)InitMain

/*============================================================================*/
{
    [super InitMain];

    CGRect rLabel = CGRectMake(10, self.view.frame.size.height - [CMainView DY_TOOLBAR]-30, self.view.frame.size.width - 20, 25);
    m_pTempText  = [[UILabel alloc] initWithFrame:rLabel];
    [CCustomPage InitStandardLabel:m_pTempText Size:22];

    [m_pTempText setText:@""];

    [self.view addSubview:m_pTempText];
    
}
/*============================================================================*/

-(void)LockHorizontalScroll:(BOOL)bNew

/*============================================================================*/
{
    // Bit of cheat but use this to hide the outside temp for history views
    [m_pTempText setHidden:bNew];
    [super LockHorizontalScroll:bNew];
}
/*============================================================================*/

-(void)UpdateScrollSubViews

/*============================================================================*/
{
    [super UpdateScrollSubViews];
    CGRect rLabel = CGRectMake(10, self.view.frame.size.height - [CMainView DY_TOOLBAR]-30, self.view.frame.size.width - 20, 25);

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) 
    {
        if (self.view.bounds.size.width > self.view.bounds.size.height)
        {
            rLabel.origin.y += [CMainView DY_TOOLBAR];
        }
    }

    [m_pTempText setFrame:rLabel];
}

@end
