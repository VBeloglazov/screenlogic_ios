//
//  MediaKeypadButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "MediaKeypadButton.h"
#import "NSQuery.h"
#import "ControlDef.h"
#import "MainView.h"
#import "CustomPage.h"
#import "hlm.h"


@implementation CMediaKeypadButton

#define CMD_SELF                    1000

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ZoneID:(int)iZoneID ControlDef:(CControlDef*)pDef

/*============================================================================*/
{
    self = [super initWithFrame:rFrame Text:[pDef Text]];
    if (self)
    {
        m_iZoneID   = iZoneID;
        m_iType     = [pDef Type];

        if (m_iType == CONTROL_AUDIO_BUTTON_INTERNAL_ZONE)
        {
            // Style field used for icon
            NSString *pIcon = [CHLButton EmbeddedIconName:[pDef Style]];
            if (pIcon != NULL)
            {
                [self SetIconEmbedded:pIcon];
                [self IconFormat:[pDef TextAlignment]];
            }

        }
        else
        {
            // Style field used for command
            m_iKeypadCmd = [pDef Style];
            NSString *pIcon = [pDef Icon];
            if (pIcon != NULL)
            {
                if (m_iType == CONTROL_AUDIO_BUTTON_INTERNAL_KEYPAD_ABC)
                {
                    m_pSearchText = pIcon;
                    [m_pSearchText retain];
                }
                else
                {
                    if ([pIcon length] > 0)
                    {
                        [self SetIcon:[pDef Icon]];
                        [self IconFormat:[pDef TextAlignment]];
                    }
                }
            }
        }

        if ([pDef Function] != 0)
            [self SetChecked:YES];

        int iFlags = [pDef Flags];
        if ((iFlags & CONTROL_FLAG_DEFAULT_TEXT_RGB) == 0)
            [self SetTextColor:[pDef RGBText]];
        if ((iFlags & CONTROL_FLAG_DEFAULT_TEXT_SIZE) == 0)
            [self SetTextSize:[pDef TextSize]];

        if ((iFlags & CONTROL_FLAG_DEFAULT_FACE_RGB) == 0)
        {
            [self SetColor:[pDef RGBFace]];
            [self InitShader];
        }


        [m_pLabel setLineBreakMode:UILineBreakModeTailTruncation];
        [m_pLabel setNumberOfLines:1];

        m_iID = [pDef Data];
        self.autoresizingMask = 0xFFFFFFFF;

        [self SetCommandID:CMD_SELF Responder:self];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pTextView removeFromSuperview];
    [m_pTextView release];
    [m_pSearchText release];
    [m_pABCTimer invalidate];
    [m_pABCTimer release];
    [super dealloc];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (iCommandID != CMD_SELF)
        return;

    if (m_iType == CONTROL_AUDIO_BUTTON_INTERNAL_KEYPAD_ABC)
    {
        UIView *pSuperview = [self superview];
        CGRect rect = pSuperview.bounds;
        m_pTextView = [[UIView alloc] initWithFrame:rect];
        m_pTextView.backgroundColor = [UIColor blackColor];
        UIFont *pFont = [UIFont boldSystemFontOfSize:[CMainView DEFAULT_TEXT_SIZE]];
        CGRect rView  = CGRectMake(2, 2, rect.size.width-4, [CMainView DEFAULT_TEXT_SIZE] + 8);
        UILabel *pLabel = [[UILabel alloc] initWithFrame:rView];
        [m_pTextView addSubview:pLabel];
        [CCustomPage InitStandardLabel:pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        [pLabel release];
        [pLabel setText:m_pSearchText];
        rView.origin.y += rView.size.height;
    

        UITextField *pTextField = [[UITextField alloc] initWithFrame:rView];
        pTextField.textColor = [UIColor blackColor];
        pTextField.font = pFont;
        pTextField.backgroundColor = [UIColor clearColor];
        pTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [pTextField setTextAlignment:UITextAlignmentLeft];
        [pTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [pTextField setReturnKeyType:UIReturnKeySearch];
        [pTextField addTarget:self action:@selector(SearchEvent:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [m_pTextView addSubview:pTextField];
        [pTextField release];

        [pSuperview addSubview:m_pTextView];
        [pTextField becomeFirstResponder];

        [m_pABCTimer invalidate];
        [m_pABCTimer release];
        
        m_pABCTimer = [[NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(CancelABC) userInfo:nil repeats:NO] retain];

        return;
    }

    if (m_iType == CONTROL_AUDIO_BUTTON_INTERNAL_ZONE)
    {
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_CUSTOMZONECOMMANDQ] autorelease];
        [pQ PutInt:m_iZoneID];
        [pQ PutInt:m_iID];
        [pQ SendMessageWithMyID:self];
        return;
    }

    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_CUSTOMKEYPADCOMMANDQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:m_iKeypadCmd];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)CancelABC

/*============================================================================*/
{
    if (m_pTextView != NULL)
    {
        [m_pTextView removeFromSuperview];
        [m_pTextView release];
        m_pTextView = NULL;
    }

    [m_pABCTimer release];
    m_pABCTimer = NULL;
}
/*============================================================================*/

-(IBAction)SearchEvent:(id)sender

/*============================================================================*/
{
    [m_pABCTimer invalidate];
    [m_pABCTimer release];
    m_pABCTimer = NULL;

    UITextField *pTextField = (UITextField*)sender;
    NSString *pText = pTextField.text;

    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_CUSTOMKEYPADCOMMAND_STRINGQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:m_iKeypadCmd];
    [pQ PutString:pText];
    [pQ SendMessageWithMyID:self];


    if (m_pTextView != NULL)
    {
        [m_pTextView removeFromSuperview];
        [m_pTextView release];
        m_pTextView = NULL;
    }
}

@end
