//
//  TVChannelsView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "TVChannelsView.h"
#import "NSMSG.h"
#import "NSQuery.h"
#import "HLComm.h"
#import "TableTitleHeaderView.h"
#import "TunerFavsPage.h"
#import "IconButton.h"
#import "iconview.h"
#import "MediaIconView.h"
#import "HLTableFrame.h"
#import "CustomPage.h"

#define TABLE_ROW_HEIGHT            44
#define EDGE_SIZE                   2
#define DX_ICON                     80
#define DY_ICON                     60
#define DX_INDEX_IPAD               80
#define DX_INDEX_IPOD               40
#define DY_CELL_BUFFER              10

#define MAX_CHARS_TITLE             18

@implementation CTVChannelGroup

/*============================================================================*/

-(id)initWithID:(int)iID DeviceID:(int)iDeviceID Name:(NSString*)pName View:(CTVChannelsView*)pView

/*============================================================================*/
{
    m_pView = pView;
    m_iID = iID;
    m_iDeviceID = iDeviceID;

    m_pName = pName;
    [m_pName retain];
    m_pStations = [[NSMutableArray alloc] init];

    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_TUNERSERVER_GETTVGROUPINFOQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:m_iDeviceID];
    [pQ SendMessageWithMyID:self];
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pName release];
    [m_pStations release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_TUNERSERVER_GETTVGROUPINFOA)
        return;

    [m_pStations removeAllObjects];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        int iID = [pMSG GetInt];
        NSString *pIcon = [pMSG GetStringNoFilter];
        NSString *pName = [pMSG GetString];
        CStation *pS = [[CStation alloc] initWithID:iID Icon:pIcon Name:pName];
        [m_pStations addObject:pS];
        [pS release];
    }

        
    [CHLComm RemoveSocketSink:self];

    [m_pView GroupLoaded];
}
/*============================================================================*/

-(int)ID                { return m_iID;                 }
-(NSString*)Name        { return m_pName;               }
-(int)NStations         { return [m_pStations count];   }

/*============================================================================*/

-(CStation*)Station:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pStations count])
        return NULL;
    CStation *pS = (CStation*)[m_pStations objectAtIndex:iIndex];
    return pS;
}

@end


@implementation CChannelCell

/*============================================================================*/

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)identifier DXIndex:(int)iDXIndex

/*============================================================================*/
{
	self = [super initWithStyle:style reuseIdentifier:identifier];

	if (self)
	{
		// you can do this here specifically or at the table level for all cells
        m_iDXIndex = iDXIndex;
		self.accessoryType = UITableViewCellAccessoryNone;
        m_pButtons = [[NSMutableArray alloc] init];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
	
	return self;
}
/*============================================================================*/

-(void)SetGroup:(CTVChannelGroup*)pGroup Section:(int)iSection Row:(int)iRow View:(CTVChannelsView*)pView

/*============================================================================*/
{
    if (pGroup == m_pGroup && [m_pButtons count] == [pGroup NStations])
        return;
    m_pGroup = pGroup;
    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CMediaIconView *pIconView = (CMediaIconView*)[m_pButtons objectAtIndex:i];
        [pIconView removeFromSuperview];
    }
    [m_pButtons removeAllObjects];
    for (int i = 0; i< [pGroup NStations]; i++)
    {
        CStation *pStation = [pGroup Station:i];
        CMediaIconView *pIconView = [[CMediaIconView alloc] init];
        [pIconView SetUserIcon:[pStation IconName]];
        [self.contentView addSubview:pIconView];
        [m_pButtons addObject:pIconView];
        [pIconView SetView:self Index:i];
        [pIconView release];
    }

    m_iSection = iSection;
    m_iRow = iRow;
    m_pView = pView;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];
    CGRect contentRect = self.frame;

    int iCol = 0;
    int iRow = 0;
    int iMaxCols = (contentRect.size.width - m_iDXIndex) / DX_ICON;
    int iPadLeft = 0;
    if (m_iDXIndex <= 0)
    {
        iPadLeft = (contentRect.size.width - iMaxCols * DX_ICON) / 2;
    }

    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CGRect rIcon = CGRectMake(iPadLeft + iCol * DX_ICON+5, iRow * DY_ICON + DY_CELL_BUFFER, DX_ICON-2, DY_ICON-2);
        CMediaIconView *pIconView = (CMediaIconView*)[m_pButtons objectAtIndex:i];
        [pIconView setFrame:rIcon];
        iCol++;
        if (iCol >= iMaxCols)
        {
            iRow++;
            iCol = 0;
        }
    }
}
/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated

/*============================================================================*/
{
    [super setSelected:FALSE animated:NO];
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pButtons release];
    [super dealloc];
}
/*============================================================================*/

-(void)SelectChannel:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pButtons count])
        return;


    CStation *pS = [m_pGroup Station:iIndex];
    CMediaIconView *pView = (CMediaIconView*)[m_pButtons objectAtIndex:iIndex];
    CGRect rBtn = pView.frame;
    [m_pView SetSelectedChannel:pS Section:m_iSection Cell:self Rect:rBtn];
}

@end


@implementation CTVChannelsView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Data:(int)iData Style:(int)iStyle

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame]) 
    {
        // Initialization code
        m_iData = iData;
        m_iStyle = iStyle;

        self.backgroundColor = [UIColor clearColor];

        m_pGroups = [[NSMutableArray alloc] init];
        m_pSectionTitles = [[NSMutableArray alloc] init];
        m_pSectionTitlesTruncated = [[NSMutableArray alloc] init];
    
        NSString *psTitle = @"Channels";
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
            psTitle = NULL;

        CGRect rTable = self.bounds;
//        m_pTableView = [[CHLTableFrame alloc] initWithFrame:rTable Title:psTitle DataSource:self Delegate:self];
        m_pTableView = [[CHLTableFrame alloc] initWithFrame:rTable DataSource:self Delegate:self];
        [m_pTableView setAutoresizingMask:0xFFFFFFFF];
        [[m_pTableView TableView] setTag:999];
        [[m_pTableView TableView] setAllowsSelection:NO];
        
        // Add rects on sides and bottom of the table view
        CGRect rL = rTable;
        rL.size.width = EDGE_SIZE;
        CGRect rR = rL;
        rR.origin.x = rTable.size.width - EDGE_SIZE;
        CGRect rB = rTable;
        rB.origin.y = rTable.size.height - EDGE_SIZE;
        rB.size.height = EDGE_SIZE;

        // set the tableview as the controller view
        [self addSubview:m_pTableView];

        [self AddRect:rL];
        [self AddRect:rR];
        [self AddRect:rB];
        

        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_TUNERSERVER_GETALLTVSTATIONGROUPSQ];
        [pQ autorelease];
        [pQ PutInt:iData];
        [pQ SendMessageWithMyID:self];
    }
    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    // Drawing code
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pTableView release];
    [m_pGroups release];
    [m_pSectionTitles release];
    [m_pSectionTitlesTruncated release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_TUNERSERVER_GETALLTVSTATIONGROUPSA)
        return;

    [m_pGroups removeAllObjects];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        int iID = [pMSG GetInt];
        NSString *pName = [pMSG GetString];
        CTVChannelGroup *pG = [[CTVChannelGroup alloc] initWithID:iID DeviceID:m_iData Name:pName View:self];
        [m_pGroups addObject:pG];
        [m_pSectionTitles addObject:pName];
        if (iN == 1)
            [m_pSectionTitlesTruncated addObject:@""];
        else
            [m_pSectionTitlesTruncated addObject:[self TruncateGenreTitle:pName]];
        [pG release];
    }

        
    [CHLComm RemoveSocketSink:self];

    [m_pTableView ReloadData];
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return MAX(1, [m_pGroups count]);
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
}
/*============================================================================*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    if (indexPath.section >= [m_pGroups count])
        return 10;

    CTVChannelGroup *pGroup = (CTVChannelGroup*)[m_pGroups objectAtIndex:indexPath.section];
    int iNStations = [pGroup NStations];
    int iDXIndex = DX_INDEX_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDXIndex = DX_INDEX_IPAD;

    if ([m_pGroups count] <= 1)
        iDXIndex = 0;
    int iMaxCols = MAX(1, (tableView.frame.size.width - iDXIndex) / DX_ICON);
    int iNRows = 1;
    while (iNRows * iMaxCols < iNStations)
        iNRows++;
    return iNRows * DY_ICON + 2 * DY_CELL_BUFFER;
    
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    if (section >= [m_pGroups count])
        return 0;
    return 1;
}
/*============================================================================*/

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView 

/*============================================================================*/
{
	// returns the array of section titles. There is one entry for each unique character that an element begins with
	// [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    return m_pSectionTitlesTruncated;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index 

/*============================================================================*/
{
	return index;
}
/*============================================================================*/

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 

/*============================================================================*/
{
    if (section >= [m_pSectionTitles count])
        return @"";

    NSString *pS = (NSString*)[m_pSectionTitles objectAtIndex:section];
    return pS;
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    CTVChannelGroup *pGroup = (CTVChannelGroup*)[m_pGroups objectAtIndex:indexPath.section];

    int iDXIndex = DX_INDEX_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDXIndex = DX_INDEX_IPAD;

    if ([m_pGroups count] <= 1)
        iDXIndex = 0;

    NSString *CellIdentifier = [NSString stringWithFormat:@"GROUP%d", [pGroup ID]];
    CChannelCell *cell = (CChannelCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[CChannelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier DXIndex:iDXIndex] autorelease];
    }

    [cell SetGroup:pGroup Section:indexPath.section Row:indexPath.row View:self];

    return cell;
}
/*============================================================================*/

-(void)GroupLoaded

/*============================================================================*/
{
    [m_pTableView ReloadData];
}
/*============================================================================*/

-(void)SetSelectedChannel:(CStation*)pStation Section:(int)iSection Cell:(UITableViewCell*)pCell Rect:(CGRect)rBtn

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_TUNERSERVER_TUNEDEVICETOSTATIONQ];
    [pQ autorelease];
    [pQ PutInt:m_iData];
    [pQ PutInt:[pStation ID]];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)AddRect:(CGRect)rRect

/*============================================================================*/
{
    UIView *pView = [[UIView alloc] initWithFrame:rRect];
    pView.backgroundColor = [UIColor darkGrayColor];
    [self addSubview:pView];
    pView.autoresizingMask = 0xFFFFFFFF;
    [pView release];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bNew

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(NSString*)TruncateGenreTitle:(NSString*)pName

/*============================================================================*/
{
    int iLen = [pName length];
    if (iLen < MAX_CHARS_TITLE)
        return pName;
    const char *pChars = [pName UTF8String];
    int iChopIndex = MAX_CHARS_TITLE;
    for (int iChop = MAX_CHARS_TITLE-1; iChop > 5 && (iChopIndex == MAX_CHARS_TITLE); iChop--)
    {
        if (pChars[iChop] == ' ')
            iChopIndex = iChop;
    }
    NSString *pNew = [pName substringToIndex:iChopIndex-1];
    NSString *pRet = [NSString stringWithFormat:@"%s...", [pNew UTF8String]];
    return pRet;
}

@end
