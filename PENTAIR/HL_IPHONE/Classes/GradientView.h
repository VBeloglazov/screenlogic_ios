//
//  GradientView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/28/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>


/*============================================================================*/

@interface CGradientView : UIView

/*============================================================================*/
{
    BOOL                        m_bSelected;
}

-(id)initWithFrame:(CGRect)rect Selected:(BOOL) bSelected;

@end
