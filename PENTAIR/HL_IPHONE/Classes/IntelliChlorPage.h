//
//  IntelliChlorPage.h
//  XPAD_VIEW
//
//  Created by dmitriy on 9/1/14.
//
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import "NSSocketSink.h"
#import "Sink.h"

@class CNSPoolConfig;
@class CHLButton;

/*============================================================================*/

@interface CStatusItem2 : UIView

/*============================================================================*/
{
    UILabel                         *m_pLabel;
    UILabel                         *m_pVal1;
    UILabel                         *m_pVal2;
    UIProgressView                  *m_pProgress;
}

-(id)initWithFrame:(CGRect) rFrame Label:(NSString*)pLabel HasSubValue:(BOOL)bSubValue HasLevel:(BOOL)bHasLevel;
-(UILabel*)AddLabel:(NSString*)pText Size:(int)iSize;
-(void)SetData:(NSString*)pText1;
-(void)SetColor:(UIColor*)color;
-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2;
-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2 Level:(int)iLevel;
-(CGRect)getLabelFrame;
-(CGRect)getValFrame;

@end

/*============================================================================*/

@interface CIntelliChlorPage : CSubSystemPage < CSink >

/*============================================================================*/
{
    CNSPoolConfig                   *m_pConfig;
    NSTimer                         *m_pQueryTimer;
    
    CStatusItem2                    *m_pPoolOutput;
    CStatusItem2                    *m_pSpaOutput;
    CStatusItem2                    *m_pSalt;
    
    UILabel                         *m_pICControl1;
    UILabel                         *m_pICControl2;
    
    UILabel                         *m_pICControl1_Line2;
    UILabel                         *m_pICControl2_Line2;
    
    BOOL                            isChlorinatorPresent;
    BOOL                            showValues;
    
    int                             poolRealOutput;
    int                             spaRealOutput;
    int                             saltLevel;
    
    int                             poolLevel;
    int                             spaLevel;
    int                             bSuper;
    int                             iSuper;
    
    CHLButton                       *buttonPoolUp;
    CHLButton                       *buttonPoolDn;
    CHLButton                       *buttonSpaUp;
    CHLButton                       *buttonSpaDn;
    
    int                             poolFeedbackCount;
    int                             spaFeedbackCount;
    BOOL                            isPoolClick;
    BOOL                            isSpaClick;

}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig IsActive:(BOOL)isActive;
-(void)QueryChlorData;

@end
