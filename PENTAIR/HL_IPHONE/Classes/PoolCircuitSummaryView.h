//
//  PoolCircuitSummaryView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/13/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundRegion.h"
#import "Sink.h"

@class CNSPoolConfig;
@class CNSPoolCircuit;


/*============================================================================*/

@interface CPoolCircuitIndicatorView : UIView

/*============================================================================*/
{
    CNSPoolCircuit                  *m_pCircuit;

    UILabel                         *m_pLabel;
    int                             m_iState;
}

-(id)initWithFrame:(CGRect)frame Circuit:(CNSPoolCircuit*)pCircuit;
-(void)Update;

@end

/*============================================================================*/

@interface CPoolCircuitSummaryView : CRoundRegion <CSink>

/*============================================================================*/
{
    CNSPoolConfig                   *m_pConfig;
    NSMutableArray                  *m_pControls;
}

-(void)MoveFirst:(NSMutableArray*)pList CircuitType:(int)iType;

@end
