//
//  tmp3.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/1/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainView.h"
#import "TabRoundRegion.h"

#import "sink.h"

@class CAudioZone;
@class CMediaZonePage;
@class CPlaylistView;
@class CNowPlayingPage;
@class CMP3ContentView;


#define                             TRANSPORT_PLAY  0x00000001
#define                             TRANSPORT_STOP  0x00000002
#define                             TRANSPORT_PAUSE 0x00000004
#define                             TRANSPORT_PREV  0x00000008
#define                             TRANSPORT_NEXT  0x00000010
#define                             TRANSPORT_LIB   0x00000020

/*============================================================================*/

@interface CMP3InfoPage : CTabRoundRegion

/*============================================================================*/
{
    int                                 m_iPageMap[32];
    UIView                              *m_pPages[32];
    int                                 m_iNPageMap;

    CAudioZone                          *m_pZone;
}

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone ZonePage:(CMediaZonePage*)pZonePage;
-(void)AddItem:(NSMutableArray*)pList Text:(NSString*)pText Icon:(NSString*)pIcon Data:(int)iData;

+(int)BUTTON_SIZE;
+(int)TRANSPORT_INSET;
+(int)SPACING;

@end
