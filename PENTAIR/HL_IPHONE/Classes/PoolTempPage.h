//
//  PoolTempPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "hlview.h"

@class CNSPoolConfig;
@class CPoolTempCtrlView;
@class CHLSegment;
@class CButtonBar;
@class CButtonBarButton;

/*============================================================================*/

@interface CPoolTempPage : UIView <CSink>

/*============================================================================*/
{
    CPoolTempCtrlView               *m_pTempCtrl;
    CButtonBar                      *m_pMode;
    CButtonBarButton                *m_pModeOff;
    CButtonBarButton                *m_pModeHeat;
    CButtonBarButton                *m_pModeSolar;
    CButtonBarButton                *m_pModeSolarPref;
    
    NSMutableArray                  *m_pCircuitButtons;

    int                             m_iType;
    CNSPoolConfig                   *m_pConfig;

    #ifdef PENTAIR
        UILabel                     *m_pTempText;
    #endif

}

-(id)initWithFrame:(CGRect) rFrame Type:(int)iType Config:(CNSPoolConfig*)pConfig;
-(void)InitModeControl;


@end
