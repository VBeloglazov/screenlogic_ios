//
//  iconview.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "iconview.h"
#import "ImageServer.h"
#import "icon.h"
#import "hlm.h"

@implementation CMainTabIconView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
	self = [super initWithFrame:frame];
    if (self)
    {
        m_pImage = NULL;
		// Initialization code
	}
	return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    if (m_pImage == NULL)
        return;

    int iDX = CGRectGetWidth(rect);
    int iDY = CGRectGetHeight(rect);
    int iDXImage = m_pImage.size.width;
    int iDYImage = m_pImage.size.height;
    int iDYDXOut = iDY * 100 / iDX;
    int iDYDXImage = iDYImage * 100 / iDXImage;
    int iDXOut = iDX;
    int iDYOut = iDY;
    
    if (iDYDXImage > iDYDXOut)
    {
        iDXOut = iDY * iDXImage / iDYImage;
    }
    else
    {
        iDYOut = iDX * iDYImage / iDXImage;
    }

    int iXOut = CGRectGetMidX(rect) - iDXOut / 2;
    int iYOut = CGRectGetMidY(rect) - iDYOut / 2;

    CGRect rOut = CGRectMake(iXOut+2, iYOut+2, iDXOut-4, iDYOut-4);

    [m_pImage drawInRect:rOut];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pImage release];
	[super dealloc];
}
/*============================================================================*/

-(void)SetImage:(UIImage*) pNew;

/*============================================================================*/
{
    if (pNew == m_pImage)
        return;
    [m_pImage release];
    m_pImage = pNew;
    [m_pImage retain];
}


@end


@implementation CUserTabIconView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
	self = [super initWithFrame:frame];
    if (self)
    {
		// Initialization code
        m_pImage = NULL;
        [self setUserInteractionEnabled:FALSE];
        self.contentMode = UIViewContentModeScaleAspectFit;
	}
	return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pImage release];
    [m_pIconName release];
	[super dealloc];
}
/*============================================================================*/

-(void)SetUserIcon:(NSString*) pNew

/*============================================================================*/
{
    [m_pImage release];
    m_pImage = NULL;

    [m_pIconName release];
    m_pIconName = pNew;
    [m_pIconName retain];
    
    [self InitImage];
}
/*============================================================================*/

-(void)SetEmbeddedIcon:(NSString*) pNew

/*============================================================================*/
{
    [m_pImage release];
    m_pImage = [UIImage imageNamed:pNew];
    [m_pImage retain];

    [m_pIconName release];
    m_pIconName = pNew;
    [m_pIconName retain];
    
    UIImage *pImage = [UIImage imageNamed:pNew];
    [self setImage:pImage];
}
/*============================================================================*/

-(void)SetOptions:(int)iOptions

/*============================================================================*/
{
    m_iOptions = iOptions;
}
/*============================================================================*/

-(void)InitImage

/*============================================================================*/
{
    if (m_pImage != NULL)
    {
        return;
    }

    CIcon *pIcon = [CImageServer IconByName:m_pIconName];
    CGImageRef pIconImage = [pIcon GetImage:self];

    if (pIconImage == NULL)
        return;

    if ((([pIcon SourceType] == ENCODE_JPEG) || ([pIcon SourceType] == ENCODE_GIF)) && (m_iOptions & ICONVIEW_TVICON))
    {
        pIconImage = [pIcon GetImageVirtualAlpha];
        if (pIconImage == NULL)
            return;
    }

    CGImageRef pRef = CGImageCreateCopy(pIconImage);
    m_pImage = [UIImage imageWithCGImage:pRef];
    [m_pImage retain];
    [self setImage:m_pImage];
}
/*============================================================================*/

-(void)NotifyIconReady

/*============================================================================*/
{
    [self InitImage];
}

@end



@implementation CAccesoryView

/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    CGContextRef ref = UIGraphicsGetCurrentContext();

    int iYMid = rect.origin.y + rect.size.height / 2;
    int iDim = MIN(4, rect.size.height * 4 / 10);
    CGPoint pts[4];
    pts[0] = CGPointMake(0, iYMid-iDim);
    pts[1] = CGPointMake(iDim, iYMid);
    pts[2] = CGPointMake(iDim, iYMid);
    pts[3] = CGPointMake(0, iYMid+iDim);
    
    CGContextAddLines(ref, pts, 3);
    CGContextSetLineWidth(ref, 3);
    CGContextSetLineCap(ref,kCGLineCapRound);
    CGContextSetRGBStrokeColor(ref, 0.30, 0.30, 0.30, 1.0);
    CGContextStrokeLineSegments(ref, pts, 3);
}

@end

