//
//  IntelliChemPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/9/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "IntelliChemPage.h"
#import "crect.h"
#import "MainView.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "NSMSG.h"
#import "NSPoolConfig.h"
#import "CustomPage.h"


@implementation CStatusItem


/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame Label:(NSString*)pLabel HasSubValue:(BOOL)bSubValue HasLevel:(BOOL)bHasLevel

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self) 
    {
        int iSize = 32;

        m_pLabel = [self AddLabel:pLabel Size:iSize];
        m_pLabel.numberOfLines = 2;
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;

        m_pVal1  = [self AddLabel:@"" Size:48];
        m_pVal1.alpha = .6;
        if (bSubValue)
        {
            m_pVal2  = [self AddLabel:@"" Size:[CMainView DEFAULT_TEXT_SIZE]];
            m_pVal2.alpha = .6;
        }

        if (bHasLevel)
        {
            m_pProgress  = [[UIProgressView alloc] initWithFrame:CGRectZero];
            [self addSubview:m_pProgress];
            [m_pProgress release];
        }

        m_pLabel.textAlignment = UITextAlignmentLeft;
       
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(UILabel*)AddLabel:(NSString*)pText Size:(int)iSize

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [CCustomPage InitStandardLabel:pLabel Size:iSize];
    [pLabel setText:pText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    CGRect rLabel = rBounds;
    [CRect BreakOffRight:&rLabel DX:100];
    [CRect BreakOffLeft:&rBounds DX:rBounds.size.width * 50 / 100];
    [m_pLabel setFrame:rLabel];

    if (m_pProgress != NULL)
    {
        CGRect rBoundsTemp = self.bounds;
        CGRect rLevel = [CRect BreakOffBottom:&rBoundsTemp DY:[CMainView DEFAULT_TEXT_SIZE]];

        int iDXSubVal = 60;
        int iDXProgress = 100;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            iDXSubVal = 100;
            iDXProgress = 200;
        }

        CGRect rSubVal2 = [CRect BreakOffLeft:&rLevel DX:iDXSubVal];
        CGRect rProgress = [CRect BreakOffLeft:&rLevel DX:iDXProgress];
        rProgress.origin.y += 4;
        [m_pVal2 setFrame:rSubVal2];
        [m_pProgress setFrame:rProgress];
    }
    else if (m_pVal2 != NULL)
    {
        [CRect CenterDownToY:&rBounds DY:([CMainView DEFAULT_TEXT_SIZE]+48)];
        CGRect rSubVal = [CRect BreakOffBottom:&rBounds DY:[CMainView DEFAULT_TEXT_SIZE]];
        [m_pVal2 setFrame:rSubVal];
    }

    [m_pVal1 setFrame:rBounds];
}
/*============================================================================*/

-(void)SetData:(NSString*)pText

/*============================================================================*/
{
    [m_pVal1 setText:pText];
}
/*============================================================================*/

-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2

/*============================================================================*/
{
    [m_pVal1 setText:pText1];
    [m_pVal2 setText:pText2];
}
/*============================================================================*/

-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2 Level:(int)iLevel

/*============================================================================*/
{
    [m_pVal1 setText:pText1];
    [m_pVal2 setText:pText2];
    float fLevel = (float)iLevel / 100.f;
    [m_pProgress setProgress:fLevel];
}
@end

@implementation CIntelliChemPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig;

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self) 
    {
        m_pConfig = pConfig;

        BOOL bORPTank = TRUE;
        isChlorinatorPresent = TRUE;
        if ([m_pConfig EquipPresent:POOL_CHLORPRESENT])
        {
            bORPTank = FALSE;
        }
        else
        {
            isChlorinatorPresent = FALSE;
        }


        m_pPH   = [[CStatusItem alloc] initWithFrame:CGRectZero Label:@"pH Level" HasSubValue:TRUE HasLevel:TRUE];
        m_pORP  = [[CStatusItem alloc] initWithFrame:CGRectZero Label:@"ORP Level" HasSubValue:bORPTank HasLevel:bORPTank];
        m_pH2O  = [[CStatusItem alloc] initWithFrame:CGRectZero Label:@"Water Balance" HasSubValue:TRUE HasLevel:FALSE];
        m_pSalt = [[CStatusItem alloc] initWithFrame:CGRectZero Label:@"Salt Level" HasSubValue:TRUE HasLevel:FALSE];
        [self addSubview:m_pPH];
        [self addSubview:m_pORP];
        [self addSubview:m_pH2O];
        [self addSubview:m_pSalt];
        [m_pPH release];
        [m_pORP release];
        [m_pH2O release];
        [m_pSalt release];


        [m_pConfig AddSink:self];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pConfig RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    [CRect Inset:&rBounds DX:10 DY:0];
    [m_pPH   setFrame:[CRect CreateYSection:rBounds Index:0 Of:4]];
    [m_pORP  setFrame:[CRect CreateYSection:rBounds Index:1 Of:4]];
    [m_pH2O  setFrame:[CRect CreateYSection:rBounds Index:2 Of:4]];
    [m_pSalt setFrame:[CRect CreateYSection:rBounds Index:3 Of:4]];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iPH     = [m_pConfig PH];
    int iORP    = [m_pConfig ORP];
    int iH2O    = [m_pConfig Saturation];
    int iSalt   = [m_pConfig Salt];
    
    NSString *pSat = @"--";
    NSString *pSatDesc = @"";
    NSString *pPH       = @"--";
    NSString *pORP      = @"--";
    NSString *pSalt     = @"--";
    
    if (iH2O >= -99 || iH2O <= 99)
    {
        if (iH2O > 0)
        {
            pSat = [NSString stringWithFormat:@"+0.%02d", iH2O];
        }
        else if (iH2O < 0)
        {
            pSat = [NSString stringWithFormat:@"-0.%02d", -iH2O];
        }
        else
        {
            pSat = @"0.00";
        }

        if (iH2O > 50)
            pSatDesc = @"SCALING";
        else if (iH2O > 10)
            pSatDesc = @"NORMAL";
        else if (iH2O < -50)
            pSatDesc = @"CORROSIVE";
        else if (iH2O < -10)
            pSatDesc = @"NORMAL";
        else 
            pSatDesc = @"IDEAL";
    }

    if (iPH >= 400 && iPH <= 1000)
    {
        float fPH = 0.01f * (float) iPH;
        pPH = [NSString stringWithFormat:@"%0.2f", fPH];
    }

    if (iORP > 0 && iORP < 1000)
    {
        pORP = [NSString stringWithFormat:@"%d", iORP];
    }

    if (iSalt > 0 && isChlorinatorPresent)
    {
        pSalt = [NSString stringWithFormat:@"%d", iSalt];
    }
    else
    {
        pSalt = @"N/A";
    }


    int iPHTank = [m_pConfig PHTankLevel];
    int iORPTank = [m_pConfig ORPTankLevel];
    iPHTank = iPHTank * 100 / 7;
    iORPTank = iORPTank * 100 / 7;

    [m_pPH   SetData:pPH Sub:@"TANK" Level:iPHTank];
    [m_pORP  SetData:pORP Sub:@"TANK" Level:iORPTank];
    [m_pH2O  SetData:pSat Sub:pSatDesc];
    [m_pSalt SetData:pSalt];
}

@end
