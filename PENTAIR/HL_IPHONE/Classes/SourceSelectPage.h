//
//  MediaPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/19/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "SubTabCell.h"
#import "HLView.h"
#import "MainView.h"
#import "MediaZonePage.h"

@class CSubTab;
@class CAudioZone;
@class CNowPlayingPage;
@class CMediaZonePage;

//
// iPAD code
//
/*============================================================================*/

@interface CSourceSelectPageIPAD : CMediaModalViewClientPage <   CHLCommandResponder  >

/*============================================================================*/
{
    NSMutableArray                      *m_pButtons;
    CMediaZonePage                      *m_pZonePage;
    CAudioZone                          *m_pZone;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone ZonePage:(CMediaZonePage*)pPage;
-(void)UpdateActiveSource;
-(void)OnCommand:(int)iCommandID;

@end
