//
//  HLSlider.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/4/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CHLButton;

/*============================================================================*/

@interface CHLSlider : UIControl

/*============================================================================*/
{
    int                             m_iPos;
    int                             m_iRad;
    int                             m_iSHOUT;
    int                             m_iSHIN;

    BOOL                            m_bVertical;
    UIImage                         *m_pImage;

    CHLButton                       *m_pMarker;
    BOOL                            m_bCapture;
    
    CGPoint                         m_ptTouchDown;
    int                             m_iPosStart;
}

-(id)initWithFrame:(CGRect)frame Vertical:(BOOL)bVeritcal;
-(void)InitMarker;

-(void)SetPositionPCT:(int)iPCT;
-(int)GetPositionPCT;
-(BOOL)HasCapture;

-(CGRect)MarkerPosition:(int)iPos;


@end
