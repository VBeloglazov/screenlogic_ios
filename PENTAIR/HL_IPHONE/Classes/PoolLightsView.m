//
//  PoolLightsView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolLightsView.h"
#import "hlm.h"
#import "NSPoolConfig.h"
#import "NSPoolCircuit.h"
#import "MainView.h"
#import "crect.h"
#import "NSQuery.h"
#import "CustomPage.h"

#define DY_ITEM                 50

@implementation CPoolLightsView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/

{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        m_pConfig = pConfig;

        m_pScrollView  = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:m_pScrollView];
        [m_pScrollView release];
//        [m_pCircuitsView setContentSize:CGSizeMake(self.bounds.size.width, iDYAllButtons)];

        m_pControls = [[NSMutableArray alloc] init];

        NSMutableArray *pList = [[[NSMutableArray alloc] init] autorelease];

        [pConfig LoadCircuitsByInterface:pList Interface:POOLINT_LIGHT];
        [pConfig LoadCircuitsByInterface:pList Interface:POOLINT_SYNCSWIM];

        for (int iC = 0; iC < [pList count]; iC++)
        {
            CNSPoolCircuit *pCircuit = (CNSPoolCircuit*)[pList objectAtIndex:iC];
            CLightCircuitWnd *pWnd = [[CLightCircuitWnd alloc] initWithFrame:CGRectZero Circuit:pCircuit];
            [m_pScrollView addSubview:pWnd];
            [m_pControls addObject:pWnd];
            [pWnd release];
        }
        
        [m_pConfig AddSink:self];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pConfig RemoveSink:self];
    [m_pControls release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    for (int i = 0; i < [m_pControls count]; i++)
    {
        CLightCircuitWnd *pWnd = (CLightCircuitWnd*)[m_pControls objectAtIndex:i];
        [pWnd Update];
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDYAllItems = [m_pControls count] * DY_ITEM;
    CGRect rScroll = self.bounds;

    if (iDYAllItems < rScroll.size.height)
    {
        [CRect CenterDownToY:&rScroll DY:iDYAllItems];
    }

    [m_pScrollView setFrame:rScroll];
    [m_pScrollView setContentSize:CGSizeMake(rScroll.size.width, iDYAllItems)];

    for (int i = 0; i < [m_pControls count]; i++)
    {
        CLightCircuitWnd *pWnd = (CLightCircuitWnd*)[m_pControls objectAtIndex:i];
        CGRect rFrame = CGRectMake(0, i*DY_ITEM, rScroll.size.width, DY_ITEM);
        [pWnd setFrame:rFrame];
    }
}

@end



@implementation CLightCircuitWnd

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Circuit:(CNSPoolCircuit*)pCircuit

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self) 
    {
        m_pCircuit = pCircuit;
        [m_pCircuit retain];

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        m_pLabel.textAlignment = UITextAlignmentLeft;
        [m_pLabel setText:[pCircuit Name]];
        
        [self addSubview:m_pLabel];
        [m_pLabel release];

        switch ([pCircuit Function])
        {
        case POOLCIRCUIT_DIMMER:
        case POOLCIRCUIT_DIMMER_25:
            {
                m_pSlider = [[UISlider alloc] initWithFrame:CGRectZero];
                [self addSubview:m_pSlider];
                [m_pSlider release];
                [m_pSlider addTarget:self action:@selector(SliderAction:) forControlEvents:UIControlEventTouchUpOutside|UIControlEventTouchUpInside|UIControlEventTouchCancel];
            }
            break;
            
        default:
            {
                m_pSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
                [self addSubview:m_pSwitch];
                [m_pSwitch release];
                [m_pSwitch addTarget:self action:@selector(SwitchAction:) forControlEvents:UIControlEventValueChanged];
            }
            break;
        }


    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pCircuit release];
    [super dealloc];
}
/*============================================================================*/

-(void)Update

/*============================================================================*/
{
    int iState = [m_pCircuit State];

    if (m_pSwitch != NULL)
    {
        if (iState == 0)
            [m_pSwitch setOn:NO];
        else
            [m_pSwitch setOn:YES];
    }
    
    if (m_pSlider != NULL)
    {
        float fPos = (float)iState / 100.f;
        [m_pSlider setValue:fPos];
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    [CRect Inset:&rBounds DX:8 DY:0];
    CGRect rLabel = [CRect BreakOffLeft:&rBounds DX:rBounds.size.width / 2];
    [m_pLabel setFrame:rLabel];

    [m_pSlider setFrame:rBounds];
    rBounds = [CRect BreakOffRight:&rBounds DX:100];
    [CRect CenterDownToY:&rBounds DY:32];

    [m_pSwitch setFrame:rBounds];
}
/*============================================================================*/

-(void)SwitchAction:(id)sender

/*============================================================================*/
{
    int iState = 0;
    if (m_pSwitch.on)
        iState = 100;

    CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_BUTTONPRESSQ];
    [p1 PutInt:0];
    [p1 PutInt:[m_pCircuit ID]];
    [p1 PutInt:iState];
    [p1 SendMessage];
    [p1 release];
}
/*============================================================================*/

-(void)SliderAction:(id)sender

/*============================================================================*/
{
    float fPos = m_pSlider.value;
    float fNewPos = fPos;

    int iState = 0;
    float fClosest = 1.0;
    
    switch ([m_pCircuit Function])
    {
    case POOLCIRCUIT_DIMMER:
        {
            for (int iThisState = 0; iThisState <= 100; iThisState += 10)
            {
                float fTest = (float)iThisState / 100.0f;
                if (fabs(fTest - fPos) < fClosest)
                {
                    iState = iThisState;
                    fClosest = fabs(fTest-fPos);
                    fNewPos = fTest;
                }
            }
        }
        break;
    case POOLCIRCUIT_DIMMER_25:
        {
            for (int iThisState = 0; iThisState <= 100; iThisState += 25)
            {
                float fTest = (float)iThisState / 100.0f;
                if (fabs(fTest - fPos) < fClosest)
                {
                    iState = iThisState;
                    fClosest = fabs(fTest-fPos);
                    fNewPos = fTest;
                }
            }
        }
    }

    CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_BUTTONPRESSQ];
    [p1 PutInt:0];
    [p1 PutInt:[m_pCircuit ID]];
    [p1 PutInt:iState];
    [p1 SendMessage];
    [m_pSlider setValue:fNewPos];
    [p1 release];

}
@end