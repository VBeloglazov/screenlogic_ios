//
//  SystemMode.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/25/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SystemMode.h"
#import "SinkServer.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"


@implementation CSystemModeList

/*============================================================================*/

-(id)init

/*============================================================================*/
{
    m_pSinkServer = [[CSinkServer alloc] init];
    m_pModes = [[NSMutableArray alloc] init];
    m_iActiveMode = -1;
    [CHLComm AddSocketSink:self];
    CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_SYSCONFIG_GETSYSTEMMODESQ] autorelease];
    [pQ1 SendMessageWithMyID:self];
    CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_MODE_GETMODEQ] autorelease];
    [pQ2 SendMessageWithMyID:self];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pSinkServer release];
    [m_pModes release];
    [super dealloc];
}
/*============================================================================*/

-(int)NModes

/*============================================================================*/
{
    return [m_pModes count];
}
/*============================================================================*/

-(int)ActiveModeIndex

/*============================================================================*/
{
    return m_iActiveMode;
}
/*============================================================================*/

-(NSString*)ModeName:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pModes count])
        return NULL;
    NSString *pText = (NSString*)[m_pModes objectAtIndex:iIndex];
    return pText;
}
/*============================================================================*/

-(void)SetSystemMode:(int)iIndex

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_MODE_SETMODEQ] autorelease];
    [pQ PutInt:iIndex];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_MODE_GETMODEQ] autorelease];
        [pQ2 SendMessageWithMyID:self];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_SYSCONFIG_GETSYSTEMMODESA:
        {
            [m_pModes removeAllObjects];
            int iN = [pMSG GetInt];
            for (int i = 0; i < iN; i++)
            {
                NSString *pName = [pMSG GetString];
                [pMSG GetString]; // Icon
                [m_pModes addObject:pName];
            }
            m_bInit = TRUE;
        }
        break;

    case HLM_MODE_GETMODEA:
    case HLM_MODE_CHANGED:
        {
            [pMSG ResetRead];
            m_iActiveMode = [pMSG GetInt];
            [m_pSinkServer NotifySinks:0 Data:m_iActiveMode];
        }
        break;
    }
}
/*============================================================================*/

-(BOOL)IsInitialized

/*============================================================================*/
{
    return m_bInit;
}
@end
