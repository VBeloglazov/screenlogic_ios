//
//  SecZoneCell.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "sink.h"

@class CSecPartition;
@class CSecZone;
@class CZoneIndicatorView;

/*============================================================================*/

@interface CSecZoneCell : UITableViewCell < CSink >

/*============================================================================*/
{
    UILabel                             *m_pZoneText;
    CZoneIndicatorView                  *m_pOK;
    CZoneIndicatorView                  *m_pFault;
    CZoneIndicatorView                  *m_pBypass;

    CSecPartition                       *m_pPartition;
    CSecZone                            *m_pZone;
    int                                 m_iLastState;
}
-(id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier Partition:(CSecPartition*)pPartition;
-(void)SetZone:(CSecZone*)pZone;
-(void)UpdateState;

@end

/*============================================================================*/

@interface CZoneIndicatorView : UIView

/*============================================================================*/
{
    UILabel                     *m_pLabel;
    UIColor                     *m_pColor;
}

-(id)initWithFrame:(CGRect)rFrame Text:(NSString*)psText Color:(UIColor*)pColor;
-(void)Set:(BOOL)bOn;

@end

