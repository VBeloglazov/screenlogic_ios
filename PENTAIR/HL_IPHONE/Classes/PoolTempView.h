//
//  CPoolTempView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "hlview.h"

@class CNSPoolConfig;
@class CHLButton;

#define TEMP_ACT                        0
#define TEMP_HEAT                       1

/*============================================================================*/

@interface CPoolTempView : UIView  < CSink, CHLCommandResponder >

/*============================================================================*/
{
    int                                 m_iDXLast;
    int                                 m_iDYLast;

    int                                 m_iBodyType;
    int                                 m_iMode;

    CNSPoolConfig                       *m_pConfig;

    UILabel                             *m_pLabel;
    UILabel                             *m_pTempText;

    CHLButton                           *m_pUp;
    CHLButton                           *m_pDn;
    
    int                                 m_iLocalTemp;
}

-(id)initWithFrame:(CGRect)rect Config:(CNSPoolConfig*)pConfig BodyType:(int)iBodyType Mode:(int)iMode;
-(void)Notify:(int)iID Data:(int)iData;
-(void)setLabels;

@end
