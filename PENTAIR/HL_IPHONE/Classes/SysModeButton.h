//
//  SysModeButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlbutton.h"
#import "NSSocketSink.h"
#import "HLView.h"

/*============================================================================*/

@interface CSysModeButton : CHLButton < CNSSocketSink, CHLCommandResponder >

/*============================================================================*/
{
    int m_iMode;
}

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Mode:(int)iMode;

@end
