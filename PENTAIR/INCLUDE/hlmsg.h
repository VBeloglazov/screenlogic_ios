/*
--------------------------------------------------------------------------------

    HLMSG.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLMSG_H
#define HLMSG_H

#include "hlstructs.h"
#include "hltime.h"
#include "hlapi.h"

class CHLTimeSpan;


/*============================================================================*/
    
    class HL_API CHLMSG

/*============================================================================*/
{

protected:
    //
    // Note: the first items in the class must match HLT_HEADER. This allows
    //       us to easily return a MSG when there is no buffer: see MSG().
    //
    short                       m_shSenderID;       // ID of sender on tablet
    short                       m_shMessageID;      // ID of message
    int                         m_iNBytes;          // Size of data: may not fill buffer
    BOOL                        m_bCritical;        // Non critical messages won't fault if WOULDBLOCKs occur on send

    // Other items don't have to match.
    BYTE                        *m_pBuffer;         // Memory
    int                         m_iNBuffer;         // Current buffer size
    int                         m_iIndex;           // Current index in memory
    BOOL                        m_bOwnData;         // Should we delete memory?

    //  Flag if we have an error during a read
    BOOL                        m_bReadError;

public:
    CHLMSG(
    BOOL                        bOwnData = FALSE);  // Default constructor

    CHLMSG(                                         // Access to header (allocates no memory for self)
    HLT_HEADER                  *pHeader );         // (use LoadHeader below to copy the header memory)

    CHLMSG(                                         // Normal constructor: creates memory for data
    short                       shSenderID,
    short                       shMessageID,
    int                         iNBytes     = 0,
    BOOL                        bCritical   = TRUE );

    CHLMSG(                                         // Normal constructor: creates memory for data
    CHLMSG                      *pCopyFrom );

    virtual                     ~CHLMSG();

    CHLMSG                      *CreateCopy();

    inline void                 SetReadDone()   {   m_iIndex = m_iNBytes;               }
    inline BOOL                 IsReadDone()    {   return  (m_iIndex == m_iNBytes);    }
    inline BOOL                 IsReadError()   {   return m_bReadError;                }

    BOOL                        Reset(
    HLT_HEADER                  *pHeader );

    BOOL                        Reset(
    int                         iNewMessageID,
    int                         iNBytes     = 0,
    BOOL                        bCritical   = TRUE );

    void                        ResetRead();

    short                       SenderID();
    void                        SenderID(short shNew);

    short                       MessageID();
    void                        MessageID(short shNew);

    BOOL                        Critical();

    BOOL                        IsSameMSG(
    CHLMSG                      *pOther );

    // This function copies an existing header (whereas the HLT_HEADER constructor
    // simply allows easy access to an existing HLT_HEADER).
    void                        LoadHeader(
    HLT_HEADER                  *pHeader );


    //
    // Access to data: the bytes that follow the header of the message.
    //
    int                         DataSize();

    void                        SetDataSize(
    int                         iSizeNew );

    BYTE                        *Data();

    //
    // Access to the actual message: normally used to send the message.
    //
    int                         MSGSize();

    BYTE                        *MSG();


    //
    // This group of functions returns TRUE if successful, FALSE if not.
    //

    // Gets...
    #ifdef MAC_OS
        inline BYTE             *DataAtRead()   { return (m_pBuffer + m_iIndex + 8);    }
    #endif

    BYTE                        *Buffer()       { return m_pBuffer;                     }

    BOOL                        GetData(
    BYTE                        *pData,
    int                         iNBytes );

    BOOL                        GetChar(
    char                        *pResult );

    BOOL                        GetBYTE(
    BYTE                        *pResult );

    BOOL                        GetShort(
    short                       *pResult );

    BOOL                        GetWORD(
    WORD                        *pResult );

    BOOL                        GetInt(
    int                         *pResult );

    BOOL                        GetString(
    CHLString                   *pResult );

    BOOL                        GetStringTCHAR(
    TCHAR                       **pString);

#ifdef UNICODE
    BOOL                        GetString(
    wchar_t                     **pText,
    int                         *piLen );
#else
    BOOL                        GetString(
    char                        **pText,
    int                         *piLen );
#endif

    BOOL                        GetTime(
    CHLTime                     *pResult );

    BOOL                        GetTimeCompressed(
    CHLTime                     *pResult );

    BOOL                        GetTimeSpan(
    CHLTimeSpan                 *pResult );

    BOOL                        GetColor(
    COLORREF                    *pResult );

    BOOL                        GetRECT(
    RECT                        *pResult );

    // Puts...
    BOOL                        PutData(
    BYTE                        *pData,
    int                         iNBytes );

    BOOL                        PutChar(
    char                        cChar );

    BOOL                        PutBYTE(
    BYTE                        byData );

    BOOL                        PutShort(
    short                       shShort );

    BOOL                        PutWORD(
    WORD                        wShort );

    BOOL                        PutInt(
    int                         iInt );

#if defined GATEWAY || (defined HLCONFIG && defined PENTAIR)
    BOOL                        PutStringCHAR(
    CHLString                   sString );
#endif

    BOOL                        PutString(
    CHLString                   sString );

    BOOL                        PutTime(
    CHLTime                     hltTime );

    BOOL                        PutTimeCompressed(
    CHLTime                     hltTime );

    BOOL                        PutTimeSpan(
    CHLTimeSpan                 hltTimeSpan );

    BOOL                        PutColor(
    COLORREF                    rgbOut );

    BOOL                        PutRECT(
    RECT                        rectOut );

    BYTE                        *GetReadPointer();

    inline void                 SkipBytes(
    int                         iNBytes )   { m_iIndex += iNBytes;  }

    //
    // This group throws an exception if something is wrong.
    // Not OK in CRYSTALPAD...
    //
    #ifdef _CPPUNWIND
    void                        X_Reset(
    HLT_HEADER                  *pHeader );

    void                        X_Reset(
    int                         iNewMessageID,
    int                         iNBytes,
    BOOL                        bCritical );

    // X_Gets...
    void                        X_GetData(
    BYTE                        *pResult,
    int                         iNBytes );

    char                        X_GetChar();

    short                       X_GetShort();

    int                         X_GetInt();

    CHLString                   X_GetString();

    CHLTime                     X_GetTime();

    CHLTimeSpan                 X_GetTimeSpan();

    COLORREF                    X_GetColor();

    RECT                        X_GetRECT();

    // Puts...
    void                        X_PutData(
    BYTE                        *pData,
    int                         iNBytes );

    void                        X_PutChar(
    char                        cChar );

    void                        X_PutShort(
    short                       shShort );

    void                        X_PutInt(
    int                         iInt );

    void                        X_PutString(
    CHLString                   sString );

    void                        X_PutTime(
    CHLTime                     hltTime );

    void                        X_PutTimeSpan(
    CHLTimeSpan                 hltTimeSpan );

    void                        X_PutColor(
    COLORREF                    rgbOut );

    void                        X_PutRECT(
    RECT                        rectOut );
    #endif // _CPPUNWIND

    BOOL                        AllocBuffer(
    int                         iSize );
};
/*============================================================================*/

    class HL_API CHLLogMSG :           public CHLMSG

/*============================================================================*/
{
protected:
    CHLTime                     m_hlTime;
    int                         m_iTypeInfo;   // This is the mask

public:
    // This constructor is used for writing to a file.
    CHLLogMSG(
    int                         iTypeInfo );

    // This constructor is used for reading from to a file.
    CHLLogMSG(
    HLT_LOG_HEADER              *pHeader );

    CHLTime                     Time();
    void                        Time(CHLTime tmNew);
    int                         TypeInfo();
};

#endif

