//
//  connectionEditor.m
//  XPAD_VIEW
//
//  Created by dmitriy on 4/1/14.
//
//

#import "ConnectionEditor.h"

@interface ConnectionEditor ()
@end


@implementation ConnectionEditor

const int RESULT_CANCEL = 0;
const int RESULT_SAVE = 1;
const int RESULT_DELETE = 2;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Orientation:(UIInterfaceOrientation)orientation;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        m_Orientation = orientation;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [systemNameText setText:self.controllerDescriptor.systemName];
    [nickNameEditBox setText:self.controllerDescriptor.nickName];
    [nickNameEditBox addTarget:self action:@selector(GoEvent:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    //customize buttons
    cancelButton.layer.cornerRadius = 6;
    cancelButton.layer.borderWidth = 1;
    cancelButton.layer.borderColor = [UIColor colorWithRed:0.0 green:122.0/255 blue:1.0 alpha:1.0].CGColor;
    
    deleteButton.layer.cornerRadius = 6;
    deleteButton.layer.borderWidth = 1;
    deleteButton.layer.borderColor = [UIColor colorWithRed:0.0 green:122.0/255 blue:1.0 alpha:1.0].CGColor;
    
    saveButton.layer.cornerRadius = 6;
    saveButton.layer.borderWidth = 1;
    saveButton.layer.borderColor = [UIColor colorWithRed:0.0 green:122.0/255 blue:1.0 alpha:1.0].CGColor;
    
    self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    //DL this makes status bar be visile (iPhone)
    //if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    //{
    //    UIView *addStausbar = [[UIView alloc] init];
    //    addStausbar.frame = CGRectMake(0,0,self.view.frame.size.width,20);
    //    addStausbar.backgroundColor = [UIColor colorWithWhite:200.0 alpha:0.5];
    //    [self.view addSubview:addStausbar];
    //}
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    
    [cancelButton release];
    [systemNameText release];
    [nickNameEditBox release];
    [saveButton release];
    [deleteButton release];
    [labelSystemName release];
    [labelNickName release];
    [super dealloc];
}
- (void)viewDidUnload
{
    
    [cancelButton release];
    cancelButton = nil;
    [systemNameText release];
    systemNameText = nil;
    [nickNameEditBox release];
    nickNameEditBox = nil;
    [saveButton release];
    saveButton = nil;
    [deleteButton release];
    deleteButton = nil;
    [labelSystemName release];
    labelSystemName = nil;
    [labelNickName release];
    labelNickName = nil;
    [super viewDidUnload];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [systemNameText setText:self.controllerDescriptor.systemName];
    [nickNameEditBox setText:self.controllerDescriptor.nickName];
    
    //int w = self.view.bounds.size.width;
    //int h = self.view.bounds.size.height;
    //CGSize s = CGSizeMake(w, h);
    //self.view.frame = self.view.superview.bounds;

}

- (IBAction)cancelButtonPress:(id)sender
{
    [self.checkStatusDelegate checkEditStatus:self isFinished:CANCEL];
    [self dismissModalViewControllerAnimated:YES];
    
}

- (IBAction)saveButtonPress:(id)sender
{
    //just for test
    [self.controllerDescriptor setNickName:nickNameEditBox.text];
    ///
    [self.checkStatusDelegate checkEditStatus:self isFinished:SAVE];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)deleteButtonPress:(id)sender
{
    [self.checkStatusDelegate checkEditStatus:self isFinished:DELETE];
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)GoEvent:(id)sender

/*============================================================================*/
{
    [nickNameEditBox resignFirstResponder];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    int vw = CGRectGetWidth(self.view.bounds);
    int y = 0;
    
    if((fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown || fromInterfaceOrientation == UIInterfaceOrientationPortrait)&& [labelNickName frame].origin.y >= 328)
    {
        y = 114;
    }
    
    CGRect r0 = [systemNameText frame];
    r0.origin.x = (vw - systemNameText.frame.size.width)/2;
    r0.origin.y -= y;
    [systemNameText setFrame:r0];

    CGRect r1 = [cancelButton frame];
    r1.origin.x = (vw - cancelButton.frame.size.width)/2;
    r1.origin.y -= y;
    [cancelButton setFrame:r1];

    CGRect r2 = [nickNameEditBox frame];
    r2.origin.x = (vw - nickNameEditBox.frame.size.width)/2;
    r2.origin.y -= y;
    [nickNameEditBox setFrame:r2];
    
    CGRect r3 = [saveButton frame];
    r3.origin.x = r2.origin.x;
    r3.origin.y -= y;
    [saveButton setFrame:r3];
    
    CGRect r4 = [deleteButton frame];
    r4.origin.x = r2.origin.x + (r2.size.width - r4.size.width);
    r4.origin.y -= y;
    [deleteButton setFrame:r4];
    
    CGRect r5 = [labelSystemName frame];
    r5.origin.x = (vw - labelSystemName.frame.size.width)/2;
    r5.origin.y -= y;
    [labelSystemName setFrame:r5];
    
    CGRect r6 = [labelNickName frame];
    r6.origin.x = (vw - labelNickName.frame.size.width)/2;
    r6.origin.y -= y;
    [labelNickName setFrame:r6];


    //DL this makes status bar be visile (iPhone)
    //if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    //{
    //   UIView *addStausbar = [[UIView alloc] init];
    //   addStausbar.frame = CGRectMake(0,0,self.view.frame.size.width,20);
    //    addStausbar.backgroundColor = [UIColor colorWithWhite:200.0 alpha:0.5];
    //    [self.view addSubview:addStausbar];
    //}

}

/*============================================================================*/

- (BOOL)shouldAutorotate:(UIInterfaceOrientation)interfaceOrientation

/*============================================================================*/
{
    return [self shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}
/*============================================================================*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

/*============================================================================*/
{
	// Return YES for supported orientations
    
    
	switch (interfaceOrientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            switch (m_Orientation)
            {
                case UIInterfaceOrientationPortrait:
                case UIInterfaceOrientationPortraitUpsideDown:
                    return TRUE;
                    
                default:
                    break;
            }
        }
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            switch (m_Orientation)
            {
                case UIInterfaceOrientationLandscapeLeft:
                case UIInterfaceOrientationLandscapeRight:
                    return TRUE;
                    
                default:
                    break;
            }
        }
            break;
    }
    return NO;
}

//keeps old text while edting has begun

- (IBAction)editingDidBegin:(id)sender
{
    ((UITextField*)sender).clearsOnBeginEditing = FALSE;
}
@end
