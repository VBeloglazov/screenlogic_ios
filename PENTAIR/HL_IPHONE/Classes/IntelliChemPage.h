//
//  IntelliChemPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/9/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import "NSSocketSink.h"
#import "Sink.h"

@class CNSPoolConfig;

/*============================================================================*/

@interface CStatusItem : UIView

/*============================================================================*/
{
    UILabel                         *m_pLabel;
    UILabel                         *m_pVal1;
    UILabel                         *m_pVal2;
    UIProgressView                  *m_pProgress;
}

-(id)initWithFrame:(CGRect) rFrame Label:(NSString*)pLabel HasSubValue:(BOOL)bSubValue HasLevel:(BOOL)bHasLevel;
-(UILabel*)AddLabel:(NSString*)pText Size:(int)iSize;
-(void)SetData:(NSString*)pText1;
-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2;
-(void)SetData:(NSString*)pText1 Sub:(NSString*)pText2 Level:(int)iLevel;

@end

/*============================================================================*/

@interface CIntelliChemPage : CSubSystemPage < CSink >

/*============================================================================*/
{
    CNSPoolConfig                   *m_pConfig;
    
    CStatusItem                     *m_pPH;
    CStatusItem                     *m_pORP;
    CStatusItem                     *m_pH2O;
    CStatusItem                     *m_pSalt;
    BOOL                            isChlorinatorPresent;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig;

@end
