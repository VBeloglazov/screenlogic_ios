//
//  timedataview.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTimeView;
@class CNSMSG;

/*============================================================================*/

@interface CTimeDataView : UIView 

/*============================================================================*/
{
    int                             m_iXOrigin;
    CTimeView                       *m_pTimeView;
    NSDate                          *m_pDateDataStart;
    NSDate                          *m_pDateDataEnd;
    BOOL                            m_bDataOK;
}

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView;

-(NSDate*)StartDataDate;
-(NSDate*)EndDataDate;
-(void)StopQuery;
-(void)NotifyPosition;
-(void)DrawDayRects:(CGContextRef)pRef;
-(void)SetXOrigin;
-(void)UpdateTimeBar;
-(BOOL)DataOK;
-(void)SetTimeView:(CTimeView*)pTimeView;

-(int)RoundTempUp:(int)iIn;
-(int)RoundTempDown:(int)iIn;

@end

/*============================================================================*/

@interface CDataPoint : NSObject

/*============================================================================*/
{
    NSDate                              *m_pDate;
    int                                 m_iVal;
}

-(id)initWithDate:(NSDate*)pDate Val:(int)iVal;

-(NSDate*)Date;
-(int)Val;

@end

/*============================================================================*/

@interface CTempSeries : NSObject

/*============================================================================*/
{
    UIColor                             *m_pRGB;
    NSMutableArray                      *m_pPoints;
}

-(id)initWithColor:(UIColor*)pColor;
-(void)ReadFrom:(CNSMSG*)pMSG;

-(NSMutableArray*)PointList;
-(UIColor*)Color;

-(int)MinTempFrom:(NSDate*)tmMin To:(NSDate*)tmMax;
-(int)MaxTempFrom:(NSDate*)tmMin To:(NSDate*)tmMax;


@end
