//
//  AllZonesOffView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 8/26/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoundRegion.h"


@class CMediaZonePage;

/*============================================================================*/

@interface CAllZonesOffView : CRoundRegion

/*============================================================================*/
{
    CMediaZonePage                  *m_pZonePage;
    int                             m_iZoneID;
    UILabel                         *m_pLabel;
    UIProgressView                  *m_pProgress;
    NSTimer                         *m_pTimer;
    int                             m_iCount;
}

-(id)initWithFrame:(CGRect)frame Page:(CMediaZonePage*)pPage ZoneID:(int)iZoneID;

-(void)Dismiss;

@end
