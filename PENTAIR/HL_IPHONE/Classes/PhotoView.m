//
//  CPhotoView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PhotoView.h"
#import "Photo.h"
#import "PhotoAlbum.h"
#import "hlview.h"
#import "HLComm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "crect.h"
#import "PhotoGridView.h"

#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

@implementation CPhotoView

#define THUMB_DX    320
#define THUMB_DY    240

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame GridView:(CPhotoGridView*)pView Photo:(CPhoto*)pPhoto

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_pGridView = pView;
        m_pPhoto    = pPhoto;
        m_iImageDX  = 320;
        m_iImageDY  = 240;
        m_pSubView1 = [[UIView alloc] initWithFrame:CGRectZero];
        m_pSubView2 = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pSubView1];
        [self addSubview:m_pSubView2];
        [m_pSubView1 release];
        [m_pSubView2 release];
        [m_pSubView1 setUserInteractionEnabled:NO];
        [m_pSubView2 setUserInteractionEnabled:NO];
        
        [m_pSubView1 setBackgroundColor:[UIColor blueColor]];
        [m_pSubView2 setAlpha:0];
    }
    
    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Photo:(CPhoto*)pPhoto Offset:(int) iOffset

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        CPhotoAlbum *pAlbum = [pPhoto Album];
        int iIndex = [pPhoto Index] + iOffset;
        while (iIndex < 0)
            iIndex += [pAlbum NPhotos];
        while (iIndex >= [pAlbum NPhotos])
            iIndex -= [pAlbum NPhotos];
            
        m_pPhoto    = [pAlbum Photo:iIndex];
        m_iImageDX  = 320;
        m_iImageDY  = 240;
        m_pSubView1 = [[UIView alloc] initWithFrame:CGRectZero];
        m_pSubView2 = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pSubView1];
        [self addSubview:m_pSubView2];
        [m_pSubView1 release];
        [m_pSubView2 release];
        [m_pSubView1 setUserInteractionEnabled:NO];
        [m_pSubView2 setUserInteractionEnabled:NO];
        
        [m_pSubView1 setBackgroundColor:[UIColor blueColor]];
        [m_pSubView2 setAlpha:0];
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    free(m_pImageData);

    switch (m_iState)
    {
    case STATE_WAIT:
        [CHLComm RemoveSocketSink:self];
        break;
    
    default:
        break;
    }

    [super dealloc];
}
/*============================================================================*/

-(BOOL)OnIdleVisible

/*============================================================================*/
{
    switch (m_iState)
    {
    case STATE_INIT:
        {
            int iDX = THUMB_DX;
            int iDY = THUMB_DY;
            
            if (m_pGridView == NULL)
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    iDX = 1280;
                    iDY = 768;
                }
                else
                {
                    iDX = 800;
                    iDY = 480;
                }
            }

            [CHLComm AddSocketSink:self];
            CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_PICSERVER_GETSLIDEDXDYINDEXQ] autorelease];
            [pQ PutInt:[CHLComm SenderID:self]];
            [pQ PutInt:iDX];
            [pQ PutInt:iDY];
            [pQ PutInt:[[m_pPhoto Album] ID]];
            [pQ PutInt:[m_pPhoto Index]];
            [pQ PutInt:0];
            [pQ SendMessageWithMyID:self];
            m_iState = STATE_WAIT;
            [m_pGridView IncPacketsInFlight];
            return TRUE;
        }
        break;
        
    case STATE_WAIT:
    case STATE_DATAOK:
        break;
    }

    return FALSE;
}
/*============================================================================*/

-(void)ReleaseImage

/*============================================================================*/
{
    free(m_pImageData);
    m_pImageData = NULL;
    m_iNImageData = 0;
    [m_pSubView2.layer setContents:NULL];
    
    // If we've gotten an image revert to init state
    // If we're in a WAIT_STATE, just stay there
    if (m_iState == STATE_DATAOK)
        m_iState = STATE_INIT;
        
    [m_pSubView2 setAlpha:0];
    [m_pSubView1 setAlpha:1];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_PICSERVER_IMAGEDATA)
        return;

    m_iImageDX = [pMSG GetInt];
    m_iImageDY = [pMSG GetInt];

    if (m_pImageData != NULL)
    {
        free(m_pImageData);
        m_pImageData = NULL;
    }
    
    m_iNImageData = [pMSG DataSize]-8;
    m_pImageData = malloc(m_iNImageData);
    unsigned char *pData = [pMSG GetDataAtReadIndex];
    memcpy(m_pImageData, pData, m_iNImageData);

    CGDataProviderRef pSrc = CGDataProviderCreateWithData(NULL, m_pImageData, m_iNImageData, NULL);
    CGImageRef pImage = CGImageCreateWithJPEGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
    CGDataProviderRelease(pSrc);

    [m_pSubView2.layer setContents:(id)pImage];
    CGImageRelease(pImage);


    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [m_pSubView1 setAlpha:0];
    [m_pSubView2 setAlpha:1];
    [self DoLayout];
    [UIView commitAnimations];

    [m_pGridView DecPacketsInFlight];
    [CHLComm RemoveSocketSink:self];

    m_iState = STATE_DATAOK;

}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [self DoLayout];
}
/*============================================================================*/

-(void)DoLayout

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    double dAspect = (double)m_iImageDX / (double)m_iImageDY;

    [CRect ConfineToAspect:&rThis DXDY:dAspect];
    [m_pSubView1 setFrame:rThis];
    [m_pSubView2 setFrame:rThis];
}
/*============================================================================*/

-(id)GetContents        { return m_pSubView2.layer.contents;    }
-(int)ImageDX           { return m_iImageDX;                    }
-(int)ImageDY           { return m_iImageDY;                    }

/*============================================================================*/

-(void)SetContents:(CPhotoView*)pSource

/*============================================================================*/
{
    [m_pSubView2.layer setContents:[pSource GetContents]];
    m_iImageDX = [pSource ImageDX];
    m_iImageDY = [pSource ImageDY];

    [m_pSubView2 setAlpha:1];
    [m_pSubView1 setAlpha:0];
}
/*============================================================================*/

-(CPhoto*)Photo

/*============================================================================*/
{
    return m_pPhoto;
}
/*============================================================================*/

-(void)OffsetPhotoIndex:(int)iOffset

/*============================================================================*/
{
    int iIndex = [m_pPhoto Index];
    iIndex += iOffset;

    m_iImageDX = 320;
    m_iImageDY = 240;

    CPhotoAlbum *pAlbum = [m_pPhoto Album];

    while (iIndex < 0)
        iIndex += [pAlbum NPhotos];
    while (iIndex >= [pAlbum NPhotos])
        iIndex -= [pAlbum NPhotos];
        
    m_pPhoto    = [pAlbum Photo:iIndex];
    
    if (m_pImageData != NULL)
    {
        free(m_pImageData);
        m_pImageData = NULL;
    }
    m_iNImageData = 0;
    [m_pSubView2.layer setContents:NULL];
    [m_pSubView2 setAlpha:0];
    [m_pSubView1 setAlpha:1];
    if (m_iState == STATE_WAIT)
    {
        [CHLComm RemoveSocketSink:self];
    }
    m_iState = STATE_INIT;
    [self OnIdleVisible];
    
}
/*============================================================================*/

-(BOOL)IsFirstImage

/*============================================================================*/
{
    return [m_pPhoto Index] == 0;
}
/*============================================================================*/

-(BOOL)IsLastImage

/*============================================================================*/
{
    return ([m_pPhoto Index] == ([[m_pPhoto Album] NPhotos] - 1));
}
@end
