//
//  PhotosListView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 8/30/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CPhotoAlbumRoot;
@class CPhotosListView;
@class CPhotoAlbum;
@class CPhotosPage;
@class CUserTabIconView;

/*============================================================================*/

@interface CAlbumView : UIControl

/*============================================================================*/
{
    CAlbumView                      *m_pParent;
    CPhotosListView                 *m_pMainView;
    
    CPhotoAlbum                     *m_pAlbum;
    UILabel                         *m_pLabel;
    
    NSMutableArray                  *m_pSubViews;
    BOOL                            m_bSelected;
    BOOL                            m_bExpanded;
    CUserTabIconView                *m_pIconView;
}

-(id)initWithFrame:(CGRect)rFrame Parent:(CAlbumView*)pParent View:(CPhotosListView*)pView Album:(CPhotoAlbum*)pAlbum;

-(int)TotalHeight;
-(void)SetSelected:(BOOL)bSelected;

-(CPhotoAlbum*)Album;
-(CAlbumView*)Parent;
-(BOOL)IsExpanded;
-(int)NSubAlbums;

-(void)BeginCollapse;
-(void)EndCollapse;
-(void)BeginExpand;
-(void)CommitExpand;
-(BOOL)FindSubView:(CAlbumView*)pSubView;
-(BOOL)FindExpandedView;
-(void)UpdateState:(CAlbumView*)pExpandedView;
-(void)DoLayout;
-(BOOL)FindExpandedViewWithoutChild:(CAlbumView*)pSubView;
-(void)BeginCollapseWithoutChild:(CAlbumView*)pSubView;

@end

/*============================================================================*/

@interface CPhotosListView : UIView 

/*============================================================================*/
{
    CPhotosPage                     *m_pMainPage;
    CAlbumView                      *m_pSelectedItem;
    CAlbumView                      *m_pFocusItem;
    NSMutableArray                  *m_pItems;
    UIScrollView                    *m_pScrollView;
}

-(id)initWithFrame:(CGRect)frame Root:(CPhotoAlbumRoot*)pRoot PhotosPage:(CPhotosPage*)pPage;
-(void)DoLayoutAnimated:(BOOL)bAnimated;
-(void)UpdateItemStates;
-(void)SelectAlbum:(id)sender;

@end
