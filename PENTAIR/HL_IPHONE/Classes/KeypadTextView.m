//
//  KeypadTextView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "KeypadTextView.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "CustomPage.h"

@implementation CKeypadTextView

/*============================================================================*/

-(id)initWithFrame: (CGRect)rView ID:(int)iID TextSize:(int)iTextSize TextColor:(UIColor*)pRGBText

/*============================================================================*/
{
    self = [super initWithFrame:rView];
    if (self)
    {
        self.autoresizingMask = 0xFFFFFFFF;

        m_iID = iID;

        if (pRGBText != NULL)
        {
            m_pRGBText = pRGBText;
            [m_pRGBText retain];
        }
        else
        {
            m_pRGBText = [UIColor whiteColor];
            [m_pRGBText retain];
        }

        m_iTextSize = 12;
        if (iTextSize > 0)
            m_iTextSize = iTextSize;

        [CHLComm AddSocketSink:self];

        int iDYItem = rView.size.height / 4;
        int iDXItem = rView.size.width;
        for (int i = 0; i < 5; i++)
        {
            CGRect rItem = CGRectMake(iDXItem * 3 / 4, 0, iDXItem / 4, iDYItem);
            if (i > 0)
            {
                int iDXItemThis = iDXItem;
                if (i == 0)
                    iDXItemThis = iDXItem * 3 / 4;
                rItem = CGRectMake(0, iDYItem * (i-1), iDXItemThis, iDYItem);
            }

            UILabel *pLabel = [[UILabel alloc] initWithFrame:rItem];
            [CCustomPage InitStandardLabel:pLabel Size:m_iTextSize];
            pLabel.textColor = m_pRGBText;
            pLabel.textAlignment = UITextAlignmentLeft;
            pLabel.autoresizingMask = 0xFFFFFFFF;
            [self addSubview:pLabel];
            m_pLabels[i] = pLabel;
        }

        [self InitComm];

    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    CNSQuery *p1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_REMOVEKEYPADSINKQ] autorelease];
    [p1 PutInt:m_iID];
    [p1 PutInt:[CHLComm SenderID:self]];
    [p1 SendMessageWithMyID:self];


    [CHLComm RemoveSocketSink:self];
    [m_pRGBText release];

    for (int i = 0; i < 5; i++)
    {
        [m_pLabels[i] release];
    }

    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
        [self InitComm];
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_AUDIO_GETKEYPADSTATUSA)
        return;

    [pMSG ResetRead];

    int iType = [pMSG GetInt];
    if (iType != AUDIO_STATUS_TEXTINFO)
    {
        [pMSG ResetRead];
        return;
    }

    NSString *pMenu     = [pMSG GetString];
    NSString *pArtist   = [pMSG GetString];
    NSString *pAlbum    = [pMSG GetString];
    NSString *pTrack    = [pMSG GetString];
    BOOL bCmd           = [pMSG GetInt];
    NSString *pCmd      = [pMSG GetString];

    
    if (bCmd)
    {
        UILabel *pCmdLabel = m_pLabels[0];
        [pCmdLabel setAlpha:1.0];
        [pCmdLabel setText:pCmd];

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:2.0];
        [pCmdLabel setAlpha:0.0];
        [UIView commitAnimations];
    }

    [m_pLabels[1] setText:pMenu];
    [m_pLabels[2] setText:pArtist];
    [m_pLabels[3] setText:pAlbum];
    [m_pLabels[4] setText:pTrack];
}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    CNSQuery *p1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDKEYPADSINKQ] autorelease];
    [p1 PutInt:m_iID];
    [p1 PutInt:[CHLComm SenderID:self]];
    [p1 SendMessageWithMyID:self];
    
    CNSQuery *p2 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_GETKEYPADSTATUSQ] autorelease];
    [p2 PutInt:m_iID];
    [p2 SendMessageWithMyID:self];
}

@end
