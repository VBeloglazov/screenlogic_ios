//
//  SourceIndicator.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "SourceIndicator.h"
#import "SourceIconView.h"
#import "AudioZone.h"
#import "AudioSource.h"
#import "MainView.h"
#import "crect.h"
#import "CustomPage.h"


@implementation CSourceIndicator

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    if ((self = [super initWithFrame:frame])) 
    {
        // Initialization code
        m_pZone = pZone;
        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [m_pZone AddSink:self];
        [self Notify:0 Data:SINK_SOURCECHANGED];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if ((iData & SINK_SOURCECHANGED) == 0)
        return;
    
    [m_pLabel removeFromSuperview];
    [m_pLabel release];
    [m_pIcon removeFromSuperview];
    [m_pIcon release];
    m_pIcon = NULL;
    m_pLabel = NULL;

    
    int iSourceIndex = [m_pZone ActiveSourceIndex];
    NSString *pSource = @"Source";

    NSString *pIcon = NULL;

    if (iSourceIndex > 0)
    {
        CAudioSource *pS = [m_pZone Source:(iSourceIndex-1)];
        pSource = pS.m_pSourceName;
        pIcon = pS.m_pIconName;
    }

    CGRect rThis = self.bounds;
    CGRect rLabel = [CRect BreakOffBottom:&rThis DY:[CMainView DEFAULT_TEXT_SIZE]];
    CGRect rIcon = rThis;
    
    m_pLabel = [[UILabel alloc] initWithFrame:rLabel];
    [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
    [m_pLabel setAdjustsFontSizeToFitWidth:YES];
    [m_pLabel setMinimumFontSize:8];
    m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
    m_pLabel.numberOfLines = 3;

    [m_pLabel setText:pSource];
    [self addSubview:m_pLabel];


    if (iSourceIndex <= 0)
    {
        [m_pLabel setTextAlignment:UITextAlignmentCenter];
    }

    if (pIcon != NULL)
    {
        m_pIcon = [[CSourceIconView alloc] initWithFrame:rIcon IconName:pIcon SourceName:@""];
        [self addSubview:m_pIcon];
    }
}


@end
