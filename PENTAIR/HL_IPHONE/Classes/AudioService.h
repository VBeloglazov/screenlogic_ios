//
//  AudioService.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

@class CNSMSG;

/*============================================================================*/

@interface CAudioService : NSObject 

/*============================================================================*/
{
    int                                 m_iID;
    NSString                            *m_pName;
    int                                 m_iType;
}

-(id)initWithMSG:(CNSMSG*)pMSG;

-(int)ID;
-(NSString*)Name;
-(NSString*)Icon;
-(int)Type;
-(int)ServiceID;

@end
