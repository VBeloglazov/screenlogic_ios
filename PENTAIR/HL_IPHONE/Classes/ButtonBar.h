//
//  ButtonBar.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 7/12/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlbutton.h"

@class CButtonBarButton;

/*============================================================================*/

@interface CButtonBar : UIView 

/*============================================================================*/
{
    NSMutableArray                  *m_pButtons;
    UIColor                         *m_pCheckedColor;
}

-(CButtonBarButton*)AddButtonText:(NSString*)pText Icon:(NSString*)pIcon;
-(void)SetCheckedColor:(UIColor*)pColor;
-(int)NItems;

@end


/*============================================================================*/

@interface CButtonBarButton : CHLButton

/*============================================================================*/
{
}

@end