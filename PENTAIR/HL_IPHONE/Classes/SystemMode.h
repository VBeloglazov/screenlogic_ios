//
//  SystemMode.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/25/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"

@class CSinkServer;

/*============================================================================*/

@interface CSystemModeList : NSObject < CNSSocketSink >

/*============================================================================*/
{
    CSinkServer                         *m_pSinkServer;
    NSMutableArray                      *m_pModes;
    int                                 m_iActiveMode;

    BOOL                                m_bInit;
}

-(id)init;

-(int)NModes;
-(int)ActiveModeIndex;
-(NSString*)ModeName:(int)iIndex;

-(void)SetSystemMode:(int)iIndex;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;

-(BOOL)IsInitialized;

@end
