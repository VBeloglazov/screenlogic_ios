//
//  timeview.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTimeDataView;

#define TYPE_MONTH      0
#define TYPE_MONTH_DAY  1
#define TYPE_DAY        2
#define TYPE_HOUR_12    3
#define TYPE_HOUR_6     4
#define TYPE_HOUR_2     5
#define TYPE_HOUR_1     6
#define TYPE_MIN_30     7
#define TYPE_MIN_15     8
#define TYPE_MIN_10     9
#define TYPE_MIN_5      10
#define TYPE_MIN_1      11

#define TYPE_START      0
#define TYPE_END        11


#import "MainView.h"

@class CTimeBar;
@class CTimeDataView;

/*============================================================================*/

@interface CTimeView : UIView 

/*============================================================================*/
{
    int                                 m_iViewSeconds;
    NSDate                              *m_pViewCenterDate;
    UITouch                             *m_pTouch1;
    UITouch                             *m_pTouch2;
    CGPoint                             m_ptTouch1;
    CGPoint                             m_ptTouch2;
    CGPoint                             m_ptTouch1Last;
    CGPoint                             m_ptTouch2Last;

    int                                 m_iYDataTouch;
    int                                 m_iViewSecondsTouch;
    NSDate                              *m_pViewCenterTouch;
    NSDate                              *m_pViewCenterZoom;

    CTimeBar                            *m_pTimeBar;
    CTimeDataView                       *m_pTimeDataView;

    BOOL                                m_bStaticScale;
    UIView                              *m_pScaleView;

    UIView                              *m_pLegendView;

    NSCalendar                          *m_pCalendar;

    UIActivityIndicatorView             *m_pActivityView;
    
    BOOL                                m_bLockYScroll;
    
    int                                 m_iDXLast;
}

-(int)IntervalDX:(int)iType;
-(int)XLogical:(NSDate*)pDate;
-(NSDate*)GetTimeAtX:(int)iX;
-(NSDate*)RoundUp:(NSDate*)pDate Type:(int)iType;
-(void)UpdateZoom;
-(void)UpdateScroll;
-(int)IntervalSeconds:(int)iType;
-(CTimeBar*)GetTimeBar;
-(void)InitScaleView;
-(void)NotifyRotationComplete;

-(UIView*)CreateLegendView;
-(CTimeDataView*)CreateTimeDataView:(CGRect)rFrame LastView:(CTimeDataView*)pLastView;
-(UIView*)GetScaleView;

-(int)DistanceFrom:(CGPoint)pPt1 To:(CGPoint)pPt2;

-(int)DataViewDY;

-(void)SetWorking:(BOOL)bWorking;

@end
