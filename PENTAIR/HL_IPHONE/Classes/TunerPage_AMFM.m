//
//  TunerPage_AMFM.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/9/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "TunerPage_AMFM.h"
#import "AudioZone.h"
#import "hlm.h"
#import "MediaZonePage.h"
#import "TunerFavsPage.h"
#import "hlbutton.h"
#import "crect.h"
#import "NSTunerControl.h"
#import "TunerContainerPage.h"
#import "CustomPage.h"

@implementation CTunerPageAMFM

#define DIGIT_DONE                          0x0A
#define CONTROL_INSETS                      4
#define CMD_TUNE_UP                         1000
#define CMD_TUNE_DOWN                       1001
#define CMD_SEEK_UP                         1002
#define CMD_SEEK_DOWN                       1003
#define CMD_HDTUNE_UP                       1004
#define CMD_HDTUNE_DOWN                     1005

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone StatusView:(CTunerHeaderView*)pStatusView

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    
    if (self)
    {
        m_pZone = pZone;
        m_pStatus = pStatusView;

        [m_pZone AddSink:self];
        [m_pZone InitData];
        m_pTunerControl = [[NSTunerControl alloc] init];

        
        //
        // Digits
        //
        int iDigit = 1;
        for (int iRow = 0; iRow < 4; iRow++)
        {
            for (int iCol = 0; iCol < 3; iCol++)
            {
                BOOL bOK = TRUE;
                if (iRow == 3)
                {
                    iDigit = 0;
                    if (iCol == 0 || iCol == 2)
                        bOK = FALSE;
                }

                if (bOK)
                {
                    NSString *pDigit = [NSString stringWithFormat:@"%d", iDigit];
                    CHLButton *pBtn = [self AddTextButton:pDigit];
                    m_pDigits[iRow*3+iCol] = pBtn;
                    switch (iDigit)
                    {
                    case 0: [pBtn addTarget:self action:@selector(Dig0:) forControlEvents:UIControlEventTouchDown];  break;
                    case 1: [pBtn addTarget:self action:@selector(Dig1:) forControlEvents:UIControlEventTouchDown];  break;
                    case 2: [pBtn addTarget:self action:@selector(Dig2:) forControlEvents:UIControlEventTouchDown];  break;
                    case 3: [pBtn addTarget:self action:@selector(Dig3:) forControlEvents:UIControlEventTouchDown];  break;
                    case 4: [pBtn addTarget:self action:@selector(Dig4:) forControlEvents:UIControlEventTouchDown];  break;
                    case 5: [pBtn addTarget:self action:@selector(Dig5:) forControlEvents:UIControlEventTouchDown];  break;
                    case 6: [pBtn addTarget:self action:@selector(Dig6:) forControlEvents:UIControlEventTouchDown];  break;
                    case 7: [pBtn addTarget:self action:@selector(Dig7:) forControlEvents:UIControlEventTouchDown];  break;
                    case 8: [pBtn addTarget:self action:@selector(Dig8:) forControlEvents:UIControlEventTouchDown];  break;
                    case 9: [pBtn addTarget:self action:@selector(Dig9:) forControlEvents:UIControlEventTouchDown];  break;
                    }

                    iDigit++;
                }
            }

        }

        m_pTuneLabel = [self AddLabel:@"TUNE"];
        m_pSeekLabel = [self AddLabel:@"SEEK"];
        m_pHDTuneLabel = [self AddLabel:@"HD CH."];

        m_pTuneBtns[0] = [self AddIconButton:ICON_TUNEUP   Command:CMD_TUNE_UP];
        m_pTuneBtns[1] = [self AddIconButton:ICON_TUNEDN   Command:CMD_TUNE_DOWN];
        m_pSeekBtns[0] = [self AddIconButton:ICON_TUNEUP   Command:CMD_SEEK_UP];
        m_pSeekBtns[1] = [self AddIconButton:ICON_TUNEDN   Command:CMD_SEEK_DOWN];
        m_pHDTuneBtns[0] = [self AddIconButton:ICON_TUNEUP Command:CMD_HDTUNE_UP];
        m_pHDTuneBtns[1] = [self AddIconButton:ICON_TUNEDN Command:CMD_HDTUNE_DOWN];

        if (!([m_pZone ActiveSubSource] & AUX_SOURCE_TUNER_MCAST_UPDOWN))
        {
            [m_pHDTuneBtns[0] setHidden:YES];
            [m_pHDTuneBtns[1] setHidden:YES];
        }


        [self Notify:0 Data:SINK_TUNERSTATE];
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTunerControl release];
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if ((iData & SINK_TUNERSTATE) == 0)
        return;

/*
    int iState = [m_pZone TunerState];

    switch (iState)
    {
    case TUNER_AM:
        {
            [m_pAM SetChecked:TRUE];
            [m_pFM SetChecked:FALSE];
        }
        break;
    case TUNER_FM:
        {
            [m_pAM SetChecked:FALSE];
            [m_pFM SetChecked:TRUE];
        }
        break;
    }
*/

    [self UpdateStationText];
    
/*
    if (m_pTextArtist != NULL)
        [m_pTextArtist setText: [m_pZone TunerArtist]];
    if (m_pTextTrack != NULL)
        [m_pTextTrack setText: [m_pZone TunerTrack]];
*/
}
/*============================================================================*/

-(CHLButton*)AddTextButton:(NSString*)pText

/*============================================================================*/
{
    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:pText];
    [self addSubview:pBtn];
    [pBtn release];
    return pBtn;
}
/*============================================================================*/

-(CHLButton*)AddIconButton:(int)iIcon Command:(int)iCmd

/*============================================================================*/
{
    int iIconFormat = HL_ICONCENTER;
    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:iIconFormat Style:iIcon Text:NULL TextSize:0 TextColor:NULL Color:NULL Icon:NULL];
    [pBtn SetCommandID:iCmd Responder:self];
    [self addSubview:pBtn];
    [pBtn release];
    return pBtn;
}
/*============================================================================*/

-(UILabel*)AddLabel:(NSString*)pText

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];

    int iTextSize = [CMainView TEXT_SIZE_SIDEBAR];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iTextSize = [CMainView DEFAULT_TEXT_SIZE];
        
    [CCustomPage InitStandardLabel:pLabel Size:iTextSize];
            
    [pLabel setText:pText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(IBAction)Dig0:(id)sender  {  [self ProcessDigit:CMD_DIG0];   }
-(IBAction)Dig1:(id)sender  {  [self ProcessDigit:CMD_DIG1];   }
-(IBAction)Dig2:(id)sender  {  [self ProcessDigit:CMD_DIG2];   }
-(IBAction)Dig3:(id)sender  {  [self ProcessDigit:CMD_DIG3];   }
-(IBAction)Dig4:(id)sender  {  [self ProcessDigit:CMD_DIG4];   }
-(IBAction)Dig5:(id)sender  {  [self ProcessDigit:CMD_DIG5];   }
-(IBAction)Dig6:(id)sender  {  [self ProcessDigit:CMD_DIG6];   }
-(IBAction)Dig7:(id)sender  {  [self ProcessDigit:CMD_DIG7];   }
-(IBAction)Dig8:(id)sender  {  [self ProcessDigit:CMD_DIG8];   }
-(IBAction)Dig9:(id)sender  {  [self ProcessDigit:CMD_DIG9];   }

-(IBAction)AMMode:(id)sender        { [m_pZone ZoneCommand:HL_PLAYLIST_SETTUNERMODE Data1:TUNER_AM Data2:0];                        }
-(IBAction)FMMode:(id)sender        { [m_pZone ZoneCommand:HL_PLAYLIST_SETTUNERMODE Data1:TUNER_FM Data2:0];                        }

/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    switch (iCommandID)
    {
    case CMD_TUNE_UP:       { [m_pZone TunerTune:1];    }   break;
    case CMD_TUNE_DOWN:     { [m_pZone TunerTune:-1];    }  break;
    case CMD_SEEK_UP:       { [m_pZone ZoneCommand:HL_PLAYLIST_NEXT Data1:0 Data2:0];     [m_pStatus SetStationText:@"SEEK UP"]; }   break;
    case CMD_SEEK_DOWN:     { [m_pZone ZoneCommand:HL_PLAYLIST_PREVIOUS Data1:0 Data2:0]; [m_pStatus SetStationText:@"SEEK DOWN"]; }   break;
    case CMD_HDTUNE_UP:     { [m_pZone HDTunerTune:1];    }   break;
    case CMD_HDTUNE_DOWN:   { [m_pZone HDTunerTune:-1];    }  break;
    }
}
/*============================================================================*/

-(void)ProcessDigit:(int)iKey

/*============================================================================*/
{
    // Already in entry mode?
    int iState = [m_pZone TunerState];
    int iSubSource = [m_pZone ActiveSubSource];

    int iRes = [m_pTunerControl ProcessDigit:iKey State:iState Type:iSubSource];

    if (iRes == 0)
        return;

    if (iRes > 0)
    {
        m_iDTune = 0;
        [m_pZone ZoneCommand:HL_PLAYLIST_SETFREQUENCY Data1:iRes Data2:0];
    }

    [self UpdateStationText];
}
/*============================================================================*/

-(void)UpdateStationText

/*============================================================================*/
{
    int iState = [m_pZone TunerState];
    int iSubSource = [m_pZone ActiveSubSource];
    NSString *pText = [m_pTunerControl ChannelText:[m_pZone TunerStation] State:iState Type:iSubSource StationName:[m_pZone TunerStationText]];
    [m_pStatus SetStationText:pText];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    CGRect rKeypad = [CRect BreakOffRight:&rBounds DX:rBounds.size.width / 2];

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        if (self.bounds.size.height > self.bounds.size.width)
        {
            rBounds = self.bounds;
            rKeypad = [CRect BreakOffTop:&rBounds DY:rBounds.size.height * 70 / 100];
        }
    }


    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [CRect CenterDownToY:&rKeypad DY:rKeypad.size.width * 4 / 3];
        [CRect CenterDownToX:&rKeypad DX:rKeypad.size.height * 3 / 4];
    }

    int iCol = 0;
    int iRow = 0;
    int iInset = rKeypad.size.width / 100;

    for (int i  = 0; i < 12; i++)
    {
        CGRect rRow = [CRect CreateYSection:rKeypad Index:iRow Of:4];
        CGRect rBtn = [CRect CreateXSection:rRow Index:iCol Of:3];
        [CRect Inset:&rBtn DX:iInset DY:iInset];
        [m_pDigits[i] setFrame:rBtn];
        iCol++;
        if (iCol >= 3)
        {
            iRow++;
            iCol = 0;
        }
    }


    BOOL bMCast = FALSE;
    if ([m_pZone ActiveSubSource] & AUX_SOURCE_TUNER_MCAST_UPDOWN)
        bMCast = TRUE;
        
    int iDXBtn = 100;
    int iDYBtn = 300;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        iDYBtn = 150;

    int iNSections = 2;
    if (bMCast)
        iNSections = 3;
        
    [CRect CenterDownToX:&rBounds DX:iNSections * iDXBtn];
    [CRect CenterDownToY:&rBounds DY:iDYBtn];

    for (int i = 0; i < iNSections; i++)
    {
        CGRect rSection = [CRect CreateXSection:rBounds Index:i Of:iNSections];
        int iDYLabel = 2*[CMainView DEFAULT_TEXT_SIZE];
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
            iDYLabel = [CMainView TEXT_SIZE_SIDEBAR];
        
        int iDYBtn = (rSection.size.height - iDYLabel)/2;
        CGRect rBtn1 = [CRect BreakOffTop:&rSection DY:iDYBtn];
        CGRect rBtn2 = [CRect BreakOffBottom:&rSection DY:iDYBtn];
        switch (i)
        {
        case 0:
            {
                [m_pTuneBtns[0] setFrame:rBtn1];
                [m_pTuneBtns[1] setFrame:rBtn2];
                [m_pTuneLabel setFrame:rSection];
            }
            break;
        case 1:
            {
                [m_pSeekBtns[0] setFrame:rBtn1];
                [m_pSeekBtns[1] setFrame:rBtn2];
                [m_pSeekLabel setFrame:rSection];
            }
            break;
        case 2:
            {
                [m_pHDTuneBtns[0] setFrame:rBtn1];
                [m_pHDTuneBtns[1] setFrame:rBtn2];
                [m_pHDTuneLabel setFrame:rSection];
            }
            break;
        }
    }
}


@end
