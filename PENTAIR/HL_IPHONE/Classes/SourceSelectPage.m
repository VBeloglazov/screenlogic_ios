//
//  MediaPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SourceSelectPage.h"
#import "SubTab.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "AudioZone.h"
#import "AudioSource.h"
#import "SubTabCell.h"
#import "MP3Menu.h"
#import "tnowplaying.h"
#import "VolumeLabel.h"
#import "MediaZonePage.h"
#import "crect.h"
#import "MainView.h"
#import "HLToolBar.h"
#import "hlbutton.h"

#define CMD_SOURCE_START            1000
#define CMD_SOURCE_END              1100

@implementation CSourceSelectPageIPAD


/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone ZonePage:(CMediaZonePage*)pPage;

/*============================================================================*/
{
    NSString *sTitle = @"Select Source";
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        sTitle = @"Source";
    

    self = [super initWithFrame:rect Title:sTitle];
    if (self)
    {
        [self setOpaque:NO];

        m_pZone = pZone;
        m_pZonePage = pPage;

        m_pButtons = [[NSMutableArray alloc] init];

        int iNSources = [m_pZone NSources];
        for (int i = 0; i < iNSources; i++)
        {
            CAudioSource *pSource = [m_pZone Source:i];
            CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:[pSource Name]];
            [pBtn SetIcon:pSource.m_pIconName];
            [pBtn IconFormat:HL_ICONTOP3X];
            [pBtn SetTextSize:[CMainView TEXT_SIZE_SIDEBAR]];
            [m_pButtons addObject:pBtn];
            [self addSubview:pBtn];
            [pBtn release];
            [pBtn SetCommandID:CMD_SOURCE_START+i Responder:self];

            UILabel *pLabel = [pBtn Label];
            [pLabel setLineBreakMode:UILineBreakModeTailTruncation];

            if (pSource.m_iSourceID == [m_pZone ActiveSourceID])
            {
                [pBtn SetChecked:YES];
            }
        }
    }
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    int iNSources = [m_pButtons count];
    int iDXBtn = 160;
    int iDYBtn = 160;
    int iDXBtn2 = 140;
    int iDYBtn2 = 140;
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iDXBtn = 84;
        iDYBtn = 84;
        iDXBtn2 = 80;
        iDYBtn2 = 80;
    }
    
    int iNCols  = MIN(iNSources, rThis.size.width / iDXBtn);
    iNCols      = MAX(iNCols, 1);
    int iNRows  = iNSources / iNCols;
    if ((iNSources % iNCols) != 0)
        iNRows++;
        
    [CRect CenterDownToX:&rThis DX:iNCols * iDXBtn];
    [CRect CenterDownToY:&rThis DY:iNRows * iDYBtn];

    int iRow = 0;
    int iCol = 0;
    
    for (int i = 0; i < iNSources; i++)
    {
        CGRect rCol = [CRect CreateXSection:rThis Index:iCol Of:iNCols];
        CGRect rSrc = [CRect CreateYSection:rCol Index:iRow Of:iNRows];

        [CRect CenterDownToX:&rSrc DX:iDXBtn2];
        [CRect CenterDownToY:&rSrc DY:iDXBtn2];

        CHLButton *pBtn = (CHLButton*)[m_pButtons objectAtIndex:i];
        [pBtn setFrame:rSrc];
        iCol++;
        if (iCol >= iNCols)
        {
            iRow++;
            iCol = 0;
        }
    }
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)UpdateActiveSource

/*============================================================================*/
{
    int iIndex = [m_pZone ActiveSourceIndex];
    
    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CHLButton *pBtn = (CHLButton*)[m_pButtons objectAtIndex:i];
        [pBtn SetChecked:(i == (iIndex-1))];
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (iCommandID >= (CMD_SOURCE_START-1) && iCommandID <= CMD_SOURCE_END)
    {
        int iIndex = (iCommandID - CMD_SOURCE_START + 1);
        if (iIndex == [m_pZone ActiveSourceIndex])
        {
            [m_pZonePage ReleaseSelectPage];
            return;
        }


        [m_pZone SetActiveSourceIndex:iIndex];
        [self UpdateActiveSource];
    }
}

@end

