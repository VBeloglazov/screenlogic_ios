//
//  SecZonePage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Sink.h"

@class CSecPartition;
@class CSecZone;

/*============================================================================*/

@interface CSecZonePage : UIView        <   UITableViewDelegate,
                                            UITableViewDataSource  >

/*============================================================================*/
{
    CSecPartition                       *m_pPartition;
    UITableView                         *m_pTableView;
}

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition;
+(int)RowHeight;
+(int)TextHeight;

@end
