//
//  AudioZone.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "AudioZone.h"
#import "NSQuery.h"
#import "AudioSource.h"
#import "AudioService.h"
#import "PlaylistItem.h"
#import "NSMSG.h"
#import "HLComm.h"
#import "VolumeLabel.h"
#import "VolumeButton.h"
#import "VolumePopupView.h"
#import "SubTabCell.h"
#import "MainView.h"
#import "crect.h"
#import "IPAD_ViewController.h"
#import "IPOD_ViewController.h"
#import "NSTunerControl.h"

@implementation CAudioZone

@synthesize m_iVolumeType;
@synthesize m_iVolume;
@synthesize m_sArtPath;

    static int                      g_iGlobalSortOptions = 0;

/*============================================================================*/

-(id)initFromQ:(CNSQuery*)pQ

/*============================================================================*/
{
    if (self == [super init])
    {
        int iID         = [pQ GetInt];
        NSString *pName = [pQ GetString];

        [super initWithName:pName ID:iID Data1:0 Data2:0];

        m_iVolumeType   = [pQ GetInt];
        m_iConfigID     = [pQ GetInt];
        m_bHasMute      = [pQ GetInt];

        m_pSinkServer = [[CSinkServer alloc] init];
    
        m_pSources = [[NSMutableArray alloc] init];

        int iNGroups = [pQ GetInt];
        if (iNGroups > 10)
            iNGroups = 0;
        for (int iGroup = 0; iGroup < iNGroups; iGroup++)
        {
            [pQ GetString]; // Group Name : Unused for now
            int iNSources = [pQ GetInt];
            for (int iSource = 0; iSource < iNSources; iSource++)
            {
                CAudioSource *pS = [[CAudioSource alloc] initFromQ:pQ];
                [m_pSources addObject:pS];
                [pS release];
            }
        }

        m_pServices         = [[NSMutableArray alloc] init];
        m_pPlaylistItems    = [[NSMutableArray alloc] init];

        [CHLComm AddSocketSink:self];
        [self InitComm];
        [self InitData];
    }
    return self;
}
/*============================================================================*/

+(int)GlobalSortOptions

/*============================================================================*/
{
    return g_iGlobalSortOptions;
}
/*============================================================================*/

+(void)GlobalSortOptions:(int)iNew

/*============================================================================*/
{
    g_iGlobalSortOptions = iNew;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pSinkServer release];
    [m_pSources release];
    [m_pVolTimer invalidate];
    [m_pVolTimer release];

    [m_pServices release];
    [m_pPlaylistItems release];

    [m_psTunerArtist release];
    [m_psTunerTrack release];
    [m_psTunerStation release];

    [super dealloc];
}
/*============================================================================*/

-(int)NSources

/*============================================================================*/
{
    return [m_pSources count];
}
/*============================================================================*/

-(CAudioSource*)Source:(int)iIndex

/*============================================================================*/
{
    return (CAudioSource*)[m_pSources objectAtIndex:iIndex];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        [self InitComm];
        [self InitData];
    }
    else 
    {
        m_bDataInit = FALSE;
    }

}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iMSGID = [pMSG MessageID];
    int iChangeType = 0;
    switch(iMSGID)
    {
    case HLM_AUDIO_ZONESOURCECHANGED:
    case HLM_AUDIO_GETZONESOURCEIDA:
        {
            //
            // The source for our current zone has changed
            //
            int iActiveSourceID = [pMSG GetInt];
            int iActiveSubSource = [pMSG GetInt];
            int iActiveUserData = [pMSG GetInt];
            [pMSG GetInt]; // Busy
            m_bDataInit = FALSE;

            [m_pServices removeAllObjects];
            int iNServices = [pMSG GetInt];
            for (int iService = 0; iService < iNServices; iService++)
            {
                CAudioService *pService = [[CAudioService alloc] initWithMSG:pMSG];
                [m_pServices addObject:pService];
                [pService release];
            }

            BOOL bChanged = FALSE;
            if (iActiveSourceID  != m_iActiveSourceID) bChanged = TRUE;
            if (iActiveSubSource != m_iActiveSubSource) bChanged = TRUE;
            if (iActiveUserData  != m_iActiveUserData) bChanged = TRUE;

            m_iActiveSourceID = iActiveSourceID;
            m_iActiveSubSource = iActiveSubSource;
            m_iActiveUserData = iActiveUserData;
            
            if (bChanged)
            {
                m_iActiveItemIndex = -1;
                m_iPlayState = PLAYLIST_IDLE;
                [m_pPlaylistItems removeAllObjects];
                
                iChangeType = SINK_SOURCECHANGED;
                CSubTabCell *pCell = [self GetSubTabCell];
                if (pCell != NULL)
                {
                    [self UpdateSubTabCell:pCell];
                }

            }
            m_bStateOK = TRUE;
        }
        break;

    case HLM_AUDIO_ZONEVOLCHANGED:
    case HLM_AUDIO_GETZONEVOLUMEA:
        {
            //
            // The volume for our zone has changed
            //
            m_iVolume = [pMSG GetInt];
            m_bMute = [pMSG GetInt];
            iChangeType = SINK_VOLCHANGED;
        }
        break;

    case HLM_AUDIO_GETALLITEMSA:
    case HLM_AUDIO_ITEMSCHANGED:
        {
            NSMutableArray *pList = [[NSMutableArray alloc] init];
            int iNItems = [pMSG GetInt];
            for (int iItem = 0; iItem < iNItems; iItem++)
            {
                CPlaylistItem *pItem = [[CPlaylistItem alloc] initWithMSG:pMSG];
                [pList addObject:pItem];
                [pItem release];
            }

            if ([self UpdatePlaylist:pList])
            {
                iChangeType = SINK_ITEMSCHANGED;
                m_wPlaylistVersion++;
            }
        }
        break;

    case HLM_AUDIO_GETSTATEA:
    case HLM_AUDIO_STATECHANGED:
        {
            iChangeType |= [self ProcessStateMSG:pMSG];

        }
        break;

    case HLM_AUDIO_ZONEARTCHANGED:
        {
            [self CheckZoneArt];
        }
        break;

    case HLM_AUDIO_GETZONEARTA:
        {
            [m_sArtPath release];
            m_sArtPath = [pMSG GetString];
            [m_sArtPath retain];
        }
        break;

    case HLM_PICSERVER_IMAGEDATA:
        {
            [pMSG GetInt]; //DX
            [pMSG GetInt]; //DY
            free(m_pJPEG);
            m_iNJPEG = [pMSG DataSize]-8;
            m_pJPEG = malloc(m_iNJPEG);
            memcpy(m_pJPEG, [pMSG GetDataAtReadIndex], m_iNJPEG);
            m_wArtworkVersion++;
            iChangeType = SINK_ARTCHANGED;
        }
        break;
    }

    if (iChangeType != 0)
    {
        [m_pSinkServer NotifySinks:m_iID Data:iChangeType];
    }
}
/*============================================================================*/

-(BOOL)HasSettings

/*============================================================================*/
{
    return (m_iConfigID != HLID_NONE);
}
/*============================================================================*/

-(int)SettingsID

/*============================================================================*/
{
    return m_iConfigID;
}
/*============================================================================*/

-(BOOL)HasMute

/*============================================================================*/
{
    return m_bHasMute;
}
/*============================================================================*/

-(void)SetMute:(BOOL)bMute

/*============================================================================*/
{
    if (m_iVolumeType == VOLUME_TYPE_RAMP)
    {
        [self ZoneCommand:HL_PLAYLIST_TOGGLEMUTE Data1:0 Data2:0];
    }
    else
    {
        CNSQuery *pV = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SETZONEVOLUMEQ];
        [pV autorelease];
        [pV PutInt:m_iID];
        [pV PutInt:m_iVolume];
        [pV PutInt:bMute];
        [pV SendMessageWithID:[CHLComm SenderID:self]];
    }
}
/*============================================================================*/

-(BOOL)GetMute

/*============================================================================*/
{
    return m_bMute;
}
/*============================================================================*/

-(int)ProcessStateMSG:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iMSGID = [pMSG MessageID];

    if ([self ActiveMP3])
    {
        //
        // MP3 State
        //

        int iMode = [pMSG GetInt];
        //BOOL bAutoPilot = (iMode & MP3SOURCE_AUTOPILOTMODE);
        iMode = iMode & 0x0000FFFF;

        if (iMode == MP3SOURCE_IRADIO)
        {
            [m_psTunerTrack release];
            [m_psTunerArtist release];
            [m_psTunerStation release];
            m_psTunerStation = [[pMSG GetString] retain];
            m_psTunerArtist = [[pMSG GetString] retain];
            m_psTunerTrack = [[pMSG GetString] retain];
            m_wPlaystateVersion++;
            return SINK_STATECHANGED;
        }


        if (iMode != MP3SOURCE_PLAYLIST)
            return 0;
        m_bRepeat = [pMSG GetInt];
        m_bRandom = [pMSG GetInt];
        m_iPlayState = [pMSG GetInt];
        m_iActiveItemIndex = [pMSG GetInt];
        m_iProgressPct = [pMSG GetInt];

        if (iMSGID == HLM_AUDIO_STATECHANGED)
        {
            m_wPlaystateVersion++;
            return SINK_STATECHANGED;
        }
    }
    
    if ([self ActiveTunerAMFM] || [self ActiveTunerXM])
    {
        //
        // AM/FM Tuner State
        //
        m_iTunerState = [pMSG GetInt];
        m_iTunerStation = [pMSG GetInt];
        m_iTunerFavoriteID = [pMSG GetInt];
        [m_psTunerTrack release];
        [m_psTunerArtist release];
        [m_psTunerStation release];
        m_psTunerArtist  = [[pMSG GetString] retain];
        m_psTunerTrack   = [[pMSG GetString] retain];
        m_psTunerStation = [[pMSG GetString] retain];
        return SINK_TUNERSTATE;
    }

    return 0;
}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    int iSenderID = [CHLComm SenderID:self];

    // Add as Client
    CNSQuery *Q1 = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDZONECLIENTQ];
    [Q1 autorelease];
    [Q1 PutInt:m_iID];
    [Q1 PutInt:iSenderID];
    [Q1 PutInt:0];
    [Q1 SendMessageWithID:iSenderID];

    // Get the source
    CNSQuery *Q2 = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETZONESOURCEIDQ];
    [Q2 autorelease];
    [Q2 PutInt:m_iID];
    [Q2 SendMessageWithID:iSenderID];

    // Get the volume
    CNSQuery *Q3 = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETZONEVOLUMEQ];
    [Q3 autorelease];
    [Q3 PutInt:m_iID];
    [Q3 SendMessageWithID:iSenderID];
}
/*============================================================================*/

-(void)InitData

/*============================================================================*/
{
    if (m_bDataInit)
        return;
    m_bDataInit = TRUE;

    CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETALLITEMSQ];
    CNSQuery *p2 = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETSTATEQ];
    [p1 autorelease];
    [p2 autorelease];

    [p1 PutInt:m_iID];
    [p2 PutInt:m_iID];

    [p1 SendMessageWithMyID:self];
    [p2 SendMessageWithMyID:self];

    [self CheckZoneArt];
}
/*============================================================================*/

-(BOOL)UpdatePlaylist:(NSMutableArray*)pList

/*============================================================================*/
{
    if ([self IsSamePlaylist:pList])
    {
        [pList release];
        return FALSE;
    }

    [m_pPlaylistItems release];
    m_pPlaylistItems = pList;
    
    return TRUE;
}
/*============================================================================*/

-(BOOL)IsSamePlaylist:(NSMutableArray*)pList

/*============================================================================*/
{
    if (m_pPlaylistItems == NULL)
        return FALSE;
    if ([m_pPlaylistItems count] != [pList count])
        return FALSE;
    for (int iItem = 0; iItem < [m_pPlaylistItems count]; iItem++)
    {
        CPlaylistItem *p1 = (CPlaylistItem*)[m_pPlaylistItems objectAtIndex:iItem];
        CPlaylistItem *p2 = (CPlaylistItem*)[pList objectAtIndex:iItem];
        if (![p1 IsSameItem:p2])
            return FALSE;
    }
    
    return TRUE;
}
/*============================================================================*/

-(void)CheckZoneArt

/*============================================================================*/
{
    int iDim = 320;
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_GETZONEARTQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iDim];
    [pQ PutInt:iDim];
    [pQ PutString:m_sArtPath];
    [pQ SendMessageWithID:[CHLComm SenderID:self]];
    
}
/*============================================================================*/

-(BOOL)ActiveMP3

/*============================================================================*/
{
    int iBasicType = (m_iActiveSubSource & 0x0000FFFF);
    switch(iBasicType)
    {
    case MP3SOURCE_PLAYLIST:
    case MP3SOURCE_IRADIO:
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(BOOL)ActiveTunerAMFM

/*============================================================================*/
{
    int iBasicType = (m_iActiveSubSource & 0x0000FFFF);
    switch(iBasicType)
    {
    case AUX_SOURCE_TUNER:
    case AUX_SOURCE_TUNER_AMFM_EURO50:
    case AUX_SOURCE_TUNER_AMFM_EURO100:
    case AUX_SOURCE_TUNER_HDAMFM:
    
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(BOOL)ActiveTunerXM

/*============================================================================*/
{
    int iBasicType = (m_iActiveSubSource & 0x0000FFFF);
    switch(iBasicType)
    {
    case AUX_SOURCE_TUNER_XM:
    case AUX_SOURCE_TUNER_SIRIUS:
    case AUX_SOURCE_TUNER_AMFMXM:
    case AUX_SOURCE_TUNER_AMFMSIRIUS:
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

-(int)MP3Flags

/*============================================================================*/
{
    return (m_iActiveSubSource & 0xFFFF0000);
}
/*============================================================================*/

-(int)ActiveSubSource

/*============================================================================*/
{
    return m_iActiveSubSource;
}
/*============================================================================*/

-(int)ActiveUserData

/*============================================================================*/
{
    return m_iActiveUserData;
}
/*============================================================================*/

-(int)NServices

/*============================================================================*/
{
    return [m_pServices count];
}
/*============================================================================*/

-(int)Volume

/*============================================================================*/
{
    if (m_iVolDir != 0)
        return m_iLocalVol;
    return m_iVolume;
}
/*============================================================================*/

-(int)ProgressPercent

/*============================================================================*/
{
    return m_iProgressPct;
}
/*============================================================================*/

-(int)PlayState

/*============================================================================*/
{
    return m_iPlayState;
}
/*============================================================================*/

-(BOOL)HasSort:(int)iSortType

/*============================================================================*/
{
    int iMP3Flags = [self MP3Flags];
    if (iSortType & SORT_ARTIST)
    {
        if (iMP3Flags & MP3SOURCE_NOSORTARTIST)
            return FALSE;
    }
    if (iSortType & SORT_TRACK)
    {
        if (iMP3Flags & MP3SOURCE_NOSORTTRACKS)
            return FALSE;
    }
    if (iSortType & SORT_GENRE)
    {
        return ((iMP3Flags & MP3SOURCE_HASGENRESORT) != 0);
    }

    return ((g_iGlobalSortOptions & iSortType) != 0);
}
/*============================================================================*/

-(BOOL)Repeat                           { return m_bRepeat; }
-(BOOL)Random                           { return m_bRandom; }

/*============================================================================*/

-(CAudioService*)Service:(int)iIndex

/*============================================================================*/
{
    CAudioService *pService = (CAudioService*)[m_pServices objectAtIndex:iIndex];
    return pService;
}
/*============================================================================*/

-(int)ActiveSourceID

/*============================================================================*/
{
    return m_iActiveSourceID;
}
/*============================================================================*/

-(int)ActiveSourceIndex

/*============================================================================*/
{
    for (int i = 0; i < [m_pSources count]; i++)
    {
        CAudioSource *pS = [self Source:i];
        if (pS.m_iSourceID == m_iActiveSourceID)
        {
            return (i+1);
        }
    }

    return 0;
}
/*============================================================================*/

-(void)SetActiveSourceIndex:(int)iIndex

/*============================================================================*/
{
    BOOL bChanged = iIndex != [self ActiveSourceIndex];

    if (iIndex <= 0)
    {
        m_iActiveSourceID = HLID_NONE;
        CNSQuery *pOff = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ZONECOMMANDQ];
        [pOff autorelease];
        [pOff PutInt:m_iID];
        [pOff PutInt:HL_PLAYLIST_OFF];
        [pOff PutInt:0];
        [pOff PutInt:0];
        [pOff SendMessageWithID:[CHLComm SenderID:self]];


        if (bChanged)
            [m_pSinkServer NotifySinks:m_iID Data:SINK_SOURCECHANGED];
        return;
    }
    
    CAudioSource *pS = [self Source:(iIndex-1)];
    CNSQuery *pSet = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SETZONESOURCEIDQ];
    [pSet autorelease];
    [pSet PutInt:m_iID];
    [pSet PutInt:pS.m_iSourceID];
    [pSet PutInt:pS.m_iRemoteID];
    [pSet PutInt:0];
    [pSet SendMessageWithID:[CHLComm SenderID:self]];
/*
    m_iActiveSourceID = pS.m_iSourceID;

    if (bChanged)
        [m_pSinkServer NotifySinks:m_iID Data:SINK_SOURCECHANGED];
*/
}
/*============================================================================*/

-(IBAction)VolDownEvent:(id)sender

/*============================================================================*/
{
    [self StopVolume];
    [self PopVolumeUI:-1];
    if ([self OnVolumeDownStart])
        return;

    [self StartTimer:-1];
}
/*============================================================================*/

-(IBAction)VolUpEvent:(id)sender

/*============================================================================*/
{
    [self StopVolume];
    [self PopVolumeUI:1];
    if ([self OnVolumeUpStart])
        return;
    [self StartTimer:1];
}
/*============================================================================*/

-(void)VolReleaseEvent

/*============================================================================*/
{
    [m_pSinkServer NotifySinks:m_iID Data:SINK_UIVOLUMEEND];
    if ([self OnVolumeEnd])
        return;
    [self StopTimer];
    return;
}
/*============================================================================*/

-(void)StopVolume

/*============================================================================*/
{
    if (m_iVolDir != 0)
        [self VolReleaseEvent];
}
/*============================================================================*/

-(void)SetVolume:(int)iNew

/*============================================================================*/
{
    CNSQuery *pV = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SETZONEVOLUMEQ];
    [pV autorelease];
    [pV PutInt:m_iID];
    [pV PutInt:iNew];
    [pV PutInt:m_bMute];
    [pV SendMessageWithID:[CHLComm SenderID:self]];
}
/*============================================================================*/

-(BOOL)OnVolumeDownStart;

/*============================================================================*/
{
    printf("VOLDOWNSTART\n");
    if (m_iVolumeType != VOLUME_TYPE_RAMP)
        return FALSE;
    [self ZoneCommand:HL_PLAYLIST_BEGINRAMPVOLDN Data1:0 Data2:0];
    m_iVolDir = -1;
    if ([CHLComm VersionMajor] >= 5)
        m_pRampTimer = [[NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(VolRampTimerEvent) userInfo:nil repeats:YES] retain];
    return TRUE;
}
/*============================================================================*/

-(BOOL)OnVolumeUpStart;

/*============================================================================*/
{
    printf("VOLUPSTART\n");
    if (m_iVolumeType != VOLUME_TYPE_RAMP)
        return FALSE;
    [self ZoneCommand:HL_PLAYLIST_BEGINRAMPVOLUP Data1:0 Data2:0];
    m_iVolDir = 1;
    if ([CHLComm VersionMajor] >= 5)
        m_pRampTimer = [[NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(VolRampTimerEvent) userInfo:nil repeats:YES] retain];
    return TRUE;
}
/*============================================================================*/

-(BOOL)OnVolumeEnd

/*============================================================================*/
{
    printf("VOLRELEASE\n");
    if (m_iVolumeType != VOLUME_TYPE_RAMP)
        return FALSE;
    [m_pRampTimer invalidate];
    [m_pRampTimer release];
    m_pRampTimer = NULL;

    if (m_iVolDir < 0)
        [self ZoneCommand:HL_PLAYLIST_ENDRAMPVOLDN Data1:0 Data2:0];
    else if (m_iVolDir > 0)
        [self ZoneCommand:HL_PLAYLIST_ENDRAMPVOLUP Data1:0 Data2:0];
    m_iVolDir = 0;
    return TRUE;
}
/*============================================================================*/

-(void)ZoneCommand:(int)iCommand Data1:(int)iData1 Data2:(int)iData2

/*============================================================================*/
{
    CNSQuery *pC = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ZONECOMMANDQ];
    [pC autorelease];
    [pC PutInt:m_iID];
    [pC PutInt:iCommand];
    [pC PutInt:iData1];
    [pC PutInt:iData2];
    [pC SendMessageWithID:[CHLComm SenderID:self]];
}
/*============================================================================*/

-(void)PlayTrackIndex:(int)iIndex

/*============================================================================*/
{
    CNSQuery *pC = [[CNSQuery alloc] initWithQ:HLM_AUDIO_PLAYTRACKQ];
    [pC autorelease];
    [pC PutInt:m_iID];
    [pC PutInt:iIndex];
    [pC SendMessageWithID:[CHLComm SenderID:self]];
}
/*============================================================================*/

-(void)AddSortedItem:(int)iSortType Sub1:(int)iSub1 Sub2:(int)iSub2 Index:(int)iIndex Play:(BOOL)bPlay

/*============================================================================*/
{
    CNSQuery *pC = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ADDSORTITEMQ];
    [pC autorelease];
    [pC PutInt:m_iID];
    [pC PutInt:iSortType];
    [pC PutInt:iSub1];
    [pC PutInt:iSub2];
    [pC PutInt:iIndex];
    [pC PutInt:bPlay];
    [pC PutInt:[self ActiveSourceID]];
    [pC SendMessageWithID:[CHLComm SenderID:self]];
}
/*============================================================================*/

-(UIImage*)GetImage

/*============================================================================*/
{
    if (m_iNJPEG == 0 || m_pJPEG == NULL)
        return NULL;
    NSData *pData = [NSData dataWithBytes:m_pJPEG length:m_iNJPEG];
    UIImage *pImage = [UIImage imageWithData:pData];
    return pImage;
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
}
/*============================================================================*/

-(void)StartTimer:(int)iDir

/*============================================================================*/
{
    m_pVolTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(VolTimerEvent) userInfo:nil repeats:YES] retain];
    m_iTimerInit = 0;
    m_iLocalVol = m_iVolume;
    m_iVolDir = iDir;
    [self VolTimerEvent];
}
/*============================================================================*/

-(void)StopTimer

/*============================================================================*/
{
    if (m_pVolTimer == NULL)
        return;
    [m_pVolTimer invalidate];
    [m_pVolTimer release];
    m_pVolTimer = NULL;
    m_iVolDir = 0;
}
/*============================================================================*/

- (void)VolTimerEvent

/*============================================================================*/
{
    m_iTimerInit++;
    if (m_iTimerInit > 1 && m_iTimerInit < 4)
        return;

    int iNewVol = m_iLocalVol + m_iVolDir;
    iNewVol = MIN(100, iNewVol);
    iNewVol = MAX(0, iNewVol);
    if (iNewVol == m_iLocalVol)
        return;
    m_iLocalVol = iNewVol;
    [self SetVolume:m_iLocalVol];
     [m_pSinkServer NotifySinks:m_iID Data:SINK_VOLCHANGED];
}
/*============================================================================*/

- (void)VolRampTimerEvent

/*============================================================================*/
{
    switch (m_iVolDir)
    {
    case 1:  [self ZoneCommand:HL_PLAYLIST_BEGINRAMPVOLUP Data1:0 Data2:0]; break;
    case -1: [self ZoneCommand:HL_PLAYLIST_BEGINRAMPVOLDN Data1:0 Data2:0]; break;
    }
}
/*============================================================================*/

-(int)ActivePlaylistItem

/*============================================================================*/
{
    return m_iActiveItemIndex;
}
/*============================================================================*/

-(int)NPlaylistItems

/*============================================================================*/
{
    return [m_pPlaylistItems count];
}
/*============================================================================*/

-(CPlaylistItem*)PlaylistItem:(int)iIndex

/*============================================================================*/
{
    CPlaylistItem *pItem = (CPlaylistItem*)[m_pPlaylistItems objectAtIndex:iIndex];
    return pItem;
}
/*============================================================================*/

-(int)TunerState                 { return m_iTunerState;      }
-(int)TunerStation               { return m_iTunerStation;    }
-(int)TunerFavoriteID            { return m_iTunerFavoriteID; }
-(NSString*)TunerArtist          { return m_psTunerArtist;    }
-(NSString*)TunerTrack           { return m_psTunerTrack;     }
-(NSString*)TunerStationText     { return m_psTunerStation;   }

/*============================================================================*/

-(void)TunerTune:(int)iDir

/*============================================================================*/
{
    int iState = [self TunerState];
    int iSubSource  = [self ActiveSubSource];

    NSTunerControl *pCtrl = [[[NSTunerControl alloc] init] autorelease];
    int iStep = [pCtrl DTune:iState Type:iSubSource];

    iStep = iStep * iDir;

    m_iTunerStation = [pCtrl PrepareDTune:iState Type:iSubSource Station:m_iTunerStation];

    int iNewStation = m_iTunerStation + iStep;
    if (iNewStation > [pCtrl MaxStation:iState Type:iSubSource])
        iNewStation = [pCtrl MinStation:iState Type:iSubSource];
    if (iNewStation < [pCtrl MinStation:iState Type:iSubSource])
        iNewStation = [pCtrl MaxStation:iState Type:iSubSource];
        
    [self ZoneCommand:HL_PLAYLIST_SETFREQUENCY Data1:iNewStation Data2:0];
    m_iTunerStation = iNewStation;
    [m_pSinkServer NotifySinks:m_iID Data:SINK_TUNERSTATE];
}
/*============================================================================*/

-(void)HDTunerTune:(int)iDir

/*============================================================================*/
{
    int iState = [self TunerState];
    int iSubSource  = [self ActiveSubSource];
    if (iState != TUNER_FM)
        return;
    if (!(iSubSource & AUX_SOURCE_TUNER_MCAST_CHANNELS))
        return;

    NSTunerControl *pCtrl = [[[NSTunerControl alloc] init] autorelease];
    int iCmd = 0;

    if (iDir > 0)
    {
        m_iTunerStation = [pCtrl NextSubChannel:m_iTunerStation];
        iCmd = HL_PLAYLIST_NEXT;
    }
    else
    {
        m_iTunerStation = [pCtrl PrevSubChannel:m_iTunerStation];
        iCmd = HL_PLAYLIST_PREVIOUS;
    }

    int iFreq = m_iTunerStation / 10;
    int iSubChannel = iFreq % 10;
    
        
    [self ZoneCommand:iCmd Data1:1 Data2:iSubChannel];
    [m_pSinkServer NotifySinks:m_iID Data:SINK_TUNERSTATE];
}
/*============================================================================*/

+(void)AddVolumeButtons:(NSMutableArray*)pArray Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    CVolumeButton *pDn = [[CVolumeButton alloc] initWithZone:pZone ImageName:@"VOL_DN.png"];
    CVolumeButton *pUp = [[CVolumeButton alloc] initWithZone:pZone ImageName:@"VOL_UP.png"];

    pDn.frame = CGRectMake(0, 0, 40, 40);
    pUp.frame = CGRectMake(0, 0, 40, 40);

    CVolumeLabel *pVolume  = [[[CVolumeLabel alloc] initWithFrame:CGRectMake(0, 0, 40, 32) Zone:pZone] autorelease];

    UIBarButtonItem *p1 = [[[UIBarButtonItem alloc] initWithCustomView:pDn] autorelease];
    UIBarButtonItem *p2 = [[[UIBarButtonItem alloc] initWithCustomView:pVolume] autorelease];
    UIBarButtonItem *p3 = [[[UIBarButtonItem alloc] initWithCustomView:pUp] autorelease];
    [pArray addObject:p1];
    [pArray addObject:p2];
    [pArray addObject:p3];
    [pDn addTarget:pZone action:@selector(VolDownEvent:) forControlEvents:UIControlEventTouchDown];
    [pUp addTarget:pZone action:@selector(VolUpEvent:) forControlEvents:UIControlEventTouchDown];
    [pUp release];
    [pDn release];
}
/*============================================================================*/

-(void)PopVolumeUI:(int)iDir

/*============================================================================*/
{
    UIApplication *pApp = [UIApplication sharedApplication];
    if (pApp == NULL)
        return;

    UIView *pWnd = NULL;
    int iDYVol = 55;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pWnd = [IPAD_ViewController MainViewController].view;
        iDYVol = 150;
    }
    else
    {
        pWnd = [IPOD_ViewController MainViewController].view;
    }
        
    if (pWnd == NULL)
        return;

    CGRect rVol = pWnd.bounds;
    rVol.origin.y = rVol.size.height / 2 - iDYVol / 2;
    rVol.size.height = iDYVol;
    
    rVol.origin.x = 0;
    rVol.origin.y = 0;
    rVol.size.height = [CMainView DY_TOOLBAR];

    if (self.m_iVolumeType == VOLUME_TYPE_RAMP)
        return;

    CVolumePopupView *pPop = [[CVolumePopupView alloc] initWithFrame:rVol Zone:self Direction:iDir];
    pPop.alpha = 0;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    [pPop setAlpha:1.0];
    [UIView commitAnimations];

    [pWnd addSubview:pPop];
    [pPop release];
}
/*============================================================================*/

-(void)SetSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    [super SetSubTabCell:pSubTabCell];
    [self UpdateSubTabCell:pSubTabCell];
}
/*============================================================================*/

-(void)UpdateSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    NSString *pIconName = NULL;

    int iSourceIndex = [self ActiveSourceIndex];
    if (iSourceIndex > 0)
    {
        CAudioSource *pSrc = [self Source:(iSourceIndex-1)];
        if (pSrc != NULL)
            pIconName = pSrc.m_pIconName;
        [pSubTabCell SetUserIcon:pIconName];
        return;
    }

    [pSubTabCell SetEmbeddedIcon:@"POWER.png"];

}
@end
