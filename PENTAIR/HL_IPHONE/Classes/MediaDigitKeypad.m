//
//  MediaDigitKeypad.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/15/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MediaDigitKeypad.h"
#import "hlm.h"
#import "hlbutton.h"
#import "NSQuery.h"
#import "crect.h"

@implementation CMediaDigitKeypad


/*============================================================================*/

-(id)initWithFrame: (CGRect)rView
                    Style:(int)iStyle
                    TextColor:(UIColor*)pRGBText
                    Color:(UIColor*)pRGBFace
                    TextSize:(int)iTextSize
                    Data:(int)iData
                    ZoneID:(int)iZoneID

/*============================================================================*/
{
    self = [super initWithFrame:rView];
    if (self)
    {
        m_iData   = iData;
        m_iZoneID = iZoneID;
        
        for (int iRow = 0; iRow < 4; iRow++)
        {
            for (int iCol = 0; iCol < 3; iCol++)
            {
                int iDigit = 1 + iRow*3+iCol;
                int iCmdID = CMD_KEY_0+iDigit;
                NSString *pText = [NSString stringWithFormat:@"%d", iDigit];


                BOOL bKeyOK = TRUE;
                if (iRow == 3)
                {
                    iCmdID = CMD_KEY_0;
                    pText = @"0";

                    if (iCol == 0)
                    {
                        bKeyOK = FALSE;
                        if (iStyle & DIGITKEYPAD_CANCEL)
                        {
                            bKeyOK = TRUE;
                            iCmdID = CMD_KEY_CLEAR;
                            pText = @"Cancel";
                        }
                    }
                    if (iCol == 2)
                    {
                        bKeyOK = FALSE;
                        if (iStyle & DIGITKEYPAD_ENTER)
                        {
                            bKeyOK = TRUE;
                            iCmdID = CMD_KEY_ENTER;
                            pText = @"Enter";
                        }
                    }
                }

                if (bKeyOK)
                {
                    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:AUDIO_ICON_TEXT Text:pText TextSize:iTextSize TextColor:pRGBText Color:pRGBFace Icon:@""];
                    [self addSubview:pBtn];
                    m_pBtns[iRow * 3 + iCol] = pBtn;


                    pBtn.autoresizingMask = 0xFFFFFFFF;
                    [pBtn SetCommandID:iCmdID Responder:self];
                    [pBtn release];
                }
            }
        }
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    for (int iRow = 0; iRow < 4; iRow++)
    {
        CGRect rRow = [CRect CreateYSection:rBounds Index:iRow Of:4];
        for (int iCol = 0; iCol < 3; iCol++)
        {
            CHLButton *pBtn = m_pBtns[iRow*3+iCol];
            if (pBtn != NULL)
            {
                CGRect rBtn = [CRect CreateXSection:rRow Index:iCol Of:3];
                [CRect Inset:&rBtn DX:2 DY:2];
                [pBtn setFrame:rBtn];
            }
        }
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_EXECDEVICEKEYQ];
    [pQ autorelease];
    [pQ PutInt:m_iData];
    [pQ PutInt:iCommandID];
    [pQ PutInt:m_iZoneID];
    [pQ SendMessage];
}

@end
