//
//  SecKeypad.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecKeypad.h"
#import "SecPartition.h"
#import "ArmingMode.h"
#import "hlbutton.h"
#import "hlm.h"
#import "NSQuery.h"

@implementation CSecKeypad

#define MAX_BUTTON_DY       40

#define CMD_ARM_START       2000
#define CMD_ARM_END         2010

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame])
    {
        m_pPartition = pPartition;
        [m_pPartition AddSink:self];
        int iNModes = [m_pPartition NVisibleArmingModes];

        int iDXThis = rFrame.size.width;
        int iDYThis = rFrame.size.height;
        int iNCols = iNModes;
        int iNRows = 1;
        if (iNCols > 4)
        {
            iNRows++;
            iNCols = 1;
            while (iNCols * iNRows < iNModes)
                iNCols++;
        }
        
        iNCols = MAX(1, iNCols);
        int iDXModeCol = (iDXThis - 20) / iNCols;
        int iDYModeRow = iDYThis / (5+iNRows);
        iDYModeRow = MIN(iDYModeRow, 80);

        m_pModeButtons = [[NSMutableArray alloc] init];
        m_pDigitButtons = [[NSMutableArray alloc] init];

        int iCol = 0;
        int iRow = 0;
        for (int i = 0; i < iNModes; i++)
        {
            CArmingMode *pMode = [m_pPartition UserArmingMode:i];
            CGRect rBtn = CGRectMake(iDXModeCol * iCol + 2 + 10, iDYModeRow * iRow + 2, iDXModeCol - 4, iDYModeRow - 4);
            CHLButton *pBtn = [[CHLButton alloc] initWithFrame:rBtn Text:[pMode Name]];
            [pBtn SetColor:[UIColor darkGrayColor]];
            [m_pModeButtons addObject:pBtn];
            [self addSubview:pBtn];
            pBtn.autoresizingMask = 0xFFFFFFFF;
            [pBtn SetCommandID:(CMD_ARM_START+i) Responder:self];
            [pBtn release];
            iCol++;
            if (iCol >= iNCols)
            {
                iRow++;
                iCol = 0;
            }
        }

        int iDYModes = iNRows * iDYModeRow + 25;
        int iDXDigits = MIN(300, iDXThis);
        int iDYDigits = MIN(iDYThis - iDYModes, iDXDigits * 4 / 3);
        int iX0Digits = iDXThis / 2 - iDXDigits / 2;
        int iY0Digits = (iDYThis - iDYModes) / 2 + iDYModes - iDYDigits / 2;
        int iDXBtn = iDXDigits / 3;
        int iDYBtn = iDYDigits / 4;

        for (iRow = 0; iRow < 4; iRow++)
        {
            for (iCol = 0; iCol < 3; iCol++)
            {
                int iDigit = 1 + iRow*3+iCol;
                int iCmdID = CMD_DIG0+iDigit;
                NSString *pText = [NSString stringWithFormat:@"%d", iDigit];

                if (iRow == 3)
                {
                    iCmdID = CMD_DIG0;
                    pText = @"0";

                    if (iCol == 0)
                    {
                        iCmdID = CMD_KEY_CLEAR;
                        pText = @"Cancel";
                    }
                    if (iCol == 2)
                    {
                        iCmdID = CMD_KEY_ENTER;
                        pText = @"Enter";
                    }
                }

                CGRect rBtn = CGRectMake(iX0Digits+iCol*iDXBtn+4, iY0Digits+iRow*iDYBtn+4, iDXBtn-8, iDYBtn-8);
                CHLButton *pBtn = [[CHLButton alloc] initWithFrame:rBtn Text:pText];
                [self addSubview:pBtn];
                pBtn.autoresizingMask = 0xFFFFFFFF;
                [pBtn SetCommandID:iCmdID Responder:self];
                [m_pDigitButtons addObject:pBtn];
                [pBtn release];
            }
        }

        [self Notify:0 Data:0];
        m_iModeIndex = -1;
        [self EnableKeypadButtons:FALSE];
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pModeButtons release];
    [m_pDigitButtons release];
    [m_pPartition RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iCheckedIndex = 0;
    if ([m_pPartition StatusArmed])
    {
        int iUserMode = [m_pPartition UserModeIndex];
        if (iUserMode >= 1 && iUserMode < [m_pModeButtons count])
            iCheckedIndex = iUserMode;
    }

    for (int i = 0; i< [m_pModeButtons count]; i++)
    {
        CHLButton *pBtn = (CHLButton*)[m_pModeButtons objectAtIndex:i];
        [pBtn SetChecked:(i == iCheckedIndex)];
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (iCommandID >= (CMD_ARM_START) && (iCommandID <= (CMD_ARM_END)))
    {
        int iUserMode = iCommandID - CMD_ARM_START;
        if (m_iModeIndex >= 0)
            [self KeypadMode:-1];
        [self KeypadMode:iUserMode];
        CArmingMode *pMode = [m_pPartition UserArmingMode:iUserMode];
        if ([pMode AutoEntry])
        {
            [self OnCommand:CMD_KEY_ENTER];
        }
        else
        {
            m_nKeysWanted = [pMode NKeysEntry];
        }
    }
    else if (iCommandID == CMD_KEY_ENTER)
    {
        // Figure out what they're trying to do
        short ushMessage = HLM_SECURITY_KEYPAD_DISARMQ;
        int iArmMode = 0;
        if (m_iModeIndex != 0)
        {
            ushMessage = HLM_SECURITY_KEYPAD_ARMQ;
            CArmingMode *pMode = [m_pPartition UserArmingMode:m_iModeIndex];
            if (pMode != NULL)
                iArmMode = [pMode Mode];
        }

        NSString *pPIN = @"";
        for (int i = 0; i < m_nKeysEntered; i++)
        {
            pPIN = [NSString stringWithFormat:@"%s%d", [pPIN UTF8String], m_Keys[i]];
        }

        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:ushMessage];
        [pQ autorelease];
        [pQ PutInt:m_pPartition.m_iID];
        [pQ PutInt:iArmMode];
        [pQ PutString:pPIN];
        [pQ SendMessage];
        
        [self KeypadMode:-1];
    }
    else if ((iCommandID >= CMD_DIG0) && (iCommandID <= CMD_DIG9))
    {
//        m_hltUIMode.Restart();
        if (m_nKeysEntered >= 10)
            return;
        int iDig = iCommandID - CMD_DIG0;
        m_Keys[m_nKeysEntered] = iDig;
        m_nKeysEntered++;

        if (m_nKeysWanted > 0 && m_nKeysEntered >= m_nKeysWanted)
        {
            [self OnCommand:CMD_KEY_ENTER];
        }
    }
    else if (iCommandID == CMD_KEY_CLEAR)
    {
        [self KeypadMode:-1];
    }
}
/*============================================================================*/

-(void)KeypadMode:(int)iNew

/*============================================================================*/
{
    m_iModeIndex = iNew;
    if ([m_pModeButtons count]  < 1)
        return;

    if (iNew < 0)
    {
        [self EnableKeypadButtons:FALSE];
        for (int i = 0; i < [m_pModeButtons count]; i++)
        {
            CHLButton *pBtn = (CHLButton*)[m_pModeButtons objectAtIndex:i];
            [pBtn SetFlashing:FALSE];
        }
    }
    else
    {
        [self EnableKeypadButtons:TRUE];
        for (int i = 0; i < [m_pModeButtons count]; i++)
        {
            CHLButton *pBtn = (CHLButton*)[m_pModeButtons objectAtIndex:i];
            [pBtn SetFlashing:(i == iNew)];
        }

    }

    m_nKeysEntered = 0;
}
/*============================================================================*/

-(void)EnableKeypadButtons:(BOOL)bNew

/*============================================================================*/
{
    for (int i = 0; i < [m_pDigitButtons count]; i++)
    {
        CHLButton *pBtn = (CHLButton*)[m_pDigitButtons objectAtIndex:i];
        [pBtn SetEnabled:bNew];
    }
}
@end
