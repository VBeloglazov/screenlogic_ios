//
//  VolumeControl.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CAudioZone;
@class CVolumeButton;

/*============================================================================*/

@interface CVolumeControl : UIView 

/*============================================================================*/
{
    CVolumeButton                   *m_pUp;
    CVolumeButton                   *m_pDn;
    UILabel                         *m_pLabel;
}

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone;
-(void)FrameStyle:(int)iNew;

@end
