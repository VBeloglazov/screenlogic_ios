//
//  TVChannelsView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "DVDSelectorView.h"
#import "NSMSG.h"
#import "NSQuery.h"
#import "HLComm.h"
#import "TableTitleHeaderView.h"
#import "TunerFavsPage.h"
#import "IconButton.h"
#import "iconview.h"
#import "DVDIconView.h"
#import "HLTableFrame.h"

#define TABLE_ROW_HEIGHT            44
#define EDGE_SIZE                   2
#define DX_ICON_IPOD                80
#define DY_ICON_IPOD                120
#define DX_ICON_IPAD                120
#define DY_ICON_IPAD                180
#define DX_INDEX_IPOD               40
#define DX_INDEX_IPAD               80
#define DY_CELL_BUFFER              10

#define MAX_CHARS_TITLE             18

@implementation CDVD

/*============================================================================*/

-(id)initWithDeviceID:(int)iDeviceID GenreIndex:(int)iGenreIndex DiscIndex:(int)iDiscIndex Name:(NSString*)pName

/*============================================================================*/
{
    m_iDeviceID = iDeviceID;
    m_iGenreIndex = iGenreIndex;
    m_iDiscIndex = iDiscIndex;
    m_pName = pName;
    [m_pName retain];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_bWaitImage)
    {
        [CHLComm RemoveSocketSink:self];
    }

    [m_pIconView SetDVD:NULL];
    [m_pName release];
    [m_pImage release];
    [super dealloc];
}
/*============================================================================*/

-(int)DeviceID      { return m_iDeviceID;   }
-(int)GenreIndex    { return m_iGenreIndex; }
-(int)DiscIndex     { return m_iDiscIndex;  }
-(NSString*)Name    { return m_pName;       }

/*============================================================================*/

-(UIImage*)GetImage;

/*============================================================================*/
{
    if (m_pImage == NULL)
    {
        if (m_bWaitImage)
            return NULL;

        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_GETDVDARTQ] autorelease];
        [pQ PutInt:[CHLComm SenderID:self]];
        [pQ PutInt:60];
        [pQ PutInt:80];
        [pQ PutInt:m_iDeviceID];
        [pQ PutInt:m_iDiscIndex];
        [pQ PutInt:m_iGenreIndex];
        [pQ SendMessageWithMyID:self];

        m_bWaitImage = TRUE;
    }

    return m_pImage;
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if (m_pImage != NULL)
        return;
    if (m_pIconView == NULL)
    {
        // Guy didn't want to wait, for memory reasons discard and continue
        m_bWaitImage = FALSE;
        [CHLComm RemoveSocketSink:self];
        return;
    }


    if ([pMSG MessageID] != HLM_PICSERVER_IMAGEDATA)
        return;

    [pMSG GetInt]; // DX
    [pMSG GetInt]; // DY
    
    int iSize = [pMSG DataSize] - 8;
    void *pData = [pMSG GetDataAtReadIndex];

    NSData *pNSData = [NSData dataWithBytes:pData length:iSize];
    m_pImage = [UIImage imageWithData:pNSData];
    [m_pImage retain];

    [m_pIconView setNeedsDisplay];
    [CHLComm RemoveSocketSink:self];

    m_bWaitImage = FALSE;
}
/*============================================================================*/

-(void)SetIconView:(CDVDIconView*)pView

/*============================================================================*/
{
    if (pView == NULL)
    {
        [m_pImage release];
        m_pImage = NULL;
    }

    m_pIconView = pView;
}

@end


@implementation CMovieGenre

/*============================================================================*/

-(id)initWithID:(int)iID DeviceID:(int)iDeviceID Name:(NSString*)pName View:(CDVDSelectorView*)pView

/*============================================================================*/
{
    m_pView = pView;
    m_iID = iID;
    m_iDeviceID = iDeviceID;

    m_pName = pName;
    [m_pName retain];
    m_pTitles = [[NSMutableArray alloc] init];

    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDGETLISTQ];
    [pQ autorelease];
    [pQ PutInt:m_iDeviceID];
    [pQ PutInt:m_iID];
    [pQ SendMessageWithMyID:self];
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [CHLComm RemoveSocketSink:self];
    [m_pName release];
    [m_pTitles release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_AUDIO_DVDGETLISTA)
        return;

    int iID = [pMSG GetInt];
    if (iID != m_iDeviceID)
    {
        [pMSG ResetRead];
        return;
    }

    m_bLoaded = TRUE;

    [m_pTitles removeAllObjects];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        int iDisc = [pMSG GetInt];
        [pMSG GetInt];  // Type
        NSString *pName = [pMSG GetString];

        CDVD *pDVD = [[CDVD alloc] initWithDeviceID:m_iDeviceID GenreIndex:m_iID DiscIndex:iDisc Name:pName];
        [m_pTitles addObject:pDVD];
        [pDVD release];
    }

        
    [CHLComm RemoveSocketSink:self];

    [m_pView GroupLoaded];
}
/*============================================================================*/

-(int)ID                { return m_iID;                 }
-(NSString*)Name        { return m_pName;               }
-(int)NTitles           { return [m_pTitles count];   }

/*============================================================================*/

-(CDVD*)Title:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pTitles count])
        return NULL;
    CDVD *pDVD = (CDVD*)[m_pTitles objectAtIndex:iIndex];
    return pDVD;
}
/*============================================================================*/

-(BOOL)IsLoaded

/*============================================================================*/
{
    return m_bLoaded;
}
@end


@implementation CMovieTitleCell

/*============================================================================*/

- (id)initWithFrame:(CGRect)aRect reuseIdentifier:(NSString *)identifier DXIndex:(int)iDXIndex

/*============================================================================*/
{
	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

	if (self)
	{
		// you can do this here specifically or at the table level for all cells
		self.accessoryType = UITableViewCellAccessoryNone;
        m_pButtons = [[NSMutableArray alloc] init];
    }
	
	return self;
}
/*============================================================================*/

-(void)SetGroup:(CMovieGenre*)pGroup View:(CDVDSelectorView*)pView

/*============================================================================*/
{
    if (pGroup == m_pGroup && m_bLoaded)
        return;
    m_pGroup = pGroup;
    
    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CDVDIconView *pIconView = (CDVDIconView*)[m_pButtons objectAtIndex:i];
        [pIconView removeFromSuperview];
    }
    [m_pButtons removeAllObjects];

    m_bLoaded = [pGroup IsLoaded];

    int iNTitles = [pGroup NTitles];
    for (int i = 0; i< iNTitles; i++)
    {
        CDVD *pDVD = [pGroup Title:i];
        CDVDIconView *pIconView = [[CDVDIconView alloc] initWithDVD:pDVD Cell:self];
        [pDVD SetIconView:pIconView];
        [m_pButtons addObject:pIconView];
        [self.contentView addSubview:pIconView];
        [pIconView release];
    }

    m_pView = pView;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];

    int iCol = 0;
    int iRow = 0;
    int iMaxCols = [m_pView MaxTitlesPerRow];
    int iPadLeft = 0;
/*
    if (m_iDXIndex <= 0)
    {
        iPadLeft = (contentRect.size.width - iMaxCols * DX_ICON) / 2;
    }
*/

    int iDXIcon = [CDVDSelectorView DXIcon];
    int iDYIcon = [CDVDSelectorView DYIcon];

    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CGRect rIcon = CGRectMake(iPadLeft + iCol * iDXIcon+5, iRow * iDYIcon + DY_CELL_BUFFER, iDXIcon-2, iDYIcon-2);
        CDVDIconView *pIconView = (CDVDIconView*)[m_pButtons objectAtIndex:i];
        [pIconView setFrame:rIcon];
        iCol++;
        if (iCol >= iMaxCols)
        {
            iRow++;
            iCol = 0;
        }
    }
}
/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated

/*============================================================================*/
{
    [super setSelected:FALSE animated:NO];
}
/*============================================================================*/


- (void)dealloc

/*============================================================================*/
{
    [m_pButtons release];
    [super dealloc];
}
/*============================================================================*/

-(void)SelectDisc:(CDVD*)pDVD View:(CDVDIconView*)pView;

/*============================================================================*/
{
    CGRect rBtn = pView.frame;
    [m_pView SetSelectedDVD:pDVD Section:[m_pView IndexOf:m_pGroup] Rect:rBtn];
}

@end


@implementation CDVDSelectorView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Data:(int)iData Style:(int)iStyle

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        // Initialization code
        self.opaque = NO;
        m_iData = iData;
        m_iStyle = iStyle;

        m_pGroups = [[NSMutableArray alloc] init];
        m_pSectionTitles = [[NSMutableArray alloc] init];
        m_pSectionTitlesTruncated = [[NSMutableArray alloc] init];
    
        CGRect rTable = self.bounds;
//        m_pTableView = [[CHLTableFrame alloc] initWithFrame:rTable Title:@"Movies" DataSource:self Delegate:self];
        m_pTableView = [[CHLTableFrame alloc] initWithFrame:rTable DataSource:self Delegate:self];
        [[m_pTableView TableView]setTag:999];
        [[m_pTableView TableView] setAllowsSelection:NO];
        [m_pTableView setAutoresizingMask:0xFFFFFFFF];
        
        // set the tableview as the controller view
        [self addSubview:m_pTableView];

        [CHLComm AddSocketSink:self];

        CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDADDCLIENTQ] autorelease];
        [pQ1 PutInt:iData];
        [pQ1 PutInt:[CHLComm SenderID:self]];
        [pQ1 SendMessageWithMyID:self];

        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDGETGENRESQ] autorelease];
        [pQ2 PutInt:iData];
        [pQ2 SendMessageWithMyID:self];
    }
    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    // Drawing code
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDREMOVECLIENTQ] autorelease];
    [pQ1 PutInt:m_iData];
    [pQ1 PutInt:[CHLComm SenderID:self]];
    [pQ1 SendMessageWithMyID:self];

    [CHLComm RemoveSocketSink:self];
    [m_pTableView release];
    [m_pGroups release];
    [m_pSectionTitles release];
    [m_pSectionTitlesTruncated release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        CNSQuery *pQ1 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDADDCLIENTQ] autorelease];
        [pQ1 PutInt:m_iData];
        [pQ1 PutInt:[CHLComm SenderID:self]];
        [pQ1 SendMessageWithMyID:self];

        CNSQuery *pQ2 = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDGETGENRESQ] autorelease];
        [pQ2 PutInt:m_iData];
        [pQ2 SendMessageWithMyID:self];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iID = [pMSG GetInt];
    if (iID != m_iData)
    {
        [pMSG ResetRead];
        return;
    }

    if ([pMSG MessageID] == HLM_AUDIO_DVDLISTINVALIDATE)
    {
        [CHLComm AddSocketSink:self];
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDGETLISTQ] autorelease];
        [pQ PutInt:m_iData];
        [pQ SendMessageWithMyID:self];
        return;
    }

    if ([pMSG MessageID] != HLM_AUDIO_DVDGETGENRESA)
        return;

    [m_pGroups removeAllObjects];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        NSString *pName = [pMSG GetString];
        if ([pName caseInsensitiveCompare:@"ALL"] != 0)
        {
            CMovieGenre *pG = [[CMovieGenre alloc] initWithID:i DeviceID:m_iData Name:pName View:self];
            [m_pGroups addObject:pG];
            [m_pSectionTitles addObject:pName];
            if (iN == 1)
                [m_pSectionTitlesTruncated addObject:@""];
            else
                [m_pSectionTitlesTruncated addObject:[self TruncateGenreTitle:pName]];
            [pG release];
        }
    }

        
    [CHLComm RemoveSocketSink:self];

    [m_pTableView ReloadData];
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return MAX(1, [m_pGroups count]);
}
/*============================================================================*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

/*============================================================================*/
{
    int section = indexPath.section;
    CMovieGenre *pGenre = (CMovieGenre*)[m_pGroups objectAtIndex:section];

    int iNTitles = [pGenre NTitles];
    int iNPerRow = [self MaxTitlesPerRow];
    int iRows = iNTitles / iNPerRow;
    if (iNTitles % iNPerRow != 0)
        iRows++;

    int iDYIcon = [CDVDSelectorView DYIcon];

    return iRows * iDYIcon + 2 * DY_CELL_BUFFER;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    if (section >= [m_pGroups count])
        return 0;

    return 1;
/*

    CMovieGenre *pGenre = (CMovieGenre*)[m_pGroups objectAtIndex:section];
    int iNTitles = [pGenre NTitles];
    int iNPerCell = [self MaxTitlesPerCell];
    int iRows = iNTitles / iNPerCell;
    if (iNTitles % iNPerCell != 0)
        iRows++;

    return iRows;
*/
}
/*============================================================================*/

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView 

/*============================================================================*/
{
	// returns the array of section titles. There is one entry for each unique character that an element begins with
	// [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    return m_pSectionTitlesTruncated;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index 

/*============================================================================*/
{
	return index;
}
/*============================================================================*/

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 

/*============================================================================*/
{
    if (section >= [m_pSectionTitles count])
        return @"";

    NSString *pS = (NSString*)[m_pSectionTitles objectAtIndex:section];
    return pS;
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    CMovieGenre *pGroup = (CMovieGenre*)[m_pGroups objectAtIndex:indexPath.section];

    int iDXIndex = DX_INDEX_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDXIndex = DX_INDEX_IPAD;
    if ([m_pGroups count] <= 1)
        iDXIndex = 0;

    int iRow = indexPath.row;

    NSString *CellIdentifier = [NSString stringWithFormat:@"ROW:%d", iRow];
    CMovieTitleCell *cell = (CMovieTitleCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[CMovieTitleCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier DXIndex:iDXIndex] autorelease];
    }

    [cell SetGroup:pGroup View:self];

    return cell;
}
/*============================================================================*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

/*============================================================================*/
{
    if (buttonIndex == 1)
    {
        CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_AUDIO_DVDCOMMANDQ] autorelease];
        [pQ PutInt:m_iData];
        [pQ PutInt:CMD_KEY_LOADDIRECT];
        int iDisc = ([m_pSelectedDVD GenreIndex] << 16) | [m_pSelectedDVD DiscIndex];
        [pQ PutInt:iDisc];
        [pQ SendMessage];
    }
}
/*============================================================================*/

-(void)GroupLoaded

/*============================================================================*/
{
    [m_pTableView ReloadData];
}
/*============================================================================*/

-(void)SetSelectedDVD:(CDVD*)pDVD Section:(int)iSection Rect:(CGRect)rBtn

/*============================================================================*/
{
    m_pSelectedDVD = pDVD;
    m_iSelectedSection = iSection;
    m_rSelectedStationRect = rBtn;

    if (m_pSelectedDVD == NULL)
        return;

	UIAlertView *pAlert = [[UIAlertView alloc] initWithTitle:@"Play Movie" message:[m_pSelectedDVD Name]
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Play", nil];

	[pAlert show];	
	[pAlert release];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bNew

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(NSString*)TruncateGenreTitle:(NSString*)pName

/*============================================================================*/
{
    int iLen = [pName length];
    if (iLen < MAX_CHARS_TITLE)
        return pName;
    const char *pChars = [pName UTF8String];
    int iChopIndex = MAX_CHARS_TITLE;
    for (int iChop = MAX_CHARS_TITLE-1; iChop > 5 && (iChopIndex == MAX_CHARS_TITLE); iChop--)
    {
        if (pChars[iChop] == ' ')
            iChopIndex = iChop;
    }
    NSString *pNew = [pName substringToIndex:iChopIndex-1];
    NSString *pRet = [NSString stringWithFormat:@"%s...", [pNew UTF8String]];
    return pRet;
}
/*============================================================================*/

-(int)MaxTitlesPerRow

/*============================================================================*/
{
    int iDXIndex = DX_INDEX_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDXIndex = DX_INDEX_IPAD;
    if ([m_pGroups count] <= 1)
        iDXIndex = 0;

    int iDXIcon = [CDVDSelectorView DXIcon];
    int iMaxCols = (m_pTableView.frame.size.width - iDXIndex) / iDXIcon;
    return MAX(1, iMaxCols);
}
/*============================================================================*/

-(int)IndexOf:(CMovieGenre*)pGenre

/*============================================================================*/
{
    for (int i = 0; i < [m_pGroups count]; i++)
    {
        if ([m_pGroups objectAtIndex:i] == pGenre)
            return i;
    }
    return -1;
}
/*============================================================================*/

+(int)DXIcon

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return DX_ICON_IPAD;
    return DX_ICON_IPOD;
}
/*============================================================================*/

+(int)DYIcon

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return DY_ICON_IPAD;
    return DY_ICON_IPOD;
}
@end
