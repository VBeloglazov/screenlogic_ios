/*
--------------------------------------------------------------------------------

    SOCKSINK.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef SOCKSINK_H
#define SOCKSINK_H

#include "hlstructs.h"

class CHLMSG;
class CHLServer;

/*============================================================================*/

    class CSocketSink

/*============================================================================*/
{
protected:
    #ifdef MAC_OS
        void                    *m_pNSSink;
    #endif

    #ifdef MULTI_SERVER
        CHLServer               *g_pHLServer;
    #endif

    short                       m_shSenderID;
    BOOL                        m_bWaitResponse;

public:
    #ifdef MULTI_SERVER
        CSocketSink(CHLServer *pServer);
        CSocketSink(CHLServer *pServer, short shSenderID );
    #else
        CSocketSink();
        CSocketSink(short shSenderID );
    #endif

    #ifdef MAC_OS
        CSocketSink(void *pNSSink);
        inline void             *NSSink()    { return m_pNSSink; }
    #endif

    virtual ~CSocketSink();

    short                       SenderID();

    void                        SenderID(
    short                       shNew );

    void                        RegenSenderID();

    void                        SwapID(
    CSocketSink                 *pOther );

    inline BOOL                 WaitingResponse()   { return m_bWaitResponse;   }

    // CSocketSink overrides
    virtual void                SinkMessage(
    CHLMSG                      *pMSG );

    virtual void                ConnectionChanged(
    BOOL                        bNowConnected );
};


#endif

