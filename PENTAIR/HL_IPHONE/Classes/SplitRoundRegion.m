//
//  SplitRoundRegion.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/25/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "SplitRoundRegion.h"
#import "RoundRegion.h"
#import "MainView.h"
#import "crect.h"
#import "CustomPage.h"

#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

@implementation CSplitRoundRegion

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Style:(int)iStyle

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_iStyle = iStyle;
        m_iLayout = LAYOUT_OVERUNDER;

        // Initialization code
        int iRadiusStyle = RADIUSSTYLE_DEFAULT;
        if (m_iStyle == SRREGION_SQUARE_CORNERS)
            iRadiusStyle = RADIUSSTYLE_NORADIUS;
        if (m_iStyle == SRREGION_MAIN_WINDOW)
        {
            if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
                iRadiusStyle = RADIUSSTYLE_NORADIUS;
        }

        m_pRgn1 = [[CRoundRegion alloc] initWithFrame:CGRectZero FrameStyle:FRAMESTYLE_UPPER RadiusStyle:iRadiusStyle];
        m_pRgn2 = [[CRoundRegion alloc] initWithFrame:CGRectZero FrameStyle:FRAMESTYLE_LOWER RadiusStyle:iRadiusStyle];
        [self addSubview:m_pRgn1];
        [self addSubview:m_pRgn2];
        [m_pRgn1 release];
        [m_pRgn2 release];
        [m_pRgn1 HasHighlight:TRUE];
    }
    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Title:(NSString*)pTitle

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_iStyle = SRREGION_SMALL_HEADER;
        m_iLayout = LAYOUT_OVERUNDER;

        // Initialization code
        int iRadiusStyle = RADIUSSTYLE_DEFAULT;
        if (m_iStyle == SRREGION_SQUARE_CORNERS)
            iRadiusStyle = RADIUSSTYLE_NORADIUS;

        m_pRgn1 = [[CRoundRegion alloc] initWithFrame:CGRectZero FrameStyle:FRAMESTYLE_UPPER RadiusStyle:iRadiusStyle];
        m_pRgn2 = [[CRoundRegion alloc] initWithFrame:CGRectZero FrameStyle:FRAMESTYLE_LOWER RadiusStyle:iRadiusStyle];
        [self addSubview:m_pRgn1];
        [self addSubview:m_pRgn2];
        [m_pRgn1 release];
        [m_pRgn2 release];

        m_pTitle = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pTitle Size:[CMainView DEFAULT_TEXT_SIZE]];
        [m_pTitle setText:pTitle];
        [m_pRgn1 SetClientView:m_pTitle];
        [m_pTitle release];
        [m_pRgn1 HasHighlight:TRUE];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    if (m_iFlags & SRREGION_FLAG_AUTOLAYOUT)
    {
        if (rBounds.size.width > rBounds.size.height)
            m_iLayout = LAYOUT_LEFTRIGHT;
        else
            m_iLayout = LAYOUT_OVERUNDER;
    }

    if (m_iLayout == LAYOUT_OVERUNDER)
    {
        int iDYHeader = [CMainView DY_TOOLBAR_SUB];
        if (m_iStyle == SRREGION_SQUARE_CORNERS)
            iDYHeader = [CMainView DY_TOOLBAR];
        if (m_iStyle == SRREGION_SMALL_HEADER)
            iDYHeader = [CMainView DY_TOOLBAR_TEXT];
        if (m_iStyle == SRREGION_MAIN_WINDOW)
            iDYHeader = [CMainView DY_TOOLBAR];

        CGRect rHeader = [CRect BreakOffTop:&rBounds DY:iDYHeader];
        [CRect BreakOffTop:&rBounds DY:1];
        [m_pRgn1 setFrame:rHeader];
        [m_pRgn2 setFrame:rBounds];
        [m_pRgn1 FrameStyle:FRAMESTYLE_UPPER];
        [m_pRgn2 FrameStyle:FRAMESTYLE_LOWER];
    }
    else
    {
        int iDXHeader = [CMainView DX_SIDEBAR];

        CGRect rHeader = [CRect BreakOffLeft:&rBounds DX:iDXHeader];
        [CRect BreakOffLeft:&rBounds DX:1];
        [m_pRgn1 setFrame:rHeader];
        [m_pRgn2 setFrame:rBounds];
        [m_pRgn1 FrameStyle:FRAMESTYLE_LEFT];
        [m_pRgn2 FrameStyle:FRAMESTYLE_RIGHT];
    }
    CALayer *pLayer = [self layer];
    pLayer.cornerRadius = 10;//[m_pRgn2 Radius];
}
/*============================================================================*/

-(void)SetTitle:(NSString*)pTitle;

/*============================================================================*/
{
    [m_pTitle setText:pTitle];
}
/*============================================================================*/

-(CRoundRegion*)UpperRegion

/*============================================================================*/
{
    return m_pRgn1;
}
/*============================================================================*/

-(CRoundRegion*)LowerRegion

/*============================================================================*/
{
    return m_pRgn2;
}
/*============================================================================*/

-(void)SetFlags:(int)iFlags

/*============================================================================*/
{
    [m_pRgn1 Flags:iFlags];
    [m_pRgn2 Flags:iFlags];
}
@end
