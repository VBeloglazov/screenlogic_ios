//
//  timebar.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

/*============================================================================*/

@interface CTimeBarLabel : UIView

/*============================================================================*/
{
    UILabel                         *m_pLabel;
    UIView                          *m_pAuxView;
    int                             m_iType;
    NSDate                          *m_pStartDate;
    NSDate                          *m_pEndDate;
}

- (id)initWithFrame:(CGRect)frame Text:(NSString*)pText Type:(int)iType StartDate:(NSDate*)pStartDate EndDate:(NSDate*)pEndDate;

- (void)UpdateAlignmentFrom:(int)iXMin To:(int)iXMax;
- (int)Type;
-(void)SetAuxView:(UIView*)pAuxView;

-(NSDate*)StartDate;
-(NSDate*)EndDate;
@end

@class CTimeView;

/*============================================================================*/

@interface CTimeBar : UIView 

/*============================================================================*/
{
    CTimeView                       *m_pTimeView;
    NSMutableArray                  *m_pLabels;
    int                             m_iDXLast;
}

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView;

-(BOOL)InitBar:(CGRect)rBar Type:(int)iType TimeView:(CTimeView*)pTimeView;

-(int)MinIntervalSize:(int)iType;
-(void)UpdateAlignments;
-(NSMutableArray*)LabelList;

@end
