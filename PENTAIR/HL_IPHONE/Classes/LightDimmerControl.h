//
//  LightDimmerControl.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/27/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CLightControl;
@class CHLButton;
@class CHLSlider;


/*============================================================================*/

@interface CLightDimmerControl : UIView 

/*============================================================================*/
{
    int                                 m_iStyle;
    CLightControl                       *m_pControl;
    CHLButton                           *m_pOff;
    CHLButton                           *m_pOn;
    CHLSlider                           *m_pSlider;
    NSTimer                             *m_pTimer;
}

-(id)initWithFrame:(CGRect)frame Style:(int)iStyle Control:(CLightControl*)pControl;
-(void)NotifyState:(int)iState;
-(void)SliderChanged:(id)sender;
-(void)SliderRelease:(id)sender;

@end
