/*
--------------------------------------------------------------------------------

    ROUNDRECT.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifndef ROUNDRECT_H
#define ROUNDRECT_H

#define ROUNDRECT_RIGHTEDGE     0x00000001
#define ROUNDRECT_LEFTEDGE      0x00000002
#define ROUNDRECT_SOLID         0x00000004
#define ROUNDRECT_ADDMODE       0x00000008
#define ROUNDRECT_INVERT        0x00000010
#define ROUNDRECT_DISCARDABLE   0x00000020

#ifdef IPHONE
#include "dibdc.h"
#define CAlphaDIB CHLDibDC
#endif

#define DARK_SIDE_PCT           50
#define MID_SIDE_PCT            75
#define LIGHT_SIDE_PCT          100

/*============================================================================*/

    class CRadiusAlphaMap

/*============================================================================*/
{
protected:
    int                         m_iRad;
    BYTE                        *m_pBits;

public:
    CRadiusAlphaMap(
    int                         iRad );

    virtual ~CRadiusAlphaMap();

    BYTE                        *GetAlpha(
    int                         iX,
    int                         iY );        
};
/*============================================================================*/

    class CRoundRect

/*============================================================================*/
{
protected:
    int                         m_iFlags;
    int                         m_iDX;
    int                         m_iDY;
    int                         m_iRad;
    int                         m_iPixelSize;
    int                         m_iCornerMask;
    int                         m_iDibFlags;
    
    CAlphaDIB                   *m_pX_UL;
    CAlphaDIB                   *m_pX_UR;
    CAlphaDIB                   *m_pX_LL;
    CAlphaDIB                   *m_pX_LR;

    CHLDibDC                    *m_pUL;
    CHLDibDC                    *m_pUR;
    CHLDibDC                    *m_pLL;
    CHLDibDC                    *m_pLR;

    COLORREF                    m_rgbEdge;
    COLORREF                    m_rgbOutside;
    COLORREF                    m_rgbInside;

    CRadiusAlphaMap             *m_pMap;

    int                         m_iAlpha;

public:
    CRoundRect(
    int                         iDX,
    int                         iDY,
    int                         iRad,
    int                         iPixelSize,
    COLORREF                    rgbEdge,
    COLORREF                    rgbOutside,
    COLORREF                    rgbInside,
    int                         iCornerMask,
    int                         iFlags  );

    virtual ~CRoundRect();

    inline int                  DX()            { return m_iDX;         }
    inline int                  DY()            { return m_iDY;         }
    inline int                  Rad()           { return m_iRad;        }
    inline int                  PixelSize()     { return m_iPixelSize;  }
    inline int                  CornerMask()    { return m_iCornerMask; }
    inline COLORREF             RGBIn()         { return m_rgbInside;   }
    inline COLORREF             RGBOut()        { return m_rgbOutside;  }
    inline void                 Alpha(int iNew) { m_iAlpha = iNew;      }

    virtual void                Draw(
    CHLDC                       *pDC,
    int                         iX,
    int                         iY,
    BOOL                        bFill = FALSE );

    virtual void                DrawX(
    CHLDC                       *pDC,
    int                         iX,
    int                         iY );

#ifndef IPHONE
    virtual void                RenderCorner(
    CHLDibDC                    *pTgt,
    CHLDibDC                    *pBG,
    int                         iCorner );
#endif

    void                        ZeroOutsideRadius(
    CHLDibDC                    *pDIB,
    int                         iCorners );
    
    void                        ZeroOutsideRadiusWith(
    CHLDibDC                    *pDIB,
    int                         iCorners,
    HL_PIXEL                    rgb );

    void                        FillInterior(
    CHLDibDC                    *pDIB,
    COLORREF                    rgbFill );

    void                        Blend(
    int                         iCorner,
    CHLDibDC                    *pTgt,
    int                         iXTgt,
    int                         iYTgt,
    CHLDibDC                    *pBG );

    static COLORREF             GetRGB(
    COLORREF                    rgb,
    int                         iPCT );

    static COLORREF             Add(
    COLORREF                    rgb1,
    COLORREF                    rgb2 );

    void                        InitAlphaCorners();
    void                        InitSolidCorners();

    static CAlphaDIB            *GenEdge(
    int                         iDir,
    int                         iDim,
    int                         iFlags,
    COLORREF                    *pRGB,
    int                         iColorType );
    
    void                        ApplyAlpha16(
    HL_PIXEL                    *pDst,
    int                         iAlpha16 );

    void                        ApplyAlpha16(
    HL_PIXEL                    *pDst,
    int                         iAlpha16,
    HL_PIXEL                    wSet );

    void                        BlendPixel16(
    HL_PIXEL                    *pDst,
    int                         iAlpha16,
    HL_PIXEL                    wColor );
};

#endif
