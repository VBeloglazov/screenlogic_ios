//
//  home.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/1/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

//
//  SubTab.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "home.h"
#import "NSMSG.h"
#import "NSQuery.h"
#import "ControlDef.h"
#import "CustomPage.h"
#import "IPAD_ViewController.h"
#import "WeatherHeaderView.h"
#import "AppDelegate.h"


@implementation CHomeSubTab

/*============================================================================*/

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int) iData1 Data2:(int)iData2

/*============================================================================*/
{
    if (self == [super initWithName:pName ID:iID Data1:iData1 Data2:iData2])
    {
        CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:HLM_SETSERVER_GETHOMEPAGEWINDOWSQ];
        [pQ1 autorelease];
        [pQ1 PutInt:HL_800x600];

        m_pDefs = [[NSMutableArray alloc] init];
            
        if ([pQ1 AskQuestion])
        {
            CNSMSG *pAnswer = [pQ1 CreateNSMSG];
            int iN = [pAnswer GetInt];
            for (int i = 0; i < iN; i++)
            {
                CControlDef *pDef = [CControlDef alloc]; 
                [pDef initWithMSG:pAnswer Flags:0];
                [m_pDefs addObject:pDef];
                [pDef release];
            }
            [pAnswer release];
        }
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
    [m_pDefs release];
}
/*============================================================================*/

-(CSubSystemPage*)CreateUserPage:(CGRect)rFrame MainTabPage:(CMainTabViewController*)pMain

/*============================================================================*/
{
    CHomePage *pPage = [[CHomePage alloc] initWithFrame:rFrame Controls:m_pDefs SubTab:self MainTabPage:pMain];
    //[pPage SetMainViewController:(UIViewController *)m_pMainViewController];
    return pPage;
}
/*============================================================================*/
@end

@implementation CHomePage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Controls:(NSMutableArray*)pList SubTab:(CSubTab*)pSub MainTabPage:(CMainTabViewController*)pMain

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame MainTabPage:pMain SubTab:pSub])
    {
        m_pPage = [[CCustomPage alloc] initWithFrame:self.bounds ControlList:pList];
        [m_pPage SetVisible:TRUE];
        [self addSubview:m_pPage];
        [m_pPage release];
    }
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pPage setFrame:self.bounds];
}
/*============================================================================*/

-(CToolBarClientView*)CreateMenuView:(CGRect)rView ViewController:(IPAD_ViewController*)pCtlr Flags:(int)iFlags

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CSubSystemHeaderBarView *pView = [[CSubSystemHeaderBarView alloc] initWithFrame:rView Label:[m_pSubTab Name] Flags:iFlags PageCtrl:NULL];
    CWeatherHeaderView *pWeather = [[CWeatherHeaderView alloc] initWithFrame:CGRectZero Forecast:[pDelegate Forecast]];
    [pView SetAccessoryView:pWeather Width:275];
    [pWeather release];
    return pView;

}
//*============================================================================*/
//
//-(void)SetMainViewController:(UIViewController*)mainViewController
//{
//    m_pMainViewController = mainViewController;
//}
//*============================================================================*/
@end
