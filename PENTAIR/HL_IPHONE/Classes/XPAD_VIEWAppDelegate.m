//
//  XPAD_VIEWAppDelegate.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import "XPAD_VIEWAppDelegate.h"
#import "XPAD_VIEWViewController.h"
#import "hlcomm.h"

@implementation XPAD_VIEWAppDelegate

@synthesize window;
@synthesize navigationController;

/*============================================================================*/

- (void)applicationDidFinishLaunching:(UIApplication *)application 

/*============================================================================*/
{	
    m_pComm = [[CHLComm alloc] init];

	// Override point for customization after app launch	
	[window addSubview:[navigationController view]];
	[window makeKeyAndVisible];
    [m_pComm InitComm];
    [application setStatusBarStyle:UIStatusBarStyleBlackOpaque];
}
/*============================================================================*/

-(void)applicationWillTerminate:(UIApplication *)application

/*============================================================================*/
{
    [m_pComm ShutDownComm];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [navigationController release];
	[window release];
    [m_pComm release];
	[super dealloc];
}


@end
