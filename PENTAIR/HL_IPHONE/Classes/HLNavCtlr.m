//
//  HLNavCtlr.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/17/13.
//
//

#import "HLNavCtlr.h"

@implementation HLNavCtlr

- (BOOL)shouldAutorotate
{
    return [self.topViewController shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

@end
