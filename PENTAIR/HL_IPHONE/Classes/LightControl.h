//
//  LightDimmer.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "hlview.h"

@class CHLButton;
@class CLightDimmerControl;

/*============================================================================*/

@interface CLightControl : UIView  < CNSSocketSink, CHLCommandResponder >

/*============================================================================*/
{
    int                             m_iType;

    CHLButton                       *m_pSwitch;
    CLightDimmerControl             *m_pDimmer;
    int                             m_iID;
    NSTimer                         *m_pTimer;
    
    int                             m_iKeypadID;
    int                             m_iSceneIndex;
}

-(id)initWithFrame:(CGRect)rFrame Type:(int)iType Style:(int)iStyle Text:(NSString*)pText ID:(int)iID;
-(void)UserChange:(id)sender;
-(void)DeviceOff:(id)sender;
-(void)DeviceOn:(id)sender;
-(void)DeviceControl:(int)iCmd;
-(void)InitComm;
@end
