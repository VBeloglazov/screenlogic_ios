//
//  AllZonesOffView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 8/26/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "AllZonesOffView.h"
#import "CustomPage.h"
#import "hlcomm.h"
#import "hlm.h"
#import "crect.h"
#import "MediaZonePage.h"
#import "NSQuery.h"

@implementation CAllZonesOffView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Page:(CMediaZonePage*)pPage ZoneID:(int)iZoneID

/*============================================================================*/
{
    self = [super initWithFrame:frame FrameStyle:FRAMESTYLE_DEFAULT RadiusStyle:RADIUSSTYLE_DEFAULT];
    if (self)
    {
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [m_pLabel setText:@"All Zones Off"];
        int iTextSize = [CHLComm GetInt:INT_APP_TEXTHEIGHT];
        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
        m_pProgress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        [self addSubview:m_pProgress];
        [m_pProgress release];
        
        m_pZonePage = pPage;
        m_iZoneID   = iZoneID;

        m_pTimer = [[NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(TimerEvent) userInfo:nil repeats:YES] retain];
    }
    return self;
}
/*============================================================================*/

-(void) dealloc

/*============================================================================*/
{
    [m_pTimer invalidate];
    [m_pTimer release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rLabel = [CRect BreakOffTop:&rThis DY:rThis.size.height / 2];
    [m_pLabel setFrame:rLabel];


    [CRect CenterDownToY:&rThis DY:32];
    [CRect Inset:&rThis DX:20 DY:0];
    [m_pProgress setFrame:rThis];
}
/*============================================================================*/

-(void)TimerEvent

/*============================================================================*/
{
    float fPos = m_iCount / 60.f;
    [m_pProgress setProgress:fPos];
    m_iCount++;

    if (m_iCount >= 60)
    {
        CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_ALLZONESOFFQ];
        [pQ SendMessage];
        [pQ release];

        [m_pTimer invalidate];
        [m_pTimer release];
        m_pTimer = NULL;
        [m_pZonePage CancelAllOffWarning];
    }
}
/*============================================================================*/

-(void)Dismiss

/*============================================================================*/
{
    [m_pTimer invalidate];
    [m_pTimer release];
    m_pTimer = NULL;
}
@end
