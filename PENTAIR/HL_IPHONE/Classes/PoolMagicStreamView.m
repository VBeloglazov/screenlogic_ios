//
//  PoolMagicStreamView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/9/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolMagicStreamView.h"
#import "hlbutton.h"
#import "crect.h"
#import "MainView.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "NSMSG.h"


@implementation CPoolMagicStreamView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pButtons[0] = [self AddButton:@"Thumper"  CommandID:POOLCMD_MS_THUMPER];
        m_pButtons[1] = [self AddButton:@"Mode"     CommandID:POOLCMD_MS_NEXT_MODE];
        m_pButtons[2] = [self AddButton:@"Reset"    CommandID:POOLCMD_MS_RESET];
        m_pButtons[3] = [self AddButton:@"Hold"     CommandID:POOLCMD_MS_HOLD];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID

/*============================================================================*/
{
    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:pText];
    [self addSubview:pBtn];
    [pBtn release];
    [pBtn SetCommandID:iID Responder:self];
    return pBtn;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [CRect CenterDownToY:&rBounds DY:64];
        for (int i = 0; i < 4; i++)
        {
            CGRect rBtn = [CRect CreateXSection:rBounds Index:i Of:4];
            [CRect Inset:&rBtn DX:2 DY:2];
            [m_pButtons[i] setFrame:rBtn];
        }
    }
    else
    {
        [CRect CenterDownToX:&rBounds DX:200];
        [CRect CenterDownToY:&rBounds DY:200];
        for (int i = 0; i < 4; i++)
        {
            CGRect rBtn = [CRect CreateYSection:rBounds Index:i Of:4];
            [CRect Inset:&rBtn DX:0 DY:2];
            [m_pButtons[i] setFrame:rBtn];
        }
    }
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_COLORLIGHTSCMDQ];
    [p1 PutInt:0];
    [p1 PutInt:iCommandID];
    [p1 SendMessage];
    [p1 release];
}

@end
