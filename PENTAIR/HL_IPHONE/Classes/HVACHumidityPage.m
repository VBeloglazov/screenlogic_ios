//
//  HVACTempPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HVACHumidityPage.h"
#import "HVACTempView.h"
#import "HVACModePage_IPAD.h"
#import "tstat.h"
#import "hlm.h"
#import "hlradio.h"
#import "MainView.h"
#import "crect.h"

@implementation CHVACHumidityPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        self.autoresizingMask = 0xFFFFFFFF;

        m_iHVACState = -1;
        m_pTStat = pTStat;
        [m_pTStat AddSink:self];

        m_pSPLo = [[CHVACTempView alloc] initWithFrame:CGRectZero TStat:m_pTStat Mode:TEMP_HUMSPLO ReadOnly:FALSE]; 
        m_pSPHi = [[CHVACTempView alloc] initWithFrame:CGRectZero TStat:m_pTStat Mode:TEMP_HUMSPHI ReadOnly:FALSE]; 
        m_pAct = [[CHVACTempView alloc] initWithFrame:CGRectZero TStat:m_pTStat Mode:TEMP_HUMIDITY ReadOnly:FALSE]; 

        [self addSubview:m_pAct];
        [self addSubview:m_pSPHi];
        [self addSubview:m_pSPLo];

        CGRect rThis = CGRectMake(self.bounds.size.width/2, self.bounds.size.height/2, 0, 0);

        [m_pSPLo setFrame:rThis];
        [m_pSPHi setFrame:rThis];
        [m_pAct setFrame:rThis];
        [m_pSPLo setAlpha:0];
        [m_pSPHi setAlpha:0];
        [m_pAct setAlpha:0];

        if ([m_pTStat StateOK])
        {
            [self Notify:0 Data:0];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pTStat RemoveSink:self];
    [m_pAct release];
    [m_pSPHi release];
    [m_pSPLo release];
    [super dealloc];
}
/*============================================================================*/

- (void)willMoveToSuperview:(UIView *)newSuperview

/*============================================================================*/
{
    [self SetVisible:(newSuperview != NULL)];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [m_pSPLo SetVisible:bVisible];
    [m_pSPHi SetVisible:bVisible];
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];

    [self UpdateControls];

	[UIView commitAnimations];
}
/*============================================================================*/

-(void)UpdateControls

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

   
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        // To account for outside temp
        iDYThis -= 32;
    }

    CGPoint ptHeat = m_pSPLo.center;
    CGPoint ptCool = m_pSPHi.center;
    CGPoint ptRoom = m_pAct.center;

    ptRoom = CGPointMake(iDXThis/4, iDYThis/2);
    ptHeat = CGPointMake(iDXThis*3/4, iDYThis*3/4);
    ptCool = CGPointMake(iDXThis*3/4, iDYThis*1/4);
    
    [m_pSPLo setAlpha:1.0];
    [m_pSPHi setAlpha:1.0];
    [m_pAct setAlpha:1.0];
    
    [m_pSPLo setUserInteractionEnabled:TRUE];
    [m_pSPHi setUserInteractionEnabled:TRUE];

    int iDXRoom = iDXThis * 50 / 100;
    int iDYRoom = MIN(250, iDYThis * 45 / 100);

    int iDXSet  = iDXThis * 50 / 100;
    int iDYSet  = iDYThis * 25 / 100;


    [m_pSPLo setFrame:[self MakeRectAt:ptHeat DX:iDXSet DY:iDYSet]];
    [m_pSPHi setFrame:[self MakeRectAt:ptCool DX:iDXSet DY:iDYSet]];
    [m_pAct setFrame:[self MakeRectAt:ptRoom DX:iDXRoom DY:iDYRoom]];

    self.autoresizingMask = 0xFFFFFFFF;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    if (iDXThis == m_iDXLast && iDYThis == m_iDYLast)
        return;


    m_iDXLast = iDXThis;
    m_iDYLast = iDYThis;

    [self UpdateControls];

}
/*============================================================================*/

-(CGRect)MakeRectAt:(CGPoint)ptC DX:(int)iDX DY:(int)iDY

/*============================================================================*/
{
    CGRect rRet = CGRectMake(ptC.x - iDX/2, ptC.y - iDY/2, iDX, iDY);
    return rRet;
}
@end
