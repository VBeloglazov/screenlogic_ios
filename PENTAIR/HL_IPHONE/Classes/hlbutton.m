//
//  hlbutton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "hlbutton.h"
#import "iconview.h"
#import "hlm.h"
#import "hlview.h"
#import "MainView.h"
#import "crect.h"
#import "hlcomm.h"
#import "shader.h"
#import "AppDelegate.h"
#import "HatSwitch.h"
#import "HLToolBar.h"
#import "CustomPage.h"

#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

@implementation CHLButton

#define FLASHSTATE_WANTFLASH    0x0000001
#define FLASHSTATE_LOWTOHIGH    0x0000002

#define CHECKED_COLOR   [UIColor colorWithRed:0.5 green:0.15 blue:1.00 alpha:1.0]

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Icon:(NSString*)sIcon

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    {
        self.contentMode = UIViewContentModeRedraw;

        if (iTextSize == 0)
            iTextSize = [CMainView DEFAULT_TEXT_SIZE];

        m_iShadeOut = [CHLComm GetInt:INT_BUTTON_SHADEOUT];
        m_iShadeIn  = [CHLComm GetInt:INT_BUTTON_SHADEIN];
        m_iRad      = [CHLComm GetInt:INT_BUTTON_ROUT];

        m_iIconFormat   = iIconFormat;
        m_iTextHeight   = iTextSize;
        m_iStyle        = iStyle;
        m_pText         = pText;
        m_pIcon         = sIcon;
        [m_pText retain];
        [m_pIcon retain];
            
        m_RGBFlash = [[CHLComm GetRGB:RGB_SELECT] retain];

        if (RGBText == NULL)
        {
            RGBText = [CHLComm GetRGB:RGB_BUTTON_TEXT];
        }

        // Keep this for button animations
        if (RGBFace == NULL)
        {
            // If Both default then use black/white
            m_RGB = [[CHLComm GetRGB:RGB_BUTTON] retain];
        }
        else
        {
            m_RGB = RGBFace;
            [m_RGB retain];
        }
        
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];

        m_pLabel.textAlignment = UITextAlignmentCenter;
        m_pLabel.numberOfLines = 3;
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        [m_pLabel setText:pText];
        [m_pLabel setTextColor:RGBText];
        [self addSubview:m_pLabel];

        if (iStyle != AUDIO_ICON_TEXT)
        {
            m_pIconView = [[CUserTabIconView alloc] initWithFrame:CGRectZero];
            [m_pIconView SetOptions:ICONVIEW_TVICON];

            NSString *pBuiltIn = [CHLButton EmbeddedIconName:iStyle];

            if (pBuiltIn != NULL)
                [m_pIconView SetEmbeddedIcon:pBuiltIn];
            else
                [m_pIconView SetUserIcon:sIcon];
            [self addSubview:m_pIconView];


        }

        [self InitShader];
        [self InitEvents];
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Text:(NSString*)pText

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    
    if (self)
    {
        self.contentMode = UIViewContentModeRedraw;

        m_iShadeOut = [CHLComm GetInt:INT_BUTTON_SHADEOUT];
        m_iShadeIn  = [CHLComm GetInt:INT_BUTTON_SHADEIN];
        m_iRad      = [CHLComm GetInt:INT_BUTTON_ROUT];
        m_iIconFormat   = HL_ICONCENTER;
        m_iTextHeight   = [CHLComm GetInt:INT_BUTTON_TEXTHEIGHT];
        m_iStyle        = AUDIO_ICON_TEXT;
        m_pText         = pText;
        [m_pText retain];
            
        // If Both default then use black/white
        m_RGB = [[CHLComm GetRGB:RGB_BUTTON] retain];
        m_RGBFlash = [[CHLComm GetRGB:RGB_SELECT] retain ];

        UIColor *RGBText = [CHLComm GetRGB:RGB_BUTTON_TEXT];

        self.opaque = NO;

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:m_iTextHeight];
        m_pLabel.textAlignment = UITextAlignmentCenter;
        m_pLabel.numberOfLines = 3;
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        [m_pLabel setText:pText];
        [m_pLabel setTextColor:RGBText];
        [self addSubview:m_pLabel];

        [self InitShader];
        [self InitEvents];
    }

    return self;
}
/*============================================================================*/

-(void)InitShader

/*============================================================================*/
{
    if (self.bounds.size.height <= 0)
        return;
    if (self.bounds.size.width <= 0)
        return;

    int iType = ELEMENT_STD;
    if (m_bChecked)
        iType = ELEMENT_CHECK;
    CGImageRef pRef = [self ImageRefForState:iType];
    [[self layer] setContents:(id)pRef];
}
/*============================================================================*/

-(void)InitEvents

/*============================================================================*/
{
    [self addTarget:self action:@selector(ButtonPress:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(ButtonRelease:) forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchUpOutside|UIControlEventTouchCancel];
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    if (m_pHatSwitch != NULL)
    {
        [m_pHatSwitch InitializeSatelliteButton:self];
    }

    CGRect rIcon = [self IconRect];
    CGRect rText = [self TextRect];
    
    //DL - moves icon and text on any button
    //rIcon.origin.y += 20;
    //rText.origin.y += 20;

    [m_pIconView setFrame:rIcon];
    [m_pLabel setFrame:rText];
}
/*============================================================================*/

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event

/*============================================================================*/
{
    if (![super pointInside:point withEvent:event])
        return FALSE;

    if (m_pEdgeList == NULL)
        return TRUE;
        
    if([m_pEdgeList IsPointInside:point] != 0)
        return TRUE;
    return FALSE;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pEdgeList release];
    [m_pText release];
    [m_pIcon release];
    [m_pLabel release];
    [m_pIconView release];
    [m_RGB release];
    [m_RGBFlash release];
    [m_RGBCheck release];

    if (m_pImageStd != NULL)
        CGImageRelease(m_pImageStd);
    if (m_pImageFlash != NULL)
        CGImageRelease(m_pImageFlash);

    [super dealloc];
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    int iState = ELEMENT_STD;
    if (m_bChecked)
        iState = ELEMENT_CHECK;
    CGImageRef pRef = [self ImageRefForState:iState];
    [layer setContents:(id)pRef];
}
/*============================================================================*/

-(int)CommandID

/*============================================================================*/
{
    return m_iCommandID;
}
/*============================================================================*/

-(void)SetCommandID:(int)iCommandID Responder:(id)pResponder

/*============================================================================*/
{
    m_iCommandID = iCommandID;
    m_pCommandResponder = pResponder;
}
/*============================================================================*/

-(void)SetCommandID:(int)iCommandID ReleaseID:(int)iReleaseID Responder:(id)pResponder;

/*============================================================================*/
{
    m_iCommandID = iCommandID;
    m_iReleaseID = iReleaseID; 
    m_pCommandResponder = pResponder;
}
/*============================================================================*/

-(void)SetRepeatDwell:(int)iDwell Rate:(int)iRate

/*============================================================================*/
{
    m_iRepeatDwell = iDwell;
    m_iRepeatRate = iRate;
//    [self setExclusiveTouch:(iRate != 0)];
}
/*============================================================================*/

-(void)ButtonPress:(id)sender

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    [pDelegate PlaySoundButtonPress];
    
    [m_pCommandResponder OnCommand:m_iCommandID];

    if (m_iRepeatRate > 0)
    {
        [m_pDwellTimer invalidate];
        [m_pDwellTimer release];
        [m_pRepeatTimer invalidate];
        [m_pRepeatTimer release];
        m_pDwellTimer = NULL;
        m_pRepeatTimer = NULL;
        float fTime = (float) m_iRepeatDwell / 1000.f;
        m_pDwellTimer = [[NSTimer scheduledTimerWithTimeInterval:fTime target:self selector:@selector(PressDwell) userInfo:nil repeats:NO] retain];
    }

    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithBool:YES]forKey:kCATransactionDisableActions];
    CGImageRef pRef = [self ImageRefForState:ELEMENT_FLASH];
    [[self layer] setContents:(id)pRef];
    [CATransaction commit];
}
/*============================================================================*/

-(void)ButtonRelease:(id)sender

/*============================================================================*/
{
    [m_pDwellTimer invalidate];
    [m_pDwellTimer release];
    [m_pRepeatTimer invalidate];
    [m_pRepeatTimer release];
    m_pDwellTimer = NULL;
    m_pRepeatTimer = NULL;

    if (m_pCommandResponder != NULL && m_iReleaseID != 0)
    {
        [m_pCommandResponder OnCommand:m_iReleaseID];
    }

    int iType = ELEMENT_STD;
    if (m_bChecked)
        iType = ELEMENT_CHECK;

    CGImageRef pRef = [self ImageRefForState:iType];

    CABasicAnimation *crossFade = [CABasicAnimation animationWithKeyPath:@"contents"];
    crossFade.duration = 0.25;
    crossFade.fromValue = self.layer.contents;
    crossFade.toValue = (id)pRef;
    [self.layer addAnimation:crossFade forKey:@"animateContents"];
    [self.layer setContents:(id)pRef];
}
/*============================================================================*/

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag

/*============================================================================*/
{
    if (!(m_iFlashState & FLASHSTATE_WANTFLASH))
    {
        // Don't want flash anymore... are we done?
        if (!(m_iFlashState & FLASHSTATE_LOWTOHIGH))
        {
            // Done
            m_iFlashState = 0;
            return;
        }
    }

    int iType = ELEMENT_FLASH;
    if (m_iFlashState & FLASHSTATE_LOWTOHIGH)
    {
        // Next is high to low
        if (m_bChecked)
            iType = ELEMENT_CHECK;
        else
            iType = ELEMENT_STD;
        m_iFlashState &= ~FLASHSTATE_LOWTOHIGH;
    }
    else
    {
        // Next is low to high
        m_iFlashState |= FLASHSTATE_LOWTOHIGH;
    }

    CGImageRef pRef = [self ImageRefForState:iType];

    CABasicAnimation *crossFade = [CABasicAnimation animationWithKeyPath:@"contents"];
    crossFade.duration = 0.5;
    crossFade.fromValue = self.layer.contents;
    crossFade.toValue = (id)pRef;
    [crossFade setDelegate:self];
    [self.layer addAnimation:crossFade forKey:@"animateContents"];
    [self.layer setContents:(id)pRef];
}
/*============================================================================*/

-(CGRect)TextRect;

/*============================================================================*/
{
    CGRect rFrame = self.bounds;

    int iInsetX = rFrame.size.width / 20;
    int iInsetY = rFrame.size.height / 20;

    int iDXThis = rFrame.size.width;
    int iDYThis = rFrame.size.height;

    CGRect rRet = CGRectMake(iInsetX,iInsetY,iDXThis-2*iInsetX-2, iDYThis-2*iInsetY);

    if (m_pEdgeList != NULL)
    {
        rRet = m_rContent;
        iDXThis = m_rContent.size.width;
        iDYThis = m_rContent.size.height;
        iInsetX = 0;
        iInsetY = 0;
    }

    int iSizeMin = m_iTextHeight * m_pLabel.numberOfLines;
    iSizeMin = MIN(iSizeMin, self.bounds.size.height);

    if (rRet.size.height < iSizeMin)
    {
        int iAdd = iSizeMin - rRet.size.height;
        int i1 = iAdd / 2;
        rRet.origin.y -= i1;
        rRet.size.height += iAdd;
        
    }


    if (m_pIconView == NULL)
        return rRet;

    CGRect rIcon = [self IconRect];
    switch (m_iIconFormat)
    {
    case HL_ICONCENTERMAX:
        {
            return CGRectZero;
        }
        break;

    case HL_ICONCENTER:
        break;

    case HL_ICONRIGHTTEXT1:
    case HL_ICONRIGHTTEXT2:
    case HL_ICONRIGHTTEXT4:
    case HL_ICONRIGHTTEXTMAX:
    case HL_ICONRIGHTTEXT_150PCT:
    case HL_ICONLEFTTHIRD:
    case HL_ICONLEFTHALF:
        {
            // Icon to the LEFT text to the RIGHT
            int iDim = m_iTextHeight;
            if (m_iIconFormat == HL_ICONRIGHTTEXT4)
                iDim = 4 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONRIGHTTEXT2)
                iDim = 2 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONRIGHTTEXT_150PCT)
                iDim = 150 * m_iTextHeight / 100;
            else if (m_iIconFormat == HL_ICONLEFTTHIRD)
                iDim = rFrame.size.width / 3;
            else if (m_iIconFormat == HL_ICONLEFTHALF)
                iDim = rFrame.size.width / 2;

            [CRect BreakOffLeft:&rRet DX:iDim];
            return rRet;
        }
        break;


    case HL_ICONLEFTTEXT1:
    case HL_ICONLEFTTEXT2:
    case HL_ICONLEFTTEXT4:
    case HL_ICONLEFTTEXTMAX:
    case HL_ICONLEFTTEXT_150PCT:
    case HL_ICONRIGHTTHIRD:
    case HL_ICONRIGHTHALF:
        {
            // Icon to the RIGHT text to the LEFT
            int iDim = m_iTextHeight;
            if (m_iIconFormat == HL_ICONLEFTTEXT4)
                iDim = 4 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONLEFTTEXT2)
                iDim = 2 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONLEFTTEXT_150PCT)
                iDim = 150 * m_iTextHeight / 100;
            else if (m_iIconFormat == HL_ICONRIGHTTHIRD)
                iDim = rFrame.size.width / 3;
            else if (m_iIconFormat == HL_ICONRIGHTHALF)
                iDim = rFrame.size.width / 2;
            [CRect BreakOffRight:&rRet DX:iDim];
            return rRet;
        }
        break;

    case HL_ICONTOPTEXT:
    case HL_ICONBOTTOMTHIRD:
    case HL_ICONBOTTOMHALF:
    case HL_ICONBOTTOM1X:
    case HL_ICONBOTTOM2X:
    case HL_ICONBOTTOM3X:
    case HL_ICONBOTTOM4X:
        {
            rRet.size.height = rIcon.origin.y - iInsetY;
        }
        break;

    case HL_ICONBOTTOMTEXT:
    case HL_ICONTOPTHIRD:
    case HL_ICONTOPHALF:
    case HL_ICONTOP1X:
    case HL_ICONTOP2X:
    case HL_ICONTOP3X:
    case HL_ICONTOP4X:
        {
            rRet.origin.y = rIcon.origin.y += rIcon.size.height;
            rRet.size.height = iDYThis - iInsetY - rRet.origin.y;
        }
        break;
    }

    return rRet;

}
/*============================================================================*/

-(CGRect)IconRect

/*============================================================================*/
{
    int iEdge = 2;
    CGRect rFrame = self.bounds;

    int iDXThis = rFrame.size.width;
    int iDYThis = rFrame.size.height;

    int iInsetX = rFrame.size.width / 20;
    int iInsetY = rFrame.size.width / 20;

    CGRect rRect = CGRectMake(iInsetX,iInsetY,iDXThis-2*iInsetX-2, iDYThis-2*iInsetY);

    if (m_pEdgeList != NULL)
    {
        rRect = m_rContent;
        iDXThis = m_rContent.size.width;
        iDYThis = m_rContent.size.height;
        iInsetX = 0;
        iInsetY = 0;
    }

    int iOffset = m_iTextHeight + 1;

    switch (m_iIconFormat)
    {
    case HL_ICONCENTER1X:
    case HL_ICONCENTER2X:
        {
            int iDim = m_iTextHeight;
            if (m_iIconFormat == HL_ICONCENTER2X)
                iDim = 2 * m_iTextHeight;


            int iDX = iDim;
            int iDY = iDim;
            [CRect CenterDownToX:&rRect DX:iDX];
            [CRect CenterDownToY:&rRect DY:iDY];
            return rRect;
        }
        break;

    case HL_ICONCENTER:
        {
            int iDX = 2 * m_iTextHeight;
            int iDY = m_iTextHeight;
            int iSH2 = iEdge * 3 / 2;
            iDX = MIN(iDX, iDXThis - iSH2);
            iDY = MIN(iDY, iDYThis - iSH2);
            [CRect CenterDownToX:&rRect DX:iDX];
            [CRect CenterDownToY:&rRect DY:iDY];
            return rRect;
        }

    case HL_ICONRIGHTTEXT4:
    case HL_ICONRIGHTTEXT2:
    case HL_ICONRIGHTTEXT1:
    case HL_ICONRIGHTTEXT_150PCT:
        {
            // Icon to the LEFT text to the RIGHT
            int iDim = m_iTextHeight;
            if (m_iIconFormat == HL_ICONRIGHTTEXT4)
                iDim = 4 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONRIGHTTEXT2)
                iDim = 2 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONRIGHTTEXT_150PCT)
                iDim = 150 * m_iTextHeight / 100;

            int iDimY = MIN(iDim, rRect.size.height);
            CGRect rRet =  [CRect BreakOffLeft:&rRect DX:iDim];
            int iInset = MAX(0, (rRet.size.height - iDimY)/2);
            [CRect Inset:&rRet DX:0 DY:iInset];
            return rRet;
        }
        break;

    case HL_ICONLEFTTEXT4:
    case HL_ICONLEFTTEXT2:
    case HL_ICONLEFTTEXT1:
    case HL_ICONLEFTTEXT_150PCT:
        {
            // Icon to the RIGHT text to the LEFT
            int iDim = m_iTextHeight;
            if (m_iIconFormat == HL_ICONLEFTTEXT4)
                iDim = 4 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONLEFTTEXT2)
                iDim = 2 * m_iTextHeight;
            else if (m_iIconFormat == HL_ICONLEFTTEXT_150PCT)
                iDim = 150 * m_iTextHeight / 100;

            int iDimY = MIN(iDim, rRect.size.height);
            CGRect rRet =  [CRect BreakOffRight:&rRect DX:iDim];
            int iInset = MAX(0, (rRet.size.height - iDimY)/2);
            [CRect Inset:&rRet DX:0 DY:iInset];
            return rRet;
        }
        break;

    case HL_ICONTOPTEXT:
        {
            rRect.origin.y += iOffset;
            rRect.size.height -= iOffset;
        }
        break;

    case HL_ICONBOTTOMTEXT:
        {
            rRect.size.height -= iOffset;
        }
        break;
    case HL_ICONRIGHTTEXTMAX:
        {
            rRect.size.width = MIN(rRect.size.width, rRect.size.height);
        }
        break;

    case HL_ICONLEFTTEXTMAX:
        {
            rRect.origin.x = iDXThis - iEdge - rRect.size.height;
            rRect.size.width = rRect.size.height;

        }
        break;

    case HL_ICONTOPHALF:
        rRect.size.height /= 2;
        break;
    case HL_ICONBOTTOMHALF:
        {
            rRect.size.height /= 2;
            rRect.origin.y += rRect.size.height;
        }
        break;
    case HL_ICONLEFTHALF:
        rRect.size.width /= 2;
        break;
    case HL_ICONRIGHTHALF:
        {
            rRect.size.width /= 2;
            rRect.origin.x += rRect.size.width;
        }
        break;
    case HL_ICONTOPTHIRD:
        rRect.size.height /= 3;
        break;
    case HL_ICONBOTTOMTHIRD:
        {
            rRect.size.height /= 3;
            rRect.origin.y += (rRect.size.height)*2;
        }
        break;
    case HL_ICONLEFTTHIRD:
        rRect.size.width /= 3;
        break;
    case HL_ICONRIGHTTHIRD:
        {
            rRect.size.width /= 3;
            rRect.origin.x += (rRect.size.width)*2;
        }
        break;

    case HL_ICON75PCTHT:
        {
            int iD = iDYThis * 75 / 100;
            iD = MIN(iD, iDXThis * 75 / 100);
            [CRect CenterDownToX:&rRect DX:iD];
            [CRect CenterDownToY:&rRect DY:iD];
            return rRect;
        }
        break;

    case HL_ICONTOP1X:
        return SplitRectY(rRect, iOffset, 2, 0, 1);
    case HL_ICONTOP2X:
        return SplitRectY(rRect, iOffset, 3, 0, 2);
    case HL_ICONTOP3X:
        return SplitRectY(rRect, iOffset, 4, 0, 3);
    case HL_ICONTOP4X:
        return SplitRectY(rRect, iOffset, 5, 0, 4);

    case HL_ICONBOTTOM1X:
        return SplitRectY(rRect, iOffset, 2, 1, 1);
    case HL_ICONBOTTOM2X:
        return SplitRectY(rRect, iOffset, 3, 1, 2);
    case HL_ICONBOTTOM3X:
        return SplitRectY(rRect, iOffset, 4, 1, 3);
    case HL_ICONBOTTOM4X:
        return SplitRectY(rRect, iOffset, 5, 1, 4);

    }


    return rRect;
}
/*============================================================================*/

    CGRect                      SplitRectY(
    CGRect                      rStart,
    int                         iChunkSize,
    int                         iNChunksTotal,
    int                         iStartingIndex,
    int                         iNChunks )

/*============================================================================*/
{
    int iYT = rStart.origin.y;
    int iYB = iYT + rStart.size.height;
    int iYMid = rStart.origin.y + rStart.size.height / 2;
    int iDY = iChunkSize * iNChunksTotal;
    int iY0 = iYMid - iDY / 2 + iStartingIndex * iChunkSize;
    int iY1 = iY0 + iNChunks * iChunkSize;
    iY0 = MAX(iYT, iY0);
    iY1 = MIN(iYB, iY1);

    return CGRectMake(rStart.origin.x, iY0, rStart.size.width, iY1-iY0);
}
/*============================================================================*/

-(int)ShadeIn               { return m_iShadeIn;    }
-(int)ShadeOut              { return m_iShadeOut;   }
-(int)ROutside              { return m_iRad;        }
-(void)SetShadeIn:(int)iNew                                     { m_iShadeIn = iNew;                }
-(void)SetShadeOut:(int)iNew                                    { m_iShadeOut = iNew;               }
-(void)SetROutside:(int)iNew                                    { m_iRad = iNew;                    }   
-(void)SetHatSwitch:(CHatSwitch*)pHatSwitch                     { m_pHatSwitch = pHatSwitch;        }

/*============================================================================*/

-(void)SetRenderDataStd:(CGImageRef)pRefStd Flash:(CGImageRef)pRefFlash Edges:(CNSEdgeList*)pEdges ContentRect:(CGRect) rContent

/*============================================================================*/
{
    if (m_pImageStd != NULL)
    {
        CGImageRelease(m_pImageStd);
    }
    if (m_pImageFlash != NULL)
    {
        CGImageRelease(m_pImageFlash);
    }

    m_pImageStd = pRefStd;
    m_pImageFlash = pRefFlash;

    [m_pEdgeList release];
    m_pEdgeList = pEdges;
    [m_pEdgeList retain];
    m_rContent = rContent;
}
/*============================================================================*/

-(UIColor*)RGBFace          { return m_RGB;         }
-(UIColor*)RGBFlash         { return m_RGBFlash;    }

/*============================================================================*/

-(UIColor*)RGBCheck

/*============================================================================*/
{ 
    if (m_RGBCheck != NULL)
        return m_RGBCheck;

    return [CHLComm GetRGB:RGB_SELECT_SUBTLE] ;   
}
/*============================================================================*/

-(UILabel*)Label            { return m_pLabel;      }

/*============================================================================*/

-(void)IconFormat:(int)iNew

/*============================================================================*/
{
    m_iIconFormat = iNew;
}
/*============================================================================*/

-(void)SetIcon:(NSString*)psIcon

/*============================================================================*/
{
    [m_pIcon release];
    m_pIcon = psIcon;
    [m_pIcon retain];

    [m_pIconView removeFromSuperview];
    [m_pIconView release];
    m_pIconView = NULL;
    if (psIcon == NULL)
        return;
        
    m_pIconView = [[CUserTabIconView alloc] initWithFrame:[self IconRect]];
    [m_pIconView setOpaque:NO];
    [m_pIconView SetUserIcon:psIcon];
//    [self insertSubview:m_pIconView belowSubview:m_pHighlight];
    [self addSubview:m_pIconView];
}
/*============================================================================*/

-(void)SetIconEmbedded:(NSString *)psIcon

/*============================================================================*/
{
    [m_pIcon release];
    m_pIcon = psIcon;
    [m_pIcon retain];

    [m_pIconView removeFromSuperview];
    [m_pIconView release];
    m_pIconView = NULL;
    if (psIcon == NULL)
        return;
        
    m_pIconView = [[CUserTabIconView alloc] initWithFrame:[self IconRect]];
    [m_pIconView setOpaque:NO];
    [m_pIconView SetEmbeddedIcon:psIcon];
//    [self insertSubview:m_pIconView belowSubview:m_pHighlight];
    [self addSubview:m_pIconView];
}
/*============================================================================*/

-(void)SetColor:(UIColor*)pRGB

/*============================================================================*/
{
    if (pRGB == NULL)
        return;

    [m_RGB release];
    m_RGB = pRGB;
    [m_RGB retain];
    [self setNeedsDisplay];
}
/*============================================================================*/

-(void)SetCheckedColor:(UIColor*)pRGB

/*============================================================================*/
{
    [m_RGBCheck release];
    m_RGBCheck = pRGB;
    [m_RGBCheck retain];
    [self setNeedsDisplay];
}
/*============================================================================*/

-(void)SetMaxLinesText:(int)iNew

/*============================================================================*/
{
    m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
    m_pLabel.numberOfLines = iNew;
}
/*============================================================================*/

-(void)SetTextSize:(int)iTextSize

/*============================================================================*/
{
    [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];
}
/*============================================================================*/

-(void)SetTextColor:(UIColor*)pRGB

/*============================================================================*/
{
    if (pRGB == NULL)
        return;
    [m_pLabel setTextColor:pRGB];
}
/*============================================================================*/

-(void)SetText:(NSString*)pText

/*============================================================================*/
{
    [m_pText release];
    m_pText = pText;
    [m_pText retain];
    [m_pLabel setText:pText];
}
/*============================================================================*/

-(BOOL)IsChecked

/*============================================================================*/
{
    return m_bChecked;
}
/*============================================================================*/

-(void)SetChecked:(BOOL)bNew

/*============================================================================*/
{
    if (bNew == m_bChecked)
        return;

    m_bChecked = bNew;

    int iType = ELEMENT_STD;
    if (m_bChecked)
        iType = ELEMENT_CHECK;
        
    CGImageRef pRef = [self ImageRefForState:iType];
    CABasicAnimation *crossFade = [CABasicAnimation animationWithKeyPath:@"contents"];
    crossFade.duration = 0.5;
    crossFade.fromValue = self.layer.contents;
    crossFade.toValue = (id)pRef;
    [self.layer addAnimation:crossFade forKey:@"animateContents"];
    [self.layer setContents:(id)pRef];
}
/*============================================================================*/

-(void)SetFlashingColor:(UIColor*)pRGB

/*============================================================================*/
{
    [m_RGBFlash release];
    m_RGBFlash = pRGB;
    [m_RGBFlash retain];
}
/*============================================================================*/

-(void)SetFlashing:(BOOL)bNew

/*============================================================================*/
{
    if (bNew)
    {
        if (m_iFlashState & FLASHSTATE_WANTFLASH)
            return;

        m_iFlashState = FLASHSTATE_WANTFLASH;
        [self animationDidStop:NULL finished:TRUE];

        return;
    }

    if (!(m_iFlashState & FLASHSTATE_WANTFLASH))
        return;


    m_iFlashState &= ~FLASHSTATE_WANTFLASH;
}
/*============================================================================*/

-(void)SetEnabled:(BOOL)bNew

/*============================================================================*/
{
    [self setUserInteractionEnabled:bNew];
}
/*============================================================================*/

- (BOOL)resignFirstResponder

/*============================================================================*/
{
    return YES;
}
/*============================================================================*/

-(CGImageRef)ImageRefForState:(int)iState

/*============================================================================*/
{
    if (m_pHatSwitch != NULL)
    {
        if (m_pImageStd == NULL)
        {
            [m_pHatSwitch InitializeSatelliteButton:self];
        }
    }

    switch (iState)
    {
    case ELEMENT_STD:
        {
            if (m_pImageStd != NULL)
                return m_pImageStd;
        }
        break;
        
    case ELEMENT_FLASH:
        {
            if (m_pImageFlash != NULL)
                return m_pImageFlash;
        }
        break;
    }

    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;


    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    int iSHIN    = [self ShadeIn];
    int iSHOUT   = [self ShadeOut];
    int iRad     = [self ROutside];
    int iGradient = [CHLComm GetInt:INT_BUTTON_GRADIENT];

    unsigned int rgbT = 0;
    unsigned int rgbB = 0;
    
    switch (iState)
    {
    case ELEMENT_STD:
        {
            rgbT = [CShader MakeRGB:[self RGBFace]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
        }
        break;
    case ELEMENT_FLASH:
        {
            rgbT = [CShader MakeRGB:[self RGBFlash]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
        }
        break;
    case ELEMENT_CHECK:
        {
            UIColor *prgbCheck = [CHLComm GetRGB:RGB_SELECT_SUBTLE];
            unsigned int rgbSel = [CShader MakeRGB:prgbCheck];
            int iOA1 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_TOP] * 100 / 255;
            int iOA2 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_BTM] * 100 / 255;
            rgbT = [CShader MakeRGB:[self RGBFace]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
            rgbT = [CShader BlendPCT:iOA1 RGB1:rgbSel RGB2:rgbT];
            rgbB = [CShader BlendPCT:iOA2 RGB1:rgbSel RGB2:rgbB];
        }
        break;
    }

    return [pDelegate GetButtonImage:iDX DY:iDY Rad:iRad SHIN:iSHIN SHOUT:iSHOUT ColorT:rgbT ColorB:rgbB];
}
/*============================================================================*/

-(void)PressDwell

/*============================================================================*/
{
    [m_pDwellTimer release];
    m_pDwellTimer = NULL;
    [m_pCommandResponder OnCommand:m_iCommandID];

    float fTime = 1.0f / (float)m_iRepeatRate;
    m_pRepeatTimer = [[NSTimer scheduledTimerWithTimeInterval:fTime target:self selector:@selector(PressRepeat) userInfo:nil repeats:YES] retain];
}
/*============================================================================*/

-(void)PressRepeat

/*============================================================================*/
{
    [m_pCommandResponder OnCommand:m_iCommandID];
}
/*============================================================================*/

-(BOOL)HasCapture

/*============================================================================*/
{
    return (m_pDwellTimer != NULL || m_pRepeatTimer != NULL);
}
/*============================================================================*/

-(int)GetData

/*============================================================================*/
{
    return m_iUserData;
}
/*============================================================================*/

-(void)SetData:(int)iData

/*============================================================================*/
{
    m_iUserData = iData;
}
/*============================================================================*/

+(NSString*)EmbeddedIconName:(int)iType

/*============================================================================*/
{
    NSString *pBuiltIn = NULL;
    switch (iType)
    {
    case AUDIO_ICON_PLAY:       pBuiltIn = @"PLAY.png";     break;
    case AUDIO_ICON_STOP:       pBuiltIn = @"STOP.png";     break;
    case AUDIO_ICON_PAUSE:      pBuiltIn = @"PAUSE.png";    break;
    case AUDIO_ICON_RIGHT:      pBuiltIn = @"RIGHT.png";    break;
    case AUDIO_ICON_LEFT:       pBuiltIn = @"LEFT.png";     break;
    case AUDIO_ICON_UP:         pBuiltIn = @"UP.png";       break;
    case AUDIO_ICON_DOWN:       pBuiltIn = @"DOWN.png";     break;
    case AUDIO_ICON_FFWD:       pBuiltIn = @"FFWD.png";     break;
    case AUDIO_ICON_RWND:       pBuiltIn = @"RWD.png";      break;
    case AUDIO_ICON_SKIP_FWD:   pBuiltIn = @"TRACK_FORWARD.png";    break;
    case AUDIO_ICON_SKIP_BACK:  pBuiltIn = @"TRACK_BACK.png";       break;
    case AUDIO_ICON_RECORD:     pBuiltIn = @"RECORD.png";           break;
    case AUDIO_ICON_POWER:      pBuiltIn = @"POWER.png";            break;
    case AUDIO_ICON_EJECT:      pBuiltIn = @"EJECT.png";            break;
    case AUDIO_ICON_SKIP_UP:    pBuiltIn = @"TRACK_BACK.png";       break;
    case AUDIO_ICON_SKIP_DOWN:  pBuiltIn = @"TRACK_FORWARD.png";    break;
    case AUDIO_ICON_UPDOWN:     pBuiltIn = @"UP_DOWN";          break;
    case AUDIO_ICON_LEFTRIGHT:  pBuiltIn = @"LEFT_RIGHT";       break;
    case AUDIO_ICON_PLUS:       pBuiltIn = @"LIST_PLUS.png";    break;
    case AUDIO_ICON_MINUS:      pBuiltIn = @"LIST_MINUS.png";   break;
    case AUDIO_ICON_CHECK:      pBuiltIn = @"CHECK.png";        break;
    case AUDIO_ICON_SHUFFLE:    pBuiltIn = @"SHUFFLE.png";      break;
    case AUDIO_ICON_REPEAT:     pBuiltIn = @"REPEAT.png";       break;
    case AUDIO_ICON_XPORT_HOME: pBuiltIn = @"HOME.png";         break;
    case AUDIO_ICON_REPLAY:     pBuiltIn = @"REPLAY.png";       break;
    case AUDIO_ICON_ADVANCE:    pBuiltIn = @"ADVANCE.png";      break;

    case ICON_GRAYUP:           pBuiltIn = @"UP.png"; break;
    case ICON_GRAYDN:           pBuiltIn = @"DOWN.png";  break;
    case ICON_SEEKUP:           pBuiltIn = @"UP.png"; break;
    case ICON_SEEKDN:           pBuiltIn = @"DOWN.png"; break;
    case ICON_TUNEUP:           pBuiltIn = @"UP.png"; break;
    case ICON_TUNEDN:           pBuiltIn = @"DOWN.png"; break;
    
    default:
        break;
    }
    return pBuiltIn;
}
@end


@implementation CToolBarButton

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Text:(NSString*)pText Icon:(NSString*)psIcon

/*============================================================================*/
{
    self = [super initWithFrame:CGRectZero IconFormat:HL_ICONTOP2X Style:0 Text:pText TextSize:[CMainView TEXT_SIZE_SIDEBAR] TextColor:NULL Color:NULL Icon:NULL];
    if (self)
    {
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            [self SetTextSize:5];
        }

        [self SetIconEmbedded:psIcon];
        m_iOrientation = TOOLBAR_BTM;
    }
    return self;
}
/*============================================================================*/

-(void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    int iState = ELEMENT_STD;
    if (m_bChecked)
        iState = ELEMENT_CHECK;
    CGImageRef pRef = [self ImageRefForState:iState];
    [layer setContents:(id)pRef];

    double d1PixelX = 5.0 / 50.0;
    double d1PixelY = 1.0 / self.bounds.size.height;
    CGRect rStretch = CGRectMake(d1PixelX, d1PixelY, 1.0 - 2 * d1PixelX, 1.0 - 2 * d1PixelY);

    [self setContentStretch:rStretch];
}
/*============================================================================*/

-(CGImageRef)ImageRefForState:(int)iState

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    CGImageRef pRef = [pDelegate GetToolBarImage:self.bounds.size.height Orientation:m_iOrientation State:iState FrameStyle:m_iFrameStyle];
    return pRef;
}
/*============================================================================*/

-(void)Orientation:(int)iNew

/*============================================================================*/
{
    m_iOrientation = iNew;
}
/*============================================================================*/

-(CGRect)IconRect

/*============================================================================*/
{
    CGRect rRet = [super IconRect];


    switch (m_iIconFormat)
    {
    case HL_ICONLEFTHALF:
    case HL_ICONRIGHTHALF:
        [CRect CenterDownToY:&rRet DY:2 * m_iTextHeight];
        break;
    }
    return rRet;
}
/*============================================================================*/

-(void)FrameStyle:(int)iNew

/*============================================================================*/
{
    if (iNew == m_iFrameStyle)
        return;
    m_iFrameStyle = iNew;
    [self InitShader];
}
@end

