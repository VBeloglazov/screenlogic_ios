//
//  PoolHistoryMainPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/15/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SubSystemPage.h"

@class CNSPoolConfig;
@class CPoolHistoryView;

/*============================================================================*/

@interface CPoolHistoryMainPage : CSubSystemPage

/*============================================================================*/
{
    CGRect                              m_rFrameOrig;
    CNSPoolConfig                       *m_pConfig;
    CPoolHistoryView                    *m_pHistoryView;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig;

@end
