//
//  PoolHistoryDataView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/15/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "timedataview.h"

@class CNSPoolConfig;
@class CRTEView;
@class CRTESeries;
@class CTimeBarLabel;

#define MIN_TEMP                55
#define MAX_TEMP                75

#define RGB_POOL_TEMP           0
#define RGB_SPA_TEMP            1
#define RGB_OUTSIDE_TEMP        2
#define RGB_SOLAR               3
#define RGB_HEAT                4
#define RGB_LIGHTS              5

/*============================================================================*/

@interface CPoolHistoryDataView : CTimeDataView <CNSSocketSink>

/*============================================================================*/
{
    int                             m_iDXLastWidth;

    CNSPoolConfig                   *m_pConfig;
    NSMutableArray                  *m_pTempSeries;
    NSMutableArray                  *m_pRTEViews;
    NSMutableArray                  *m_pRTEViewLabels;
    NSMutableArray                  *m_pRTESeries;
    NSTimer                         *m_pQueryTimer;
    
    int                             m_iTempMin;
    int                             m_iTempMax;

    BOOL                            m_bCheckScale;
    int                             m_iFrameResizeOld;
    int                             m_iFrameResizeNew;
}

-(id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView Config:(CNSPoolConfig*)pConfig LastView:(CPoolHistoryDataView*)pLastView;

-(int)YLogical:(int)iTemp;

-(NSMutableArray*)TempSeries;
-(NSMutableArray*)RTESeries;
-(void)PostQuery;
-(void)AddRTEViews;
-(void)UpdateRTEViews;
-(void)UpdateTimeBar;
-(void)UpdateLabel:(CTimeBarLabel*)pLabel;
-(void)AddInfoTextToView:(UIView*)pView Rect:(CGRect)rect PCT:(int)iPCT Color:(UIColor*)pColor;
-(int)CalcUsage:(CRTESeries*)pSeries StartDate:(NSDate*)pStartDate EndDate:(NSDate*)pEndDate;

-(int)MinTemp;
-(int)MaxTemp;
-(BOOL)SetScaleMin:(int)iMin Max:(int)iMax;
-(BOOL)ResizeFrameFrom:(int)iFrom To:(int)iTo;
-(void)SetCheckScale;
-(void)SetFrameResizeOld:(int)iDYDataOld New:(int)iDYDataNew;

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;

+(UIColor*)ItemColor:(int)iType;
+(UIColor*)FillColor:(int)iType;

-(int)AdjustMinTemp:(int)iMin Max:(int)iMax;

@end
