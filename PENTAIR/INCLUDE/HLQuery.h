/*
--------------------------------------------------------------------------------

    HLQUERY.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLQUERY_H
#define HLQUERY_H

#include "hlstructs.h"
#include "socksink.h"
#include "hltime.h"
#include "hloblist.h"

class CHLMSG;
class CSocketSink;
class CStatusSink;

/*====================================================================*/

    class CQACache    

/*====================================================================*/
{
protected:
    CHLMSG                      *m_pQ;
    CHLMSG                      *m_pA;
    CHLTimer                    m_hltAge;

public:
    CQACache(
    CHLMSG                      *pQ,
    CHLMSG                      *pA );

    virtual ~CQACache();

    inline CHLMSG               *Question() { return m_pQ;                      }
    inline CHLMSG               *Response() { return m_pA;                      }
    inline DWORD                Age()       { return m_hltAge.HLTickCount();    }
};
/*====================================================================*/

    class CHLQuery : public CSocketSink

/*====================================================================*/
{
protected:
    #ifdef MULTI_SERVER
        CHLServer               *g_pHLServer;
    #endif

    DWORD                       m_dwTimeout;
    BOOL                        m_bOwnResponse;
    BOOL                        m_bWaitResponse;
    CHLMSG                      *m_pQuestion;
    CHLMSG                      *m_pResponse;
    BOOL                        m_bAbort;
    short                       m_shResponseID;
    BOOL                        m_bPostOK;    
    BOOL                        m_bCache;

    static CHLObList            *m_pCache;


public:
    #ifdef MULTI_SERVER
        CHLQuery(CHLServer *pServer);
    #else
        CHLQuery(
        BOOL                    bCache = FALSE);
    #endif
    
    virtual ~CHLQuery();

    // CSocketSink override
    virtual void                SinkMessage(
    CHLMSG                      *pMSG );

    void                        SetTimeout(
    DWORD                       dwNew );

    static void                 InitCache();
    static void                 UnInitCache();
    static void                 RemoveAllCache();
    static CQACache             *FindQA(CHLMSG *pQ);

    #ifdef MAC_OS
        inline CHLMSG           *Response() { return m_pResponse;   }
    #endif

    static void                 AddQACache(
    CHLMSG                      *pQ,
    CHLMSG                      *pA );

    //
    // Questions that return full CHLMSG:
    //  - OK for pAnswer to be NULL in first one.
    //  - Others assert if pAnswer is NULL.
    //
    BOOL                        PostQuestion(
    CHLMSG                      *pQuestion );

    #ifdef HLUTIL
        BOOL                        WaitResponse(
        CHLMSG                      **pAnswer,
        CStatusSink                 *pSink = NULL );
    #else
        BOOL                        WaitResponse(
        CHLMSG                      **pAnswer );
    #endif

    BOOL                        AskQuestion(
    CHLMSG                      **pAnswer,
    CHLMSG                      *pQuestion );

    BOOL                        AskQuestion(
    CHLMSG                      **pAnswer,
    short                       ushQuestion,
    int                         iData1 );

    BOOL                        AskQuestion(
    CHLMSG                      **pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2 );

    BOOL                        AskQuestion(
    CHLMSG                      **pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    CHLString                   sData );

    //
    // Questions with no return
    //
    BOOL                        AskQuestionVoid(
    short                       ushQuestion );

    BOOL                        AskQuestionVoid(
    short                       ushQuestion,
    CHLString                   sText );

    BOOL                        AskQuestionVoid(
    short                       ushQuestion,
    int                         iVal );

    BOOL                        AskQuestionVoid(
    short                       ushQuestion,
    int                         iVal1,
    int                         iVal2 );

    //
    // Questions with single INT returns
    //
    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2 );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    int                         iData3 );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    int                         iData3,
    int                         iData4 );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    CHLString                   sText );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    CHLString                   sText );

    BOOL                        AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    CHLString                   sText );

    //
    // Questions with string returns
    //
    BOOL                        AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion );

    BOOL                        AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion,
    int                         iData  );

    BOOL                        AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2  );

    BOOL                        AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    int                         iData3  );
};
#endif

