//
//  MediaKeypadButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlbutton.h"
#import "hlview.h"

@class CControlDef;

/*============================================================================*/

@interface CMediaKeypadButton : CHLButton < CHLCommandResponder >

/*============================================================================*/
{
    NSString                            *m_pSearchText;
    UIView                              *m_pTextView;
    int                                 m_iZoneID;
    int                                 m_iType;
    int                                 m_iKeypadCmd;
    int                                 m_iID;
    NSTimer                             *m_pABCTimer;
}

-(id)initWithFrame:(CGRect)rFrame ZoneID:(int)iZoneID ControlDef:(CControlDef*)pDef;

@end
