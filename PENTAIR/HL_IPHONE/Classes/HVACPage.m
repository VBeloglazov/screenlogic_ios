//
//  HVACPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HVACPage.h"
#import "HVACTempPage.h"
#import "HVACHumidityPage.h"
#import "HVACPgmPage.h"
#import "HVACModePage_IPOD.h"
#import "hvachistview.h"
#import "hlview.h"
#import "tstat.h"
#import "hlm.h"
#import "MainView.h"
#import "crect.h"
#import "hlsegment.h"
#import "zoneselector.h"


@implementation CHVACPage

#define HVAC_PAGE_TEMP      0
#define HVAC_PAGE_MODE      1
#define HVAC_PAGE_SCHED     2
#define HVAC_PAGE_HISTORY   3
#define HVAC_PAGE_HUMIDITY  4

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:pSubTab];
    if (self)
    {
        [self Init:pSubTab];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            [self SelectPage:m_pPageCtrl];
            m_bInit = TRUE;
            m_iLastState = -1;
        }
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pTStat RemoveSink:self];
    [m_pMainView release];
    [m_pPage1 release];
    [m_pPage2 release];
    [m_pPage3 release];
    [m_pPage4 release];
    [m_pPage5 release];
    [m_pPageCtrl release];
    [super dealloc];
}
/*============================================================================*/

-(void)Init:(CSubTab*)pSubTab

/*============================================================================*/
{
    [super Init:pSubTab];
    m_pTStat = (CTStat*)pSubTab;
    
    CGRect rPage = self.bounds;


    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        [CRect BreakOffBottom:&rPage DY:[CMainView DY_TOOLBAR]];

    m_pMainView = [[UIView alloc] initWithFrame:rPage];
    [m_pMainView setBackgroundColor:[UIColor clearColor]];
    [m_pMainView setOpaque:NO];
    [self addSubview:m_pMainView];

    m_bProgramOption = [self ShowProgramOption];
    [self InitPageCtrl];
    [m_pPageCtrl SetSelectedIndex:0];
    [m_pTStat AddSink:self];
}
/*============================================================================*/

-(void)SelectPage:(id)sender

/*============================================================================*/
{
    UIView *pNewView = NULL;
    int iIndex = 0;
    if (sender != NULL)
    {
        CHLSegment *pCtrl = (CHLSegment*)sender;
        iIndex = [pCtrl SelectedIndex];
    }

    CGRect rPage = m_pMainView.bounds;

    if (!m_bProgramOption)
    {
        if (iIndex >= 2)
            iIndex++;
    }

    switch ([self SelectedPageType])
    {
    case HVAC_PAGE_TEMP: 
        {
            if (m_pPage1 == NULL)
                m_pPage1 = [[CHVACTempPage alloc] initWithFrame:rPage TStat:m_pTStat ReadOnly:NO];
            pNewView = m_pPage1;
        }
        break;
     case HVAC_PAGE_MODE: 
        {
            if (m_pPage2 == NULL)
                m_pPage2 = [[CHVACModePage_IPOD alloc] initWithFrame:rPage TStat:m_pTStat];
            pNewView = m_pPage2;
        }
        break;
    case HVAC_PAGE_HISTORY: 
        {
            if (m_pPage4 == NULL)
                m_pPage4 = [[CHVACHistoryView alloc] initWithFrame:rPage TStat:m_pTStat];
            pNewView = m_pPage4;
        }
        break;
            
    }

    if (pNewView == m_pPage)
        return;

    [self LockHorizontalScroll:iIndex == 3];
    [pNewView setFrame:m_pMainView.bounds];

    if (m_bInit)
    {
        [m_pMainView addSubview:pNewView];
        [self TranslateNewPage:pNewView OldPage:m_pPage ThisIndex:iIndex LastIndex:m_iLastIndex];
        m_pPage = pNewView;
        m_iLastIndex = iIndex;
        return;
    }

    [m_pPage removeFromSuperview];
    m_pPage = pNewView;
    [m_pMainView addSubview:pNewView];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [super SetVisible:bVisible];
    if (bVisible)
        [self LockHorizontalScroll:(m_pPage4 != NULL && m_pPage == m_pPage4)];

    if (m_pPage == m_pPage1)
        [m_pPage1 SetVisible:bVisible];
}
/*============================================================================*/

-(BOOL)ShouldRotate:(UIInterfaceOrientation)interfaceOrientation;

/*============================================================================*/
{
    return TRUE;
//    return (m_pPage4 != NULL && m_pPage == m_pPage4);
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    BOOL bShowProgram = [self ShowProgramOption];
    if (bShowProgram != m_bProgramOption)
    {
        int iIndex =  [m_pPageCtrl SelectedIndex];
        m_bProgramOption = bShowProgram;
        [self InitPageCtrl];
        if (!bShowProgram)
        {
            // Turning Off Page Ctrl
            switch (iIndex)
            {
            case 2: { [m_pPageCtrl SetSelectedIndex:1];   } break;
            case 3: { [m_pPageCtrl SetSelectedIndex:2];   } break;
            default:
                [m_pPageCtrl SetSelectedIndex:iIndex];
                break;
            }
        }
        else
        {
            // Turning On Page Ctrl
            switch (iIndex)
            {
            case 2: { [m_pPageCtrl SetSelectedIndex:3];   } break;
            default:
                [m_pPageCtrl SetSelectedIndex:iIndex];
                break;
            }
        }
    }

    [self InitSubTabViewOverlay];

    if ([m_pTStat HVACMode] != m_iLastState)
    {
        [self BeginUpdateSubTabViewOverlay:600 Every:20];
        m_iLastState = [m_pTStat HVACMode];
    }
}
/*============================================================================*/

-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    [m_pPage4 NotifyRotationComplete];
}
/*============================================================================*/

-(void)InitPageCtrl

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        [m_pPageCtrl removeFromSuperview];
        [m_pPageCtrl release];
        m_pPageCtrl = NULL;

        NSMutableArray *pItems = [[NSMutableArray alloc] init];
        [pItems addObject:@"Temp"];
        [pItems addObject:@"Mode"];
        if (m_bProgramOption)
            [pItems addObject:@"Program"];
        [pItems addObject:@"History"];
        

        CGRect rThis = self.bounds;
        CGRect rPageCtrl = [CRect BreakOffBottom:&rThis DY:[CMainView DY_TOOLBAR]];
        m_pPageCtrl = [[CHLSegment alloc] initWithFrame:rPageCtrl ImageProvider:NULL Items:pItems Type:HLSEGMENT_PAGE_CTRL Data:NO];
        
        [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];

        [self addSubview:m_pPageCtrl];

        [pItems release];
    }
}
/*============================================================================*/

-(void)InitSubTabViewOverlay

/*============================================================================*/
{
    CSubTabViewOverlay *pOverlay = [self Overlay];
    [pOverlay FreeClientView];

    if (pOverlay == NULL)
        return;

    CGRect rBounds = [pOverlay ClientRect];
    CHVACTempPage *pView = [[CHVACTempPage alloc] initWithFrame:rBounds TStat:m_pTStat ReadOnly:YES];

    [pOverlay SetClientView:pView];
    [pOverlay SetHighlightMode:([m_pTStat HVACMode] != HVAC_STATE_OFF && [m_pTStat HVACMode] != HVAC_STATE_NOTOK)];
        

    [pView release];
}
/*============================================================================*/

-(CHLSegment*)GetPageCtrl

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        return NULL;

    if (m_pPageCtrl != NULL)
        return m_pPageCtrl;
    
    NSMutableArray *pItems = [[NSMutableArray alloc] init];

    CHLSegmentIconView *p1 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"TEMP.png" Text:@"Temp"];
    [pItems addObject:p1];
    [p1 release];
    
    if ([m_pTStat HVACFlags] & HVAC_HAS_HUMIDITY_CONTROL)
    {
        CHLSegmentIconView *pH = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"IRRIGATION.png" Text:@"Humidity"];
        [pItems addObject:pH];
        [pH release];
    }
    
    CHLSegmentIconView *p2 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"HISTORY.png" Text:@"History"];
    [pItems addObject:p2];
    [p2 release];

    m_pPageCtrl = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Views:pItems  Type:HLSEGMENT_TOOLBAR];
    [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
    [pItems release];
    return m_pPageCtrl;
}
/*============================================================================*/

-(BOOL)ShowProgramOption;

/*============================================================================*/
{
    if (![m_pTStat HasSchedule])
        return FALSE;

    switch ([m_pTStat HVACMode])
    {
    case HVAC_STATE_COOL:
    case HVAC_STATE_HEAT:
    case HVAC_STATE_AUTO:
        return TRUE;
    default:
        break;
    }

    return FALSE;
}
/*============================================================================*/

-(void)UIInit

/*============================================================================*/
{
    if (!m_bInit)
    {
        [self SelectPage:m_pPageCtrl];
        m_bInit = TRUE;
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [m_pMainView setFrame:rBounds];
    }
    else 
    {
        int iDYTabs = [CMainView DY_TOOLBAR];
        if (rBounds.size.width > rBounds.size.height)
        {
            // Landscape Mode
            CGRect rTabBar = CGRectMake(0, rBounds.size.height, rBounds.size.width, iDYTabs);
            [m_pMainView setFrame:rBounds];
            [m_pPageCtrl setFrame:rTabBar];
        }
        else
        {
            // Portrait Mode
            CGRect rTabBar = [CRect BreakOffBottom:&rBounds DY:iDYTabs];
            [m_pMainView setFrame:rBounds];
            [m_pPageCtrl setFrame:rTabBar];
        }
    }
}
/*============================================================================*/

-(int)SelectedPageType

/*============================================================================*/
{
    int iIndex = [m_pPageCtrl SelectedIndex];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if ([m_pTStat HVACFlags] & HVAC_HAS_HUMIDITY_CONTROL)
        {
            switch(iIndex)
            {
            case 0: return HVAC_PAGE_TEMP;
            case 1: return HVAC_PAGE_HUMIDITY;
            case 2: return HVAC_PAGE_HISTORY;
            }
        }
        else
        {
            switch(iIndex)
            {
            case 0: return HVAC_PAGE_TEMP;
            case 1: return HVAC_PAGE_HISTORY;
            }
        }
    }
    else
    {
        if (m_bProgramOption)
        {
            switch(iIndex)
            {
            case 0: return HVAC_PAGE_TEMP;
            case 1: return HVAC_PAGE_MODE;
            case 2: return HVAC_PAGE_SCHED;
            case 3: return HVAC_PAGE_HISTORY;
            }
        }
        else
        {
            switch(iIndex)
            {
            case 0: return HVAC_PAGE_TEMP;
            case 1: return HVAC_PAGE_MODE;
            case 2: return HVAC_PAGE_HISTORY;
            }
            
        }

    }

    return HVAC_PAGE_TEMP;
}
@end
