//
//  MediaIconView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/14/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "MediaIconView.h"
#import "ImageServer.h"
#import "icon.h"
#import "hlm.h"
#import "TVChannelsView.h"

@implementation CMediaIconView


/*============================================================================*/

- (id)initWithFrame:(CGRect)frame 

/*============================================================================*/
{
	if (self = [super initWithFrame:frame]) 
    {
		// Initialization code
        m_pImage = NULL;
        [self setBackgroundColor:[UIColor clearColor]];
        [self setExclusiveTouch:NO];
	}
	return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    [self InitImageX:rect.size.width Y:rect.size.height ];
    if (m_pImage == NULL)
        return;

    int iDX = CGRectGetWidth(rect);
    int iDY = CGRectGetHeight(rect);
    int iDXImage = m_pImage.size.width;
    int iDYImage = m_pImage.size.height;
    int iDYDXOut = iDY * 100 / iDX;
    int iDYDXImage = iDYImage * 100 / iDXImage;
    int iDXOut = iDX;
    int iDYOut = iDY;
    
    if (iDYDXImage > iDYDXOut)
    {
        iDXOut = iDY * iDXImage / iDYImage;
    }
    else
    {
        iDYOut = iDX * iDYImage / iDXImage;
    }

    int iXOut = CGRectGetMidX(rect) - iDXOut / 2;
    int iYOut = CGRectGetMidY(rect) - iDYOut / 2;

    CGRect rOut = CGRectMake(iXOut+2, iYOut+2, iDXOut-4, iDYOut-4);

    [m_pImage drawInRect:rOut];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pImage release];
    [m_pIconName release];
	[super dealloc];
}
/*============================================================================*/

-(void)SetUserIcon:(NSString*) pNew

/*============================================================================*/
{
    [m_pImage release];
    m_pImage = NULL;

    [m_pIconName release];
    m_pIconName = pNew;
    [m_pIconName retain];
}
/*============================================================================*/

-(void)SetEmbeddedIcon:(NSString*) pNew

/*============================================================================*/
{
    [m_pImage release];
    m_pImage = [UIImage imageNamed:pNew];
    [m_pImage retain];

    [m_pIconName release];
    m_pIconName = pNew;
    [m_pIconName retain];

}
/*============================================================================*/

-(void)InitImageX:(int) iDimX Y:(int)iDimY

/*============================================================================*/
{
    if (m_pImage != NULL)
    {
        return;
    }

    CIcon *pIcon = [CImageServer IconByName:m_pIconName];
    CGImageRef pIconImage = [pIcon GetImage:self];

    if (pIconImage == NULL)
        return;

    if (([pIcon SourceType] == ENCODE_JPEG) || ([pIcon SourceType] == ENCODE_GIF))
    {
        pIconImage = [pIcon GetImageVirtualAlpha];
        if (pIconImage == NULL)
            return;
    }

    int iSizeData = iDimX * iDimY * 4;
    void *pImageData = malloc(iSizeData);
    memset(pImageData, 0, iSizeData);
    CGColorSpaceRef pColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef pRef = CGBitmapContextCreate(pImageData, iDimX, iDimY, 8, iDimX*4, pColorSpace, kCGImageAlphaPremultipliedFirst);
    CGContextScaleCTM(pRef, 1, -1);
    CGColorSpaceRelease(pColorSpace);
    UIGraphicsPushContext(pRef);

    int iDXIn = CGImageGetWidth(pIconImage);
    int iDYIn = CGImageGetHeight(pIconImage);
    int iDXOut = iDimX;
    int iDYOut = iDimY;
    if ((iDXIn * 1000 / iDYIn) > (iDimX * 1000 / iDimY))
    {
        iDYOut = iDYIn * iDXOut / iDXIn;
    }
    else
    {
        iDXOut = iDXIn * iDYOut / iDYIn;
    }

    CGRect rOutIcon = CGRectMake(iDimX/2-iDXOut/2, iDimY/2-iDYOut/2, iDXOut, iDYOut);

//    UIImage *pBase1 = [UIImage imageNamed:@"BASE_1.png"];
//    [pBase1 drawInRect:rOut];

    CGContextScaleCTM(pRef, 1, -1);
    CGContextDrawImage(pRef, rOutIcon, pIconImage);
    CGContextScaleCTM(pRef, 1, -1);

//    UIImage *pBase2 = [UIImage imageNamed:@"BASE_2.png"];
//    [pBase2 drawInRect:rOut];
    
    CGImageRef pImageRef = CGBitmapContextCreateImage(pRef);
    m_pImage = [[UIImage alloc] initWithCGImage:pImageRef];
    UIGraphicsPopContext();
    CGContextRelease(pRef);
    free(pImageData);
}
/*============================================================================*/

-(void)SetView:(CChannelCell*)pCell Index:(int)iIndex

/*============================================================================*/
{
    m_pCell = pCell;
    m_iIndex = iIndex;
}

/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    [m_pCell SelectChannel:m_iIndex];
    [super touchesBegan:touches withEvent:event];
    if (m_pOverlay != NULL)
    {
        [m_pOverlay removeFromSuperview];
        m_pOverlay = NULL;
    }
    
    m_pOverlay = [[UIView alloc] initWithFrame:self.bounds];
    m_pOverlay.backgroundColor = [UIColor blueColor];
    [self addSubview:m_pOverlay];
    [m_pOverlay release];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    [m_pOverlay setAlpha:0.0];
    [m_pOverlay setBackgroundColor:[UIColor whiteColor]];
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;

    [m_pOverlay removeFromSuperview];
    m_pOverlay = NULL;
}
@end
