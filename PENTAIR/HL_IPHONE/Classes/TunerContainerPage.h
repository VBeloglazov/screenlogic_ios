//
//  TunerContainerPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/23/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitRoundRegion.h"
#import "sink.h"

@class CHLButton;
@class CAudioZone;
@class CHLSegment;
@class CTunerStatusView;
@class CTunerHeaderView;
@class CMediaZonePage;

/*============================================================================*/

@interface CTunerContainerPage : CSplitRoundRegion < CSink >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    CTunerHeaderView                    *m_pHeaderView;
    CMediaZonePage                      *m_pMediaZonePage;
    int                                 m_iActivePageType;
    BOOL                                m_bInit;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone MediaZonePage:(CMediaZonePage*)pMediaZonePage;
-(void)NotifyEmptyFavorites;

@end


/*============================================================================*/

@interface CTunerHeaderView : UIView < CSink >

/*============================================================================*/
{
    CTunerContainerPage                 *m_pContainer;
    CAudioZone                          *m_pZone;
    int                                 m_iActivePageType;
    
    CHLButton                           *m_pBandButton;
    CHLButton                           *m_pFavButton;
    CHLSegment                          *m_pSegment;
    CTunerStatusView                    *m_pStatusView;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone Container:(CTunerContainerPage*)pContainer;
-(CTunerStatusView*)StatusView;
-(void)SetStationText:(NSString*)pText;
-(void)SelectPage:(int)iIndex;
-(CHLSegment*)SegmentControl;


@end


/*============================================================================*/

@interface CTunerStatusView : UIView < CSink >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    UILabel                             *m_pChannelText;
    UILabel                             *m_pChannelName;
    UILabel                             *m_pArtistText;
    UILabel                             *m_pTrackText;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone;
-(UILabel*)AddLabel;
-(void)SetStationText:(NSString*)pText;

@end
