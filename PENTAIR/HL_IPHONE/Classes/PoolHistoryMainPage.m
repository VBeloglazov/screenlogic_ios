//
//  PoolHistoryMainPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/15/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "PoolHistoryMainPage.h"
#import "PoolHistoryView.h"

#import "crect.h"


@implementation CPoolHistoryMainPage
/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:NULL]) 
    {
        m_rFrameOrig = rFrame;
        m_pConfig = pConfig;
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pHistoryView release];
    [super dealloc];
}
/*============================================================================*/

-(BOOL)ShouldRotate:(UIInterfaceOrientation)interfaceOrientation;

/*============================================================================*/
{
    return YES;
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [super SetVisible:bVisible];
    if (bVisible)
        [self LockHorizontalScroll:YES];

    if (m_pHistoryView == NULL && bVisible)
    {
        CGRect rFrame = self.frame;
        rFrame.origin.x = 0;
        rFrame.origin.y = 0;
        m_pHistoryView = [[CPoolHistoryView alloc] initWithFrame:rFrame Config:m_pConfig];
        [self addSubview:m_pHistoryView];
    }
}
/*============================================================================*/

-(void)NotifyRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo

/*============================================================================*/
{
    if (m_pHistoryView != NULL)
    {
        CGRect rPage = CGRectMake(0, 0, m_rFrameOrig.size.width, m_rFrameOrig.size.height);

        switch(ioTo)
        {
            case UIDeviceOrientationPortrait:
            case UIDeviceOrientationPortraitUpsideDown:
                break;

            case UIDeviceOrientationLandscapeLeft:
            case UIDeviceOrientationLandscapeRight:
                {
                    rPage = [CRect ScreenRect:ioTo];
                }
                break;
        }

        [m_pHistoryView setFrame:rPage];
        [m_pHistoryView NotifyRotation];
    }
}
/*============================================================================*/

-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    [m_pHistoryView NotifyRotationComplete];
}
@end
