//
//  MP3Menu.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#define MENU_AUDIO_ROOT                             100
#define MENU_AUDIO_NOWPLAYING                       101
#define MENU_AUDIO_SERVICE_ROOT                     102
#define MENU_AUDIO_LIBRARY_ARTISTS_ALPHA            103
#define MENU_AUDIO_LIBRARY_ARTISTS_ALL              104
#define MENU_AUDIO_LIBRARY_ALBUMSBYARTIST           105
#define MENU_AUDIO_LIBRARY_TRACKSBYARTISTALBUM      106
#define MENU_AUDIO_LIBRARY_ALBUMS_ALPHA             107
#define MENU_AUDIO_LIBRARY_ALBUMS_ALL               108
#define MENU_AUDIO_LIBRARY_TRACKSBYALBUM            109
#define MENU_AUDIO_LIBRARY_TRACKS_ALL               110
#define MENU_AUDIO_LIBRARY_PLAYLISTS                111
#define MENU_AUDIO_LIBRARY_TRACKSBYPLAYLIST         112
#define MENU_AUDIO_LIBRARY_COVERS_ALPHA             113
#define MENU_AUDIO_LIBRARY_ALBUMS_ALL_COVERS        114
#define MENU_AUDIO_LIBRARY_ALBUMSBYARTIST_COVERS    115
#define MENU_AUDIO_LIBRARY_ALBUM_GENRES             116
#define MENU_AUDIO_LIBRARY_ALBUMSBYGENRE_COVERS     117

#define MENU_IRADIO_ROOT                            200
#define MENU_IRADIO_ALL                             201
#define MENU_IRADIO_FAVS                            202
#define MENU_IRADIO_ALL_STATIONS_BYGENRE            203
#define MENU_IRADIO_FAV_STATIONS_BYGENRE            204

#define MENU_XM_ROOT                                300
#define MENU_XM_GENRES                              301
#define MENU_XM_FAVS                                302
#define MENU_XM_STATIONS                            303
#define MENU_XM_TUNE                                304

#define MENU_TUNER_ROOT                             400
#define MENU_TUNER_FAVS                             401
#define MENU_TUNER_TUNE                             402
#define MENU_TUNER_TUNE_AM                          403
#define MENU_TUNER_TUNE_FM                          404

#import "NSSocketSink.h"
#import "sink.h"
#import "hlview.h"
#import "MainView.h"
#import "MediaZonePage.h"
#import "HLTableFrame.h"
#import "MP3ContentView.h"

@class CAudioService;
@class CAudioZone;
@class CSubTab;
@class CMP3ItemCell;
@class CSectionHeader;
@class CAlphaScroll;
@class CNowPlayingPage;
@class CVolumeControl;
@class CMP3MenuView;
@class CMP3ContentView;

#define CMD_MP3_PLAY                    1000
#define CMD_MP3_ADD                     1001

#define DATASTATE_INIT                  0
#define DATASTATE_WAIT                  1
#define DATASTATE_OK                    2


/*============================================================================*/

@interface CMP3MenuItem : NSObject

/*============================================================================*/
{
    CAudioService                       *m_pService;
    NSString                            *m_pText;
    NSString                            *m_pAltText;
    int                                 m_iData;
    int                                 m_iType;
}

-(id)initWithName:(NSString*)pText Data:(int)iData Service:(CAudioService*)pService;
-(int)Data;
-(CAudioService*)Service;
-(BOOL)OnIdleVisible;
-(NSString*)Text;
-(NSString*)AltText;
-(int)Index;
-(int)ArtistIndex;
-(int)AlbumIndex;
-(int)TrackIndex;
-(int)GetType;
-(void)SetType:(int)iType;
-(CGImageRef)GetImage;

@end

/*============================================================================*/

@interface CMP3Menu : UIView                    <  UITableViewDelegate, 
                                                   UITableViewDataSource,
                                                   CNSSocketSink,
                                                   UIAlertViewDelegate,
                                                   CSink,
                                                   CHLCommandResponder >

/*============================================================================*/
{
    UITableView                         *m_pTableView;
    CMP3ContentView                     *m_pContentView;
    CAudioZone                          *m_pZone;
    int                                 m_iType;
    int                                 m_iSubType1;
    int                                 m_iSubType2;

    CAudioService                       *m_pService;
    NSMutableArray                      *m_pSections;
    NSMutableArray                      *m_pSectionHeaders;
    BOOL                                m_bWaitData;
    int                                 m_iNPacketsInFlight;

    int                                 m_iLastSel;
    int                                 m_iLastOptions;


    NSTimer                             *m_pTimerIO;
    UIView                              *m_pTextView;

    NSString                            *m_pTitle;
}

-(id)initWithFrame:(CGRect)rFrame
                ContentView:(CMP3ContentView*)pModalView
                Zone:(CAudioZone*)pSubTab
                Type:(int)iType
                Sub1:(int)iSub1 
                Sub2:(int)iSub2 
                Title:(NSString*)pTitle 
                Service:(CAudioService*)pService;

-(NSString*)Title;
-(CAudioService*)Service;
-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

-(void)Init;
-(void)AddItem:(NSString*)pText Data:(int)iData Service:(CAudioService*)pService;
-(NSMutableArray*)InitSingleSection;

-(void)BeginWaitData;
-(void)EndWaitData;
-(void)PostGetCount:(int)iType Sub1:(int)iSub1 Sub2:(int)iSub2;

-(int)GetPacketsInFlight;
-(void)SetPacketsInFlight:(int)iNew;

-(CAudioZone*)Zone;
-(int)SortType;
-(int)SortSubType1;
-(int)SortSubType2;
-(int)ActivePageID;

-(int)ItemOptions:(CMP3MenuItem*)pItem;
-(BOOL)ItemHasSubMenus:(CMP3MenuItem*)pItem;

-(CMP3ItemCell*)MP3CellForSection:(int)iSection Index:(int)iIndex;

-(IBAction)SearchEvent:(id)sender;

-(void)AnimateSelectedItem:(BOOL)bPlay;
-(void)QueueSelectedItem:(BOOL)bPlay;
-(void)TimerProc;

-(BOOL)HasAlphaInformation;

-(void)StartTimer;
-(void)PushView:(CMP3Menu*)pView;

-(void)SetID:(int)iID;
-(void)OnItemSelected:(int)iID;


@end
