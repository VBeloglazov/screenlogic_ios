//
//  PhotoAlbum.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PhotoAlbum.h"
#import "hlcomm.h"
#import "nsquery.h"
#import "nsmsg.h"
#import "PhotosPage.h"
#import "Photo.h"

@implementation CPhotoAlbumRoot
/*============================================================================*/

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;

/*============================================================================*/
{
    self = [super initWithName:pName ID:iID Data1:(int)iData1 Data2:(int)iData2];
    if (self)
    {
        
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pAlbums release];
    [super dealloc];
}
/*============================================================================*/

-(BOOL)IsLoaded

/*============================================================================*/
{
    return m_bLoaded;
}
/*============================================================================*/

-(void)SetPhotosPage:(CPhotosPage*)pPage

/*============================================================================*/
{
    m_pPhotosPage = pPage;
}
/*============================================================================*/

-(void)Load

/*============================================================================*/
{
    if (m_pAlbums != NULL)
        return;
    [CHLComm AddSocketSink:self];
    m_pAlbums = [[NSMutableArray alloc] init];
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_PICSERVER_GETALLALBUMSQ] autorelease];
    [pQ PutInt:1];
    [pQ SendMessageWithMyID:self];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_PICSERVER_GETALLALBUMSA)
        return;

    [self LoadAlbum:pMSG];
    [CHLComm RemoveSocketSink:self];

    m_bLoaded = TRUE;
    if (m_pPhotosPage != NULL)
    {
        [m_pPhotosPage NotifyDataReady];
    }
}
/*============================================================================*/

-(void)LoadAlbum:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iID         = [pMSG GetInt];
    NSString *pName = [pMSG GetString];

    CPhotoAlbum *pAlbum = [[CPhotoAlbum alloc] initWithName:pName ID:iID];
    [pAlbum LoadPictures:pMSG];

    if ([pAlbum NPhotos] > 0)
    {
        // Root Album has photos in it
        [pAlbum LoadAlbums:pMSG];
        [m_pAlbums addObject:pAlbum];
        [pAlbum release];
    }
    else
    {
        [pAlbum release];

        int iNAlbums        = [pMSG GetInt];
        for (int i = 0; i < iNAlbums; i++)
        {
            int iID         = [pMSG GetInt];
            NSString *pName = [pMSG GetString];

            pAlbum = [[CPhotoAlbum alloc] initWithName:pName ID:iID];
            [pAlbum LoadPictures:pMSG];
            [pAlbum LoadAlbums:pMSG];
            [m_pAlbums addObject:pAlbum];
            [pAlbum release];
        }
    }

}
/*============================================================================*/

-(int)NAlbums

/*============================================================================*/
{
    return [m_pAlbums count];
}
/*============================================================================*/

-(CPhotoAlbum*)Album:(int)iIndex

/*============================================================================*/
{
    CPhotoAlbum *pAlbum = (CPhotoAlbum*)[m_pAlbums objectAtIndex:iIndex];
    return pAlbum;
}

@end



@implementation CPhotoAlbum
/*============================================================================*/

-(id)initWithName:(NSString*)pName ID:(int)iID

/*============================================================================*/
{
    m_pName = pName;
    m_iID = iID;
    
    [m_pName retain];
    m_pPictures = [[NSMutableArray alloc] init];
    m_pAlbums   = [[NSMutableArray alloc] init];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [m_pPictures release];
    [m_pAlbums release];
    [super dealloc];
}
/*============================================================================*/

-(void)LoadPictures:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iNPics = [pMSG GetInt];
    for (int i = 0; i < iNPics; i++)
    {
        NSString *pName = [pMSG GetString];
        CPhoto *pPhoto = [[CPhoto alloc] initWithAlbum:self Index:i Name:pName];
        [m_pPictures addObject:pPhoto];
        [pPhoto release];
    }
}
/*============================================================================*/

-(void)LoadAlbums:(CNSMSG*)pMSG

/*============================================================================*/
{
    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        int iID         = [pMSG GetInt];
        NSString *pName = [pMSG GetString];

        CPhotoAlbum *pAlbum = [[CPhotoAlbum alloc] initWithName:pName ID:iID];
        [pAlbum LoadPictures:pMSG];
        [pAlbum LoadAlbums:pMSG];

        if ([pAlbum NPhotos] > 0 || [pAlbum NAlbums] > 0)
            [m_pAlbums addObject:pAlbum];
        [pAlbum release];
    }
}
/*============================================================================*/

-(int)ID                                { return m_iID;                 }
-(NSString*)Name                        { return m_pName;               }

/*============================================================================*/

-(int)NPhotos                           { return [m_pPictures count];   }

/*============================================================================*/

-(CPhoto*)Photo:(int)iIndex

/*============================================================================*/
{
    CPhoto *pPhoto = (CPhoto*)[m_pPictures objectAtIndex:iIndex];
    return pPhoto;
}
/*============================================================================*/

-(int)NAlbums                           { return [m_pAlbums count]; }

/*============================================================================*/

-(CPhotoAlbum*)Album:(int)iIndex

/*============================================================================*/
{
    CPhotoAlbum *pAlbum = (CPhotoAlbum*)[m_pAlbums objectAtIndex:iIndex];
    return pAlbum;
}
/*============================================================================*/

-(void)ReleaseAllHiRes;

/*============================================================================*/
{
    int iN = [m_pPictures count];
    for (int i = 0; i < iN; i++)
    {
        CPhoto *pPhoto = (CPhoto*)[m_pPictures objectAtIndex:i];
        [pPhoto ReleaseHiRes];
    }
}
@end
