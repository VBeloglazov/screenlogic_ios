//
//  tstat.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "tstat.h"
#import "SinkServer.h"
#import "hlcomm.h"
#import "NSQuery.h"
#import "NSMSG.h"
#import "SubTabCell.h"

@implementation CAltObject
/*============================================================================*/

-(id)initWithMSG:(CNSMSG*)pMSG

/*============================================================================*/
{
    m_iID   = [pMSG GetInt];
    m_pName = [[pMSG GetString] retain];
    m_pRGB  = [[pMSG GetColor] retain];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [m_pRGB release];
    [super dealloc];
}
@end

@implementation CTStat
/*============================================================================*/

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2

/*============================================================================*/
{
    if (self = [super initWithName:pName ID:iID Data1:iData1 Data2:iData2])
    {
        m_AltTemps = [[NSMutableArray alloc] init];
        m_AltStates = [[NSMutableArray alloc] init];

        m_pSinkServer = [[CSinkServer alloc] init];
        if (![self CustomPage])
        {
            // Send out query to get detailed info
            [CHLComm AddSocketSink:self];
            CNSQuery *pQ1 = [[CNSQuery alloc] initWithQ:HLM_HVAC_GETTSTATINFOBYIDQ];
            [pQ1 autorelease];
            [pQ1 PutInt:m_iID];
            [pQ1 SendMessageWithMyID:self];

            CNSQuery *pQ2 = [[CNSQuery alloc] initWithQ:HLM_HVAC_GETSCHEDLAYOUTBYIDQ];
            [pQ2 autorelease];
            [pQ2 PutInt:m_iID];
            [pQ2 SendMessageWithMyID:self];
        }
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (![self CustomPage])
    {
        [CHLComm RemoveSocketSink:self];
    }

    [m_AltTemps release];
    [m_AltStates release];
    [m_pSinkServer release];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
        if ([m_pSinkServer NSinks] > 0)
            [self InitComm];
    }
    else 
    {
        [self ReleaseComm];
    }

}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    switch ([pMSG MessageID])
    {
    case HLM_HVAC_SETHEATA:
    case HLM_HVAC_SETCOOLA:
    case HLM_HVAC_SETDEHUMIDA:
    case HLM_HVAC_SETHUMIDA:
        m_iNPacketsInFlight--;
        break;
        
    case HLM_HVAC_GETTSTATINFOBYIDA:
        {
            m_iHeatUnitID       = [pMSG GetInt];
            m_iCoolUnitID       = [pMSG GetInt];
            m_iVentUnitID       = [pMSG GetInt];
            m_iHVACFlags        = [pMSG GetInt];

            [pMSG GetInt]; // Active Schedule, chuck this


            [pMSG GetInt];  // Max Hold Deviation Degrees
            [pMSG GetInt];  // Max Hold Time (mins)

            m_iMinHeatSP        = [pMSG GetInt];     // Min Heat SP
            m_iMaxHeatSP        = [pMSG GetInt];     // Max Heat SP
            m_iMinCoolSP        = [pMSG GetInt];     // Min Cool SP
            m_iMaxCoolSP        = [pMSG GetInt];     // Max Cool SP

            [pMSG GetInt]; // Remote Flags1
            [pMSG GetInt]; // Remote Flags2

            [pMSG GetString];   // Remote Names1
            [pMSG GetString];   // Remote Names1

            // Graph Objects
            for (int iPass = 0; iPass < 2; iPass++)
            {
                NSMutableArray *pArray = NULL;
                switch (iPass)
                {
                case 0: pArray = m_AltTemps;    break;    
                case 1: pArray = m_AltStates;   break;    
                }

                int iNAlt = [pMSG GetInt];
                for (int i = 0; i < iNAlt; i++)
                {
                    CAltObject *pObj = [[CAltObject alloc] initWithMSG:pMSG];
                    [pArray addObject:pObj];
                    [pObj release];
                }

            }

           [m_pSinkServer NotifySinks:m_iID Data:SINK_CONFIGCHANGED];
        }
        break;

    case HLM_HVAC_GETSCHEDLAYOUTBYIDA:
        {
            [pMSG GetInt];                  // Global/Local
            int iNWeekMaps = [pMSG GetInt]; // NWeekMaps
            m_bHasSchedule = (iNWeekMaps > 0);
        }
        break;

    case HLM_HVAC_STATECHANGED:
        {
            if (m_iNPacketsInFlight == 0)
                [self PostGetState];
        }
        break;

    case HLM_HVAC_GETSTATEA:
        {
            int iLastTemp = m_iActualTemp;
            int iLastState = m_iHVACState;

            if ([self ProcessStateMSG:pMSG])
            {
                [m_pSinkServer NotifySinks:m_iID Data:0];

                CSubTabCell *pCell = [self GetSubTabCell];
                if (iLastTemp != m_iActualTemp || iLastState != m_iHVACState)
                {
                    if (pCell != NULL)
                        [self UpdateSubTabCell:pCell];
                }
            }
        }
    }
}
/*============================================================================*/

-(BOOL)CustomPage

/*============================================================================*/
{
    return (m_iData1 & SYSCOMP_CUSTOMPAGE);
}
/*============================================================================*/

-(BOOL)HasHeat      { return (m_iHeatUnitID > 0 && m_iHeatUnitID != HLID_NONE); }
-(BOOL)HasCool      { return (m_iCoolUnitID > 0 && m_iCoolUnitID != HLID_NONE); }
-(BOOL)HasFan       { return (m_iVentUnitID > 0 && m_iVentUnitID != HLID_NONE); }
-(int)HVACFlags     { return m_iHVACFlags;                                      }
-(int)HVACMode      { return m_iHVACState;                                      }
-(int)FanMode       { return m_iFanState;                                       }
-(int)ProgramState  { return m_iProgramState;                                   }
-(BOOL)StateOK      { return m_bStateOK;                                        }
-(BOOL)HasSchedule  { return m_bHasSchedule;                                    }

/*============================================================================*/

-(int)HoldTimeMinutes

/*============================================================================*/
{
    return (m_iTempTime / 60);
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer AddSink:pSink];
    [self InitComm];
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    [m_pSinkServer RemoveSink:pSink];
    if ([m_pSinkServer NSinks] == 0 && m_pSubTabCell == NULL)
        [self ReleaseComm];
}
/*============================================================================*/

-(void)InitComm

/*============================================================================*/
{
    if (m_bCommInit)
        return;
        
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_HVAC_ADDCLIENTQ];
    [pQ autorelease];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];
    [self PostGetState];
    
    m_bCommInit = TRUE;
}
/*============================================================================*/

-(void)ReleaseComm

/*============================================================================*/
{
    if (!m_bCommInit)
        return;

    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_HVAC_REMOVECLIENTQ];
    [pQ autorelease];
    [pQ PutInt:[CHLComm SenderID:self]];
    [pQ SendMessageWithMyID:self];

    m_bCommInit = FALSE;
}
/*============================================================================*/

-(void)PostGetState

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_HVAC_GETSTATEQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ SendMessageWithMyID:self];

    m_bCommInit = FALSE;
}
/*============================================================================*/

-(BOOL)ProcessStateMSG:(CNSMSG*)pMSG

/*============================================================================*/
{
    [pMSG ResetRead];
    int iID = [pMSG GetInt];
    if (iID != m_iID)
        return FALSE;
    m_iHVACState    = [pMSG GetInt];
    m_iFanState     = [pMSG GetInt];
    m_iProgramState = [pMSG GetInt];
    m_iActualTemp   = [pMSG GetInt];
    m_iHeatSP       = [pMSG GetInt];
    m_iCoolSP       = [pMSG GetInt];
    m_bHeatActive   = [pMSG GetInt];
    m_bCoolActive   = [pMSG GetInt];
    m_bFanActive    = [pMSG GetInt];

    if ([CHLComm VersionMajor] >= 5 && [CHLComm VersionMinor] >= 1)
    {
        m_iHumidity = [pMSG GetInt]; //Humid Act
        m_iHumSPHi  = [pMSG GetInt]; //Dehum SP
        m_iHumSPLo  = [pMSG GetInt]; //Humid SP
    }


    [pMSG GetInt]; //R1
    [pMSG GetInt]; //R2
    [pMSG GetInt]; //E-Heat
    m_iTempTime     = [pMSG GetInt];
    [pMSG GetString]; // Status
    m_bReady        = [pMSG GetInt];
    m_bStateOK      = TRUE;
    
    return TRUE;
}
/*============================================================================*/

-(int)RoomTemp      { return m_iActualTemp; }
-(int)CoolSP        { return m_iCoolSP;     }
-(int)HeatSP        { return m_iHeatSP;     }
-(BOOL)HeatActive   { return m_bHeatActive; }
-(BOOL)CoolActive   { return m_bCoolActive; }
-(int)Humidity      { return m_iHumidity;   }
-(int)HumSPLo       { return m_iHumSPLo;    }
-(int)HumSPHi       { return m_iHumSPHi;    }

/*============================================================================*/

-(void)HumSPLo:(int)iNew

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_HVAC_SETHUMIDQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iNew];
    [pQ SendMessageWithMyID:self];
    m_iNPacketsInFlight++;
    m_iHumSPLo = iNew;
    [m_pSinkServer NotifySinks:0 Data:0];
}
/*============================================================================*/

-(void)HumSPHi:(int)iNew

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_HVAC_SETDEHUMIDQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iNew];
    [pQ SendMessageWithMyID:self];
    m_iNPacketsInFlight++;
    m_iHumSPHi = iNew;
    [m_pSinkServer NotifySinks:0 Data:0];
}
/*============================================================================*/

-(int)MaxHeatSP     { return m_iMaxHeatSP;  }
-(int)MinHeatSP     { return m_iMinHeatSP;  }
-(int)MaxCoolSP     { return m_iMaxCoolSP;  }
-(int)MinCoolSP     { return m_iMinCoolSP;  }

/*============================================================================*/

-(void)SetHVACMode:(int)iMode

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_HVAC_UNITCOMMANDQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iMode];
    [pQ PutInt:m_iFanState];
    [pQ SendMessage];
}
/*============================================================================*/

-(void)SetFanMode:(int)iMode

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_HVAC_UNITCOMMANDQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:m_iHVACState];
    [pQ PutInt:iMode];
    [pQ SendMessage];
}
/*============================================================================*/

-(void)SetProgramState:(int)iMode

/*============================================================================*/
{
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_HVAC_COMMANDQ];
    [pQ autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iMode];
    [pQ SendMessage];
}
/*============================================================================*/

-(void)SetHeatSP:(int)iNew

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_HVAC_SETHEATQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iNew];
    [pQ SendMessageWithMyID:self];
    m_iNPacketsInFlight++;
    m_iHeatSP = iNew;
    [m_pSinkServer NotifySinks:0 Data:0];
}
/*============================================================================*/

-(void)SetCoolSP:(int)iNew

/*============================================================================*/
{
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_HVAC_SETCOOLQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:iNew];
    [pQ SendMessageWithMyID:self];
    m_iNPacketsInFlight++;
    m_iCoolSP = iNew;
    [m_pSinkServer NotifySinks:0 Data:0];
}
/*============================================================================*/

-(void)SetHoldTimeMinutes:(int)iNew

/*============================================================================*/
{
    m_iTempTime = iNew * 60 + 59;
    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_HVAC_SETTHOLDTIMEQ] autorelease];
    [pQ PutInt:m_iID];
    [pQ PutInt:m_iTempTime];
    [pQ SendMessage];
}
/*============================================================================*/

-(void)SetSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    if (pSubTabCell != NULL)
        [self InitComm];
    else if ([m_pSinkServer NSinks] == 0)
        [self ReleaseComm];

    [super SetSubTabCell:pSubTabCell];
    [self UpdateSubTabCell:pSubTabCell];
}
/*============================================================================*/

-(void)UpdateSubTabCell:(CSubTabCell*)pSubTabCell

/*============================================================================*/
{
    NSString *pIcon = NULL;

    switch (m_iHVACState) 
    {
    case HVAC_STATE_OFF:     {   pIcon = @"HVAC_OFF.png";    }   break;
    case HVAC_STATE_HEAT:    {   pIcon = @"HVAC_HEAT.png";    }   break;
    case HVAC_STATE_COOL:    {   pIcon = @"HVAC_COOL.png";    }   break;
    case HVAC_STATE_AUTO:    {   pIcon = @"HVAC_AUTO.png";    }   break;
    }

    [pSubTabCell SetEmbeddedIcon:pIcon];

    NSString *pText = [NSString stringWithFormat:@"%d° ", m_iActualTemp];
    [pSubTabCell SetAuxText:pText];
}

@end
