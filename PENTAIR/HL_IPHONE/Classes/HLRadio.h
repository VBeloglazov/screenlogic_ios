//
//  HLRadio.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/21/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlview.h"

@class CHLButton;

/*============================================================================*/

@interface CHLRadio : UIView < CHLCommandResponder >

/*============================================================================*/
{
    int                             m_iNCols;
    NSMutableArray                  *m_pButtons;
    int                             m_iSelectedIndex;

    id                              m_pCommandResponder;
    int                             m_iCommandID;
}

-(id)initWithFrame: (CGRect)rView StringList:(NSArray*)pStrings NCols:(int)iNCols; 
-(void)SetCommandID:(int)iCommandID Responder:(id)pResponder;

-(void)SetSelectedIndex:(int)iIndex;
-(int)GetSelectedIndex;
-(CHLButton*)GetSelectedButton;
-(void)NColumns:(int)iNew;
-(int)NRows;

-(int)NButtons;
-(CHLButton*)Button:(int)iIndex;

@end
