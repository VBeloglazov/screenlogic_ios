//
//  NSTunerControl.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef __cplusplus
    class CTunerControl;
#else
    @class CTunerControl;
#endif


/*============================================================================*/

@interface NSTunerControl : NSObject 

/*============================================================================*/
{
    CTunerControl                   *m_pTunerControl;
}

-(id)init;
-(NSString*)ChannelText:(int)iStation State:(int)iState Type:(int)iType StationName:(NSString*)pName;
-(int)ProcessDigit:(int)iCmdID State:(int)iState Type:(int)iType;
-(int)DTune:(int)iState Type:(int)iType;
-(int)PrepareDTune:(int)iState Type:(int)iType Station:(int)iStation;
-(int)MaxStation:(int)iState Type:(int)iType;
-(int)MinStation:(int)iState Type:(int)iType;
-(int)NextBand:(int)iState Type:(int)iType;
-(int)NextSubChannel:(int)iChannel;
-(int)PrevSubChannel:(int)iChannel;

@end
