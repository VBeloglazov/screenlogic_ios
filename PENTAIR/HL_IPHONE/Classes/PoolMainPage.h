//
//  PoolMainPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/19/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabRoundRegion.h"
@class CNSPoolConfig;
@class CRoundRegion;
@class CHLSegment;

#define SUBPAGE_POOLSPA             0
#define SUBPAGE_FEATURES            1
#define SUBPAGE_LIGHTS              2
#define SUBPAGE_DELAYS_ALERTS       3
#define SUBPAGE_INTELLICHEM         4
#define SUBPAGE_CHLORINATOR         5

/*============================================================================*/

@interface CPoolMainPage : CTabRoundRegion 

/*============================================================================*/
{
    CNSPoolConfig                   *m_pConfig;
    int                             m_iSubPages[6];
    int                             m_iNSubPages;
}

-(id)initWithFrame:(CGRect)rect Config:(CNSPoolConfig*)pConfig;

@end
