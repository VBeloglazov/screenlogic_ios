//
//  DVDIconView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/14/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMovieTitleCell;
@class CDVD;

/*============================================================================*/

    @interface CDVDIconView : UIView 

/*============================================================================*/
{
    CMovieTitleCell                 *m_pCell;
    CDVD                            *m_pDVD;
    UIView                          *m_pOverlay;
}

-(id)initWithDVD:(CDVD*)pDVD Cell:(CMovieTitleCell*)pCell;
-(void)SetDVD:(CDVD*)pDVD;
@end
