//
//  ArmingMode.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "ArmingMode.h"
#import "nsquery.h"


@implementation CArmingMode


/*============================================================================*/

-(id)initFromQ:(CNSQuery*)pQ

/*============================================================================*/
{
    if (self == [super init])
    {
        m_iMode         = [pQ GetBYTE];
        m_bVisible      = [pQ GetBYTE];
        m_bAutoEntry    = [pQ GetBYTE];
        m_iKeysEntry    = [pQ GetBYTE];
        m_pName         = [[pQ GetString] retain];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [super dealloc];
}
/*============================================================================*/

-(NSString*)Name        { return m_pName;       }
-(int)Mode              { return m_iMode;       }
-(int)NKeysEntry        { return m_iKeysEntry;  }
-(BOOL)Visible          { return m_bVisible;    }
-(BOOL)AutoEntry        { return m_bAutoEntry;  }

@end
