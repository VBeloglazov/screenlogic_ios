/*
--------------------------------------------------------------------------------

    POOLCONFIG.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#define MAX_BODIES              2  // Pool = 0, Spa = 1

class CHLButton;
class CHLMSG;
class CPoolConfig;

#if defined(POOLCONFIG) | defined(HLCONFIG)
    #define ANYCONFIG
#endif

#define IFLOWINFO_SIZE         45  // size of each pump data in CPoolConfig::m_pIFlowData[]

/*============================================================================*/
    
    class CPoolCircuit

/*============================================================================*/
{
protected:
    CPoolConfig                 *m_pConfig;

    CHLString                   m_sName;
    BYTE                        m_iNameIndex;
    int                         m_iCircuitID;
    WORD                        m_iDefaultRT;
    BYTE                        m_iFunction;
    BYTE                        m_iInterface;
    BYTE                        m_iColorPos;
    BYTE                        m_iColorSet;
    BYTE                        m_iColorStagger;
    BYTE                        m_iDeviceID;
    BYTE                        m_iFlags;
    BYTE                        m_bDelay;

    int                         m_iState;
    void                       *m_pButton;

    #ifndef CRYSTALPAD
        int                     m_iCtlrIndex;   
    #endif

public:
    #ifdef CRYSTALPAD
        CPoolCircuit(
        CPoolConfig                 *pConfig,
        CHLMSG                      *pMSG );
    #endif

    #ifndef CRYSTALPAD
        CPoolCircuit(
        CPoolConfig                 *pConfig,
        int                         iCtlrIndex,
        CHLMSG                      *pMSG );

        inline BYTE                 CtlrIndex()         { return m_iCtlrIndex;  }
    #endif

    inline int                  CircuitID()             { return m_iCircuitID;  }
    inline CHLString            Name()                  { return m_sName;       }
    inline void                 Name(CHLString sNew)    { m_sName = sNew;       }

    inline BYTE                 NameIndex()             { return m_iNameIndex;  }
    inline void                 NameIndex(BYTE iNew)    { m_iNameIndex = iNew;  }
    inline BYTE                 Function()              { return m_iFunction;   }
    inline void                 Function(BYTE iNew)     { m_iFunction = iNew;   }
    inline BYTE                 Interface()             { return m_iInterface;  }
    inline void                 Interface(BYTE iNew)    { m_iInterface = iNew;  }
    inline BYTE                 Flags()                 { return m_iFlags;      }
    inline void                 Flags(BYTE iNew)        { m_iFlags = iNew;      }

    inline int                  State()                 { return m_iState;      }
    inline void                 State(int iNew)         { m_iState = iNew;      }
    
    inline BYTE                 ColorPos()              { return m_iColorPos;       }
    inline void                 ColorPos(BYTE iNew)     { m_iColorPos = iNew;       }
    inline BYTE                 ColorSet()              { return m_iColorSet;       }
    inline void                 ColorSet(BYTE iNew)     { m_iColorSet = iNew;       }
    inline BYTE                 ColorStagger()          { return m_iColorStagger;   }
    inline void                 ColorStagger(BYTE iNew) { m_iColorStagger = iNew;   }
    inline BYTE                 Delay()                 { return m_bDelay;          }
    inline void                 Delay(BYTE byNew)       { m_bDelay = byNew;         }

    inline int                  DeviceID()              { return m_iDeviceID;   }

    inline WORD                 DefaultRunTime()        { return m_iDefaultRT;  }

    void                        DefaultRunTime(
    int                         iCtlrIndex,
    WORD                        iNew);

    inline void                 Button(CHLButton *pBtn) { m_pButton = pBtn;     }
    inline void                 *Button()               { return m_pButton;     }

    BOOL                        HeatCmdValid();
};
/*========================================================================*/

    class CLightColor

/*========================================================================*/
{
protected:
    CHLString                   m_sName;
    COLORREF                    m_rgb;

public:
    inline                      CLightColor(
    CHLString                   sName,
    COLORREF                    rgb )       { m_sName = sName; m_rgb = rgb;     }

    inline CHLString            Name()      { return m_sName;                   }
    inline COLORREF             Color()     { return m_rgb;                     }
};
#ifndef CRYSTALPAD
/*========================================================================*/

    class CCircuitType

/*========================================================================*/
{
protected:
    CHLString                   m_sName;
    int                         m_iType;

public:
    inline                      CCircuitType(
    int                         iType,
    CHLString                   sName )     { m_sName = sName; m_iType = iType; }

    inline CHLString            Name()      { return m_sName;                   }
    inline int                  Type()      { return m_iType;                   }
};
#endif
/*========================================================================*/

    class CPoolConfig

/*========================================================================*/
{
protected:
    #ifdef MULTI_SERVER
        CHLServer               *g_pHLServer;
    #endif

    WORD                        m_wMajorVersion;
    WORD                        m_wMinorVersion;

    BOOL                        m_bDataOK;

    int                         m_iControllerIndex;
    int                         m_iControllerID;
    BYTE                        m_bDegC;
    DWORD                       m_dwInterfaceTabFlags;
    DWORD                       m_byEquipFlags;

    BYTE                        m_bFreezeMode;
    BYTE                        m_bRemotes;
    BYTE                        m_bPoolDelay;   
    BYTE                        m_bSpaDelay;
    BYTE                        m_bCleanerDelay;
    BYTE                        m_byDeviceType;                 // KeepAlive byte[28], old crystalpad?
    BYTE                        m_bControllerType;              // KeepAlive byte[28]
    BYTE                        m_bHWType;                      // KeepAlive byte[27], EasyTouch type
    BYTE                        m_bControllerData;              // KeepAlive byte[9],  expansion units

    CHLObList                   m_AllCircuits;
    CHLObList                   m_Colors;

    CHLString                   m_sGenCircuitName;

    // Device OK
    BYTE                        m_byDeviceStatus;

    // Set Point Limits for bodies
    BYTE                        m_iMinSetPoint[MAX_BODIES];
    BYTE                        m_iMaxSetPoint[MAX_BODIES];

    // Current settings for bodies    
    int                         m_iAirTemp;
    int                         m_iCurrentTemp[MAX_BODIES];
    int                         m_iHeatStatus[MAX_BODIES];
    int                         m_iSetPoint[MAX_BODIES];
    int                         m_iCoolSetPoint[MAX_BODIES];
    int                         m_iHeatMode[MAX_BODIES]; 

    BYTE                        m_byPumpCir[8];

    int                         m_byIChemData[42];

    int                         m_iPH;              // this val = actual pH*100, e.g. 715 is pH 7.15
    int                         m_iORP;             //
    int                         m_iSaturation;      // valid values -99..+99, others are various errors
    int                         m_iSaltPPM;         // this val = actual SaltPPM/50
    int                         m_iPHTank;          // 0= tank does not exist, range 1= empty..7= tank full
    int                         m_iORPTank;         //  same as pHTank

    int                         m_iAlarms;
    BOOL                        m_bShowAlarms;

    #ifndef CRYSTALPAD
        // Specific to config
        CHLObList               m_ControllerNames;
        CHLObList               m_CustomNames;
        CHLObList               m_CircuitTypes;

        BYTE                    *m_pVersionInfo;
        int                     m_iNVersionInfo;

        BYTE                    *m_p2SpeedConfig;
        int                     m_iN2SpeedConfig;

        BYTE                    *m_pValveConfig;
        int                     m_iNValveConfig;

        BYTE                    *m_pRemoteConfig;
        int                     m_iNRemoteConfig;

        BYTE                    *m_pSensorConfig;
        int                     m_iNSensorConfig;

        BYTE                    *m_pDelayConfig;
        int                     m_iNDelayConfig;

        BYTE                    *m_pMacroConfig;
        int                     m_iNMacroConfig;

        BYTE                    *m_pMiscData;
        int                     m_iNMiscData;

        BYTE                    *m_pLightData;
        int                     m_iNLightData;

        BYTE                    *m_pIFlowData;
        int                     m_iNIFlowData;
    
        BYTE                    *m_pSCGData;
        int                     m_iNSCGData;

        BYTE                    *m_pSpaIFlowData;
        int                     m_iNSpaIFlowData;

        CHLString               m_sControllerString;

    #endif

public:
    #ifdef ANYCONFIG
        #ifdef MULTI_SERVER
            CPoolConfig(
            CHLServer                   *pServer,
            int                         iControllerIndex,
            WORD                        wMajorVersion,
            WORD                        wMinorVersion );
        #else
            CPoolConfig(
            int                         iControllerIndex,
            WORD                        wMajorVersion,
            WORD                        wMinorVersion );
        #endif
    #else
        #ifdef IPHONE
            CPoolConfig(
            int                         iControllerIndex,
            CHLMSG                      *pMSGA,
            WORD                        wMajorVersion,
            WORD                        wMinorVersion );
        #else
            CPoolConfig(
            int                         iControllerIndex,
            CHLMSG                      *pMSGA  );
        #endif
    #endif

    virtual ~CPoolConfig();



    CHLString                   HeatModeText(int iMode);
    CHLString                   HeatStatusText(int iStatus);
    static CHLString            EnableText(BOOL bEnabled);

    void                        ProcessMSG(
    CHLMSG                      *pMSG );

    void                        ProcessChemMSG(
    CHLMSG                      *pMSG );

    inline BOOL                 DataOK()            { return m_bDataOK;             }
    inline int                  ControllerIndex()   { return m_iControllerIndex;    }
    inline CHLObList            *AllCircuits()      { return &m_AllCircuits;        }
    inline CHLString            GenCircuitName()    { return m_sGenCircuitName;     }

    #ifdef ANYCONFIG
        inline WORD             MajorVersion()      { return m_wMajorVersion;       }
        inline WORD             MinorVersion()      { return m_wMinorVersion;       }
    #endif

    #ifdef MULTI_SERVER
        CHLServer               *Server()           { return g_pHLServer;           }
    #endif

    #ifndef CRYSTALPAD
        inline CHLObList        *ControllerNames()  { return &m_ControllerNames;    }
        inline CHLObList        *CustomNames()      { return &m_CustomNames;        }

        inline int              NCircuitTypes()         { return m_CircuitTypes.GetCount(); }
        CCircuitType            *CircuitType(int iIndex);
        CHLString               TypeNameByIndex(int Index);
        CHLString               TypeNameByType(int iType);
        int                     TypeByIndex(int Index);

        inline BYTE             ControllerType()    { return m_bControllerType;     }
        inline BYTE             ControllerData()    { return m_bControllerData;     }
        inline int              Firmware()          { return (1000*VersionInfo(0) +  VersionInfo(1));  }   // e.g.= 1160, 2040, etc.
    #endif
    inline BOOL                 IsIntelliTouch()    { return (m_bControllerType != 0x0E && m_bControllerType != 0x0D && m_bControllerType != 0x0A);  }
    inline BOOL                 IsEasyTouch()       { return (m_bControllerType == 0x0E || m_bControllerType == 0x0D);  }
    inline BOOL                 IsDualBody()        { return (m_bControllerType == 0x05);  }

    inline BYTE                 MinSetPoint(int iBodyType)  { if (!ValidIndex(iBodyType)) return 0; return m_iMinSetPoint[iBodyType];   }
    inline BYTE                 MaxSetPoint(int iBodyType)  { if (!ValidIndex(iBodyType)) return 0; return m_iMaxSetPoint[iBodyType];   }
    inline int                  AirTemp()                   { return m_iAirTemp;    }
    inline BYTE                 CurrentTemp(int iBodyType)  { if (!ValidIndex(iBodyType)) return 0; return m_iCurrentTemp[iBodyType];   }
    inline BYTE                 HeatStatus(int iBodyType)   { if (!ValidIndex(iBodyType)) return 0; return m_iHeatStatus[iBodyType];    }
    inline BYTE                 SetPoint(int iBodyType)     { if (!ValidIndex(iBodyType)) return 0; return m_iSetPoint[iBodyType];      }
    inline void                 SetPoint(int iBT, int iVal) { if (!ValidIndex(iBT)) return;  m_iSetPoint[iBT] = iVal;                   }
    inline BYTE                 CoolSetPoint(int iBodyType) { if (!ValidIndex(iBodyType)) return 0; return m_iCoolSetPoint[iBodyType];  }
    inline BYTE                 HeatMode(int iBodyType)     { if (!ValidIndex(iBodyType)) return 0; return m_iHeatMode[iBodyType];      }


    inline int                  PH()                        { return m_iPH;         }
    inline int                  ORP()                       { return m_iORP;        }
    inline int                  PHTank()                    { return m_iPHTank;     }
    inline int                  ORPTank()                   { return m_iORPTank;    }
    inline int                  Saturation()                { return m_iSaturation; }
    inline int                  SaltPPM()                   { return m_iSaltPPM;    }
    void                        SaveLastPH(int iVal)        { m_iPH = iVal;         }
    void                        SaveLastORP(int iVal)       { m_iORP = iVal;        }
    void                        SaveLastSaturation(int iVal){ m_iSaturation = iVal; }
    void                        SaveLastSaltPPM(int iVal)   { m_iSaltPPM = iVal;    }

    inline DWORD                InterfaceTabFlags()         { return m_dwInterfaceTabFlags;     }
    inline DWORD                EquipFlags()                { return m_byEquipFlags;            }
    inline DWORD                EquipPresent(DWORD byMask)  { return (m_byEquipFlags & byMask); }
    void                        SetEquipFlag(DWORD dwMask, BOOL bTF );

    int                         GetPH();
    int                         GetORP();
    int                         GetSaturation();
    int                         GetSaltPPM();
    int                         GetPHTankLevel();
    int                         GetORPTankLevel();

    CHLString                   SolarText();

    BOOL                        DeviceReady();
    BOOL                        DeviceSync();
    BOOL                        DeviceServiceMode();

    void                        LoadCircuitsByInterface(
    CHLObList                   *pList,
    int                         iIFace );

    void                        SetCircuitFirstByFunction(
    CHLObList                   *pList, 
    int                         iFunction );

    void                        SortByColorPosition(
    CHLObList                   *pList );

    CPoolCircuit                *CircuitByID(int iID);
    CPoolCircuit                *CircuitByDeviceID(int iID);
    CPoolCircuit                *CircuitByFunction(int iFunction);
    
    CPoolCircuit                *NextCircuit(CPoolCircuit *pC);
    CPoolCircuit                *PrevCircuit(CPoolCircuit *pC);

    BOOL                        CircuitInstalled(
    CPoolCircuit                *pC );

    BOOL                        Slave10X(
    int                         iSlaveIndex );        

    void                        SetCircuitOnOff(
    CPoolCircuit                *pC,
    BOOL                        bOn );

    void                        SetCircuitColor(
    CPoolCircuit                *pC,
    int                         iIndex );

    int                         NColors();

    CLightColor                 *Color(int iIndex);

    void                        SendCircuitColorInfo(
    CPoolCircuit                *pC );

    CHLString                   FormatDegText(
    int                         iTemp );

    inline BOOL                 RemotesEnabled(){ return m_bRemotes;        }
    inline BOOL                 CleanerDelay()  { return m_bCleanerDelay;   }
    inline BOOL                 PoolDelay()     { return m_bPoolDelay;      }
    inline BOOL                 SpaDelay()      { return m_bSpaDelay;       }

    BOOL                        FreezeMode();
    
    BOOL                        Alarms();
    BOOL                        ShowAlarms()    { return m_bShowAlarms;     }
    void                        ShowAlarms( BOOL bShow );

    BOOL                        CancelDelays();
    BOOL                        EnableRemotes( BOOL bEnable );

    CHLString                   PoolText();
    CHLString                   SpaText();
    
    CHLString                   PHORPText();

    CHLString                   FreezeModeStatusText();

    CHLString                   DeviceIDToString(
    BYTE                        byID );

    inline BYTE                 DeviceType()    { return m_byDeviceType;    }


    #ifndef CRYSTALPAD
        BOOL                    SetCircuitConfig(
        CPoolCircuit            *pC  );

        BOOL                    SetCustomName(
        int                     iIndex,
        CHLString               sNewName );

        inline int              N2SpeedConfig()                 { return m_iN2SpeedConfig;          }
        inline BYTE             Get2SpeedConfig(int iIndex )    { return GetByte(m_p2SpeedConfig, m_iN2SpeedConfig, iIndex);    }

        BOOL                    Set2SpeedConfig(
        int                     iIndex,
        int                     iVal );
    
        inline BYTE             NValveConfig()                  { return m_iNValveConfig;           }
        inline BYTE             GetValveConfig(int iIndex)      { return GetByte(m_pValveConfig, m_iNValveConfig, iIndex);      }

        BOOL                    SetValveConfig(
        int                     iIndex,
        int                     iVal );

        inline BYTE             GetRemoteConfig(int iIndex)     { return GetByte(m_pRemoteConfig, m_iNRemoteConfig, iIndex);    }

        BOOL                    SetRemoteConfig(
        int                     iIndex,
        int                     iVal,
        BOOL                    bSendToGateway );

        inline BYTE             GetSensorConfig(int iIndex)     { return GetByte(m_pSensorConfig, m_iNSensorConfig, iIndex);    }        
        BOOL                    SetSensorConfig(
        int                     iIndex,
        int                     iVal,
        BOOL                    bSend );

        inline BYTE             GetDelayConfig(int iIndex)      { return GetByte(m_pDelayConfig, m_iNDelayConfig, iIndex);      }

        BOOL                    SetDelayConfig(
        int                     iIndex,
        int                     iVal,
        BOOL                    bSend );

        inline BYTE             GetMacroConfig(int iIndex)      { return GetByte(m_pMacroConfig, m_iNMacroConfig, iIndex);      }

        BOOL                    SetMacroConfig(
        int                     iIndex,
        int                     iVal,
        BOOL                    bSendToGateway );

        inline BYTE             GetMiscData(int iIndex)         { return GetByte(m_pMiscData, m_iNMiscData, iIndex);            }

        BOOL                    SetMiscData(
        int                     iIndex,
        BYTE                    bVal,
        BOOL                    bSend );


        inline BYTE             GetLightData(int iIndex)        { return GetByte(m_pLightData, m_iNLightData, iIndex);            }

        BOOL                    SetLightData(
        int                     iIndex,
        BYTE                    bVal,
        BOOL                    bSend );

        inline BYTE             GetIFlowData(int iIndex)        { return GetByte(m_pIFlowData, m_iNIFlowData, iIndex);            }

        BOOL                    SetIFlowData(
        int                     iIndex,
        BYTE                    bVal,
        BOOL                    bSend );

        void                    ZeroIFlowData(int iIndex);


        inline BYTE             GetSCGData(int iIndex)          { return GetByte(m_pSCGData, m_iNSCGData, iIndex);          }

        inline BYTE             GetSpaIFlowCtrlData(int iIndex) { return GetByte(m_pSpaIFlowData, m_iNSpaIFlowData, iIndex); }
        
        BOOL                    SetSpaIFlowCtrlData(
        int                     iIndex,
        BYTE                    bVal,
        BOOL                    bSend );

        inline BYTE             VersionInfo(int iIndex)         { return GetByte(m_pVersionInfo, m_iNVersionInfo, iIndex);  }

        inline CHLString        ControllerString()              { return m_sControllerString;                               }

        BOOL                    SetCircuitColorPos(
        CPoolCircuit            *pC,
        int                     iIndex );

        BYTE                    GetByte(
        BYTE                    *pArray,
        int                     iArraySize,
        int                     iIndex );

        BOOL                    SetCalibration(
        int                     iTempType,
        int                     iDiff );
    #endif

    #ifdef CRYSTALPAD
        CHLString               PumpName(
        int                     iPumpIndex );

        BOOL                    PumpIs4Speed(
        int                     iPumpIndex );

        BOOL                    SchedulesFull(int iNSchedItems);
    #endif

protected:
    inline BOOL                 ValidIndex(int iIndex)      { return (iIndex >= 0 && iIndex < MAX_BODIES);                              }

#ifndef CRYSTALPAD
        BOOL                    LoadConfigData();
        
        BOOL                    SendEquipConfig();

        BOOL                    LoadNames(
        CHLObList               *pList,
        short                   shQ );

        void                    DeleteConfigData();

        BOOL                    UnpackConfig(
        int                     *piCount,
        BYTE                    **ppData,
        CHLMSG                  *pMSG );

        BOOL                    PackConfig(
        int                     piCount,
        BYTE                    *ppData,
        CHLMSG                  *pMSG );

        void                    UpdateCircuitNames();
#endif

        BOOL                    IsMinimumVersion(
        DWORD                   dwMajor,
        DWORD                   dwMinor );
};

