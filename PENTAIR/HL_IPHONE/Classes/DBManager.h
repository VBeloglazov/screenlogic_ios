//
//  DBManager.h
//  XPAD_VIEW
//
//  Created by dmitriy on 3/16/14.
//
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ControllerDescriptor.h"

@interface DBManager : NSObject
{
    NSString *databasePath;
}
+(DBManager*)getSharedInstance;
-(BOOL)createDB;
-(void)addController:(ControllerDescriptor*) descriptor;
-(BOOL)ifExists:(NSString*) systemName;
-(BOOL)getControllerByName:(ControllerDescriptor*)descriptor;
-(BOOL)deleteControllerByName:(ControllerDescriptor*)descriptor;
-(BOOL)updateNickname:(ControllerDescriptor*)descriptor;
-(NSMutableArray*)getAllControllers;
@end
