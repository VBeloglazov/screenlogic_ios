//
//  HVACPgmPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "hlview.h"

@class CTStat;
@class CHVACFanControlView;
@class CHVACScheduleView;

/*============================================================================*/

@interface CHVACProgramPage : UIView < CSink >

/*============================================================================*/
{
    BOOL                                m_bInit;
    CTStat                              *m_pTStat;

    CHVACScheduleView                   *m_pScheduleView;
}

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat;


@end
