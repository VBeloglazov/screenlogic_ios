//
//  PoolFeaturePage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/10/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolFeaturePage.h"
#import "NSPoolConfig.h"
#import "hlm.h"
#import "PoolLightsView.h"
#import "crect.h"

@implementation CPoolFeaturePage

#define DY_ITEM                     50

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self) 
    {
        // Initialization code
        m_pScrollView  = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:m_pScrollView];
        [m_pScrollView release];

        m_pControls = [[NSMutableArray alloc] init];

        NSMutableArray *pList = [[[NSMutableArray alloc] init] autorelease];

        [pConfig LoadCircuitsByInterface:pList Interface:POOLINT_GENERAL];

        for (int iC = 0; iC < [pList count]; iC++)
        {
            CNSPoolCircuit *pCircuit = (CNSPoolCircuit*)[pList objectAtIndex:iC];
            CLightCircuitWnd *pWnd = [[CLightCircuitWnd alloc] initWithFrame:CGRectZero Circuit:pCircuit];
            [m_pScrollView addSubview:pWnd];
            [m_pControls addObject:pWnd];
            [pWnd release];
        }

        m_pConfig = pConfig;
        [m_pConfig AddSink:self];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pConfig RemoveSink:self];
    [m_pControls release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    for (int i = 0; i < [m_pControls count]; i++)
    {
        CLightCircuitWnd *pWnd = (CLightCircuitWnd*)[m_pControls objectAtIndex:i];
        [pWnd Update];
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDYAllItems = [m_pControls count] * DY_ITEM;
    CGRect rScroll = self.bounds;

    if (iDYAllItems < rScroll.size.height)
    {
        [CRect CenterDownToY:&rScroll DY:iDYAllItems];
    }

    [m_pScrollView setFrame:rScroll];
    [m_pScrollView setContentSize:CGSizeMake(rScroll.size.width, iDYAllItems)];

    for (int i = 0; i < [m_pControls count]; i++)
    {
        CLightCircuitWnd *pWnd = (CLightCircuitWnd*)[m_pControls objectAtIndex:i];
        CGRect rFrame = CGRectMake(0, i*DY_ITEM, rScroll.size.width, DY_ITEM);
        [pWnd setFrame:rFrame];
    }
}
@end
