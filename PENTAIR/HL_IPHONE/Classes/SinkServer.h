//
//  SinkServer.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

@class CSink;

/*============================================================================*/

@interface CSinkServer : NSObject 

/*============================================================================*/
{
    CFMutableArrayRef                   m_pSinks;
}

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;
-(void)NotifySinks:(int)iID Data:(int)iData;
-(int)NSinks;

@end
