//
//  VideoStreamView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CAAnimation.h> 
#import <QuartzCore/CALayer.h> 
#import "NSSocketSink.h"
#import "HLView.h"
#import "SinkServer.h"

@class CSubTab;
@class CHLRadio;

/*============================================================================*/

@interface CVideoStreamView : UIView  < CNSSocketSink,
                                        CHLView >

/*============================================================================*/
{
    BOOL                                m_bFullScaleView;

    int                                 m_iCameraID;

    int                                 m_iOptions;
    int                                 m_iStartupRes;
    int                                 m_iResMode;
    int                                 m_iActivePreset;

    BOOL                                m_bStreaming;
    CGImageRef                          m_pLastImage;
    void                                *m_pData1;
    void                                *m_pData2;
    BOOL                                m_bData;
    
    CHLRadio                            *m_pResControl;
    CHLRadio                            *m_pPresetControl;

    BOOL                                m_b180;

    CSinkServer                         *m_pSinkServer;
}

@property (nonatomic, retain) CHLRadio *m_pResControl;
@property (nonatomic, retain) CHLRadio *m_pPresetControl;

-(id)initWithFrame:(CGRect)rFrame CameraID:(int)iID Options:(int)iOptions;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;
-(void)StartStream;
-(void)StopStream;
-(int)ResMode;
-(int)ActivePreset;
-(void)StartupRes:(int)iStartupRes;
-(BOOL)IsStreaming;
-(BOOL)Rotate180;
-(void)SetFullScaleView:(BOOL)bFullScaleView;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;

@end
