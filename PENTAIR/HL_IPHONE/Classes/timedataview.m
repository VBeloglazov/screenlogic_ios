//
//  timedataview.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "timedataview.h"
#import "timeview.h"
#import "NSMSG.h"


@implementation CTimeDataView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame TimeView:(CTimeView*)pTimeView

/*============================================================================*/
{
    if (self = [super initWithFrame:frame]) 
    {
        // Initialization code
        m_pTimeView = pTimeView;
        [self setUserInteractionEnabled:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setOpaque:NO];
        m_pDateDataStart = [pTimeView GetTimeAtX:frame.origin.x];
        m_pDateDataEnd = [pTimeView GetTimeAtX:frame.origin.x + frame.size.width];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pDateDataStart release];
    [m_pDateDataEnd release];
    [super dealloc];
}
/*============================================================================*/

-(NSDate*)StartDataDate             { return m_pDateDataStart;  }
-(NSDate*)EndDataDate               { return m_pDateDataEnd;    }

/*============================================================================*/

-(void)StopQuery

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifyPosition

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SetXOrigin

/*============================================================================*/
{
    m_iXOrigin = self.frame.origin.x;
}
/*============================================================================*/

-(void)DrawDayRects:(CGContextRef)pRef

/*============================================================================*/
{
    int iDXThis = self.frame.size.width;
    int iXLeft = m_iXOrigin;

    UIColor *pColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.15 alpha:0.1];
    [pColor set];
    
    NSDate *pDayStart = [m_pTimeView GetTimeAtX:iXLeft];
    NSDate *pDayEnd   = [[m_pTimeView RoundUp:pDayStart Type:TYPE_DAY] autorelease];
    NSTimeInterval interval = [m_pTimeView IntervalSeconds:TYPE_DAY];

    int iXL = 0;
    int iXR = 0;
    while (iXR < iDXThis)
    {
        iXL = [m_pTimeView XLogical:pDayStart] - iXLeft;
        iXR = [m_pTimeView XLogical:pDayEnd] - iXLeft;
        NSDateComponents *pComps = [[NSCalendar currentCalendar] components:0xFFFFFFFF fromDate:pDayStart];
        BOOL bDrawThisDay = (pComps.weekday) % 2 == 0;
        if (((pComps.week) % 2) == 0)
            bDrawThisDay = !bDrawThisDay;
        if (bDrawThisDay)
        {
            CGContextFillRect(pRef, CGRectMake(iXL, 0, iXR-iXL, self.frame.size.height));
        }

        [pDayStart release];
        pDayStart = [[NSDate alloc] initWithTimeInterval:0 sinceDate:pDayEnd];
        pDayEnd = [pDayEnd dateByAddingTimeInterval:interval];
    }

    [pDayStart release];
}
/*============================================================================*/

-(void)UpdateTimeBar

/*============================================================================*/
{
}
/*============================================================================*/

-(BOOL)DataOK

/*============================================================================*/
{
    return m_bDataOK;
}
/*============================================================================*/

-(void)SetTimeView:(CTimeView*)pTimeView

/*============================================================================*/
{
    m_pTimeView = pTimeView;
}
/*============================================================================*/

-(int)RoundTempUp:(int)iIn

/*============================================================================*/
{
    int iRet = ((iIn / 5 ) + 1) * 5;
    return iRet;
}
/*============================================================================*/

-(int)RoundTempDown:(int)iIn

/*============================================================================*/
{
    int iRet = iIn;
    if (iIn <= 0)
        iRet = ((-iIn / 5 ) + 1) * -5;
    else
        iRet = (iIn / 5 ) * 5;
    return iRet;
}

@end
@implementation CDataPoint
/*============================================================================*/

-(id)initWithDate:(NSDate*)pDate Val:(int)iVal

/*============================================================================*/
{
    m_pDate = pDate;
    [m_pDate retain];
    m_iVal = iVal;
    return self;
}
/*============================================================================*/

-(NSDate*)Date      { return m_pDate;   }
-(int)Val           { return m_iVal;    }

/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pDate release];
    [super dealloc];
}
/*============================================================================*/
@end

@implementation CTempSeries
/*============================================================================*/

-(id)initWithColor:(UIColor*)pColor

/*============================================================================*/
{
    m_pRGB = pColor;
    [m_pRGB retain];
    m_pPoints = [[NSMutableArray alloc] init];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pRGB release];
    [m_pPoints release];
    [super dealloc];
}
/*============================================================================*/

-(void)ReadFrom:(CNSMSG*)pMSG

/*============================================================================*/
{
    NSCalendar *pCal = [NSCalendar currentCalendar];
    NSDateComponents *pComps = [[[NSDateComponents alloc] init] autorelease];
    CDateHelper *pHelper = [[[CDateHelper alloc] init] autorelease];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        NSDate *pDate = [pMSG GetTimeWith:pComps Calendar:pCal DateHelper:pHelper];
        int iVal = [pMSG GetInt];
        CDataPoint *pPt = [[CDataPoint alloc] initWithDate:pDate Val:iVal];
        [m_pPoints addObject:pPt];
        [pPt release];
    }
}

/*============================================================================*/

-(NSMutableArray*)PointList

/*============================================================================*/
{
    return m_pPoints;
}
/*============================================================================*/

-(UIColor*)Color

/*============================================================================*/
{
    return m_pRGB;
}
/*============================================================================*/

-(int)MinTempFrom:(NSDate*)tmMin To:(NSDate*)tmMax

/*============================================================================*/
{
    int iTemp = HVAC_MIN_TEMP;
    int iEarlyTemp = HVAC_MIN_TEMP;
    BOOL bDataOK = FALSE;
    for (int i = 0; i < [m_pPoints count]; i++)
    {
        CDataPoint *pPt = (CDataPoint*)[m_pPoints objectAtIndex:i];
        BOOL bOK = TRUE;
        int iVal = [pPt Val];
        if ([[pPt Date] laterDate:tmMin] == tmMin)
        {
            if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP)
                iEarlyTemp = iVal;
            bOK = FALSE;
        }
        if ([[pPt Date] earlierDate:tmMax] == tmMax)
        {
            if (!bDataOK)
            {
                if (iEarlyTemp != HVAC_MIN_TEMP)
                {
                    if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP)
                    {
                        if (iEarlyTemp != HVAC_MIN_TEMP)
                            return MIN(iEarlyTemp, iVal);
                        return iVal;
                    }
                    else
                    {
                        if (iEarlyTemp != HVAC_MIN_TEMP)
                            return iEarlyTemp;
                    }
                }
                else if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP)
                    return iVal;
            }

            bOK = FALSE;
        }

        if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP && bOK)
        {
            bDataOK = TRUE;
            if (iTemp == HVAC_MIN_TEMP)
                iTemp = iVal;
            else
                iTemp = MIN(iTemp, iVal);
        }
    }

    return iTemp;
}
/*============================================================================*/

-(int)MaxTempFrom:(NSDate*)tmMin To:(NSDate*)tmMax

/*============================================================================*/
{
    int iTemp = HVAC_MAX_TEMP;
    int iEarlyTemp = HVAC_MAX_TEMP;
    BOOL bDataOK = FALSE;

    for (int i = 0; i < [m_pPoints count]; i++)
    {
        CDataPoint *pPt = (CDataPoint*)[m_pPoints objectAtIndex:i];
        int iVal = [pPt Val];
        BOOL bOK = TRUE;
        if ([[pPt Date] laterDate:tmMin] == tmMin)
        {
            if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP)
                iEarlyTemp = iVal;
            bOK = FALSE;
        }
        if ([[pPt Date] earlierDate:tmMax] == tmMax)
        {
            if (!bDataOK)
            {
                if (iEarlyTemp != HVAC_MAX_TEMP)
                {
                    if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP)
                    {
                        if (iEarlyTemp != HVAC_MAX_TEMP)
                            return MAX(iEarlyTemp, iVal);
                        return iVal;
                    }
                    else
                    {
                        if (iEarlyTemp != HVAC_MAX_TEMP)
                            return iEarlyTemp;
                    }
                }
                else if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP)
                    return iVal;
            }
            bOK = FALSE;
        }

        if (iVal > HVAC_MIN_TEMP && iVal < HVAC_MAX_TEMP && bOK)
        {
            bDataOK = TRUE;
            if (iTemp == HVAC_MAX_TEMP)
                iTemp = iVal;
            else
                iTemp = MAX(iTemp, iVal);
        }
    }

    return iTemp;

}
@end
