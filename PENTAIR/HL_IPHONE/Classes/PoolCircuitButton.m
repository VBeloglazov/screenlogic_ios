//
//  PoolCircuitButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PoolCircuitButton.h"
#import "NSPoolConfig.h"
#import "NSPoolCircuit.h"
#import "crect.h"

#define CMD_CIRCUIT                 1000
#define DX_SWITCH                   80

@implementation CPoolCircuitButton
/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame Circuit:(CNSPoolCircuit*)pCircuit Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:rFrame Text:[pCircuit Name]];
    if (self)
    {
        // Initialization code
        m_pCircuit  = pCircuit;
        [m_pCircuit retain];
        m_pConfig   = pConfig;
        [m_pConfig AddSink:self];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pCircuit release];
    [m_pConfig RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    [self SetChecked:([m_pCircuit State] != 0)];
}
/*============================================================================*/

-(void)ButtonPress:(id)sender

/*============================================================================*/
{
    BOOL bNewChecked = !m_bChecked;

    [m_pConfig SetCircuitOnOff:m_pCircuit On:bNewChecked];
    [super ButtonPress:sender];
}
@end
