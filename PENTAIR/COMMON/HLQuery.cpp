/*
--------------------------------------------------------------------------------

    HLQUERY.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlmsg.h"
#include "hlserver.h"
#include "hltime.h"
#include "hlmsgs.h"
#include "main.h"

#include "hlquery.h"

#ifdef HLCONFIG
    #include "..\hlconfig\statwnd.h"
#endif

    #define MAX_CACHE           100
    #define MAX_CACHE_AGE       3600000

    //
    // Defines: we use this just to make it clear that the sender ID is
    // not valid yet.
    //
    #define NO_SENDER           0

    CHLObList                   *CHLQuery::m_pCache = NULL;

/*====================================================================*/

    CQACache::CQACache(
    CHLMSG                      *pQ,
    CHLMSG                      *pA )

/*====================================================================*/
{
    m_pQ = pQ;
    m_pA = pA;
    m_hltAge.Restart();
}
/*====================================================================*/

    CQACache::~CQACache()

/*====================================================================*/
{
    delete m_pQ;
    delete m_pA;
}

#ifdef MULTI_SERVER
/*====================================================================*/

    CHLQuery::CHLQuery(
    CHLServer                   *pServer )

    : CSocketSink(pServer)

/*====================================================================*/
#else
/*====================================================================*/

    CHLQuery::CHLQuery(
    BOOL                        bCache )

/*====================================================================*/
#endif
{
    #ifdef MULTI_SERVER
        g_pHLServer = pServer;
        m_bCache    = FALSE;
    #else
        m_bCache    = FALSE;
        #if defined(CRYSTALPAD) | defined(HLOSD)
            m_bCache = bCache;
        #endif
    #endif

    m_bOwnResponse  = FALSE;
    m_bWaitResponse = FALSE;
    m_pResponse     = NULL;
    m_pQuestion     = NULL;
    m_dwTimeout     = HL_TIMEOUT_QUESTION;
    m_bPostOK       = FALSE;
    m_shResponseID  = 0;
}
/*====================================================================*/

    CHLQuery::~CHLQuery()

/*====================================================================*/
{
    if (m_bOwnResponse)
        delete m_pResponse;
    if (m_pQuestion != NULL)
        delete m_pQuestion;

    if (g_pHLServer != NULL)
        g_pHLServer->RemoveSocketSink(this);
}
/*========================================================================*/

    void                        CHLQuery::SinkMessage(
    CHLMSG                      *pMSG )

/*========================================================================*/
{
    if (pMSG->SenderID() == HLM_ALL_SENDERS_ID)
        return;

    if (m_bOwnResponse)
        delete m_pResponse;
    m_pResponse = NULL;
    m_pResponse = pMSG->CreateCopy();
    m_bOwnResponse  = TRUE;
    m_bWaitResponse = FALSE;

    // Got a response
    g_pHLServer->RemoveSocketSink(this);
}
/*========================================================================*/

    void                        CHLQuery::SetTimeout(
    DWORD                       dwNew )

/*========================================================================*/
{
    m_dwTimeout = dwNew;
}
/*========================================================================*/

    void                        CHLQuery::InitCache()

/*========================================================================*/
{
    LOGASSERT(m_pCache == NULL);
    m_pCache = new CHLObList();
}
/*========================================================================*/

    void                        CHLQuery::UnInitCache()

/*========================================================================*/
{
    if (m_pCache == NULL)
        return;
    RemoveAllCache();
    delete m_pCache;
    m_pCache = NULL;    
}
/*========================================================================*/

    void                        CHLQuery::RemoveAllCache()

/*========================================================================*/
{
    if (m_pCache == NULL)
        return;
    while (!m_pCache->IsEmpty())
    {
        CQACache *pCache = (CQACache*) m_pCache->RemoveHead();
        delete pCache;
    }
}
/*========================================================================*/

    CQACache                    *CHLQuery::FindQA(
    CHLMSG                      *pQ)

/*========================================================================*/
{
    if (m_pCache == NULL)
        return NULL;

    CHLPos *pPos = m_pCache->GetHeadPosition();
    while (pPos != NULL)
    {
        CQACache *pQA = (CQACache*) pPos->Object();
        if (pQA->Question()->IsSameMSG(pQ))
        {
            if (pQA->Age() > MAX_CACHE_AGE)
            {
                delete pQA;
                m_pCache->RemoveAt(pPos);
                return NULL;
            }

            return pQA;
        }

        pPos = pPos->Next();
    }

    return NULL;
}
/*========================================================================*/

    void                        CHLQuery::AddQACache(
    CHLMSG                      *pQ,
    CHLMSG                      *pA )

/*========================================================================*/
{
    if (m_pCache == NULL)
        return;

    while (m_pCache->GetCount() > 100)
    {
        CQACache *pCache = (CQACache*)m_pCache->RemoveHead();   
        delete pCache;
    }

    m_pCache->AddTail(new CQACache(pQ, pA));
}
/*========================================================================*/

    BOOL                        CHLQuery::PostQuestion(
    CHLMSG                      *pQuestion )

/*========================================================================*/
{
    if (!g_pHLServer->IsConnected())
        return FALSE;

    if (m_bCache)
    {
        CQACache *pQA = FindQA(pQuestion);
        if (pQA != NULL)
        {
            m_bPostOK = TRUE;
            m_pResponse = pQA->Response();
            m_bOwnResponse = FALSE;
            return TRUE;
        }

        LOGASSERT(m_pQuestion == NULL);
        m_pQuestion = pQuestion->CreateCopy();
    }

    // Add us as a sink.
    g_pHLServer->AddSocketSink(this);
    m_bWaitResponse = TRUE;

    // Make sure we have the right sender ID.
    pQuestion->SenderID(SenderID());
    if (!g_pHLServer->SendMSG(pQuestion))
    {
        g_pHLServer->RemoveSocketSink(this);
        return FALSE;
    }

    m_shResponseID = pQuestion->MessageID()+1;
    m_bPostOK = TRUE;
    return TRUE;
}
#ifdef HLUTIL
/*========================================================================*/

    BOOL                        CHLQuery::WaitResponse(
    CHLMSG                      **pAnswer,
    CStatusSink                 *pSink  )

/*========================================================================*/
#else
/*========================================================================*/

    BOOL                        CHLQuery::WaitResponse(
    CHLMSG                      **pAnswer )

/*========================================================================*/
#endif
{
    if (!m_bPostOK)
        return FALSE;

    #if defined(CRYSTALPAD) & !defined(MAC_OS)
        START_TIMER(GDI_SOCKET);
    #endif

    #ifdef HLCONFIG
        if (g_pMainWindow != NULL)
            g_pMainWindow->DrawStatus(STATUS_WAITING);
    #endif

    // Wait for response or timeout
    CHLTimer QTimer;

    if (m_bCache && m_pResponse != NULL)
    {
        if (pAnswer != NULL)
        {
            m_pResponse->ResetRead();
            *pAnswer = m_pResponse;
            if (m_bCache && m_pQuestion != NULL)
            {
                AddQACache(m_pQuestion, m_pResponse);
            }
        }

        m_pQuestion = NULL;
        m_pResponse = NULL;
        #if defined(CRYSTALPAD) & !defined(MAC_OS)
            END_TIMER(GDI_SOCKET);
        #endif
        return TRUE;
    }

    if (m_pResponse != NULL)
    {
        m_pResponse->ResetRead();
        if (pAnswer != NULL)
            *pAnswer = m_pResponse;
        return TRUE;
    }

    m_bWaitResponse = TRUE;

    while (QTimer.HLTickCount() < m_dwTimeout)
    {
        #ifdef HLUTIL
            if (pSink != NULL)
            {
                if (!pSink->DoWaitProc())
                {
                    g_pHLServer->RemoveSocketSink(this);
                    #if defined(CRYSTALPAD) & !defined(MAC_OS)
                        END_TIMER(GDI_SOCKET);
                    #endif
                    return FALSE;
                }
            }
        #endif
    
        if (m_bWaitResponse == FALSE)
        {
            // Cleanup.
            g_pHLServer->RemoveSocketSink(this);

            #ifdef HLCONFIG
                if (g_pMainWindow != NULL)
                    g_pMainWindow->DrawStatus(STATUS_IDLE);
            #endif

            #ifdef CRYSTALPAD
            //CHLString sTrace;
            //sTrace.Format(_T("HLQuery Wait %d msec"), QTimer.HLTickCount());
            //TraceWrap(sTrace);
            #endif

            // See if it's the right one.
            if (m_pResponse->MessageID() != m_shResponseID)
            {
                m_bWaitResponse = FALSE;
                #if defined(CRYSTALPAD) & !defined(MAC_OS)
                    END_TIMER(GDI_SOCKET);
                #endif
                return FALSE;
            }

            if (pAnswer != NULL)
            {
                *pAnswer = m_pResponse;
                if (m_bCache && m_pQuestion != NULL)
                {
                    AddQACache(m_pQuestion, m_pResponse);
                    m_pQuestion = NULL;
                    m_pResponse = NULL;
                }
            }


            #ifdef HLCONFIG
                if (g_pMainWindow != NULL)
                    g_pMainWindow->DrawStatus(STATUS_IDLE);
            #endif

            #if defined(CRYSTALPAD) & !defined(MAC_OS)
                END_TIMER(GDI_SLEEP);
            #endif

            m_bWaitResponse = FALSE;
            #if defined(CRYSTALPAD) & !defined(MAC_OS)
                END_TIMER(GDI_SOCKET);
            #endif
            return TRUE;
        }

        #ifdef HLCONFIG
            // Let All Messages go
            int iReceive = g_pHLServer->PumpSocketMessages(10);

            // Allow screen updates
            if (g_pMainWindow != NULL)
                g_pMainWindow->ValidateScreen(NULL);
        #else
            // Queue up messages that arn't for us
            int iReceive = g_pHLServer->PumpSocketMessages(NULL, SenderID(), 50);
        #endif

        if (iReceive == RECEIVE_ERROR)
        {
            #ifdef HLCONFIG
                if (g_pMainWindow != NULL)
                    g_pMainWindow->DrawStatus(STATUS_IDLE);
            #endif
            g_pHLServer->RemoveSocketSink(this);
            m_bWaitResponse = FALSE;
            #if defined(CRYSTALPAD) & !defined(MAC_OS)
                END_TIMER(GDI_SOCKET);
            #endif
            return FALSE;
        }

        #ifdef CRYSTALPAD
//            if (g_pMainWindow != NULL)
//                g_pMainWindow->OnIdle(TRUE);
        #endif
    }

    #ifdef HLCONFIG
        if (g_pMainWindow != NULL)
            g_pMainWindow->DrawStatus(STATUS_IDLE);
    #endif

    // No response
    m_bWaitResponse = FALSE;
    g_pHLServer->RemoveSocketSink(this);
    #if defined(CRYSTALPAD) & !defined(MAC_OS)
        END_TIMER(GDI_SOCKET);
    #endif

    return FALSE;
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestion(
    CHLMSG                      **pAnswer,
    CHLMSG                      *pQuestion )

/*========================================================================*/
{
    if (m_bOwnResponse)
        delete m_pResponse;
    m_pResponse = NULL;

    if (!PostQuestion(pQuestion))
        return FALSE;
    if (!WaitResponse(pAnswer))
        return FALSE;
    return TRUE;
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestion(
    CHLMSG                      **pAnswer,
    short                       ushQuestion,
    int                         iData1 )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8);
    msgQ.PutInt(iData1);
    return AskQuestion(pAnswer, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestion(
    CHLMSG                      **pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2 )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8);
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    return AskQuestion(pAnswer, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestion(
    CHLMSG                      **pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    CHLString                   sData )

/*========================================================================*/
{
    int iSize = 10 + sData.GetLength();
    CHLMSG msgQ(NO_SENDER, ushQuestion, iSize);
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    msgQ.PutString(sData);
    return AskQuestion(pAnswer, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionVoid(
    short                       ushQuestion )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion);
    return AskQuestion(NULL, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionVoid(
    short                       ushQuestion,
    CHLString                   sText )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 2 + sText.GetLength());
    msgQ.PutString(sText);
    return AskQuestion(NULL, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionVoid(
    short                       ushQuestion,
    int                         iVal )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 4);
    msgQ.PutInt(iVal);
    return AskQuestion(NULL, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionVoid(
    short                       ushQuestion,
    int                         iVal1,
    int                         iVal2 )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8);
    msgQ.PutInt(iVal1);
    msgQ.PutInt(iVal2);
    return AskQuestion(NULL, &msgQ);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion), *pMSGA;
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 4), *pMSGA;
    msgQ.PutInt(iData);
    if (AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2 )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    int                         iData3 )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    msgQ.PutInt(iData3);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    int                         iData3,
    int                         iData4 )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    msgQ.PutInt(iData3);
    msgQ.PutInt(iData4);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    CHLString                   sText )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 12 + sText.GetLength()), *pMSGA;
    msgQ.PutString(sText);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    CHLString                   sText )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 12 + sText.GetLength()), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutString(sText);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionInt(
    int                         *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    CHLString                   sText )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 12 + sText.GetLength()), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    msgQ.PutString(sText);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetInt(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 0), *pMSGA;
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetString(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion,
    int                         iData  )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 4), *pMSGA;
    msgQ.PutInt(iData);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetString(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2  )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 8), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetString(pAnswer);
}
/*========================================================================*/

    BOOL                        CHLQuery::AskQuestionString(
    CHLString                   *pAnswer,
    short                       ushQuestion,
    int                         iData1,
    int                         iData2,
    int                         iData3  )

/*========================================================================*/
{
    CHLMSG msgQ(NO_SENDER, ushQuestion, 12), *pMSGA;
    msgQ.PutInt(iData1);
    msgQ.PutInt(iData2);
    msgQ.PutInt(iData3);
    if (!AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    LOGASSERT(pAnswer != NULL);
    return pMSGA->GetString(pAnswer);
}
