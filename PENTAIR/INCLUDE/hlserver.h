/*
--------------------------------------------------------------------------------

    HLSERVER.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLSERVER_H
#define HLSERVER_H

#include "hlmsgs.h"
#include "hlstructs.h"
#include "socksink.h"
#include "hltime.h"
#include "hloblist.h"
#include "hlstd.h"

#ifdef GATEWAY
    #include "..\lcore\ServersoftCommunicatorClient.h"
#endif

#define RECEIVE_ERROR           0
#define RECEIVE_IDLE            1
#define RECEIVE_BUSY            2
#define RECEIVE_MSGREADY        3

class CHLSocket;
class CHLMSG;

    #define LINUX_WHM_MESSAGE_READY         11000

    extern unsigned short       g_ushBasePort;

//
// Little helper class to give feedback during the lookup/login process
//
/*============================================================================*/

    class CStatusSink

/*============================================================================*/
{
#ifdef MAC_OS
    void                        *m_pNSSink;
#endif


public:
    #ifdef MAC_OS
        CStatusSink(
        void                    *pSink );
    #endif

    virtual void                SetStatus(
    CHLString                   sStatus );

    virtual BOOL                DoWaitProc();
};
#ifdef MAC_OS
    extern "C"
    {
    // C++ callable wrapper
    extern void                 NSStatusSinkSetStatus(
    void                        *pSink,
    const char                  *pMSG );
    
    // C++ callable wrapper
    extern void                 HLConnectionChanged(
    void                        *pNSSink,
    int                         iSenderID,
    BOOL                        bNowConnected);

    extern void                 HLSinkMessage(
    void                        *pNSSink,
    int                         iSenderID,
    CHLMSG                      *pMSG );
    }
#endif
/*============================================================================*/

    class CHLServer : public CSocketSink

/*============================================================================*/
{
protected:
    CHLSocket                   *m_pTheSocket;
    CHLObList                   m_Sinks;
    CHLObList                   m_AsyncMSGs;

    CHLObList                   m_PendingMSG;

    short                       m_shNextSenderID;

    CHLTimer                    m_hltLastConnAttempt;

    BOOL                        m_bConnect;
    SOCKADDR_IN                 m_Address;

    char                        m_pGatewayName[32];

    CHLString                   m_sPassword;
    int                         m_iConnectionType;
    CHLString                   m_sLoginName;

    BOOL                        m_bEnableKeepAlive;
    BOOL                        m_bRemoteMode;
    CHLString                   m_sRemoteUser;
    CHLString                   m_sRemotePass;
    CHLString                   m_sMachineName;             // for tablets, usually the MAC address
    CHLString                   m_sUUID;                    // unique indentifier for tablet manufacturer,model
    CHLString                   m_sOSVer;                   // OS details

    BOOL                        m_bPasswordFail;
    BOOL                        m_bLoggingIn;

    int                         m_iEntry;

    BOOL                        m_bNotifyConnection;
    
    #ifdef IPHONE
        BOOL                    m_bSuspendIO;
    #endif
    

#ifndef IPHONE
#endif

public:
    CHLServer();

    virtual                     ~CHLServer();

    void                        SetGatewayName(char *pName);
    char                        *GetGatewayName();

    CHLSocket                   *Socket();

    inline SOCKADDR_IN          *Address()                  { return &m_Address;            }

    inline BOOL                 LoggingIn()                 { return m_bLoggingIn;          }
    inline void                 LoggingIn(BOOL bNew)        { m_bLoggingIn = bNew;          }

    BOOL                        CreateConnection();

    BOOL                        CreateConnection(
    DWORD                       dwIPAddress,
    WORD                        wPort = 80,
    int                         iConType = -1 );

    inline void                 EnableKeepAlive(BOOL bNew)  { m_bEnableKeepAlive = bNew;    }

    inline BOOL                 RemoteMode()                { return m_bRemoteMode;         }
    inline void                 RemoteMode(BOOL bNew)       { m_bRemoteMode = bNew;         }
    inline CHLString            RemoteUser()                    { return m_sRemoteUser;     }
    inline void                 RemoteUser(CHLString sNew)      { m_sRemoteUser = sNew;     }
    inline CHLString            RemotePassword()                { return m_sRemotePass;     }
    inline void                 RemotePassword(CHLString sNew)  { m_sRemotePass= sNew;      }
    inline CHLString            MachineName()                   { return m_sMachineName;    }
    inline void                 MachineName(CHLString sNew)     { m_sMachineName= sNew;     }
    inline CHLString            UUID()                          { return m_sUUID;           }
    inline void                 UUID(CHLString sNew)            { m_sUUID= sNew;            }
    inline CHLString            OSVersion()                     { return m_sOSVer;          }
    inline void                 OSVersion(CHLString sNew)       { m_sOSVer = sNew;          }
    inline BOOL                 PasswordFail()                  { return m_bPasswordFail;   }
    
    #ifdef IPHONE
        inline BOOL             SuspendIO()                     { return m_bSuspendIO;      }
        inline void             SuspendIO(BOOL bNew)            { m_bSuspendIO = bNew;      }
    #endif

    //
    // Functions used to login over the internet
    //

    void                        StopConnection();

    BOOL                        CreateCrystalpadConnection(
    CHLString                   sUserName,
    CHLString                   sPassword,
    CStatusSink                 *pStatus,
    int                         iConType = -1 );

#ifdef HLSURVEY
    BOOL                        CreateCrystalpadConnection(
    BYTE                        byAd1,
    BYTE                        byAd2,
    BYTE                        byAd3,
    BYTE                        byAd4,
    WORD                        wPort,
    CHLString                   sPassword,
    CStatusSink                 *pStatus,
    int                         iConType  );
#endif

    BOOL                        CreateRelayServerConnection(
    CHLString                   sUserName,
    CHLString                   sPassword,
    CStatusSink                 *pStatus );

    BOOL                        CreateServiceConnection(
    CHLString                   sUserName,
    BOOL                        bInternal,
    CStatusSink                 *pStatus );

    BOOL                        CreateServiceConnection(
    BYTE                        byAd1,
    BYTE                        byAd2,
    BYTE                        byAd3,
    BYTE                        byAd4,
    WORD                        wPort,
    CStatusSink                 *pStatus,
    SOCKET                      hRelayServer );

    BOOL                        CreateServiceConnection(
    SOCKADDR_IN                 *pAddress,
    CStatusSink                 *pStatus );

    BOOL                        CreateConnection(
    CHLString                   sChallenge,
    CHLString                   sPassword,
    int                         iConnectionType,
    CHLString                   sLoginName,
    CStatusSink                 *pStatus,
    SOCKET                      hRelayServer);

    BOOL                        DoLogin(
    CHLString                   sChallenge,
    CHLString                   sPassword,
    int                         iConnectionType,
    CHLString                   sLoginName );

    BOOL                        CheckConnection();

    BOOL                        IsConnected();

    BOOL                        SendMSG(
    CHLMSG                      *pMSG );

    short                       GenerateSenderID();

    BOOL                        WaitingResponse();

    CSocketSink                 *FindSender(
    short                       shSenderID );

    #ifdef MAC_OS
        CSocketSink             *FindSink(
        void                    *pNSSink );
    #endif

    BOOL                        AddSocketSink(
    CSocketSink                 *pSink  );
    
    void                        RemoveAllSocketSinks();

    void                        RemoveSocketSink(
    CSocketSink                 *pSink );

    BOOL                        ReadyToSend();

    int                         PumpSocketMessages(
    DWORD                       dwMaxWaitForData = 10);
    
    void                        ProcessMessage(
    CHLMSG                      *pMSG );

    int                         PumpSocketMessages(
    CStatusSink                 *pSink,
    short                       shSenderID          = HLM_ALL_SENDERS_ID,
    DWORD                       dwMaxWaitForData    = 10 );

    void                        SendSinkMessage(
    CHLMSG                      *pMSG  );

    void                        SetConnectionChanged();
    
protected:
    //
    // Functions used to login over the internet
    //
    BOOL                        FindGateway(
    CStatusSink                 *pStatus,
    CHLString                   sUserName,
    BOOL                        bInternal,
    SOCKADDR_IN                 *pResult,
    CHLString                   *pChallenge );

#ifdef GATEWAY
    BOOL                        DoLookupServerSoft(
    CHLString                   sUserName,
    CHLString                   *pIP,
    CHLString                   *pPort,
    CStatusSink                 *pStatus );

#endif

    SOCKET                      FindRelayServerGateway(
    CStatusSink                 *pStatus,
    CHLString                   sUserName,
    CHLString                   *pChallenge );

    BOOL                        DoLookupPost(
    SOCKET                      hSocket,
    CHLString                   sUserName,
    BOOL                        bInternal,
    CHLString                   *pIP,
    CHLString                   *pPort,
    CHLString                   *pChallenge,
    CStatusSink                 *pStatus );

    CHLString                   GetParam(
    TCHAR                       cDelimiter,
    CHLString                   *sLine );

    void                        SendConnectionChanged(
    BOOL                        bNowConnected );
};
#ifdef GATEWAY
/*============================================================================*/

    class CGatewayLookupServerSoft : public CServersoftCommunicatorClient

/*============================================================================*/
{
protected:
    CHLString                   sUserName;
    CHLString                   *m_pIP;
    CHLString                   *m_pPort;
    CStatusSink                 *m_pSink;

    int                         m_iStatus;
   
public:
    CGatewayLookupServerSoft(
    CHLString                   sUserName,
    CHLString                   *pIP,
    CHLString                   *pPort,
    CStatusSink                 *pSink );
    
    virtual void                ProcessServersoftResponse(
    CHLMSG*                     pMsgA );

    // Timeout/Error Handler. Must be overridden by derived class.
    virtual void                ProcessServersoftNoResponse(
    WORD                        shSenderID );

    BOOL                        WaitResponse();
};

#endif

#endif

