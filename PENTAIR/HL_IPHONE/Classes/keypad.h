//
//  keypad.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "HLView.h"

@class CNSMSG;


/*============================================================================*/

@interface CButtonDef : NSObject

/*============================================================================*/
{
    NSString                        *m_pName;
    int                             m_iType;
    int                             m_iX1;
    int                             m_iY1;
    int                             m_iDX;
    int                             m_iDY;
    UIColor                         *m_pRGBFace;
    UIColor                         *m_pRGBText;
}
-(id)initWithName:(NSString*)pName Type:(int)iType X1:(int)iX1 Y1:(int)iY1 DX:(int)iDX DY:(int)iDY Face:(UIColor*)pFace Text:(UIColor*)pText;

-(NSString*)Text;
-(UIColor*)FaceColor;
-(UIColor*)TextColor;
-(int)Type;
-(int)X1;
-(int)Y1;
-(int)DX;
-(int)DY;


@end

/*============================================================================*/

@interface CLightingKeypad : UIView  < CNSSocketSink, CHLCommandResponder >

/*============================================================================*/
{
    int                                 m_iID;
    NSMutableArray                      *m_pButtons;
}

-(id)initWithFrame:(CGRect)rFrame ID:(int)iID;

-(void)LoadKeypad:(CNSMSG*)pMSG;
-(void)Execute:(int)iCmd Index:(int)iIndex;

@end
