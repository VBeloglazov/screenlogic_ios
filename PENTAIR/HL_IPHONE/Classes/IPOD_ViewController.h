//
//  IPOD_ViewController.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "Sink.h"
#import "hlview.h"

//
// iPOD only
//


@class CConnectPage;
@class CMainTab;
@class CHLButton;
@class CSystemModeList;
@class CHLToolBar;
@class IPOD_ViewController;
@class CToolBarButton;

/*============================================================================*/

@interface CIPODTableView : UITableView

/*============================================================================*/
{
    IPOD_ViewController             *m_pController;
    int                             m_iDXLast;
    int                             m_iDYLast;
}

-(id)initWithFrame:(CGRect)rFrame Controller:(IPOD_ViewController*)pController;

@end

/*============================================================================*/

@interface CIPODMainView : UIView

/*============================================================================*/
{
    UIView                          *m_pToolBar;
    UIView                          *m_pContent;
}

-(void)SetToolBar:(UIView*)pView;
-(void)SetContent:(UIView*)pView;

@end

/*============================================================================*/

@interface IPOD_ViewController : UIViewController <         UINavigationBarDelegate,
                                                            UINavigationControllerDelegate,
                                                            UITableViewDelegate, 
                                                            UITableViewDataSource,
                                                            UIActionSheetDelegate,
                                                            UIScrollViewDelegate,
                                                            CNSSocketSink,
                                                            CSink  >

/*============================================================================*/
{
    BOOL                        m_bInit;
	CConnectPage                *m_pConnectPage;

    NSTimer                     *m_pTimer;
	NSMutableArray              *m_pMainTabs;
	IBOutlet UITableView        *m_pTableView;
    int                         m_iMainTabHeight;
    
    UILabel                     *m_pInfoText;
    UIAlertView                 *m_pLockView;
    

    CMainTab                    *m_pExpandedTab;
    int                         m_iExpandedTabIndex;

    CHLToolBar                  *m_pToolBar;
    
    UILabel                     *m_pTitleView;
    UIImageView                 *m_pBackground;

    UIView                      *m_pHeaderAccessory;
    
    UIImageView                 *fishImage;
    UIView                      *rightBorder;
}

-(void)CheckConnectTimer;

  @property (nonatomic, retain) NSMutableArray *m_pMainTabs;
  @property (nonatomic, retain) UITableView *m_pTableView;

-(void)ShowConnectPage;
-(void)ShowConnectPage2;
-(void)HideConnectPage;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

-(CMainTab*)SelectedMainTab;

-(void)CollapseAll;
-(void)SetExpandedMode:(BOOL)bNew;
-(void)SetExpanded:(CMainTab*)pMainTab Expanded:(BOOL)bExpanded MainTabIndex:(int)iMainIndex;
-(int)FindMainTabIndex:(CMainTab*)pMainTab;
-(void)TableViewResize;
-(void)SetBackgroundImage:(int)iImage;

-(void)ShowSystemPage:(CMainTab*)pMainTab SubTabIndex:(int)iSubTabIndex;
-(BOOL)IsOldOS;

+(UIViewController*)MainViewController;
-(void)longPress:(UILongPressGestureRecognizer*)gesture;

@end

