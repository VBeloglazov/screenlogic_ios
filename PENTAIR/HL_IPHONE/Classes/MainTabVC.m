//
//  MainTabPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "MainTabVC.h"
#import "MainTab.h"
#import "hlm.h"
#import "SubTab.h"
#import "SubTabHeader.h"
#import "SubSystemPage.h"
#import "HLScroll.h"
#import "HLPageControl.h"
#import "HLComm.h"
#import "mainview.h"
#import "HLToolBar.h"
#import "crect.h"
#import "hlbutton.h"
#import "HLToolBar.h"
#import "shader.h"
#import "CustomPage.h"

@implementation CMainTabViewController

/*============================================================================*/

-(id)initWithMainTab:(CMainTab*)pMainTab Background:(UIImageView*)pBackground

/*============================================================================*/
{
    if (self == [super initWithNibName:nil bundle:nil])
    {
        m_pBackground = pBackground;
        m_pMainTab = pMainTab;
        m_bInit = FALSE;
        m_pPages = [[NSMutableArray alloc] init];
        m_bEnableScroll = TRUE;
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pScrollView release];
    [m_pPageControl release];
    [m_pHLPageControl release];
    [m_pZoneLabel release];
    [m_pPages release];
    [super dealloc];
}
/*============================================================================*/

- (void)loadView 

/*============================================================================*/
{
    CSubSystemView *pView = [[CSubSystemView alloc] initWithFrame:CGRectZero MainTabPage:(CMainTabViewController*)self];
    pView.backgroundColor = [UIColor clearColor];
    [self setView:pView];
    [pView release];
}
/*============================================================================*/

- (void)viewDidLoad 

/*============================================================================*/
{
    [super viewDidLoad];
    [self.view setOpaque:NO];
    [self.view setBackgroundColor:[UIColor clearColor]];
}
/*============================================================================*/

- (void)viewWillAppear:(BOOL)animated

/*============================================================================*/
{
    m_rOrig = self.view.frame;
    [self InitMain];
}
/*============================================================================*/

- (void)viewDidAppear:(BOOL)animated

/*============================================================================*/
{
    int iDefaultTab = [m_pMainTab GetDefaultTab];
    [self LoadTab:iDefaultTab-1];
    [self LoadTab:iDefaultTab+1];
    
    for (int i = 0; i < [m_pPages count]; i++)
    {
        CSubSystemPage *pPage = (CSubSystemPage*)[m_pPages objectAtIndex:i];
        if ((NSNull*)pPage != [NSNull null]) 
        {
            [pPage SetVisible:(i == iDefaultTab)];
        }
    }
}
/*============================================================================*/

-(NSUInteger)supportedInterfaceOrientations

/*============================================================================*/
{
    NSUInteger iRet = 0;
    //DL - comment it out so far (david M request) - only portrait orientation for iPhone
    //if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationLandscapeLeft])
    //    iRet |= UIInterfaceOrientationMaskLandscapeLeft;
    //if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationLandscapeRight])
    //    iRet |= UIInterfaceOrientationMaskLandscapeRight;
    if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait])
        iRet |= UIInterfaceOrientationMaskPortrait;
    if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortraitUpsideDown])
        iRet |= UIInterfaceOrientationMaskPortraitUpsideDown;
    return iRet;
}
/*============================================================================*/

- (BOOL)shouldAutorotate

/*============================================================================*/
{
    return YES;
}
/*============================================================================*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 

/*============================================================================*/
{
    // Return YES for supported orientations
    if ([CHLComm IsConnecting])
        return FALSE;
    if (![CHLComm Connected])
        return FALSE;

    int iPage = [m_pMainTab GetDefaultTab];
    if (iPage > 0 || iPage < [m_pPages count])
    {
        CSubSystemPage *pPage = (CSubSystemPage*) [m_pPages objectAtIndex:iPage];
        if ((NSNull*)pPage != [NSNull null]) 
        {
            return [pPage ShouldRotate:interfaceOrientation];
        }
    }

    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
/*============================================================================*/

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
    double dAngle = 0.0;
    
    switch (interfaceOrientation)
    {
    case UIDeviceOrientationPortrait:
    case UIDeviceOrientationPortraitUpsideDown:
        dAngle = 0.0;
        break;
        
    default:
        {
            dAngle = 3.14159/2.0;
        }
        break;
    }

    CGRect rFrame = [CRect ScreenRect:interfaceOrientation];
    m_pBackground.transform = CGAffineTransformMakeRotation(dAngle);
    m_pBackground.frame = rFrame;
}
/*============================================================================*/

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration

/*============================================================================*/
{
    BOOL bHideCtrls = NO;
    
    switch (toInterfaceOrientation)
    {
    case UIDeviceOrientationLandscapeLeft:
    case UIDeviceOrientationLandscapeRight:
        {
            printf("Rotate to Landscape\n");
            bHideCtrls = YES;
        }
        break;

    default:
        printf("Rotate to Portrait\n");
        break;
   }

    if (m_bLabels)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.5];
        [m_pLabelsOverlay setAlpha:0.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(LabelsFadeOut:finished:context:)];
        [UIView commitAnimations];

        m_bLabels = FALSE;
    }

    UIApplication *pApp = [UIApplication sharedApplication];
    pApp.statusBarHidden = YES;
    m_bInRotation = TRUE;
}
/*============================================================================*/

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation

/*============================================================================*/
{
    for (int i = 0; i < [m_pPages count]; i++)
    {
        CSubSystemPage *pPage = (CSubSystemPage*)[m_pPages objectAtIndex:i];
        if ((NSNull*)pPage != [NSNull null]) 
        {
            [pPage NotifyRotationComplete:self.interfaceOrientation];
        }
    }

    printf("Rotation Done\n");
    [self.view setNeedsLayout];

    m_bInRotation = FALSE;
}
/*============================================================================*/

- (void)didReceiveMemoryWarning 

/*============================================================================*/
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}
/*============================================================================*/

-(void)InitMain

/*============================================================================*/
{
    if (m_bInit)
        return;
    printf("MainTabPage InitMain Entry\n");
    
    //DL this makes status bar be visile (iPhone)
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        UIView *addStausbar = [[UIView alloc] init];
        addStausbar.frame = CGRectMake(0,0,self.view.frame.size.width,20);
        addStausbar.backgroundColor = [UIColor colorWithWhite:200.0 alpha:0.5];
        [self.view addSubview:addStausbar];
    }
    
    
    CGRect rView = self.view.frame;
    NSMutableArray *pSubTabs = m_pMainTab.m_pSubTabs;
    if ([pSubTabs count] == 0)
        return;

    //
    // Add Our Toolbar  
    //
    CGRect rBar = self.view.bounds;
    rBar.origin.y = 0;
    rBar.size.height = [CMainView DY_TOOLBAR];
    
    //DL
    //rBar.origin.y += 20;
    
    CHLToolBar *pToolBar = [[CHLToolBar alloc] initWithFrame:rBar Orientation:TOOLBAR_TOP];
    m_pToolBar = pToolBar;
    [self.view addSubview:m_pToolBar];


    //
    // Home Button to get back to main
    //
    m_pHomeButton = [[CToolBarButton alloc] initWithFrame:CGRectZero Text:NULL Icon:@"G.png"];
    [m_pHomeButton Orientation:TOOLBAR_TOP];
    
    //DL - was like this - on touch down
    //[m_pHomeButton addTarget:self action:@selector(PopSelf) forControlEvents:UIControlEventTouchDown];
    [m_pHomeButton addTarget:self action:@selector(PopSelf) forControlEvents:UIControlEventTouchUpInside];

    //DL - long press addition
    UILongPressGestureRecognizer *longPressGesture = [[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)] autorelease];
    [longPressGesture setMinimumPressDuration:2];
    [m_pHomeButton addGestureRecognizer:longPressGesture];


    CGRect rToolBar = m_pToolBar.bounds;
    int iDXButton = rToolBar.size.width / 4;
    iDXButton = MIN(iDXButton, 100);
    CGRect rBtn = CGRectMake(0, 0, iDXButton, rToolBar.size.height);
    //DL - iPhone UI controls fish button top margin - it is relative to the tool bar, don't move it!
    //rBtn.origin.y += 20;
    m_pHomeButton.frame = rBtn;

    int iDXImage = rBtn.size.height - 2;
    int iDYImage = iDXImage;
    CGRect rImage = CGRectMake(rBtn.size.width/2-iDXImage/2, rBtn.size.height/2-iDYImage/2, iDXImage, iDYImage);
    
    #ifdef PENTAIR
        UIImageView *pImageView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PWPS_fish.png"]];
    #else
        UIImageView *pImageView  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"G.PNG"]];
    #endif

    pImageView.frame = rImage;
    [m_pHomeButton addSubview:pImageView];
    [pImageView release];

    [pToolBar HLAddSubView:m_pHomeButton];
    [m_pHomeButton setHidden:m_bHideHomeButton];
    [m_pHomeButton release];


    if ([pSubTabs count] == 1)
    {
        //
        // Single Page
        //
        rView.origin.y += [CMainView DY_TOOLBAR];
        rView.size.height -= [CMainView DY_TOOLBAR];
        CSubTab *pSubTab = (CSubTab*)[pSubTabs objectAtIndex:0];
        CSubSystemPage *pPage = [m_pMainTab CreateUserPage:rView SubTab:pSubTab MainTabPage:self];
        [pPage SetVisible:TRUE];
        [self.view addSubview:pPage];
        [m_pPages addObject:pPage];
        m_pModalView = pPage;
        [pPage release];
        [self UpdateVisiblePages];
        [self setTitle:pSubTab.m_pName];
        [self InitZoneLabel];
        m_bInit = TRUE;

        return;
    }

    // Multi-Page

    CGRect rTools = m_pToolBar.bounds;
  
    CGRect rPage;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rPage = CGRectMake(100, 0, rToolBar.size.width/4-100, rTools.size.height);
    }
    else
    {
        int iDXPageL = rToolBar.size.width / 4;
        int iDXPageR = rToolBar.size.width / 3;
        rPage = CGRectMake(iDXPageL+2, 0, rToolBar.size.width-iDXPageL-iDXPageR-4, rTools.size.height);
    }

    CGRect rScrollView = CGRectMake(0, rTools.size.height, rView.size.width, rView.size.height-rTools.size.height);
 //   m_pScrollView = [[UIScrollView alloc] initWithFrame:rScrollView];
    m_pScrollView = [[CHLScrollView alloc] initWithFrame:rScrollView MainTabPage:self];

    if ([pSubTabs count] < 6)
    {
        m_pPageControl = [[UIPageControl alloc] initWithFrame:rPage];
        [m_pToolBar addSubview:m_pPageControl];
    }
    else
    {
        m_pPageControl = [[UIPageControl alloc] initWithFrame:rPage];
        rPage.origin.x += 10;
        rPage.size.width -= 10;
        m_pHLPageControl = [[CHLPageControl alloc] initWithFrame:rPage MainTabPage:self];
        [m_pToolBar addSubview:m_pHLPageControl];
    }

    [self.view addSubview:m_pScrollView];


    m_pScrollView.pagingEnabled = YES;
    m_pScrollView.contentSize = CGSizeMake(rScrollView.size.width * [pSubTabs count], rScrollView.size.height);
    m_pScrollView.showsHorizontalScrollIndicator = NO;
    m_pScrollView.showsVerticalScrollIndicator = NO;
    m_pScrollView.scrollsToTop = NO;
    m_pScrollView.delegate = self;
    [m_pScrollView setDelaysContentTouches:YES];
    [m_pScrollView setDirectionalLockEnabled:YES];

    m_pPageControl.numberOfPages = [pSubTabs count];
    m_pPageControl.currentPage = 0;
    [m_pHLPageControl SetNumberOfPages:[pSubTabs count]];
    [m_pHLPageControl SetCurrentPage:0];
    [m_pPageControl addTarget:self action:@selector(ChangePage:) forControlEvents:UIControlEventValueChanged];

    for (int i = 0; i < [pSubTabs count]; i++)
    {
        [m_pPages addObject:[NSNull null]];
    }

    int iDefaultTab = [m_pMainTab GetDefaultTab];
    [m_pHLPageControl SetCurrentPage:iDefaultTab];
    m_pPageControl.currentPage = iDefaultTab;
    CSubTabHeader *pHeader = [[CSubTabHeader alloc] initWithFrame:CGRectMake(0, 0, 320, 32) MainTab:m_pMainTab];
    [self.navigationItem setTitleView:pHeader];
    [pHeader release];
    [self InitZoneLabel];
    
    // Initial Page
    m_bPageControlBusy = TRUE;
    rPage = rScrollView;
    rPage.origin.x = rPage.size.width * iDefaultTab;
    [self LoadTab:iDefaultTab];
    [m_pScrollView scrollRectToVisible:rPage animated:NO];
    CSubSystemPage *pPage = (CSubSystemPage*)[m_pPages objectAtIndex:iDefaultTab];
    if ((NSNull*)pPage != [NSNull null]) 
    {
        [pPage SetVisible:YES];
    }

    m_bPageControlBusy = FALSE;
    printf("MainTabPage InitMain Done\n");

    m_bInit = TRUE;
}
/*============================================================================*/

-(void)longPress:(UILongPressGestureRecognizer*)gesture

/*============================================================================*/
{
    switch(gesture.state)
    {
        case UIGestureRecognizerStateFailed:
            //printf("Long Falied...");
            break;
        case UIGestureRecognizerStateBegan:
            //printf("Long Began");
            
            //DL - do block animation
            [UIView animateWithDuration:0.5 delay:0 options: UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                             animations:^(void)
            {
                 //here is the animation (pulse from 1 to 0.3 and back)
                 [m_pToolBar setAlpha:0.3];
            }
            completion:^(BOOL finished)
            {
                //restore full alpha when finished
                [m_pToolBar setAlpha:1.0];
                return;
            }];
            break;
        case UIGestureRecognizerStateEnded:
            //kill infinite animation
            [m_pToolBar.layer removeAllAnimations];
            //go back to login screen?
            [((IPAD_ViewController*)m_pMainViewController) ShowConnectPage2];
            break;
        case UIGestureRecognizerStateCancelled:
            //printf("Long Cancelled");
            break;
        case UIGestureRecognizerStateChanged:
            //printf("Long Changed");
            break;
        case UIGestureRecognizerStatePossible:
            //printf("Long Possible");
            break;
    }
}

/*============================================================================*/

-(CMainTab*)MainTab

/*============================================================================*/
{
    return m_pMainTab; 
}
/*=============================================================================*/

-(void)SetMainViewController:(IPOD_ViewController*)mainViewController

/*=============================================================================*/
{
    m_pMainViewController = mainViewController;
}

/*============================================================================*/

-(void)PageLeftRight:(int)iDir

/*============================================================================*/
{
    int iPage = m_pPageControl.currentPage;
    if (iDir == DIR_LEFT)
    {
        if (iPage <= 0)
            return ;
        iPage--;
    }
    else
    {
        if (iPage >= ([m_pMainTab.m_pSubTabs count]-1))
            return;
        iPage++;
    }

    m_pPageControl.currentPage = iPage;
    [m_pHLPageControl SetCurrentPage:iPage];
    [self ChangePage:m_pPageControl];
}
/*============================================================================*/

-(void)PopSelf

/*============================================================================*/
{
      //DL - iPhone - Fish button has been pushed
    UINavigationController *pCtlr = self.navigationController;
    [pCtlr setNavigationBarHidden:YES animated:YES];
    //DL - A
    [pCtlr popViewControllerAnimated:YES];
}
/*============================================================================*/

- (void)scrollViewDidScroll:(UIScrollView *)sender 

/*============================================================================*/
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    // Switch the indicator when more than 50% of the previous/next page is visible
    if (m_bInRotation)
        return;

    switch (self.interfaceOrientation)
    {
    case UIInterfaceOrientationLandscapeLeft:
    case UIInterfaceOrientationLandscapeRight:
        {
            if (m_pLabelsOverlay == NULL)
            {
                CGSize size = [m_pScrollView contentSize];
                m_pLabelsOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 40)];
                [m_pLabelsOverlay setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.75]];
                [m_pLabelsOverlay setOpaque:NO];
                int iDX = m_pScrollView.frame.size.width;
                for (int i = 0; i < [m_pMainTab NSubTabs]; i++)
                {
                    CGRect rTab = CGRectMake(i * iDX, 0, iDX, 40);
                    UILabel *pLabel = [[UILabel alloc] initWithFrame:rTab];
                    CSubTab *pSubTab = [m_pMainTab SubTab:i];
                    [pLabel setText:[pSubTab Name]];
                    [CCustomPage InitStandardLabel:pLabel Size:32];
                    pLabel.highlightedTextColor = [UIColor whiteColor];
                    [m_pLabelsOverlay addSubview:pLabel];
                    [pLabel release];
                }

                [m_pScrollView addSubview:m_pLabelsOverlay];
                [m_pLabelsOverlay release];
                [m_pLabelsOverlay setAlpha:0.0];
            }


            if (!m_bLabels)
            {
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.3];
                [m_pLabelsOverlay setAlpha:1.0];
                [UIView setAnimationDelegate:self];
                [UIView commitAnimations];
            }
            m_bLabels = TRUE;
        }
        break;

    case UIInterfaceOrientationPortrait:
    case UIInterfaceOrientationPortraitUpsideDown:
        break;
    }

    CGFloat pageWidth = m_pScrollView.frame.size.width;
    int page = floor((m_pScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;

    if (m_bPageControlBusy) 
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        if (page == m_iTargetPage)
            m_bPageControlBusy = FALSE;
       return;
    }

    [m_pHLPageControl SetCurrentPage:page];
    m_pPageControl.currentPage = page;

    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self LoadTab:page - 1];
    [self LoadTab:page];
    [self LoadTab:page + 1];
}
/*============================================================================*/

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView 

/*============================================================================*/
{
    m_bPageControlBusy = NO;
    [self UpdateVisiblePages];

    if (m_bLabels)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.5];
        [m_pLabelsOverlay setAlpha:0.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(LabelsFadeOut:finished:context:)];
        [UIView commitAnimations];

        m_bLabels = FALSE;
    }
}
/*============================================================================*/

-(void)LabelsFadeOut:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;
    
    if (m_pLabelsOverlay.alpha > 0)
        return;

    [m_pLabelsOverlay removeFromSuperview];
    m_pLabelsOverlay = NULL;
}
/*============================================================================*/

- (IBAction)ChangePage:(id)sender 

/*============================================================================*/
{
    int iPage = m_pPageControl.currentPage;
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    // update the scroll view to the appropriate page
    CGRect frame = m_pScrollView.frame;
    frame.origin.x = frame.size.width * iPage;
    frame.origin.y = 0;
    [m_pScrollView scrollRectToVisible:frame animated:YES];

    // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    m_bPageControlBusy = YES;
    m_iTargetPage = iPage;

    [self LoadTab:iPage - 1];
    [self LoadTab:iPage];
    [self LoadTab:iPage + 1];

    for (int i = 0; i < [m_pPages count]; i++)
    {
        CSubSystemPage *pPage = (CSubSystemPage*)[m_pPages objectAtIndex:i];
        if ((NSNull*)pPage != [NSNull null]) 
        {
            [pPage SetVisible:(i == iPage)];
        }
    }

    [m_pMainTab SetDefaultTab:iPage];
    [self InitZoneLabel];
}
/*============================================================================*/

-(void)UpdateVisiblePages

/*============================================================================*/
{
    int iContentOffsetX = 0;
    int iDXPage = self.view.frame.size.width;
    if (m_pScrollView != NULL)
        iContentOffsetX = m_pScrollView.contentOffset.x;
    
    for (int i = 0; i < [m_pPages count]; i++)
    {
        CSubSystemPage *pPage = (CSubSystemPage*)[m_pPages objectAtIndex:i];
        if ((NSNull*)pPage != [NSNull null]) 
        {
            int iXL = i * iDXPage - iContentOffsetX;
            int iXR = iXL + iDXPage;
            BOOL bVisible = (iXL < iDXPage && iXR > 0);
            [pPage SetVisible:bVisible];
            if (bVisible)
            {
                int iDXVis = 0;
                if (iXL > 0)
                    iDXVis = iDXPage - iXL;
                else
                    iDXVis = iXR;
                    
                if (iDXVis > iDXPage / 2)
                {
                    if ([m_pMainTab GetDefaultTab] != i)
                    {
                        [m_pMainTab SetDefaultTab:i];
                        [self InitZoneLabel];
                    }
                }
            }
        }
    }
}
/*============================================================================*/

-(void)InitZoneLabel

/*============================================================================*/
{
    if (m_pZoneLabel != NULL)
    {
        [m_pZoneLabel removeFromSuperview];
        [m_pZoneLabel release];
        m_pZoneLabel = NULL;
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        switch (self.interfaceOrientation)
        {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            return;

        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            break;
        }
    }

    CSubTab *pSubTab = [m_pMainTab DefaultTab];


    CGRect rToolBar = m_pToolBar.bounds;
    CGRect rBtn;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rBtn = rToolBar;
    }
    else
    {
        int iDXButton = rToolBar.size.width / 3;
        rBtn = CGRectMake(rToolBar.size.width-iDXButton+2, 2, iDXButton-4, rToolBar.size.height-4);
    }

    m_pZoneLabel = [[UILabel alloc] initWithFrame:rBtn];
    m_pZoneLabel.backgroundColor = [UIColor clearColor];
    m_pZoneLabel.textColor = [UIColor whiteColor];
    m_pZoneLabel.textAlignment = UITextAlignmentCenter;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [CCustomPage InitStandardLabel:m_pZoneLabel Size:40];
    }
    else
    {
        if ([m_pMainTab NSubTabs] <= 1)
        {
            [CCustomPage InitStandardLabel:m_pZoneLabel Size:22];
            m_pZoneLabel.frame = CGRectMake(0, 0, rToolBar.size.width, rToolBar.size.height);
        }
        else
        {
            [CCustomPage InitStandardLabel:m_pZoneLabel Size:16];
        }
    }

    [m_pZoneLabel setText:pSubTab.m_pName];
    [m_pZoneLabel setAdjustsFontSizeToFitWidth:YES];
    [m_pZoneLabel setMinimumFontSize:8];
    [m_pZoneLabel setLineBreakMode:UILineBreakModeWordWrap];
    [m_pZoneLabel setNumberOfLines:2];
    [m_pToolBar addSubview:m_pZoneLabel];
}
/*============================================================================*/

-(void)LoadTab:(int)iIndex

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= [m_pPages count])
        return;


    printf("Loading Page #%d\n", iIndex);

    // replace the placeholder if necessary
    CGRect rScrollView = m_pScrollView.bounds;
    CSubSystemPage *pPage = [m_pPages objectAtIndex:iIndex];
    if ((NSNull*)pPage == [NSNull null]) 
    {
        CGRect rPage = CGRectMake(iIndex * rScrollView.size.width, 0, rScrollView.size.width, rScrollView.size.height);
        NSMutableArray *pSubTabs = m_pMainTab.m_pSubTabs;
        CSubTab *pSubTab = (CSubTab*)[pSubTabs objectAtIndex:iIndex];
        UIView *pView = [m_pMainTab CreateUserPage:rPage SubTab:pSubTab MainTabPage:self ];
        [m_pScrollView addSubview:pView];
        if (m_pLabelsOverlay != NULL)
        {
            [m_pScrollView bringSubviewToFront:m_pLabelsOverlay];
        }
        [m_pPages replaceObjectAtIndex:iIndex withObject:pView];
        [pView release];
    }
}
/*============================================================================*/

-(void)LockHorizontalScroll:(BOOL)bNew

/*============================================================================*/
{
    [m_pScrollView setDelaysContentTouches:!bNew];
    [m_pScrollView setCanCancelContentTouches:!bNew];
    [m_pScrollView setScrollEnabled:!bNew];
    m_bEnableScroll = !bNew;
}
/*============================================================================*/

-(BOOL)IsVisible:(CSubSystemPage*)pPage

/*============================================================================*/
{
    if (m_pScrollView == NULL)
        return TRUE;

    int iContentOffsetX = m_pScrollView.contentOffset.x;
    
    int iXL = pPage.frame.origin.x - iContentOffsetX;
    int iXR = iXL + pPage.frame.size.width;
    if (iXL >= m_pScrollView.frame.size.width)
        return FALSE;
    if (iXR <= 0)
        return FALSE;
    return TRUE;
}
/*============================================================================*/

-(void)EnableHomeButton:(BOOL)bNew

/*============================================================================*/
{
    [m_pHomeButton setHidden:!bNew];
    m_bHideHomeButton = !bNew;
}
/*============================================================================*/

-(BOOL)EnableScroll

/*============================================================================*/
{
    return m_bEnableScroll;
}
/*============================================================================*/

-(CHLToolBar*)ToolBar

/*============================================================================*/
{
    return m_pToolBar;
}
/*============================================================================*/

-(UIView*)ContentView

/*============================================================================*/
{
    if (m_pScrollView != NULL)
        return m_pScrollView;
    return m_pModalView;
}
/*============================================================================*/

-(void)UpdateScrollSubViews

/*============================================================================*/
{
    if (m_pScrollView == NULL)
        return;
    int iNPages = [m_pPages count];
    int iDXView = m_pScrollView.bounds.size.width;
    int iDYView = m_pScrollView.bounds.size.height;
    
    // Update the scroll Views content size
    int iDXAll = iNPages * iDXView;
    [m_pScrollView setContentSize:CGSizeMake(iDXAll, iDYView)];
    int iDefPage = [m_pMainTab GetDefaultTab];
    int iXPage = iDefPage * iDXView;
    [m_pScrollView setContentOffset:CGPointMake(iXPage, 0)];
    for (int i = 0; i < iNPages; i++)
    {
        CSubSystemPage *pPage = (CSubSystemPage*)[m_pPages objectAtIndex:i];
        if ((NSNull*)pPage != [NSNull null]) 
        {
            CGRect rView = CGRectMake(i * iDXView, 0, iDXView, iDYView);
            [pPage setFrame:rView];
        }
    }
}
/*============================================================================*/

-(void)HideNavBar:(BOOL)bNew

/*============================================================================*/
{
    CGRect rThis = self.view.bounds;

    if (rThis.size.height > rThis.size.width)
    {
        // Portrait mode, change now
        CGRect rFrame = CGRectMake(0, 0, self.view.bounds.size.width, [CMainView DY_TOOLBAR]);
        if (bNew)
            rFrame.origin.y -= rFrame.size.height;
        [m_pToolBar setFrame:rFrame];
    }
    m_bHideNavBar = bNew;

}
/*============================================================================*/

-(BOOL)HideNavBar

/*============================================================================*/
{
    return m_bHideNavBar;
}
@end


@implementation CSubSystemView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MainTabPage:(CMainTabViewController*)pPage

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pPage = pPage;
        m_iDXLast = rFrame.size.width;
        m_iDYLast = rFrame.size.height;
    }

    return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    printf("MAINTABVC %0.f x %.0f\n", self.bounds.size.width, self.bounds.size.height);
    CGRect rThis = self.bounds;
    
    if (rThis.size.width == m_iDXLast && rThis.size.height == m_iDYLast)
        return;
    m_iDXLast = rThis.size.width;
    m_iDYLast = rThis.size.height;
    
    UIView *pToolBar = [m_pPage ToolBar];
    UIView *pMain    = [m_pPage ContentView];

    if (rThis.size.width < rThis.size.height)
    {
        // Portrait mode, make sure toolbar is visible
        CGRect rToolBar = [CRect BreakOffTop:&rThis DY:[CMainView DY_TOOLBAR]];
        
        if ([m_pPage HideNavBar])
            rToolBar.origin.y -= rToolBar.size.height;
        //DL - this controls top margin for teh whole tool bar (with the fish button) - iPhone
        rToolBar.origin.y += 20;

        //DL - this controls top margin of the page (pool, spa..) and size
        rThis.origin.y += 20;
        rThis.size.height -= 20;
        
        [pToolBar setFrame:rToolBar];
        [pMain setFrame:rThis];
    }
    else 
    {
        // Landscape mode, make sure toolbar is hidden
        int iDY = [CMainView DY_TOOLBAR];
        CGRect rToolBar = CGRectMake(0, -iDY, pToolBar.frame.size.width, iDY);
        [pToolBar setFrame:rToolBar];
        [pMain setFrame:rThis];
    }
    [m_pPage UpdateScrollSubViews];
}

@end
