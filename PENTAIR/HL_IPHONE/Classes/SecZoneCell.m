//
//  SecZoneCell.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//


#import "SecZoneCell.h"
#import "SecPartition.h"
#import "SecZone.h"
#import "hlm.h"
#import "MainView.h"
#import "crect.h"
#import "CustomPage.h"

@implementation CSecZoneCell

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier Partition:(CSecPartition*)pPartition

/*============================================================================*/
{
	self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];

    if (self)
    {
        // Initialization code
        m_pPartition = pPartition;
        [m_pPartition AddSink:self];


        m_pZoneText = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame
        [CCustomPage InitStandardLabel:m_pZoneText Size:[CMainView DEFAULT_TEXT_SIZE]];
        [m_pZoneText setTextAlignment:UITextAlignmentLeft];

        m_pOK     = [[CZoneIndicatorView alloc] initWithFrame:CGRectZero Text:@"OK"     Color:[UIColor greenColor]];
        m_pFault  = [[CZoneIndicatorView alloc] initWithFrame:CGRectZero Text:@"FAULT"  Color:[UIColor redColor]];
        m_pBypass = [[CZoneIndicatorView alloc] initWithFrame:CGRectZero Text:@"BYPASS" Color:[UIColor blueColor]];

        [self.contentView addSubview:m_pZoneText];
        [self.contentView addSubview:m_pOK];
        [self.contentView addSubview:m_pFault];
        [self.contentView addSubview:m_pBypass];
        [m_pOK release];
        [m_pFault release];
        [m_pBypass release];
        [m_pZoneText release];
    }
    return self;
}
/*============================================================================*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated 

/*============================================================================*/
{

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pPartition RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
	[super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    int iSizeOK = 45;
    int iSizeBP = 45;
    int iSizeFT = 45;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iSizeOK = 75;
        iSizeBP = 75;
        iSizeFT = 75;
    }

    m_pOK.frame     = [CRect BreakOffRight:&contentRect DX:iSizeOK]; 
    m_pBypass.frame = [CRect BreakOffRight:&contentRect DX:iSizeBP]; 
    m_pFault.frame  = [CRect BreakOffRight:&contentRect DX:iSizeFT]; 
    m_pZoneText.frame = contentRect;
    
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    [self UpdateState];
}
/*============================================================================*/

-(void)SetZone:(CSecZone*)pZone

/*============================================================================*/
{
    if (pZone == m_pZone)
        return;
    m_pZone = pZone;
    m_iLastState = -1;
    [self UpdateState];
    [m_pZoneText setText:[m_pZone Name]];
}
/*============================================================================*/

-(void)UpdateState

/*============================================================================*/
{
    if (m_pZone == NULL)
        return;

    int iState = [m_pZone GetState];
    if (iState == m_iLastState)
        return;
    m_iLastState = iState;
    BOOL bOK = TRUE;
    BOOL bFault = FALSE;
    BOOL bBypass = FALSE;

    if (iState & SEC_ZONE_FAULTED)
    {
        bOK = FALSE;
        bFault = TRUE;
    }
    else
    {
        if (iState & SEC_ZONE_TROUBLE)
            bOK = FALSE;
    }
    if ((iState & SEC_ZONE_BYPASSED))
    {
        bBypass = TRUE;
    }

    [m_pFault  Set:bFault];
    [m_pBypass Set:bBypass];
    [m_pOK     Set:bOK];
}
@end

@implementation CZoneIndicatorView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Text:(NSString*)psText Color:(UIColor*)pColor

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        self.opaque = NO;
        m_pLabel  = [[UILabel alloc] initWithFrame:CGRectZero];	// layoutSubViews will decide the final frame

        int iTextSize = 8;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            iTextSize = 12;

        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];

        [m_pLabel setText:psText];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
        m_pColor = pColor;
        [m_pColor retain];
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pColor release];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();
    UIColor *pA = [m_pColor colorWithAlphaComponent:0.25];
    [pA set];
    CGContextFillRect(pRef, rect);
    [m_pColor set];
    CGContextStrokeRect(pRef, rect);
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pLabel setFrame:self.bounds];
}
/*============================================================================*/

-(void)Set:(BOOL)bOn

/*============================================================================*/
{
    if (bOn)
        self.alpha = 1;
    else 
        self.alpha = 0.25;

}
@end