//
//  CustomPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "NSQuery.h"
#import "NSMSG.h"
#import "hlcomm.h"
#import "ControlDef.h"
#import "HatSwitch.h"
#import "hlbutton.h"

#import "CustomPage.h"

@implementation CCustomPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pControls = [[NSMutableArray alloc] init];
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame SysFam:(int)iSysFam ID:(int)iID Orientation:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_iSysFam = iSysFam;
        m_iID = iID;
        m_pControls = [[NSMutableArray alloc] init];
        [self PostGetControls:ioNow];
        self.autoresizingMask = 0xFFFFFFFF;
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MSG:(CNSMSG*)pMSG ZoneID:(int)iZoneID

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pControls = [[NSMutableArray alloc] init];
        [self LoadControls:pMSG ZoneID:iZoneID Flags:0];
        self.autoresizingMask = 0xFFFFFFFF;
    }

    return self;
}
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ControlList:(NSMutableArray*)pList

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        m_pControls = [[NSMutableArray alloc] init];
        [self LoadControlsFromList:pList ZoneID:HLID_NONE];
        self.autoresizingMask = 0xFFFFFFFF;
     }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iN = [m_pControls count];
    for (int i = 0; i < iN; i++)
    {
        CControlDef *pDef = (CControlDef*)[m_pControls objectAtIndex:i];
        UIView *pView = [pDef GetView];
        if (pView != NULL)
        {
            CGRect rFrame = [pDef RectInBounds:rBounds];
            [pView setFrame:rFrame];
        }
    }
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_SETSERVER_GETINTERFACELAYOUTBYIDA)
        return;

    for (int i = 0; i < [m_pControls count]; i++)
    {
        CControlDef *pDef = (CControlDef*)[m_pControls objectAtIndex:i];
        UIView *pView = [pDef GetView];
        [pView removeFromSuperview];
    }

    [m_pControls removeAllObjects];
        
    [self LoadControls:pMSG ZoneID:0 Flags:0];
        
    [CHLComm RemoveSocketSink:self];

    [UIView setAnimationDuration:0.20];
    [self setAlpha:1];
    [UIView commitAnimations];

    m_bWaitData = FALSE;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pName release];
    [CHLComm RemoveSocketSink:self];
    [m_pControls release];
    [super dealloc];
}
/*============================================================================*/

-(void)LoadControlsFromList:(NSMutableArray*)pList ZoneID:(int)iZoneID

/*============================================================================*/
{
    //
    // Load Everybody up into an array
    //
    NSMutableArray *pAudioButtons = [[NSMutableArray alloc] init];
    NSMutableArray *pHatSwitches  = [[NSMutableArray alloc] init];

    int iN = [pList count];
    for (int i = 0; i < iN; i++)
    {
        CControlDef *pDef = (CControlDef*)[pList objectAtIndex:i];
        UIView *pView = [self AddControl:pDef];

        [pDef SetView:pView];
        [m_pControls addObject:pDef];

        if (pView != NULL)
        {
            switch ([pDef Type])
            {
            case CONTROL_HATSWITCH:
            case CONTROL_HATSWITCH_COMPLEX:
                [pHatSwitches addObject:pView];
                break;
                
            case CONTROL_AUDIO_BUTTON:
                [pAudioButtons addObject:pView];
                break;
            }
        }
    }
    
    for (int iButton = 0; iButton < [pAudioButtons count]; iButton++)
    {
        CHLButton *pButton = (CHLButton*)[pAudioButtons objectAtIndex:iButton];
        CGRect rFrameButton = pButton.frame;
        for (int iHat = 0; iHat < [pHatSwitches count]; iHat++)
        {
            CHatSwitch *pHat = (CHatSwitch*)[pHatSwitches objectAtIndex:iHat];
            CGRect rFrameHat = pHat.frame;
            if (CGRectIntersectsRect(rFrameHat, rFrameButton))
            {
                [pButton SetHatSwitch:pHat];
            }
        }
    }

    [pAudioButtons release];
    [pHatSwitches release];
}
/*============================================================================*/

-(void)LoadControls:(CNSMSG*)pMSG ZoneID:(int)iZoneID Flags:(int)iFlags

/*============================================================================*/
{
    //
    // Load Everybody up into an array
    //
    NSMutableArray *pAudioButtons = [[NSMutableArray alloc] init];
    NSMutableArray *pHatSwitches  = [[NSMutableArray alloc] init];

    int iN = [pMSG GetInt];

    for (int i = 0; i < iN; i++)
    {
        CControlDef *pDef = [CControlDef alloc];
        [pDef SetZoneID:iZoneID];
        [pDef initWithMSG:pMSG Flags:(int)iFlags];

        UIView *pView = [self AddControl:pDef];
        
        [pDef SetView:pView];
        [m_pControls addObject:pDef];


        if (pView != NULL)
        {
            switch ([pDef Type])
            {
            case CONTROL_HATSWITCH:
            case CONTROL_HATSWITCH_COMPLEX:
                [pHatSwitches addObject:pView];
                break;
                
            case CONTROL_AUDIO_BUTTON:
                [pAudioButtons addObject:pDef];
                break;
            }
        }

        [pDef release];
    }
    
    CGRect rBounds = self.bounds;
    for (int iButton = 0; iButton < [pAudioButtons count]; iButton++)
    {
        CControlDef *pButtonDef = (CControlDef*)[pAudioButtons objectAtIndex:iButton];
//        CGRect rFrameButton = pButton.frame;
        CGRect rFrameButton = [pButtonDef RectInBounds:rBounds];
        for (int iHat = 0; iHat < [pHatSwitches count]; iHat++)
        {
            CHatSwitch *pHat = (CHatSwitch*)[pHatSwitches objectAtIndex:iHat];
            CGRect rFrameHat = pHat.frame;
            if (CGRectIntersectsRect(rFrameHat, rFrameButton))
            {
                CHLButton *pButton = (CHLButton*)[pButtonDef GetView];
                [pButton SetHatSwitch:pHat];
            }
        }
    }

    [pAudioButtons release];
    [pHatSwitches release];
}
/*============================================================================*/

-(UIView*)AddControl:(CControlDef*)pDef

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    UIView *pView = [pDef CreateView:rBounds];
    
    if (pView != NULL)
    {
        [self addSubview:pView];
        [pView release];
        if ([pView conformsToProtocol:@protocol(CHLView)])
        {
            CHLView *pHLView = (CHLView*)pView;
            [pHLView SetVisible:m_bVisible];
        }

        return pView;
    }
    
    return NULL;
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bNew

/*============================================================================*/
{
    for (int i = 0; i < [m_pControls count]; i++)
    {
        CControlDef *pDef = (CControlDef*)[m_pControls objectAtIndex:i];
        id pView = [pDef GetView];
        if ([pView conformsToProtocol:@protocol(CHLView)])
        {
            CHLView *pHLView = (CHLView*)pView;
            [pHLView SetVisible:bNew];
        }
    }

    m_bVisible = bNew;
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

+(void)SetVisible:(UIView*)pView Visible:(BOOL)bVisible

/*============================================================================*/
{
    if (pView == NULL)
        return;

    if (![pView conformsToProtocol:@protocol(CHLView)])
        return;
        
    CHLView *pHLView = (CHLView*)pView;
    [pHLView SetVisible:bVisible];
}
/*============================================================================*/

-(void)SetName:(NSString*)pName

/*============================================================================*/
{
    [m_pName release];
    m_pName = pName;
    [m_pName retain];
}
/*============================================================================*/

-(NSString*)GetName

/*============================================================================*/
{
    return m_pName;
}
/*============================================================================*/

-(void)NotifyRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.20];
    [self setAlpha:0];
    [UIView commitAnimations];
    

    [self PostGetControls:ioNow];
}
/*============================================================================*/

-(void)PostGetControls:(UIInterfaceOrientation)ioNow

/*============================================================================*/
{
    if (m_bWaitData)
    {
        [CHLComm RemoveSocketSink:self];
    }


    int iRes = HL_320x240;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iRes = HL_800x600;
    }
    else
    {
        switch (ioNow)
        {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            iRes = HL_240x320;
            break;

        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            break;
        }

        if (m_iSysFam != FAMILY_VIDEO)
        {
            iRes = HL_240x320;
        }
    }

    [CHLComm AddSocketSink:self];
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_SETSERVER_GETINTERFACELAYOUTBYIDQ];
    [pQ autorelease];
    [pQ PutInt:m_iSysFam];
    [pQ PutInt:m_iID];
    [pQ PutInt:iRes];
    [pQ SendMessageWithMyID:self];
    m_bWaitData = TRUE;
}
/*============================================================================*/

+(UIImage*)ScaleImage:(UIImage*)pImage Height:(int)iDY

/*============================================================================*/
{
    CGSize szNew = CGSizeMake(iDY * pImage.size.width / pImage.size.height, iDY);

    UIGraphicsBeginImageContext(szNew);

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextTranslateCTM(context, 0.0, szNew.height);

    CGContextScaleCTM(context, 1.0, -1.0);

    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, szNew.width, szNew.height), pImage.CGImage);

    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return scaledImage;
}
/*============================================================================*/

+(void)AddScaledImage:(NSMutableArray*)pArray Named:(NSString*)pName Height:(int)iDY;

/*============================================================================*/
{
    UIImage *pBase = [UIImage imageNamed:pName];
    UIImage *pScale = [self ScaleImage:pBase Height:iDY];
    [pArray addObject:pScale];
}
/*============================================================================*/

+(void)InitStandardLabel:(UILabel*)pLabel Size:(int)iTextSize

/*============================================================================*/
{
    [pLabel setBackgroundColor:[UIColor clearColor]];
    [pLabel setTextAlignment:UITextAlignmentCenter];

#ifdef PENTAIR
    UIFont *pFont = [UIFont systemFontOfSize:iTextSize];
#else
    UIFont *pFont = [UIFont fontWithName:@"Arial" size:iTextSize];
    if (pFont == NULL)
        pFont = [UIFont boldSystemFontOfSize:iTextSize];
#endif
    [pLabel setFont:pFont];

    [pLabel setTextColor:[CHLComm GetRGB:RGB_APP_TEXT]];
}
/*============================================================================*/

+(void)InitStandardLabelSize:(UILabel*)pLabel Size:(int)iTextSize

/*============================================================================*/
{
#ifdef PENTAIR
    UIFont *pFont = [UIFont boldSystemFontOfSize:iTextSize];
#else
    UIFont *pFont = [UIFont fontWithName:@"Arial" size:iTextSize];
    if (pFont == NULL)
        pFont = [UIFont boldSystemFontOfSize:iTextSize];
#endif
    [pLabel setFont:pFont];
}
@end
