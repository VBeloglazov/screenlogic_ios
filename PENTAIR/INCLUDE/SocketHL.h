/*
--------------------------------------------------------------------------------

    SocketHL.H

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once
#include "hltime.h"

class CHLMSG;

#define OUTPUT_BUFFER           20000

/*============================================================================*/

    class CSocketHL

/*============================================================================*/
{
public:
    CHLTimer                    m_hltConnect;

private:
    SOCKET                      m_hSocket;

    BOOL                        m_bValid;

    CHLString                   m_strLastError;

    int                         m_iTimeout;

    DWORD                       m_dwIP;
	BYTE						m_btIP1;
	BYTE						m_btIP2;
	BYTE						m_btIP3;
	BYTE						m_btIP4;
	WORD						m_wPort;
	CHLString					m_sAddress;
	CHLString					m_sAddressPort;

    BOOL                        m_bConnected;

    BOOL                        m_bBlocking;
    BOOL                        m_bTCPNoDelay;

    CHLTimer                    m_hltRecv;

    BYTE*                       m_pBufferIn;			// Working buffer
    int                         m_iNBufferIn;			// Working buffer size
    int                         m_iBufferIn;			// Insertion point

    BYTE*                       m_pMessage;			    // Complete message
    int                         m_iNMessage;			// Message size

    int                         m_iBytesLastRead;       // Last read portion size

    int                         m_iNTotalBytesIn;		// Bytes received from client
    int                         m_iNTotalBytesOut;		// Bytes sent to client
    int                         m_iNMessagesIn;			// Questions
    int                         m_iNMessagesOut;		// Responses

    int                         m_iNPartialHeaderRX;	// Total partial headers
    int                         m_iNPartialRX;			// Total partial receives
    int                         m_iNMultipleRX;			// Total multi-message receives
    int                         m_iNMultiPacketRX;		// Total multiple-packet messages
    int                         m_iNQATickCount;		// Total secs thinking Q to A

    BYTE                        m_pOutputBuffer[OUTPUT_BUFFER];
    int                         m_iNOutputBuffer;

    // Public properties
public:
    BOOL                        IsValid()           { return m_bValid;          }
    CHLString                   LastError()         { return m_strLastError;    }
    SOCKET                      Socket()            { return m_hSocket;         }
    DWORD                       IP()                { return m_dwIP;            }
    BYTE						IP1()               { return m_btIP1;           }
    BYTE						IP2()               { return m_btIP2;           }
    BYTE						IP3()               { return m_btIP3;           }
    BYTE						IP4()               { return m_btIP4;           }
	WORD						Port()				{ return m_wPort;			}
	CHLString					Address()			{ return m_sAddress;		}
	CHLString					AddressPort()		{ return m_sAddressPort;	}
    BOOL                        IsConnected()       { return m_bConnected;      }
    BOOL                        Blocking()          { return m_bBlocking;       }
    BYTE*                       Message()           { return m_pMessage;        }
    int                         NBytesLastRead()    { return m_iBytesLastRead;  }
    int                         NTotalBytesIn()     { return m_iNTotalBytesIn;  }
    int                         NTotalBytesOut()    { return m_iNTotalBytesOut; }
    int                         NMessagesIn()       { return m_iNMessagesIn;    }
    int                         NMessagesOut()      { return m_iNMessagesOut;   }
    int                         NPartialRX()        { return m_iNPartialRX;     }
    int                         NMultipleRX()       { return m_iNMultipleRX;    }
    int                         NQATickCount()      { return m_iNQATickCount;   }

public:
    CSocketHL(
    BYTE*                       pIP,
    WORD                        wPort,
    int                         iTimeout = 60000 );

    CSocketHL(
    CHLString                   sIP,
    WORD                        wPort,
    int                         iTimeout = 60000 );

    CSocketHL(
    SOCKET                      hSocket,
    SOCKADDR_IN*                pAddress,
    int                         iTimeout = 60000 );

    virtual ~CSocketHL();

    // Status Messages
    void                        GetCommStats(
    CHLMSG*                     pMSG );

    // Communications
    void                        Disconnect(
    CHLString                   sReason );

    void                        SetBlocking( BOOL bNew );

    BOOL                        TCPNoDelay();
    void                        SetTCPNoDelay( BOOL bNew );

    void                        IncrementNMessagesOut();

    int                         NPartialHeaderRX();
    int                         NMultiPacketRX();
    void                        IncrementNQATickCount( int iDTicks );

    BOOL                        ReadyToSend();

    BOOL                        Send(
    int                         iNBytes,
    BYTE*                       pBytes,
    BOOL                        bCritical = TRUE );

    BOOL                        CheckReceiveMsg();

    BOOL                        CheckReceiveBuf(
    ULONG                       ulBytesExpected = 0 );

protected:
    void                        SetLastError(
    CHLString                   sLastError )            { m_strLastError = sLastError; }

    void                        AllocBuffer(
    int                         iSize );

    void                        AllocMessage(
    int                         iSize );

    BOOL                        AppendOutputBuffer(
    int                         iNBytes,
    BYTE*                       pData );

    BOOL                        ClearOutputBuffer();
};

