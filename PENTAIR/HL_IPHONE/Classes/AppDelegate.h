//
//  XPAD_VIEWAppDelegate.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class IPOD_ViewController;
@class CHLComm;
@class CSystemModeList;
@class CForecast;

#ifdef PENTAIR
    @class CNSPoolConfig;
#endif

/*============================================================================*/

@interface CButtonImage : NSObject

/*============================================================================*/
{
    int                                 m_iDX;
    int                                 m_iDY;
    int                                 m_iSHIN;
    int                                 m_iSHOUT;
    int                                 m_iRad;
    unsigned int                        m_rgbT;
    unsigned int                        m_rgbB;
    
    CGImageRef                          m_pImageRef;
}

-(id)initWithDX:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT Rad:(int)iRad RGBT:(unsigned int)rgbT RGBB:(unsigned int)rgbB;
-(BOOL)IsEqualTo:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT Rad:(int)iRad RGBT:(unsigned int)rgbT RGBB:(unsigned int)rgbB;
-(BOOL)IsFree;
-(CGImageRef)GetImageRef;

@end
/*============================================================================*/

@interface CMainTabImage : NSObject

/*============================================================================*/
{
    int                                 m_iDX;
    int                                 m_iDY;
    BOOL                                m_bSelected;
    
    CGImageRef                          m_pImageRef;
}

-(id)initWithDX:(int)iDX DY:(int)iDY Selected:(BOOL)bSelected;
-(BOOL)IsEqualTo:(int)iDX DY:(int)iDY Selected:(BOOL)bSelected;
-(BOOL)IsFree;
-(CGImageRef)GetImageRef;

@end
/*============================================================================*/

@interface CToolBarImage : NSObject

/*============================================================================*/
{
    int                                 m_iFrameStyle;
    int                                 m_iOrientation;
    int                                 m_iState;
    int                                 m_iDY;
    CGImageRef                          m_pImageRef;
}

-(id)initWithDY:(int)iDY Orientation:(int)iOrientation State:(int)iState FrameStyle:(int)iFrameStyle;
-(BOOL)IsEqualToDY:(int)iDY Orientation:(int)iOrientation State:(int)iState FrameStyle:(int)iFrameStyle;
-(BOOL)IsFree;
-(CGImageRef)GetImageRef;

@end
/*============================================================================*/

@interface CAppDelegate : NSObject <UIApplicationDelegate, AVAudioPlayerDelegate > 

/*============================================================================*/
{
    CHLComm                             *m_pComm;
	IBOutlet UIWindow                   *window;
	IBOutlet UINavigationController     *navigationController;
    CSystemModeList                     *m_pModeList;

#ifdef PENTAIR
    CNSPoolConfig                       *m_pPoolConfig;
#endif

    NSMutableArray                      *m_pButtons;
    NSMutableArray                      *m_pMainTabs;
    NSMutableArray                      *m_pToolBars;

    CForecast                           *m_pForecast;
    
#ifndef PENTAIR
    AVAudioPlayer                       *m_pButtonPlayer;
#endif
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;

-(CSystemModeList*)SysModeList;
-(CSystemModeList*)InitSysModeList;

+(int)BackgroundStyle;
+(void)BackgroundStyle:(int)iNew;
-(void)InitSounds;

#ifdef PENTAIR
-(void)InitPoolConfig;
-(void)RestartPoolConfig;
-(CNSPoolConfig*)PoolConfig;
#endif

-(CGImageRef)GetButtonImage:(int)iDX DY:(int)iDY Rad:(int)iRad SHIN:(int)iSHIN SHOUT:(int)iSHOUT ColorT:(unsigned int)rgbT ColorB:(unsigned int)rgbB;
-(CGImageRef)GetMainTabImage:(int)iDX DY:(int)iDY Selected:(BOOL)bSelected;
-(CGImageRef)GetToolBarImage:(int)iDY Orientation:(int)iOrientation State:(int)iState FrameStyle:(int)iFrameStyle;
-(void)CollectGarbage;

-(CForecast*)Forecast;

-(void)PlaySoundButtonPress;

@end

