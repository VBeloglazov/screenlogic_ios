//
//  rect.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/19/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "crect.h"


@implementation CRect

/*============================================================================*/

+(CGRect)BreakOffLeft:(CGRect*) pRect DX:(int) iDX

/*============================================================================*/
{
    CGRect rRet = CGRectMake(pRect->origin.x, pRect->origin.y, iDX, pRect->size.height);
    pRect->origin.x += iDX;
    pRect->size.width -= iDX;
    return rRet;
}
/*============================================================================*/

+(CGRect)BreakOffRight:(CGRect*) pRect DX:(int) iDX

/*============================================================================*/
{
    CGRect rRet = CGRectMake(pRect->origin.x + pRect->size.width-iDX, pRect->origin.y, iDX, pRect->size.height);
    pRect->size.width -= iDX;
    return rRet;
}
/*============================================================================*/

+(CGRect)BreakOffTop:(CGRect*) pRect DY:(int) iDY

/*============================================================================*/
{
    CGRect rRet = CGRectMake(pRect->origin.x, pRect->origin.y, pRect->size.width, iDY);
    pRect->origin.y += iDY;
    pRect->size.height -= iDY;
    return rRet;
}
/*============================================================================*/

+(CGRect)BreakOffBottom:(CGRect*) pRect DY:(int) iDY

/*============================================================================*/
{
    CGRect rRet = CGRectMake(pRect->origin.x, pRect->origin.y + pRect->size.height-iDY, pRect->size.width , iDY);
    pRect->size.height -= iDY;
    return rRet;
}
/*============================================================================*/

+(void)Inset:(CGRect*) pRect DX:(int) iDX DY:(int)iDY

/*============================================================================*/
{
    pRect->origin.x += iDX;
    pRect->origin.y += iDY;
    pRect->size.width -= 2 * iDX;
    pRect->size.height -= 2 * iDY;
}
/*============================================================================*/

+(CGRect)ScreenRect:(UIInterfaceOrientation)orientation

/*============================================================================*/
{
    CGRect rMain = [UIScreen mainScreen].bounds;

    switch(orientation)
    {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            break;

        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            {
                int iDX = rMain.size.width;
                int iDY = rMain.size.height;
                rMain.size.height = iDX;
                rMain.size.width  = iDY;
            }
            break;
    }

    return rMain;
}
/*============================================================================*/

+(int)XMid:(CGRect)rRect

/*============================================================================*/
{
    return rRect.origin.x + rRect.size.width / 2;
}
/*============================================================================*/

+(int)YMid:(CGRect)rRect

/*============================================================================*/
{
    return rRect.origin.y + rRect.size.height / 2;
}
/*============================================================================*/

+(void)ConfineToAspect:(CGRect*)pRect DXDY:(double)dAspect

/*============================================================================*/
{
    int iDX = pRect->size.width;
    int iDY = pRect->size.height;

    double dAspectThis = (double)iDX / (double)iDY;

    int iXC = pRect->origin.x + iDX / 2;
    int iYC = pRect->origin.y + iDY / 2;

    if (dAspect > dAspectThis)
    {
        iDY = (int) ((double)iDX/dAspect);
    }
    else 
    {
        iDX = (int)((double)iDY*dAspect);
    }

    pRect->origin.x = iXC-iDX/2;
    pRect->origin.y = iYC-iDY/2;
    pRect->size.width   = iDX;
    pRect->size.height  = iDY;
    
}
/*============================================================================*/

+(void)ConfineToAspect:(CGRect*)pRect Source:(CGRect)rSource

/*============================================================================*/
{
    int iDX = pRect->size.width;
    int iDY = pRect->size.height;

    double dAspectThis = (double)iDX / (double)iDY;

    int iXC = pRect->origin.x + iDX / 2;
    int iYC = pRect->origin.y + iDY / 2;

    double dAspect = (double)rSource.size.width / (double)rSource.size.height;

    if (dAspect > dAspectThis)
    {
        iDY = (int) ((double)iDX/dAspect);
    }
    else 
    {
        iDX = (int)((double)iDY*dAspect);
    }

    pRect->origin.x = iXC-iDX/2;
    pRect->origin.y = iYC-iDY/2;
    pRect->size.width   = iDX;
    pRect->size.height  = iDY;
    
}
/*============================================================================*/

+(void)ExpandToAspect:(CGRect*)pRect DXDY:(double)dAspect

/*============================================================================*/
{
    int iDX = pRect->size.width;
    int iDY = pRect->size.height;

    double dAspectThis = (double)iDX / (double)iDY;

    int iXC = pRect->origin.x + iDX / 2;
    int iYC = pRect->origin.y + iDY / 2;

    if (dAspect < dAspectThis)
    {
        iDY = (int) ((double)iDX/dAspect);
    }
    else 
    {
        iDX = (int)((double)iDY*dAspect);
    }

    pRect->origin.x = iXC-iDX/2;
    pRect->origin.y = iYC-iDY/2;
    pRect->size.width   = iDX;
    pRect->size.height  = iDY;
    
}

/*============================================================================*/

+(void)CenterDownToX:(CGRect*)pRect DX:(int)iDX

/*============================================================================*/
{
    int iDXNow = pRect->size.width;
    if (iDXNow < iDX)
        return;
    int iDiff = iDXNow - iDX;
    pRect->origin.x += iDiff / 2;
    pRect->size.width -= iDiff;
}
/*============================================================================*/

+(void)CenterDownToY:(CGRect*)pRect DY:(int)iDY

/*============================================================================*/
{
    int iDYNow = pRect->size.height;
    if (iDYNow < iDY)
        return;
    int iDiff = iDYNow - iDY;
    pRect->origin.y += iDiff / 2;
    pRect->size.height -= iDiff;
}
/*============================================================================*/

+(CGRect)CenteredRect:(CGRect)rect DX:(int)iDX DY:(int)iDY

/*============================================================================*/
{
    int iXC = rect.origin.x + rect.size.width / 2;
    int iYC = rect.origin.y + rect.size.height / 2;
    return CGRectMake(iXC-iDX/2, iYC-iDY/2, iDX, iDY);
}
/*============================================================================*/

+(CGRect)CreateXSection:(CGRect)rect Index:(int)iIndex Of:(int)iN

/*============================================================================*/
{
    float fDX = rect.size.width / (float)iN;
    return CGRectMake(rect.origin.x + iIndex * fDX, rect.origin.y, fDX, rect.size.height);
}
/*============================================================================*/

+(CGRect)CreateYSection:(CGRect)rect Index:(int)iIndex Of:(int)iN

/*============================================================================*/
{
    float fDY = rect.size.height / (float)iN;
    return CGRectMake(rect.origin.x, rect.origin.y + iIndex * fDY, rect.size.width, fDY);
}
@end
