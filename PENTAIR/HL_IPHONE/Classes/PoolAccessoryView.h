//
//  PoolAccessoryView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"

@class CNSPoolConfig;

#define TYPE_AIRTEMP                    21000

/*============================================================================*/

@interface CPoolAccessoryView : UIView < CSink >

/*============================================================================*/
{
    UILabel                             *m_pLabel;
    UILabel                             *m_pSubLabel;
    CNSPoolConfig                       *m_pConfig;
    int                                 m_iType;
}


-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig Type:(int)iType;
@end
