//
//  WeatherHeaderView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/18/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "WeatherHeaderView.h"
#import "weather.h"
#import "crect.h" 
#import "hlcomm.h"
#import "NSMSG.h"
#import "NSQuery.h"
#import "CustomPage.h"

#define TEMP_SIZE_IPAD              52
#define DESC_SIZE_IPAD              18
#define TEMP_SIZE_IPOD              32
#define DESC_SIZE_IPOD              12

@implementation CWeatherTextHeaderView

//DL - this is for upper bar weather temperature and text
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Forecast:(CForecast*)pForecast

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        m_pForecast = pForecast;

        int iSize1 = TEMP_SIZE_IPAD;
        int iSize2 = DESC_SIZE_IPAD;

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            iSize1 = TEMP_SIZE_IPOD;
            iSize2 = DESC_SIZE_IPOD;
        }

        m_pTemp = [self AddLabel:@"" Size:iSize1];
        m_pDesc = [self AddLabel:@"" Size:iSize2];
        [m_pForecast AddSink:self];
        [self Notify:0 Data:0];
        

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            // Doesn't really fit
            [m_pDesc setHidden:YES];
            m_pTemp.textColor = [UIColor lightGrayColor];

        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pForecast RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iTemp = [m_pForecast CurrentTemp];
    if (iTemp == FC_VALUE_UNKNOWN)
    {
        [m_pTemp setText:@"-"];
    }
    else
    {
        NSString *pTemp = [NSString stringWithFormat:@"%d°", [m_pForecast CurrentTemp]];
        [m_pTemp setText:pTemp];
        NSLog(@"%@", pTemp);
    }
    [m_pDesc setText:[m_pForecast Conditions]];
}
/*============================================================================*/

-(UILabel*)AddLabel:(NSString*)pText Size:(int)iSize

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [CCustomPage InitStandardLabel:pLabel Size:iSize];
    [pLabel setText:pText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iSize1 = TEMP_SIZE_IPAD;
    int iSize2 = DESC_SIZE_IPAD;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iSize1 = TEMP_SIZE_IPOD;
        iSize2 = DESC_SIZE_IPOD;
    }

    CGRect rBounds = self.bounds;
    [CRect CenterDownToY:&rBounds DY:(iSize1+iSize2)];
    CGRect rDesc = [CRect BreakOffBottom:&rBounds DY:iSize2];
    [m_pTemp setFrame:rBounds];
    [m_pDesc setFrame:rDesc];
}
@end

@implementation CWeatherIconHeaderView

//DL this is for upper bar weather picture
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Forecast:(CForecast*)pForecast

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        m_pForecast = pForecast;
        [CHLComm AddSocketSink:self];
        [m_pForecast AddSink:self];
        [self Notify:0 Data:0];
        self.opaque = FALSE;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    if (m_pImage != NULL)
        CGImageRelease(m_pImage);
    if (m_pData != NULL)
        free(m_pData);

    [m_pForecast RemoveSink:self];
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    printf("WEATHER ICON HEADERVIEW1\n");

    CGContextRef context = UIGraphicsGetCurrentContext();
    if (m_pImage == NULL)
        return;

    int iDYThis = rect.size.height;
    int iDX = CGImageGetWidth(m_pImage);
    int iDY = CGImageGetHeight(m_pImage);

    float fDXDY = (float)iDX / (float)iDY;
    [CRect ConfineToAspect:&rect DXDY:fDXDY];
    
    CGContextScaleCTM(context, 1, -1);
    rect.origin.y -= iDYThis;
    CGContextDrawImage(context, rect, m_pImage);
    CGContextScaleCTM(context, 1, -1);
    printf("WEATHER ICON HEADERVIEW2\n");

}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (m_pImage != NULL)
        CGImageRelease(m_pImage);
    if (m_pData != NULL)
        free(m_pData);

    m_pImage = NULL;
    m_pData = NULL;

    CNSQuery *pQ = [[[CNSQuery alloc] initWithQ:HLM_WEATHER_GETCCIMAGEQ] autorelease];
    [pQ PutInt:200];
    [pQ PutInt:200];
    [pQ SendMessageWithMyID:self];
    [self.superview layoutSubviews];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    [m_pForecast Post];
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    
    switch ([pMSG MessageID]) 
    {
    case HLM_WEATHER_GETCCIMAGEA:
        {
            if (m_pImage != NULL)
                CGImageRelease(m_pImage);
            if (m_pData != NULL)
                free(m_pData);

            m_pImage = NULL;
            m_pData = NULL;

            int iEncodeType     = [pMSG GetInt];
            [pMSG GetString];   // Aux Data eg 20%,30% etc.
            int iSize           = [pMSG GetInt];
            void *pData         = [pMSG GetDataAtReadIndex];
            m_pData = malloc(iSize);
            memcpy(m_pData, pData, iSize);

            CGDataProviderRef pSrc = CGDataProviderCreateWithData(NULL, m_pData, iSize, NULL);
            switch (iEncodeType)
            {
            case ENCODE_JPEG:
                m_pImage = CGImageCreateWithJPEGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
                break;

            case ENCODE_GIF:
                {
                    NSData *pNSData = [NSData dataWithBytes:pData length:iSize];
                    UIImage *pImage = [UIImage imageWithData:pNSData];
                    m_pImage = [pImage CGImage];
                    CGImageRetain(m_pImage);
                }
                break;
            
            case ENCODE_PNG:
                m_pImage = CGImageCreateWithPNGDataProvider(pSrc, NULL, FALSE, kCGRenderingIntentDefault);
                break;
            }

            CGDataProviderRelease(pSrc);
            [self setNeedsDisplay];
            [self.superview layoutSubviews];
        }
        break;

    default:
        break;
    }
}
/*============================================================================*/

-(BOOL)HasImage

/*============================================================================*/
{
    return (m_pImage != NULL);
}
@end


@implementation CWeatherHeaderView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Forecast:(CForecast*)pForecast

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
        m_pForecast = pForecast;
        [m_pForecast retain];
        m_pText = [[CWeatherTextHeaderView alloc] initWithFrame:CGRectZero Forecast:pForecast];
        m_pIcon = [[CWeatherIconHeaderView alloc] initWithFrame:CGRectZero Forecast:pForecast];
        [self addSubview:m_pText];
        [self addSubview:m_pIcon];
        [m_pText release];
        [m_pIcon release];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pForecast release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDXImage = rThis.size.height;

    if (![m_pIcon HasImage])
    {
        CGRect rText = rThis;
        [CRect BreakOffLeft:&rText DX:iDXImage];
        [m_pText setFrame:rText];
    }
    else
    {
        CGRect rText = rThis;
        CGRect rIcon = [CRect BreakOffRight:&rText DX:iDXImage];
        [m_pIcon setFrame:rIcon];
        [m_pText setFrame:rText];
    }

    
}

@end

