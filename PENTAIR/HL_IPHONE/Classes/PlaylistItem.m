//
//  PlaylistItem.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "PlaylistItem.h"
#import "NSMSG.h"


@implementation CPlaylistItem

@synthesize m_pArtist;
@synthesize m_pAlbum;
@synthesize m_pTrack;
@synthesize m_iTrack;
@synthesize m_iLen;
@synthesize m_iState;


/*============================================================================*/

-(id)initWithMSG:(CNSMSG*)pMSG;

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pArtist = [[pMSG GetString] retain];
        m_pAlbum  = [[pMSG GetString] retain];
        m_iTrack  = [pMSG GetInt];
        m_pTrack  = [[pMSG GetString] retain];
        m_iLen    = [pMSG GetInt];
        m_iState  = [pMSG GetInt];
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pArtist release];
    [m_pAlbum release];
    [m_pTrack release];
    [super dealloc];
}
/*============================================================================*/

-(BOOL)IsSameItem:(CPlaylistItem*)pOther

/*============================================================================*/
{
    if (![self.m_pArtist isEqual:pOther.m_pArtist])
        return FALSE;
    if (![self.m_pAlbum isEqual:pOther.m_pAlbum])
        return FALSE;
    if (![self.m_pTrack isEqual:pOther.m_pTrack])
        return FALSE;
    if (m_iTrack != pOther.m_iTrack)
        return FALSE;
    if (m_iLen != pOther.m_iLen)
        return FALSE;
    if (m_iState != pOther.m_iState)
        return FALSE;
    return TRUE;
}
@end
