/*
--------------------------------------------------------------------------------

    ServerDataRetriever.h

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#pragma once

/*============================================================================*/

    class CServerDataRetriever

/*============================================================================*/
{
    BOOL                        m_bValid;

    CHLString                   m_sURL;
    BYTE                        m_byAddress[4];
    CHLString                   m_sAddress;
    WORD                        m_wPort;

public:
    // Properties
    BOOL                        IsValid()       { return m_bValid;                                        }
    CHLString                   URL()           { return m_bValid ? m_sURL      : CHLString(_HLTEXT("")); }
    CHLString                   AddressString() { return m_bValid ? m_sAddress  : CHLString(_HLTEXT("")); }
    BYTE*                       AddressByte()   { return m_bValid ? m_byAddress : NULL;                   }
    WORD                        Port()          { return m_bValid ? m_wPort     : 0;                      }

public:
    // Construction/Destruction
    CServerDataRetriever(int iServer);
    ~CServerDataRetriever();

private:
    BOOL                        ReadServersoftDataFromFile();
};