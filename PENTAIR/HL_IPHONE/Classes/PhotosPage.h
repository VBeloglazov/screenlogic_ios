//
//  PhotosPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/4/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"

@class CPhotoAlbumRoot;
@class CPhotosListView;
@class CPhotoAlbum;
@class CPhotoGridView;
@class CPhotoFullFrameView;
@class CToolBarButton;

/*============================================================================*/

@interface CPhotosPage : CSubSystemPage     

/*============================================================================*/
{
    BOOL                                m_bInitView;
    CPhotosListView                     *m_pPhotosListView;
    CPhotoGridView                      *m_pPhotosGridView;
    CPhotoGridView                      *_m_pPhotosGridView;
    CPhotoAlbumRoot                     *m_pPhotoAlbumRoot;
    UIActivityIndicatorView             *m_pWaitGraphic;
    CPhotoFullFrameView                 *m_pFullFrameView;
    CPhotoFullFrameView                 *_m_pFullFrameView;
    CToolBarButton                      *m_pGoButton;
    BOOL                                m_bIPODShift;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)NotifyDataReady;
-(void)SetActiveAlbum:(CPhotoAlbum*)pAlbum;
-(void)GoFullFrame:(id)sender;
-(void)ExitFullFrame:(id)sender;
-(void)GridChangeComplete:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;
-(void)ExitFullFrameComplete:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;
-(void)IPodShift:(id)sender;

@end
