//
//  VolumeButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "VolumeButton.h"
#import "AudioZone.h"
#import "CustomPage.h"
#import "MainView.h"

@implementation CVolumeButton

/*============================================================================*/

-(id)initWithZone:(CAudioZone*)pZone ImageName:(NSString*)pImageName

/*============================================================================*/
{
    self = [super initWithFrame:CGRectZero Text:@"" Icon:pImageName];
    
    if (self)
    {
        m_pZone = pZone;
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pZone StopVolume];
    [super dealloc];
}
/*============================================================================*/

-(void)ButtonRelease:(id)sender

/*============================================================================*/
{
    [m_pZone VolReleaseEvent];
    [super ButtonRelease:sender];
}

@end
