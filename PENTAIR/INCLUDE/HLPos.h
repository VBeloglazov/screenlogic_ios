/*
--------------------------------------------------------------------------------

    HLPOS.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLPOS_H
#define HLPOS_H

#include "hlapi.h"

/*============================================================================*/

    class CHLPos

/*============================================================================*/
{
    friend class CHLObList;

protected:
    CHLPos                      *m_pPrev;
    CHLPos                      *m_pNext;
    void                        *m_pObject;


public :
    CHLPos(
    void                        *pObject );

    CHLPos(
    void                        *pObject,
    int                         iSortIndex );

    ~CHLPos();

    CHLPos                      *CreateCopy();

    CHLPos(
    CHLPos                      *pNext,
    CHLPos                      *pPrev,
    void                        *pObject );

//
// Direct access to the Next / Prev members is not thread safe: OK in
// Crystalpad and Minipad, but safer to use the GetAt, GetNext and GetPrev members
// directly from the CHLObList.
//
    void                        Next( CHLPos *pNew );
    void                        Prev( CHLPos *pNew );

    inline CHLPos               *Next()                 { return m_pNext;   }
    inline CHLPos               *Prev()                 { return m_pPrev;   }
    inline void                 *Object()               { return m_pObject; }
    inline void                 Object(void *pNew)      { m_pObject = pNew; }
};

#endif

