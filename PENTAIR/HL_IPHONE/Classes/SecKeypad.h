//
//  SecKeypad.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "hlview.h"


@class CSecPartition;

/*============================================================================*/

@interface CSecKeypad : UIView          < CSink,
                                          CHLCommandResponder >

/*============================================================================*/
{
    CSecPartition                       *m_pPartition;
    NSMutableArray                      *m_pModeButtons;
    NSMutableArray                      *m_pDigitButtons;
    int                                 m_iModeIndex;
    int                                 m_nKeysWanted;
    int                                 m_nKeysEntered;
    int                                 m_Keys[10];
}

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition;

-(void)KeypadMode:(int)iNew;
-(void)EnableKeypadButtons:(BOOL)bNew;

@end
