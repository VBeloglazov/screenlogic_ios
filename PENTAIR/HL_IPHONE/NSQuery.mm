/*
--------------------------------------------------------------------------------

    NSQUERY.MM

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"

#import "nsquery.h"
#import "NSMSG.h"
#import "hlcomm.h"

#include "hlmsg.h"
#include "hlquery.h"
#include "hlserver.h"

@implementation CNSQuery

/*============================================================================*/

- (id) initWithQ:(int)shQ

/*============================================================================*/
{
    if (self == [super init])
    {
        m_pQ = new CHLQuery();
        m_pMSGQ = new CHLMSG(0, shQ);
    }
    return self;
}
/*============================================================================*/

-(void)dealloc 

/*============================================================================*/
{
    delete m_pQ;
    delete m_pMSGQ;

    [super dealloc];
    
    // NOTE: m_pMSGA is owned by the query, don't delete it here
}
/*============================================================================*/

-(void)PutInt:(int)iVal

/*============================================================================*/
{
    m_pMSGQ->PutInt(iVal);
}
/*============================================================================*/

-(void)PutString:(NSString*)sVal

/*============================================================================*/
{
    if (sVal == NULL)
    {
        m_pMSGQ->PutString(_T(""));
        return;
    }

    int iLen = [sVal length];
    CHLString sOut;
    char *pBuf = sOut.GetBufferCHAR(iLen);
    const char *pChars = [sVal UTF8String];
    memcpy(pBuf, pChars, iLen);
    m_pMSGQ->PutString(sOut);
}
/*============================================================================*/

-(void)PutTime:(NSDate*)pDate

/*============================================================================*/
{
    NSDateComponents *pComps = [[NSCalendar currentCalendar] components:0xFFFFFFFF fromDate:pDate];
    m_pMSGQ->PutShort([pComps year]);
    m_pMSGQ->PutShort([pComps month]);
    m_pMSGQ->PutShort(0);
    m_pMSGQ->PutShort([pComps day]);
    m_pMSGQ->PutShort([pComps hour]);
    m_pMSGQ->PutShort([pComps minute]);
    m_pMSGQ->PutShort([pComps second]);
    m_pMSGQ->PutShort(0);

}
/*============================================================================*/

-(int)AskQuestion

/*============================================================================*/
{
    if (!m_pQ->AskQuestion(&m_pMSGA, m_pMSGQ))
        return FALSE;

    return TRUE;
}
/*============================================================================*/

-(void)SendMessage

/*============================================================================*/
{
    g_pHLServer->SendMSG(m_pMSGQ);
}
/*============================================================================*/

-(void)SendMessageWithID:(int)iID

/*============================================================================*/
{
    m_pMSGQ->SenderID(iID);
    g_pHLServer->SendMSG(m_pMSGQ);
}
/*============================================================================*/

-(void)SendMessageWithMyID:(id)pSender

/*============================================================================*/
{
    int iID = [CHLComm SenderID:pSender];
    m_pMSGQ->SenderID(iID);
    g_pHLServer->SendMSG(m_pMSGQ);
}
/*============================================================================*/
    
-(int)GetInt

/*============================================================================*/
{
    if (m_pMSGA == NULL)
        return 0;
    int iVal;
    m_pMSGA->GetInt(&iVal);
    return iVal;
}
/*============================================================================*/
    
-(int)GetBYTE

/*============================================================================*/
{
    if (m_pMSGA == NULL)
        return 0;
    BYTE byVal;
    m_pMSGA->GetBYTE(&byVal);
    return byVal;
}
/*============================================================================*/

-(NSString*)GetString

/*============================================================================*/
{
    if (m_pMSGA == NULL)
        return @"";
 
    CHLString sRet;
    m_pMSGA->GetString(&sRet);

    int iLen = sRet.GetLength();
    char *pBuf = sRet.GetBufferCHAR(iLen);
    for (int i = 0; i < (iLen-1); i++)
    {
        if (pBuf[i] == '\\' && pBuf[i+1] == 'n')
        {
            pBuf[i] = ' ';
            int iLenRem = iLen - i - 2;
            memmove(pBuf+i+1, pBuf+i+2, iLenRem);
            iLen--;
            pBuf[iLen] = 0;
        }
    }

    NSString *pRet = [NSString stringWithCString:pBuf encoding:NSASCIIStringEncoding];
    return pRet;
}
/*============================================================================*/

-(NSString*)GetStringNoFilter

/*============================================================================*/
{
    if (m_pMSGA == NULL)
        return @"";
 
    CHLString sRet;
    m_pMSGA->GetString(&sRet);

    int iLen = sRet.GetLength();
    char *pBuf = sRet.GetBufferCHAR(iLen);

    NSString *pRet = [NSString stringWithCString:pBuf encoding:NSASCIIStringEncoding];
    return pRet;
}
/*============================================================================*/

-(CNSMSG*)CreateNSMSG

/*============================================================================*/
{
    CNSMSG *pRet = [[CNSMSG alloc] initWithMSG:m_pMSGA];
    return pRet;
}
@end

