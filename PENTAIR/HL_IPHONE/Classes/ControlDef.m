//
//  ControlDef.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "ControlDef.h"
#import "nsmsg.h"

#import "VideoStreamView.h"
#import "TVChannelsView.h"
#import "AudioButton.h"
#import "UserButton.h"
#import "HatSwitch.h"
#import "MediaDigitKeypad.h"
#import "keypad.h"
#import "LightControl.h"
#import "KeypadTextView.h"
#import "MediaKeypadView.h"
#import "MediaKeypadButton.h"
#import "DVDSelectorView.h"
#import "weather.h"
#import "Calendar.h"
#import "SysModeButton.h"
#import "MainView.h"
#import "PoolSummaryView.h"
#import "PoolCircuitSummaryView.h"
#import "AudioKeypadListControl.h"

@implementation CControlDef

#define MOVE_LEFT       0
#define MOVE_UPPERLEFT  1
#define MOVE_LOWERLEFT  2
#define MOVE_RIGHT      3
#define MOVE_UPPERRIGHT 4
#define MOVE_LOWERRIGHT 5

/*============================================================================*/

-(id)initWithMSG:(CNSMSG*)pMSG Flags:(int)iFlags

/*============================================================================*/
{
    m_iType     = [pMSG GetInt];
    m_iData     = [pMSG GetInt];
    m_sText     = [[pMSG GetString] retain];
    m_iX        = [pMSG GetInt];
    m_iY        = [pMSG GetInt];
    m_iDX       = [pMSG GetInt];
    m_iDY       = [pMSG GetInt];
    m_RGBText   = [[pMSG GetColor] retain];
    m_RGBFace   = [[pMSG GetColor] retain];
    m_byTextSize    = [pMSG GetBYTE];

    m_byTextAlign   = [pMSG GetBYTE];
    m_byTextQuality = [pMSG GetBYTE];
    m_byBorder      = [pMSG GetBYTE];
    m_byRadius      = [pMSG GetBYTE];
    m_byShadeIn     = [pMSG GetBYTE];
    m_byShadeOut    = [pMSG GetBYTE];
    m_byStyle       = [pMSG GetBYTE];
    
    int byFunc0    = [pMSG GetBYTE];
    int byFunc1    = [pMSG GetBYTE];
    m_wFunction = byFunc1 << 8 | byFunc0;

    [pMSG GetBYTE];
    [pMSG GetBYTE];

    m_sIcon         = [[pMSG GetString] retain];
    m_iFlags        = [pMSG GetInt];


    m_iX = MAX(0, m_iX);
    m_iY = MAX(0, m_iY);
    m_iDX = MIN(m_iDX, 10000);
    m_iDY = MIN(m_iDY, 10000);
    m_iX = MIN(m_iX, 9999-m_iDX);
    m_iY = MIN(m_iY, 9999-m_iDY);
    
    m_iXBL = m_iX;
    m_iYBT = m_iY;
    m_iXBR = m_iX + m_iDX;
    m_iYBB = m_iY + m_iDY; 

    if (m_iFlags & CONTROL_FLAG_DEFAULT_TEXT_SIZE)
    {
        m_byTextSize = [CMainView DEFAULT_TEXT_SIZE];
    }
    
    if (m_iFlags & CONTROL_FLAG_DEFAULT_TEXT_RGB)
    {
        [m_RGBText release];
        m_RGBText = NULL;
    }

    if (m_iFlags & CONTROL_FLAG_DEFAULT_FACE_RGB)
    {
        [m_RGBFace release];
        m_RGBFace = NULL;
    }
    
    if (iFlags & CONTROLDEF_FLAG_SCALETEXT)
    {
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            m_byTextSize = m_byTextSize * 75 / 100;
        }
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_sText release];
    [m_sIcon release];
    [m_RGBText release];
    [m_RGBFace release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetZoneID:(int)iZoneID   { m_iZoneID = iZoneID;  }
-(int)GetZoneID                 { return m_iZoneID;     }

/*============================================================================*/

-(int)Type                      { return m_iType;       }
-(int)Data                      { return m_iData;       }

/*============================================================================*/

-(int)XRight                    { return m_iX + m_iDX;  }
-(int)XLeft                     { return m_iX;          }
-(int)YTop                      { return m_iY;          }
-(int)YBottom                   { return m_iY + m_iDY;  }

-(NSString*)Text                { return m_sText;       }
-(NSString*)Icon                { return m_sIcon;       }
-(int)Flags                     { return m_iFlags;      }
-(int)Style                     { return m_byStyle;     }
-(int)TextSize                  { return m_byTextSize;  }
-(int)TextAlignment             { return m_byTextAlign; }
-(int)ShadeIn                   { return m_byShadeIn;   }
-(int)ShadeOut                  { return m_byShadeOut;  }
-(int)Radius                    { return m_byRadius;    }
-(UIColor*)RGBFace              { return m_RGBFace;     }
-(UIColor*)RGBText              { return m_RGBText;     }
-(int)Function                  { return m_wFunction;   }


/*============================================================================*/

-(void)SetXLeft:(int)iX         { m_iDX += (m_iX - iX); m_iX = iX;  }
-(void)SetXRight:(int)iX        { m_iDX = (iX - m_iX);              }
-(void)SetYTop:(int)iY          { m_iDY += (m_iY - iY); m_iY = iY;  }
-(void)SetYBottom:(int)iY       { m_iDY = (iY - m_iY);              }

/*============================================================================*/

-(CGRect)RectInBounds:(CGRect)rect

/*============================================================================*/
{
    int iDXR = rect.size.width;
    int iDYR = rect.size.height;
    int iXOut = m_iX * iDXR / 10000;
    int iYOut = m_iY * iDYR / 10000;
    int iDXOut = m_iDX * iDXR / 10000;
    int iDYOut = m_iDY * iDYR / 10000;

    CGRect rView = CGRectMake(iXOut, iYOut, iDXOut, iDYOut);
    return rView;
}
/*============================================================================*/

-(UIView*)CreateView:(CGRect)rect

/*============================================================================*/
{
    CGRect rView = [self RectInBounds:rect];
    UIView *pView = NULL;
    CHLButton *pThisButton = NULL;

        
    switch(m_iType)
    {
    case CONTROL_STATICTEXT:
    case CONTROL_3DTEXT:
        {
            UILabel *pLabel = [[UILabel alloc] initWithFrame:rView];
            pView = pLabel;

            pLabel.textColor = [UIColor whiteColor];
            pLabel.backgroundColor = [UIColor clearColor];
            int iTextSize = [CMainView DEFAULT_TEXT_SIZE];
            if ((m_iFlags & CONTROL_FLAG_DEFAULT_TEXT_SIZE) == 0)
                iTextSize = m_byTextSize;
                
            [CCustomPage InitStandardLabel:pLabel Size:iTextSize];

            if (m_RGBText != NULL)
                pLabel.textColor = m_RGBText;
            [pLabel setText:m_sText];
            
            if (m_byTextAlign & HL_HCENTER)
            {
                [pLabel setTextAlignment:UITextAlignmentCenter];
            }
            else if (m_byTextAlign & HL_HRIGHT)
            {
                [pLabel setTextAlignment:UITextAlignmentRight];
            }
            else
            {
                [pLabel setTextAlignment:UITextAlignmentLeft];
            }

            [pLabel setLineBreakMode:UILineBreakModeWordWrap];
            [pLabel setNumberOfLines:2];
        }
        break;

    case CONTROL_REGION:
        {
            pView = [[CRoundRegion alloc] initWithFrame:rView FrameStyle:FRAMESTYLE_DEFAULT RadiusStyle:RADIUSSTYLE_DEFAULT];
        }
        break;


    case CONTROL_HOMEPAGE_BUTTON:
    case CONTROL_HOMEPAGE_BUTTON_TOGGLE:
    case CONTROL_HOMEPAGE_BUTTON_MOMENTARY:
        {
            pThisButton = [[CUserButton alloc] initWithFrame: CGRectZero 
                                                            IconFormat:m_byTextAlign
                                                            Style:m_byStyle 
                                                            Text:m_sText 
                                                            TextSize:m_byTextSize 
                                                            TextColor:m_RGBText 
                                                            Color:m_RGBFace 
                                                            Icon:m_sIcon 
                                                            Data:m_iData
                                                            Type:m_iType ];
            pView = pThisButton;
        }
        break;

    case CONTROL_WEATHER_FORECAST:
        {
            pView = [[CWeatherForecastView alloc] initWithFrame:rView];
        }
        break;

    case CONTROL_CALENDAR:
        {
            pView = [[CCalendarControl alloc] initWithFrame:rView];
        }
        break;


    case CONTROL_SYSTEMMODE_0:
    case CONTROL_SYSTEMMODE_1:
    case CONTROL_SYSTEMMODE_2:
        {
            int iMode = (m_iType-CONTROL_SYSTEMMODE_0);

            pView = [[CSysModeButton alloc] initWithFrame:  rView 
                                                            IconFormat:m_byTextAlign
                                                            Style:m_byStyle 
                                                            Text:m_sText 
                                                            TextSize:m_byTextSize 
                                                            TextColor:m_RGBText 
                                                            Color:m_RGBFace 
                                                            Mode:iMode  ];
        }
        break;

    case CONTROL_SYSTEMMODE_3:
    case CONTROL_SYSTEMMODE_4:
    case CONTROL_SYSTEMMODE_5:
    case CONTROL_SYSTEMMODE_6:
    case CONTROL_SYSTEMMODE_7:
    case CONTROL_SYSTEMMODE_8:
    case CONTROL_SYSTEMMODE_9:
        {
            int iMode = (m_iType-CONTROL_SYSTEMMODE_3+3);
            pThisButton = [[CSysModeButton alloc] initWithFrame:  rView 
                                                            IconFormat:m_byTextAlign
                                                            Style:m_byStyle 
                                                            Text:m_sText 
                                                            TextSize:m_byTextSize 
                                                            TextColor:m_RGBText 
                                                            Color:m_RGBFace 
                                                            Mode:iMode  ];
            pView = pThisButton;
        }
        break;
    
    case CONTROL_POOL_SUMMARY:
        {
            pView = [[CPoolSummaryView alloc] initWithFrame: rView];
        }
        break;

    case CONTROL_POOL_CIRSUMMARY:
        {
            pView = [[CPoolCircuitSummaryView alloc] initWithFrame: rView];
        }
        break;

    default:
        break;
    }
    
    if (pThisButton != NULL)
    {
        if ((m_iFlags & CONTROL_FLAG_DEFAULT_RADIUS) == 0)
            [pThisButton SetROutside:m_byRadius];
        if ((m_iFlags & CONTROL_FLAG_DEFAULT_SHIN) == 0)
            [pThisButton SetShadeIn:m_byShadeIn];
        if ((m_iFlags & CONTROL_FLAG_DEFAULT_SHOUT) == 0)
            [pThisButton SetShadeOut:m_byShadeOut];
    }

    return pView;
}
/*============================================================================*/

-(BOOL)IntersectsWith:(CControlDef*) pDef

/*============================================================================*/
{
    if ([pDef YTop] > [self YBottom])
        return FALSE;
    if ([pDef YBottom] < [self YTop])
        return FALSE;
    if ([pDef XLeft] > [self XRight])
        return FALSE;
    if ([pDef XRight] < [self XLeft])
        return FALSE;

    return TRUE;
}
/*============================================================================*/

-(BOOL)WantScrollLock

/*============================================================================*/
{
    switch(m_iType)
    {
    case CONTROL_LIGHT_DIMMER:
    case CONTROL_TVCHANNELS1:
    case CONTROL_TVCHANNELS2:
        return TRUE;
    default:
        break;
    }
    return FALSE;
}
/*============================================================================*/

-(UIView*)GetView

/*============================================================================*/
{
    return m_pView;
}
/*============================================================================*/

-(void)SetView:(UIView*)pView

/*============================================================================*/
{
    m_pView = pView;
}
@end
