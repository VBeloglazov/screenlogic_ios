//
//  AudioButton.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hlbutton.h"
#import "HLView.h"

#define CONTROL_FLAG_HATBUTTON          0x00010000

/*============================================================================*/

@interface CAudioButton :  CHLButton < CHLCommandResponder >

/*============================================================================*/
{
    int                                 m_iZoneID;
    int                                 m_iData;
    int                                 m_iFunction;
}

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Icon:(NSString*)sIcon 
                    Data:(int)iData
                    Flags:(int)iFlags
                    Function:(int)iFunction
                    ZoneID:(int)iZoneID;

-(void)OnCommand:(int)iCommandID;

@end
