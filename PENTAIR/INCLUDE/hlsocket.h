/*
--------------------------------------------------------------------------------

    HLSOCKET.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLSOCKET_H
#define HLSOCKET_H

#include "hltime.h"

class CStatusSink;

#if defined(MINIPAD) | defined(POOLCONFIG) | defined(HLSURVEY)
    #define DYNAMIC_BUFFER
#endif
#ifdef MULTI_SERVER
    class CHLServer;
#endif

#ifdef CRYSTALPAD
    #if defined(HL_ARMV4IA) | defined(HL_ARMV4IC)
        #define HL_TOUCHSCREEN
    #endif
#endif

#define SIZE_TX_BUFFER          4096
#define TIME_TX_BUFFER          50

/*============================================================================*/

    class CMessageTracker

/*============================================================================*/
{
public:
    virtual void                ProcessMessageStatus(
    short                       shCurrentMessageID,
    short                       shCurrentSenderID,
    DWORD                       dwNBytesIn,
    DWORD                       dwNBytesMessage );    

    virtual void                ProcessMessage(
    CHLString                   sMSG );
};
/*============================================================================*/

    class CHLSocket

/*============================================================================*/
{
public:
    #ifdef GATEWAY
        // These are publicly accessible from gencom
        int                         m_iNTotalBytesIn;       // Bytes received from server
        int                         m_iNTotalBytesOut;      // Bytes sent to server
        int                         m_iNMessagesIn;         // Incoming messages
        int                         m_iNMessagesOut;        // Outgoing messages
    #endif

protected:
    #ifdef MULTI_SERVER
        CHLServer               *g_pHLServer;
    #endif

    // For Locating Gateway
    CHLTimer                    m_hltPingCreate;
    CHLTimer                    m_hltPing;
    SOCKET                      m_hPing;
    BOOL                        m_bAltPing;

    SOCKET                      m_hSocket;              // Always INVALID_SOCKET if not connected
    BOOL                        m_bDisconnectOnTimeout; // Reset connection if we timeout
    BOOL                        m_bTCPNoDelay;          // TCP_NODELAY flag
    BOOL                        m_bEnableKeepAlive;     // Check for keepalives
    CHLTimer                    m_hltTX;

    HLT_HEADER                  m_RXHeader;
    int                         m_iNRXHeader;
    CHLMSG                      *m_pRXMSG;
    int                         m_iNRXMSG;

    #ifndef GATEWAY
        // Protected for everyone else
        int                         m_iNTotalBytesIn;       // Bytes received from server
        int                         m_iNTotalBytesOut;      // Bytes sent to server
        int                         m_iNMessagesIn;         // Incoming messages
        int                         m_iNMessagesOut;        // Outgoing messages
    #endif

    int                         m_iNPartialHeaderRX;    // Total partial headers
    int                         m_iNPartialRX;          // Total partial receives
    int                         m_iNMultipleRX;         // Total multi-message receives
    int                         m_iNMultiPacketRX;      // Total multiple-packet messages
    int                         m_iNQATickCount;        // Total ticks waiting for answers

    CHLTimer                    m_hltLastKeepAlive;     // Time since last keepalive packet
    CHLTimer                    m_hltLastTimeUpdate;    // Time since we synched our time to the gateway
    CHLTimer                    m_hltLastReceive;       // Last time we got bytes
    BOOL                        m_bFirstKeepAlive;      // TRUE until AFTER the first keep-alive is received    

    int                         m_iNDrops;              // Number of times we've dropped

    DWORD                       m_dwKeepAliveTimeout;
    DWORD                       m_dwLastCounter;

    BOOL                        m_bServerNotify;

    CMessageTracker             *m_pMessageTracker;

    #ifdef IPHONE
        CHLTimer                m_hltLastPing;
        BOOL                    m_bWirelessOK;
    #endif

    BYTE                        m_TXBuffer[SIZE_TX_BUFFER];
    int                         m_iNTXBuffer;
    CHLTimer                    m_hltTXBuffer;

#ifdef NO_DISCONNECT
    CHLTimer                    m_hltDebug;
#endif

public:
    #ifdef MULTI_SERVER
        CHLSocket(CHLServer *pServer);
    #else
        CHLSocket();
    #endif

    virtual ~CHLSocket();

    void                        Socket(SOCKET hSocket);
    inline SOCKET               Socket()                    { return m_hSocket;             }
    inline void                 ServerNotify(BOOL bNew)     { m_bServerNotify = bNew;       }

    inline BOOL                 EnableKeepAlive()           { return m_bEnableKeepAlive;    }
    inline void                 EnableKeepAlive(BOOL bNew)  { m_bEnableKeepAlive = bNew;    }

    BOOL                        IsConnected();

    BOOL                        Connect(
    char                        *pName,
    CStatusSink                 *pSink );

    BOOL                        ConnectAndLogin(
    SOCKADDR_IN                 *pAddress,
    BOOL                        bLocalLogin,
    SOCKET                      hRelayServer = INVALID_SOCKET,
    int                         iConType = -1,
    CStatusSink                 *pSink = NULL );

    //
    // Crystalpad ping gateway
    //
    BOOL                        FindGateway(
    SOCKADDR_IN                 *pAddress,
    char                        *pName,
    CStatusSink                 *pSink );
    
    void                        Disconnect();

    BOOL                        DisconnectOnTimeout();
    void                        DisconnectOnTimeout( BOOL bNew );

    BOOL                        TCPNoDelay();
    void                        SetTCPNoDelay( BOOL bNew );

    inline CMessageTracker      *MessageTracker()                           { return m_pMessageTracker; }
    inline void                 MessageTracker(CMessageTracker *pNew)       { m_pMessageTracker = pNew; }

    #ifdef IPHONE
        inline BOOL             WirelessOK()                                { return m_bWirelessOK;     }
    #endif

    int                         NDrops();
    void                        ZeroCounts();

    BOOL                        IsReadyToSend();

    BOOL                        Send(
    int                         iNBytes,
    BYTE                        *pBytes );

    CHLMSG                      *WaitMessage(DWORD dwMaxWaitForData);
    BOOL                        CheckTX();
    BOOL                        FlushTXBuffer();    

    BOOL                        KeepAliveTimeout();

    DWORD                       LastReceive()   { return m_hltLastReceive.HLTickCount();    }
};

#endif

