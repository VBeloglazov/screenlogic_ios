//
//  Calendar.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/28/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "Calendar.h"
#import "HLTableFrame.h"
#import "crect.h"
#import "TableTitleHeaderView.h"
#import "MainView.h"
#import "RoundRegion.h"
#import "CustomPage.h"

#define HEADER_SIZE         40

@implementation CCalendarControl

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame

/*============================================================================*/
{
    self = [super initWithFrame:rFrame Title:@"Calendar"];
    if (self)
    {
        CCalendarView *pView = [[CCalendarView alloc] initWithFrame:CGRectZero Control:self];
        [m_pRgn2 SetClientView:pView];
        [pView release];
    }
    return self;
}
@end

@implementation CCalendarView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Control:(CSplitRoundRegion*)pParentRegion;

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        m_pParentRegion = pParentRegion;
        m_pDays = [[NSMutableArray alloc] init];

        NSDate *pToday = [NSDate date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];    
        unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit| NSDayCalendarUnit| NSWeekdayCalendarUnit;
        NSDateComponents *pComponents = [gregorian components:unitFlags fromDate:pToday];
        m_iYear = [pComponents year];
        m_iMonth = [pComponents month];

        m_iYearToday = m_iYear;
        m_iMonthToday = m_iMonth;
        m_iDayToday = [pComponents day];
        m_iDayOfWeekToday = [pComponents weekday];

        [gregorian release];

        [self Init];
    }

    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pDays release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    
    if ([m_pDays count] > 0)
    {
        CDayView *pLast = (CDayView*)[m_pDays objectAtIndex:[m_pDays count]-1];
        int iNRows = [pLast Row]+1;
        [CRect ConfineToAspect:&rThis DXDY:7.0 / (float)iNRows];
        int iDXDay = rThis.size.width / 7;
        int iDYDay = rThis.size.height / iNRows;
        int iXOrg = rThis.origin.x;
        int iYOrg = rThis.origin.y;
        for (int i = 0; i < [m_pDays count]; i++)
        {
            CDayView *pDay = (CDayView*)[m_pDays objectAtIndex:i];
            CGRect rDay = CGRectMake(iXOrg + [pDay Column] * iDXDay, iYOrg + [pDay Row] * iDYDay, iDXDay, iDYDay);
            [CRect Inset:&rDay DX:2 DY:2];
            [pDay setFrame:rDay];
        }
    }

    [super layoutSubviews];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
}
/*============================================================================*/

-(void)Init

/*============================================================================*/
{
    NSString *psMonth = NULL;
    switch (m_iMonth)
    {
    case 1: psMonth = @"January";   break;
    case 2: psMonth = @"February";  break;
    case 3: psMonth = @"March";     break;
    case 4: psMonth = @"April";     break;
    case 5: psMonth = @"May";       break;
    case 6: psMonth = @"June";      break;
    case 7: psMonth = @"July";      break;
    case 8: psMonth = @"August";    break;
    case 9: psMonth = @"September"; break;
    case 10: psMonth = @"October";  break;
    case 11: psMonth = @"November"; break;
    case 12: psMonth = @"December"; break;
    }

    NSString *psTitle = [NSString stringWithFormat:@"%s %d", [psMonth UTF8String], m_iYear];
    [m_pParentRegion SetTitle:psTitle];

    [m_pDays removeAllObjects];
    
    NSDateComponents *components = [[[NSDateComponents alloc]init] autorelease];
    [components setMonth:m_iMonth];
    [components setDay:1];
    [components setYear:m_iYear];

    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *myDate = [gregorian dateFromComponents:components];
    NSRange range = [gregorian rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:[gregorian dateFromComponents:components]];

    //Now get the day of the week the 1st day of this month falls on (ie. 6 Friday)
    unsigned uFlags = NSWeekdayCalendarUnit;
    NSDateComponents *comp2 = [gregorian components:uFlags fromDate:myDate];
    int iStartDayOfWeek = [comp2 weekday];
    int iNDays = range.length;

    for (int iDay = 0; iDay < 7; iDay++)
    {
        NSString *psText = NULL;
        switch (iDay)
        {
        case 0: psText = @"S"; break;
        case 1: psText = @"M"; break;
        case 2: psText = @"T"; break;
        case 3: psText = @"W"; break;
        case 4: psText = @"T"; break;
        case 5: psText = @"F"; break;
        case 6: psText = @"S"; break;
        }
        BOOL bToday = (iDay == m_iDayOfWeekToday-1);

        CDayView *pView = [[CDayView alloc] initWithFrame:CGRectZero Text:psText IsToday:bToday Column:iDay Row:0];
        [self addSubview:pView];
        [m_pDays addObject:pView];
        [pView release];
    }

    [gregorian release];

    int iCol = iStartDayOfWeek - 1;
    int iRow = 1;

    for (int iDay = 1; iDay <= iNDays; iDay++)
    {
        BOOL bToday = TRUE;
        if (iDay != m_iDayToday)    bToday = FALSE;
        if (m_iYear != m_iYearToday) bToday = FALSE;
        if (m_iMonth != m_iMonthToday) bToday = FALSE;
        
        NSString *psText = [NSString stringWithFormat:@"%d", iDay];

        CDayView *pView = [[CDayView alloc] initWithFrame:CGRectZero Text:psText IsToday:bToday Column:iCol Row:iRow];
        iCol++;
        if (iCol >= 7)
        {
            iRow++;
            iCol = 0;
        }
        [self addSubview:pView];
        [m_pDays addObject:pView];
        [pView release];

    }

//    NSLog(@"Date: %@, days in this month: %d, starts on weekday:%i", myDate,range.length, [comp2 weekday] );}
}
@end


@implementation CDayView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Text:(NSString*)psText IsToday:(BOOL)bIsToday Column:(int)iCol Row:(int)iRow

/*============================================================================*/
{
    if (self == [super initWithFrame:rect])
    {
        m_iCol = iCol;
        m_iRow = iRow;

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        m_pLabel.backgroundColor = [UIColor clearColor];
        m_pLabel.textColor = [UIColor whiteColor];

        if (!bIsToday)
        {
            m_pLabel.alpha = 0.5;
        }

        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        m_pLabel.textAlignment = UITextAlignmentCenter;

        [m_pLabel setText:psText];

        [self addSubview:m_pLabel];
        [m_pLabel release];
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pLabel setFrame:self.bounds];
}
/*============================================================================*/

-(int)Column            { return m_iCol;    }
-(int)Row               { return m_iRow;    }

/*============================================================================*/
@end

