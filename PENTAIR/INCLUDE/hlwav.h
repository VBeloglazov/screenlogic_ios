/*
--------------------------------------------------------------------------------

    HLWAV.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#ifndef HLWAV_H
#define HLWAV_H

#ifndef IPHONE
#include "mmreg.h"
#endif

#include "socksink.h"

/*============================================================================*/

    class CHLWAV : public CSocketSink

/*============================================================================*/
{
protected:
    CHLString                   m_sFileName;

    BOOL                        m_bDataOK;
    BOOL                        m_bError;

#ifndef IPHONE
    WAVEFORMATEX                m_WaveFormat;
#endif

    BYTE                        *m_pWAVData;
    int                         m_iNWAVData;

public:
    CHLWAV(
    CHLString                   sName );

    virtual ~CHLWAV();

    // CSocketSink overrides
    virtual void                SinkMessage(
    CHLMSG                      *pMSG );

    virtual void                ConnectionChanged(
    BOOL                        bNowConnected );
    
    // CHLWAV methods
    CHLString                   Name();

    BOOL                        DataOK();
    BYTE                        *Data();
    int                         NBytesData();

#ifndef IPHONE
    WAVEFORMATEX                WaveFormat();

    BOOL                        Play(
    BOOL                        bAnnouncVolume = FALSE);
#endif
};

#endif
