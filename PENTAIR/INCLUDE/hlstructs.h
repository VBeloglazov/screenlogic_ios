/*
--------------------------------------------------------------------------------

    HLSTRUCTS.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

These structures are used to pass message parameters and data back and forth
between the tablet and the server.

IMPORTANT: enums are 4 bytes: they are treated like ints on both embedded VC++
           and VC.Net.

The data from the gateway goes to the C++ application CRYSTALPAD and also to the
java applet on another PC. To minimize confusion, we have tried to use types here
that are the same on all those targets.

Note also that default settings in VC++ packs structures to 4 byte boundary, so
we need to make sure all our HLT_ structures are a multiple of 4 bytes.

Also, within each structure make sure to pad variables such that they start on boundaries
of the same size.  For example ints must start on 4 byte boundaries, WORDs must start on
2 byte boundaries.

--------------------------------------------------------------------------------
*/
#ifndef HLSTRUCTS_H
#define HLSTRUCTS_H

#include "audiostruct.h"
#include "hlerror.h"
#include "hlmsgs.h"


//
// General messages.
//
// IMPORTANT: Make sure structure size is a multiple of 4 bytes so that calls to
//            sizeof report the correct size. Recall that enums are 4 bytes.
//

typedef struct
{
    int                         m_iMessageID;           // Bytes 0-3
    DWORD                       m_dwIP;                 // Bytes 4-7
    WORD                        m_wPort;                // Bytes 8-9
    BYTE                        m_cGatewayType;         // Byte 10
    BYTE                        m_cGatewaySubType;      // Byte 11
    char                        m_cGatewayName[28];     // Bytes 12-39
} HLT_PINGA;

typedef struct
{
    int                         m_iMessageID;           // Bytes 0-3
    DWORD                       m_dwIP;                 // Bytes 4-7
    WORD                        m_wPort;                // Bytes 8-9
    BYTE                        m_cGatewayType;         // Byte 10
    BYTE                        m_cGatewaySubType;      // Byte 11
    char                        m_cGatewayName[28];     // Bytes 12-39
    char                        m_cGatewayMAC[12];      // Bytes 40-51
    char                        m_cDescription[128];    // Bytes 52-179
} HLT_PING51A;

typedef struct
{
    short                       m_shSenderID;      // Sink originating message
    short                       m_shMessageID;     // ID
    int                         m_iNBytes;         // Data
} HLT_HEADER;

typedef struct
{
    int                         m_iInsetL;
    int                         m_iInsetR;
} HLT_INSETS;

typedef struct
{
    short                       m_shSenderID;
    short                       m_shMessageID;
    int                         m_iNBytes;

    short                       m_shYear;
    short                       m_shMonth;
    short                       m_shDayOfWeek;
    short                       m_shDay;
    short                       m_shHour;
    short                       m_shMin;
    short                       m_shSec;
    short                       m_shMSec;

    int                         m_iTypeInfo;
} HLT_LOG_HEADER;

typedef struct
{
    short                       m_shYear;   
    short                       m_shMonth;  
    short                       m_shDay;    
    short                       m_shPad;
    UINT                        m_lOffset;  
} HLT_LOGINDEX;

typedef struct
{
    int                         m_iInt1;
    int                         m_iInt2;
} HLT_INT_INT;

typedef struct
{
    int                         m_iInt1;
    int                         m_iInt2;
    int                         m_iInt3;
} HLT_INT_INT_INT;

typedef struct
{
    int                         m_iInt1;
    int                         m_iInt2;
    int                         m_iInt3;
    int                         m_iInt4;
} HLT_INT_INT_INT_INT;


//
// Date/Time structures
//
typedef struct
{
    WORD                        m_wYear;
    WORD                        m_wMonth;
    WORD                        m_wDayOfWeek;
    WORD                        m_wDay;
    WORD                        m_wHour;
    WORD                        m_wMinute;
    WORD                        m_wSecond;
    WORD                        m_wMSecond;
} HLT_SYSTEMTIME;


//
// UPS structures
//
typedef struct
{
    int                         m_iUPSState;        // Current HVAC state
    double                      m_dVACInput;        // Line voltage
    double                      m_dVACOutput;       // Output voltage
    double                      m_dVDCBattery;      // Battery voltage
    double                      m_dACAmps;          // Output amps
    int                         m_iFT;              // Current temp
} HLT_UPS_STATE;


typedef struct
{
    int                         m_iIndex;           // HVAC index
    int                         m_iProgramState;    // New program state
} HLT_HVAC_COMMAND;

typedef struct
{
    int                         m_iUnitID;          // Unit ID
    int                         m_iHVACState;       // New HVAC state
    int                         m_iFanState;        // New fan state
} HLT_HVAC_UNITCOMMAND;


//
// Communications structures.
//
typedef struct
{
    int                         m_iTotalSent;
    int                         m_iTotalRecv;
    int                         m_iNSends;
    int                         m_iNRecvs;
    int                         m_iSendErrors;
    int                         m_iRecvErrors;
    int                         m_iConnAttepts;
    int                         m_iConnErrors;
} HLT_STAT_INFO;

#define PLAYBACKINFO_BYTESINBUFFER_VALID    0x00000001
#define PLAYBACKINFO_BYTESDECODED_VALID     0x00000002

typedef struct
{
    int                         m_iFlags;
    DWORD                       m_dwBytesInBuffer;
    DWORD                       m_dwBytesDecoded;

} HLT_PLAYER_PLAYBACKINFO;

typedef struct
{
    //
    //  NetToGateway    SerialToDevice
    //  FALSE           FALSE       Not allowed
    //  FALSE           TRUE        Device plugs directly into gateway
    //  TRUE            FALSE       Device is on network, uses TCPIP
    //  TRUE            TRUE        Device is on network via Ethernet to RS-232/485 bridge
    //
    char                        m_chNetToGateway;   // The gateway finds the device via network
    char                        m_chSerialToDevice; // The device itself uses RS-232/485

    short                       m_shPadding;        // Push out to 4 Byte Boundary

    short                       m_shAd1;            // IP address (to bridge, for serial dev)
    short                       m_shAd2;            // (normal order: 192.168...)
    short                       m_shAd3;
    short                       m_shAd4;
    int                         m_iNetPort;         // Network port

    // Required for serial connection to device.
    int                         m_iCOMPort;
    int                         m_iProtocol;        // (see gencom.h)
    int                         m_iBaudRate;        // Actual number
    int                         m_iFlowC;           // (see gencom.h)
    int                         m_iParity;          // (see gencom.h)
    int                         m_iDataBits;        // 7 or 8
    int                         m_iStopBits;        // 1 or 2
} HLT_COM_INFO;


typedef struct _CPingBrickPacket
{
    DWORD                       m_lIPAddress;   // 4
    DWORD                       m_lNetmask;     // 8
    DWORD                       m_lGateway;     // 12
    DWORD                       m_lDNS;         // 16
    BYTE                        m_byMAC[6];     // 22
    char                        m_Name[32];     // 54
    char                        m_Version[36];  // 90
    char                        m_cType;        // 91
    char                        m_cSubType;     // 92
    short                       m_iPTProto;     // 94
    unsigned short              m_iPTBaud;      // 96
    char                        m_cFlowC;       // 97
    char                        m_cDataBits;    // 98
    char                        m_cStopBits;    // 99
    char                        m_cParity;      // 100
} HLT_PING_BRICK_PACKET;



#endif

