/*
--------------------------------------------------------------------------------

    HLWAV.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "socksink.h"
#include "hlserver.h"
#include "hlmsg.h"

#ifndef IPHONE

#include "main.h"
#include "helpers.h"

#ifdef HLCONFIG
    #include "mmsystem.h"
#endif

#ifdef CRYSTALPAD
    #include "..\crystalpad\caplayer.h"
    #include "..\crystalpad\options.h"
#endif
#ifdef HLOSD
    #include "..\hlosd\caplayer.h"
    #include "..\hlosd\options.h"
#endif

#endif

#include "hlwav.h"

/*============================================================================*/

    CHLWAV::CHLWAV(
    CHLString                   sName )

/*============================================================================*/
{
    m_sFileName     = sName;

    m_bDataOK       = FALSE;
    m_bError        = FALSE;
    m_pWAVData      = NULL;
    m_iNWAVData     = 0;

#ifndef IPHONE
    memset(&m_WaveFormat, 0, sizeof(WAVEFORMATEX));
#endif

    g_pHLServer->AddSocketSink(this);
    ConnectionChanged(TRUE);
}
/*============================================================================*/

    CHLWAV::~CHLWAV()

/*============================================================================*/
{
    if (!m_bDataOK)
        g_pHLServer->RemoveSocketSink(this);
    delete [] m_pWAVData;
}
/*============================================================================*/

    void                        CHLWAV::SinkMessage(
    CHLMSG                      *pMSG )

/*============================================================================*/
{
    // Remove ourselves: once we get the file we no longer get notifications.
    if (pMSG->SenderID() == SenderID())
        g_pHLServer->RemoveSocketSink(this);

    // Check this message.
    if (pMSG->MessageID() == HLM_WAVSERV_GETWAVFILEA)
    {
#ifdef IPHONE
        delete [] m_pWAVData;
        m_iNWAVData = pMSG->DataSize();
        m_pWAVData = new BYTE[m_iNWAVData];

        m_bError = FALSE;
        m_bDataOK = TRUE;

        pMSG->GetData(m_pWAVData, m_iNWAVData);
#else
        m_bError = TRUE;
        int iNData = pMSG->DataSize();
        BYTE *pFile = new BYTE[iNData];
        m_bError = TRUE;
        m_bDataOK = TRUE;

        if (pFile == NULL)
            return;

        pMSG->GetData(pFile, iNData);
        memcpy(&m_WaveFormat, pFile + 20, 16);
        m_WaveFormat.cbSize = 0;

        // Now get the data: first read data chunk header to get the size...
        DWORD dwLength = 0;
        int iReadIndex = 36;
        for (iReadIndex=36; iReadIndex<48; iReadIndex++)
        {
            // search for 'd','a','t','a'
            if (pFile[iReadIndex+0] == 'd')
            {
                if ((pFile[iReadIndex+1] == 'a') && 
                    (pFile[iReadIndex+2] == 't') && 
                    (pFile[iReadIndex+3] == 'a'))
                {
                    iReadIndex+=4;
                    dwLength = FOURCCToDWORD((char*)(pFile+iReadIndex));
                    iReadIndex+=4;
                    break;
                }
            }
            // search for "fact<dwFileSize:DWORD>"
            if (pFile[iReadIndex+0] == 'f')
            {
                if ((pFile[iReadIndex+1] == 'a') && 
                    (pFile[iReadIndex+2] == 'c') && 
                    (pFile[iReadIndex+3] == 't'))
                {
                    iReadIndex+=4;
                    dwLength = FOURCCToDWORD((char*) (pFile+iReadIndex+12));
                    iReadIndex+=16;
                    break;
                }
            }
        }

        if (dwLength == 0)
        {
            delete [] pFile;
            return;
        }

        dwLength = __min((int)dwLength, iNData - iReadIndex);        
        m_pWAVData = new BYTE[dwLength];
        memcpy(m_pWAVData, pFile+iReadIndex, dwLength);
        m_iNWAVData = dwLength;

        m_bError = FALSE;
        delete [] pFile;
#endif
    }

    return;
}
/*============================================================================*/

    void                        CHLWAV::ConnectionChanged(
    BOOL                        bNowConnected )

/*============================================================================*/
{
    // Normally, this only gets called from the constructor, though it is
    // possible for the connection to go away before we get our response, in
    // which case we'll come in here again and re-ask for our file.
    if (bNowConnected)
    {
        CHLMSG msgQ(SenderID(), HLM_WAVSERV_GETWAVFILEQ);
        msgQ.PutString(m_sFileName);
        g_pHLServer->SendMSG(&msgQ);
    }
}
/*============================================================================*/

    CHLString                   CHLWAV::Name()

/*============================================================================*/
{
    return m_sFileName;
}
/*============================================================================*/

    BOOL                        CHLWAV::DataOK()

/*============================================================================*/
{
    return m_bDataOK;
}
/*============================================================================*/

    BYTE                        *CHLWAV::Data()

/*============================================================================*/
{
    return m_pWAVData;
}
/*============================================================================*/

    int                         CHLWAV::NBytesData()

/*============================================================================*/
{
    return m_iNWAVData;
}
#ifndef IPHONE
/*============================================================================*/

    BOOL                        CHLWAV::Play(
    BOOL                        bAnnounceVolume )

/*============================================================================*/
{
    if (m_bError)
        return FALSE;
    if (!m_bDataOK)
        return FALSE;

    #ifdef NO_SOUNDEFFECTS
        return TRUE;
    #endif

    #ifdef HLCONFIG
        return TRUE;
    #elif DDTEST
        return TRUE;
    #else
        int iVolume =  g_pMainWindow->Options()->DefaultVolume();
        if (bAnnounceVolume)
            iVolume =  g_pMainWindow->Options()->AnnounceVolume();

        if (!g_pPlayer->PrepareToPlay(&m_WaveFormat, iVolume))
            return FALSE;
        g_pPlayer->AutoCloseWaveOut(TRUE);
        g_pPlayer->PlayData(m_pWAVData, m_iNWAVData);
    #endif

    return TRUE;
}
#endif



