//
//  TabRoundRegion.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "TabRoundRegion.h"
#import "hlsegment.h"
#import "RoundRegion.h"
#import "crect.h"
#import "HLComm.h"
#import "MainView.h"
#import "hlm.h"
#import "iconview.h"

/*============================================================================*/

@implementation CTabRoundRegion

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame

/*============================================================================*/
{
    self = [super initWithFrame:frame Style:SRREGION_SMALL_HEADER];
    if (self) 
    {
        // Initialization code
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)InitPageWithStrings:(NSMutableArray*)pTabs

/*============================================================================*/
{
    m_pPageCtrl = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:m_pRgn1 Items:pTabs Type:HLSEGMENT_PAGE_CTRL_ROUNDREGION Data:NO];
    [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
    [m_pPageCtrl SetSelectedIndex:0];
    [m_pRgn1 SetClientView:m_pPageCtrl];
    [m_pPageCtrl release];
    id idPage = m_pPageCtrl;
    [self SelectPage:idPage];
}
/*============================================================================*/

-(void)InitPageWithViews:(NSMutableArray*)pTabs

/*============================================================================*/
{
    m_pPageCtrl = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:m_pRgn1 Views:pTabs Type:HLSEGMENT_PAGE_CTRL_ROUNDREGION];
    [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
    [m_pPageCtrl SetSelectedIndex:0];
    [m_pRgn1 SetClientView:m_pPageCtrl];
    [m_pPageCtrl release];
    id idPage = m_pPageCtrl;
    [self SelectPage:idPage];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rFrame = self.bounds;
    printf("TabRoundRegion %f %f\r\n", rFrame.size.width, rFrame.size.height);


    [super layoutSubviews];
    [m_pPageCtrl Layout:m_iLayout];
}
/*============================================================================*/

-(void)SelectPage:(id)sender

/*============================================================================*/
{
    CHLSegment *pCtrl = (CHLSegment*)sender;
    int iIndex = [pCtrl SelectedIndex];

    int iDir = DIR_RIGHT;
    if (iIndex  > m_iLastIndex)
        iDir = DIR_LEFT;
    m_iLastIndex = iIndex;

    UIView *pNewPage = [self CreatePageForIndex:iIndex];
    [m_pRgn2 SetClientViewAnimated:pNewPage Direction:iDir];
}
/*============================================================================*/

-(UIView*)CreatePageForIndex:(int)iIndex

/*============================================================================*/
{
    return NULL;
}
@end
