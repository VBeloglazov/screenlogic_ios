//
//  MediaZonePage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import "Sink.h"
#import "HLView.h"
#import "SplitRoundRegion.h"

@class CAudioZone;
@class CSourceButton;
@class CVolumeControl;
@class CMP3InfoPage;
@class CCustomMediaPage;
@class CTunerPageXM;
@class CHLToolBar;
@class CSourceIndicator;
@class CHLButton;
@class CMediaZonePage;
@class CSourceIconView;
@class CUserTabIconView;
@class CAllZonesOffView;
@class CMediaZoneToolBarClientView;

#define MODAL_SELECTSOURCE              1
#define MODAL_CONFIG                    2

#define POWER_BUTTON_STATE_NONE                 0
#define POWER_BUTTON_STATE_ZONEOFF              1
#define POWER_BUTTON_STATE_CANCELWARNING        2

/*============================================================================*/

@interface CMediaModalViewHeaderPage : UIView

/*============================================================================*/
{
    UILabel                             *m_pLabel;
    CUserTabIconView                    *m_pIconView;
}

-(id)initWithFrame:(CGRect)rFrame Title:(NSString*)pTitle Icon:(NSString*)psIcon;

@end

/*============================================================================*/

@interface CMediaModalViewClientPage : UIView

/*============================================================================*/
{
}

-(id)initWithFrame:(CGRect)rFrame Title:(NSString*)pTitle;

@end


/*============================================================================*/

@interface CMediaModalView : CSplitRoundRegion < CHLCommandResponder >

/*============================================================================*/
{
    CHLButton                           *m_pBackBtn;
    CHLButton                           *m_pDoneBtn;
    UILabel                             *m_pLabel;
    UIToolbar                           *m_pToolBar;
    NSMutableArray                      *m_pPages;
    CMediaZonePage                      *m_pMediaZonePage;
    int                                 m_iType;
}

-(id)initWithFrame:(CGRect)rFrame MediaZonePage:(CMediaZonePage*)pPage Type:(int)iType;
-(int)Type;

@end


/*============================================================================*/

@interface CMediaZonePage : CSubSystemPage <CSink, CHLCommandResponder>

/*============================================================================*/
{
    CGRect                              m_rBaseFrame;
    CAudioZone                          *m_pZone;
    UIView                              *m_pPage;
    CHLToolBar                          *m_pToolBar;
    CMediaZoneToolBarClientView         *m_pToolBarClient;
    BOOL                                m_bInit;

    CMediaModalView                     *m_pModalPage;
    CMediaModalView                     *_m_pModalPage;
    
    CAllZonesOffView                    *m_pAllZonesOffView;
    CAllZonesOffView                    *_m_pAllZonesOffView;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)Init:(CSubTab*)pSubTab;
-(void)InitToolBar;

-(void)SelectSource;
-(void)SelectSourceAnimated:(BOOL)bAnimate;
-(void)ShowSettings;
-(void)RemoveSubViews;
-(void)SetActiveSubPage;
-(void)SourceSelectFinished;
-(void)PresentModalView:(CMediaModalView*)pView Animated:(BOOL)bAnimate;
-(void)ReleaseSelectPage;
-(void)ReleaseModalView;
-(CGRect)ContentPageRect;
+(UILabel*)CreateLabel:(CGRect)rLabel View:(UIView*)pView;
-(void)BeginAllOffWarning;
-(void)CancelAllOffWarning;


@end
