//
//  VolumeLabel.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/26/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "VolumeLabel.h"
#import "AudioZone.h"
#import "hlm.h"
#import "CustomPage.h"

@implementation CVolumeLabel


/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    if (self == [super initWithFrame:rect])
    {
        m_pZone = pZone;

        rect.origin.x = 0;
        rect.origin.y = 0;
        CGRect r1 = rect;
        r1.size.height = rect.size.height/2;
        CGRect r2 = r1;
        r2.origin.y += r1.size.height;

        if (m_pZone.m_iVolumeType == VOLUME_TYPE_RAMP)
        {
            r1.origin.y += (r1.size.height/2);
        }

        self.opaque = NO;
        m_pLabel1 = [[UILabel alloc] initWithFrame:r1];
        [CCustomPage InitStandardLabel:m_pLabel1 Size:16];

        [self addSubview:m_pLabel1];

        if (m_pZone.m_iVolumeType != VOLUME_TYPE_RAMP)
        {
            m_pLabel2 = [[UILabel alloc] initWithFrame:r2];
            m_pLabel2.backgroundColor = [UIColor clearColor];
            m_pLabel2.textColor = [UIColor whiteColor];
            m_pLabel2.textAlignment = UITextAlignmentCenter;
            m_pLabel2.opaque = NO;
            [m_pLabel2 setAdjustsFontSizeToFitWidth:YES];
            [m_pLabel2 setMinimumFontSize:11];
            [self addSubview:m_pLabel2];
        }

        
        [m_pLabel1 setText:@"Volume"];
        [m_pLabel1 setAdjustsFontSizeToFitWidth:YES];
        [m_pLabel1 setMinimumFontSize:8];
        [m_pZone AddSink:self];
        [self Notify:0 Data:SINK_VOLCHANGED];
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if ((iData & SINK_VOLCHANGED) == 0)
        return;
     if (m_pZone.m_iVolumeType == VOLUME_TYPE_RAMP)
        return;

    int iVolume = [m_pZone Volume];
    if (iVolume < 0)
    {
        [m_pLabel2 setText:@"Mute"];
        return;
    }
    
    NSString *pS = [NSString stringWithFormat:@"%d%%", m_pZone.m_iVolume];
    [m_pLabel2 setText:pS];
}

@end
