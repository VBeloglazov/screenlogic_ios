//
//  PoolSummaryView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/13/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"

@class CNSPoolConfig;

#define ITEM_POOLTEMP           0
#define ITEM_POOLSP             1
#define ITEM_POOLHEATMODE       2
#define ITEM_POOLHEATSTATUS     3
#define ITEM_SPATEMP            4
#define ITEM_SPASP              5
#define ITEM_SPAHEATMODE        6
#define ITEM_SPAHEATSTATUS      7
#define ITEM_AIRTEMP            8
#define ITEM_REMOTES            9
#define ITEM_FREEZE             10
#define ITEM_PHORP              11

#define S_POOLTEMP          [CHLComm TranslateString:@"Temperature"      Context:@"Pool"]
#define S_POOLSP            [CHLComm TranslateString:@"Set Point"        Context:@"Pool"]
#define S_POOLHEATMODE      [CHLComm TranslateString:@"Heat Mode"        Context:@"Pool"]
#define S_POOLHEATSTATUS    [CHLComm TranslateString:@"Heat Status"      Context:@"Pool"]
#define S_SPATEMP           [CHLComm TranslateString:@"Temperature"      Context:@"Pool"]
#define S_SPASP             [CHLComm TranslateString:@"Set Point"        Context:@"Pool"]
#define S_SPAHEATMODE       [CHLComm TranslateString:@"Heat Mode"        Context:@"Pool"]
#define S_SPAHEATSTATUS     [CHLComm TranslateString:@"Heat Status"      Context:@"Pool"]
#define S_AIRTEMP           [CHLComm TranslateString:@"Air Temperature"  Context:@"Pool"]
#define S_REMOTES           [CHLComm TranslateString:@"Spa-Side Remotes" Context:@"Pool"]
#define S_FREEZE            [CHLComm TranslateString:@"Freeze Mode"      Context:@"Pool"]
#define S_PHORP             [CHLComm TranslateString:@"pH / ORP"         Context:@"Pool"]
#define TSIZE_MAX           18


/*============================================================================*/

@interface CPoolSummaryItem : UIView

/*============================================================================*/
{
    int                                 m_iType;
    UILabel                             *m_pLabel;
    UILabel                             *m_pValue;

}

-(id)initWithFrame:(CGRect)frame Type:(int)iType Label:(NSString*)pLabel;
-(int)Type;
-(void)SetText:(NSString*)pText;
-(UILabel*)AddLabel:(NSString*)pText;
-(void)SetLabel:(NSString*)pText;

@end

/*============================================================================*/

@interface CPoolSummaryView : UIView < CSink >

/*============================================================================*/
{
    CNSPoolConfig                   *m_pConfig;
    NSMutableArray                  *m_pControls;
}

-(void)AddItem:(int)iType Label:(NSString*)pLabel;
-(void)AddItem:(int)iType Label:(NSString*)pLabel Suffix:(NSString*)pSuffix;
-(void)SetItemText:(int)iType Text:(NSString*)pText;
-(void)SetItemLabel:(int)iType Text:(NSString*)pText;

@end
