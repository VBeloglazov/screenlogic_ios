//
//  TunerFavsPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sink.h"
#import "NSSocketSink.h"
#import "TunerFavsPage.h"

@class CAudioZone;
@class CHLTableFrame;
@class CTunerContainerPage;

/*============================================================================*/

@interface CStation : NSObject

/*============================================================================*/
{
    NSString                                *m_pName;
    NSString                                *m_pIcon;
    int                                     m_iID;
}

-(id)initWithID:(int)iID Name:(NSString*)pName;
-(id)initWithID:(int)iID Icon:(NSString*)pIcon Name:(NSString*)pName;

-(int)ID;
-(NSString*)Name;
-(NSString*)IconName;

@end

/*============================================================================*/

@interface CTunerFavsPage : UIView                <     UITableViewDelegate, 
                                                        UITableViewDataSource,
                                                        CSink,
                                                        CNSSocketSink >
                                                        
/*============================================================================*/
{
    CAudioZone                              *m_pZone;
    CHLTableFrame                           *m_pTableView;
    NSMutableArray                          *m_pStations;
    CTunerContainerPage                     *m_pContainer;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone Container:(CTunerContainerPage*)pContainer;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

@end
