/*
--------------------------------------------------------------------------------

    PLATFORM.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "main.h"


#ifdef IPHONE
    extern int                 g_iConnectionType;
#endif

//
// See also the HL_PROC_ defines in HLM.H.
//
#undef PROC_INT
#ifndef UNDER_CE
    #ifdef IPHONE
        #define PROC_INT HL_PROC_IPHONE
    #else
        #define PROC_INT HL_PROC_WIN32
    #endif
#elif HL_X86
    #error SHOULD NOT BE USED - see Jim. // define PROC_INT HL_PROC_X86
#elif HL_X86A
    #define PROC_INT HL_PROC_X86A
#elif HL_X86B
    #define PROC_INT HL_PROC_X86B
#elif HL_X86C
    #define PROC_INT HL_PROC_X86C
#elif HL_ARM
    #define PROC_INT HL_PROC_ARM
#elif HL_ARMV4A
    #define PROC_INT HL_PROC_ARMV4A
#elif HL_ARMV4B
    #define PROC_INT HL_PROC_ARMV4B
#elif HL_ARMV4C
    #define PROC_INT HL_PROC_ARMV4C
#elif HL_ARMV4IA
    #define PROC_INT HL_PROC_ARMV4IA
#elif HL_ARMV4IB
    #define PROC_INT HL_PROC_ARMV4IB
#elif HL_ARMV4IC
    #define PROC_INT HL_PROC_ARMV4IC
#endif

#undef RES_INT

#ifdef CRYSTALPAD
    #define RES_INT()   (CMainWnd::ResInt())
#elif MINIPAD
    #define RES_INT()   (CMainWnd::ResInt())
#elif HLOSD
    #define RES_INT()   (HL_OSD_RES)
#else
    #define RES_INT()   (HL_800x600)
#endif

#undef CONNECTION_INT

#ifdef POOLCONFIG
    #ifdef HL_PPC
        #define CONNECTION_INT() (CONNECTION_TYPE_CP_240X320)
    #else
        #define CONNECTION_INT() (CONNECTION_TYPE_CP_800X600)
    #endif
#elif HLUTIL
    #define CONNECTION_INT() (CONNECTION_TYPE_CONFIG)
#elif MINIPAD
    #define CONNECTION_INT() (CONNECTION_TYPE_MINIPAD);
#elif HLCONFIG
    #define CONNECTION_INT() (CONNECTION_TYPE_CONFIG);
#elif CRYSTALPAD
    #ifdef IPHONE
        #define CONNECTION_INT() (g_iConnectionType)
    #else
        #define CONNECTION_INT() (g_pMainWindow->ConnectionInt())
    #endif
#elif HLOSD
    #define CONNECTION_INT() (g_pMainWindow->ConnectionInt())
#elif GATEWAY
    #define CONNECTION_INT() (CONNECTION_TYPE_CLIENTGATEWAY)
#elif DDTEST
    #define CONNECTION_INT() (CONNECTION_TYPE_SERVICE)
#elif PENTAIRMON
    #define CONNECTION_INT() (CONNECTION_TYPE_CP_800X600)
#else
    #define CONNECTION_INT() (CONNECTION_TYPE_SERVICE)
#endif


#ifdef CRYSTALPAD
    #if !defined(HL_PLATFORM_LARGE) && !defined(HL_PLATFORM_SMALL)
        #error "Major Platform Not Defined"
    #endif
#endif

#ifdef UNDER_CE
    #if defined(HL_ARMV4IA)
        #define HL_FOLDER(s)        _T("\\NAND_Flash\\"s)
        #define TEMP_FOLDER(s)      _T("\\"s)
    #elif defined(HL_X86C)
        #ifdef GATEWAY
            #include "..\lcore\system.h"
            #define HL_FOLDER(s)        CSystem::HLFolder(s)
        #elif MONITOR
            #include "..\monitor\monitor.h"
            #define HL_FOLDER(s)        HLFolder(s)
        #else
            #define HL_FOLDER(s)        _T("\\Hard Disk\\HOMELOGIC\\"s)
        #endif
    #else error FIX ME
        #define HL_FOLDER(s)        _T("\\FlashStorage2\\"s)
        #define TEMP_FOLDER(s)      _T("\\"s)
    #endif
#else
    #define HL_FOLDER(s)        _T("C:\\HOMELOGIC\\"s)
    #define TEMP_FOLDER(s)      _T("C:\\HOMELOGIC\\APPLET\\"s)
#endif
