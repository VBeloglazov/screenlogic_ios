/*
--------------------------------------------------------------------------------

    ConnectionGDiscoveryMgr.cpp

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#include "hlstd.h"

#include "hlmsg.h"
#include "lock.h"
#include "StatusSink.h"
#include "SocketHL.h"
#include "RelayServerDef.h"

#include "ConnectionGDiscoveryMgr.h"

#define ERR_GDM_CONNECTION          _HLTEXT("Cannot connect to Discovery Service")
#define ERR_GDM_NOT_CONNECTED       _HLTEXT("Not connected to Discovery Service")
#define ERR_GDM_COMMUNICATION       _HLTEXT("Communication to Discovery Service failed")
#define ERR_GDM_SEND_FAILED         _HLTEXT("Send failed")
#define ERR_GDM_BAD_RESPONSE        _HLTEXT("Unrecognized response")
#define ERR_GDM_NO_GATEWAY          _HLTEXT("Gateway not found")
#define ERR_GDM_NO_LICENCE          _HLTEXT("Subscription for remote service expired")
#define ERR_GDM_UNKNOWN_DATATYPE    _HLTEXT("Unsupported type in recordset")

#ifdef PENTAIRMON
    #define m_pThis this
#else
CConnectionGDiscoveryMgr*       CConnectionGDiscoveryMgr::m_pThis = NULL;
#endif

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::GetGatewayData(
    CHLString                   sGatewayName,
    CHLString                   sLoginName,
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus )

/*============================================================================*/
{
    if ( !Connect(pServersoftAddress, wServersoftPort, pStatus) )
        return FALSE;

    pStatus->SetStatus( _HLTEXT("Sending GATEWAY remote connection request...") );

    // If the subscription for the remote Gateway Finder Service
    // expired, this call will return FALSE.
    if ( !m_pThis->GatewayConnectionRequest( sGatewayName, sLoginName ) )
    {
        CHLString sStatus;
        sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*) m_pThis->LastError() );
        pStatus->SetStatus( sStatus );

        return FALSE;
    }

    pStatus->SetStatus( _HLTEXT("Remote connection authorized.") );

    CHLString sStatus;
    sStatus.Format( _HLTEXT("Found system at %s:%d."),
        (const TCHAR*) m_pThis->m_sGatewayAddress, m_pThis->m_wGatewayPort
    );
    pStatus->SetStatus( sStatus );

    // 03/16/2011 Now it will report port open for Relay connection.
    if ( !m_pThis->m_bPortOpen )
        pStatus->SetStatus( _HLTEXT("Public port closed.") );

    // 03/16/2011 Now it will report Relay mode OFF for Relay connection.
    if ( m_pThis->m_bRelayOn )
        pStatus->SetStatus( _HLTEXT("Relay mode is ON.") );

    return TRUE;

} // GetGatewayData()

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::GetGatewayDbDump(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus )

/*============================================================================*/
{
    if ( !Connect(pServersoftAddress, wServersoftPort, pStatus) )
        return FALSE;

    pStatus->SetStatus( _HLTEXT("Downloading Gateway DB...") );

    if ( !m_pThis->DownloadGatewayDB() )
    {
        CHLString sStatus;
        sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*) m_pThis->LastError() );
        pStatus->SetStatus( sStatus );

        return FALSE;
    }

    pStatus->SetStatus( _HLTEXT("Gateway DB downloaded.") );

    return TRUE;

} // GetGatewayDbDump()

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::GetActiveRelayList(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus )

/*============================================================================*/
{
    if ( !Connect(pServersoftAddress, wServersoftPort, pStatus) )
        return FALSE;

    pStatus->SetStatus( _HLTEXT("Downloading Active Relays list...") );

    if ( !m_pThis->DownloadActiveRelayList() )
    {
        CHLString sStatus;
        sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*) m_pThis->LastError() );
        pStatus->SetStatus( sStatus );

        return FALSE;
    }

    pStatus->SetStatus( _HLTEXT("Active Relays list downloaded.") );

    return TRUE;

} // GetActiveRelayList()

/*============================================================================*/

    void                        CConnectionGDiscoveryMgr::Dispose()

/*============================================================================*/
{
#ifndef PENTAIRMON
    delete m_pThis;
    m_pThis = NULL;
#endif

} // Dispose()

/*============================================================================*/

    CConnectionGDiscoveryMgr::CConnectionGDiscoveryMgr(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort )

    : m_bValid              ( FALSE ),
      m_strLastError        ( CHLString(_HLTEXT("")) ),
      m_sGatewayAddress     ( _HLTEXT("") ),
      m_wGatewayPort        ( -1 ),
      m_bPortOpen           ( FALSE ),
      m_bRelayOn            ( FALSE ),
      m_iRowCount           ( 0 ),
      m_iColCount           ( 0 ),
      m_Schema              ( NULL )

/*============================================================================*/
{
    m_pSocket = new CSocketHL( pServersoftAddress, wServersoftPort );
    
    if ( !m_pSocket->IsValid() )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GDM_CONNECTION, (const HLTCHAR*)m_pSocket->LastError() );
#if defined(UNICODE)
		TRACE( (wchar_t*)strError );
#else
		TRACE( strError );
#endif
        SetLastError( strError );

        return;
    }

    m_bValid = TRUE;

} // CConnectionGDiscoveryMgr()

/*============================================================================*/

    CConnectionGDiscoveryMgr::~CConnectionGDiscoveryMgr()

/*============================================================================*/
{
    m_pSocket->Disconnect( _HLTEXT("Destructor") );
    delete m_pSocket;

    delete[] m_Schema;

    m_Recordset.RemoveAll();

    CleanRelayList();

} // CConnectionGDiscoveryMgr()

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::Connect(
    BYTE*                       pServersoftAddress,
    WORD                        wServersoftPort,
    CStatusSink*                pStatus )

/*============================================================================*/
{
#ifndef PENTAIRMON
    if ( !m_pThis )
    {
        pStatus->SetStatus( _HLTEXT("Connecting to Discovery Service...") );

        m_pThis =
            new CConnectionGDiscoveryMgr( pServersoftAddress, wServersoftPort );

        if ( !m_pThis->IsValid() )
        {
            CHLString sStatus;
            sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*)m_pThis->LastError() );
            pStatus->SetStatus( sStatus );

            return FALSE;
        }
    }

    pStatus->SetStatus( _HLTEXT("Connected.") );
#endif

    return TRUE;

} // Connect()

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::GatewayConnectionRequest(
    CHLString                   sGatewayName,
    CHLString                   sLoginName )

/*============================================================================*/
{
    if ( !m_pSocket->IsConnected() )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GDM_NOT_CONNECTED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Send Request
    //
    CHLMSG  msgQ( 0, HLM_SERVICE_GDISCOVERY_GETGATEWAYDATA_Q );
    msgQ.PutString( sGatewayName ); // Gateway Name
    msgQ.PutString( sLoginName );   // Login Name

    if ( !m_pSocket->Send( msgQ.MSGSize(), msgQ.MSG(), msgQ.Critical() ) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GDM_SEND_FAILED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Wait and process response(s)
    //
    BOOL bReceived = FALSE;
    while( !bReceived )
    {
        if ( !m_pSocket->IsConnected() )
        {
            break; // while()
        }

        if ( m_pSocket->CheckReceiveMsg() )
        {
            HLT_HEADER* pHeader = (HLT_HEADER*) m_pSocket->Message();
            CHLMSG msgA( pHeader );

            short shMessageID = msgA.MessageID();

            switch( shMessageID )
            {
            case HLM_SERVICE_GDISCOVERY_GETGATEWAYDATA_A:
                {
                    bReceived = TRUE;

                    BOOLEAN   bGatewayFound; msgA.GetBYTE  ( &bGatewayFound );
                    BOOLEAN   bLicenseOK;    msgA.GetBYTE  ( &bLicenseOK    );
                    CHLString sAddress;      msgA.GetString( &sAddress      );
                    WORD      wPort;         msgA.GetWORD  ( &wPort         );
                    BOOLEAN   bPortOpen;     msgA.GetBYTE  ( &bPortOpen     );
                    BOOLEAN   bRelayOn;      msgA.GetBYTE  ( &bRelayOn      );

                    if ( !bGatewayFound )
                    {
                        SetLastError( ERR_GDM_NO_GATEWAY );
                        return FALSE;
                    }

                    if ( sLoginName != SERVICE_LOGIN  &&  !bLicenseOK )
                    {
                        SetLastError( ERR_GDM_NO_LICENCE );
                        return FALSE;
                    }

                    m_sGatewayAddress      = sAddress;
                    m_wGatewayPort         = wPort;
                    m_bPortOpen            = bPortOpen;
                    m_bRelayOn             = bRelayOn;
                }
                break;

            case HLM_SERVERSOFT_ERROR:
                {
                    bReceived = TRUE;

                    CHLString sError;   msgA.GetString( &sError );

                    SetLastError( sError );

                    return FALSE;
                }
                break;

            default:
                {
                    CHLString sMes;
                    sMes.Format( _HLTEXT("%s (%d)"),
                        ERR_GDM_BAD_RESPONSE, shMessageID );

                    SetLastError( sMes );

                    return FALSE;
                }
            }
        }
    }

    if ( !bReceived )
    {
        SetLastError( ERR_GDM_COMMUNICATION );
        return FALSE;
    }

    return TRUE;

} // GatewayConnectionRequest()

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::DownloadGatewayDB()

/*============================================================================*/
{
    //
    // Send Request
    //
    CHLMSG  msgQ( 0, HLM_SERVICE_GDISCOVERY_GETGATEWAYRECORDSET_Q );

    if ( !m_pSocket->Send( msgQ.MSGSize(), msgQ.MSG(), msgQ.Critical() ) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GDM_SEND_FAILED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Wait and process response(s)
    //
    BOOL bReceived = FALSE;
    while( !bReceived )
    {
        if ( !m_pSocket->IsConnected() )
        {
            break; // while()
        }

        if ( m_pSocket->CheckReceiveMsg() )
        {
            HLT_HEADER* pHeader = (HLT_HEADER*) m_pSocket->Message();
            CHLMSG msgA( pHeader );

            short shMessageID = msgA.MessageID();

            switch( shMessageID )
            {
            case HLM_SERVICE_GDISCOVERY_GETGATEWAYRECORDSET_A:
                {
                    bReceived = TRUE;

                    msgA.GetInt( &m_iRowCount );
                    msgA.GetInt( &m_iColCount );

                    delete m_Schema;
                    m_Schema = new CHLString[m_iColCount];

                    for(int j=0; j<m_iColCount; j++)
                    {
                        msgA.GetString( m_Schema + j );
                    }

                    m_Recordset.RemoveAll();

                    for( int i=0; i<m_iRowCount; i++ )      // records
                    {
                        CHLObList* record = new CHLObList();

                        for( int j=0; j<m_iColCount; j++ )  // columns
                        {
                            void* pValue = NULL;

                            if ( m_Schema[j] == "System.String" )
                            {
                                pValue = new CHLString();   msgA.GetString( (CHLString*) pValue );
                                record->AddTail( pValue );
                            }
                            else

                            if ( m_Schema[j] == "System.Int16" )
                            {
                                pValue = new WORD;          msgA.GetWORD( (WORD*) pValue );
                                record->AddTail( pValue );
                            }
                            else

                            if ( m_Schema[j] == "System.Int32" )
                            {
                                pValue = new int;           msgA.GetInt( (int*) pValue );
                                record->AddTail( pValue );
                            }
                            else

                            if ( m_Schema[j] == "System.Boolean" )
                            {
                                pValue = new BOOLEAN;       msgA.GetBYTE( (BOOLEAN*) pValue );
                                record->AddTail( pValue );
                            }
                            else

                            if ( m_Schema[j] == "System.DateTime" )
                            {
                                short shYear;               msgA.GetShort( &shYear   );
                                BYTE  byMonth;              msgA.GetBYTE ( &byMonth  );
                                BYTE  byDay;                msgA.GetBYTE ( &byDay    );
                                BYTE  byHour;               msgA.GetBYTE ( &byHour   );
                                BYTE  byMinute;             msgA.GetBYTE ( &byMinute );
                                BYTE  bySecond;             msgA.GetBYTE ( &bySecond );

                                CHLTime* pDateTime = new CHLTime;
                                pDateTime->Set(
                                    shYear, byMonth, 0, byDay, byHour, byMinute, bySecond, 0 );
                                record->AddTail( (void*) pDateTime );
                            }

                            else
                            {
                                CHLString sMes;
                                sMes.Format( _HLTEXT("%s: %s"),
                                    ERR_GDM_UNKNOWN_DATATYPE, (const HLTCHAR*)m_Schema[j] );

                                SetLastError( sMes );

                                return FALSE;
                            }

                        } // for (j) columns

                        m_Recordset.AddTail( (void*) record );
                    
                    } // for (i) records
                }
                break;

            case HLM_SERVERSOFT_ERROR:
                {
                    bReceived = TRUE;

                    CHLString sError;   msgA.GetString( &sError );

                    SetLastError( sError );
                    return FALSE;
                }
                break;

            default:
                {
                    CHLString sMes;
                    sMes.Format( _HLTEXT("%s (%d)"),
                        ERR_GDM_BAD_RESPONSE, shMessageID );

                    SetLastError( sMes );

                    return FALSE;
                }
            }
        }
    }

    if ( !bReceived )
    {
        SetLastError( ERR_GDM_COMMUNICATION );
        return FALSE;
    }

    return TRUE;
 
} // DownloadGatewayDB()

/*============================================================================*/

    BOOL                        CConnectionGDiscoveryMgr::DownloadActiveRelayList()

/*============================================================================*/
{
    //
    // Send Request
    //
    CHLMSG  msgQ( 0, HLM_SERVICE_GDISCOVERY_RELAYLIST_Q );
    msgQ.PutInt( GDISCOVERY_SCHEMA_LATEST );

    if ( !m_pSocket->Send( msgQ.MSGSize(), msgQ.MSG(), msgQ.Critical() ) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GDM_SEND_FAILED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Wait and process response(s)
    //
    BOOL bReceived = FALSE;
    while( !bReceived )
    {
        if ( !m_pSocket->IsConnected() )
        {
            break; // while()
        }

        if ( m_pSocket->CheckReceiveMsg() )
        {
            HLT_HEADER* pHeader = (HLT_HEADER*) m_pSocket->Message();
            CHLMSG msgA( pHeader );

            short shMessageID = msgA.MessageID();

            switch( shMessageID )
            {
            case HLM_SERVICE_GDISCOVERY_RELAYLIST_A:
                {
                    bReceived = TRUE;

                    CleanRelayList();

                    int iSchema;                            msgA.GetInt   ( &iSchema                 ); // Not in use

                    int iNRelays;                           msgA.GetInt   ( &iNRelays                );
                    for ( int i=0; i<iNRelays; i++ )
                    {
                        CHLString sAddress;                 msgA.GetString( &sAddress                );
                        WORD      wGatewayPort;             msgA.GetWORD  ( &wGatewayPort            );
                        WORD      wClientPort;              msgA.GetWORD  ( &wClientPort             );
                        BOOLEAN   bActiveFlag;              msgA.GetBYTE  ( &bActiveFlag             );
                        int       iActiveConnectionCount;   msgA.GetInt   ( &iActiveConnectionCount  );

                        WORD      wYear;                    msgA.GetWORD  ( &wYear                   );
                        BYTE      byMonth;                  msgA.GetBYTE  ( &byMonth                 );
                        BYTE      byDay;                    msgA.GetBYTE  ( &byDay                   );
                        BYTE      byHour;                   msgA.GetBYTE  ( &byHour                  );
                        BYTE      byMinute;                 msgA.GetBYTE  ( &byMinute                );
                        BYTE      bySecond;                 msgA.GetBYTE  ( &bySecond                );

                        SYSTEMTIME tmLastUpdated;
#ifndef IPHONE
                        tmLastUpdated.wYear   = wYear;
                        tmLastUpdated.wMonth  = byMonth;
                        tmLastUpdated.wDay    = byDay;
                        tmLastUpdated.wHour   = byHour;
                        tmLastUpdated.wMinute = byMinute;
                        tmLastUpdated.wSecond = bySecond;
#endif
                        
                        m_Relays.AddTail(
                            new CRelayServerDef(
                                sAddress,
                                wGatewayPort,
                                wClientPort,
                                bActiveFlag,
                                iActiveConnectionCount,
                                tmLastUpdated
                            )
                        );
                    }
                }
                break;

            case HLM_SERVERSOFT_ERROR:
                {
                    bReceived = TRUE;

                    CHLString sError;   msgA.GetString( &sError );

                    SetLastError( sError );
                    return FALSE;
                }
                break;

            default:
                {
                    CHLString sMes;
                    sMes.Format( _HLTEXT("%s (%d)"),
                        ERR_GDM_BAD_RESPONSE, shMessageID );

                    SetLastError( sMes );

                    return FALSE;
                }
            }
        }
    }

    if ( !bReceived )
    {
        SetLastError( ERR_GDM_COMMUNICATION );
        return FALSE;
    }

    return TRUE;

} // DownloadActiveRelayList()

/*============================================================================*/

    void                        CConnectionGDiscoveryMgr::CleanRelayList()

/*============================================================================*/
{
    while( !m_Relays.IsEmpty() )
    {
        CRelayServerDef* pRelay = (CRelayServerDef*) m_Relays.RemoveHead();
        delete pRelay;
    }

} // CleanRelayList()
