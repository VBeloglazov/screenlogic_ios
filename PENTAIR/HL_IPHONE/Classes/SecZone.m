//
//  SecZone.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/15/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecZone.h"
#import "NSQuery.h"

@implementation CSecZone


/*============================================================================*/

-(id)initFromQ:(CNSQuery*)pMSG

/*============================================================================*/
{
    m_iID = [pMSG GetInt];
    m_pName = [[pMSG GetString] retain];
    m_iFlags = [pMSG GetInt];

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pName release];
    [super dealloc];
}
/*============================================================================*/

-(NSString*)Name                { return m_pName;   }
-(int)ID                        { return m_iID;     }
-(int)GetState                  { return m_iState;  }
-(void)SetState:(int)iNew       { m_iState = iNew;  }

/*============================================================================*/

-(BOOL)ReadyState

/*============================================================================*/
{
    if (m_iState & (SEC_ZONE_FAULTED|SEC_ZONE_BYPASSED))
        return FALSE;
    return TRUE;
}

@end
