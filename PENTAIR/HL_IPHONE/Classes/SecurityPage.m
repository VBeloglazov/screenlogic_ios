//
//  SecurityPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/15/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecurityPage.h"
#import "SecKeypad.h"
#import "SecPartition.h"
#import "SecZonePage.h"
#import "SecurityHistory.h"
#import "SecurityStatusView.h"
#import "MainView.h"
#import "crect.h"
#import "hlsegment.h"
#import "zoneselector.h"
#import "CustomPage.h"

@implementation CSecurityPage

/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab

/*============================================================================*/
{
    self = [super initWithFrame:rFrame MainTabPage:pMainTabPage SubTab:pSubTab];
    if (self)
    {
        [self Init:pSubTab];
        m_bInit = TRUE;
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pPageCtrl release];
    [m_pMainView release];
    [m_pPartition RemoveSink:self];
    [m_pStatusText release];
    [m_pKeypad release];
    [m_pZonePage release];
    [m_pHistPage release];
    [super dealloc];
}
/*============================================================================*/

-(void)Init:(CSubTab*)pSubTab

/*============================================================================*/
{
    [super Init:pSubTab];
    m_pPartition = (CSecPartition*)pSubTab;
    [m_pPartition AddSink:self];

    int iDXThis = self.bounds.size.width;

    CGRect rThis = self.bounds;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        CGRect rTabBar = [CRect BreakOffBottom:&rThis DY:[CMainView DY_TOOLBAR]];

        NSMutableArray *pItems = [[NSMutableArray alloc] init];
        [pItems addObject:@"Keypad"];
        [pItems addObject:@"Zones"];
        [pItems addObject:@"History"];

        m_pPageCtrl = [[CHLSegment alloc] initWithFrame:rTabBar ImageProvider:NULL Items:pItems Type:HLSEGMENT_PAGE_CTRL Data:NO];
        [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];

        [self addSubview:m_pPageCtrl];
        [m_pPageCtrl SetSelectedIndex:0];
        [pItems release];
    }

/*
    m_pTabBar = [[UITabBar alloc] initWithFrame:rTabBar];
    [self addSubview:m_pTabBar];
*/

    CGRect rPage = rThis;
    m_pMainView = [[UIView alloc] initWithFrame:rPage];
    [m_pMainView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:m_pMainView];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        m_pKeypad = [[CSecurityStatusView alloc] initWithFrame:m_pMainView.bounds Partition:m_pPartition];
        [m_pMainView addSubview:m_pKeypad];
        m_pActivePage = m_pKeypad;
    }
    else 
    {
        m_pKeypad = [[UIView alloc] initWithFrame:m_pMainView.bounds];
        m_pKeypad.autoresizingMask = 0xFFFFFFFF;

        int iDYText = 35;
        CGRect rText = rPage;
        rText.size.height = iDYText;
        rText.origin.x = 0;
        rText.origin.y = 0;
        m_pStatusText = [[UILabel alloc] initWithFrame:rText];
        [CCustomPage InitStandardLabel:m_pStatusText Size:iDYText / 2];
        m_pStatusText.autoresizingMask = 0xFFFFFFFF;
        [m_pStatusText setText:@"Status"];
        [m_pStatusText setAdjustsFontSizeToFitWidth:YES];
        [m_pStatusText setMinimumFontSize:8];
        [m_pKeypad addSubview:m_pStatusText];

        CGRect rKeypad = CGRectMake(0, rText.size.height, iDXThis, rPage.size.height - rText.size.height - 5);
        CSecKeypad *pKeypad = [[CSecKeypad alloc] initWithFrame:rKeypad Partition:m_pPartition];
        pKeypad.autoresizingMask = 0xFFFFFFFF;
        [m_pKeypad addSubview:pKeypad];
        [pKeypad release];
        m_pActivePage = m_pKeypad;
        [m_pMainView addSubview:m_pKeypad];
    }
    [self Notify:0 Data:0];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    [super SetVisible:bVisible];
    if (bVisible)
        [self LockHorizontalScroll:(m_pHistPage != NULL && m_pActivePage == m_pHistPage)];
}
/*============================================================================*/

-(CHLSegment*)GetPageCtrl

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        return NULL;

    if (m_pPageCtrl != NULL)
        return m_pPageCtrl;
    
    NSMutableArray *pItems = [[NSMutableArray alloc] init];

    CHLSegmentIconView *p1 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"STATUS.png" Text:@"Status"];
    CHLSegmentIconView *p2 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"ZONES.png" Text:@"Zones"];
    CHLSegmentIconView *p3 = [[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"HISTORY.png" Text:@"History"];
    [pItems addObject:p1];
    [pItems addObject:p2];
    [pItems addObject:p3];
    [p1 release];
    [p2 release];
    [p3 release];

    m_pPageCtrl = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:NULL Views:pItems  Type:HLSEGMENT_TOOLBAR];
    [m_pPageCtrl addTarget:self action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
    [pItems release];
    return m_pPageCtrl;
}
/*============================================================================*/

-(void)InitSubTabViewOverlay

/*============================================================================*/
{
    CSubTabViewOverlay *pOverlay = [self Overlay];

    if (pOverlay == NULL)
        return;

    [pOverlay FreeClientView];


    NSString *psText = [m_pPartition StatusText];

    CGRect rBounds = [pOverlay ClientRect];
    CSubSystemOverlayClientView *pView = [[CSubSystemOverlayClientView alloc] initWithFrame:rBounds Icon:@"SECURITY.png" Text:psText];
    [pOverlay SetHighlightMode:[m_pPartition StatusArmed]];
    [pOverlay SetClientView:pView];
    [pView release];
}
/*============================================================================*/

-(void)SelectPage:(id)sender

/*============================================================================*/
{
    UIView *pNewView = NULL;
    CHLSegment *pCtrl = (CHLSegment*)sender;
    CGRect rPage = m_pMainView.bounds;
    
    switch ([pCtrl SelectedIndex])
    {
    case 0: 
        pNewView = m_pKeypad;
        break;

    case 1: 
        {
            if (m_pZonePage == NULL)
                m_pZonePage = [[CSecZonePage alloc] initWithFrame:rPage Partition:m_pPartition];
            pNewView = m_pZonePage;
        }
        break;

    case 2: 
        {
            if (m_pHistPage == NULL)
                m_pHistPage = [[CSecurityHistoryView alloc] initWithFrame:rPage Partition:m_pPartition];
            pNewView = m_pHistPage;
        }
        break;
            
    }

    if (m_bInit)
    {
        [m_pMainView addSubview:pNewView];
        [pNewView setFrame:m_pMainView.bounds];
        [pNewView layoutIfNeeded];
        int iIndex = [m_pPageCtrl SelectedIndex];
        [self TranslateNewPage:pNewView OldPage:m_pActivePage ThisIndex:iIndex LastIndex:m_iLastIndex];
        m_pActivePage = pNewView;
        m_iLastIndex = iIndex;
        return;
    }


    [m_pActivePage removeFromSuperview];
    m_pActivePage = pNewView;
    [m_pMainView addSubview:pNewView];
    [pNewView setFrame:m_pMainView.bounds];
    [pNewView layoutIfNeeded];
    if (m_bInit)
        [UIView commitAnimations];

    [self LockHorizontalScroll:(m_pHistPage != NULL && m_pActivePage == m_pHistPage)];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    [m_pStatusText setText:[m_pPartition StatusText]];
    [self InitSubTabViewOverlay];
    [self UpdateOverlay];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [m_pMainView setFrame:rBounds];
        [m_pActivePage setFrame:m_pMainView.bounds];
    }
    else
    {
        int iDYTabs = [CMainView DY_TOOLBAR];
        if (rBounds.size.width > rBounds.size.height)
        {
            // Landscape Mode
            CGRect rTabBar = CGRectMake(0, rBounds.size.height, rBounds.size.width, iDYTabs);
            [m_pMainView setFrame:rBounds];
            [m_pPageCtrl setFrame:rTabBar];
        }
        else
        {
            // Portrait Mode
            CGRect rTabBar = [CRect BreakOffBottom:&rBounds DY:iDYTabs];
            [m_pMainView setFrame:rBounds];
            [m_pPageCtrl setFrame:rTabBar];
        }
    }
}

@end
