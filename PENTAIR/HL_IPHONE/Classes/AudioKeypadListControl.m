//
//  AudioKeypadListControl.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/26/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "AudioKeypadListControl.h"


@implementation CAudioKeypadListControl

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame ID:(int)iID TextSize:(int)iTextSize TextColor:(UIColor*)pColor Style:(int)iStyle

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self) {
        // Initialization code
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}

@end
