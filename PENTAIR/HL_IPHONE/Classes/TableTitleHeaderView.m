//
//  TableTitleHeaderView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "TableTitleHeaderView.h"
#import "hlcomm.h"
#import "hlm.h"
#import "MainView.h"
#import "CustomPage.h"


@implementation CTableTitleHeaderView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Title:(NSString*)pTitle

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_iRad = [CHLComm GetInt:INT_TAB_ROUT];

        // Initialization code
        // Initialization code
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

        CGFloat colors[8];
        [CHLComm LoadGradientTo:colors G1:RGB_PAGETAB_UNSELECT_TOP G2:RGB_PAGETAB_UNSELECT_BTM];

		m_Gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
		CGColorSpaceRelease(rgb);
        self.opaque = NO;

        CGRect rLabel = CGRectMake(0, 0, frame.size.width, frame.size.height);
        m_pLabel = [[UILabel alloc] initWithFrame:rLabel];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        m_pLabel.autoresizingMask = 0xFFFFFFFF;
        
        [m_pLabel setText:pTitle];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
//        [self setUserInteractionEnabled:YES];
    }

    return self;
}
/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Title:(NSString*)pTitle Radius:(int)iRad

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_iRad = iRad;

        // Initialization code
        // Initialization code
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

        CGFloat colors[8];
        [CHLComm LoadGradientTo:colors G1:RGB_PAGETAB_UNSELECT_TOP G2:RGB_PAGETAB_UNSELECT_BTM];

		m_Gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
		CGColorSpaceRelease(rgb);
        self.opaque = NO;

        CGRect rLabel = CGRectMake(0, 0, frame.size.width, frame.size.height);
        m_pLabel = [[UILabel alloc] initWithFrame:rLabel];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        m_pLabel.autoresizingMask = 0xFFFFFFFF;
        
        [m_pLabel setText:pTitle];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
//        [self setUserInteractionEnabled:YES];
    }

    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    // Drawing code


    CGContextRef context = UIGraphicsGetCurrentContext();

    CGPoint ptStart = CGPointMake(0, 0);
    CGPoint ptEnd   = CGPointMake(0, rect.size.height);

    int iRad = m_iRad;

    int iX = rect.origin.x;
    int iY = rect.origin.y;
    int iDX = rect.size.width;
    int iDY = rect.size.height;

    CGContextSaveGState(context);


    CGContextBeginPath (context);
    CGContextMoveToPoint(context, iX+iRad, iY);
    CGContextAddArcToPoint(context, iX+iDX, iY, iX+iDX, iY+iRad, iRad);
    CGContextAddLineToPoint(context,iX+iDX, iY+iDY); 
    CGContextAddLineToPoint(context,iX, iY+iDY); 
    CGContextAddArcToPoint(context, iX, iY, iX+iRad, iY, iRad);

	CGContextClip(context);

    CGContextDrawLinearGradient(context, m_Gradient, ptStart, ptEnd, 0);

    CGContextRestoreGState(context);

    CGContextSetBlendMode(context, kCGBlendModePlusLighter);
    CGContextSetLineWidth(context, 2);

    CGContextBeginPath (context);
    CGContextMoveToPoint(context, iX, iY+iDY);
    CGContextAddArcToPoint(context, iX, iY, iX+iRad, iY, iRad);
    CGContextAddArcToPoint(context, iX+iDX, iY, iX+iDX, iY+iRad, iRad);
    CGContextAddLineToPoint(context,iX+iDX, iY+iDY); 

    UIColor *pC = [CHLComm GetRGB:RGB_EDGE];
    [pC set];
    CGContextStrokePath(context);

    CGContextBeginPath(context);
    CGContextMoveToPoint(context, iX+iDX, iY+iDY);
    CGContextAddLineToPoint(context,iX, iY+iDY); 
    pC = [CHLComm GetRGB:RGB_EDGE Adjust:0.5f];
    [pC set];
    CGContextStrokePath(context);
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    CGGradientRelease(m_Gradient);
    [super dealloc];
}
/*============================================================================*/

-(void)SetTitle:(NSString*)psTitle

/*============================================================================*/
{
    [m_pLabel setText:psTitle];
}
@end
