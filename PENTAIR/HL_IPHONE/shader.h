/*
--------------------------------------------------------------------------------

    SHADER.H

    Copyright 2011 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

@class CHLButton;
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CATransaction.h>

#ifdef __cplusplus
    class CShadeDIB;
    class CEdgeList;
    class CHLDibDC;
#else
    @class CShadeDIB;
    @class CEdgeList;
    @class CHLDibDC;
#endif


#define ELEMENT_STD         0
#define ELEMENT_FLASH       1
#define ELEMENT_CHECK       2

#define RREGION_EDGE_TOP            0x00000001
#define RREGION_EDGE_BOTTOM         0x00000002
#define RREGION_EDGE_LEFT           0x00000004
#define RREGION_EDGE_RIGHT          0x00000008

#define TOOLBAR_TOP             0
#define TOOLBAR_BTM             1
#define TOOLBAR_BTM_LEFT        2
#define TOOLBAR_BTM_RIGHT       3

#define TOOLBAR_FRAME_FULL              0
#define TOOLBAR_FRAME_TOPHALF           1
#define TOOLBAR_FRAME_BOTTOMHALF        2

#define NS_INIT_RAISED_INT             0
#define NS_INIT_RAISED_EXT             1
#define NS_INIT_RAISED_INT_INV         2
#define NS_INIT_RAISED_EXT_INV         3
#define NS_INIT_RAISED_INT_GRADIENT    4

#define EDGE_BIT_LEFT           (1<<EDGE_LEFT)
#define EDGE_BIT_RIGHT          (1<<EDGE_RIGHT)

/*============================================================================*/

    @protocol CShaderModifier

/*============================================================================*/

-(void)DoModifyStage1:(CHLDibDC*)pDIB;
-(void)DoModifyStage2:(CHLDibDC*)pDIB;

@end
/*============================================================================*/

    @interface CShader : NSObject

/*============================================================================*/
{
}

-(id)init;

+(CGImageRef)CreateRecessedImageDX:(int)iDX DY:(int)iDY Rad:(int)iRad SHOUT:(int)iSHOUT;
+(CGImageRef)CreateButtonImageDX:(int)iDX DY:(int)iDY Rad:(int)iRad SHIN:(int)iSHIN SHOUT:(int)iSHOUT ColorT:(unsigned int)rgbT ColorB:(unsigned int)rgbB;
+(CGImageRef)CreateMainTabImageDX:(int)iDX DY:(int)iDY Selected:(int)bSelected;
+(CGImageRef)CreateRegionImageDX:(int)iDX 
                              DY:(int)iDY 
                              Rad:(int)iRad 
                              RoundedEdges:(int)iEdges 
                              ColorT:(unsigned int*)prgbT 
                              ColorM:(unsigned int*)prgbM 
                              ColorB:(unsigned int)rgbB 
                              FillCenter:(int)bFill 
                              FillColor:(unsigned int)rgbFill 
                              Modifier:(id)pModifier 
                              SolidBG:(UIColor*)pSolidBG;

+(CGImageRef)CreateRegionSeparatorDX:(int)iDX Position:(int)iPosition;
+(CGImageRef)CreateToolBarButtonImage:(int)iDX DY:(int)iDY ColorT:(unsigned int)rgbT ColorB:(unsigned int) rgbB Edge:(unsigned int)rgbEdge Orientation:(int)iOrientation;

+(void)Init;
+(void)Release;

+(unsigned int)BlendPCT:(int)iPCT RGB1:(unsigned int)rgb1 RGB2:(unsigned int)rgb2;
+(unsigned int)MakeRGB:(int)iR G:(int)iG B:(int)iB;
+(unsigned int)MakeRGB:(UIColor*)pColor;
+(UIColor*)Blend:(float)f1 RGB1:(UIColor*)rgb1 RGB2:(UIColor*)rgb2;
+(UIColor*)Add:(UIColor*)rgb1 Val:(int)iVal;
+(unsigned int)AddRGB:(unsigned int)rgb1 Val:(int)iVal;
+(void)MakeSpread:(int)iSpread Color1:(unsigned int*)p1 Color2:(unsigned int*)p2;

@end


/*============================================================================*/

    @interface CNSEdgeList : NSObject

/*============================================================================*/
{
    CEdgeList                       *m_pEdgeList;
}

-(id)initWithEdgeList:(CEdgeList*)pEdgeList;
-(void)TranslateDX:(int)iDX DY:(int)iDY;
-(CGRect)GetBounds;
-(CEdgeList*)EdgeList;
-(int)IsPointInside:(CGPoint)pt;

@end
/*============================================================================*/

    @interface CNSShader : NSObject

/*============================================================================*/
{
    CShadeDIB                       *m_pDIB;
    int                             m_iUserData;
}

-(id)initWithDX:(int)iDX DY:(int)iDY;
-(CShadeDIB*)DIB;
-(int)GetUserData;
-(void)SetUserData:(int)iNew;
-(int)DX;
-(int)DY;
-(void)InitStandard:(int)iRad Shading:(int)iShading Type:(int)iType Inset:(int)iInset;
-(void)Subtract:(CNSShader*)pOther X:(int)iX Y:(int)iY;
-(void)AndWith:(CNSShader*)pOther X:(int)iX Y:(int)iY Flip:(int)iFlip;
-(void)InitWedgeDX:(int)iDYL YR:(int)iDYR Shading:(int)iNShading SHOUT:(int)iSHOUT Raised:(int)bRaised;
-(void)InitWedgeDY:(int)iDXT XB:(int)iDXB Shading:(int)iNShading SHOUT:(int)iSHOUT Raised:(int)bRaised;
-(void)BltFromX:(int)iX Y:(int)iY Source:(CNSShader*)pOther XSrc:(int)iXSrc YSrc:(int)iYSrc DX:(int)iDX DY:(int)iDY;
-(CGImageRef)CreateImageWithColor1:(unsigned int)rgb1 Color2:(unsigned int)rgb2 ShadeIn:(int)iShadeIn ShadeOut:(CNSShader*)pShadeOut Bounds:(CGRect)bounds;
-(CNSEdgeList*)GenEdgeListByElevSunken;
-(CNSEdgeList*)GenEdgeListByElev:(int)iElev;
-(CGRect)FindContentRect:(CNSEdgeList*)pEdgeList;


@end

/*============================================================================*/

    @interface CRegionModifierHZBar : NSObject < CShaderModifier >

/*============================================================================*/
{
    int                             m_iDir;
    int                             m_iSize;
}

-(id)initWithDir:(int)iDir Size:(int)iSize;

@end
