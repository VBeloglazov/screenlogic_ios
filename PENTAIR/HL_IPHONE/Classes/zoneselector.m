//
//  zoneselector.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 4/22/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import "zoneselector.h"
#import "crect.h"
#import "MainTab.h"
#import "SubTab.h"
#import "CustomPage.h"
#import "HLToolBar.h"
#import "IPAD_ViewController.h"
#import "hlm.h"
#import "hlcomm.h"
#import "AppDelegate.h"
#import "CustomPage.h"


#define LABEL_SIZE_IPAD         90
#define LABEL_SIZE_IPOD         32
#define LABEL_TEXTSIZE_IPAD     40
#define LABEL_TEXTSIZE_IPOD     14
#define REFLECTION_OFFSET_IPAD  20
#define REFLECTION_OFFSET_IPOD  5


@implementation CZoneSelectorHorizonView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame

/*============================================================================*/
{
    self = [super initWithFrame:rFrame];
    if (self)
    {
        // Initialization code
		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

        CGFloat colors[8];
        [CHLComm GetRGB:RGB_APP_BACKGROUND To:(colors+0)];
        [CHLComm GetRGB:RGB_ZONEPAGE_LOWER To:(colors+4)];

		m_Gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, 2);
		CGColorSpaceRelease(rgb);
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    CGGradientRelease(m_Gradient);
    [super dealloc];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    printf("ZONEGRADIENT DRAW1\n");

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGPoint ptStart = CGPointMake(rect.origin.y, rect.origin.y);
    CGPoint ptEnd = ptStart;
    ptEnd.y += rect.size.height;
    CGContextDrawLinearGradient(context, m_Gradient, ptStart, ptEnd, 0);
    printf("ZONEGRADIENT DRAW2\n");
}

@end

@implementation CZonePageZoomDelegate

/*============================================================================*/

-(id)initWithSubTabView:(CSubTabView*)pView SubSystemPage:(CSubSystemPage*)pPage ZoneSelector:(CZoneSelector *)pZoneSelector ToolBar:(CHLToolBar*)pToolBar

/*============================================================================*/
{
    if ((self = [super init])) 
    {
        m_pZoneSelector = pZoneSelector;
        m_pPage = pPage;
        m_pView = pView;
        m_pToolBar = pToolBar;
    }

    return self;
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!finished)
        return;
    [m_pView SetSubSystemPage:m_pPage];
    [m_pZoneSelector FadeInReflections];
    [m_pToolBar AnimationComplete];
}
@end

@implementation CSubTabViewOverlay
/*============================================================================*/

-(id)initWithFrame:(CGRect)rect SubTabView:(CSubTabView*)pView

/*============================================================================*/
{
    self = [super initWithFrame:rect];

    if (self) 
    {
        self.contentMode = UIViewContentModeRedraw;
        m_pSubTabView = pView;
        [self displayLayer:self.layer];
    }
    
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    int iDX = self.bounds.size.width;
    int iDY = self.bounds.size.height;
    CGImageRef pRef = [pDelegate GetMainTabImage:iDX DY:iDY Selected:m_bHighlightMode];
    [layer setContents:(id)pRef];
}
/*============================================================================*/

-(void)SetHighlightMode:(BOOL)bNew

/*============================================================================*/
{
    if (bNew == m_bHighlightMode)
        return;
    m_bHighlightMode = bNew;
    [self displayLayer:self.layer];
}
/*============================================================================*/

-(CGRect)ClientRect

/*============================================================================*/
{
    CGRect rClient = self.bounds;
    int iXInset = rClient.size.width / 20;
    int iYInset = rClient.size.height / 20;
    [CRect Inset:&rClient DX:iXInset DY:iYInset];
    return rClient;
}
/*============================================================================*/

-(void)SetClientView:(UIView*)pView

/*============================================================================*/
{
    [self addSubview:pView];
    m_pClientView = pView;
}
/*============================================================================*/

-(void)FreeClientView

/*============================================================================*/
{
    [m_pClientView removeFromSuperview];
    m_pClientView = NULL;
}
/*============================================================================*/

-(void)InvalidateReflection

/*============================================================================*/
{
    [m_pSubTabView InvalidateReflection];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    if (m_pClientView == NULL)
        return;
    CGRect rClient = [self ClientRect];
    [m_pClientView setFrame:rClient];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bNew

/*============================================================================*/
{
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
    [m_pSubTabView InvalidateReflection];
}
@end

@implementation CSubSystemOverlayClientView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Icon:(NSString*)psIcon Text:(NSString*)psText

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        self.opaque = NO;
        [self setUserInteractionEnabled:NO];
        
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pLabel];

        int iLabelSize = [CMainView DEFAULT_TEXT_SIZE];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
            iLabelSize = LABEL_SIZE_IPOD;
        
        [CCustomPage InitStandardLabel:m_pLabel Size:iLabelSize];

        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        m_pLabel.numberOfLines = 2;
        [m_pLabel setText:psText];
        
        int iImageHeight = [CMainView DY_TOOLBAR];
        
        UIImage *pImage = [CCustomPage ScaleImage:[UIImage imageNamed:psIcon] Height:iImageHeight];
        CGSize size = pImage.size;
        m_iImageDX = size.width;
        m_iImageDY = size.height;

        m_pImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        [m_pImage setImage:pImage];
        [self addSubview:m_pImage];
        [m_pImage release];
    }
    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rLabel = [CRect BreakOffBottom:&rThis DY:3*[CMainView DEFAULT_TEXT_SIZE]];
    CGRect rImage = [CRect CenteredRect:rThis DX:m_iImageDX DY:m_iImageDY];
    [m_pImage setFrame:rImage];
    [m_pLabel setFrame:rLabel];
}
@end




@implementation CSubTabReflectionView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rect SubTabView:(CSubTabView*)pView

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_pSubTabView = pView;
        self.contentMode = UIViewContentModeRedraw;
    }

    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)SetImage:(UIImage*)pImage;

/*============================================================================*/
{
    CGImageRef pRef= pImage.CGImage;
    [self.layer setContents:(id)pRef];
}
@end


@implementation CSubTabView
/*============================================================================*/

-(id)initWithMainTab:(CMainTab*)pMain SubTab:(CSubTab*)pSub SubSystemPage:(CSubSystemPage*)pSubSystemPage

/*============================================================================*/
{
    CGRect rFrame = CGRectZero;
    if ((self = [super initWithFrame:rFrame])) 
    {
        m_pSysPage = pSubSystemPage;
        [m_pSysPage retain];
        
        if ([m_pSysPage VisibleInOverview])
        {
            [self addSubview:m_pSysPage];
            [m_pSysPage PrepareScaleView:FALSE];
            [m_pSysPage SetVisible:YES];
            [m_pSysPage setUserInteractionEnabled:NO];
        }

        if ([m_pSysPage WantOverviewOverlay])
        {
            m_pOverlay = [[CSubTabViewOverlay alloc] initWithFrame:[self OverlayRect] SubTabView:self];
            [self addSubview:m_pOverlay];
            [m_pOverlay setOpaque:NO];
            [m_pOverlay release];
        }

        [m_pSysPage SetSubTabView:self];

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [m_pLabel setLineBreakMode:UILineBreakModeWordWrap];
        [m_pLabel setTextAlignment:UITextAlignmentCenter];
        [m_pLabel setNumberOfLines:2];
        [m_pLabel setText:[pSub Name]];

        int iTextSize = [CHLComm GetInt:INT_APP_TEXTHEIGHT] * 75 / 100;
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
            iTextSize = LABEL_TEXTSIZE_IPOD;
    
        [CCustomPage InitStandardLabel:m_pLabel Size:iTextSize];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        
        [self setOpaque:NO];
        m_pMainTab = pMain;
        m_pSubTab = pSub;

        m_pReflectionView = [[CSubTabReflectionView alloc] initWithFrame:CGRectZero SubTabView:self];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pUpdateTimer invalidate];
    [m_pUpdateTimer release];
    [m_pSysPage release];
    [m_pReflectionView release];

    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    if (rBounds.size.width <= 0 || rBounds.size.height <= 0)
        return;

    CGRect rLabel = [self LabelRect];
    [m_pLabel setFrame:rLabel];


    CGRect rOverlay = [self OverlayRect];

    CGRect rThisPage = [self SubSystemRect];

    CGAffineTransform XFormOld = m_pSysPage.transform;
    [m_pSysPage setTransform:CGAffineTransformIdentity];

    CGRect rMainFrame = [self MainFrameRect];

    CGRect rSysPage = CGRectMake(   [CRect XMid:rThisPage]-rMainFrame.size.width/2, 
                                    [CRect YMid:rThisPage]-rMainFrame.size.height/2,
                                    rMainFrame.size.width,
                                    rMainFrame.size.height );

    [m_pSysPage setFrame:rSysPage];
    [m_pSysPage setTransform:XFormOld];

    double dScaleX = rThisPage.size.width / rMainFrame.size.width;
    double dScaleY = rThisPage.size.height / rMainFrame.size.height;
    CGAffineTransform MyScaler = CGAffineTransformMakeScale(dScaleX, dScaleY);
    m_pSysPage.transform = MyScaler;

    if (m_pOverlay != NULL)
    {
        if (m_pOverlay.superview == self)
        {
            [m_pOverlay setFrame:rOverlay];
        }
    }
    [m_pSysPage InitSubTabViewOverlay];
    [self InvalidateReflection];
}
/*============================================================================*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    [pDelegate PlaySoundButtonPress];

    printf("m_pSysPage Page retain Count = %d Should be 1\n", [m_pSysPage retainCount]);
    [self sendActionsForControlEvents:UIControlEventTouchDown];
}
/*============================================================================*/

-(CSubSystemPage*)SubSystemPage

/*============================================================================*/
{
    return m_pSysPage;
}
/*============================================================================*/

-(void)DetachSystemPage

/*============================================================================*/
{
    [m_pSysPage setUserInteractionEnabled:YES];
    [m_pSysPage SetSubTabView:NULL];
    [m_pSysPage release];
    m_pSysPage = NULL;
}
/*============================================================================*/

-(CGRect)SubSystemRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    CGRect rMainFrame = [self MainFrameRect];

    if ([self LandscapeMode])
    {
        [CRect BreakOffTop:&rThis DY:[CZoneSelector LabelSize]];

        int iTabID = [m_pMainTab TabID];
        switch (iTabID)
        {
        case TAB_VIDEO:
            [CRect ConfineToAspect:&rThis DXDY:4.0/3.0];
            break;

        default:
            [CRect ConfineToAspect:&rThis Source:rMainFrame];
            break;
        }
    }
    else
    {
        [CRect BreakOffRight:&rThis DX:[self PortraitLabelWidth]];

        int iTabID = [m_pMainTab TabID];
        switch (iTabID)
        {
        case TAB_VIDEO:
            [CRect ConfineToAspect:&rThis DXDY:4.0/3.0];
            break;

        default:
            [CRect ConfineToAspect:&rThis Source:rMainFrame];
            break;
        }
    }


    return rThis;
}
/*============================================================================*/

-(CGRect)OverlayRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    CGRect rMainFrame = [self MainFrameRect];

    if ([self LandscapeMode])
    {
        [CRect BreakOffTop:&rThis DY:[CZoneSelector LabelSize]];
        CGRect rOverlay = rThis;
        int iTabID = [m_pMainTab TabID];
        switch (iTabID)
        {
        case TAB_VIDEO:
            [CRect ConfineToAspect:&rOverlay Source:rMainFrame];
            break;

        default:
            [CRect ConfineToAspect:&rOverlay DXDY:1.0];
            break;
        }

        return rOverlay;
    }
    else
    {
        [CRect BreakOffRight:&rThis DX:[self PortraitLabelWidth]];
        CGRect rOverlay = rThis;
        int iTabID = [m_pMainTab TabID];
        switch (iTabID)
        {
        case TAB_VIDEO:
            [CRect ConfineToAspect:&rOverlay Source:rMainFrame];
            break;
        }

        [CRect ConfineToAspect:&rOverlay DXDY:1.0];
        return rOverlay;
    }

}
/*============================================================================*/

-(CGRect)LabelRect;

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    
    if ([self LandscapeMode])
    {
        CGRect rLabel = [CRect BreakOffTop:&rThis DY:[CZoneSelector LabelSize]];
        return rLabel;
    }
    else 
    {
        CGRect rLabel = [CRect BreakOffRight:&rThis DX:[self PortraitLabelWidth]];
        return rLabel;
    }

}
/*============================================================================*/

-(void)SetSubSystemPage:(CSubSystemPage*)pPage

/*============================================================================*/
{
    [pPage setUserInteractionEnabled:NO];
    [pPage retain];
    [pPage removeFromSuperview];

    m_pSysPage = pPage;

    if ([m_pSysPage VisibleInOverview])
    {
        [self addSubview:m_pSysPage];
        [m_pSysPage setUserInteractionEnabled:NO];
    }

    printf("m_pSysPage Page retain Count = %d Should be 1\n", [pPage retainCount]);

    if (m_pOverlay != NULL)
    {
        [m_pSysPage setAlpha:0];
        if (m_pOverlay.superview != self)
        {
            [self addSubview:m_pOverlay];
            [m_pOverlay setTransform:CGAffineTransformIdentity];
            [m_pOverlay setFrame:[self OverlayRect]];
            [m_pOverlay setNeedsDisplay];
            [m_pOverlay setNeedsLayout];
        }
    }

    CGRect rSubSys = [self SubSystemRect];

    CGRect rMainFrame = [self MainFrameRect];

    CGRect rSysPage = CGRectMake(   [CRect XMid:rSubSys]-rMainFrame.size.width/2, 
                                    [CRect YMid:rSubSys]-rMainFrame.size.height/2,
                                    rMainFrame.size.width,
                                    rMainFrame.size.height );

    [m_pSysPage setCenter:CGPointMake([CRect XMid:rSubSys], [CRect YMid:rSubSys])];
    [m_pSysPage setFrame:rSysPage];
    double dScaleX = rSubSys.size.width / rMainFrame.size.width;
    double dScaleY = rSubSys.size.height / rMainFrame.size.height;
    CGAffineTransform MyScaler = CGAffineTransformMakeScale(dScaleX, dScaleY);
    m_pSysPage.transform = MyScaler;
}
/*============================================================================*/

-(CSubTabViewOverlay*)GetOverlay

/*============================================================================*/
{
    return m_pOverlay;
}
/*============================================================================*/

-(CSubTabViewOverlay*)CreateOverlay

/*============================================================================*/
{
    if (m_pOverlay == NULL)
    {
        m_pOverlay = [[CSubTabViewOverlay alloc] initWithFrame:[self OverlayRect] SubTabView:self];
        [self addSubview:m_pOverlay];
        [m_pOverlay setOpaque:NO];
        [m_pOverlay release];
        [m_pOverlay setAlpha:0];
    }
    return m_pOverlay;
}
/*============================================================================*/

-(BOOL)LandscapeMode

/*============================================================================*/
{
    UIInterfaceOrientation _i = [IPAD_ViewController MainViewController].interfaceOrientation;
    switch (_i)
    {
    case UIInterfaceOrientationLandscapeLeft:
    case UIInterfaceOrientationLandscapeRight:
        return TRUE;
    
    default:
        break;
    }
    return FALSE;
}
/*============================================================================*/

-(int)PortraitLabelWidth

/*============================================================================*/
{
    return self.bounds.size.width * 60 / 100;
}
/*============================================================================*/

-(CSubTabReflectionView*)ReflectionView

/*============================================================================*/
{
    return m_pReflectionView;
}
/*============================================================================*/

-(UIImage*)GenerateReflectionImage

/*============================================================================*/
{
    CGRect rOverlay = [self OverlayRect];
    UIGraphicsBeginImageContext(rOverlay.size);
    CGContextRef pRef = UIGraphicsGetCurrentContext();
    CGContextSaveGState(pRef);
    CGContextScaleCTM(pRef, 1, -1);
    int iOffset = -rOverlay.origin.y - rOverlay.size.height;
    CGContextTranslateCTM(pRef, -rOverlay.origin.x, iOffset);
    [self.layer renderInContext:pRef];

    CGContextRestoreGState(pRef);
    
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

    CGFloat colors[8];
    [CHLComm GetRGB:RGB_APP_BACKGROUND To:(colors+0)];
    [CHLComm GetRGB:RGB_APP_BACKGROUND To:(colors+4)];
    colors[3] = 0.5;
    colors[7] = 1.0;
    CGGradientRef pGradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, 2);
    CGColorSpaceRelease(rgb);


    CGPoint ptStart = CGPointMake(0,0);
    CGPoint ptEnd   = CGPointMake(0,rOverlay.size.height);
    CGContextDrawLinearGradient(pRef, pGradient, ptStart, ptEnd, 0);
    CGGradientRelease(pGradient);


    UIImage *pImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return pImage;
}
/*============================================================================*/

-(void)InvalidateReflection

/*============================================================================*/
{
    UIImage *pImage = [self GenerateReflectionImage];
    [m_pReflectionView SetImage:pImage];

    if (m_pUpdateTimer != NULL)
    {
        m_iUpdateRepeat--;
        if (m_iUpdateRepeat <= 0)
        {
            [m_pUpdateTimer invalidate];
            [m_pUpdateTimer release];
            m_pUpdateTimer = NULL;
        }
    }
}
/*============================================================================*/

-(void)BeginUpdateReflection:(int)iDuration Every:(int)iRate

/*============================================================================*/
{
    [m_pUpdateTimer invalidate];
    [m_pUpdateTimer release];

    double dRate = (double)iRate / 1000.f;

    m_iUpdateRepeat = iDuration / iRate + 1;
    m_pUpdateTimer = [[NSTimer scheduledTimerWithTimeInterval:dRate target:self selector:@selector(InvalidateReflection) userInfo:nil repeats:YES] retain];
}
/*============================================================================*/

-(CGRect)MainFrameRect

/*============================================================================*/
{
    UIInterfaceOrientation Orientation = UIInterfaceOrientationPortrait;
    CGRect rParentFrame = self.superview.bounds;
    if (rParentFrame.size.width > rParentFrame.size.height)
        Orientation = UIInterfaceOrientationLandscapeLeft;

    CGRect rMain = [CRect ScreenRect:Orientation];
    [CRect BreakOffTop:&rMain DY:[CMainView DY_TOOLBAR]];
    return rMain;
}
@end

@implementation CZoneSelector

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame MainTab:(CMainTab*)pMainTab SubPageIndex:(int)iIndex ViewController:(IPAD_ViewController*)pCtlr

/*============================================================================*/
{
    if ((self = [super initWithFrame:rFrame])) 
    {
        // Initialization code
        m_pMainTab = pMainTab;
        m_pTabs = [[NSMutableArray alloc] init];

        [self setOpaque:NO];
        [self setBackgroundColor:[UIColor clearColor]];

        m_pHorizon = [[CZoneSelectorHorizonView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pHorizon];
        [m_pHorizon release];

        m_pScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self addSubview:m_pScrollView];
        [m_pScrollView setOpaque:NO];
        [m_pScrollView setBackgroundColor:[UIColor clearColor]];
        [m_pScrollView release];
        int iNTabs = [pMainTab NSubTabs];
        for (int i = 0; i < iNTabs; i++)
        {
            CSubTab *pSub = [pMainTab SubTab:i];
            CSubSystemPage *pSubSystemPage = NULL;
            if (i != iIndex)
                pSubSystemPage = [pMainTab CreateUserPage:CGRectZero SubTab:pSub MainTabPage:NULL];
            CSubTabView *pView = [[CSubTabView alloc] initWithMainTab:pMainTab SubTab:pSub SubSystemPage:pSubSystemPage];
            [pSubSystemPage release];
            [m_pTabs addObject:pView];
            [m_pScrollView addSubview:pView];
            [pView addTarget:pCtlr action:@selector(ZoomToSubPageView:) forControlEvents:UIControlEventTouchDown];
            [pView release];

            CSubTabReflectionView *pReflection = [pView ReflectionView];
            [m_pScrollView addSubview:pReflection];
        }
        
        m_iDefaultIndex = iIndex;
    }

    printf("ZoneSelected Created\n");
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    printf("ZONEPAGE DEALLOC\n");

    [m_pTabs release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iNVis = 6;
    int iTabID = [m_pMainTab TabID];




    if (self.bounds.size.width > self.bounds.size.height)
    {
        //
        // Landscape Mode, like TS7
        // 


        if (iTabID == TAB_VIDEO)
            iNVis = 4;

        iNVis = MIN(iNVis, [m_pTabs count]);
        int iDXSub = self.bounds.size.width / iNVis;
        int iDYSub = self.bounds.size.height / 2 + [CZoneSelector LabelSize];

        int iHorizonY1 = self.bounds.size.height/2;;

        switch (iTabID)
        {
        case TAB_VIDEO:
            break;

        default:
            {
                // Limit so doesn't look silly
                iDXSub = MIN(iDXSub, 200);
            }
            break;
        }

        for (int i = 0; i < [m_pTabs count]; i++)
        {
            CSubTabView *pView = (CSubTabView*)[m_pTabs objectAtIndex:i];
            CGRect rView = CGRectMake(i * iDXSub, [CRect YMid:self.bounds]-iDYSub/2, iDXSub, iDYSub);
            [CRect Inset:&rView DX:10 DY:0];
            [pView setFrame:rView];
            CGRect rSubSystem = [pView OverlayRect];
            
            iHorizonY1 = pView.frame.origin.y + rSubSystem.origin.y + rSubSystem.size.height;
            
            CSubTabReflectionView *pReflection = [pView ReflectionView];
            rSubSystem.origin.y = iHorizonY1 + [CZoneSelector ReflectionOffset];
            rSubSystem.origin.x += pView.frame.origin.x;
            [pReflection setFrame:rSubSystem];
            [pReflection setAlpha:1.0];
        }

        int iHorizonDY = self.bounds.size.height * [CHLComm GetInt:INT_HORIZON_DY] / 500;
        iHorizonY1 -= [CHLComm GetInt:INT_MAINTAB_ROUT];
        CGRect rHorizon = CGRectMake(0, iHorizonY1-iHorizonDY, self.bounds.size.width, iHorizonDY);
        [m_pHorizon setFrame:rHorizon];
        [m_pHorizon setAlpha:1];

        int iDXAll = iDXSub * [m_pTabs count];
        m_pScrollView.contentSize = CGSizeMake(iDXAll, self.bounds.size.height);

        if (iDXAll < self.bounds.size.width)
        {
            int iInsetX = (self.bounds.size.width - iDXAll)/2;
            CGRect rThis = self.bounds;
            [CRect Inset:&rThis DX:iInsetX DY:0];
            m_pScrollView.frame = rThis;
        }
        else
        {
            m_pScrollView.frame = self.bounds;
        }
        
       
        if (m_iDefaultIndex >= 0)
        {
            int iXRDefault = (m_iDefaultIndex + 1 ) * iDXSub;

            int iXRMax = self.bounds.size.width;
            if (iXRDefault > iXRMax)
            {
                [m_pScrollView setContentOffset:CGPointMake(iXRDefault-iXRMax, 0)];
            }
            m_iDefaultIndex = -1;
        }
    }
    else 
    {
        //
        // Portrait Mode, like iPod
        // 
        iNVis = MIN(iNVis, [m_pTabs count]);
        int iDXSub = self.bounds.size.width;
        int iDYSub = self.bounds.size.height / iNVis;

        switch (iTabID)
        {
        case TAB_VIDEO:
            break;

        default:
            {
                // Limit so doesn't look silly
                iDYSub = MIN(iDYSub, 200);
            }
            break;
        }

        m_pScrollView.contentSize = CGSizeMake(self.bounds.size.width, iDYSub * [m_pTabs count]);


        int iDYAll = iDYSub * [m_pTabs count];


        if (iDYAll < self.bounds.size.height)
        {
            int iInsetY = (self.bounds.size.height - iDYAll)/2;
            CGRect rThis = self.bounds;
            [CRect Inset:&rThis DX:0 DY:iInsetY];
            m_pScrollView.frame = rThis;
        }
        else
        {
            m_pScrollView.frame = self.bounds;
        }

        if (m_iDefaultIndex >= 0)
        {
            int iYBDefault = (m_iDefaultIndex + 1 ) * iDYSub;

            int iYBMax = self.bounds.size.height;
            if (iYBDefault > iYBMax)
            {
                [m_pScrollView setContentOffset:CGPointMake(0, iYBDefault-iYBMax)];
            }
            m_iDefaultIndex = -1;
        }

        for (int i = 0; i < [m_pTabs count]; i++)
        {
            CSubTabView *pView = (CSubTabView*)[m_pTabs objectAtIndex:i];
            CGRect rView = CGRectMake(0, i * iDYSub, iDXSub, iDYSub);
            [CRect Inset:&rView DX:10 DY:5];
            [pView setFrame:rView];

            CSubTabReflectionView *pReflection = [pView ReflectionView];
            [pReflection setAlpha:0.0];

        }
        [m_pHorizon setFrame:self.bounds];
        [m_pHorizon setAlpha:0];
    }
}
/*============================================================================*/

-(CToolBarClientView*)CreateMenuView:(CGRect)rView ViewController:(IPAD_ViewController*)pCtlr Flags:(int)iFlags

/*============================================================================*/
{
    return NULL;
}
/*============================================================================*/

-(CSubTabView*)SubTabView:(int)iIndex

/*============================================================================*/
{
    return (CSubTabView*)[m_pTabs objectAtIndex:iIndex];
}
/*============================================================================*/

-(void)FadeInReflections;

/*============================================================================*/
{
    for (int i = 0; i < [m_pTabs count]; i++)
    {
        CSubTabView *pView = (CSubTabView*)[m_pTabs objectAtIndex:i];
        [pView InvalidateReflection];
        [[pView ReflectionView] setAlpha:0];
    }

	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];

    for (int i = 0; i < [m_pTabs count]; i++)
    {
        CSubTabView *pView = (CSubTabView*)[m_pTabs objectAtIndex:i];
        [pView InvalidateReflection];
        [[pView ReflectionView] setAlpha:1.0];
    }

	[UIView commitAnimations];
}
/*============================================================================*/

-(void)SetReflectionAlpha:(double)dA

/*============================================================================*/
{
    for (int i = 0; i < [m_pTabs count]; i++)
    {
        CSubTabView *pView = (CSubTabView*)[m_pTabs objectAtIndex:i];
        [[pView ReflectionView] setAlpha:dA];
    }
}
/*============================================================================*/

+(int)LabelSize

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return LABEL_SIZE_IPAD;
    return LABEL_SIZE_IPOD;
}
/*============================================================================*/

+(int)ReflectionOffset

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return REFLECTION_OFFSET_IPAD;
    return REFLECTION_OFFSET_IPOD;
}
/*============================================================================*/

-(BOOL)WantSystemMenu

/*============================================================================*/
{
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        return FALSE;
    return TRUE;
}

@end
