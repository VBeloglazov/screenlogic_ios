/*
--------------------------------------------------------------------------------

    SETSERVER.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#ifdef IPHONE
    #include "hlserver.h"
    #include "hlmsg.h"
    #include "hlquery.h"
    #include "hlwav.h"
#else
    #include "helpers.h"
    #include "hlserver.h"
    #include "hlwindow.h"
    #include "hlquery.h"
    #include "hlmsg.h"
    #include "hlwav.h"
    #include "main.h"
    #include "shadepoly.h"
    #include "shaderect.h"
    #include "shadeassem.h"
    #include "hlfile.h"
    #include "fontserver.h"
    #include "schema.h"
    #include "gifdecode.h"
    #include "pngdecode.h"
    #include "vdecode.h"
#endif

#if defined(CRYSTALPAD) | defined(HLOSD)
    #include "platform.h"
#endif

#include "setserver.h"


#ifdef IPHONE
    CSetServer               *g_pSetServer = NULL;
#endif

/*============================================================================*/

    CSetServer::CSetServer()

/*============================================================================*/
{
#ifdef IPHONE
    m_pButtonWAV        = NULL;
#else
    m_pConnectWAV       = NULL;
    m_pDisconnectWAV    = NULL;
#endif
    m_iHostType         = HLM_GATEWAY_FULL;
    m_iFlags            = 0;

#ifdef HLOSD
    m_pTexture          = NULL;
#endif

    LoadDefaults();

    g_pHLServer->AddSocketSink(this);
}
/*============================================================================*/

    CSetServer::~CSetServer()

/*============================================================================*/
{
#ifdef HLOSD
    delete m_pTexture;
#endif

    g_pHLServer->RemoveSocketSink(this);

#ifdef IPHONE
    delete m_pButtonWAV;
#else
    CHLPos *pPos = m_WAVList.GetHeadPosition();
    while (pPos != NULL)
    {
        CHLWAV *pWav = (CHLWAV*) pPos->Object();
        pPos = pPos->Next();

        delete pWav;
    }
#endif
}
/*============================================================================*/

    BOOL                        CSetServer::FullGateway()

/*============================================================================*/
{
    switch (m_iHostType)
    {
    case HLM_GATEWAY_FULL:
    case HLM_GATEWAY_FULL_X86C:
    case HLM_GATEWAY_FULL_ARMV4IA:
        break;

    case HLM_GATEWAY_BRICK_MASTER:
    case HLM_GATEWAY_BRICK_SLAVE:
    case HLM_GATEWAY_BRICK_APRILAIRE:
    case HLM_GATEWAY_BRICK_PASSTHROUGH:
        {
            return FALSE;
        }
        break;    
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CSetServer::OEMGateway()

/*============================================================================*/
{
    switch (m_iHostType)
    {
    case HLM_GATEWAY_FULL:
    case HLM_GATEWAY_FULL_X86C:
    case HLM_GATEWAY_FULL_ARMV4IA:
    case HLM_GATEWAY_BRICK_PASSTHROUGH:
        break;

    case HLM_GATEWAY_BRICK_MASTER:
    case HLM_GATEWAY_BRICK_SLAVE:
    case HLM_GATEWAY_BRICK_APRILAIRE:
        return TRUE;
    }

    return FALSE;
}
/*============================================================================*/

    void                        CSetServer::GlobalOpts(
    int                         iNew)

/*============================================================================*/
{
    m_iGlobalOpts = iNew;     
}
/*============================================================================*/

    int                         CSetServer::GetInt(int iIntType)

/*============================================================================*/
{
    if (iIntType < 0 || iIntType >= N_INT_TYPES)
        return 0;
    return m_pInts[iIntType];
}
/*============================================================================*/

    void                        CSetServer::SetInt(int iIntType, int iNew)
    
/*============================================================================*/
{
    if (iIntType < 0 || iIntType >= N_INT_TYPES)
        return;
    m_pInts[iIntType] = iNew;
}
/*============================================================================*/

    COLORREF                    CSetServer::GetRGB(int iRGBType)

/*============================================================================*/
{
    if (iRGBType < 0 || iRGBType >= N_RGB_TYPES)
        return 0;
    return m_pRGB[iRGBType];
}
/*============================================================================*/

    void                        CSetServer::SetRGB(int iRGBType, COLORREF rgbNew)

/*============================================================================*/
{
    if (iRGBType < 0 || iRGBType >= N_RGB_TYPES)
        return;
    m_pRGB[iRGBType] = rgbNew;
}
/*============================================================================*/

    HL_PIXEL                    CSetServer::GetRGB16(int iRGBType)

/*============================================================================*/
{
    return RGBTOHLPIXEL(GetRGB(iRGBType));
}
/*============================================================================*/

    void                        CSetServer::SetFontData(
    CHLString                   sFontFile,
    CHLString                   sFontFam )

/*============================================================================*/
{
    m_sFontFile     = sFontFile;
    m_sFontFam      = sFontFam;
}
#ifdef HLOSD
/*============================================================================*/

    CGLDIB                  *CSetServer::GetTexture()

/*============================================================================*/
{
    if (m_pTexture == NULL)
        return NULL;
    return m_pTexture->DIB();
}
#endif
/*============================================================================*/

    void                        CSetServer::SetTexture(
    CHLString                   sNew )

/*============================================================================*/
{
    m_sTexture = sNew;

#ifdef HLOSD
    if (sNew.GetLength() < 1)
    {
        delete m_pTexture;
        m_pTexture = NULL;
        return;
        
    }

    if (m_pTexture != NULL)
    {
        if (m_pTexture->Name().CompareNoCase(sNew) == 0)
            return;
        delete m_pTexture;
        m_pTexture = NULL;
    }

    m_pTexture = new CTexture(m_sTexture);
#endif
}
/*============================================================================*/

    int                         CSetServer::ROutByType(int iWndType)

/*============================================================================*/
{
    switch (iWndType)
    {
    case WND_NONE:
    case WND_APP_LABEL:
    case WND_LIST_CONTROL:
        break;

    case WND_BUTTON:    return GetInt(INT_BUTTON_ROUT); 
    case WND_PAGE_TAB:  return GetInt(INT_TAB_ROUT);

    default:
        break;
    }

    return 0;
}
/*============================================================================*/

    int                         CSetServer::ShadeInByType(int iWndType)

/*============================================================================*/
{
    if (iWndType != WND_BUTTON)
        return 0;
    return GetInt(INT_BUTTON_SHADEIN);
}
/*============================================================================*/

    int                         CSetServer::ShadeOutByType(int iWndType)

/*============================================================================*/
{
    if (iWndType != WND_BUTTON)
        return 0;
    return GetInt(INT_BUTTON_SHADEOUT);
}
/*============================================================================*/

    int                         CSetServer::TextHeightByType(int iWndType)

/*============================================================================*/
{
    switch (iWndType)
    {
    case WND_NONE:
    case WND_APP_LABEL:
        return GetInt(INT_LABEL_TEXTHEIGHT);

    case WND_LIST_CONTROL:
        return GetInt(INT_LIST_TEXTHEIGHT);

    case WND_PAGE_TAB:  
    case WND_BUTTON:    
        return GetInt(INT_BUTTON_TEXTHEIGHT);
    }

    return 0;
}
/*============================================================================*/

    COLORREF                    CSetServer::ColorByType(int iWndType)

/*============================================================================*/
{
    switch (iWndType)
    {
    case WND_NONE:
    case WND_APP_LABEL:
    case WND_LIST_CONTROL:
        return GetRGB(RGB_LIST_AREA);

    case WND_PAGE_TAB:  
        return GetRGB(RGB_APP_BACKGROUND);

    case WND_BUTTON:    
        return GetRGB(RGB_BUTTON);
    }

    return 0;
}
/*============================================================================*/

    COLORREF                    CSetServer::TextColorByType(int iWndType)

/*============================================================================*/
{
    switch (iWndType)
    {
    case WND_NONE:
    case WND_APP_LABEL:
    case WND_LIST_CONTROL:
        break;

    case WND_PAGE_TAB:  
    case WND_BUTTON:    
        return GetRGB(RGB_BUTTON_TEXT);
    }

    return GetRGB(RGB_APP_TEXT);
}
/*============================================================================*/

    void                        CSetServer::SinkMessage(
    CHLMSG                      *pMSG )

/*============================================================================*/
{
    static BOOL bBusy = FALSE;
    if (bBusy)
        return;
    bBusy = TRUE;

    switch (pMSG->MessageID())
    {
    case HLM_SETSERVER_SETTINGSCHANGED:
        {
            LoadSettings();
            #ifndef IPHONE
            #if defined(CRYSTALPAD) | defined(HLOSD)
                if (!g_pFontServer->WaitingForFile())
                    g_pMainWindow->SettingsChanged(-1);
            #endif
            #endif
        }
        break;

    case HLM_SYSCONFIG_UICHANGED:
        {
            int iConfigID = 0;
            pMSG->GetInt(&iConfigID);

            #ifndef IPHONE
            #if defined(CRYSTALPAD) | defined(HLOSD)
                g_pMainWindow->SettingsChanged(iConfigID);
            #endif
            #endif
        }
        break;
    }


    bBusy = FALSE;
}
/*============================================================================*/

    void                        CSetServer::ConnectionChanged(
    BOOL                        bNowConnected )

/*============================================================================*/
{
    #if defined(CRYSTALPAD) | defined(HLOSD)

        #ifndef IPHONE
            if (g_pMainWindow == NULL)
                return;

            if (bNowConnected)
            {
                if (m_pConnectWAV == NULL && !g_pHLServer->RemoteMode())
                    m_pConnectWAV = RegisterWAVFile(m_sConnectWAV);
                if (m_pDisconnectWAV == NULL && !g_pHLServer->RemoteMode())
                    m_pDisconnectWAV  = RegisterWAVFile(m_sDisconnectWAV);

                if (m_pConnectWAV != NULL)
                    m_pConnectWAV->Play();
            }
            else
            {
                if (m_pDisconnectWAV != NULL)
                    m_pDisconnectWAV->Play();
            }
        #endif

        if (bNowConnected && !(m_iFlags & SETSERVER_LOADED))
        {
            LoadSettings();
            if (m_iFlags & SETSERVER_LOADED)
            {
                #ifndef IPHONE
                    if (g_pMainWindow != NULL)
                        g_pMainWindow->SettingsChanged(-1);
                #endif
            }
        }
    #endif
}
/*============================================================================*/

    void                        CSetServer::LoadSettings()

/*============================================================================*/
{
    // Get the settings...
    m_iFlags &= ~SETSERVER_LOADED;
    CHLQuery q;

    #ifdef HLCONFIG
        q.SetTimeout(30000);
    #else
        #ifdef HLOSD
            // Can take a long time if box is first starting up
            q.SetTimeout(30000);
        #else
            q.SetTimeout(5000);
        #endif
    #endif

    CHLMSG msgQ((short)0, HLM_SETSERVER_GETDEFAULTSCHEMEQ);
    CHLMSG *pMSGA = NULL;

    #ifdef IPHONE
        msgQ.PutInt(HL_800x600);
    #else
        #ifdef HLOSD
            msgQ.PutInt(HL_OSD_RES);
        #else
            #if defined(CRYSTALPAD)
                msgQ.PutInt(RES_INT());
            #else
                msgQ.PutInt(HL_800x600);
            #endif
        #endif
    #endif

    if (!q.AskQuestion(&pMSGA, &msgQ))
        return;

    LoadSettings(pMSGA);
}
#ifdef HLCONFIG
/*============================================================================*/

    BOOL                        CSetServer::LoadSettings(
    int                         iResMode,
    int                         iID  )

/*============================================================================*/
{
    // Get the settings...
    m_iFlags &= ~SETSERVER_LOADED;
    CHLQuery q;
    q.SetTimeout(5000);
    CHLMSG msgQ((short)0, HLM_SETSERVER_GETSCHEMEBYIDQ);
    CHLMSG *pMSGA = NULL;
    msgQ.PutInt(iResMode);
    msgQ.PutInt(iID);
    if (!q.AskQuestion(&pMSGA, &msgQ))
        return FALSE;

    return LoadSettings(pMSGA);
}
/*============================================================================*/

    BOOL                        CSetServer::SaveSettings(
    int                         iResMode,
    int                         iID )

/*============================================================================*/
{
    CHLMSG msg(0, HLM_SETSERVER_SETSCHEMEBYIDQ, 4096);
    msg.PutInt(iResMode);
    msg.PutInt(iID);

    msg.PutInt(m_iGlobalOpts);
    msg.PutString(m_sFontFile);
    msg.PutString(m_sFontFam);
    
    msg.PutString("");
    msg.PutString("");
    msg.PutString("");
    msg.PutString("");
    
    msg.PutString(m_sTexture);
    msg.PutString(m_sPressWAV);
    msg.PutString(m_sReleaseWAV);
    msg.PutString(m_sConnectWAV);
    msg.PutString(m_sDisconnectWAV);

    msg.PutInt(N_INT_TYPES);
    int i = 0;
    for (i = 0; i < N_INT_TYPES; i++)
        msg.PutInt(GetInt(i));
    
    msg.PutInt(N_RGB_TYPES);
    for (i = 0; i < N_RGB_TYPES; i++)
        msg.PutColor(GetRGB(i));

    CHLQuery q;
    return q.AskQuestion(NULL, &msg);
}
#endif
/*============================================================================*/

    BOOL                        CSetServer::LoadSettings(
    CHLMSG                      *pMSG )

/*============================================================================*/
{
    m_iFlags |= SETSERVER_LOADED;

    if (!pMSG->GetInt(&m_iHostType))
        return FALSE;

    LoadDefaults();

    switch (m_iHostType)
    {
    case HLM_GATEWAY_FULL:
    case HLM_GATEWAY_FULL_X86C:
    case HLM_GATEWAY_FULL_ARMV4IA:
        break;

    case HLM_GATEWAY_BRICK_MASTER:
    case HLM_GATEWAY_BRICK_SLAVE:
    case HLM_GATEWAY_BRICK_APRILAIRE:
    case HLM_GATEWAY_BRICK_PASSTHROUGH:
        break;    
    }

    pMSG->GetInt(&m_iGlobalOpts);
    pMSG->GetString(&m_sFontFile);
    pMSG->GetString(&m_sFontFam);

    CHLString sJunk;
    pMSG->GetString(&sJunk);
    pMSG->GetString(&sJunk);
    pMSG->GetString(&sJunk);
    pMSG->GetString(&sJunk);

    pMSG->GetString(&m_sTexture);
    pMSG->GetString(&m_sPressWAV);
    pMSG->GetString(&m_sReleaseWAV);
    pMSG->GetString(&m_sConnectWAV);
    pMSG->GetString(&m_sDisconnectWAV);


    int iNInts = 0;
    pMSG->GetInt(&iNInts);
    int i = 0;
    for (i = 0; i < iNInts; i++)
    {
        int iVal = 0;
        pMSG->GetInt(&iVal);
        SetInt(i, iVal);
    }
    
    int iNRGB = 0;
    pMSG->GetInt(&iNRGB);
    for (i = 0; i < iNRGB; i++)
    {
        COLORREF rgb = 0x0000;
        pMSG->GetColor(&rgb);
        SetRGB(i, rgb);
    }

    int iCodePage = 0;
    int iFontFileSize = 0;
    pMSG->GetInt(&iCodePage);
    pMSG->GetInt(&iFontFileSize);

    #ifndef IPHONE
    #if defined(CRYSTALPAD) | defined(HLOSD)
        g_pFontServer->SetFontInfo(m_sFontFile, m_sFontFam, iFontFileSize);
    #endif
    #endif

    #ifdef IPHONE
        delete m_pButtonWAV;
        m_pButtonWAV = new CHLWAV(m_sPressWAV);
    #endif

    SetTexture(m_sTexture);

    return TRUE;
}
#ifndef IPHONE
/*============================================================================*/

    CHLWAV                      *CSetServer::RegisterWAVFile(
    CHLString                   sWAVFile )

/*============================================================================*/
{
    CHLWAV *pWAV = WAVByFileName(sWAVFile);

    if (pWAV == NULL)
    {
        pWAV = new CHLWAV(sWAVFile);
        m_WAVList.AddTail(pWAV);
    }

    return pWAV;
}
/*============================================================================*/

    CHLWAV                      *CSetServer::WAVByFileName(
    CHLString                   sName )

/*============================================================================*/
{
    CHLPos *pPos = m_WAVList.GetHeadPosition();
    while (pPos != NULL)
    {
        CHLWAV *pWAV = (CHLWAV*) pPos->Object();
        pPos = pPos->Next();
        if (pWAV->Name().CompareNoCase(sName) == 0)
            return pWAV;
    }

    return NULL;
}
#endif
#ifdef HLCONFIG
/*============================================================================*/

    BOOL                        CSetServer::Read(
    CHLFile                     *pFile, 
    int                         iSchema)

/*============================================================================*/
{
    if (!pFile->ReadInt(&m_iGlobalOpts))        return FALSE;
    if (!pFile->ReadString(&m_sFontFile))       return FALSE;
    if (!pFile->ReadString(&m_sFontFam))        return FALSE;

    if (iSchema >= SCHEMA_290 && iSchema < SCHEMA_349)
    {
        CHLString sUnused;        
        if (!pFile->ReadString(&sUnused))       return FALSE;
        if (!pFile->ReadString(&sUnused))        return FALSE;
        if (!pFile->ReadString(&sUnused))       return FALSE;
        if (!pFile->ReadString(&sUnused))        return FALSE;
    }

    if (iSchema >= SCHEMA_297)
    {
        if (!pFile->ReadString(&m_sTexture))
            return FALSE;
    }

    if (!pFile->ReadString(&m_sPressWAV))       return FALSE;
    if (!pFile->ReadString(&m_sReleaseWAV))     return FALSE;
    if (!pFile->ReadString(&m_sConnectWAV))     return FALSE;
    if (!pFile->ReadString(&m_sDisconnectWAV))  return FALSE;

    int iNInts = 0;
    if (!pFile->ReadInt(&iNInts))
        return FALSE;

    int i = 0;
    for (i = 0; i < iNInts; i++)
    {
        int iVal = 0;
        if (!pFile->ReadInt(&iVal))
            return FALSE;
        SetInt(i, iVal);
    }
    
    int iNRGB = 0;
    if (!pFile->ReadInt(&iNRGB))
        return FALSE;
    for (i = 0; i < iNRGB; i++)
    {
        COLORREF rgb = 0x0000;
        if (!pFile->ReadBytes(&rgb, sizeof(COLORREF)))
            return FALSE;
        SetRGB(i, rgb);
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CSetServer::Write(
    CHLFile                     *pFile, 
    int                         iSchema)

/*============================================================================*/
{
    if (!pFile->WriteInt(m_iGlobalOpts))        return FALSE;

    if (!pFile->WriteString(m_sFontFile))       return FALSE;
    if (!pFile->WriteString(m_sFontFam))        return FALSE;
    if (!pFile->WriteString(m_sTexture))        return FALSE;

    if (!pFile->WriteString(m_sPressWAV))       return FALSE;
    if (!pFile->WriteString(m_sReleaseWAV))     return FALSE;
    if (!pFile->WriteString(m_sConnectWAV))     return FALSE;
    if (!pFile->WriteString(m_sDisconnectWAV))  return FALSE;

    if (!pFile->WriteInt(N_INT_TYPES))
        return FALSE;

    int i = 0;
    for (i = 0; i < N_INT_TYPES; i++)
    {
        int iVal = GetInt(i);
        if (!pFile->WriteInt(iVal))
            return FALSE;
    }
    
    if (!pFile->WriteInt(N_RGB_TYPES))
        return FALSE;
    for (i = 0; i < N_RGB_TYPES; i++)
    {
        COLORREF rgb = GetRGB(i);
        if (!pFile->WriteBytes(&rgb, sizeof(COLORREF)))
            return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    void                        CSetServer::AddSounds(
    CHLObList                   *pList)

/*============================================================================*/
{
    pList->AddTail(new CHLString(m_sPressWAV));
    pList->AddTail(new CHLString(m_sReleaseWAV));
    pList->AddTail(new CHLString(m_sConnectWAV));
    pList->AddTail(new CHLString(m_sDisconnectWAV));
}
/*============================================================================*/

    void                        CSetServer::AddTextures(
    CHLObList                   *pList)

/*============================================================================*/
{
    pList->AddTail(new CHLString(m_sTexture));
}
/*============================================================================*/

#endif
/*============================================================================*/

    void                        CSetServer::LoadDefaults()

/*============================================================================*/
{
    m_sFontFile     = _T("");
    m_sFontFam      = _T("TAHOMA");

    m_sTexture      = _T("");

#ifdef HLOSD
    SetRGB(RGB_OSD_BG_UL,                   RGBDEF_OSD_BG_UL);
    SetRGB(RGB_OSD_BG_UR,                   RGBDEF_OSD_BG_UR);
    SetRGB(RGB_OSD_BG_LL,                   RGBDEF_OSD_BG_LL);
    SetRGB(RGB_OSD_BG_LR,                   RGBDEF_OSD_BG_LR);
    SetRGB(RGB_OSD_HDR_UL,                  RGBDEF_OSD_HDR_UL);  
    SetRGB(RGB_OSD_HDR_UR,                  RGBDEF_OSD_HDR_UR);
    SetRGB(RGB_OSD_HDR_LL,                  RGBDEF_OSD_HDR_LL);
    SetRGB(RGB_OSD_HDR_LR,                  RGBDEF_OSD_HDR_LR);
    SetRGB(RGB_OSD_TEXT,                    RGBDEF_OSD_TEXT);
    SetRGB(RGB_OSD_GROUP1,                  RGBDEF_OSD_GROUP2);
    SetRGB(RGB_OSD_GROUP2,                  RGBDEF_OSD_GROUP2);

    SetInt(INT_OSD_HEADER_OPAC1,            INTDEF_OSD_HEADER_OPAC1);
    SetInt(INT_OSD_HEADER_OPAC2,            INTDEF_OSD_HEADER_OPAC2);
    SetInt(INT_OSD_GROUP_OPAC1,             INTDEF_OSD_GROUP_OPAC1);
    SetInt(INT_OSD_GROUP_OPAC2,             INTDEF_OSD_GROUP_OPAC2);
    SetInt(INT_OSD_CURSOR_HALO,             INTDEF_OSD_CURSOR_HALO);
    SetInt(INT_OSD_CURSOR_RAD,              INTDEF_OSD_CURSOR_RAD);                       
    SetInt(INT_OSD_SCROLL_SIZE,             INTDEF_OSD_SCROLL_SIZE);
    SetInt(INT_OSD_GROUP_PAD,               INTDEF_OSD_GROUP_PAD);
    SetInt(INT_OSD_SUMMARY_YPOS,            INTDEF_OSD_SUMMARY_YPOS);
    SetInt(INT_OSD_MAIN_YPOS,               INTDEF_OSD_MAIN_YPOS);
    SetInt(INT_OSD_HEADER_SIZE,             INTDEF_OSD_HEADER_SIZE);
    SetInt(INT_OSD_HEADER_INSET,            INTDEF_OSD_HEADER_INSET);
    SetInt(INT_OSD_OSCAN_LEFT,              INTDEF_OSD_OSCAN_LEFT);
    SetInt(INT_OSD_OSCAN_RIGHT,             INTDEF_OSD_OSCAN_RIGHT);
    SetInt(INT_OSD_OSCAN_TOP,               INTDEF_OSD_OSCAN_TOP);
    SetInt(INT_OSD_OSCAN_BOTTOM,            INTDEF_OSD_OSCAN_BOTTOM);
#else
    SetInt(INT_BUTTON_SHADEIN,              INTDEF_BUTTON_SHADEIN);
    SetInt(INT_BUTTON_SHADEOUT,             INTDEF_BUTTON_SHADEOUT);
    SetInt(INT_BUTTON_TEXTHEIGHT,           INTDEF_BUTTON_TEXTHEIGHT);
    SetInt(INT_BUTTON_ROUT,                 INTDEF_BUTTON_ROUT);
    SetInt(INT_APP_TEXTHEIGHT,              INTDEF_APP_TEXTHEIGHT);
    SetInt(INT_APP_HEADERSIZE,              INTDEF_APP_HEADERSIZE);
    SetInt(INT_APP_HEADERINSET,             INTDEF_APP_HEADERINSET);
    SetInt(INT_LIST_TEXTHEIGHT,             INTDEF_LIST_TEXTHEIGHT);
    SetInt(INT_LABEL_TEXTHEIGHT,            INTDEF_LABEL_TEXTHEIGHT);
    SetInt(INT_TAB_ROUT,                    INTDEF_TAB_ROUT);
    SetInt(INT_SCROLL_WIDTH,                INTDEF_SCROLL_WIDTH);
    SetInt(INT_FONT_WEIGHT,                 INTDEF_FONT_WEIGHT);
    SetInt(INT_ALPHA_RMINOR,                INTDEF_ALPHA_RMINOR);
    SetInt(INT_ALPHAMAX,                    INTDEF_ALPHAMAX);
    SetInt(INT_ALPHA_UPPER_RATIO,           INTDEF_ALPHA_UPPER_RATIO);
    SetInt(INT_ALPHA_BOTTOM_EDGE,           INTDEF_ALPHA_BOTTOM_EDGE);
    SetInt(INT_MAINTAB_ROUT,                INTDEF_MAINTAB_ROUT);
    SetInt(INT_MAINTAB_ALPHAMAX,            INTDEF_MAINTAB_ALPHAMAX);
    SetInt(INT_MAINTAB_ALPHA_UPPER_RATIO,   INTDEF_MAINTAB_ALPHA_UPPER_RATIO );
    SetInt(INT_MAINTAB_INSET,               INTDEF_MAINTAB_INSET );
    SetInt(INT_MAINTAB_FLAGS,               INTDEF_MAINTAB_FLAGS );
    SetInt(INT_ALPHA_SUBTLE_SELECT_TOP,     INTDEF_ALPHA_SUBTLE_SELECT_TOP);
    SetInt(INT_ALPHA_SUBTLE_SELECT_BTM,     INTDEF_ALPHA_SUBTLE_SELECT_BTM);
    SetInt(INT_NAVBAR_INSET,                INTDEF_NAVBAR_INSET);
    SetInt(INT_PAGETAB_INSET,               INTDEF_PAGETAB_INSET);
    SetInt(INT_MAINTAB_RMINOR,              INTDEF_MAINTAB_RMINOR);
    SetInt(INT_EDGESIZE,                    INTDEF_EDGESIZE);
    SetInt(INT_TOPBARFLAGS,                 INTDEF_TOPBARFLAGS);
    SetInt(INT_BUTTONGROUP_SHADEOUT,        INTDEF_BUTTONGROUP_SHADEOUT);
    SetInt(INT_SCROLL_ALPHAMIN,             INTDEF_SCROLL_ALPHAMIN);
    SetInt(INT_SCROLL_ALPHAMAX,             INTDEF_SCROLL_ALPHAMAX);
    SetInt(INT_TOPBAR_SIZEPCT,              INTDEF_TOPBAR_SIZEPCT);
    SetInt(INT_BTMBAR_SIZEPCT,              INTDEF_BTMBAR_SIZEPCT);
    SetInt(INT_BUTTON_GRADIENT,             INTDEF_BUTTON_GRADIENT);
    SetInt(INT_DISABLE_MAIN_ALPHA,          INTDEF_DISABLE_MAIN_ALPHA);
    SetInt(INT_MAINTAB_ALPHA,               INTDEF_MAINTAB_ALPHA);
    SetInt(INT_MAINTAB_SELECT_ALPHA,        INTDEF_MAINTAB_SELECT_ALPHA);
    SetInt(INT_HORIZON_DY,                  INTDEF_HORIZON_DY);
    SetInt(INT_LIST_EDGEFADE,               INTDEF_LIST_EDGEFADE);
    SetInt(INT_STD_INSET,                   INTDEF_STD_INSET);
    SetInt(INT_PAGE_INSET,                  INTDEF_PAGE_INSET);
    SetInt(INT_TABBUTTON_TEXTHEIGHT,        INTDEF_TABBUTTON_TEXTHEIGHT);
    SetInt(INT_ZONEICON_ALPHAOVERRIDE,      INTDEF_ZONEICON_ALPHAOVERRIDE);
    SetInt(INT_DEFAULT_ICONSIZE,            INTDEF_DEFAULT_ICONSIZE);
    SetInt(INT_LIST_SELRAD,                 INTDEF_LIST_SELRAD);
    SetInt(INT_GLOWMODE_SIZE1,              INTDEF_GLOWMODE_SIZE1);
    SetInt(INT_GLOWMODE_SIZE2,              INTDEF_GLOWMODE_SIZE2);

    SetRGB(RGB_BUTTON,              RGBDEF_BUTTON);
    SetRGB(RGB_BUTTON_TEXT,         RGBDEF_BUTTON_TEXT);                                 
    SetRGB(RGB_APP_BACKGROUND,      RGBDEF_APP_BACKGROUND);
    SetRGB(RGB_APP_TEXT,            RGBDEF_APP_TEXT );
    SetRGB(RGB_EDGE,                RGBDEF_EDGE );
    SetRGB(RGB_LEGEND_HEAT,         RGBDEF_LEGEND_HEAT);
    SetRGB(RGB_LEGEND_COOL,         RGBDEF_LEGEND_COOL);
    SetRGB(RGB_LEGEND_ROOM,         RGBDEF_LEGEND_ROOM);
    SetRGB(RGB_LEGEND_OUTSIDE,      RGBDEF_LEGEND_OUTSIDE);
    SetRGB(RGB_LEGEND_ARMED,        RGBDEF_LEGEND_ARMED);
    SetRGB(RGB_LEGEND_NOTREADY,     RGBDEF_LEGEND_NOTREADY);
    SetRGB(RGB_LEGEND_ALARM,        RGBDEF_LEGEND_ALARM);
    SetRGB(RGB_LEGEND_FIRE,         RGBDEF_LEGEND_FIRE);
    SetRGB(RGB_LEGEND_ZONE1,        RGBDEF_LEGEND_ZONE1);
    SetRGB(RGB_LEGEND_ZONE2,        RGBDEF_LEGEND_ZONE2);
    SetRGB(RGB_SELECT,              RGBDEF_SELECT);
    SetRGB(RGB_SELECT_TEXT,         RGBDEF_SELECT_TEXT);
    SetRGB(RGB_SELECT_SUBTLE,       RGBDEF_SELECT_SUBTLE);
    SetRGB(RGB_MAINTAB_TOP,         RGBDEF_MAINTAB_TOP);
    SetRGB(RGB_MAINTAB_BTM,         RGBDEF_MAINTAB_BTM);
    SetRGB(RGB_MAINTAB_EDGE,        RGBDEF_MAINTAB_EDGE);
    SetRGB(RGB_PAGETAB_TOP,         RGBDEF_PAGETAB_TOP);
    SetRGB(RGB_PAGETAB_BTM,         RGBDEF_PAGETAB_BTM);
    SetRGB(RGB_LIST_AREA,           RGBDEF_LIST_AREA);
    SetRGB(RGB_PAGETAB_SELECT_TOP_LS,  RGBDEF_PAGETAB_SELECT_TOP_LS);
    SetRGB(RGB_PAGETAB_SELECT_BTM_LS,  RGBDEF_PAGETAB_SELECT_BTM_LS);
    SetRGB(RGB_PAGETAB_SELECT_TOP_PT,  RGBDEF_PAGETAB_SELECT_TOP_PT);
    SetRGB(RGB_PAGETAB_SELECT_BTM_PT,  RGBDEF_PAGETAB_SELECT_BTM_PT);
    SetRGB(RGB_VPANEL_1,            RGBDEF_VPANEL_1);
    SetRGB(RGB_VPANEL_2,            RGBDEF_VPANEL_2);
    SetRGB(RGB_MAINTAB_SELECT_TOP,  RGBDEF_MAINTAB_SELECT_TOP);
    SetRGB(RGB_MAINTAB_SELECT_BTM,  RGBDEF_MAINTAB_SELECT_BTM);
    SetRGB(RGB_TOPBAR_TOP,          RGBDEF_TOPBAR_TOP);
    SetRGB(RGB_TOPBAR_BTM,          RGBDEF_TOPBAR_BTM);
    SetRGB(RGB_SCROLL,              RGBDEF_SCROLL);
    SetRGB(RGB_BTMBAR_TOP,          RGBDEF_BTMBAR_TOP);
    SetRGB(RGB_BTMBAR_BTM,          RGBDEF_BTMBAR_BTM);
    SetRGB(RGB_LCD_TOP,             RGBDEF_LCD_TOP);
    SetRGB(RGB_LCD_BTM,             RGBDEF_LCD_BTM);
    SetRGB(RGB_LCD_TEXT,            RGBDEF_LCD_TEXT);
    SetRGB(RGB_UNUSED3,             RGBDEF_UNUSED3);
    SetRGB(RGB_ZONEPAGE_LOWER,      RGBDEF_ZONEPAGE_LOWER);
    SetRGB(RGB_ICONOVERRIDE,        RGBDEF_ICONOVERRIDE);
    SetRGB(RGB_BAR_EDGE,            RGBDEF_BAR_EDGE);
    SetRGB(RGB_PAGETAB_UNSELECT_TOP,    RGBDEF_PAGETAB_UNSELECT_TOP);
    SetRGB(RGB_PAGETAB_UNSELECT_BTM,    RGBDEF_PAGETAB_UNSELECT_BTM);
    SetRGB(RGB_HEAT_STD,            RGBDEF_HEAT_STD);
    SetRGB(RGB_HEAT_MAINTAB,        RGBDEF_HEAT_MAINTAB);
    SetRGB(RGB_COOL_STD,            RGBDEF_COOL_STD);
    SetRGB(RGB_COOL_MAINTAB,        RGBDEF_COOL_MAINTAB);
    SetRGB(RGB_LIST_SELECTITEMT,     RGBDEF_LIST_SELECTITEMT);
    SetRGB(RGB_LEGEND_MORNING,      RGBDEF_LEGEND_MORNING);
    SetRGB(RGB_LEGEND_DAY,          RGBDEF_LEGEND_DAY);
    SetRGB(RGB_LEGEND_EVENING,      RGBDEF_LEGEND_EVENING);
    SetRGB(RGB_LEGEND_NIGHT,        RGBDEF_LEGEND_NIGHT);
    SetRGB(RGB_LEGEND_POOL,         RGBDEF_LEGEND_POOL);
    SetRGB(RGB_LEGEND_SPA,          RGBDEF_LEGEND_SPA);
    SetRGB(RGB_LEGEND_SOLAR,        RGBDEF_LEGEND_SOLAR);
    SetRGB(RGB_LEGEND_LIGHTS,       RGBDEF_LEGEND_LIGHTS);
    SetRGB(RGB_LEGEND_OFF,          RGBDEF_LEGEND_OFF);
    SetRGB(RGB_LEGEND_RUNPROG,      RGBDEF_LEGEND_RUNPROG);
    SetRGB(RGB_LEGEND_MANUAL,       RGBDEF_LEGEND_MANUAL);
    SetRGB(RGB_LEGEND_FAN,          RGBDEF_LEGEND_FAN);
    SetRGB(RGB_ZONEICON_OVERRIDE,   RGBDEF_ZONEICON_OVERRIDE);
    SetRGB(RGB_LIST_SELECTITEMB,    RGBDEF_LIST_SELECTITEMB);
    SetRGB(RGB_LIST_PLAYABLE,       RGBDEF_LIST_PLAYABLE);       
    SetRGB(RGB_LIST_SELECTABLE,     RGBDEF_LIST_SELECTABLE);
    SetRGB(RGB_LIST_AUTO,           RGBDEF_LIST_AUTO);
    SetRGB(RGB_LIST_PLAY1,          RGBDEF_LIST_PLAY1);
    SetRGB(RGB_LIST_PLAY2,          RGBDEF_LIST_PLAY2);
    SetRGB(RGB_LIST_IDLE,           RGBDEF_LIST_IDLE);
    SetRGB(RGB_LIST_PAUSED,         RGBDEF_LIST_PAUSED);
    SetRGB(RGB_LIST_WORKING,        RGBDEF_LIST_WORKING);
    SetRGB(RGB_LIST_NEWMSG,         RGBDEF_LIST_NEWMSG);
    SetRGB(RGB_LIST_OLDMSG,         RGBDEF_LIST_OLDMSG);
    SetRGB(RGB_LEGEND_RAIN,         RGBDEF_LEGEND_RAIN);
	SetRGB(RGB_ERROR,				RGBDEF_ERROR);
	SetRGB(RGB_ERROR_2,				RGBDEF_ERROR_2);
    SetRGB(RGB_PAGETAB_MID,         RGBDEF_PAGETAB_MID);
#endif

    SetTexture(_T(""));
}
#ifdef HLOSD
/*============================================================================*/

    CTexture::CTexture(
    CHLString                   sName )

/*============================================================================*/
{
    m_sName = sName;
    m_pDIB = NULL;
    g_pHLServer->AddSocketSink(this);
    CHLMSG msgQ(SenderID(), HLM_IMAGESERV_GETTEXTUREQ);
    msgQ.PutString(m_sName);
    g_pHLServer->SendMSG(&msgQ);
    m_bWaitMSG = TRUE;
}
/*============================================================================*/

    CTexture::~CTexture()

/*============================================================================*/
{
    if (m_bWaitMSG)
        g_pHLServer->RemoveSocketSink(this);

    delete m_pDIB;
}
/*============================================================================*/

    void                        CTexture::SinkMessage(CHLMSG *pMSG)

/*============================================================================*/
{
    switch (pMSG->MessageID())
    {
    case HLM_IMAGESERV_GETTEXTUREA:
        {
            LOGASSERT(m_pDIB == NULL);
            m_bWaitMSG = FALSE;
            int iDX, iDY, iSize;

            int iOffset = 12;
            int iFileType = ENCODE_GIF;
            pMSG->GetInt(&iFileType);
            iOffset += 4;

            pMSG->GetInt(&iDX);
            pMSG->GetInt(&iDY);
            pMSG->GetInt(&iSize);

#ifdef HLOSD
            if (iFileType == ENCODE_PNG)
            {
                CHLDibDC *pDIB = CPNGDecoder::LoadHLPNG(pMSG->Data()+iOffset, iSize, RENDIMG_24BITDOWNCONVERT);
                if (pDIB != NULL)
                {
                    m_pDIB = new CGLDIB(pDIB, 0);
                    delete pDIB;
                }
            }
            else if (iFileType == ENCODE_JPEG)
            {
                m_pDIB = CGLDIB::CreateJPEG(pMSG->Data()+iOffset, iSize, NULL);
            }
#else
            if (iFileType == ENCODE_GIF)
            {
                m_pDIB = CGIFDecoder::LoadHLGIF(pMSG->Data()+iOffset, iSize, FALSE);
            }
            if (iFileType == ENCODE_PNG)
            {
                m_pDIB = CPNGDecoder::LoadHLPNG(pMSG->Data()+iOffset, iSize, RENDIMG_24BITDOWNCONVERT);
            }
            else if (iFileType == ENCODE_JPEG)
            {
                m_pDIB = new CHLDibDC(iDX, iDY, 0, 0, FALSE);
                if (!CVideoDecoder::DecodeJPG(pMSG->Data()+iOffset, iSize, (BYTE*)m_pDIB->Bits(), m_pDIB->DX(), m_pDIB->DY(), 1))
                {
                    delete [] m_pDIB;
                    m_pDIB = NULL;
                }
        
                m_pDIB->ImageDX(iDX);
            }

            if (m_pDIB != NULL)
            {
                int iDXMain = g_pMainWindow->DX();
                int iDYMain = g_pMainWindow->DY();
                if (m_pDIB->ImageDX() != iDXMain || m_pDIB->DY() != iDYMain)
                {
                    SIZE sz;
                    sz.cx = iDXMain;
                    sz.cy = iDYMain;
                    CHLDibDC *pTemp = m_pDIB;
                    m_pDIB = pTemp->CreateScaledInstance(sz, TRUE, 0);
                    delete pTemp;
                }
            }
#endif
            
            g_pHLServer->RemoveSocketSink(this);
        }
        break;

    case HLM_BAD_PARAMETERS:
    case HLM_UNKNOWN:
        {
            // We've failed to load remove us from the server, we give up
            m_bWaitMSG = FALSE;
            g_pHLServer->RemoveSocketSink(this);
        }
        break;
    }
}
/*============================================================================*/

    void                        CTexture::ConnectionChanged(BOOL bNowConnected)

/*============================================================================*/
{
    if (bNowConnected && m_bWaitMSG)
    {
        CHLMSG msgQ(SenderID(), HLM_IMAGESERV_GETTEXTUREQ);
        msgQ.PutString(m_sName);
        g_pHLServer->SendMSG(&msgQ);
    }
}
/*============================================================================*/

    BOOL                        CTexture::WaitLoad()

/*============================================================================*/
{
    CHLTimer hltWait;
    while (m_bWaitMSG)
    {
        if (!g_pHLServer->IsConnected())
            return FALSE;
        if (hltWait.HLTickCount() > 5000)
            return FALSE;

        #ifdef HLCONFIG
            int iReceive = g_pHLServer->PumpSocketMessages();
        #else
            int iReceive = g_pHLServer->PumpSocketMessages(NULL, SenderID());
        #endif

        if (iReceive == RECEIVE_ERROR)
        {
            g_pHLServer->RemoveSocketSink(this);
            return FALSE;
        }

        if (iReceive == RECEIVE_IDLE)
        {
            #ifdef WIN32
                #ifndef UNDER_CE
                    ::Sleep(25);
                #else
                    ::Sleep(0);
                #endif
            #else
                #ifdef HLUTIL
                    ::Sleep(10);
                #else
                    ::Sleep(0);
                #endif
            #endif
        }
        
    }

    return (m_pDIB != NULL);
}
#endif