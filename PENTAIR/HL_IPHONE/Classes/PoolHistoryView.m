//
//  PoolHistoryPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/15/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "PoolHistoryMainPage.h"
#import "timedataview.h"
#import "PoolHistoryDataView.h"
#import "timebar.h"
#import "LegendItem.h"
#import "PoolHistoryView.h"
#import "NSPoolConfig.h"
#import "CustomPage.h"

@implementation CPoolHistoryView
/*============================================================================*/

- (id)initWithFrame:(CGRect)rFrame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    m_pConfig = pConfig;

    if (self = [super initWithFrame:rFrame]) 
    {
        // Initialization code
        m_pConfig = pConfig;
        m_bLockYScroll = TRUE;

        CGRect rData = CGRectMake(-rFrame.size.width, 0, 3 * rFrame.size.width, [self DataViewDY]);
        m_pTimeDataView = [self CreateTimeDataView:rData LastView:NULL];
        [self insertSubview:m_pTimeDataView belowSubview:m_pTimeBar];

        [self InitScaleView];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(UIView*)CreateLegendView

/*============================================================================*/
{
    CGRect rLegend = CGRectMake(4, 5, self.frame.size.width-8, 22);
    //DL
    rLegend.origin.y += 20;
    
    UIView *pView = [[UIView alloc] initWithFrame:rLegend];
    [pView setOpaque:YES];
    for (int i = 0; i < 3; i++)
    {
        int iDXItem = rLegend.size.width / 4;
        CGRect rItem = CGRectMake(iDXItem * (i+1), 0, iDXItem, rLegend.size.height);
        NSString *pText = NULL;
        UIColor *pColor = NULL;
        UIColor *pColorFill = NULL;
        switch (i)
        {
        case 0: 
            {   
                NSString *pBodyType = [m_pConfig PoolText];
                pText = [NSString stringWithFormat:@"%s Temp", [pBodyType UTF8String]];      
                pColorFill = [CPoolHistoryDataView FillColor:RGB_POOL_TEMP];     
                pColor = [CPoolHistoryDataView ItemColor:RGB_POOL_TEMP];   
            } 
            break;

        case 1: 
            { 
                NSString *pBodyType = [m_pConfig SpaText];
                pText = [NSString stringWithFormat:@"%s Temp", [pBodyType UTF8String]];      
                pColorFill = [CPoolHistoryDataView FillColor:RGB_POOL_TEMP];     
                pColor = [CPoolHistoryDataView ItemColor:RGB_SPA_TEMP];   
            } 
            break;

        case 2: 
            { 
                pText = @"Outside Temp";  
                pColorFill = [CPoolHistoryDataView FillColor:RGB_OUTSIDE_TEMP];     
                pColor = [CPoolHistoryDataView ItemColor:RGB_OUTSIDE_TEMP];   
            } 
            break;
        }

        CLegendItem *pItem = [[CLegendItem alloc] initWithFrame:rItem Color:pColor FillColor:pColorFill Text:pText Round:NO];
        [pView addSubview:pItem];
        [pItem release];
    }

    return pView;
}
/*============================================================================*/

-(CTimeDataView*)CreateTimeDataView:(CGRect)rFrame LastView:(CTimeDataView*)pLastView

/*============================================================================*/
{
    CPoolHistoryDataView *pLastPoolView = (CPoolHistoryDataView*)pLastView;
    CPoolHistoryDataView *pView = [[CPoolHistoryDataView alloc] initWithFrame:rFrame TimeView:self Config:m_pConfig LastView:pLastPoolView];
    return pView;
}
/*============================================================================*/

-(int)DataViewDY

/*============================================================================*/
{
    return self.frame.size.height;
}
/*============================================================================*/

-(void)NotifyRotation

/*============================================================================*/
{
    [m_pTimeBar removeFromSuperview];
    [m_pTimeBar release];

    CGRect rFrame = self.frame;
    CGRect rTimeBar = CGRectMake(-rFrame.size.width, rFrame.size.height-[CMainView DY_TIMEBAR], 3*rFrame.size.width, [CMainView DY_TIMEBAR]);
    m_pTimeBar = [[CTimeBar alloc] initWithFrame:rTimeBar TimeView:self];
    [self addSubview:m_pTimeBar];

    CGRect rData = m_pTimeDataView.frame;
    rData.origin.x = -self.frame.size.width;
    rData.size.width = 3 * self.frame.size.width;
    rData.size.height = self.frame.size.height;
    int iDYDataOld = m_pTimeDataView.frame.size.height;
    int iDYDataNew = rData.size.height;
    [m_pTimeDataView setFrame:rData];

    CPoolHistoryDataView *pHistData = (CPoolHistoryDataView*)m_pTimeDataView;
    [pHistData SetXOrigin];
    [pHistData UpdateRTEViews];
    [pHistData UpdateTimeBar];
    
    CGRect rLegend = m_pLegendView.frame;
    rLegend.origin.x = self.frame.size.width - rLegend.size.width - 4;
    rLegend.origin.y = 2;
    [m_pLegendView setFrame:rLegend];
    [pHistData SetFrameResizeOld:iDYDataOld New:iDYDataNew];
    [m_pTimeDataView setNeedsDisplay];
}
/*============================================================================*/

-(void)InitScaleView

/*============================================================================*/
{
    [m_pScaleView removeFromSuperview];
    [m_pScaleView release];

    CGRect rScale = CGRectMake(0, 0, 60, [self DataViewDY]);
    //DL
    rScale.origin.y += 20;
    rScale.size.height -= 20;

    //DL - this the view with temps only (vertical strip)
    m_pScaleView = [[UIView alloc] initWithFrame:rScale];
    m_pScaleView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.75];

    if (m_pLegendView != NULL)
        [self insertSubview:m_pScaleView belowSubview:m_pLegendView];
    else
        [self insertSubview:m_pScaleView belowSubview:m_pTimeBar];


    CPoolHistoryDataView *pPool = (CPoolHistoryDataView*)m_pTimeDataView;
    int iStep = 2;
    int iDTemp = [pPool MaxTemp] - [pPool MinTemp];
    if (iDTemp > 50)
        iStep = 10;
    else if (iDTemp > 30)
        iStep = 5;
    else if (iDTemp <= 10)
        iStep = 1;
        

    for (int i = [pPool MinTemp]; i <= [pPool MaxTemp]; i+=iStep)
    {
        int iY = [pPool YLogical:i];
        CGRect rTemp = CGRectMake(0, iY-10, 60, 20);
        UILabel *pLabel = [[UILabel alloc] initWithFrame:rTemp];
        [CCustomPage InitStandardLabel:pLabel Size:18];
        NSString *pText = [NSString stringWithFormat:@"%d°", i];
        [pLabel setText:pText];

        [m_pScaleView addSubview:pLabel];
        [pLabel release];
    }
    
    
    
}

@end
