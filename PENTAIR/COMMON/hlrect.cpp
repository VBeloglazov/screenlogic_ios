/*
--------------------------------------------------------------------------------

    HLRECT.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "hlrect.h"

#undef RECT_INTERFACE
#ifndef IPHONE
#if defined (CRYSTALPAD) | defined(HLCONFIG) | defined(HLOSD)
    #define RECT_INTERFACE
    #include "hlwindow.h"
#endif

#ifdef _DEBUG
    #include "..\crystalpad\mainwnd.h"
#endif
#endif


/*============================================================================*/

   CHLRect::~CHLRect()

/*============================================================================*/
{
}
/*============================================================================*/

    BOOL                        CHLRect::IsEqualTo(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (XLeft() != pOther->XLeft())
        return FALSE;
    if (YTop() != pOther->YTop())
        return FALSE;
    if (DX() != pOther->DX())
        return FALSE;
    if (DY() != pOther->DY())
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    const CHLRect               &CHLRect::Inset(
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    m_iXLeft += iDX;
    m_iYTop += iDY;
    m_iDX -= 2*iDX;
    m_iDY -= 2*iDY;

    return *this;
}
/*============================================================================*/

    const CHLRect               &CHLRect::Offset(
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    m_iXLeft -= iDX;
    m_iYTop -= iDY;
    m_iDX += 2*iDX;
    m_iDY += 2*iDY;

    ASSERT(m_iXLeft >= 0);
    ASSERT(m_iYTop >= 0);

    return *this;
}
/*============================================================================*/

    const CHLRect               &CHLRect::Translate(
    int                         iDX,
    int                         iDY)

/*============================================================================*/
{
    m_iXLeft    += iDX;
    m_iYTop     += iDY;

    return *this;
}
#ifdef RECT_INTERFACE
/*============================================================================*/

    const CHLRect               &CHLRect::ClientToScreen(
    CHLWindow                   *pWnd )

/*============================================================================*/
{
    m_iXLeft = pWnd->XScreen(m_iXLeft);
    m_iYTop = pWnd->YScreen(m_iYTop);

    return *this;
}
/*============================================================================*/

    const CHLRect               &CHLRect::ScreenToClient(
    CHLWindow                   *pWnd )

/*============================================================================*/
{
    LOGASSERT(FALSE);

    return *this;
}
#endif
/*============================================================================*/

    void                        CHLRect::CenterDownTo(
    int                         iDXMax,
    int                         iDYMax )

/*============================================================================*/
{
    if (m_iDX > iDXMax)
    {
        m_iXLeft = m_iXLeft + m_iDX/2-iDXMax/2;
        m_iDX = iDXMax;
    }
    if (m_iDY > iDYMax)
    {
        m_iYTop = m_iYTop + m_iDY/2-iDYMax/2;
        m_iDY = iDYMax;
    }
}
/*============================================================================*/

    void                        CHLRect::CenterUpTo(
    int                         iDXMin,
    int                         iDYMin )

/*============================================================================*/
{
    if (m_iDX < iDXMin)
    {
        m_iXLeft = m_iXLeft + m_iDX/2-iDXMin/2;
        m_iDX = iDXMin;
    }
    if (m_iDY < iDYMin)
    {
        m_iYTop = m_iYTop + m_iDY/2-iDYMin/2;
        m_iDY = iDYMin;
    }
}
/*============================================================================*/

    RECT                        CHLRect::GetRECT()

/*============================================================================*/
{
    RECT r;
    r.left  = XLeft();
    r.top   = YTop();
    r.right = r.left + DX();
    r.bottom = r.top + DY();
    return r;
}
/*============================================================================*/

    CHLRect                     CHLRect::CreateSplitLeft(
    int                         iDX )

/*============================================================================*/
{
    iDX = __min(m_iDX, iDX);
    iDX = __max(0, iDX);
    CHLRect rect(m_iXLeft, m_iYTop, iDX, m_iDY);
    return rect;
}
/*============================================================================*/

    CHLRect                     CHLRect::CreateSplitRight(
    int                         iDX )

/*============================================================================*/
{
    iDX = __min(m_iDX, iDX);
    iDX = __max(0, iDX);
    CHLRect rect(m_iXLeft + m_iDX - iDX, m_iYTop, iDX, m_iDY);
    return rect;
}
/*============================================================================*/

    CHLRect                     CHLRect::CreateSplitTop(
    int                         iDY )

/*============================================================================*/
{
    iDY = __max(0, iDY);
    iDY = __min(iDY, m_iDY);
    CHLRect rect(m_iXLeft, m_iYTop, m_iDX, iDY);
    return rect;
}
/*============================================================================*/

    CHLRect                     CHLRect::CreateSplitBottom(
    int                         iDY )

/*============================================================================*/
{
    iDY = __max(0, iDY);
    iDY = __min(iDY, m_iDY);
    CHLRect rect(m_iXLeft, m_iYTop + m_iDY - iDY, m_iDX, iDY);
    return rect;
}
/*========================================================================*/

    CHLRect                     CHLRect::CreateXSectionWeighted(
    int                         iIndex,
	int							iIndexWeight,
    int                         iNSections,
	int							iPrevRectRight,
    int                         iBorder )

/*========================================================================*/
{
	if (iIndexWeight > 0 &&
		iIndexWeight < (100 - iNSections - 1))
	{
		int iDX2 = DX() + iBorder;
		float f = (float)iIndexWeight / 100;
		int iDXSection = (int)(iDX2 * f);
		int iXLeft = iPrevRectRight;
		int iDX = iDXSection - iBorder;

		CHLRect rect(m_iXLeft + iXLeft, m_iYTop, iDX, m_iDY);
		return rect;
	}
	else
	{
		CHLRect rect(m_iXLeft, m_iYTop, m_iDX, m_iDY);
		return rect;
	}
}
/*========================================================================*/

    CHLRect                     CHLRect::ConfineToAspect(
    int                         iDX,
    int                         iDY )

/*========================================================================*/
{
    int iDXDYThis = m_iDX * 100 / m_iDY;
    int iDXDYIn   = iDX   * 100 / iDY;

    CHLRect rRet = CDXYRect(m_iXLeft, m_iYTop, m_iDX, m_iDY);

    if (iDXDYIn < iDXDYThis)
    {
        // Incoming aspect is taller than "this"
        int iDXOut = m_iDY * iDX / iDY;
        rRet.CenterDownTo(iDXOut, m_iDY);
    }
    else
    {
        // Incoming aspect is wider than "this"
        int iDYOut = m_iDX * iDY / iDX;
        rRet.CenterDownTo(m_iDX, iDYOut);
    }
    return rRet;
}
/*========================================================================*/

    CHLRect                     CHLRect::CreateXSection(
    int                         iIndex,
    int                         iNSections,
    int                         iBorder )

/*========================================================================*/
{
    int iDX2 = DX() + iBorder;
    int iDXSection = iDX2 / iNSections;
    int iXLeft = iDXSection * iIndex;
    int iDX = iDXSection - iBorder;

    CHLRect rect(m_iXLeft + iXLeft, m_iYTop, iDX, m_iDY);
    return rect;
}
/*========================================================================*/

    CHLRect                     CHLRect::BreakOffLeft(
    int                         iDX,
    int                         iBuffer )

/*========================================================================*/
{
    CHLRect rect(XLeft(), YTop(), iDX-iBuffer,  DY());
    DX(DX()-iDX);
    Translate(iDX, 0);
    return rect;
}
/*========================================================================*/

    CHLRect                     CHLRect::BreakOffRight(
    int                         iDX,
    int                         iBuffer )

/*========================================================================*/
{
    CHLRect rect(XRight()-iDX+iBuffer+1, YTop(), iDX-iBuffer,  DY());
    DX(DX()-iDX);
    return rect;
}
/*========================================================================*/

    CHLRect                     CHLRect::BreakOffTop(
    int                         iDY,
    int                         iBuffer )

/*========================================================================*/
{
    CHLRect rect(XLeft(), YTop(), DX(),  iDY-iBuffer);
    Translate(0, iDY);
    DY(DY()-iDY);
    return rect;
}
/*========================================================================*/

    CHLRect                     CHLRect::BreakOffBottom(
    int                         iDY,
    int                         iBuffer )

/*========================================================================*/
{
    CHLRect rect(XLeft(), YBottom()-iDY+iBuffer+1, DX(),  iDY-iBuffer);
    DY(DY()-iDY);
    return rect;
}
/*========================================================================*/

    CHLRect                     *CHLRect::CreateBreakOffLeft(
    int                         iDX,
    int                         iBuffer )

/*========================================================================*/
{
    if (iDX <= 0)
        return NULL;

    CHLRect *pRect = new CHLRect(XLeft(), YTop(), iDX-iBuffer,  DY());
    DX(DX()-iDX);
    Translate(iDX, 0);
    return pRect;
}
/*========================================================================*/

    CHLRect                     *CHLRect::CreateBreakOffRight(
    int                         iDX,
    int                         iBuffer )

/*========================================================================*/
{
    if (iDX <= 0)
        return NULL;

    CHLRect *pRect = new CHLRect(XRight()-iDX+iBuffer+1, YTop(), iDX-iBuffer,  DY());
    DX(DX()-iDX);
    return pRect;
}
/*========================================================================*/

    CHLRect                     *CHLRect::CreateBreakOffTop(
    int                         iDY,
    int                         iBuffer )

/*========================================================================*/
{
    if (iDY <= 0)
        return NULL;

    CHLRect *pRect = new CHLRect(XLeft(), YTop(), DX(),  iDY-iBuffer);
    Translate(0, iDY);
    DY(DY()-iDY);
    return pRect;
}
/*========================================================================*/

    CHLRect                     *CHLRect::CreateBreakOffBottom(
    int                         iDY,
    int                         iBuffer )

/*========================================================================*/
{
    if (iDY <= 0)
        return NULL;

    CHLRect *pRect = new CHLRect(XLeft(), YBottom()-iDY+iBuffer+1, DX(),  iDY-iBuffer);
    DY(DY()-iDY);
    return pRect;
}
/*========================================================================*/

    CHLRect                     CHLRect::CreateYSection(
    int                         iIndex,
    int                         iNSections,
    int                         iBorder )

/*========================================================================*/
{
    int iDY2 = DY() + iBorder;
    int iDYSection = iDY2 / iNSections;
    int iYTop  = iDYSection * iIndex;
    int iDY = iDYSection - iBorder;

    CHLRect rect(m_iXLeft, m_iYTop + iYTop, m_iDX, iDY);
    return rect;
}
/*============================================================================*/

    CHLRect                     CHLRect::BlendWith(
    CHLRect                     r,  
    int                         i1000 )

/*============================================================================*/
{
    int _i1000 = 1000-i1000;
    int iX = (m_iXLeft * _i1000 + r.m_iXLeft * i1000) / 1000;
    int iY = (m_iYTop  * _i1000 + r.m_iYTop  * i1000) / 1000;
    int iDX = (m_iDX * _i1000 + r.m_iDX * i1000) / 1000;
    int iDY = (m_iDY * _i1000 + r.m_iDY * i1000) / 1000;
    return CDXYRect(iX, iY, iDX, iDY);
}
/*============================================================================*/

    CHLRect                     CHLRect::CreateInset(
    int                         iDXEdge,
    int                         iDYEdge )

/*============================================================================*/
{
    int iDX = m_iDX;
    int iDY = m_iDY;
    int iXL = m_iXLeft;
    int iYT = m_iYTop;
    iDX -= (2*iDXEdge);
    iDY -= (2*iDYEdge);
    iXL += iDXEdge;
    iYT += iDYEdge;

    ASSERT((iDX >= 0) && (iDY >= 0));
    return CHLRect(iXL, iYT, iDX, iDY);
}
/*============================================================================*/

    BOOL                        CHLRect::IsPointInside(
    POINT                       p )

/*============================================================================*/
{
    if (p.x < XLeft())
        return FALSE;
    if (p.x > XRight())
        return FALSE;
    if (p.y < YTop())
        return FALSE;
    if (p.y > YBottom())
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    void                        CHLRect::Flip(
    BOOL                        bHz,
    BOOL                        bVt,
    int                         iDXBounds,
    int                         iDYBounds )

/*============================================================================*/
{
    if (bHz)
    {
        int iDR = iDXBounds - (XRight()+1);
        m_iXLeft = iDR;
    }

    if (bVt)
    {
        int iDB = iDYBounds - (YBottom()+1);
        m_iYTop = iDB;
    }
}
/*============================================================================*/

    CHLRect                     CHLRect::IntersectWith(
    CHLRect                     rSubRect )

/*============================================================================*/
{
    int iX0 = rSubRect.XLeft();
    int iY0 = rSubRect.YTop();
    int iDX = rSubRect.DX();
    int iDY = rSubRect.DY();
    
    if (iX0 > XRight())
    {
        iX0 = XRight();
        iDX = 0;
    }

    if (iY0 > YBottom())
    {
        iY0 = YBottom();
        iDY = 0;
    }

    if (iX0 < XLeft())
    {
        iDX += (iX0 - XLeft());
        iX0 = XLeft();
    }

    if (iY0 < YTop())
    {
        iDY += (iY0 - YTop());
        iY0 = YTop();
    }

    iDX = __min(iDX, XRight() - iX0 + 1);
    iDY = __min(iDY, YBottom() - iY0 + 1);

    return CDXYRect(iX0, iY0, iDX, iDY);    
}
/*============================================================================*/

    BOOL                        CHLRect::IntersectsWith(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (YBottom() < pOther->YTop())
        return FALSE;
    if (YTop() > pOther->YBottom())
        return FALSE;
    if (XRight() < pOther->XLeft())
        return FALSE;
    if (XLeft() > pOther->XRight())
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLRect::IsInside(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (m_iXLeft < pOther->XLeft() || m_iXLeft > pOther->XRight())
        return FALSE;
    if (XRight() > pOther->XRight() || XRight() < pOther->XLeft())
        return FALSE;
        
    if (m_iYTop < pOther->YTop() || m_iYTop > pOther->YBottom())
        return FALSE;
    if (YBottom() > pOther->YBottom() || YBottom() < pOther->YTop())
        return FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLRect::ContainsX(
    int                         iX )

/*============================================================================*/
{
    return (iX >= m_iXLeft && iX < (m_iXLeft + m_iDX));
}
/*============================================================================*/

    BOOL                        CHLRect::ContainsY(
    int                         iY )

/*============================================================================*/
{
    return (iY >= m_iYTop && iY < (m_iYTop + m_iDY));
}
/*============================================================================*/

    BOOL                        CHLRect::CombineWith(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (SameTopBottom(pOther))
    {
        if (XRight() >= (pOther->XLeft()-1) && XLeft() < pOther->XRight())
        {
            // Our right edge is close (or inside other)
            XRight(__max(XRight(), pOther->XRight()));
            XLeft(__min(XLeft(), pOther->XLeft()));
            return TRUE;
        }

        if (XLeft() <= (pOther->XRight()+1) && XRight() > pOther->XLeft())
        {
            // Our left edge is close (or inside other)
            XRight(__max(XRight(), pOther->XRight()));
            XLeft(__min(XLeft(), pOther->XLeft()));
            return TRUE;
        }
    }
    if (SameLeftRight(pOther))
    {
        if (YBottom() >= (pOther->YTop()-1) && YBottom() < pOther->YTop())
        {
            // Our right edge is close (or inside other)
            YBottom(__max(YBottom(), pOther->YBottom()));
            YTop(__min(YTop(), pOther->YTop()));
            return TRUE;
        }

        if (YTop() <= (pOther->YBottom()+1) && YBottom() > pOther->YTop())
        {
            // Our left edge is close (or inside other)
            YBottom(__max(YBottom(), pOther->YBottom()));
            YTop(__min(YTop(), pOther->YTop()));
            return TRUE;
        }
    }

    return FALSE;
}
/*============================================================================*/

    BOOL                        CHLRect::IsWithinXBoundsOf(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (m_iXLeft >= pOther->XLeft() && XRight() <= pOther->XRight())
        return TRUE;
    return FALSE;
}
/*============================================================================*/

    BOOL                        CHLRect::IsWithinYBoundsOf(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (m_iYTop >= pOther->YTop() && YBottom() <= pOther->YBottom())
        return TRUE;
    return FALSE;
}
/*============================================================================*/

    BOOL                        CHLRect::SameTopBottom(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (pOther->YTop() != m_iYTop)
        return FALSE;
    if (YBottom() != pOther->YBottom())
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLRect::SameLeftRight(
    CHLRect                     *pOther )

/*============================================================================*/
{
    if (pOther->XLeft() != m_iXLeft)
        return FALSE;
    if (XRight() != pOther->XRight())
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    CRectList::CRectList()

/*============================================================================*/
{
    m_iNRects = 0;
}
/*============================================================================*/

    CRectList::~CRectList()

/*============================================================================*/
{
}
/*============================================================================*/

    CRectList                   *CRectList::CreateCopy()

/*============================================================================*/
{
    CRectList *pCopy = new CRectList();
    for (int i = 0; i < m_iNRects; i++)
    {
        pCopy->AddRectDirectDXDY(X(i), Y(i), DX(i), DY(i));
    }

    return pCopy;
}
/*============================================================================*/

    void                        CRectList::CopyFrom(
    CRectList                   *pOther )

/*============================================================================*/
{
    Clear();
    pOther->AppendTo(this);
}
/*============================================================================*/

//    void                        CRectList::AddRect(
//    CHLRect                     rect )

/*============================================================================*/
//{
//    AddRectRef(&rect);
//}
/*============================================================================*/

    void                        CRectList::AddRectRef(
    CHLRect                     *pRect )

/*============================================================================*/
{
    if (pRect == NULL)
        return;

    if (pRect->DX() <= 0 || pRect->DY() <= 0)
    {
        return;
    }

    for (int iIndex = 0; iIndex < m_iNRects; iIndex++)
    {   
        if (RectAtIndexContains(iIndex, pRect))
        {
            return;
        }

        if (RectAtIndexIsInside(iIndex, pRect))
        {
            XLeft(iIndex, pRect->XLeft());
            YTop(iIndex, pRect->YTop());
            DX(iIndex, pRect->DX());
            DY(iIndex, pRect->DY());

            return;
        }

        if (CombineAtIndex(iIndex, pRect))
        {
            return;
        }

        if (RectAtIndexContainsX(iIndex, pRect->XLeft()) && RectAtIndexContainsY(iIndex, pRect->YTop()) && RectAtIndexContainsY(iIndex, pRect->YBottom()))
            pRect->XLeft(XRight(iIndex)+1);
        else if (pRect->ContainsX(X(iIndex)) && pRect->ContainsY(Y(iIndex)) && pRect->ContainsY(YBottom(iIndex)))
            XLeft(iIndex, pRect->XRight()+1);

        if (RectAtIndexContainsX(iIndex, pRect->XRight()) && RectAtIndexContainsY(iIndex, pRect->YTop()) && RectAtIndexContainsY(iIndex, pRect->YBottom()))
            pRect->XRight(X(iIndex));
        else if (pRect->ContainsX(XRight(iIndex)) && pRect->ContainsY(Y(iIndex)) && pRect->ContainsY(YBottom(iIndex)))
            XLeft(iIndex, pRect->XLeft());

        if (RectAtIndexContainsY(iIndex, pRect->YTop()) && RectAtIndexContainsX(iIndex, pRect->XLeft()) && RectAtIndexContainsX(iIndex, pRect->XRight()))
            pRect->YTop(YBottom(iIndex)+1);
        else if (pRect->ContainsY(Y(iIndex)) && pRect->ContainsX(X(iIndex)) && pRect->ContainsX(XRight(iIndex)))
            YTop(iIndex, pRect->YBottom()+1);

        if (RectAtIndexContainsY(iIndex, pRect->YBottom()) && RectAtIndexContainsX(iIndex, pRect->XLeft()) && RectAtIndexContainsX(iIndex, pRect->XRight()))
            pRect->YBottom(Y(iIndex));
        else if (pRect->ContainsY(YBottom(iIndex)) && pRect->ContainsX(X(iIndex)) && pRect->ContainsX(XRight(iIndex)))
            YBottom(iIndex, pRect->YTop());
    }

    AddRectDirectRef(pRect);
}
/*============================================================================*/

    void                        CRectList::AddRectDirectRef(
    CHLRect                     *pRect )

/*============================================================================*/
{
    if (pRect == NULL)
        return;
    AddRectDirectDXDY(pRect->XLeft(), pRect->YTop(), pRect->DX(), pRect->DY());
}
/*============================================================================*/

    void                        CRectList::AddRectDirectDXDY(
    int                         iX0,
    int                         iY0,
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    if (m_iNRects >= MAX_RECT_LIST)
        return;
    if (iDX <= 0 || iDY <= 0)
    {
        return;
    }

    m_iX0[m_iNRects] = iX0;
    m_iY0[m_iNRects] = iY0;
    m_iDX[m_iNRects] = iDX;
    m_iDY[m_iNRects] = iDY;
    m_iNRects++;
}
/*============================================================================*/

    void                        CRectList::AppendTo(
    CRectList                   *pOther )

/*============================================================================*/
{
    for (int iIndex = 0; iIndex < m_iNRects; iIndex++)
    {
        pOther->AddRectDirectDXDY(X(iIndex), Y(iIndex), DX(iIndex), DY(iIndex));
    }
}
/*============================================================================*/

    void                        CRectList::SubtractFrom(
    CRectList                   *pOther )

/*============================================================================*/
{
    for (int iIndex = 0; iIndex < m_iNRects; iIndex++)
    {
        CHLRect rect = RectAtIndex(iIndex);
        pOther->Subtract(&rect);
    }
}
/*============================================================================*/

    void                        CRectList::Subtract(
    CHLRect                     *pRect )

/*============================================================================*/
{
    // First pass, delete all rects that are totally within the new rect
    int iIndex = 0;
    for (iIndex = 0; iIndex < m_iNRects; iIndex++)
    {
        if (RectAtIndexContains(iIndex, pRect))
        {
            int iDXL = pRect->XLeft()-X(iIndex);
            int iDYT = pRect->YTop()-Y(iIndex);
            int iDXR = XRight(iIndex)-pRect->XRight();
            int iDYB = YBottom(iIndex)-pRect->YBottom();
            SplitBreakOffTop(iIndex, iDYT);
            SplitBreakOffBottom(iIndex, iDYB);
            SplitBreakOffRight(iIndex, iDXR);
            if (iDXL <= 0)
                this->DeleteRect(iIndex);
            else
                DX(iIndex, iDXL);

            return;
        }

        if (RectAtIndexIsInside(iIndex, pRect))
        {
            DeleteRect(iIndex);
            iIndex--;
        }
    }

    // Second pass, look for partial overlaps
    int iNRect = m_iNRects;
    for (iIndex = 0; iIndex < iNRect; iIndex++)
    {
        if (RectAtIndexIntersectsWith(iIndex, pRect))
        {
            if (RectAtIndexIsWithinXBoundsOf(iIndex, pRect))
            {
                // pRect encompasses pTest in X
                int iDYT = pRect->YTop()-Y(iIndex);
                int iDYB = YBottom(iIndex)-pRect->YBottom();
                LOGASSERT(iDYT > 0 || iDYB > 0);
                if (iDYT > 0 && iDYB > 0)
                {
                    AddRectDirectDXDY(X(iIndex), YBottom(iIndex)-iDYB+1, DX(iIndex), iDYB);
                    DY(iIndex, iDYT);
                }
                else if (iDYT > 0)
                {
                    DY(iIndex, iDYT);
                }
                else
                {
                    YTop(iIndex, YBottom(iIndex)-iDYB+1);
                    DY(iIndex, iDYB);
                }
            }
            else if (RectAtIndexIsWithinYBoundsOf(iIndex, pRect))
            {
                // pRect encompasses pTest in Y
                int iDXL = pRect->XLeft()-X(iIndex);
                int iDXR = XRight(iIndex)-pRect->XRight();
                LOGASSERT(iDXL > 0 || iDXR > 0);
                if (iDXL > 0 && iDXR > 0)
                {
                    AddRectDirectDXDY(XRight(iIndex)-iDXR+1, Y(iIndex), iDXR, DY(iIndex));
                    DX(iIndex, iDXL);
                }
                else if (iDXL > 0)
                {
                    DX(iIndex, iDXL);
                }
                else
                {
                    XLeft(iIndex, XRight(iIndex)-iDXR+1);
                    DX(iIndex, iDXR);
                }
            }
            else if (pRect->ContainsX(XRight(iIndex)) || pRect->ContainsX(X(iIndex)))
            {

                CHLRect rect = RectAtIndex(iIndex);
                LOGASSERT(rect.IntersectsWith(pRect));

                int iDYT = pRect->YTop()-Y(iIndex);
                int iDYB = YBottom(iIndex)-pRect->YBottom();

                SplitBreakOffTop(iIndex, iDYT);
                SplitBreakOffBottom(iIndex, iDYB);

                if (pRect->ContainsX(XRight(iIndex)))
                    XRight(iIndex, pRect->XLeft()-1);
                else
                    XLeft(iIndex, pRect->XRight()+1);
            }
            else if (pRect->ContainsY(Y(iIndex)) || pRect->ContainsY(YBottom(iIndex)))
            {
                CHLRect rect = RectAtIndex(iIndex);
                LOGASSERT(rect.IntersectsWith(pRect));

                int iDXL = pRect->XLeft()-X(iIndex);
                int iDXR = XRight(iIndex)-pRect->XRight();
                
                SplitBreakOffLeft(iIndex, iDXL);
                SplitBreakOffRight(iIndex, iDXR);

                if (pRect->ContainsY(YBottom(iIndex)))
                    YBottom(iIndex, pRect->YTop()-1);
                else
                    YTop(iIndex, pRect->YBottom()+1);
            }

            // Did we kill this one?
            if (m_iDX[iIndex] <= 0 || m_iDY[iIndex] <= 0)
            {
                DeleteRect(iIndex);
                iIndex--;
            }
        }
    }
}
/*============================================================================*/

    void                        CRectList::Clear()

/*============================================================================*/
{
    m_iNRects = 0;
}
/*============================================================================*/

    void                        CRectList::Translate(
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    for (int i = 0; i < m_iNRects; i++)
    {
        m_iX0[i] += iDX;
        m_iY0[i] += iDY;
    }
}
/*============================================================================*/

    CHLRect                     CRectList::RectAtIndex(int iIndex)

/*============================================================================*/
{
    if (iIndex < 0 || iIndex >= m_iNRects)
    {
        LOGASSERT(FALSE);
        return CDXYRect(0,0,0,0);
    }

    return CDXYRect(X(iIndex), Y(iIndex), DX(iIndex), DY(iIndex));
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexContains(
    int                         iIndex,
    CHLRect                     *pTest )

/*============================================================================*/
{
    int iXMin = X(iIndex);
    int iYMin = Y(iIndex);
    int iXMax = XRight(iIndex);
    int iYMax = YBottom(iIndex);

    if (pTest->XLeft() < iXMin || pTest->XLeft() > iXMax)
        return FALSE;
    if (pTest->XRight() < iXMin || pTest->XRight() > iXMax)
        return FALSE;
    if (pTest->YTop() < iYMin || pTest->YTop() > iYMax)
        return FALSE;
    if (pTest->YBottom() < iYMin || pTest->YBottom() > iYMax)
        return FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexIsInside(
    int                         iIndex,
    CHLRect                     *pTest )

/*============================================================================*/
{
    int iXMin = X(iIndex);
    int iYMin = Y(iIndex);
    int iXMax = XRight(iIndex);
    int iYMax = YBottom(iIndex);

    if (iXMin < pTest->XLeft()|| iXMin > pTest->XRight())
        return FALSE;
    if (iXMax < pTest->XLeft() || iXMax > pTest->XRight())
        return FALSE;
    if (iYMin < pTest->YTop() || iYMin > pTest->YBottom())
        return FALSE;
    if (iYMax < pTest->YTop() || iYMax > pTest->YBottom())
        return FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexIntersectsWith(
    int                         iIndex,
    CHLRect                     *pTest )

/*============================================================================*/
{
    if (YBottom(iIndex) < pTest->YTop())
        return FALSE;
    if (Y(iIndex) > pTest->YBottom())
        return FALSE;
    if (XRight(iIndex) < pTest->XLeft())
        return FALSE;
    if (X(iIndex) > pTest->XRight())
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexIsWithinXBoundsOf(
    int                         iIndex,
    CHLRect                     *pTest )

/*============================================================================*/
{
    if (X(iIndex) >= pTest->XLeft() && XRight(iIndex) <= pTest->XRight())
        return TRUE;
    return FALSE;
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexIsWithinYBoundsOf(
    int                         iIndex,
    CHLRect                     *pTest )

/*============================================================================*/
{
    if (Y(iIndex) >= pTest->YTop() && YBottom(iIndex) <= pTest->YBottom())
        return TRUE;
    return FALSE;
}
/*============================================================================*/

    void                        CRectList::Consolidate()

/*============================================================================*/
{
    int iIndex = 0;
    while (iIndex < m_iNRects)
    {
        BOOL bCombined = FALSE;
        CHLRect rThis = RectAtIndex(iIndex);
        for (int iTest = iIndex+1; iTest < m_iNRects; iTest++)
        {
            if (SameTopBottom(iTest, &rThis))
            {
                // Top and bottom congruent, see if left or right are a match
                if (abs(rThis.XLeft()-XRight(iTest))<=1)
                {
                    CombineHz(iIndex, iTest);
                    bCombined = TRUE;
                }
                else if (abs(rThis.XRight()-X(iTest))<=1)
                {
                    CombineHz(iIndex, iTest);
                    bCombined = TRUE;
                }
            }
            else if (SameLeftRight(iTest, &rThis))
            {
                if (abs(rThis.YTop()-YBottom(iTest))<=1)
                {
                    CombineVt(iIndex, iTest);
                    bCombined = TRUE;
                }
                else if (abs(rThis.YBottom()-Y(iTest))<=1)
                {
                    CombineVt(iIndex, iTest);
                    bCombined = TRUE;
                }
            }
        }
        if (!bCombined)
            iIndex++;
    }
}
/*============================================================================*/

    void                        CRectList::CombineHz(int iDstIndex, int iSrcIndex)

/*============================================================================*/
{
    int iXL = __min(X(iDstIndex), X(iSrcIndex));
    int iXR = __max(XRight(iDstIndex), XRight(iSrcIndex));
    m_iX0[iDstIndex] = iXL;
    m_iDX[iDstIndex] = (iXR-iXL+1);
    DeleteRect(iSrcIndex);
}
/*============================================================================*/

    void                        CRectList::CombineVt(int iDstIndex, int iSrcIndex)

/*============================================================================*/
{
    int iYT = __min(Y(iDstIndex), Y(iSrcIndex));
    int iYB = __max(YBottom(iDstIndex), YBottom(iSrcIndex));
    m_iY0[iDstIndex] = iYT;
    m_iDY[iDstIndex] = (iYB-iYT+1);
    DeleteRect(iSrcIndex);
}
/*============================================================================*/

    void                        CRectList::SortForTranslate(
    int                         iDX,
    int                         iDY )

/*============================================================================*/
{
    if (iDX < 0)
    {
        BOOL bSort = TRUE;
        while (bSort)
        {
            bSort = FALSE;
            for (int i = 0; i < m_iNRects-1; i++)
            {
                if (m_iX0[i] > m_iX0[i+1])
                {
                    Swap(i, i+1);
                    bSort = TRUE;    
                }
            }
        }
    }
    if (iDX > 0)
    {
        BOOL bSort = TRUE;
        while (bSort)
        {
            bSort = FALSE;
            for (int i = 0; i < m_iNRects-1; i++)
            {
                if (m_iX0[i] < m_iX0[i+1])
                {
                    Swap(i, i+1);
                    bSort = TRUE;    
                }
            }
        }
    }

    if (iDY < 0)
    {
        BOOL bSort = TRUE;
        while (bSort)
        {
            bSort = FALSE;
            for (int i = 0; i < m_iNRects-1; i++)
            {
                if (m_iY0[i] > m_iY0[i+1])
                {
                    Swap(i, i+1);
                    bSort = TRUE;    
                }
            }
        }
    }
    if (iDY > 0)
    {
        BOOL bSort = TRUE;
        while (bSort)
        {
            bSort = FALSE;
            for (int i = 0; i < m_iNRects-1; i++)
            {
                if (m_iY0[i] < m_iY0[i+1])
                {
                    Swap(i, i+1);
                    bSort = TRUE;    
                }
            }
        }
    }
}
/*============================================================================*/

    BOOL                        CRectList::CombineAtIndex(
    int                         iIndex,
    CHLRect                     *pTest )

/*============================================================================*/
{
    if (SameTopBottom(iIndex, pTest))
    {
        if (XRight(iIndex) >= (pTest->XLeft()-1) && X(iIndex) < pTest->XRight())
        {
            // Our right edge is close (or inside other)
            XRight(iIndex, __max(XRight(iIndex), pTest->XRight()));
            XLeft(iIndex, __min(X(iIndex), pTest->XLeft()));
            return TRUE;
        }

        if (X(iIndex) <= (pTest->XRight()+1) && XRight(iIndex) > pTest->XLeft())
        {
            // Our left edge is close (or inside other)
            XRight(iIndex, __max(XRight(iIndex), pTest->XRight()));
            XLeft(iIndex, __min(X(iIndex), pTest->XLeft()));
            return TRUE;
        }
    }
    if (SameLeftRight(iIndex, pTest))
    {
        if (YBottom(iIndex) >= (pTest->YTop()-1) && Y(iIndex) < pTest->YBottom())
        {
            // Our bottom edge is close (or inside other)
            YBottom(iIndex, __max(YBottom(iIndex), pTest->YBottom()));
            YTop(iIndex, __min(Y(iIndex), pTest->YTop()));
            return TRUE;
        }

        if (Y(iIndex) <= (pTest->YBottom()+1) && YBottom(iIndex) > pTest->YTop())
        {
            // Our left edge is close (or inside other)
            YBottom(iIndex, __max(YBottom(iIndex), pTest->YBottom()));
            YTop(iIndex, __min(Y(iIndex), pTest->YTop()));
            return TRUE;
        }
    }

    return FALSE;
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexContainsX(int iIndex, int iX)

/*============================================================================*/
{
    if (iX < X(iIndex))
        return FALSE;
    if (iX > XRight(iIndex))
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CRectList::RectAtIndexContainsY(int iIndex, int iY)

/*============================================================================*/
{
    if (iY < Y(iIndex))
        return FALSE;
    if (iY > YBottom(iIndex))
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    void                        CRectList::SplitBreakOffLeft(int iIndex, int iDX)

/*============================================================================*/
{
    if (iDX <= 0)
        return;

    AddRectDirectDXDY(X(iIndex), Y(iIndex), iDX, DY(iIndex));
    m_iX0[iIndex] += iDX;
    m_iDX[iIndex] -= iDX;
}
/*============================================================================*/

    void                        CRectList::SplitBreakOffRight(int iIndex, int iDX)

/*============================================================================*/
{
    if (iDX <= 0)
        return;

    AddRectDirectDXDY(XRight(iIndex)-iDX+1, Y(iIndex), iDX, DY(iIndex));
    m_iDX[iIndex] -= iDX;
}
/*============================================================================*/

    void                        CRectList::SplitBreakOffTop(int iIndex, int iDY)

/*============================================================================*/
{
    if (iDY <= 0)
        return;

    AddRectDirectDXDY(X(iIndex), Y(iIndex), DX(iIndex), iDY);
    m_iY0[iIndex] += iDY;
    m_iDY[iIndex] -= iDY;
}
/*============================================================================*/

    void                        CRectList::SplitBreakOffBottom(int iIndex, int iDY)

/*============================================================================*/
{
    if (iDY <= 0)
        return;

    AddRectDirectDXDY(X(iIndex), YBottom(iIndex)-iDY+1, DX(iIndex), iDY);
    m_iDY[iIndex] -= iDY;
}
/*============================================================================*/

    void                        CRectList::DeleteRect(
    int                         iIndex )

/*============================================================================*/
{
    LOGASSERT(iIndex >= 0 && iIndex < m_iNRects);
    m_iNRects--;
    memmove(m_iX0 + iIndex, m_iX0 + iIndex + 1, m_iNRects * sizeof(int));
    memmove(m_iY0 + iIndex, m_iY0 + iIndex + 1, m_iNRects * sizeof(int));
    memmove(m_iDX + iIndex, m_iDX + iIndex + 1, m_iNRects * sizeof(int));
    memmove(m_iDY + iIndex, m_iDY + iIndex + 1, m_iNRects * sizeof(int));
}
/*============================================================================*/

    DWORD                       CRectList::Area()

/*============================================================================*/
{
    DWORD dwSum = 0;
    for (int i = 0; i < m_iNRects; i++)
    {
        dwSum += (m_iDX[i] * m_iDY[i]);
    }

    return dwSum;
}
/*============================================================================*/

    CHLRect                     CRectList::GetBounds()

/*============================================================================*/
{
    if (m_iNRects < 1)
        return CDXYRect(0, 0, 0, 0);

    int iXMin = m_iX0[0];
    int iYMin = m_iY0[0];
    int iXMax = iXMin + m_iDX[0];
    int iYMax = iYMin + m_iDY[0];
    for (int i = 1; i < m_iNRects; i++)
    {
        iXMin = __min(m_iX0[i], iXMin);
        iYMin = __min(m_iY0[i], iYMin);
        iXMax = __max(iXMax, m_iX0[i] + m_iDX[i]);
        iYMax = __max(iYMax, m_iY0[i] + m_iDY[i]);
    }

    return CDXYRect(iXMin, iYMin, iXMax-iXMin+1, iYMax-iYMin+1);
}
