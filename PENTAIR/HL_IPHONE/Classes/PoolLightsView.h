//
//  PoolLightsView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/6/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "sink.h"

@class CNSPoolConfig;
@class CNSPoolCircuit;

/*============================================================================*/

@interface CPoolLightsView : UIView  < CSink >

/*============================================================================*/
{

    CNSPoolConfig                   *m_pConfig;
    UIScrollView                    *m_pScrollView;
    NSMutableArray                  *m_pControls;
}

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig;

@end


/*============================================================================*/

@interface CLightCircuitWnd : UIView

/*============================================================================*/
{
    UISlider                        *m_pSlider;
    UISwitch                        *m_pSwitch;
    UILabel                         *m_pLabel;
    CNSPoolCircuit                  *m_pCircuit;
}

-(id)initWithFrame:(CGRect)rect Circuit:(CNSPoolCircuit*)pCircuit;

-(void)Update;

@end