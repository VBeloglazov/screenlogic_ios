//
//  MainTabCell.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/8/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMainTab;
@class CMainTabIconView;
@class CAccesoryView;
@class CGradientView;

/*============================================================================*/

@interface CMainTabCell : UITableViewCell

/*============================================================================*/
{
	UILabel                 *m_pLabel;
    CGradientView           *m_pGradientView1;
    CGradientView           *m_pGradientView2;
	CMainTabIconView        *m_pIcon;
    CMainTab                *m_pMainTab;
    UIView                  *m_pAccessoryView;
}

-(void)SetMainTab:(CMainTab*)pTab;

@property (nonatomic, retain) CMainTab *m_pMainTab;
@property (nonatomic, retain) UILabel *m_pLabel;

-(void)SetHighlighted:(BOOL)bHighlighted;
-(void)SetAccessoryView:(UIView*)pView;

@end

