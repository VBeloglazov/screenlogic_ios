/*
--------------------------------------------------------------------------------

    HLSOCK.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"
#include "hltime.h"
#include "httphelp.h"

#ifndef MAC_OS
    #include "iphlpapi.h"
#endif

#include "hlsock.h"

#ifdef MINIPAD
    #include "hlserver.h"
    #include "hloblist.h"
    #include "..\minipad\minipad.h"
    #include "..\minipad\mainwnd.h"
#endif


#ifdef UNDER_CE
    #include "winioctl.h"
    #include "ntddndis.h"
    #ifndef HL_ARM
        #include "nuiouser.h"
    #endif
#endif

    //
    // Statics.
    //
//    int                         g_iSOCKADDRSize     = sizeof(SOCKADDR);
//    unsigned long               g_ulTrue            = TRUE;


#ifdef MAC_OS
/*============================================================================*/

    void                    SetIP(
    SOCKADDR_IN             *pAddr,
    BYTE                    by1,
    BYTE                    by2,
    BYTE                    by3,
    BYTE                    by4 )

/*============================================================================*/
{
    // BYTE 1 is lowest bits (ie 192.)
    DWORD dw1 = by4;
    DWORD dw2 = by3;
    DWORD dw3 = by2;
    DWORD dw4 = by1;
    DWORD dwIP = (dw1 << 24) | (dw2 << 16) | (dw3 << 8) | dw4;
    pAddr->sin_addr.s_addr = dwIP;
}
/*============================================================================*/

    BYTE                    GetIP(
    SOCKADDR_IN             *pAddr,
    int                     iDigit )

/*============================================================================*/
{
    DWORD dwIP = pAddr->sin_addr.s_addr;
    DWORD dwMask = (0xFF << (8*iDigit));
    return ((dwIP & dwMask) >> (8*iDigit)) & 0xFF;
}
#endif

#ifndef MAC_OS
/*============================================================================*/

    int                         CountAdapters()

/*============================================================================*/
{
    DWORD lSize = 0;
    GetIpAddrTable(NULL, &lSize, FALSE);
    BYTE *pTable = new BYTE[lSize];
    PMIB_IPADDRTABLE pIPTABLE = (PMIB_IPADDRTABLE) pTable;
    int iRet = 0;

    if (GetIpAddrTable(pIPTABLE, &lSize, FALSE) == NO_ERROR)
        iRet = pIPTABLE->dwNumEntries;
    delete [] pTable;
    return iRet;
}
/*============================================================================*/

    BOOL                        HLLoadIPAddress(
    int                         iIndex,
    SOCKADDR_IN                 *pIP )

/*============================================================================*/
{
    DWORD dwIP = 0;
    if (!GetNetInfo(iIndex, NULL, &dwIP))
        return FALSE;
    pIP->sin_addr.S_un.S_addr = dwIP;
    return TRUE;
}
#endif
/*============================================================================*/

    BOOL                        HLLoadIPAddress(
    SOCKADDR_IN                 *pIP )

/*============================================================================*/
{
    #ifdef MAC_OS
        SetIP(pIP, 127, 0, 0, 1);
        return TRUE;
        /*
        char cName[512];
        memset(cName, 0, 512);
        if (gethostname(cName,512) == 0)
        {
            // OK
            const char *pName = cName;
            hostent *pEnt = gethostbyname(pName);
            if (pEnt == NULL)
                return FALSE;
            memcpy(&(pIP->sin_addr.s_addr), pEnt->h_addr, pEnt->h_length);
            return TRUE;
        }

        return FALSE;
        */
    #else
        return HLLoadIPAddress(0, pIP);
    #endif
}
#if defined(GATEWAY) | defined(MONITOR)
/*============================================================================*/

    BOOL                        HLLoadIPInfo(
    SOCKADDR_IN                 *pIPAddress,
    SOCKADDR_IN                 *pSubNet,
    SOCKADDR_IN                 *pGateway,
    SOCKADDR_IN                 *pDNS )

/*============================================================================*/
{
    memset(pIPAddress,  0, sizeof(SOCKADDR_IN));
    memset(pSubNet,     0, sizeof(SOCKADDR_IN));
    memset(pGateway,    0, sizeof(SOCKADDR_IN));
    memset(pDNS,        0, sizeof(SOCKADDR_IN));

    #ifdef UNDER_CE
        HKEY keyCard = NetworkAdapterKey();
        if (keyCard == NULL)
            return FALSE;

        if (!GetAddress(keyCard, _T("IpAddress"), pIPAddress))
            goto ABORT;
        if (!GetAddress(keyCard, _T("Subnetmask"), pSubNet))
            goto ABORT;
        if (!GetAddress(keyCard, _T("DefaultGateway"), pGateway))
            goto ABORT;
        if (!GetAddress(keyCard, _T("DNS"), pDNS))
            goto ABORT;

        RegCloseKey(keyCard);
        return TRUE;
    #else
        HKEY keyCard = NetworkAdapterKey();
        if (keyCard == NULL)
            return FALSE;

        if (!GetAddress(keyCard, _T("IPAddress"), pIPAddress))
            goto ABORT;
        if (!GetAddress(keyCard, _T("SubnetMask"), pSubNet))
            goto ABORT;
        if (!GetAddress(keyCard, _T("DefaultGateway"), pGateway))
            goto ABORT;
        if (!GetAddress(keyCard, _T("NameServer"), pDNS))
            goto ABORT;

        RegCloseKey(keyCard);
        return TRUE;
    #endif


ABORT:
    RegCloseKey(keyCard);
    return FALSE;
}
/*============================================================================*/

    BOOL                        HLSetIPInfo(
    SOCKADDR_IN                 *pIPAddress,
    SOCKADDR_IN                 *pSubNet,
    SOCKADDR_IN                 *pGateway,
    SOCKADDR_IN                 *pDNS )

/*============================================================================*/
{
    #ifdef UNDER_CE
        HKEY keyCard = NetworkAdapterKey();
        if (keyCard == NULL)
            return FALSE;

        if (!SetAddress(keyCard, _T("IpAddress"), pIPAddress, TRUE))
            return FALSE;
        if (!SetAddress(keyCard, _T("Subnetmask"), pSubNet, TRUE))
            return FALSE;
        if (!SetAddress(keyCard, _T("DefaultGateway"), pGateway, TRUE))
            return FALSE;
        if (!SetAddress(keyCard, _T("DNS"), pDNS, FALSE))
            return FALSE;

        DWORD dwDHCP = 0;
        if (pIPAddress->sin_addr.S_un.S_addr == 0)
            dwDHCP = 1;

        if (RegSetValueEx(keyCard, _T("EnableDHCP"), NULL, REG_DWORD, (BYTE*)&dwDHCP, sizeof(DWORD)) != ERROR_SUCCESS)
            return FALSE;

        RegCloseKey(keyCard);
        return TRUE;
    #else
        HKEY keyCard = NetworkAdapterKey();
        if (keyCard == NULL)
            return FALSE;

        if (!SetAddress(keyCard, _T("IPAddress"), pIPAddress, TRUE))
            return FALSE;
        if (!SetAddress(keyCard, _T("SubnetMask"), pSubNet, TRUE))
            return FALSE;
        if (!SetAddress(keyCard, _T("DefaultGateway"), pGateway, TRUE))
            return FALSE;
        if (!SetAddress(keyCard, _T("NameServer"), pDNS, FALSE))
            return FALSE;

        DWORD dwNO = 0;
        if (RegSetValueEx(keyCard, _T("EnableDHCP"), NULL, REG_DWORD, (BYTE*)&dwNO, sizeof(DWORD)) != ERROR_SUCCESS)
            return FALSE;

        RegCloseKey(keyCard);
        return TRUE;
    #endif
}
/*============================================================================*/

    HKEY                        NetworkAdapterKey()

/*============================================================================*/
{
    HKEY keyComm = NULL;

    #ifdef UNDER_CE
        #ifdef HL_X86C
            if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Comm\\PCI\\RTCENIC1\\Parms\\TCPIP"), 0, 0, &keyComm) != ERROR_SUCCESS)
                return NULL;
            return keyComm;
        #else
            if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Comm"), 0, 0, &keyComm) != ERROR_SUCCESS)
                return FALSE;
        #endif
    #else
        if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces"), 0, KEY_ENUMERATE_SUB_KEYS, &keyComm) != ERROR_SUCCESS)
            return FALSE;
    #endif

    SOCKADDR_IN thisIP;
    HLLoadIPAddress(&thisIP);
    HKEY keyCard = IterateFindNetworkKey(keyComm, &thisIP);


    RegCloseKey(keyComm);
    return keyCard;
}
/*============================================================================*/

    HKEY                        IterateFindNetworkKey(
    HKEY                        keyStart,
    SOCKADDR_IN                 *pActiveIP )

/*============================================================================*/
{
    TCHAR pName[256];
    DWORD dwSizeName = 256;
    HKEY keyCard = NULL;


    DWORD dwIndex = 0;
    while (RegEnumKeyEx(keyStart, dwIndex, pName, &dwSizeName, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
    {
    #ifdef UNDER_CE
        if (RegOpenKeyEx(keyStart, pName, 0, 0, &keyCard) == ERROR_SUCCESS)
    #else
        if (RegOpenKeyEx(keyStart, pName, 0, KEY_ALL_ACCESS, &keyCard) == ERROR_SUCCESS)
    #endif
        {

            CHLString sName = pName;
            if (sName.CompareNoCase(_T("Parms")) == 0)
            {
                int iTest = 0;
            }

            HKEY keyInside = IterateFindNetworkKey(keyCard, pActiveIP);
            if (keyInside != NULL)
            {
                RegCloseKey(keyCard);
                return keyInside;
            }
            
            if (IsNetworkKey(keyCard, pActiveIP))
                return keyCard;

            // Keep going
            RegCloseKey(keyCard);
        }

        dwIndex++;
        dwSizeName = 256;
    }

    return NULL;
}
/*============================================================================*/

    BOOL                        IsNetworkKey(
    HKEY                        keyTry,
    SOCKADDR_IN                 *pActiveIP )

/*============================================================================*/
{
    SOCKADDR_IN address, subnet, gateway, dns;

    #ifdef UNDER_CE
        if (!GetAddress(keyTry, _T("IpAddress"), &address))
            return FALSE;
        if (!GetAddress(keyTry, _T("Subnetmask"), &subnet))
            return FALSE;
        if (!GetAddress(keyTry, _T("DefaultGateway"), &gateway))
            return FALSE;
        if (!GetAddress(keyTry, _T("DNS"), &dns))
            return FALSE;
    #else
        if (!GetAddress(keyTry, _T("IPAddress"), &address))
            return FALSE;

        // Make sure it matches Adapter 0
        DWORD dw1 = address.sin_addr.S_un.S_addr;
        DWORD dw2 = pActiveIP->sin_addr.S_un.S_addr;
        if (dw1 != dw2)
            return FALSE;

        if (!GetAddress(keyTry, _T("SubnetMask"), &subnet))
            return FALSE;
        if (!GetAddress(keyTry, _T("DefaultGateway"), &gateway))
            return FALSE;
        if (!GetAddress(keyTry, _T("NameServer"), &dns))
            return FALSE;
    #endif

    return TRUE;
}
/*============================================================================*/

    BOOL                        GetAddress(
    HKEY                        keyTry,
    CHLString                   sParam,
    SOCKADDR_IN                 *pAddress )

/*============================================================================*/
{
    TCHAR pData[256];
    DWORD dwSize = 256 * sizeof(TCHAR);
    DWORD dwType = 0;

    if (RegQueryValueEx(keyTry, (const TCHAR*) sParam, NULL, &dwType, (BYTE*)pData, &dwSize) != ERROR_SUCCESS)
        return FALSE;

    // There can be multiple entries for these strings with NULL separators, we want to take the
    // last substring so scan from reverse to find the last string
    int iStartIndex = dwSize/2-1;
    BOOL bOK = FALSE;
    while (iStartIndex > 0 && !bOK)
    {
        if (pData[iStartIndex] != 0)
            bOK = TRUE;
        else
            iStartIndex--;
    }

    bOK = FALSE;
    while (iStartIndex > 0 && !bOK)
    {
        if (pData[iStartIndex] == 0)
        {
            iStartIndex++;
            bOK = TRUE;
        }
        else
        {
            iStartIndex--;
        }
    }

    BYTE by1, by2, by3, by4;
    CHLString sIP = (TCHAR*) (pData+iStartIndex);
    int iLen = sIP.GetLength();
    char *pBuf = sIP.GetBufferCHAR(iLen);
    if (!ParseAddress(pBuf, _T(""), &by1, &by2, &by3, &by4, NULL))
    {
        memset(pAddress, 0, sizeof(SOCKADDR_IN));
        return TRUE;
    }

    memset(pAddress, 0, sizeof(SOCKADDR_IN));
    pAddress->sin_addr.S_un.S_un_b.s_b1 = by1;
    pAddress->sin_addr.S_un.S_un_b.s_b2 = by2;
    pAddress->sin_addr.S_un.S_un_b.s_b3 = by3;
    pAddress->sin_addr.S_un.S_un_b.s_b4 = by4;

    return TRUE;
}
/*============================================================================*/

    BOOL                        SetAddress(
    HKEY                        keyTry,
    CHLString                   sParam,
    SOCKADDR_IN                 *pAddress,
    BOOL                        bMultiSz )

/*============================================================================*/
{
    CHLString sIP;
    sIP.Format(_T("%d.%d.%d.%d"),   pAddress->sin_addr.S_un.S_un_b.s_b1,
                                    pAddress->sin_addr.S_un.S_un_b.s_b2,
                                    pAddress->sin_addr.S_un.S_un_b.s_b3,
                                    pAddress->sin_addr.S_un.S_un_b.s_b4 );

    int iLen = sIP.GetLength();
    TCHAR *pData = (TCHAR*)sIP.GetBufferTCHAR(iLen+2);
    pData[iLen] = 0;
    pData[iLen+1] = 0;

    DWORD dwType = REG_SZ;
    int iLenReg = iLen + 1;
    if (bMultiSz)
    {   
        dwType = REG_MULTI_SZ;
        iLenReg++;
    }

    if (RegSetValueEx(keyTry, (const TCHAR*) sParam, NULL, dwType, (BYTE*)pData, iLenReg * sizeof(TCHAR)) != ERROR_SUCCESS)
        return FALSE;
    return TRUE;
}
#endif
/*============================================================================*/

    CHLString                   HLFormatAddress(
    SOCKADDR_IN                 *pIP )

/*============================================================================*/
{
    #ifdef MAC_OS
    
        DWORD dwAddress = pIP->sin_addr.s_addr;
    #else
        DWORD dwAddress = pIP->sin_addr.S_un.S_addr;
    #endif

    return HLFormatAddress(dwAddress);    
}
/*============================================================================*/

    CHLString                   HLFormatAddress(
    DWORD                       dwIP )

/*============================================================================*/
{
    BYTE by1 = (BYTE)(dwIP & 0x000000FF);
    BYTE by2 = (BYTE)((dwIP & 0x0000FF00) >> 8);
    BYTE by3 = (BYTE)((dwIP & 0x00FF0000) >> 16);
    BYTE by4 = (BYTE)((dwIP & 0xFF000000) >> 24);

    CHLString sAddress;
    sAddress.Format(_T("%d.%d.%d.%d"), by1, by2, by3, by4);
    return sAddress;
}
#ifndef MAC_OS
/*============================================================================*/

    BOOL                        GetNetInfo(
    int                         iAdapter,
    DWORD                       *pdwMask,
    DWORD                       *pdwLocalIP )

/*============================================================================*/
{
#ifdef RELAYSERVER
    return FALSE;
#else
    DWORD dwReturn = 0;
    PIP_ADAPTER_INFO pAdapterInfo = NULL;
    DWORD dwLen = 0;

    // Should fail then we get the size from dwLen
    if (GetAdaptersInfo(pAdapterInfo, &dwLen) == ERROR_SUCCESS)
        return 0;

    // Memory to hold the linked list
    BYTE *pData = new BYTE[dwLen];
    pAdapterInfo = (PIP_ADAPTER_INFO)pData;
    if (pAdapterInfo == NULL)
        return 0;

    // Get for real
    if (!GetAdaptersInfo(pAdapterInfo, &dwLen) == ERROR_SUCCESS)
    {
        delete [] pData;
        return 0;
    }

    PIP_ADAPTER_INFO pAdapter = pAdapterInfo;

    while (pAdapter != NULL)
    {
        // Try to get the name
        #ifdef UNDER_CE
            char *pName = pAdapter->AdapterName;
        #else
            char *pName = pAdapter->Description;
        #endif

        BOOL bUSBAdapter = FALSE;
        #ifdef HL_X86C
            if (strncmp(pName, "AX", 2) == 0)
                bUSBAdapter = TRUE;
        #else
            if (strncmp(pName, "ASIX", 4) == 0)
                bUSBAdapter = TRUE;
        #endif
    

        int iThisIndex = 0;
        if (bUSBAdapter)
            iThisIndex = 1;

        if (iThisIndex == iAdapter)
        {
            PIP_ADDR_STRING pAddressString = &(pAdapter->IpAddressList);
            while (pAddressString != NULL)
            {
                IP_ADDRESS_STRING pString = pAddressString->IpAddress;
                
                char buf[32];
                memset(buf, 0, 32);
                memcpy(buf, pString.String, 16);
                DWORD dwThisIP = StringToIP(buf);

                BOOL bIPOK = TRUE;
//                if ((dwThisIP & 0x0000FFFF) == 0x0000640A)      
//                    bIPOK = FALSE;
                if (dwThisIP == 0x00000000)
                    bIPOK = FALSE;

                if (!bIPOK && pAddressString->Next != NULL)
                {
                    pAddressString = pAddressString->Next;
                }
                else
                {
                    // Normal IP
                    IP_ADDRESS_STRING pMaskString = pAddressString->IpMask;
                    DWORD dwIP = dwThisIP;
                    DWORD dwMask = StringToIP(pMaskString.String);
                    if (pdwMask != NULL)
                        *pdwMask = dwMask;
                    if (pdwLocalIP != NULL)
                        *pdwLocalIP = dwIP;
                    delete [] pData;
                    return TRUE;
                }
            }
        }

        pAdapter = pAdapter->Next;
    }

    delete [] pData;
    return FALSE;
#endif
}
/*============================================================================*/

    DWORD                       GetPrimaryIP()

/*============================================================================*/
{
    PIP_ADAPTER_INFO pAdapterInfo = NULL;
    DWORD dwLen = 0;

    // Should fail then we get the size from dwLen
    if (GetAdaptersInfo(pAdapterInfo, &dwLen) == ERROR_SUCCESS)
        return 0;

    // Memory to hold the linked list
    BYTE *pData = new BYTE[dwLen];
    pAdapterInfo = (PIP_ADAPTER_INFO)pData;
    if (pAdapterInfo == NULL)
        return 0;

    // Get for real
    if (!GetAdaptersInfo(pAdapterInfo, &dwLen) == ERROR_SUCCESS)
    {
        delete [] pData;
        return 0;
    }

    PIP_ADAPTER_INFO pAdapter = pAdapterInfo;
    while (pAdapter != NULL)
    {
        // Try to get the name
        // Try to get the name
        #ifdef UNDER_CE
            char *pName = pAdapter->AdapterName;
        #else
            char *pName = pAdapter->Description;
        #endif

        BOOL bUSBAdapter = FALSE;
        #ifdef HL_X86C
            if (strncmp(pName, "AX", 2) == 0)
                bUSBAdapter = TRUE;
        #else
            if (strncmp(pName, "ASIX", 4) == 0)
                bUSBAdapter = TRUE;
        #endif

        if (!bUSBAdapter)
        {
            PIP_ADDR_STRING pAddressString = &(pAdapter->IpAddressList);
            while (pAddressString != NULL)
            {
                IP_ADDRESS_STRING pString = pAddressString->IpAddress;
                
                char buf[32];
                memset(buf, 0, 32);
                memcpy(buf, pString.String, 16);
                DWORD dwThisIP = StringToIP(buf);

                BOOL bIPOK = TRUE;
//                if ((dwThisIP & 0x0000FFFF) == 0x0000640A)      
//                    bIPOK = FALSE;
                if (dwThisIP == 0x00000000)
                    bIPOK = FALSE;

                if (!bIPOK && pAddressString->Next != NULL)
                {
                    // Niles IP
                    pAddressString = pAddressString->Next;
                }
                else
                {
                    // Normal IP
                    delete [] pData;
                    return dwThisIP;
                }
            }
        }

        pAdapter = pAdapter->Next;
    }

    delete [] pData;
    return 0;
}
#endif
/*============================================================================*/

    BOOL                        ReadyToSend(
    SOCKET                      hSocket )

/*============================================================================*/
{
    if (hSocket == INVALID_SOCKET)
        return FALSE;

    //
    // See if there's room to send
    //
    fd_set set;
    fd_set setZero;
    
#ifdef IPHONE
    FD_ZERO(&setZero);
    FD_ZERO(&set);
    FD_SET(hSocket, &set);
#else
    setZero.fd_count    = 0;
    set.fd_count        = 1;
    set.fd_array[0]     = hSocket;
#endif

    timeval tv;
    tv.tv_sec   = 0;
    tv.tv_usec  = 10;

#ifdef IPHONE
    int iSel = select(hSocket+1, &setZero, &set, &setZero, &tv);
#else
    int iSel = select(0, &setZero, &set, &setZero, &tv);
#endif
    if (iSel <= 0)
    {
        // Not ready
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        ReadyToReceive(
    SOCKET                      hSocket )

/*============================================================================*/
{
    if (hSocket == INVALID_SOCKET)
        return FALSE;

    //
    // See if there's room to send
    //
    fd_set set;
    fd_set setZero;


#ifdef IPHONE
    FD_ZERO(&setZero);
    FD_ZERO(&set);
    FD_SET(hSocket, &set);
#else
    setZero.fd_count    = 0;
    set.fd_count        = 1;
    set.fd_array[0]     = hSocket;
#endif

    timeval tv;
    tv.tv_sec   = 0;
    tv.tv_usec  = 10;

#ifdef IPHONE
    int iSel = select(hSocket+1, &set, &setZero, &setZero, &tv);
#else
    int iSel = select(0, &set, &setZero, &setZero, &tv);
#endif

    if (iSel <= 0)
    {
        // Not ready
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    HL_API BOOL                 WaitSocketReadState(
    SOCKET                      hSocket,
    DWORD                       dwMSEC )

/*============================================================================*/
{
    if (hSocket == INVALID_SOCKET)
        return FALSE;

    //
    // See if there's room to send
    //
    fd_set setRead;
    fd_set setZero;


#ifdef IPHONE
    FD_ZERO(&setZero);
    FD_ZERO(&setRead);
    FD_SET(hSocket, &setRead);
#else
    setZero.fd_count    = 0;
    setRead.fd_count        = 1;
    setRead.fd_array[0]     = hSocket;
#endif

    timeval tv;
    tv.tv_sec   = 0;
    tv.tv_usec  = dwMSEC * 1000;

    //                   READ      WRITE     EXCEPTION
#ifdef IPHONE
    int iSel = select(hSocket+1, &setRead, &setZero, &setZero, &tv);
#else
    int iSel = select(0, &setRead, &setZero, &setZero, &tv);
#endif
    if (iSel <= 0)
    {
        // No Error and Not ready for read
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    HL_API BOOL                 WaitSocketWriteState(
    SOCKET                      hSocket,
    DWORD                       dwMSEC )

/*============================================================================*/
{
    if (hSocket == INVALID_SOCKET)
        return FALSE;

    //
    // See if there's room to send
    //
    fd_set setWrite;
    fd_set setZero;


#ifdef IPHONE
    FD_ZERO(&setZero);
    FD_ZERO(&setWrite);
    FD_SET(hSocket, &setWrite);
#else
    setZero.fd_count    = 0;
    setWrite.fd_count        = 1;
    setWrite.fd_array[0]     = hSocket;
#endif

    timeval tv;
    tv.tv_sec   = 0;
    tv.tv_usec  = dwMSEC * 1000;

    //                   READ      WRITE      EXCEPTION
#ifdef IPHONE
    int iSel = select(hSocket+1, &setZero, &setWrite, &setZero, &tv);
#else
    int iSel = select(0, &setZero, &setWrite, &setZero, &tv);
#endif

    if (iSel <= 0)
    {
        // No Error and Not ready for read
        return FALSE;
    }

    return TRUE;
}
#if defined(GATEWAY) | defined(RELAYAGENT) | defined(WSERVER) | defined(RADIOSERVER) | defined(HLUPDATE) | defined(HLSERVICE) | defined(HLCONFIG) | defined(HLSTART) | defined(HLSURVEY) | defined (CMXUPDATE)
/*============================================================================*/

    BOOL                        AsyncConnect(
    SOCKET                      hSocket,
    SOCKADDR_IN                 *pAddress,
    BOOL                        *pbAbort,
    DWORD                       dwMaxMSEC )

/*============================================================================*/
{
    DWORD uTrue = TRUE;

    if (pAddress == NULL)
        return FALSE;

    // Make non-blocking
        ioctlsocket(hSocket, FIONBIO, &uTrue);

    // Start Connect
    int iResult = ::connect(hSocket, (SOCKADDR*)pAddress, sizeof(SOCKADDR_IN));

    // Loop till we connect or timeout
    CHLTimer hltStart;
    BOOL bReady  = FALSE;
    while (!bReady)
    {
        if (hltStart.HLTickCount() > dwMaxMSEC)
        {
            // Timed out waiting to connect
            return FALSE;
        }

        fd_set set;
        fd_set setZero;
        setZero.fd_count    = 0;
        set.fd_count        = 1;
        set.fd_array[0]     = hSocket;

        timeval tv;
        tv.tv_sec   = 0;
        tv.tv_usec  = 1000;

#ifdef IPHONE
        int iSel = select(hSocket+1, &setZero, &set, &setZero, &tv);
#else
        int iSel = select(0, &setZero, &set, &setZero, &tv);
#endif

        // Ready?
        if (iSel > 0)
            bReady = TRUE;
        else
            Sleep(50);

        // Check on caller abort
        if (pbAbort != NULL)
        {
            if (*pbAbort)
                return FALSE;
        }
    }

    return TRUE;
}
#endif
#pragma optimize( "", off)
#pragma warning(disable: 4748)
/*============================================================================*/

    BOOL                        RestartZeroConfig()

/*============================================================================*/
{
    #ifndef UNDER_CE
        return TRUE;
    #else
        #ifdef _DEBUG
            return TRUE;
        #endif

        HINSTANCE hWZClib = NULL;

        // See if zero config API present in system
        if ((hWZClib = LoadLibrary(L"wzcsapi.dll")) == NULL) 
        {
            #ifdef MINIPAD
                g_pMainWindow->TraceOut(_T("ERROR: Can't LoadLibrary()"));
            #endif
            return FALSE;
        }

        PFN_WZCSetInterface      pfnWZCSetInterface;
        PFN_WZCQueryInterface    pfnWZCQueryInterface;

    
        INTF_ENTRY ifEntry;
        pfnWZCQueryInterface    = (PFN_WZCQueryInterface)GetProcAddress(hWZClib,L"WZCQueryInterface");
        pfnWZCSetInterface      = (PFN_WZCSetInterface)GetProcAddress(hWZClib,L"WZCSetInterface");

        if (pfnWZCQueryInterface == NULL || pfnWZCSetInterface == NULL)
        {
            #ifdef MINIPAD
                g_pMainWindow->TraceOut(_T("ERROR: Bad Function Pointer(s)"));
            #endif
            FreeLibrary(hWZClib);
            return FALSE;
        }

        BOOL bReturn = FALSE;

        DWORD dwSize = 0;
        if (GetAdaptersInfo(NULL, &dwSize) == NO_ERROR)
        {
            // Should'nt be zero here
            FreeLibrary(hWZClib);
            return FALSE;
        }    

        if (dwSize <= 0)
            return FALSE;

        BYTE *pIFData = new BYTE[dwSize];
        memset(pIFData, 0, dwSize);
        PIP_ADAPTER_INFO pAdapters = (PIP_ADAPTER_INFO)pIFData;
        if (GetAdaptersInfo(pAdapters, &dwSize) != NO_ERROR)
        {
            delete [] pIFData;  
            FreeLibrary(hWZClib);
            return FALSE;
        }

        while (pAdapters != NULL)
        {
            if (pAdapters->AdapterName != NULL)
            {
                if (strlen(pAdapters->AdapterName) > 0)
                {
                    CHLString sName = (char*) pAdapters->AdapterName;
                    DWORD dwOIDFlags = 0xFFFFFFFF;
                    memset(&ifEntry, 0, sizeof(INTF_ENTRY));

                    #if defined (CRYSTALPAD) | defined (GATEWAY) | defined(MINIPAD)
                        sName.ConvertToTCHAR();
                    #endif


                    #ifdef MINIPAD
                        CHLString sTrace;
                        sTrace.Format(_T("Trying Adapter: %s"), (const TCHAR*)sName);
                        g_pMainWindow->TraceOut(sTrace);
                    #endif

                    int iLen = sName.GetLength();
                    TCHAR *pBuf = sName.GetBufferTCHAR(iLen);
    	            ifEntry.wszGuid = pBuf;

                    if (pfnWZCQueryInterface(NULL, INTF_ALL, &ifEntry, &dwOIDFlags) == ERROR_SUCCESS)
                    {    
                        ifEntry.dwCtlFlags &= ~INTFCTL_ENABLED;

                        // Disable
                        if (pfnWZCSetInterface(NULL, INTF_CTLFLAGS ,&ifEntry,&dwOIDFlags) == ERROR_SUCCESS)
                            bReturn = TRUE;
                    
                        ifEntry.dwCtlFlags |= INTFCTL_ENABLED;
                        Sleep(500);

                        // Enable
                        if (pfnWZCSetInterface(NULL, INTF_CTLFLAGS,&ifEntry,&dwOIDFlags) == ERROR_SUCCESS)
                            bReturn = TRUE;

                    }
                }
            }

            pAdapters = pAdapters->Next;
        }


        delete [] pIFData;
        FreeLibrary(hWZClib);
        return bReturn;
    #endif
}
/*============================================================================*/

    int                         GetWirelessStrength()

/*============================================================================*/
{
    #ifndef UNDER_CE
        return -1;
    #else
        #if defined(HL_ARMV4IB) | defined(HL_ARMV4IC)
            return -1;
        #else
            IP_ADAPTER_INFO info[10];
            ULONG dwSize = 10 * sizeof(IP_ADAPTER_INFO);
            if (GetAdaptersInfo(info, &dwSize) == NO_ERROR)
            {
                IP_ADAPTER_INFO *pInfo = info;
                while (pInfo != NULL)
                {
                    CHLString sName = (char*) pInfo->AdapterName;
                    DWORD dwOIDFlags = 0xFFFFFFFF;
                    INTF_ENTRY ifEntry;
                    memset(&ifEntry, 0, sizeof(INTF_ENTRY));

                    #if defined (CRYSTALPAD) | defined (GATEWAY) | defined(MINIPAD)
                        sName.ConvertToTCHAR();
                    #endif

                    TCHAR *pBuf = sName.GetBufferTCHAR(sName.GetLength());
        	        ifEntry.wszGuid = pBuf;
                    int iLevel = GetWirelessStrength(pBuf);
                    if (iLevel >= 0)
                        return iLevel;

                    pInfo = pInfo->Next;
                }
            }
        #endif
        
        return -1;
    #endif
}
/*============================================================================*/

    int                         GetWirelessStrength(
    TCHAR                       *ptcDeviceName )

/*============================================================================*/
{

    #ifndef UNDER_CE
        return -1;
    #else
        #ifdef HL_ARM
            return -1;
        #else
            PNDISUIO_QUERY_OID queryOID; 
            DWORD  dwBytesReturned = 0; 
            UCHAR  QueryBuffer[sizeof(NDISUIO_QUERY_OID)+sizeof(DWORD)]; 
            HANDLE  ndisAccess = INVALID_HANDLE_VALUE; 
            BOOL  retval; 


            // Attach to NDISUIO. 
            ndisAccess = CreateFile(NDISUIO_DEVICE_NAME, 0, 0, NULL, 
                                    OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 
                                    INVALID_HANDLE_VALUE ); 


            if (ndisAccess == INVALID_HANDLE_VALUE) 
                return -1; 


            // Get Signal strength 
            queryOID = (PNDISUIO_QUERY_OID)&QueryBuffer[0]; 
            queryOID->ptcDeviceName = ptcDeviceName; 
            queryOID->Oid = OID_802_11_RSSI; 


            retval = DeviceIoControl(   ndisAccess, 
                                        IOCTL_NDISUIO_QUERY_OID_VALUE, (LPVOID)queryOID, 
                                        sizeof(NDISUIO_QUERY_OID) + sizeof(DWORD), (LPVOID)queryOID, 
                                        sizeof(NDISUIO_QUERY_OID) + sizeof(DWORD), &dwBytesReturned, NULL); 

            if (retval) 
            { 
                CloseHandle(ndisAccess); 
                DWORD dwLevel = *(DWORD *)&queryOID->Data; 
                int iDB = dwLevel;
                return -iDB;
            } 

            CloseHandle(ndisAccess); 
            return -1;
        #endif
    #endif
}
#pragma optimize( "", on)
#pragma warning(default: 4748)
/*============================================================================*/

    void                        SetAddress(
    SOCKADDR_IN                 *pAddress,
    BYTE                        by1,
    BYTE                        by2, 
    BYTE                        by3,
    BYTE                        by4,
    WORD                        wPort )

/*============================================================================*/
{
    #ifdef MAC_OS
        pAddress->sin_family = AF_INET;
        SetIP(pAddress, by1, by2, by3, by4);
        pAddress->sin_port  = htons(wPort);
    #else
        pAddress->sin_family = AF_INET;
        pAddress->sin_addr.S_un.S_un_b.s_b1 = by1;
        pAddress->sin_addr.S_un.S_un_b.s_b2 = by2;
        pAddress->sin_addr.S_un.S_un_b.s_b3 = by3;
        pAddress->sin_addr.S_un.S_un_b.s_b4 = by4;
        pAddress->sin_port  = htons(wPort);
    #endif
}
/*============================================================================*/

    void                        GetAddress(
    SOCKADDR_IN                 *pAddress,
    BYTE                        *by1,
    BYTE                        *by2, 
    BYTE                        *by3,
    BYTE                        *by4,
    WORD                        *wPort )

/*============================================================================*/
{
    #ifdef MAC_OS
        DWORD dwIP = pAddress->sin_addr.s_addr;
    #else
        DWORD dwIP = pAddress->sin_addr.S_un.S_addr;
    #endif

    *by1 = (BYTE)(dwIP & 0x000000FF);
    *by2 = (BYTE)((dwIP & 0x0000FF00) >> 8);
    *by3 = (BYTE)((dwIP & 0x00FF0000) >> 16);
    *by4 = (BYTE)((dwIP & 0xFF000000) >> 24);
    *wPort = ntohs(pAddress->sin_port);
}
/*============================================================================*/

    BOOL                        SetNonBlocking(
    SOCKET                      hSocket )

/*============================================================================*/
{
    // ioctlsocket takes non-zero for non-blocking, zero for blocking.
    #ifdef MAC_OS
        // ioctlsocket takes non-zero for non-blocking, zero for blocking.
        int iRes = fcntl(hSocket, F_SETFL, fcntl(hSocket,F_GETFL) | O_NONBLOCK);
        return (iRes == 0);
    #else
        BOOL bParam = TRUE;
        int iReturn = ioctlsocket(hSocket, FIONBIO, (DWORD*)&bParam);
        return (iReturn == 0);
    #endif
}
/*============================================================================*/

    BOOL                        SetBlocking(
    SOCKET                      hSocket )

/*============================================================================*/
{
    // ioctlsocket takes non-zero for non-blocking, zero for blocking.
    #ifdef MAC_OS
        int iRes = fcntl(hSocket, F_SETFL, fcntl(hSocket,F_GETFL) & ~O_NONBLOCK);
        return (iRes == 0);
    #else
        BOOL bParam = FALSE;
        int iReturn = ioctlsocket(hSocket, FIONBIO, (DWORD*)&bParam);
        return (iReturn == 0);
    #endif
}
/*============================================================================*/

    void                        CloseSocket(
    SOCKET                      hSocket )

/*============================================================================*/
{
    shutdown(hSocket,0);
    closesocket(hSocket);
}
/*============================================================================*/

    BOOL                        HLRecvFrom(
    SOCKET                      hSocket,
    char                        *pBuffer,
    int                         iNMaxRecv,
    SOCKADDR_IN                 *pAddressFrom )

/*============================================================================*/
{
    #ifdef MAC_OS
        socklen_t iHostSize = sizeof(SOCKADDR_IN);
        if (recvfrom(hSocket, pBuffer, iNMaxRecv, 0, (SOCKADDR*)pAddressFrom, &iHostSize) == (int) iNMaxRecv)
            return TRUE;
        return FALSE;
    #else
        int iHostSize = sizeof(SOCKADDR_IN);

        if (recvfrom(hSocket, pBuffer, iNMaxRecv, 0, (SOCKADDR*)pAddressFrom, &iHostSize) == (int) iNMaxRecv)
            return TRUE;
        return FALSE;
    #endif
}
/*============================================================================*/

    int                         HLRecvFromUpTo(
    SOCKET                      hSocket,
    char                        *pBuffer,
    int                         iNMaxRecv,
    SOCKADDR_IN                 *pAddressFrom )

/*============================================================================*/
{
    #ifdef MAC_OS
        socklen_t iHostSize = sizeof(SOCKADDR_IN);
    #else
        int iHostSize = sizeof(SOCKADDR_IN);
    #endif

    return recvfrom(hSocket, pBuffer, iNMaxRecv, 0, (SOCKADDR*)pAddressFrom, &iHostSize);
}
/*============================================================================*/

    BOOL                        IsAddress(
    CHLString                   sTest )

/*============================================================================*/
{
    int iLen = sTest.GetLength();
    #if defined(UNICODE) & defined(HLUTIL)
        char pBuf[256];
        iLen = __min(255, iLen);
        TCHAR *pBufIn = sTest.GetBuffer(iLen);
        wcstombs(pBuf, (wchar_t*) pBufIn, iLen);
    #else
        char *pBuf = sTest.GetBufferCHAR(iLen);
    #endif


    char *pStart = pBuf;
    for (int i = 0; i < 3; i++)
    {
        char *pPeriod = strstr(pStart, ".");
        if (pPeriod == NULL)
            return FALSE;
        // OK Got a '.' make sure stuff to the left is numeric
        if ((pPeriod - pBuf) < 1)
            return FALSE;
        char *pLeft = pPeriod-1;
        if (*pLeft < '0' || *pLeft > '9')
            return FALSE;

        if (i == 3)
        {
            char *pRight = pPeriod+1;
            if (*pRight < '0' || *pRight > '9')
                return FALSE;
        }
        else
        {
            pStart = (pPeriod + 1);
        }
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        ParseAddress(
    char                        *pData,
    CHLString                   sKey,
    BYTE                        *pBy1,
    BYTE                        *pBy2,
    BYTE                        *pBy3,
    BYTE                        *pBy4,
    UINT                        *pPort )

/*============================================================================*/
{
    int iKeyLen = sKey.GetLength();
    char *pStart = pData;

    if (iKeyLen > 0)
    {
        char *pKey = (char*)sKey.GetBufferCHAR(iKeyLen);
        pStart = strstr(pData, pKey);
        if (pStart == NULL)
            return FALSE;
        pStart += iKeyLen;
    }
    
    BYTE by1 = 0;
    BYTE by2 = 0;
    BYTE by3 = 0;
    BYTE by4 = 0;
    UINT nPort = 0;

    char *pEnd = strstr(pStart, ".");
    if (pEnd == NULL)
        return FALSE;
    *pEnd = 0;
    by1 = atoi(pStart);
    *pEnd = ' ';

    pStart = pEnd+1;
    pEnd = strstr(pStart, ".");
    if (pEnd == NULL)
        return FALSE;
    *pEnd = 0;
    by2 = atoi(pStart);
    *pEnd = ' ';

    pStart = pEnd+1;
    pEnd = strstr(pStart, ".");
    if (pEnd == NULL)
        return FALSE;
    *pEnd = 0;
    by3 = atoi(pStart);
    *pEnd = ' ';

    pEnd++;
    pStart = pEnd;
    if (pPort == NULL)
    {
        // No port, just look for EOL
        while ((*pEnd != 0) && (*pEnd != '\r') && (*pEnd != '\n'))
            pEnd++;
        *pEnd = 0;
        by4 = atoi(pStart);
        *pEnd = ' ';
    }
    else
    {
        // Need port, just look for EOL
        while ((*pEnd != 0) && (*pEnd != ':'))
            pEnd++;
        if (*pEnd == 0)
            return FALSE;
        *pEnd = 0;
        by4 = atoi(pStart);
        *pEnd = ' ';

        // Need port, just look for EOL
        pEnd++;
        pStart = pEnd;
        while ((*pEnd != 0) && (*pEnd != '\n') && (*pEnd != '\r'))
            pEnd++;
        *pEnd = 0;
        nPort = atoi(pStart);
        *pEnd = ' ';
    }

    if (pBy1 != NULL)
        *pBy1 = by1;
    if (pBy2 != NULL)
        *pBy2 = by2;
    if (pBy3 != NULL)
        *pBy3 = by3;
    if (pBy4 != NULL)
        *pBy4 = by4;
    if (pPort != NULL)
        *pPort = nPort;
    return TRUE;
}
/*============================================================================*/

    DWORD                       StringToIP(
    char                        *pString )

/*============================================================================*/
{
    DWORD dwReturn = 0;
    char buf[32];
    memset(buf, 0, 32);
    memcpy(buf, pString, 16);
    char *pStart = buf;
    char *pNext = strstr(pStart, ".");

    for (int i = 0; i < 4; i++)
    {
        if (pNext != NULL)
            *pNext = 0;
        DWORD dwOctet = atoi(pStart);  
        dwReturn |= (dwOctet << (i * 8));


        if (pNext != NULL)
        {
            pStart = pNext +1;
            pNext = strstr(pStart, ".");
        }
    }

    return dwReturn;
}
