//
//  MP3ContentView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 7/12/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "MP3ContentView.h"
#import "AudioZone.h"
#import "AudioService.h"
#import "AudioServiceMenu.h"
#import "iconview.h"
#import "MP3Menu.h"
#import "crect.h"
#import "hlbutton.h"
#import "MainView.h"
#import "hlcomm.h"
#import "hlm.h"
#import "RoundRegion.h"
#import "NSMSG.h"
#import "NSQuery.h"
#import "CustomPage.h"

@implementation CMP3AddressHeader

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Menu:(CMP3Menu*)pMenu Icon:(NSString*)psIcon ContentView:(CMP3ContentView*)pContentView

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [m_pLabel setText:[pMenu Title]];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        [m_pLabel setLineBreakMode:UILineBreakModeWordWrap];
        [m_pLabel setTextAlignment:UITextAlignmentLeft];
        [m_pLabel setNumberOfLines:3];


        [self addSubview:m_pLabel];
        [m_pLabel release];

        if (psIcon != NULL)
        {
            m_pIconView = [[CUserTabIconView alloc] initWithFrame:CGRectZero];
            [m_pIconView SetEmbeddedIcon:psIcon];
            [self addSubview:m_pIconView];
            [m_pIconView release];
        }

        m_pMenu = pMenu;
    }
    return self;
}
/*============================================================================*/

-(CMP3Menu*)Menu    { return m_pMenu;   }

/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    if (m_pIconView != NULL)
    {
        CGRect rIcon = [CRect BreakOffLeft:&rThis DX:rThis.size.height];
        [m_pIconView setFrame:rIcon];
    }

    [m_pLabel setFrame:rThis];
}
/*============================================================================*/

-(void)SetData:(int)iID Name:(NSString*)pName;

/*============================================================================*/
{
    [m_pMenu SetID:iID];
    [m_pLabel setText:pName];
}
@end

/*============================================================================*/

@implementation CMP3ContentView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Icon:(NSString*)pIcon

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_psIcon = pIcon;
        [m_psIcon retain];
        m_pZone = pZone;

        m_pAddressButtons = [[NSMutableArray alloc] init];
        _m_pAddressButtons = [[NSMutableArray alloc] init];

        m_pSeparator = [[CRoundRegionSeparator alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pSeparator];
        [m_pSeparator release];
        
        m_pBackButton = [[CHLButton alloc] initWithFrame:CGRectZero Text:NULL];
        [m_pBackButton SetIconEmbedded:@"LEFT.png"];
        [m_pBackButton IconFormat:HL_ICONCENTERMAX];
        [self addSubview:m_pBackButton];
        [m_pBackButton release];
        [m_pBackButton setAlpha:0];
        [m_pBackButton addTarget:self action:@selector(PopMenu) forControlEvents: UIControlEventTouchDown];
        
        [self setClipsToBounds:YES];
        // Initialization code
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_psIcon release];
    [m_pAddressButtons release];
    [_m_pAddressButtons release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDYNav = [CMainView DY_TOOLBAR];
    int iYSep = 0;
    iYSep = iDYNav-2;

    CGRect rSep = CGRectMake(0, iYSep, self.bounds.size.width, 3);
    [m_pSeparator setFrame:rSep];
    int iPCT = iYSep * 100 / self.bounds.size.height;
    [m_pSeparator SetPosition:iPCT];

    if ([m_pAddressButtons count] > 0)
    {
        CGRect rHeader = [self HeaderRect:[m_pAddressButtons count] == 1];
        CGRect rButton = [self ButtonRect];
        
        [m_pBackButton setFrame:rButton];

        int iN = [m_pAddressButtons count];
        CMP3AddressHeader *pHeader = (CMP3AddressHeader*)[m_pAddressButtons objectAtIndex:iN-1];
        [pHeader setFrame:rHeader];

        CMP3Menu *pMenu = [pHeader Menu];
        [pMenu setFrame:[self ContentRect]];
    }
}
/*============================================================================*/

-(void)PushView:(CMP3Menu*)pView

/*============================================================================*/
{
    [self PushView:pView Animated:YES];
}
/*============================================================================*/

-(void)PushView:(CMP3Menu*)pView Animated:(BOOL)bAnimated

/*============================================================================*/
{
    NSString *psIcon = m_psIcon;
    if ([m_pAddressButtons count] > 0)
        psIcon = NULL;

    int iDXContent = self.bounds.size.width;
    int iDXHeader = iDXContent/4;

    CMP3AddressHeader *pNewHeader = [[CMP3AddressHeader alloc] initWithFrame:CGRectZero Menu:pView Icon:psIcon ContentView:self];
    [pNewHeader setAlpha:0];

    BOOL bAtRoot = ([m_pAddressButtons count] <= 1);


    CGRect rNewHeaderEnd   = [self HeaderRect:FALSE];
    CGRect rNewHeaderStart = rNewHeaderEnd;
    rNewHeaderStart.origin.x += iDXHeader;

    CGRect rNewViewEnd = [self ContentRect];
    CGRect rNewViewStart = rNewViewEnd;
    rNewViewStart.origin.x += iDXContent;
    CGRect rOldViewEnd = rNewViewEnd;
    rOldViewEnd.origin.x -= iDXContent;

    [pNewHeader setFrame:rNewHeaderStart];
    [pView setFrame:rNewViewStart];

    CMP3AddressHeader *pLastHeader = NULL;
    if ([m_pAddressButtons count] > 0)
    {
        pLastHeader = (CMP3AddressHeader*)[m_pAddressButtons objectAtIndex:[m_pAddressButtons count]-1];
    }
    CMP3Menu *pLastView = [pLastHeader Menu];

    CGRect rOldHeaderStart = [self HeaderRect:bAtRoot];
    CGRect rOldHeaderEnd = rOldHeaderStart;
    rOldHeaderEnd.origin.x -=iDXHeader;

    if (bAtRoot)
    {
        CGRect rButton = [self ButtonRect];
        rButton.origin.x += iDXHeader;
        [m_pBackButton setFrame:rButton];
    }

    if (bAnimated)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
    }

    [pNewHeader setAlpha:1.0];
    [pLastHeader setAlpha:0.0];
    [pNewHeader setFrame:rNewHeaderEnd];
    [pLastHeader setFrame:rOldHeaderEnd];
    [pView setFrame:rNewViewEnd];
    [pLastView setFrame:rOldViewEnd];

    if (bAtRoot && [m_pAddressButtons count] > 0)
    {
        CGRect rButton = [self ButtonRect];
        [m_pBackButton setFrame:rButton];
        [m_pBackButton setAlpha:1.0];
    }
    
    [self addSubview:pNewHeader];
    [self addSubview:pView];
    [pNewHeader release];
    [m_pAddressButtons addObject:pNewHeader];

    if (bAnimated)
    {
        [UIView commitAnimations];
    }
}
/*============================================================================*/

-(void)PopMenu

/*============================================================================*/
{
    [self NavigationStop:NULL finished:NULL context:NULL];

    CGRect rFrame = [self ContentRect];

    if ([m_pAddressButtons count] <= 1)
        return;

    int iLast = [m_pAddressButtons count]-1;
    int iNew  = iLast-1;
    int iDXContent = self.frame.size.width;
    int iDXHeader = iDXContent / 4;

    if ([m_pAddressButtons count] == 1)
    {
        // Remove back button
        CGRect rBackButton = [self ButtonRect];
        rBackButton.origin.x -= iDXHeader;
        [m_pBackButton setFrame:rBackButton];
    }

    CMP3AddressHeader *pLast = (CMP3AddressHeader*)[m_pAddressButtons objectAtIndex:iLast];
    CMP3AddressHeader *pNew  = (CMP3AddressHeader*)[m_pAddressButtons objectAtIndex:iNew];

    CGRect rNewStart = rFrame;
    rNewStart.origin.x -= iDXContent;
    [[pNew Menu ] setFrame:rNewStart];

    [_m_pAddressButtons addObject:pLast];
    [m_pAddressButtons removeObjectAtIndex:iLast];

    CGRect rHeaderNew = [self HeaderRect:[m_pAddressButtons count] == 1];
    CGRect rHeaderNewStart = rHeaderNew;
    rHeaderNewStart.origin.x -= iDXHeader;

    //
    // Begin Animation Block
    //
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDidStopSelector:@selector(NavigationStop:finished:context:)];

    CGRect rLast = rFrame;
    rLast.origin.x += iDXContent;
    
    [[pNew Menu] setFrame:rFrame];
    [[pLast Menu] setFrame:rLast];


    CGRect rHeaderLast = [self HeaderRect:FALSE];
    rHeaderLast.origin.x += iDXHeader;
    [pLast setFrame:rHeaderLast];
    [pLast setAlpha:0];
    
    [pNew setFrame:rHeaderNew];
    [pNew setAlpha:1.0];

    if ([m_pAddressButtons count] == 1)
    {
        // Remove back button
        CGRect rBackButton = [self ButtonRect];
        rBackButton.origin.x += iDXHeader;
        [m_pBackButton setFrame:rBackButton];
        [m_pBackButton setAlpha:0.0];
    }

    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(NavigationStop:finished:context:)];
    [UIView commitAnimations];

}
/*============================================================================*/

-(void)NavigationStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    for (int i = 0; i < [_m_pAddressButtons count]; i++)
    {
        CMP3AddressHeader *pBtn = (CMP3AddressHeader*)[_m_pAddressButtons objectAtIndex:i];
        CMP3Menu *pMenu = [pBtn Menu];
        [pBtn removeFromSuperview];
        [pMenu removeFromSuperview];
    }
    [_m_pAddressButtons removeAllObjects];
}
/*============================================================================*/

-(CGRect)ContentRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDYNav = [CMainView DY_TOOLBAR];
    [CRect BreakOffTop:&rThis DY:iDYNav];
    return rThis;
}
/*============================================================================*/

-(CGRect)HeaderRect:(BOOL)bRoot

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDYNav = [CMainView DY_TOOLBAR];
    CGRect rHeader;
    rHeader = [CRect BreakOffTop:&rThis DY:iDYNav];

    if (!bRoot)
    {
        [CRect BreakOffLeft:&rHeader DX:[CMainView DX_SIDEBAR]+4];
    }

    return rHeader;
}
/*============================================================================*/

-(CGRect)ButtonRect

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    int iDYNav = [CMainView DY_TOOLBAR];
    CGRect rHeader;
    rHeader = [CRect BreakOffTop:&rThis DY:iDYNav];

    CGRect rButton = [CRect BreakOffLeft:&rHeader DX:rHeader.size.height];
    [CRect CenterDownToX:&rButton DX:48];
    [CRect CenterDownToY:&rButton DY:48];
    return rButton;
}
/*============================================================================*/

-(void)PushNewViewFrom:(int)iFrom To:(int)iTo Data:(NSString*)pData

/*============================================================================*/
{
}
@end


@implementation CArtistContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:@"ARTIST.png"];
    if (self)
    {
        CMP3Menu *pMenu = [[CMP3Menu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_LIBRARY_ARTISTS_ALL Sub1:0 Sub2:0 Title:@"All Artists" Service:NULL];
        [self PushView:pMenu Animated:NO];
        [pMenu release];
    }
    return self;
}
@end

@implementation CAlbumContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:@"ALBUM.png"];
    if (self)
    {
        CMP3Menu *pMenu = [[CMP3Menu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_LIBRARY_ALBUMS_ALL Sub1:0 Sub2:0 Title:@"All Albums" Service:NULL];
        [self PushView:pMenu Animated:NO];
        [pMenu release];
    }
    return self;
}
@end

@implementation CTrackContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:@"TRACK.png"];
    if (self)
    {
        CMP3Menu *pMenu = [[CMP3Menu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_LIBRARY_TRACKS_ALL Sub1:0 Sub2:0 Title:@"All Tracks" Service:NULL];
        [self PushView:pMenu Animated:NO];
        [pMenu release];
    }
    return self;
}
@end

@implementation CPlaylistContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:@"PLAYLIST.png"];
    if (self)
    {
        CMP3Menu *pMenu = [[CMP3Menu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_LIBRARY_PLAYLISTS Sub1:0 Sub2:0 Title:@"Playlists" Service:NULL];
        [self PushView:pMenu Animated:NO];
        [pMenu release];
    }
    return self;
}
@end

@implementation CGenreContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:@"GENRE.png"];
    if (self)
    {
        CMP3Menu *pMenu = [[CMP3Menu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_LIBRARY_ALBUM_GENRES Sub1:0 Sub2:0 Title:@"Playlists" Service:NULL];
        [self PushView:pMenu Animated:NO];
        [pMenu release];
    }
    return self;
}
@end


@implementation CAudioServiceContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Service:(CAudioService*)pService

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:[pService Icon]];
    if (self)
    {
        m_pService = pService;
//      CAudioServiceMenu *pMenu = [[CAudioServiceMenu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_SERVICE_ROOT Sub1:0 Sub2:0 Title:[pService Name] Service:pService];
//      [self PushView:pMenu Animated:NO];
//        [pMenu release];
        [CHLComm AddSocketSink:self];



        // 
        // For Rhapsody this can take some time, so put up an animation to show that we're busy
        //
        m_pWaitGraphic = [[UIActivityIndicatorView alloc] initWithFrame:CGRectZero];
        [self addSubview:m_pWaitGraphic];
        [m_pWaitGraphic release];

        [self PushNewViewFrom:0 To:0 Data:NULL];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pNavString release];
    [CHLComm RemoveSocketSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    [_m_pMenu removeFromSuperview];
    [_m_pHeader removeFromSuperview];
    _m_pMenu = m_pMenu;
    _m_pHeader = m_pHeader;


    int iDXShift1 = self.frame.size.width;
    int iDXShift2 = iDXShift1 / 4;



    if ([pMSG MessageID] != HLM_AUDIO_SERVICE_DOSERVICEOBJECTA)
        return;

    int iOpType = [pMSG GetInt];
    if (iOpType != 0)
        return;
        
    int iNLast = m_iNAddress;
    m_iNAddress = [pMSG GetInt];

    if (m_iNAddress < iNLast)
    {
        iDXShift1 = -iDXShift1;
        iDXShift2 = -iDXShift2;
    }

    for (int i = 0; i < m_iNAddress; i++)
    {

        int iID         = [pMSG GetInt];      // Address ID
        NSString *pName = [pMSG GetString];   // Address Name


        if (i == 0)
            m_iAddress0 = iID;
        if (i == (m_iNAddress-2))
            m_iAddressB = iID;

        if (i == (m_iNAddress-1))
        {
            if ([pName length] < 1)
            {
//                int itest = 1;
            }

            CGRect rHeader0 = [self HeaderRect:(i == 0)];
            CGRect rMenu0   = [self ContentRect];

            if (_m_pMenu != NULL)
            {
                rHeader0.origin.x += iDXShift2;
                rMenu0.origin.x += iDXShift1;
            }

            NSString *psIcon = NULL;
            if (i == 0)
                psIcon = [m_pService Icon];

            m_pMenu = [[CAudioServiceMenu alloc] initWithFrame:rMenu0 ContentView:self Zone:m_pZone Type:MENU_AUDIO_SERVICE_ROOT Sub1:m_iLastID Sub2:iID Title:pName Service:m_pService NavString:m_pNavString];
            m_pHeader = [[CMP3AddressHeader alloc] initWithFrame:rHeader0 Menu:m_pMenu Icon:psIcon ContentView:self];

            [m_pMenu LoadFrom:pMSG];
            [self addSubview:m_pHeader];
            [self addSubview:m_pMenu];

            if (_m_pMenu != NULL)
            {
                [m_pHeader setAlpha:0];
            }
        }
    }            

    if (_m_pMenu != NULL)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(AudioServiceAnimationDone:finished:context:)];

        CGRect r1 = _m_pMenu.frame;
        CGRect r2 = _m_pHeader.frame;
        r1.origin.x -= iDXShift1;
        r2.origin.x -= iDXShift2;
        [_m_pMenu setFrame:r1];
        [_m_pHeader setFrame:r2];
        [_m_pHeader setAlpha:0];

        [m_pHeader setFrame:[self HeaderRect:(m_iNAddress <= 1)]];
        [m_pMenu setFrame:[self ContentRect]];
        [m_pHeader setAlpha:1];
    }

    if (m_iNAddress > 1)
    {
        [m_pBackButton setAlpha:1.0];
    }
    else
    {
        [m_pBackButton setAlpha:0.0];
    }

    [m_pWaitGraphic stopAnimating];
    [m_pWaitGraphic setHidden:YES];

    if (_m_pMenu != NULL)
    {
        [UIView commitAnimations];
    }
}
/*============================================================================*/

-(void)PopMenu

/*============================================================================*/
{
    int iFrom = [m_pMenu SortSubType1];
    [self PushNewViewFrom:iFrom To:m_iAddressB Data:NULL];
}
/*============================================================================*/

-(void)PushNewViewFrom:(int)iFrom To:(int)iTo Data:(NSString*)pData

/*============================================================================*/
{
    m_iLastID = iFrom;
    [m_pNavString release];

    m_pNavString = pData;
    [m_pNavString retain];
    
    
    [m_pWaitGraphic setHidden:FALSE];
    [m_pWaitGraphic startAnimating];
    
    CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_SERVICE_DOSERVICEOBJECTQ];
    [pQ autorelease];
    [pQ PutInt:[m_pService ServiceID]];
    [pQ PutInt:iFrom];
    [pQ PutInt:iTo];
    [pQ PutInt:ITEM_OPERATION_GO];
    [pQ PutString:pData];
    [pQ PutInt:iTo];
    [pQ SendMessageWithMyID:self];
    
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDYNav = [CMainView DY_TOOLBAR];
    int iYSep = 0;
    iYSep = iDYNav-2;

    CGRect rSep = CGRectMake(0, iYSep, self.bounds.size.width, 3);
    [m_pSeparator setFrame:rSep];
    int iPCT = iYSep * 100 / self.bounds.size.height;
    [m_pSeparator SetPosition:iPCT];

    CGRect rHeader = [self HeaderRect:m_iNAddress <= 1];
    CGRect rButton = [self ButtonRect];
    
    [m_pBackButton setFrame:rButton];
    [m_pHeader setFrame:rHeader];

    [m_pMenu setFrame:[self ContentRect]];

    CGRect rThis = self.bounds;
    [CRect CenterDownToX:&rThis DX:32];
    [CRect CenterDownToY:&rThis DY:32];
    [m_pWaitGraphic setFrame:rThis];
}
/*============================================================================*/

-(void)AudioServiceAnimationDone:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    [_m_pMenu removeFromSuperview];
    [_m_pHeader removeFromSuperview];
    _m_pMenu = NULL;
    _m_pHeader = NULL;
}
@end

@implementation CMyLibraryContentView
/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame Zone:pZone Icon:@"TRACK.png"];
    if (self)
    {
        CMP3Menu *pMenu = [[CMP3Menu alloc] initWithFrame:CGRectZero ContentView:self Zone:m_pZone Type:MENU_AUDIO_ROOT Sub1:0 Sub2:0 Title:@"My Library" Service:NULL];
        [self PushView:pMenu Animated:NO];
        [pMenu release];
    }
    return self;
}
@end
