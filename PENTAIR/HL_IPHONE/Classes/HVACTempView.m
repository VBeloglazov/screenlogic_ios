//
//  HVACTempView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "HVACTempView.h"
#import "tstat.h"
#import "hlbutton.h"
#import "hlm.h"
#import "MainView.h"
#import "CustomPage.h"

@implementation CHVACTempView

#define CMD_TEMPUP                      1000
#define CMD_TEMPDN                      1001

#define DY_LABEL_IPAD                       40
#define DY_LABEL_IPOD                      25

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect TStat:(CTStat*)pTStat Mode:(int)iMode ReadOnly:(BOOL)bReadOnly

/*============================================================================*/
{
    self = [super initWithFrame:rect];
    if (self)
    {
        m_bReadOnly = bReadOnly;
        m_pTStat = pTStat;
        [m_pTStat AddSink:self];
        m_iMode = iMode;
        m_bFlashEnable = TRUE;
        self.opaque = NO;

        m_pTempText = [[UILabel alloc] initWithFrame:CGRectZero];
        m_pTempText.backgroundColor = [UIColor clearColor];
        m_pTempText.opaque = NO;
        m_pTempText.textColor = [UIColor whiteColor];

        [m_pTempText setAdjustsFontSizeToFitWidth:YES];
        [m_pTempText setMinimumFontSize:8];
        m_pTempText.textAlignment = UITextAlignmentCenter;
        [m_pTempText setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [m_pTempText setText:@"-"];
        [self addSubview:m_pTempText];

        if (!m_bReadOnly)
        {
            m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            m_pLabel.backgroundColor = [UIColor clearColor];
            m_pLabel.opaque = NO;
            m_pLabel.textColor = [UIColor whiteColor];
            m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
            m_pLabel.numberOfLines = 2;
            m_pLabel.textAlignment = UITextAlignmentCenter;
        }


        BOOL bDisplayOnly = FALSE;
        switch (m_iMode)
        {
        case TEMP_ROOM:
        case TEMP_HUMIDITY:
            bDisplayOnly = TRUE;
            break;
        }

        if (!bDisplayOnly && !m_bReadOnly)
        {
            UIColor *pRGBTint = NULL;
            switch (m_iMode)
            {
            case TEMP_HEAT: pRGBTint = [UIColor colorWithRed:.1 green:0.0 blue:0.0 alpha:0.0];  break;
            case TEMP_COOL: pRGBTint = [UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.0]; break;
            case TEMP_HUMSPLO: pRGBTint = [UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.0]; break;
            case TEMP_HUMSPHI: pRGBTint = [UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.0]; break;
            }

            m_pUp = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYUP Text:NULL TextSize:20 TextColor:NULL Color:pRGBTint Icon:NULL];
            m_pDn = [[CHLButton alloc] initWithFrame:CGRectZero IconFormat:HL_ICONCENTER Style:ICON_GRAYDN Text:NULL TextSize:20 TextColor:NULL Color:pRGBTint Icon:NULL];
            [m_pUp SetCommandID:CMD_TEMPUP Responder:self];
            [m_pDn SetCommandID:CMD_TEMPDN Responder:self];
            [m_pUp SetRepeatDwell:300 Rate:4];
            [m_pDn SetRepeatDwell:300 Rate:4];
        }

        [self addSubview:m_pUp];
        [self addSubview:m_pDn];

        switch (m_iMode)
        {
        case TEMP_ROOM:     [m_pLabel setText:@"Room Temp"];    break;
        case TEMP_HUMIDITY: [m_pLabel setText:@"Humidity"];     break;
        case TEMP_HUMSPLO:  [m_pLabel setText:@"Min"];     break;
        case TEMP_HUMSPHI:  [m_pLabel setText:@"Max"];  break;
        case TEMP_COOL: { [m_pLabel setText:@"Cool"];   m_pTempText.textColor = [UIColor colorWithRed:.5 green:.5 blue:1.0 alpha:1.0];    } break;
        case TEMP_HEAT: { [m_pLabel setText:@"Heat"];   m_pTempText.textColor = [UIColor colorWithRed:1.0 green:.5 blue:.5 alpha:1.0];    } break;
        }

        [self addSubview:m_pLabel];


        [self Notify:0 Data:0];
        
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pUp release];
    [m_pDn release];
    [m_pTStat RemoveSink:self];
    [m_pTempText release];
    [m_pLabel release];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iVal = 0;
    switch (m_iMode)
    {
    case TEMP_HUMIDITY:     { iVal = [m_pTStat Humidity];   } break;
    case TEMP_HUMSPLO:      { iVal = [m_pTStat HumSPLo];    } break;
    case TEMP_HUMSPHI:      { iVal = [m_pTStat HumSPHi];    } break;
    case TEMP_ROOM: { iVal = [m_pTStat RoomTemp];   }   break;
    case TEMP_COOL: { iVal = [m_pTStat CoolSP];     }   break;
    case TEMP_HEAT: { iVal = [m_pTStat HeatSP];     }   break;
    }

    if (m_iMode != TEMP_ROOM)
    {
        if ([m_pUp HasCapture] || [m_pDn HasCapture])
            iVal = m_iLocalTemp;
    }

    NSString *pText = NULL;
    switch (m_iMode)
    {
    case TEMP_HUMIDITY:
    case TEMP_HUMSPLO:
    case TEMP_HUMSPHI:
        pText = [NSString stringWithFormat:@"%d", iVal];
        break;

    default:
        pText = [NSString stringWithFormat:@"%d°", iVal];
        break;
    }

    if (iVal == 0)
        pText = @"-";

    m_pLabel.hidden = FALSE;

    if ([m_pTStat HVACMode] == HVAC_STATE_NOTOK)
    {
        pText = @"Thermostat Offline";
        m_pLabel.hidden = TRUE;
    }

    if ([self ShouldFlash] && !m_bFlashing)
    {
        m_bFlashOn = TRUE;
        m_bFlashing = TRUE;

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
        [m_pTempText setAlpha:0.5];
        [UIView commitAnimations];
    }

    [m_pTempText setText:pText];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (![m_pUp HasCapture] && ![m_pDn HasCapture])
    {
        switch (m_iMode)
        {
        case TEMP_HEAT:     m_iLocalTemp = [m_pTStat HeatSP]; break;
        case TEMP_COOL:     m_iLocalTemp = [m_pTStat CoolSP]; break;
        case TEMP_HUMSPLO:  m_iLocalTemp = [m_pTStat HumSPLo]; break;
        case TEMP_HUMSPHI:  m_iLocalTemp = [m_pTStat HumSPHi]; break;
        }
    }

    int iDir = 1;
    switch (iCommandID)
    {
    case CMD_TEMPUP:
        break;
    case CMD_TEMPDN:
        iDir = -1;
        break;
    default:
        return;
    }

    int iMaxTemp = 50;
    int iMinTemp = 50;

    switch (m_iMode)
    {
    case TEMP_HEAT:
        {
            iMaxTemp = [m_pTStat MaxHeatSP];
            iMinTemp = [m_pTStat MinHeatSP];
        }
        break;
    case TEMP_COOL:
        {
            iMaxTemp = [m_pTStat MaxCoolSP];
            iMinTemp = [m_pTStat MinCoolSP];
        }
        break;
    case TEMP_HUMSPLO:
        {
            iMaxTemp = 99;
            iMinTemp = 0;
        }
        break;
    case TEMP_HUMSPHI:
        {
            iMaxTemp = 100;
            iMinTemp = 1;
        }
        break;
    }

    if (m_iLocalTemp >= iMaxTemp && iDir > 0)
        return;
    if (m_iLocalTemp <= iMinTemp && iDir < 0)
        return;


    m_iLocalTemp += iDir;

    switch (m_iMode)
    {
    case TEMP_HEAT:        [m_pTStat SetHeatSP:m_iLocalTemp];   break;
    case TEMP_COOL:        [m_pTStat SetCoolSP:m_iLocalTemp];   break;
    case TEMP_HUMSPLO:     [m_pTStat HumSPLo:m_iLocalTemp];     break;
    case TEMP_HUMSPHI:     [m_pTStat HumSPHi:m_iLocalTemp];     break;
    }


}
/*============================================================================*/

-(BOOL)ShouldFlash

/*============================================================================*/
{
    if (self.superview == NULL)
        return FALSE;
    if (!m_bFlashEnable)
        return FALSE;

    switch (m_iMode)
    {
    case TEMP_COOL:
        return [m_pTStat CoolActive];
    
    case TEMP_HEAT:
        return [m_pTStat HeatActive];
    }

    return FALSE;
}
/*============================================================================*/

-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context

/*============================================================================*/
{
    if (!m_bFlashOn && ![self ShouldFlash])
    {
        m_bFlashing = FALSE;
        return;
    }

    m_bFlashOn = !m_bFlashOn;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(AnimationDidStop:finished:context:)];
    if (m_bFlashOn)
        [m_pTempText setAlpha:0.5];
    else
        [m_pTempText setAlpha:1.0];
    [UIView commitAnimations];
}
/*============================================================================*/

-(void)SetVisible:(BOOL)bVisible

/*============================================================================*/
{
    m_bFlashEnable = bVisible;
    if (bVisible)
        [self Notify:0 Data:0];
}
/*============================================================================*/

-(void)NotifySubViewChanged

/*============================================================================*/
{
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    int iDXThis = self.bounds.size.width;
    int iDYThis = self.bounds.size.height;

    if (iDXThis == m_iDXLast && iDYThis == m_iDYLast)
    {
        return;
    }

    m_iDXLast = iDXThis;
    m_iDYLast = iDYThis;

    int iDYLabel = DY_LABEL_IPOD;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDYLabel = DY_LABEL_IPAD;

    CGRect rLabel   = CGRectMake(0, 0, iDXThis/2, iDYLabel);

    int iDXBtn = MIN(200, iDXThis /2);
    

    int iBtnInset = MIN(5, iDXThis / 20);
    CGRect rUp      = CGRectMake(iDXThis-iDXBtn, iBtnInset,  iDXBtn, iDYThis/2-2*iBtnInset);
    CGRect rDn      = CGRectMake(iDXThis-iDXBtn, iDYThis/2+iBtnInset,  iDXBtn, iDYThis/2-2*iBtnInset);
    CGRect rTemp    = CGRectMake(0, 0, iDXThis/2, iDYThis);

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && !m_bReadOnly)
        rTemp.origin.y = iDYLabel;

    switch (m_iMode)
    {
    case TEMP_ROOM:
    case TEMP_HUMIDITY:
        {
            rLabel.size.width = iDXThis*9/10;
            rTemp.size.width = iDXThis;
        }
        break;
    }

    [CCustomPage InitStandardLabelSize:m_pTempText Size:rTemp.size.height * 90 / 100];
    [CCustomPage InitStandardLabelSize:m_pLabel Size:iDYLabel * 90 / 100];

    [m_pTempText setFrame:rTemp];
    [m_pLabel setFrame:rLabel];
    [m_pUp setFrame:rUp];
    [m_pDn setFrame:rDn];
}
@end
