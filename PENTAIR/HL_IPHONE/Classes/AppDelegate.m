//
//  XPAD_VIEWAppDelegate.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import "AppDelegate.h"
#import "IPOD_ViewController.h"
#import "hlcomm.h"
#import "SystemMode.h"
#import "shader.h"
#import "weather.h"
#import "hlm.h"
#import "hltoolbar.h"

#ifdef PENTAIR
    #import "NSPoolConfig.h"
#endif

@implementation CButtonImage
/*============================================================================*/

-(id)initWithDX:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT Rad:(int)iRad RGBT:(unsigned int)rgbT RGBB:(unsigned int)rgbB;

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_iDX       = iDX;
        m_iDY       = iDY;
        m_iSHIN     = iSHIN;
        m_iSHOUT    = iSHOUT;
        m_iRad      = iRad;
        m_rgbT      = rgbT;
        m_rgbB      = rgbB;

        m_pImageRef = [CShader CreateButtonImageDX:iDX DY:iDY Rad:iRad SHIN:iSHIN SHOUT:iSHOUT ColorT:rgbT ColorB:rgbB];

        int iTest = CFGetRetainCount(m_pImageRef);
        printf("Retain Count %d\n", iTest);
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_pImageRef != NULL)
    {
        CGImageRelease(m_pImageRef);
    }
    [super dealloc];
}
/*============================================================================*/

-(BOOL)IsEqualTo:(int)iDX DY:(int)iDY SHIN:(int)iSHIN SHOUT:(int)iSHOUT Rad:(int)iRad RGBT:(unsigned int)rgbT RGBB:(unsigned int)rgbB

/*============================================================================*/
{
    if (iDX != m_iDX) return FALSE;
    if (iDY != m_iDY) return FALSE;
    if (iSHIN != m_iSHIN) return FALSE;
    if (iSHOUT != m_iSHOUT) return FALSE;
    if (iRad != m_iRad) return FALSE;
    if (rgbT != m_rgbT) return FALSE;
    if (rgbB != m_rgbB) return FALSE;
    
    return TRUE;
}
/*============================================================================*/

-(BOOL)IsFree

/*============================================================================*/
{
    if (m_pImageRef == NULL)
        return TRUE;
    if (CFGetRetainCount(m_pImageRef) == 1)
        return TRUE;
    return FALSE;
}
/*============================================================================*/

-(CGImageRef)GetImageRef

/*============================================================================*/
{
    return m_pImageRef;
}
@end


@implementation CMainTabImage
/*============================================================================*/

-(id)initWithDX:(int)iDX DY:(int)iDY Selected:(BOOL)bSelected

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_iDX       = iDX;
        m_iDY       = iDY;
        m_bSelected = bSelected;

        m_pImageRef = [CShader CreateMainTabImageDX:iDX DY:iDY Selected:bSelected];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_pImageRef != NULL)
    {
        CGImageRelease(m_pImageRef);
    }
    [super dealloc];
}
/*============================================================================*/

-(BOOL)IsEqualTo:(int)iDX DY:(int)iDY Selected:(BOOL)bSelected

/*============================================================================*/
{
    if (iDX != m_iDX) return FALSE;
    if (iDY != m_iDY) return FALSE;
    if (bSelected != m_bSelected) return FALSE;
    
    return TRUE;
}
/*============================================================================*/

-(BOOL)IsFree

/*============================================================================*/
{
    if (m_pImageRef == NULL)
        return TRUE;
    if (CFGetRetainCount(m_pImageRef) == 1)
        return TRUE;
    return FALSE;
}
/*============================================================================*/

-(CGImageRef)GetImageRef

/*============================================================================*/
{
    return m_pImageRef;
}

@end

@implementation CToolBarImage
/*============================================================================*/

-(id)initWithDY:(int)iDY Orientation:(int)iOrientation State:(int)iState FrameStyle:(int)iFrameStyle

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_iDY           = iDY;
        m_iOrientation  = iOrientation;
        m_iState        = iState;
        m_iFrameStyle   = iFrameStyle;

        unsigned int rgbEdge = [CHLComm GetRGBWin32:RGB_BAR_EDGE];
        unsigned int rgbT = 0;
        unsigned int rgbB = 0;
        switch (iState)
        {
        case ELEMENT_STD:
            {
                switch (m_iOrientation)
                {
                case TOOLBAR_BTM:
                case TOOLBAR_BTM_LEFT:
                case TOOLBAR_BTM_RIGHT:
                    {
                        rgbT = [CHLComm GetRGBWin32:RGB_BTMBAR_TOP];
                        rgbB = [CHLComm GetRGBWin32:RGB_BTMBAR_BTM];
                    }
                    break;

                case TOOLBAR_TOP:
                    {
                        rgbT = [CHLComm GetRGBWin32:RGB_TOPBAR_TOP];
                        rgbB = [CHLComm GetRGBWin32:RGB_TOPBAR_BTM];
                    }
                    break;
                }
            }
            break;
        case ELEMENT_FLASH:
            {
                rgbT = [CShader MakeRGB:[CHLComm GetRGB:RGB_SELECT]];
                rgbB = rgbT;
            }
            break;
        case ELEMENT_CHECK:
            {
                switch (m_iOrientation)
                {
                case TOOLBAR_BTM:
                case TOOLBAR_BTM_LEFT:
                case TOOLBAR_BTM_RIGHT:
                    {
                        rgbT = [CHLComm GetRGBWin32:RGB_BTMBAR_TOP];
                        rgbB = [CHLComm GetRGBWin32:RGB_BTMBAR_BTM];
                    }
                    break;

                case TOOLBAR_TOP:
                    {
                        rgbT = [CHLComm GetRGBWin32:RGB_TOPBAR_TOP];
                        rgbB = [CHLComm GetRGBWin32:RGB_TOPBAR_BTM];
                    }
                    break;
                }

                UIColor *prgbCheck = [CHLComm GetRGB:RGB_SELECT_SUBTLE];
                unsigned int rgbSel = [CShader MakeRGB:prgbCheck];
                int iOA1 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_TOP] * 100 / 255;
                int iOA2 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_BTM] * 100 / 255;
                rgbT = [CShader BlendPCT:iOA1 RGB1:rgbSel RGB2:rgbT];
                rgbB = [CShader BlendPCT:iOA2 RGB1:rgbSel RGB2:rgbB];
            }
            break;
        }

        switch (iFrameStyle)
        {
        case TOOLBAR_FRAME_FULL:
            break;
        case TOOLBAR_FRAME_TOPHALF:
            {
                rgbB = [CShader BlendPCT:50 RGB1:rgbT RGB2:rgbB];
            }
            break;
        case TOOLBAR_FRAME_BOTTOMHALF:
            {
                rgbT = [CShader BlendPCT:50 RGB1:rgbT RGB2:rgbB];
            }
            break;
        }

        m_pImageRef = [CShader CreateToolBarButtonImage:50 DY:iDY ColorT:rgbT ColorB:rgbB Edge:rgbEdge Orientation:m_iOrientation];
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    if (m_pImageRef != NULL)
    {
        CGImageRelease(m_pImageRef);
    }
    [super dealloc];
}
/*============================================================================*/

-(BOOL)IsEqualToDY:(int)iDY Orientation:(int)iOrientation State:(int)iState FrameStyle:(int)iFrameStyle

/*============================================================================*/
{
    if (iDY != m_iDY) return FALSE;
    if (iOrientation != m_iOrientation) return FALSE;
    if (iState != m_iState) return FALSE;
    if (iFrameStyle != m_iFrameStyle) return FALSE;
    
    return TRUE;
}
/*============================================================================*/

-(BOOL)IsFree

/*============================================================================*/
{
    if (m_pImageRef == NULL)
        return TRUE;
    if (CFGetRetainCount(m_pImageRef) == 1)
        return TRUE;
    return FALSE;
}
/*============================================================================*/

-(CGImageRef)GetImageRef

/*============================================================================*/
{
    return m_pImageRef;
}
@end


@implementation CAppDelegate

@synthesize window;
@synthesize navigationController;



    static int                      g_iBackgroundStyle = 0;



/*============================================================================*/

- (void)applicationDidFinishLaunching:(UIApplication *)application

/*============================================================================*/

 {
    //this line fixes iOS8 issue with rotating if initial orientation was LANDSCAPE
    self.window.frame = [[UIScreen mainScreen] bounds];

    m_pComm = [[CHLComm alloc] init];
    
	// Override point for customization after app launch	
    [CShader Init];
    [m_pComm InitComm];
    [CHLComm SuspendIO:TRUE];
    m_pButtons = [[NSMutableArray alloc] init];
    m_pMainTabs = [[NSMutableArray alloc] init];
    m_pToolBars = [[NSMutableArray alloc] init];

    if (self.window == NULL)
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    else
        [window addSubview:[navigationController view]];
    [window setRootViewController:navigationController];
	[window makeKeyAndVisible];
    
    //[application setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
   
}
/*============================================================================*/

-(void)applicationWillTerminate:(UIApplication *)application

/*============================================================================*/
{
    [m_pComm ShutDownComm];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
#ifndef PENTAIR
    [m_pButtonPlayer release];
#endif
    [m_pForecast release];
    [m_pModeList release];
    [navigationController release];
	[window release];
    [m_pComm release];
    [CShader Release];
    [m_pButtons release];
    [m_pMainTabs release];
    [m_pToolBars release];

#ifdef PENTAIR
    [m_pPoolConfig release];
#endif

	[super dealloc];
}
/*============================================================================*/

-(CSystemModeList*)SysModeList

/*============================================================================*/
{
    return m_pModeList;
}
/*============================================================================*/

-(CSystemModeList*)InitSysModeList

/*============================================================================*/
{
    [m_pModeList release];
    m_pModeList = [[CSystemModeList alloc] init];
    return m_pModeList;
}
/*============================================================================*/

+(int)BackgroundStyle

/*============================================================================*/
{
    return g_iBackgroundStyle;
}
/*============================================================================*/

+(void)BackgroundStyle:(int)iNew

/*============================================================================*/
{
    g_iBackgroundStyle = iNew;
}
/*============================================================================*/

-(void)InitSounds

/*============================================================================*/
{
#ifndef PENTAIR
    [m_pButtonPlayer release];
    m_pButtonPlayer = NULL;
#endif
}
#ifdef PENTAIR
/*============================================================================*/

-(void)InitPoolConfig;

/*============================================================================*/
{
    [m_pPoolConfig EndPoll];
    [m_pPoolConfig release];
    m_pPoolConfig = NULL;
    m_pPoolConfig = [[CNSPoolConfig alloc] init];
}
/*============================================================================*/

-(void)RestartPoolConfig    //DL - this will not deallocate the instance

/*============================================================================*/
{
    [m_pPoolConfig EndPoll];
    [m_pPoolConfig init];
    //DL - this will re-init sinks
    [m_pPoolConfig ConnectionChanged:FALSE];
    
    m_pForecast  = NULL;//ConnectionChanged:TRUE];
}
/*============================================================================*/

-(CNSPoolConfig*)PoolConfig

/*============================================================================*/
{
    return m_pPoolConfig;
}
#endif
/*============================================================================*/

-(CGImageRef)GetButtonImage:(int)iDX DY:(int)iDY Rad:(int)iRad SHIN:(int)iSHIN SHOUT:(int)iSHOUT ColorT:(unsigned int)rgbT ColorB:(unsigned int)rgbB

/*============================================================================*/
{
    [self CollectGarbage];

    if (iDX <= 0)
        return NULL;
    if (iDY <= 0)
        return NULL;

    int iN = [m_pButtons count];
    for (int i = 0; i < iN; i++)
    {
        CButtonImage *pBI = (CButtonImage*)[m_pButtons objectAtIndex:i];
        if ([pBI IsEqualTo:iDX DY:iDY SHIN:iSHIN SHOUT:iSHOUT Rad:iRad RGBT:rgbT RGBB:rgbB])
        {
            return [pBI GetImageRef];
        }
    }

    CButtonImage *pBI = [[CButtonImage alloc] initWithDX:iDX DY:iDY SHIN:iSHIN SHOUT:iSHOUT Rad:iRad RGBT:rgbT RGBB:rgbB];
    [m_pButtons addObject:pBI];
    [pBI release];
    return [pBI GetImageRef];
}
/*============================================================================*/

-(CGImageRef)GetMainTabImage:(int)iDX DY:(int)iDY Selected:(BOOL)bSelected

/*============================================================================*/
{
    [self CollectGarbage];

    if (iDX <= 0)
        return NULL;
    if (iDY <= 0)
        return NULL;

    int iN = [m_pMainTabs count];
    for (int i = 0; i < iN; i++)
    {
        CMainTabImage *pBI = (CMainTabImage*)[m_pMainTabs objectAtIndex:i];
        if ([pBI IsEqualTo:iDX DY:iDY Selected:bSelected])
        {
            return [pBI GetImageRef];
        }
    }

    CMainTabImage *pBI = [[CMainTabImage alloc] initWithDX:iDX DY:iDY Selected:bSelected];
    [m_pMainTabs addObject:pBI];
    [pBI release];
    return [pBI GetImageRef];
}
/*============================================================================*/

-(CGImageRef)GetToolBarImage:(int)iDY Orientation:(int)iOrientation State:(int)iState FrameStyle:(int)iFrameStyle

/*============================================================================*/
{
    [self CollectGarbage];

    if (iDY <= 0)
        return NULL;

    int iN = [m_pToolBars count];
    for (int i = 0; i < iN; i++)
    {
        CToolBarImage *pBI = (CToolBarImage*)[m_pToolBars objectAtIndex:i];
        if ([pBI IsEqualToDY:iDY Orientation:iOrientation State:iState FrameStyle:iFrameStyle])
        {
            return [pBI GetImageRef];
        }
    }

    CToolBarImage *pBI = [[CToolBarImage alloc] initWithDY:iDY Orientation:iOrientation State:(int)iState FrameStyle:iFrameStyle];
    [m_pToolBars addObject:pBI];
    [pBI release];
    return [pBI GetImageRef];
}
/*============================================================================*/

-(void)CollectGarbage

/*============================================================================*/
{
    int i = 0;
    while (i < [m_pButtons count])
    {
        CButtonImage *pBI = (CButtonImage*)[m_pButtons objectAtIndex:i];
        if ([pBI IsFree])
        { 
            [m_pButtons removeObjectAtIndex:i];
        }
        else
        {
            i++;
        }
    }

    i = 0;
    while (i < [m_pMainTabs count])
    {
        CMainTabImage *pBI = (CMainTabImage*)[m_pMainTabs objectAtIndex:i];
        if ([pBI IsFree])
        { 
            [m_pMainTabs removeObjectAtIndex:i];
        }
        else
        {
            i++;
        }
    }
    i = 0;
    while (i < [m_pToolBars count])
    {
        CToolBarImage *pBI = (CToolBarImage*)[m_pToolBars objectAtIndex:i];
        if ([pBI IsFree])
        { 
            [m_pToolBars removeObjectAtIndex:i];
        }
        else
        {
            i++;
        }
    }
}
/*============================================================================*/

-(CForecast*)Forecast

/*============================================================================*/
{
    //DL when reconnect it is not NULL, how many of them an where
    if (m_pForecast == NULL)
    {
        m_pForecast = [[CForecast alloc] init];
    }

    return m_pForecast;
}
/*============================================================================*/

-(void)PlaySoundButtonPress

/*============================================================================*/
{
#ifndef PENTAIR
    if (m_pButtonPlayer == NULL)
        m_pButtonPlayer = [CHLComm CreateButtonSoundPlayer];
    [m_pButtonPlayer play];
#endif
}
@end
