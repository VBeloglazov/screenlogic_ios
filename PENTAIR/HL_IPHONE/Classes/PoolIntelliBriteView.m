//
//  PoolIntelliBriteView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/9/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolIntelliBriteView.h"
#import "NSPoolConfig.h"
#import "hlbutton.h"
#import "crect.h"
#import "MainView.h"
#import "NSQuery.h"
#import "hlcomm.h"
#import "NSMSG.h"
#import "CustomPage.h"

#define DY_LABEL                    30

@implementation CPoolIntelliBriteView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pConfig = pConfig;
        [m_pConfig AddSink:self];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            m_pColorButtons[0] = [self AddButton:@"All Off"         CommandID:POOLCMD_ALLOFF];
            m_pColorButtons[1] = [self AddButton:@"All On"          CommandID:POOLCMD_ALLON];
            m_pColorButtons[2] = [self AddButton:@"Set"             CommandID:POOLCMD_SET];
            m_pColorButtons[3] = [self AddButton:@"Sync"            CommandID:POOLCMD_SYNC];
            m_pColorButtons[4] = [self AddButton:@"Swim"            CommandID:POOLCMD_SWIM];
        }

        m_pIBriteButtons[0] = [self AddButton:@"Party"          CommandID:POOLCMD_PARTY];
        m_pIBriteButtons[1] = [self AddButton:@"Romance"        CommandID:POOLCMD_ROMANCE];
        m_pIBriteButtons[2] = [self AddButton:@"Caribbean"      CommandID:POOLCMD_CARIBBEAN];
        m_pIBriteButtons[3] = [self AddButton:@"American"       CommandID:POOLCMD_AMERICAN];
        m_pIBriteButtons[4] = [self AddButton:@"Sunset"         CommandID:POOLCMD_SUNSET];
        m_pIBriteButtons[5] = [self AddButton:@"Royal"          CommandID:POOLCMD_ROYALTY];
        m_pIBriteButtons[6] = [self AddButton:@""               CommandID:POOLCMD_BLUE      Color:RGB(50,50,255)];
        m_pIBriteButtons[7] = [self AddButton:@""               CommandID:POOLCMD_GREEN     Color:RGB(50,255,50)];
        m_pIBriteButtons[8] = [self AddButton:@""               CommandID:POOLCMD_RED       Color:RGB(255,50,50)];
        m_pIBriteButtons[9] = [self AddButton:@""               CommandID:POOLCMD_WHITE     Color:RGB(255,255,255)];
        m_pIBriteButtons[10] = [self AddButton:@""              CommandID:POOLCMD_MAGENTA   Color:RGB(255,50,255)];
        m_pIBriteButtons[11] = [self AddButton:@"Save"          CommandID:POOLCMD_SAVE];
        m_pIBriteButtons[12] = [self AddButton:@"Recall"        CommandID:POOLCMD_RECALL];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
        {
            m_pLabel1 = [self AddLabel:@"COLOR LIGHTS"];
            m_pLabel2 = [self AddLabel:@"INTELLIBRITE"];
        }

        [CHLComm AddSocketSink:self];   
        
        CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_ADDCLIENTQ];
        [p1 PutInt:0];
        [p1 PutInt:[CHLComm SenderID:self]];
        [p1 SendMessage];
        [p1 release];

    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
     CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_REMOVECLIENTQ];
     [p1 PutInt:0];
     [p1 PutInt:[CHLComm SenderID:self]];
     [p1 SendMessage];
     [p1 release];

    [CHLComm RemoveSocketSink:self];
    [m_pConfig RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID

/*============================================================================*/
{
    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:pText];
    [self addSubview:pBtn];
    [pBtn release];
    [pBtn SetCommandID:iID Responder:self];
    return pBtn;
}
/*============================================================================*/

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID Color:(COLORREF)rgb

/*============================================================================*/
{
    int iR = ((rgb >> 16) & 0xFF);
    int iG = ((rgb >> 8) & 0xFF);
    int iB = (rgb & 0xFF);

    float fR = (float)iR / 255.0f;
    float fG = (float)iG / 255.0f;
    float fB = (float)iB / 255.0f;

    CHLButton *pBtn = [[CHLButton alloc] initWithFrame:CGRectZero Text:pText];
    UIColor *pColor = [UIColor colorWithRed:fR green:fG blue:fB alpha:1.0];
    [pBtn SetColor:pColor];
    [pBtn SetFlashingColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];

    [pBtn SetTextColor:[UIColor blackColor]];
    
    [self addSubview:pBtn];
    [pBtn release];
    [pBtn SetCommandID:iID Responder:self];
    return pBtn;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iDYAvail = rBounds.size.height - 2 * DY_LABEL;
    int iDYColors = iDYAvail * 2 / 6 + DY_LABEL;

    CGRect rColors = CGRectZero;
    CGRect rLabel1 = CGRectZero;
    CGRect rLabel2 = CGRectZero;
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        rColors = [CRect BreakOffTop:&rBounds DY:iDYColors];
        rLabel1 = [CRect BreakOffTop:&rColors DY:DY_LABEL];
        rLabel2 = [CRect BreakOffTop:&rBounds DY:DY_LABEL];
    }

    [m_pLabel1 setFrame:rLabel1];
    [m_pLabel2 setFrame:rLabel2];
    
    int iCol = 0;
    int iRow = 0;

    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        for (int i = 0; i < 5; i++)
        {
            CGRect rRow = [CRect CreateYSection:rColors Index:iRow Of:2];
            CGRect rBtn = [CRect CreateXSection:rRow Index:iCol Of:3];
            [CRect Inset:&rBtn DX:2 DY:2];
            [m_pColorButtons[i] setFrame:rBtn];
            iCol++;
            if (iCol >= 3)
            {
                iRow++;
                iCol = 0;
            }
        }
    }

    int iNIBriteRows = 4;
    CGRect rSaveRecall = CGRectZero;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        iNIBriteRows = 3;
        rSaveRecall = [CRect BreakOffRight:&rBounds DX:rBounds.size.width / 4];
    }


    iRow = 0;
    iCol = 0;
    for (int i = 0; i < 6; i++)
    {
        CGRect rRow = [CRect CreateYSection:rBounds Index:iRow Of:iNIBriteRows];
        CGRect rBtn = [CRect CreateXSection:rRow Index:iCol Of:3];
        [CRect Inset:&rBtn DX:2 DY:2];
        [m_pIBriteButtons[i] setFrame:rBtn];
        iCol++;
        if (iCol >= 3)
        {
            iRow++;
            iCol = 0;
        }
    }

    CGRect rIBriteColors = [CRect CreateYSection:rBounds Index:2 Of:iNIBriteRows];
    for (int i = 0; i < 5; i++)
    {
        CGRect rBtn = [CRect CreateXSection:rIBriteColors Index:i Of:5];
        [CRect Inset:&rBtn DX:2 DY:2];
        [m_pIBriteButtons[i+6] setFrame:rBtn];
    }


    CGRect rSave = CGRectZero;
    CGRect rRecall = CGRectZero;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [CRect CenterDownToY:&rSaveRecall DY:rSaveRecall.size.height * 2/3];
        rSave = [CRect CreateYSection:rSaveRecall Index:0 Of:2];
        rRecall = [CRect CreateYSection:rSaveRecall Index:1 Of:2];
    }
    else
    {
        rSaveRecall = [CRect CreateYSection:rBounds Index:3 Of:4];
        rSave    = [CRect CreateXSection:rSaveRecall Index:0 Of:3];
        rRecall  = [CRect CreateXSection:rSaveRecall Index:1 Of:3];
    }

    [CRect Inset:&rSave DX:2 DY:2];
    [CRect Inset:&rRecall DX:2 DY:2];

    [m_pIBriteButtons[11] setFrame:rSave];
    [m_pIBriteButtons[12] setFrame:rRecall];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_COLORLIGHTSCMDQ];
    [p1 PutInt:0];
    [p1 PutInt:iCommandID];
    [p1 SendMessage];
    [p1 release];
}
/*============================================================================*/

-(void)ConnectionChanged:(BOOL)bNowConnected

/*============================================================================*/
{
    if (bNowConnected)
    {
         CNSQuery *p1 = [[CNSQuery alloc] initWithQ:HLM_POOL_ADDCLIENTQ];
         [p1 PutInt:0];
         [p1 PutInt:[CHLComm SenderID:self]];
         [p1 SendMessage];
         [p1 release];
    }
}
/*============================================================================*/

-(void)SinkMessage:(CNSMSG*)pMSG

/*============================================================================*/
{
    if ([pMSG MessageID] != HLM_POOL_COLORUPDATE)
        return;
    int iMode = [pMSG GetInt];
    int iNow  = [pMSG GetInt];
    int iTotal = [pMSG GetInt];
    
    if (iNow <= 0 || iNow >= iTotal)
        iMode = -1;
    for (int i = 0; i < 13; i++)
    {
        if ([m_pIBriteButtons[i] CommandID] == iMode)
            [m_pIBriteButtons[i] SetFlashing:TRUE];
        else
            [m_pIBriteButtons[i] SetFlashing:FALSE];
    }
}
/*============================================================================*/

-(UILabel*)AddLabel:(NSString*)pText;

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [CCustomPage InitStandardLabel:pLabel Size:16];
    [pLabel setText:pText];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
@end
