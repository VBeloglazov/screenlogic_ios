//
//  SourceIconView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/30/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SourceIconView.h"
#import "ImageServer.h"
#import "icon.h"
#import "MainView.h"
#import "crect.h"
#import "CustomPage.h"

@implementation CSourceIconView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame IconName:(NSString*)pIconName SourceName:(NSString*)pSourceName

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        m_pIconName = pIconName;
        [m_pIconName retain];
        self.opaque = FALSE;
        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView DEFAULT_TEXT_SIZE]];
        m_pLabel.lineBreakMode = UILineBreakModeWordWrap;
        m_pLabel.numberOfLines = 2;
        [m_pLabel setText:pSourceName];
        [self addSubview:m_pLabel];
        [m_pLabel release];
        self.autoresizingMask = 0xFFFFFFFF;
    }
    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    if (m_pIconName == NULL)
        return;

    CIcon *pIcon = [CImageServer IconByName:m_pIconName];
    CGImageRef pIconImage = [pIcon GetImage:self];

    if (pIconImage == NULL)
        return;

    CGContextRef pRef = UIGraphicsGetCurrentContext();

    int iDX = CGRectGetWidth(rect);
    int iDY = CGRectGetHeight(rect) - 3*[CMainView DEFAULT_TEXT_SIZE];
    int iDXImage = CGImageGetWidth(pIconImage);
    int iDYImage = CGImageGetHeight(pIconImage);
    int iDYDXOut = iDY * 100 / iDX;
    int iDYDXImage = iDYImage * 100 / iDXImage;
    int iDXOut = iDX * 80 / 100;
    int iDYOut = iDY * 80 / 100;
    
    if (iDYDXImage > iDYDXOut)
    {
        iDXOut = iDYOut * iDXImage / iDYImage;
    }
    else
    {
        iDYOut = iDXOut * iDYImage / iDXImage;
    }

    int iXOut = iDX / 2 - iDXOut / 2;
    int iYOut = iDY / 2 - iDYOut / 2;

    CGRect rOutIcon = CGRectMake(iXOut, -iDY+iYOut, iDXOut, iDYOut);
    CGContextScaleCTM(pRef, 1, -1);
    CGContextDrawImage(pRef, rOutIcon, pIconImage);
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pIconName release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;

    CGRect rLabel = [CRect BreakOffBottom:&rThis DY:3*[CMainView DEFAULT_TEXT_SIZE]];
    [m_pLabel setFrame:rLabel];
    
    if (rThis.size.height != m_SizeLast.height || rThis.size.width != m_SizeLast.width)
    {
        [self setNeedsDisplay];
        m_SizeLast = rThis.size;
    }
}

@end
