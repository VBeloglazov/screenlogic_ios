//
//  VolumeLabel.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/26/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"

@class CAudioZone;

/*============================================================================*/

@interface CVolumeLabel : UIView < CSink >

/*============================================================================*/
{
    CAudioZone                          *m_pZone;
    UILabel                             *m_pLabel1;
    UILabel                             *m_pLabel2;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone;

@end
