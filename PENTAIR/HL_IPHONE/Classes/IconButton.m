//
//  IconButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "IconButton.h"
#import "iconview.h"


@implementation CIconButton

/*============================================================================*/

-(id)init

/*============================================================================*/
{
    if (self == [super initWithItems:[NSArray arrayWithObjects:@"", nil]])
    {
        self.momentary = YES;
        self.segmentedControlStyle = UISegmentedControlStyleBar;
        self.tintColor = [UIColor blackColor];
        
        m_pIconView = [[CUserTabIconView alloc] initWithFrame:CGRectZero];
        [m_pIconView setOpaque:NO];
        [m_pIconView SetOptions:ICONVIEW_TVICON];
        [self addSubview:m_pIconView];
        
    }
    
    return self;
}
/*============================================================================*/

- (void)layoutSubviews

/*============================================================================*/
{
    [super layoutSubviews];

    [m_pIconView setFrame:self.bounds];
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pIconView release];
    [super dealloc];
}
/*============================================================================*/

-(void)SetIcon:(NSString*)pName

/*============================================================================*/
{
    [m_pIconView SetUserIcon:pName];
    [m_pIconView setNeedsDisplay];
}
@end
