//
//  GradientView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 1/28/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import "GradientView.h"

@implementation CGradientView

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Selected:(BOOL)bSelected

/*============================================================================*/
{
    if (self = [super initWithFrame:rect]) 
    {
        // Initialization code
        
        self.opaque = NO;
        m_bSelected = bSelected;
    }


    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();

    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();

    CGGradientRef pGradient = NULL;
    if (m_bSelected)
    {
        CGFloat colors[] =
        {
              0.0,        0.0,          0.6,    0.20,
              0.3,        0.3,          1.0,    0.35,
        };

        pGradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    }
    else 
    {
        CGFloat colors[] =
        {
              0.8,        0.8,          0.8,    0.35,
              0.8,        0.8,          0.8,    0.45,
        };

        pGradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    }

    CGColorSpaceRelease(rgb);


    CGPoint ptStart = CGPointMake(rect.origin.y, rect.origin.y);
    CGPoint ptEnd = ptStart;
    ptEnd.y += rect.size.height;

	CGContextClipToRect(pRef, rect);
    CGContextDrawLinearGradient(pRef, pGradient, ptStart, ptEnd, 0);
    CGGradientRelease(pGradient);
    
    UIColor *p1 = NULL;
    if (m_bSelected)
        p1 = [UIColor colorWithRed:0.6 green:0.6 blue:1.0 alpha:.75];
    else
        p1 = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:.45];

    [p1 set];

    CGContextBeginPath(pRef);
    CGContextMoveToPoint(pRef, rect.origin.x,rect.origin.y+rect.size.height);
    CGContextAddLineToPoint(pRef, rect.origin.x, rect.origin.y);
    CGContextAddLineToPoint(pRef, rect.origin.x+rect.size.width, rect.origin.y);

    CGContextStrokePath(pRef);   
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}

@end

