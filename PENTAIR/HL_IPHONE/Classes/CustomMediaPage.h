//
//  CustomMediaPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/13/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLView.h"
#import "NSSocketSink.h"
#import "TabRoundRegion.h"

@class CCustomPage;
@class CMediaZonePage;
@class CHLSegment;
@class CCustomMediaTabPage;

/*============================================================================*/

@interface CCustomMediaPage : UIView <  CNSSocketSink,
                                        CHLView >

/*============================================================================*/
{
    int                                 m_iZoneID;
    int                                 m_iID;
    BOOL                                m_bVisible;
    CMediaZonePage                      *m_pZonePage;
    CCustomPage                         *m_pActivePage;
    CCustomMediaTabPage                 *m_pTabPage;
}

-(id)initWithFrame:(CGRect) rFrame ID:(int)iID ZoneID:(int)iZoneID ZonePage:(CMediaZonePage*)pZonePage;


@end


/*============================================================================*/

@interface CCustomMediaTabPage : CTabRoundRegion

/*============================================================================*/
{
    NSMutableArray                      *m_pPages;
    CCustomPage                         *m_pActivePage;
    BOOL                                m_bVisible;
}

-(id)initWithFrame:(CGRect)rFrame CustomPages:(NSMutableArray*)pPages;
-(void)SetVisible:(BOOL)bNew;


@end
