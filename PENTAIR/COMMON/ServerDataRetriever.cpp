/*
--------------------------------------------------------------------------------

    ServerDataRetriever.cpp

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlm.h"

#include "ServerDataRetriever.h"

/*============================================================================*/

    CServerDataRetriever::CServerDataRetriever(
    int                                 iServer )

    : m_bValid(FALSE)

/*============================================================================*/
{
    if ( !ReadServersoftDataFromFile() )
    {
        //
        // URL
        //
        
        switch (iServer)
        {
        case 0: m_sURL = SERVER_DISPATCHER_URL_0; break;
        case 1: m_sURL = SERVER_DISPATCHER_URL_1; break;
        }

        //
        // Get the address
        //
        HOSTENT *pHE = gethostbyname( (const char*) m_sURL );
        if ( !pHE )
            return;
        for ( int i=0; i<4; i++ )
            m_byAddress[i] = (BYTE) pHE->h_addr_list[0][i];

        m_sAddress.Format( _HLTEXT("%d.%d.%d.%d" ),
            m_byAddress[0], m_byAddress[1], m_byAddress[2], m_byAddress[3] );

        //
        // Get the port
        //
        m_wPort = SERVER_DISPATCHER_DEFAULT_PORT;
    }

    //
    // Everything is OK
    //
    m_bValid = TRUE;
}
/*============================================================================*/

    CServerDataRetriever::~CServerDataRetriever()

/*============================================================================*/
{
}
/*============================================================================*/

    BOOL                        CServerDataRetriever::ReadServersoftDataFromFile()

/*============================================================================*/
{
#ifndef IPHONE

    HANDLE	hFileCFG;
	TCHAR	pPath[1024];

    DWORD dwLen = GetModuleFileName( NULL, pPath, 1000 );
    if ( dwLen < 1 )
        return FALSE;

    TCHAR* pLastSlash = _tcsrchr( pPath, '\\' );
    if ( pLastSlash == NULL )
        return FALSE;

    _tcscpy( pLastSlash+1, _T("Serversoft.dat") );

    hFileCFG = CreateFile(
		    pPath,
		    GENERIC_READ,
		    FILE_SHARE_READ,
		    NULL,
		    OPEN_EXISTING,
		    FILE_ATTRIBUTE_NORMAL,
		    NULL
    );

    if ( INVALID_HANDLE_VALUE == hFileCFG )
        return FALSE;

    char  pData[32];
    DWORD dwRead(0);
    if ( !ReadFile( hFileCFG, pData, 31, &dwRead, NULL ) )
    {
        CloseHandle( hFileCFG );
        return FALSE;
    }
    if ( dwRead < 1 )
    {
        CloseHandle( hFileCFG );
        return FALSE;
    }

    pData[dwRead] = 0;

    char* pColon = strrchr( pData, ':' );
    if ( pColon == NULL )
    {
        CloseHandle( hFileCFG );
        return FALSE;
    }

    int nAddressLen = (int)(pColon-pData);
    char* pAddress = new char[nAddressLen+1];
    strncpy( pAddress, pData, nAddressLen );
    pAddress[nAddressLen] = 0;
    m_sAddress = pAddress;
    delete pAddress;

    char* pPort = new char[dwRead-nAddressLen];
    strcpy( pPort, pColon+1 );
    m_wPort = (WORD) atol( pPort );
    delete pPort;

    if ( !( 0 < m_wPort && m_wPort < 65536 ) )
    {
        CloseHandle( hFileCFG );
        return FALSE;
    }
    
    CloseHandle( hFileCFG );

	return TRUE;
    
#else // IPHONE

    return FALSE;
    
#endif
    
}