//
//  SecPartition.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "subtab.h"
#import "NSSocketSink.h"

@class CNSQuery;
@class CSinkServer;
@class CArmingMode;
@class CSecZone;

/*============================================================================*/

@interface CSecPartition : CSubTab      < CNSSocketSink >

/*============================================================================*/
{
    CSinkServer                         *m_pSinkServer;

    BOOL                                m_bHasPanic;
    BOOL                                m_bArmAllowed;
    BOOL                                m_bGetBypass;
    NSMutableArray                      *m_pArmingModes;
    NSMutableArray                      *m_pZones;
    NSMutableArray                      *m_pFaults;
    
    BOOL                                m_bCommInit;
    
    int                                 m_iPartStatus;
}

-(id)initFromQ:(CNSQuery*)pQ;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

-(void)AddSink:(id)pSink;
-(void)RemoveSink:(id)pSink;

-(int)NArmingModes;
-(CArmingMode*)ArmingMode:(int)iIndex;

-(int)NZones;
-(CSecZone*)Zone:(int)iIndex;

-(int)NZonesNotReady;
-(CSecZone*)ZoneNotReady:(int)iIndex;

-(int)NFaults;
-(NSString*)Fault:(int)iIndex;

-(int)PartitionStatus;

-(void)InitComm;
-(void)ReleaseComm;
-(void)PostGetStatus;

-(void)SetZoneState:(int)iZoneID State:(int)iState;

-(BOOL)StatusArmed; 
-(BOOL)StatusArmable;
-(BOOL)StatusBypassable; 
-(BOOL)StatusAlarmActive;
-(BOOL)StatusEntryDel;     
-(BOOL)StatusExitDel;      
-(BOOL)StatusNoResponse;   
-(BOOL)StatusFire;

-(int)UserModeIndex;
-(int)ArmingMode;
-(CArmingMode*)UserArmingMode:(int)iIndex;
-(int)NVisibleArmingModes;
-(NSString*)ArmingModeText;

-(NSString*)StatusText;

-(void)UpdateSubTabCell:(CSubTabCell*)pSubTabCell;

@end
