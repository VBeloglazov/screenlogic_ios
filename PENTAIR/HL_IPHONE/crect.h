/*
--------------------------------------------------------------------------------

    RECT.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

/*============================================================================*/

@interface CRect : NSObject 

/*============================================================================*/
{

}

+(CGRect)BreakOffLeft:(CGRect*) pRect DX:(int) iDX;
+(CGRect)BreakOffRight:(CGRect*) pRect DX:(int) iDX;
+(CGRect)BreakOffTop:(CGRect*) pRect DY:(int) iDY;
+(CGRect)BreakOffBottom:(CGRect*) pRect DY:(int) iDY;
+(void)Inset:(CGRect*) pRect DX:(int) iDX DY:(int)iDY;
+(CGRect)ScreenRect:(UIInterfaceOrientation)orientation;
+(int)XMid:(CGRect)rRect;
+(int)YMid:(CGRect)rRect;
+(void)ConfineToAspect:(CGRect*)pRect DXDY:(double)dAspect;
+(void)ConfineToAspect:(CGRect*)pRect Source:(CGRect)rSource;
+(void)ExpandToAspect:(CGRect*)pRect DXDY:(double)dAspect;
+(void)CenterDownToX:(CGRect*)pRect DX:(int)iDX;
+(void)CenterDownToY:(CGRect*)pRect DY:(int)iDY;
+(CGRect)CenteredRect:(CGRect)rect DX:(int)iDX DY:(int)iDY;
+(CGRect)CreateXSection:(CGRect)rect Index:(int)iIndex Of:(int)iN;
+(CGRect)CreateYSection:(CGRect)rect Index:(int)iIndex Of:(int)iN;

@end
