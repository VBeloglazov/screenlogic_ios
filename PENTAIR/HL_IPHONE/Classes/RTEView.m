//
//  CRTEView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "RTEView.h"
#import "nsmsg.h"
#import "timeview.h"

@implementation CRunTimeEvent
/*============================================================================*/

-(id)initWithDate:(NSDate*)pStart End:(NSDate*)pEnd

/*============================================================================*/
{
    m_pDateStart = pStart;
    m_pDateEnd = pEnd;
    [m_pDateStart retain];
    [m_pDateEnd retain];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pDateStart release];
    [m_pDateEnd release];
    [super dealloc];
}
/*============================================================================*/

-(NSDate*)StartDate     { return m_pDateStart;  }
-(NSDate*)EndDate       { return m_pDateEnd;    }

/*============================================================================*/

-(int)OverlapWith:(NSDate*)pStart End:(NSDate*)pEnd

/*============================================================================*/
{
    if ([pStart earlierDate:m_pDateEnd] == m_pDateEnd)
        return 0;
    if ([pEnd laterDate:m_pDateStart] == m_pDateStart)
        return 0;
    NSDate *pStartCalc = [m_pDateStart laterDate:pStart];
    NSDate *pEndCalc = [m_pDateEnd earlierDate:pEnd];
    int iDiff =  [pEndCalc timeIntervalSinceDate:pStartCalc];
    if (iDiff < 0)
    {
        iDiff = -iDiff;
    }
    
    return iDiff;
}
@end

@implementation CRTESeries
/*============================================================================*/

-(id)initWithColor:(UIColor*)pColor

/*============================================================================*/
{
    m_pRGB = pColor;
    [m_pRGB retain];
    m_pPoints = [[NSMutableArray alloc] init];
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pRGB release];
    [m_pPoints release];
    [m_pStartDate release];
    [super dealloc];
}
/*============================================================================*/

-(void)ReadFrom:(CNSMSG*)pMSG

/*============================================================================*/
{
    NSCalendar *pCal = [NSCalendar currentCalendar];
    NSDateComponents *pComps = [[[NSDateComponents alloc] init] autorelease];
    CDateHelper *pHelper = [[[CDateHelper alloc] init] autorelease];

    int iN = [pMSG GetInt];
    for (int i = 0; i < iN; i++)
    {
        NSDate *pDateStart = [[pMSG GetTimeWith:pComps Calendar:pCal DateHelper:pHelper] retain];
        NSDate *pDateEnd   = [[pMSG GetTimeWith:pComps Calendar:pCal DateHelper:pHelper] retain];
        CRunTimeEvent *pPt = [[CRunTimeEvent alloc] initWithDate:pDateStart End:pDateEnd];
        [m_pPoints addObject:pPt];
        [pDateStart release];
        [pDateEnd release];
        [pPt release];
    }
}
/*============================================================================*/

-(void)ProcessState:(NSDate*)pDate True:(BOOL)bTrue

/*============================================================================*/
{
    if (bTrue)
    {
        if (m_pStartDate != NULL)
            return;
        m_pStartDate = pDate;
        [m_pStartDate retain];
        return;
    }
    else
    {
        if (m_pStartDate == NULL)
            return;
        CRunTimeEvent *pRTE = [[CRunTimeEvent alloc] initWithDate:m_pStartDate End:pDate];
        [m_pPoints addObject:pRTE];
        [pRTE release];
        [m_pStartDate release];
        m_pStartDate = NULL;
    }
}
/*============================================================================*/

-(void)AddRTE:(NSDate*)pStart End:(NSDate*)pEnd;

/*============================================================================*/
{
    CRunTimeEvent *pRTE = [[CRunTimeEvent alloc] initWithDate:pStart End:pEnd];
    [m_pPoints addObject:pRTE];
    [pRTE release];
}
/*============================================================================*/

-(NSMutableArray*)PointList

/*============================================================================*/
{
    return m_pPoints;
}
/*============================================================================*/

-(UIColor*)Color

/*============================================================================*/
{
    return m_pRGB;
}
@end

@implementation CRTEView
/*============================================================================*/

-(id)initWithFrame:(CGRect) rFrame XOrigin:(int)iXOrigin Series:(CRTESeries*)pSeries TimeView:(CTimeView*)pTimeView;

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame]) 
    {
        // Initialization code
        m_iXOrigin = iXOrigin;
        m_pSeries = pSeries;
        m_pTimeView = pTimeView;
        [self setOpaque:NO];
    }
    return self;
}
/*============================================================================*/

- (void)drawRect:(CGRect)rect 

/*============================================================================*/
{
    CGContextRef pRef = UIGraphicsGetCurrentContext();

    for (int iPass = 0; iPass < 2; iPass++)
    {
        UIColor *pRGB = NULL;
        switch (iPass)
        {
        case 0: pRGB = [[m_pSeries Color] colorWithAlphaComponent:.5];  break;
        case 1: pRGB = [m_pSeries Color];   break;
        }

        [pRGB set];

        NSMutableArray *pPoints = [m_pSeries PointList];
        BOOL bDone = FALSE;
        for (int iPoint = 0; iPoint < [pPoints count] && !bDone; iPoint++)
        {
            CRunTimeEvent *pEvent = (CRunTimeEvent*)[pPoints objectAtIndex:iPoint];
            int iX0 = [m_pTimeView XLogical:[pEvent StartDate]] - m_iXOrigin;
            int iX1 = [m_pTimeView XLogical:[pEvent EndDate]] - m_iXOrigin;

            int iDX = MAX(1, iX1-iX0);
            switch (iPass)
            {
            case 0:     CGContextFillRect(pRef, CGRectMake(iX0, 0, iDX, self.frame.size.height));               break;
            case 1:     CGContextStrokeRect(pRef, CGRectMake(iX0, 0, iDX, self.frame.size.height));             break;
            }

            if (iX0 >= self.frame.size.width)
                bDone = TRUE;
        }
    }
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)SetXOrigin:(int)iXOrigin

/*============================================================================*/
{
    m_iXOrigin = iXOrigin;
}

@end
