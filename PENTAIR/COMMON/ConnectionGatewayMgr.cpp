/*
--------------------------------------------------------------------------------

    ConnectionGatewayMgr.cpp

    Copyright 2009 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "schema.h"
#include "platform.h"
#include "hlmsg.h"
#include "hlcode.h"
#include "StatusSink.h"
#include "SocketError.h"
#include "SocketHL.h"

#include "ConnectionGatewayMgr.h"

#define RELAYSERVER_PORT                7576    // TODO: Read from Servers DB ?

#define ERR_GWM_SINGLETON               _HLTEXT("Previous instance of CConnectionGatewayMgr is not disposed")
#define ERR_GWM_CONNECTION              _HLTEXT("Cannot connect")
#define ERR_GWM_NOT_CONNECTED           _HLTEXT("Not connected")
#define ERR_GWM_SEND_FAILED             _HLTEXT("Send failed")
#define ERR_GWM_COMMUNICATION_RELAY     _HLTEXT("Communication to Relay Server failed")
#define ERR_GWM_CONNECTION_REQUEST      _HLTEXT("Relay Server rejected the request")
#define ERR_GWM_COMMUNICATION           _HLTEXT("Communication to Gateway failed")
#define ERR_GWM_BAD_RESPONSE            _HLTEXT("Unrecognized response")
#define ERR_GWM_LOGIN_REJECTED          _HLTEXT("Login rejected by Gateway")
#define ERR_GWM_GET_CHALLENGE_FAILED    _HLTEXT("Can't get Challenge String")
#define ERR_GWM_GET_VERSION_FAILED      _HLTEXT("Can't get Version Info")

#ifdef PENTAIRMON
    #define m_pThis this
#else
    CConnectionGatewayMgr*                    CConnectionGatewayMgr::m_pThis = NULL;
#endif

/*============================================================================*/

    BOOL                        CConnectionGatewayMgr::GetGatewayData(
    CHLString                   sUserName,
    CHLString                   sPassword,
    CHLString                   sGatewayAddress,
    WORD                        wGatewayPort,
    BOOL                        bRelayOn,
    int                         iConType,
    CStatusSink*                pStatus )

/*============================================================================*/
{
#ifndef PENTAIRMON
    if ( m_pThis )
    {
        m_pThis->SetLastError( ERR_GWM_SINGLETON );
        return FALSE;
    }

    m_pThis = new CConnectionGatewayMgr( sUserName, sGatewayAddress, wGatewayPort, bRelayOn, pStatus );
#endif

    if ( !m_pThis->IsValid() )
    {
        CHLString sStatus;
        sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*)m_pThis->LastError() );
        pStatus->SetStatus( sStatus );
        return FALSE;
    }

    pStatus->SetStatus( _HLTEXT("Connected to System, logging in...") );

    if ( !m_pThis->DoLogin( sUserName, sPassword, iConType ) )
    {
        CHLString sStatus;
        sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*)m_pThis->LastError() );
        pStatus->SetStatus( sStatus );
        return FALSE;
    }

    pStatus->SetStatus( _HLTEXT("Connection to System OK.") );

    pStatus->SetStatus( _HLTEXT("Getting Version Information...") );

    if ( !m_pThis->GetVersion() )
    {
        CHLString sStatus;
        sStatus.Format( _HLTEXT("ERROR: %s"), (const HLTCHAR*)m_pThis->LastError() );
        pStatus->SetStatus( sStatus );
        return FALSE;
    }

    CHLString sMSG;
    sMSG.Format( _HLTEXT("Remote System Version is: %s."),
        (const HLTCHAR*) m_pThis->m_sGatewayVersion );
    pStatus->SetStatus( sMSG );

    return TRUE;
}
/*============================================================================*/

    void                        CConnectionGatewayMgr::Dispose(
    BOOL                        bDisconnect )

/*============================================================================*/
{
#if PENTAIRMON
    if ( bDisconnect )
    {
        m_pSocket->Disconnect( _HLTEXT("Dispose") );
        delete m_pSocket;
        m_pSocket = NULL;
    }
#else
    if ( m_pThis )
    {
        m_pThis->m_bDisconnect = bDisconnect;
        delete m_pThis;
        m_pThis = NULL;
    }
#endif
}
/*============================================================================*/

    CConnectionGatewayMgr::CConnectionGatewayMgr(
    CHLString                   sUserName,
    CHLString                   sGatewayAddress,
    WORD                        wGatewayPort,
    BOOL                        bRelayOn,
    CStatusSink*                pStatus )

    : m_bValid          ( FALSE ),
      m_strLastError    ( CHLString(_HLTEXT("")) ),
      m_pSocket         ( NULL ),
      m_bDisconnect     ( FALSE ),
      m_sGatewayVersion ( _HLTEXT("") ),
      m_iSystemType     ( -1 )

/*============================================================================*/
{
    BOOL bConnected(FALSE);

    if ( !bRelayOn )
    {
        //
        // Gateway should be in the Listen mode: try Direct first
        //

        bConnected = ConnectionDirect( sGatewayAddress, wGatewayPort, pStatus );

        if ( !bConnected )
        {
            pStatus->SetStatus( CHLString( _HLTEXT("Direct connection failed.") ) );

            if ( !ConnectionRelay( sUserName, wGatewayPort, pStatus ) )
            {
                CHLString strError;
                strError.Format( _HLTEXT("%s. %s"), ERR_GWM_CONNECTION, (const HLTCHAR*)m_pSocket->LastError() );
#if defined(UNICODE)
                TRACE( (wchar_t*)strError );
#else
                TRACE( strError );
#endif
                SetLastError( strError );
                return;
            }
        }
    }
    else
    {
        //
        // Gateway should be in the Relay Server mode: try Relay first
        //

        bConnected = ConnectionRelay( sUserName, wGatewayPort, pStatus );

        if ( !bConnected )
        {
            pStatus->SetStatus( CHLString( _HLTEXT("Relay connection failed.") ) );

            if ( !ConnectionDirect( sGatewayAddress, wGatewayPort, pStatus ) )
            {
                CHLString strError;
				strError.Format( _HLTEXT("%s. %s"), ERR_GWM_CONNECTION, (const HLTCHAR*)m_pSocket->LastError() );
#if defined(UNICODE)
				TRACE( (wchar_t*)strError );
#else
				TRACE( strError );
#endif
                SetLastError( strError );
                return;
            }
        }
    }

    // Tell the GATEWAY to switch us from the CFileServer to CServerHost
    char byOut[] = { "CONNECTSERVERHOST\r\n\r\n" };
    m_pSocket->Send(21, (BYTE*)byOut);

    m_bValid = TRUE;
}
/*============================================================================*/

    CConnectionGatewayMgr::~CConnectionGatewayMgr()

/*============================================================================*/
{
    if ( m_bDisconnect )
    {
        m_pSocket->Disconnect( _HLTEXT("Destructor") );
    }

    delete m_pSocket;
}
/*============================================================================*/

    BOOL                        CConnectionGatewayMgr::ConnectionDirect(
    CHLString                   sAddress,
    WORD                        wPort,
    CStatusSink*                pStatus )

/*============================================================================*/
{
    CHLString sStatus;
    sStatus.Format( _HLTEXT("Connecting to System at %s:%d..." ),
        (const HLTCHAR*) sAddress, wPort );
    pStatus->SetStatus( sStatus );

    m_pSocket = new CSocketHL( sAddress, wPort );
    
    if ( !m_pSocket->IsValid() )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_CONNECTION, (const HLTCHAR*)m_pSocket->LastError() );
#if defined(UNICODE)
		TRACE( (wchar_t*)strError );
#else
		TRACE( strError );
#endif
        SetLastError( strError );
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CConnectionGatewayMgr::ConnectionRelay(
    CHLString                   sUserName,
    WORD                        wPort,
    CStatusSink*                pStatus )

/*============================================================================*/
{
    //
    // Get the address
    //
    HOSTENT *pHE = gethostbyname( "homelogic.com" );
    if ( !pHE )
    {
        SetLastError( SocketError::LastSocketError() );
        return FALSE;
    }

    //
    // Establish connection
    //
    BYTE pAddress[4];
    for ( int i=0; i<4; i++ )
        pAddress[i] = (BYTE) pHE->h_addr_list[0][i];

    CHLString sStatus;
    sStatus.Format( _HLTEXT("Connecting to Relay Server at %d.%d.%d.%d:%d..." ),
        pAddress[0], pAddress[1], pAddress[2], pAddress[3], RELAYSERVER_PORT );
    pStatus->SetStatus( sStatus );

#ifdef PENTAIRMON
    if (m_pSocket)
        Dispose(TRUE);
#endif
    m_pSocket = new CSocketHL( pAddress, RELAYSERVER_PORT );
    if ( !m_pSocket || !m_pSocket->IsValid() || !m_pSocket->IsConnected() )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_CONNECTION, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    pStatus->SetStatus( _HLTEXT("Connected to Relay Server.") );

    pStatus->SetStatus( _HLTEXT("Requesting Gateway connection...") );

    //
    // Send connection message and wait for response
    //
    char cBuf[38];
    memset(cBuf, 0, 38);    
    
    int iLen = __min( 31, sUserName.GetLength() );
    char* pName = (char*)sUserName.GetBufferCHAR(iLen);

    memcpy( cBuf, pName, iLen );
    memcpy( cBuf+32, &wPort, sizeof(WORD) );

    if ( !m_pSocket->Send(38, (BYTE*)cBuf) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_COMMUNICATION_RELAY, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    CHLTimer tmWait;
    BOOL bReceived(FALSE);
    while ( !bReceived  &&  tmWait.HLTickCount() < 30000 )
    {
        bReceived = m_pSocket->CheckReceiveBuf();
    }

    if ( !bReceived )
    {
        SetLastError( ERR_GWM_CONNECTION_REQUEST );
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CConnectionGatewayMgr::DoLogin(
    CHLString                   sLoginName,
    CHLString                   sPassword,
    int                         iConnectionType )

/*============================================================================*/
{
    if ( !m_pSocket->IsConnected() )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_NOT_CONNECTED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    // Get Challenge String
    CHLString sChallengeStr;
    if ( !GetChallengeString( &sChallengeStr ) )
    {
        return FALSE;
    }

    // Encrypt the password
    int iLen = 0;
    char *pPass = CHLCode::MakeBlock( sPassword, &iLen, (BYTE)0 );
    CHLCode encoder;
    encoder.MakeKey( pPass, CHLCode::sm_chain0 );
    char* pChallenge = CHLCode::MakeBlock( sChallengeStr, &iLen, 0 );

    char pEncrypted[16];
    encoder.EncryptBlock( pChallenge, pEncrypted );

    // Send Login Request to the GATEWAY
    CHLMSG msgQ( (short)0, HLM_CLIENTLOGINQ, 256 );

    msgQ.PutInt   ( SCHEMA_LATEST         );
    msgQ.PutInt   ( iConnectionType       );
    msgQ.PutString( sLoginName            );
    msgQ.PutInt   ( 16                    );
    msgQ.PutData  ( (BYTE*)pEncrypted, 16 );
    msgQ.PutInt   ( PROC_INT              );
    
    delete [] pPass;
    delete [] pChallenge;

    if ( !m_pSocket->Send( msgQ.MSGSize(), msgQ.MSG(), msgQ.Critical() ) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_SEND_FAILED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Wait and process response(s)
    //
    BOOL bReceived(FALSE);
    while( !bReceived )
    {
        if ( !m_pSocket->IsConnected() )
        {
            break; // while()
        }

        if ( m_pSocket->CheckReceiveMsg() )
        {
            HLT_HEADER* pHeader = (HLT_HEADER*) m_pSocket->Message();

            short shMessageID = pHeader->m_shMessageID;

            switch( shMessageID )
            {
            case HLM_CLIENTLOGINA:
                {
                    bReceived = TRUE;

                    /* In case we needed these data
                    CHLMSG msgA( pHeader );
                    BOOL bLocal;       msgA.GetInt ( &bLocal      );
                    BYTE byReturn[16]; msgA.GetData( byReturn, 16 );
                    */
                }
                break;

            case HLM_CLIENTLOGINREJECTED:
                SetLastError( ERR_GWM_LOGIN_REJECTED );
                return FALSE;

            default:
                // Unsolicited Gateway's messages: ignored
                break;
            }
        }
    }
    
    if ( !bReceived )
    {
        SetLastError( ERR_GWM_COMMUNICATION );
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CConnectionGatewayMgr::GetChallengeString(
    CHLString*                  pChallengeStr )

/*============================================================================*/
{
    CHLMSG msgQ(0, HLM_CHALLENGESTRINGQ);

    if ( !m_pSocket->Send( msgQ.MSGSize(), msgQ.MSG(), msgQ.Critical() ) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_SEND_FAILED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Wait and process response(s)
    //
    BOOL bReceived = FALSE;
    while( !bReceived )
    {
        if ( !m_pSocket->IsConnected() )
        {
            break; // while()
        }

        if ( m_pSocket->CheckReceiveMsg() )
        {
            HLT_HEADER* pHeader = (HLT_HEADER*) m_pSocket->Message();

            short shMessageID = pHeader->m_shMessageID;

            switch( shMessageID )
            {
            case HLM_CHALLENGESTRINGA:
                {
                    bReceived = TRUE;

                    CHLMSG msgA( pHeader );

                    if ( !msgA.GetString(pChallengeStr) )
                    {
                        SetLastError( ERR_GWM_GET_CHALLENGE_FAILED );
                        return FALSE;
                    }
                }
                break;

            default:
                // Unsolicited Gateway's messages: ignored
                break;
            }
        }
    }
    
    if ( !bReceived )
    {
        SetLastError( ERR_GWM_COMMUNICATION );
        return FALSE;
    }

    return TRUE;
}
/*============================================================================*/

    BOOL                        CConnectionGatewayMgr::GetVersion()

/*============================================================================*/
{
    CHLMSG msgQ(0, HLM_SYSCONFIG_GETVERSIONQ);

    if ( !m_pSocket->Send( msgQ.MSGSize(), msgQ.MSG(), msgQ.Critical() ) )
    {
        CHLString strError;
        strError.Format( _HLTEXT("%s. %s"), ERR_GWM_SEND_FAILED, (const HLTCHAR*)m_pSocket->LastError() );
        SetLastError( strError );
        return FALSE;
    }

    //
    // Wait and process response(s)
    //
    BOOL bReceived = FALSE;
    while( !bReceived )
    {
        if ( !m_pSocket->IsConnected() )
        {
            break; // while()
        }

        if ( m_pSocket->CheckReceiveMsg() )
        {
            HLT_HEADER* pHeader = (HLT_HEADER*) m_pSocket->Message();

            short shMessageID = pHeader->m_shMessageID;

            switch( shMessageID )
            {
            case HLM_SYSCONFIG_GETVERSIONA:
                {
                    bReceived = TRUE;

                    CHLMSG msgA( pHeader );

                    if ( !msgA.GetString(&m_sGatewayVersion) )
                    {
                        SetLastError( ERR_GWM_GET_VERSION_FAILED );
                        return FALSE;
                    }

                     msgA.GetInt( &m_iSystemType );
                }
                break;

            default:
                // Unsolicited Gateway's messages: ignored
                break;
            }
        }
    }
    
    if ( !bReceived )
    {
        SetLastError( ERR_GWM_COMMUNICATION );
        return FALSE;
    }

    return TRUE;
}
