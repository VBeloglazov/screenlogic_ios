//
//  PoolMainPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"

@class CNSPoolConfig;
@class CHLSegment;
@class CPoolHistoryView;

#define PAGE_POOL                       0
#define PAGE_SPA                        1
#define PAGE_CIRCUITS                   2
#define PAGE_HISTORY                    3
#define PAGE_POOL_SPA                   4
#define PAGE_MAIN                       5

/*============================================================================*/

@interface CPoolSubSystemPage : CSubSystemPage

/*============================================================================*/
{
    CNSPoolConfig                       *m_pConfig;

    UITabBar                            *m_pTabBar;
    int                                 m_iPages[16];

    UIView                              *m_pMainView;
    
    UIView                              *m_pPage;
    UIView                              *m_pPage0;
    UIView                              *m_pPage1;
    UIView                              *m_pPage2;
    UIView                              *m_pPage3;
    CPoolHistoryView                    *m_pPage4;
    UIView                              *m_pPage5;
    
    BOOL                                m_bInit;
    int                                 m_iLastIndex;
    CHLSegment                          *m_pPageCtrl;

}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage Config:(CNSPoolConfig*)pConfig SubTab:(CSubTab*)pSubTab;
-(void)SelectPage:(id)sender;

@end
