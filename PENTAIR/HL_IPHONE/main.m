//
//  main.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageServer.h"

 extern void mysighandler(int sig, siginfo_t *info, void *context);

/*============================================================================*/

    int main(int argc, char *argv[]) 

/*============================================================================*/
{
    struct sigaction mySigAction;
    mySigAction.sa_sigaction = mysighandler;
    mySigAction.sa_flags = SA_SIGINFO;
    sigemptyset(&mySigAction.sa_mask);
    sigaction(SIGQUIT, &mySigAction, NULL);
    sigaction(SIGILL, &mySigAction, NULL);
    sigaction(SIGTRAP, &mySigAction, NULL);
    sigaction(SIGABRT, &mySigAction, NULL);
    sigaction(SIGEMT, &mySigAction, NULL);
    sigaction(SIGFPE, &mySigAction, NULL);
    sigaction(SIGBUS, &mySigAction, NULL);
    sigaction(SIGSEGV, &mySigAction, NULL);
    sigaction(SIGSYS, &mySigAction, NULL);
    sigaction(SIGPIPE, &mySigAction, NULL);
    sigaction(SIGALRM, &mySigAction, NULL);
    sigaction(SIGXCPU, &mySigAction, NULL);
    sigaction(SIGXFSZ, &mySigAction, NULL);
        
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    [CImageServer Init];
	int retVal = UIApplicationMain(argc, argv, nil, @"CAppDelegate");
    [CImageServer Dealloc];
	[pool release];
	return retVal;
}
/*============================================================================*/

    void mysighandler(int sig, siginfo_t *info, void *context) 
    
/*============================================================================*/
{
}