//
//  HVACPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/17/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubSystemPage.h"
#import "Sink.h"

@class CTStat;
@class CHVACHistoryView;
@class CHVACTempPage;
@class CHVACHumidityPage;
@class CHLSegment;

/*============================================================================*/

@interface CHVACPage : CSubSystemPage < CSink >

/*============================================================================*/
{
    BOOL                                m_bInit;
    CTStat                              *m_pTStat;
    UIView                              *m_pMainView;
    CHVACTempPage                       *m_pPage1;
    UIView                              *m_pPage2;
    CHVACHistoryView                    *m_pPage3;
    CHVACHistoryView                    *m_pPage4;
    CHVACHumidityPage                   *m_pPage5;
    UIView                              *m_pPage;
    int                                 m_iLastIndex;
    CHLSegment                          *m_pPageCtrl;
    BOOL                                m_bProgramOption;
    int                                 m_iLastState;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)Init:(CSubTab*)pSubTab;
-(void)SelectPage:(id)sender;
-(void)InitPageCtrl;
-(BOOL)ShowProgramOption;
-(int)SelectedPageType;

@end
