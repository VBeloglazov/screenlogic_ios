//
//  TabRoundRegion.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitRoundRegion.h"

@class CRoundRegion;
@class CHLSegment;
@class CUserTabIconView;

#define TAB_RREGION_NOINSET         0x00010000

/*============================================================================*/

@interface CTabRoundRegion : CSplitRoundRegion 

/*============================================================================*/
{
    int                             m_iLastIndex;
    CHLSegment                      *m_pPageCtrl;
}

-(void)InitPageWithStrings:(NSMutableArray*)pTabs;
-(void)InitPageWithViews:(NSMutableArray*)pTabs;

-(void)SelectPage:(id)sender;
-(UIView*)CreatePageForIndex:(int)iIndex;

@end
