//
//  Calendar.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/28/10.
//  Copyright 2010 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitRoundRegion.h"
#import "HLTableFrame.h"
#import "NSSocketSink.h"

/*============================================================================*/

@interface CCalendarControl : CSplitRoundRegion

/*============================================================================*/
{
}

-(id)initWithFrame:(CGRect)rFrame;

@end
/*============================================================================*/

@interface CCalendarView : UIView  < CNSSocketSink >

/*============================================================================*/
{
    CSplitRoundRegion           *m_pParentRegion;
    int                         m_iYear;
    int                         m_iMonth;
    NSMutableArray              *m_pDays;

    int                         m_iYearToday;
    int                         m_iMonthToday;
    int                         m_iDayToday;
    int                         m_iDayOfWeekToday;
}

-(id)initWithFrame:(CGRect)rFrame Control:(CSplitRoundRegion*)pParentRegion;
-(void)Init;

@end


/*============================================================================*/

@interface CDayView : UIView

/*============================================================================*/
{
    int                     m_iCol;
    int                     m_iRow;
    UILabel                 *m_pLabel;
}

-(id)initWithFrame:(CGRect)rect Text:(NSString*)psText IsToday:(BOOL)bIsToday Column:(int)iCol Row:(int)iRow;

-(int)Column;
-(int)Row;

@end
