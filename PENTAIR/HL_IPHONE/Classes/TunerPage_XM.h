//
//  TunerPage_XM.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "Sink.h"
#import "NSSocketSink.h"

@class CAudioZone;
@class CTunerFavsPage;
@class CTableTitleHeaderView;
@class CMediaZonePage;
@class CHLTableFrame;

@class CTunerPageXM;
@class CStation;

/*============================================================================*/

@interface CXMGenre : NSObject < CNSSocketSink >

/*============================================================================*/
{
    NSString                            *m_pName;
    int                                 m_iID;
    NSMutableArray                      *m_pStations;
    CTunerPageXM                        *m_pView;
}

-(id)initWithID:(int)iID Name:(NSString*)pName View:(CTunerPageXM*)pView;
-(int)ID;
-(NSString*)Name;
-(int)NStations;
-(CStation*)Station:(int)iIndex;

@end

/*============================================================================*/

@interface CTunerPageXM : UIView              < UITableViewDelegate, 
                                                UITableViewDataSource,
                                                CNSSocketSink >

/*============================================================================*/
{
    CMediaZonePage                      *m_pMediaZonePage;

    UIView                              *m_pMainView;

    CAudioZone                          *m_pZone;
    CHLTableFrame                       *m_pTableView;

    NSMutableArray                      *m_pSectionTitles;
    NSMutableArray                      *m_pGenres;
}

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone MediaZonePage:(CMediaZonePage*)pZonePage;

-(void)DeselectAll;
-(void)GroupLoaded;

-(NSString*)TruncateGenreTitle:(NSString*)pName;

@end
