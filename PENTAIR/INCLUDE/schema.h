/*
--------------------------------------------------------------------------------

    SCHEMA.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef SCHEMA_H
#define SCHEMA_H


    //
    // Versions for file reading and writing.
    //
    #define SCHEMA_0            0   // Initial
    #define SCHEMA_1            1   // Build 36+
    #define SCHEMA_2            2   // Build 37+
    #define SCHEMA_3            3   // Build 40+
    #define SCHEMA_4            4   // Build 42+
    #define SCHEMA_5            5   // Build 50+
    #define SCHEMA_6            6   // Build 55+
    #define SCHEMA_7            7   // Build 59+
    #define SCHEMA_8            8   // Build 63+
    #define SCHEMA_9            9   // Build 71+
    #define SCHEMA_10           10  // Build 79+
    #define SCHEMA_11           11  // Build 82+
    #define SCHEMA_12           12  // Build 86+
    #define SCHEMA_13           13  // Build 91+
    #define SCHEMA_14           14  // Build 94+
    #define SCHEMA_15           15  // Build 95+
    #define SCHEMA_16           16  // Build 98+
    #define SCHEMA_17           17  // Build 101+
    #define SCHEMA_18           18  // Version 2 Build 1
    #define SCHEMA_19           19  // Version 2 Build 11+
    #define SCHEMA_20           20  // Version 2 Build 36+
    #define SCHEMA_21           21  // Version 2 Build 81+
    #define SCHEMA_22           22  // Version 2 Build 82+
    #define SCHEMA_23           23  // Version 2 Build 83+
    #define SCHEMA_24           24  // Version 2 Build 87+
    #define SCHEMA_25           25  // Version 2 Build 94+
    #define SCHEMA_26           26  // Version 2 Build 108+
    #define SCHEMA_27           27  // Version 2 Build 116+
    #define SCHEMA_28           28  // Version 2 Build 127+
    #define SCHEMA_29           29  // Version 2 Build 153+
    #define SCHEMA_30           30  // Version 2 Build 158+
    #define SCHEMA_31           31  // Version 2 Build 184+
    #define SCHEMA_32           32  // Version 2 Build 221+
    #define SCHEMA_33           33  // Version 2 Build 228+
    #define SCHEMA_34           34  // Version 2 Build 229+
    #define SCHEMA_35           35  // Version 2 Build 232+
    #define SCHEMA_36           36  // Version 2 Build 248+
    #define SCHEMA_37           37  // Version 2 Build 260+
    #define SCHEMA_38           38  // Version 2 Build 270+
    #define SCHEMA_39           39  // Version 2 Build 277+
    #define SCHEMA_40           40  // Version 2 Build 279+
    #define SCHEMA_41           41  // Version 2 Build 291+
    #define SCHEMA_42           42  // Version 3 Build 7+
    #define SCHEMA_43           43  // Version 3 Build 49+
    #define SCHEMA_44           44  // Version 3 Build 54+
    #define SCHEMA_45           45  // Version 3 Build 77+
    #define SCHEMA_46           46  // Version 3 Build 81+
    #define SCHEMA_47           47  // Version 3 Build 102+
    #define SCHEMA_48           48  // Version 3 Build 126+
    #define SCHEMA_49           49  // Version 3 Build 128+
    #define SCHEMA_50           50  // Version 3 Build 153+
    #define SCHEMA_51           51  // Version 3 Build 192+
    #define SCHEMA_52           52  // Version 3 Build 198+
    #define SCHEMA_53           53  // Version 3 Build 229+
    #define SCHEMA_54           54  // Version 3 Build 259+
    #define SCHEMA_55           55  // Version 3 Build 260+
    #define SCHEMA_56           56  // Version 3 Build 261+
    #define SCHEMA_57           57  // Version 3 Build 261+
    #define SCHEMA_58           58  // Version 3 Build 268+
    #define SCHEMA_59           59  // Version 3 Build 270+
    #define SCHEMA_60           60  // Version 3 Build 270+
    #define SCHEMA_61           61  // Version 3 Build 274+
    #define SCHEMA_62           62  // Version 3 Build 274+
    #define SCHEMA_63           63  // Version 3 Build 274+
    #define SCHEMA_64           64  // Version 3 Build 281+
    #define SCHEMA_65           65  // Version 3 Build 285+
    #define SCHEMA_66           66  // Version 3 Build 285+
    #define SCHEMA_67           67  // Version 3 Build 294+
    #define SCHEMA_68           68  // Version 3 Build 294+
    #define SCHEMA_69           69  // Version 3 Build 313+
    #define SCHEMA_70           70  // Version 3 Build 319+
    #define SCHEMA_71           71  // Version 3 Build 319+
    #define SCHEMA_72           72  // Version 3 Build 319+
    #define SCHEMA_73           73  // Version 3 Build 323+
    #define SCHEMA_74           74  // Version 3 Build 345+
    #define SCHEMA_75           75  // Version 3 Build 353+
    #define SCHEMA_76           76  // Version 3 Build 360+
    #define SCHEMA_77           77  // Version 3 Build 366+
    #define SCHEMA_78           78  // Version 4 Build 9+
    #define SCHEMA_79           79  // Version 4 Build 12+
    #define SCHEMA_80           80  // Version 4 Build 14+
    #define SCHEMA_81           81  // Version 4 Build 17+
    #define SCHEMA_82           82  // Version 4 Build 17+
    #define SCHEMA_83           83  // Version 4 Build 17+
    #define SCHEMA_84           84  // Version 4 Build 26+
    #define SCHEMA_85           85  // Version 4 Build 26+
    #define SCHEMA_86           86  // Version 4 Build 36+
    #define SCHEMA_87           87  // Version 4 Build 38+
    #define SCHEMA_88           88  // Version 4 Build 38+
    #define SCHEMA_89           89  // Version 4 Build 38+
    #define SCHEMA_90           90  // Version 4 Build 56+
    #define SCHEMA_91           91  // Version 4 Build 61+
    #define SCHEMA_92           92  // Version 4 Build 65+
    #define SCHEMA_93           93  // Version 4 Build 65+
    #define SCHEMA_94           94  // Version 4 Build 72+
    #define SCHEMA_95           95  // Version 4 Build 83+
    #define SCHEMA_96           96  // Version 4 Build 86+
    #define SCHEMA_97           97  // Version 4 Build 87+
    #define SCHEMA_98           98  // Version 4 Build 88+
    #define SCHEMA_99           99  // Version 4 Build 89+
    #define SCHEMA_100          100 // Version 4 Build 91+
    #define SCHEMA_101          101 // Version 4 Build 101+
    #define SCHEMA_102          102 // Version 4 Build 148+
    #define SCHEMA_103          103 // Version 4 Build 148+
    #define SCHEMA_104          104 // Version 4 Build 155+
    #define SCHEMA_105          105 // Version 4 Build 175+
    #define SCHEMA_106          106 // Version 4 Build 187+
    #define SCHEMA_107          107 // Version 4 Build 189+
    #define SCHEMA_108          108 // Version 4 Build 238+
    #define SCHEMA_109          109 // Version 4 Build 246+
    #define SCHEMA_110          110 // Version 4 Build 248+
    #define SCHEMA_111          111 // Version 4 Build 263+
    #define SCHEMA_112          112 // Version 4 Build 267+
    #define SCHEMA_113          113 // Version 4 Build 274+
    #define SCHEMA_114          114 // Version 4 Build 275+
    #define SCHEMA_115          115 // Version 4 Build 297+
    #define SCHEMA_116          116 // Version 4 Build 298+
    #define SCHEMA_117          117 // Version 4 Build 314+
    #define SCHEMA_118          118 // Version 4 Build 315+
    #define SCHEMA_119          119 // Version 4 Build 341+
    #define SCHEMA_120          120 // Version 4 Build 341+
    #define SCHEMA_121          121 // Version 4 Build 363+
    #define SCHEMA_122          122 // Version 4 Build 377+
    #define SCHEMA_123          123 // Version 4 Build 398+
    #define SCHEMA_124          124 // Version 4 Build 414+
    #define SCHEMA_125          125 // Version 4 Build 415+
    #define SCHEMA_126          126 // Version 4 Build 456+
    #define SCHEMA_127          127 // Version 4 Build 478+
    #define SCHEMA_128          128 // Version 4 Build 479+
    #define SCHEMA_129          129 // Version 4 Build 482+
    #define SCHEMA_130          130 // Version 4 Build 487+
    #define SCHEMA_131          131 // Version 4 Build 499+
    #define SCHEMA_132          132 // Version 4 Build 505+
    #define SCHEMA_133          133 // Version 4 Build 513+
    #define SCHEMA_134          134 // Version 4 Build 514+
    #define SCHEMA_135          135 // Version 4 Build 519+
    #define SCHEMA_136          136 // Version 4 Build 527+
    #define SCHEMA_137          137 // Version 4 Build 532+
    #define SCHEMA_138          138 // Version 4 Build 532+
    #define SCHEMA_139          139 // Version 4 Build 537+
    #define SCHEMA_140          140 // Version 4 Build 540+
    #define SCHEMA_141          141 // Version 4 Build 560+
    #define SCHEMA_142          142 // Version 4 Build 566+
    #define SCHEMA_143          143 // Version 4 Build 567+
    #define SCHEMA_144          144 // Version 4 Build 579+
    #define SCHEMA_145          145 // Version 4 Build 600+
    #define SCHEMA_146          146 // Version 4 Build 601+
    #define SCHEMA_147          147 // Version 4 Build 609+
    #define SCHEMA_148          148 // Version 4 Build 650+
    #define SCHEMA_149          149 // Version 4 Build 656+
    #define SCHEMA_150          150 // Version 4 Build 657+
    #define SCHEMA_151          151 // Version 4 Build 668+
    #define SCHEMA_152          152 // Version 4 Build 695+
    #define SCHEMA_153          153 // Version 4 Build 706+
    #define SCHEMA_154          154 // Version 4 Build 708+
    #define SCHEMA_155          155 // Version 4 Build 709+
    #define SCHEMA_156          156 // Version 4 Build 709+
    #define SCHEMA_157          157 // Version 4 Build 739+
    #define SCHEMA_158          158 // Version 4 Build 761+
    #define SCHEMA_159          159 // Version 4 Build 765+
    #define SCHEMA_160          160 // Version 4 Build 765+
    #define SCHEMA_161          161 // Version 4 Build 766+
    #define SCHEMA_162          162 // Version 4 Build 772+
    #define SCHEMA_163          163 // Version 4 Build 773+
    #define SCHEMA_164          164 // Version 4 Build 777+
    #define SCHEMA_165          165 // Version 4 Build 789+
    #define SCHEMA_166          166 // Version 4 Build 797+
    #define SCHEMA_167          167 // Version 4 Build 801+
    #define SCHEMA_168          168 // Version 4 Build 806+
    #define SCHEMA_169          169 // Version 4 Build 807+
    #define SCHEMA_170          170 // Version 4 Build 821+
    #define SCHEMA_171          171 // Version 4 Build 836+
    #define SCHEMA_172          172 // Version 4 Build 837+
    #define SCHEMA_173          173 // Version 4 Build 838+
    #define SCHEMA_174          174 // Version 4 Build 838+
    #define SCHEMA_175          175 // Version 4 Build 848+
    #define SCHEMA_176          176 // Version 4 Build 858+
    #define SCHEMA_177          177 // Version 4 Build 869+
    #define SCHEMA_178          178 // Version 4 Build 871+
    #define SCHEMA_179          179 // Version 4 Build 880+
    #define SCHEMA_180          180 // Version 4 Build 885+
    #define SCHEMA_181          181 // Version 4 Build 891+
    #define SCHEMA_182          182 // Version 4 Build 892+
    #define SCHEMA_183          183 // Version 4 Build 893+
    #define SCHEMA_184          184 // Version 4 Build 893+
    #define SCHEMA_185          185 // Version 4 Build 894+
    #define SCHEMA_186          186 // Version 4 Build 897+
    #define SCHEMA_187          187 // Version 4 Build 899+
    #define SCHEMA_188          188 // Version 4 Build 902+
    #define SCHEMA_189          189 // Version 4 Build 902+
    #define SCHEMA_190          190 // Version 4 Build 914+
    #define SCHEMA_191          191 // Version 4 Build 922+
    #define SCHEMA_192          192 // Version 4 Build 933+
    #define SCHEMA_193          193 // Version 4 Build 943+
    #define SCHEMA_194          194 // Version 4 Build 952+
    #define SCHEMA_195          195 // Version 4 Build 959+
    #define SCHEMA_196          196 // Version 4 Build 979+
    #define SCHEMA_197          197 // Version 4 Build 989+
    #define SCHEMA_198          198 // Version 4 Build 991+
    #define SCHEMA_199          199 // Version 4 Build 1002+
    #define SCHEMA_200          200 // Version 4 Build 1018+
    #define SCHEMA_201          201 // Version 4 Build 1035+
    #define SCHEMA_202          202 // Version 4 Build 1041+
    #define SCHEMA_203          203 // Version 4 Build 1041+
    #define SCHEMA_204          204 // Version 4 Build 1068+
    #define SCHEMA_205          205 // Version 4 Build 1071+
    #define SCHEMA_206          206 // Version 4 Build 1072+
    #define SCHEMA_207          207 // Version 4 Build 1076+
    #define SCHEMA_208          208 // Version 4 Build 1079+
    #define SCHEMA_209          209 // Version 4 Build 1098+
    #define SCHEMA_210          210 // Version 4 Build 1100+
    #define SCHEMA_211          211 // Version 4 Build 1101+
    #define SCHEMA_212          212 // Version 4 Build 1102+
    #define SCHEMA_213          213 // Version 4 Build 1119+
    #define SCHEMA_214          214 // Version 4 Build 1121+
    #define SCHEMA_215          215 // Version 4 Build 1133+
    #define SCHEMA_216          216 // Version 4 Build 1134+
    #define SCHEMA_217          217 // Version 4 Build 1138+
    #define SCHEMA_218          218 // Version 4 Build 1164+
    #define SCHEMA_219          219 // Version 4 Build 1174+
    #define SCHEMA_220          220 // Version 4 Build 1185+
    #define SCHEMA_221          221 // Version 4 Build 1215+
    #define SCHEMA_222          222 // Version 4 Build 1234+
    #define SCHEMA_223          223 // Version 4 Build 1236+
    #define SCHEMA_224          224 // Version 4 Build 1237+
    #define SCHEMA_225          225 // Version 4 Build 1241+
    #define SCHEMA_226          226 // Version 4 Build 1248+
    #define SCHEMA_227          227 // Version 4 Build 1251+
    #define SCHEMA_228          228 // Version 4 Build 1260+
    #define SCHEMA_229          229 // Version 4 Build 1271+
    #define SCHEMA_230          230 // Version 4 Build 1286+
    #define SCHEMA_231          231 // Version 4 Build 1289+
    #define SCHEMA_232          232 // Version 4 Build 1295+
    #define SCHEMA_233          233 // Version 4 Build 1312+
    #define SCHEMA_234          234 // Version 4 Build 1319+
    #define SCHEMA_235          235 // Version 4 Build 1324+
    #define SCHEMA_236          236 // Version 4 Build 1326+
    #define SCHEMA_237          237 // Version 4 Build 1329+
    #define SCHEMA_238          238 // Version 4 Build 1329+
    #define SCHEMA_239          239 // Version 4 Build 1329+
    #define SCHEMA_240          240 // Version 4 Build 1331+
    #define SCHEMA_241          241 // Version 4 Build 1331+
    #define SCHEMA_242          242 // Version 4 Build 1335+
    #define SCHEMA_243          243 // Version 4 Build 1337+
    #define SCHEMA_244          244 // Version 4 Build 1337+
    #define SCHEMA_245          245 // Version 4 Build 1343+
    #define SCHEMA_246          246 // Version 4 Build 1356+
    #define SCHEMA_247          247 // Version 4 Build 1357+
    #define SCHEMA_248          248 // Version 4 Build 1358+
    #define SCHEMA_249          249 // Version 4 Build 1372+
    #define SCHEMA_250          250 // Version 4 Build 1373+
    #define SCHEMA_251          251 // Version 4 Build 1377+
    #define SCHEMA_252          252 // Version 4 Build 1386+
    #define SCHEMA_253          253 // Version 4 Build 1415+
    #define SCHEMA_254          254 // Version 4 Build 1416+
    #define SCHEMA_255          255 // Version 4 Build 1420+
    #define SCHEMA_256          256 // Version 4 Build 1424+
    #define SCHEMA_257          257 // Version 4 Build 1425+
    #define SCHEMA_258          258 // Version 4 Build 1422+
    #define SCHEMA_259          259 // Version 4 Build 1453 5 Build 35
    #define SCHEMA_260          260 // Version 4 Build 1460 5 Build 38
    #define SCHEMA_261          261 // Version 4 Build 1481 5 Build 47
    #define SCHEMA_262          262 // Version 4 Build 1489 5 Build 49
    #define SCHEMA_263          263 // Version 4 Build 1510 5 Build 85
    #define SCHEMA_264          264 // Version 4 Build 1513 5 Build 94
    #define SCHEMA_265          265 // Version 4 Build 1529 5 Build 105
    #define SCHEMA_266          266 // Version 4 Build 1535 5 Build 109
    #define SCHEMA_267          267 // Version 4 Build 1538 5 Build 121
    #define SCHEMA_268          268 // Version 4 Build 1543 5 Build 147
    #define SCHEMA_269          269 // Version 4 Build 1550 5 Build 161
    #define SCHEMA_270          270 // Version 4 Build 1550 5 Build 162
    #define SCHEMA_271          271 // Version 4 Build 1564 5 Build 179
    #define SCHEMA_272          272 // Version 4 Build 1570 5 Build 187
    #define SCHEMA_273          273 // Version 4 Build 1570 5 Build 187
    #define SCHEMA_274          274 // Version 4 Build 1571 5 Build 191
    #define SCHEMA_275          275 // Version 4 Build 1572 5 Build 193
    #define SCHEMA_276          276 // Version 4 Build 1576 5 Build 276
    #define SCHEMA_277          277 // Version 4 Build 1586 5 Build 276
    #define SCHEMA_278          278 // Version 4 Build 1586 5 Build 276
    #define SCHEMA_279          279 // Version 4 Build 1586 5 Build 288
    #define SCHEMA_280          280 // Version 4 Build 1588 5 Build 304
    #define SCHEMA_281          281 // Version 4 Build 1590 5 Build 337
    #define SCHEMA_282          282 // Version 4 Build 1589 5 Build 352
    #define SCHEMA_283          283 // Version 4 Build 1590 5 Build 354
    #define SCHEMA_284          284 // Version 4 Build 1592 5 Build 357
    #define SCHEMA_285          285 // Version 4 Build 1592 5 Build 357
    #define SCHEMA_286          286 // Version 4 Build 1593 5 Build 410
    #define SCHEMA_287          287 // Version 4 Build 1600 5 Build 413
    #define SCHEMA_288          288 // Version 4 Build 1601 5 Build 422
    #define SCHEMA_289          289 // Version 4 Build 1602 5 Build 441
    #define SCHEMA_290          290 // Version 4 Build 1602 5 Build 444
	#define SCHEMA_291			291 // Version 4 Build 1605 5 Build 456
	#define SCHEMA_292			292 // Version 4 Build 1606 5 Build 487
	#define SCHEMA_293			293 // Version 4 Build 1606 5 Build 487
	#define SCHEMA_294			294 // Version 4 Build 1606 5 Build 491
	#define SCHEMA_295			295 // Version 4 Build 1606 5 Build 491
	#define SCHEMA_296			296 // Version 4 Build 1606 5 Build 491
	#define SCHEMA_297			297 // Version 4 Build 1606 5 Build 526
	#define SCHEMA_298			298 // Version 4 Build 1606 5 Build 527
	#define SCHEMA_299			299 // Version 4 Build 1606 5 Build 542
	#define SCHEMA_300          300 // Version 4 Build 1606 5 Build 552
	#define SCHEMA_301          301 // Version 4 Build 1606 5 Build 552
	#define SCHEMA_302          302 // Version 4 Build 1606 5 Build 560
	#define SCHEMA_303          303 // Version 4 Build 1606 5 Build 567
	#define SCHEMA_304          304 // Version 4 Build 1606 5 Build 568
	#define SCHEMA_305          305 // Version 4 Build 1606 5 Build 580
	#define SCHEMA_306          306 // Version 4 Build 1606 5 Build 606
	#define SCHEMA_307          307 // Version 4 Build 1606 5 Build 618
	#define SCHEMA_308          308 // Version 5.1.92
	#define SCHEMA_309          309 // Version 5.1.93
	#define SCHEMA_310          310 // Version 5.1.97
	#define SCHEMA_311          311 // Version 5.1.103
	#define SCHEMA_312          312 // Version UNICODE
	#define SCHEMA_313          313 // Version UNICODE
	#define SCHEMA_314          314 // Version 5.1.147
	#define SCHEMA_315          315 // Version 5.1.156
	#define SCHEMA_316          316 // Version 5.1.164
  	#define SCHEMA_317          317 // Version 5.1.184
    #define SCHEMA_318          318 // Version 5.1.196
    #define SCHEMA_319          319 // Version 5.1.209
    #define SCHEMA_320          320 // Version 5.1.258
    #define SCHEMA_321          321 // Version 5.1.258
    #define SCHEMA_322          322 // Version 5.1.268
    #define SCHEMA_323          323 // Version 5.1.278
    #define SCHEMA_324          324 // Version 5.1.278
    #define SCHEMA_325          325 // Version 5.1.283
    #define SCHEMA_326          326 // Version 5.1.283
    #define SCHEMA_327          327 // Version 5.1.304
    #define SCHEMA_328          328 // Version 5.1.304
    #define SCHEMA_329          329 // Version 5.1.337
    #define SCHEMA_330          330 // Version 5.1.342
    #define SCHEMA_331          331 // Version 5.1.353
    #define SCHEMA_332          332 // Version 5.1.355
    #define SCHEMA_333          333 // Version 5.1.357
    #define SCHEMA_334          334 // Version 5.1.366
    #define SCHEMA_335          335 // Version 5.1.386
    #define SCHEMA_336          336 // Version 5.1.396
    #define SCHEMA_337          337 // Version 5.1.398
    #define SCHEMA_338          338 // Version 5.1.415
    #define SCHEMA_339          339 // Version 5.1.452
    #define SCHEMA_340          340 // Version 5.1.486
    #define SCHEMA_341          341 // Version 5.1.497
    #define SCHEMA_342          342 // Version 5.1.504
    #define SCHEMA_343          343 // Version 5.1.507
    #define SCHEMA_344          344 // Version 5.1.547
    #define SCHEMA_345          345 // Version 5.1.564
    #define SCHEMA_346          346 // Version 5.3.8
    #define SCHEMA_347          347 // Version 5.3.19
    #define SCHEMA_348          348 // Version 5.2.623, 5.3.89
    #define SCHEMA_349          349 // 5.3.105
    #define SCHEMA_350          350 // 5.3.105
    #define SCHEMA_351          351 // 5.3.105
    #define SCHEMA_352          352 // 5.3.118
    #define SCHEMA_353          353 // 5.3.119
    #define SCHEMA_354          354 // 5.3.119
//  #define SCHEMA_355          355 // Version 5.4.5 (KNX support)
//  #define SCHEMA_356          356 // Version 5.4.25 (HVAC redesigned)
//  #define SCHEMA_357          357 // Version 5.4.46 (Bar Groups support)
//  #define SCHEMA_358          358 // Version 5.4.54 (KNX Scene)
//  #define SCHEMA_359          359 // Version 5.4.55 (KNX GA WORD -> int)
//  #define SCHEMA_360          360 // Version 5.4.55 (Volume Mapping)
//  #define SCHEMA_361          361 // Changed fixed volume
//  #define SCHEMA_362          362 // Version 5.4.73 (KNX TStat SP Shift level size)

    #define SCHEMA_LATEST       354

#endif

