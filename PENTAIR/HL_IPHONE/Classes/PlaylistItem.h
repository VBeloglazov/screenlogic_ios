//
//  PlaylistItem.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

@class CNSMSG;

/*============================================================================*/

@interface CPlaylistItem : NSObject 

/*============================================================================*/
{
    NSString                            *m_pArtist;
    NSString                            *m_pAlbum;
    NSString                            *m_pTrack;
    int                                 m_iTrack;
    int                                 m_iLen;
    int                                 m_iState;
}

@property (nonatomic, retain) NSString *m_pArtist;
@property (nonatomic, retain) NSString *m_pAlbum;
@property (nonatomic, retain) NSString *m_pTrack;
@property (nonatomic)         int       m_iTrack;
@property (nonatomic)         int       m_iLen;
@property (nonatomic)         int       m_iState;

-(id)initWithMSG:(CNSMSG*)pMSG;

-(BOOL)IsSameItem:(CPlaylistItem*)pOther;

@end
