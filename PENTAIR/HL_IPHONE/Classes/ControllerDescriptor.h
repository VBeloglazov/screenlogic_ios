//
//  ControllerDescriptor.h
//  XPAD_VIEW
//
//  Created by dmitriy on 3/16/14.
//
//

#import <Foundation/Foundation.h>

@interface ControllerDescriptor : NSObject

    @property NSString *systemName;
    @property NSString *nickName;
    @property NSString *ipAddress;
    @property NSString *password;
    @property NSInteger portName;
    @property NSInteger lastConnection;
    @property NSInteger connectionType;
@end
