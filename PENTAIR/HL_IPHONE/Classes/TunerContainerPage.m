//
//  TunerContainerPage.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/23/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "TunerContainerPage.h"
#import "AudioZone.h"
#import "hlm.h"
#import "TunerPage_AMFM.h"
#import "TunerPage_XM.h"
#import "TableTitleHeaderView.h"
#import "hlsegment.h"
#import "hlbutton.h"
#import "crect.h"
#import "MainView.h"
#import "NSTunerControl.h"
#import "TunerFavsPage.h"
#import "RoundRegion.h"
#import "hlcomm.h"
#import "CustomPage.h"

#define PAGE_TYPE_NONE              0
#define PAGE_TYPE_AMFM              1
#define PAGE_TYPE_SATELLITE         2

@implementation CTunerContainerPage

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone MediaZonePage:(CMediaZonePage*)pMediaZonePage

/*============================================================================*/
{
    self = [super initWithFrame:rect Style:SRREGION_MAIN_WINDOW];
    if (self) 
    {
        m_pZone = pZone;
        m_pMediaZonePage = pMediaZonePage;

        m_iActivePageType = PAGE_TYPE_NONE;
        [m_pZone AddSink:self];
        
        m_pHeaderView = [[CTunerHeaderView alloc] initWithFrame:CGRectZero Zone:m_pZone Container:self];
        [m_pRgn1 SetClientView:m_pHeaderView];
        [m_pHeaderView release];

        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iThisPageType = PAGE_TYPE_NONE;
    int iTunerState = [m_pZone TunerState];
    switch (iTunerState)
    {
    case TUNER_AM:
    case TUNER_FM:
        iThisPageType = PAGE_TYPE_AMFM;
        break;

    case TUNER_SIRIUS:
    case TUNER_XM:
        iThisPageType = PAGE_TYPE_SATELLITE;
        break;
    }

    if (iThisPageType == m_iActivePageType)
        return;
    
//    CTunerStatusView *pStatus = [m_pHeaderView StatusView];

    CTunerContainerPage *pContainer = self;
    if (m_bInit)
        pContainer = NULL;

    UIView *pNewView = NULL;
    m_iActivePageType = iThisPageType;
    pNewView = [[CTunerFavsPage alloc] initWithFrame:CGRectZero Zone:m_pZone Container:pContainer];
    [m_pHeaderView SelectPage:0];

/*
    switch (iThisPageType)
    {
    case PAGE_TYPE_AMFM:
        pNewView = [[CTunerPageAMFM alloc] initWithFrame:CGRectZero Zone:m_pZone StatusView:pStatus];
        break;

    case PAGE_TYPE_SATELLITE:
        pNewView = [[CTunerPageXM alloc] initWithFrame:CGRectZero Zone:m_pZone];
        break;
    }
*/    

    [m_pRgn2 SetClientView:pNewView];
    [pNewView release];
    
}
/*============================================================================*/

-(void)SelectPage:(id)sender

/*============================================================================*/
{
//	[UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.35];
//    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:m_pMainPage cache:YES];

    CHLSegment *pCtrl = (CHLSegment*)sender;
    int iIndex = [pCtrl SelectedIndex];

    UIView *pNewView = NULL;
    if (iIndex == 0)
    {
        pNewView = [[CTunerFavsPage alloc] initWithFrame:CGRectZero Zone:m_pZone Container:NULL];
    }
    else
    {
        switch (m_iActivePageType)
        {
        case PAGE_TYPE_AMFM:
            pNewView = [[CTunerPageAMFM alloc] initWithFrame:CGRectZero Zone:m_pZone StatusView:m_pHeaderView];
            break;
        case PAGE_TYPE_SATELLITE:
            pNewView = [[CTunerPageXM alloc] initWithFrame:CGRectZero Zone:m_pZone MediaZonePage:m_pMediaZonePage];
            break;
        }
    }

    int iDir = DIR_LEFT;
    if (iIndex == 0)
        iDir = DIR_RIGHT;

    [m_pRgn2 SetClientViewAnimated:pNewView Direction:iDir];
    [pNewView release];
}
/*============================================================================*/

-(void)NotifyEmptyFavorites

/*============================================================================*/
{
    [m_pHeaderView SelectPage:1];
    [self SelectPage:[m_pHeaderView SegmentControl]];
}
@end




@implementation CTunerHeaderView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Zone:(CAudioZone*)pZone Container:(CTunerContainerPage*)pContainer

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pContainer = pContainer;
        m_pZone = pZone;

        m_iActivePageType = PAGE_TYPE_NONE;
        [m_pZone AddSink:self];
        [self Notify:0 Data:0];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(CTunerStatusView*)StatusView

/*============================================================================*/
{
    return m_pStatusView;
}
/*============================================================================*/

-(void)SetStationText:(NSString*)pText

/*============================================================================*/
{
    [m_pStatusView SetStationText:pText];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iThisPageType = PAGE_TYPE_NONE;
    int iTunerState = [m_pZone TunerState];
    BOOL bBandButton = FALSE;

    switch (iTunerState)
    {
    case TUNER_AM:
    case TUNER_FM:
        iThisPageType = PAGE_TYPE_AMFM;
        bBandButton = TRUE;
        break;

    case TUNER_SIRIUS:
    case TUNER_XM:
        iThisPageType = PAGE_TYPE_SATELLITE;
        break;
    }

    int iBasicTunerType = ([m_pZone ActiveSubSource] & 0x0000FFFF);
    if (iBasicTunerType == AUX_SOURCE_TUNER_AMFMXM)
        bBandButton = TRUE;
    if (iBasicTunerType == AUX_SOURCE_TUNER_AMFMSIRIUS)
        bBandButton = TRUE;


    if (iThisPageType == m_iActivePageType)
        return;

    m_iActivePageType = iThisPageType;

    [m_pBandButton removeFromSuperview];
    [m_pFavButton removeFromSuperview];
    [m_pSegment removeFromSuperview];
    [m_pStatusView removeFromSuperview];
    m_pBandButton = NULL;
    m_pFavButton = NULL;
    m_pSegment = NULL;
    m_pStatusView = NULL;

    if (bBandButton)
    {
        m_pBandButton = [[CHLButton alloc] initWithFrame:CGRectZero Text:@"BAND"];
        [self addSubview:m_pBandButton];
        [m_pBandButton release];
        [m_pBandButton addTarget:self action:@selector(NextBand) forControlEvents:UIControlEventTouchDown];

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
            [m_pBandButton SetTextSize:10];
    }

    NSMutableArray *pItems = [[[NSMutableArray alloc] init] autorelease];
    [pItems addObject:[[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"FAVORITES.png" Text:@"FAVORITES"]];

    if (iThisPageType == PAGE_TYPE_AMFM)
        [pItems addObject:[[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"KEYPAD.png" Text:@"KEYPAD"]];
    else
        [pItems addObject:[[CHLSegmentIconView alloc] initWithFrame:CGRectZero Icon:@"KEYPAD.png" Text:@"STATIONS"]];
    
    int iRad = [CHLComm GetInt:INT_TAB_ROUT];
    int iCorners = (1<<CORNER_UL);
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        iRad = 0;
        iCorners = 0;
    }

    m_pSegment = [[CHLSegment alloc] initWithFrame:CGRectZero ImageProvider:[m_pContainer UpperRegion] Views:pItems Type:HLSEGMENT_PAGE_CTRL_ROUNDREGION];
    [self addSubview:m_pSegment];
    [m_pSegment release];
    [m_pSegment addTarget:m_pContainer action:@selector(SelectPage:) forControlEvents:UIControlEventValueChanged];
    
    m_pStatusView = [[CTunerStatusView alloc] initWithFrame:CGRectZero Zone:m_pZone];
    [self addSubview:m_pStatusView];
    [m_pStatusView release];
}
/*============================================================================*/

-(void)SelectPage:(int)iIndex

/*============================================================================*/
{
    [m_pSegment SetSelectedIndex:iIndex];
}
/*============================================================================*/

-(CHLSegment*)SegmentControl

/*============================================================================*/
{
    return m_pSegment;
}
/*============================================================================*/

-(void)NextBand

/*============================================================================*/
{
    NSTunerControl *pCtrl = [[[NSTunerControl alloc] init] autorelease];
    int iNextBand = [pCtrl NextBand:[m_pZone TunerState] Type:[m_pZone ActiveSubSource]];
    [m_pZone ZoneCommand:HL_PLAYLIST_SETTUNERMODE Data1:iNextBand Data2:0];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;
    int iDXItem = 50;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDXItem = 150;
        
    if (m_pBandButton != NULL)
    {
        CGRect rBand = [CRect BreakOffRight:&rBounds DX:iDXItem];
        [m_pBandButton setFrame:rBand];
    }
    if (m_pSegment != NULL)
    {
        CGRect rSegment = [CRect BreakOffLeft:&rBounds DX:2*iDXItem];
        [m_pSegment setFrame:rSegment];
    }

    [CRect Inset:&rBounds DX:0 DY:2];
    [m_pStatusView setFrame:rBounds];
}

@end


@implementation CTunerStatusView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Zone:(CAudioZone *)pZone

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pChannelText = [self AddLabel];

        BOOL bChannelName = FALSE;
        BOOL bMetaData = FALSE;

        int iTunerState = [pZone TunerState];
        switch (iTunerState)
        {
        case TUNER_AM:
        case TUNER_FM:
            {
                int iTunerType = [pZone ActiveSubSource];
                int iBasicTunerType = iTunerType & 0x0000FFFF;
                if (iBasicTunerType == AUX_SOURCE_TUNER_HDAMFM)
                    bMetaData = TRUE;
                if (iTunerType & AUX_SOURCE_TUNER_LONG_RDS_RT)
                    bMetaData = TRUE;
            }
            break;

        case TUNER_XM:
        case TUNER_SIRIUS:
            {
                bMetaData = TRUE;
                bChannelName = TRUE;
            }
            break;
        }

        if (bChannelName)
            m_pChannelName = [self AddLabel];

        if (bMetaData)
        {
            m_pArtistText = [self AddLabel];
            m_pTrackText = [self AddLabel];
        }

        m_pZone = pZone;
        [m_pZone AddSink:self];
        [self Notify:0 Data:0];
        
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    NSTunerControl *pCtrl = [[[NSTunerControl alloc] init] autorelease];
    int iState = [m_pZone TunerState];
    int iSubSource = [m_pZone ActiveSubSource];
    NSString *pText = [pCtrl ChannelText:[m_pZone TunerStation] State:iState Type:iSubSource StationName:@""];
    [m_pChannelText setText:pText];
    
    [m_pChannelName setText:[m_pZone TunerStationText]];
    [m_pArtistText setText:[m_pZone TunerArtist]];
    [m_pTrackText setText:[m_pZone TunerTrack]];
}
/*============================================================================*/

-(UILabel*)AddLabel

/*============================================================================*/
{
    UILabel *pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [CCustomPage InitStandardLabel:pLabel Size:[CMainView TEXT_SIZE_SIDEBAR]];
    [self addSubview:pLabel];
    [pLabel release];
    return pLabel;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = self.bounds;

    CGRect rChannel     = CGRectZero;
    CGRect rChannelName = CGRectZero;
    CGRect rArtist      = CGRectZero;
    CGRect rTrack       = CGRectZero;

    int iDYMeta = [CMainView TEXT_SIZE_SIDEBAR];
    int iDXChannelSat = 80;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        iDXChannelSat = 150;

    int iTunerState = [m_pZone TunerState];
    switch (iTunerState)
    {
    case TUNER_AM:
    case TUNER_FM:
        {
            if (m_pTrackText != NULL)
                rTrack = [CRect BreakOffBottom:&rBounds DY:iDYMeta];
            if (m_pArtistText != NULL)
                rArtist = [CRect BreakOffBottom:&rBounds DY:iDYMeta];
            if (m_pChannelName != NULL)
                rChannelName = [CRect BreakOffBottom:&rBounds DY:iDYMeta];
            rChannel = rBounds;
        }
        break;

    case TUNER_XM:
    case TUNER_SIRIUS:
        {
            rChannel = [CRect BreakOffLeft:&rBounds DX:iDXChannelSat];

            if (m_pTrackText != NULL)
                rTrack = [CRect BreakOffBottom:&rBounds DY:iDYMeta];
            if (m_pArtistText != NULL)
                rArtist = [CRect BreakOffBottom:&rBounds DY:iDYMeta];
            rChannelName = rBounds;
        }
        break;
    }

    int iTextSize = MIN([CMainView TEXT_SIZE_TITLE], rChannel.size.height);
    [CCustomPage InitStandardLabel:m_pChannelText Size:iTextSize];

    [m_pChannelText setFrame:rChannel];
    [m_pChannelName setFrame:rChannelName];
    [m_pArtistText setFrame:rArtist];
    [m_pTrackText setFrame:rTrack];
}
/*============================================================================*/

-(void)SetStationText:(NSString*)pText

/*============================================================================*/
{
    [m_pChannelText setText:pText];
}
@end
