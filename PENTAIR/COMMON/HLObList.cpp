/*
--------------------------------------------------------------------------------

    HLOBLIST.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "hlpos.h"

#include "hloblist.h"


/*========================================================================*/

    CHLObList::CHLObList()

/*========================================================================*/
{
    m_pStart    = NULL;
    m_pEnd      = NULL;
    m_iCount    = 0;
}
/*========================================================================*/

    CHLObList::~CHLObList()

/*========================================================================*/
{
    RemoveAll();
}
/*========================================================================*/

    BOOL                        CHLObList::IsEmpty()

/*========================================================================*/
{
    return (m_pStart == NULL);
}
/*========================================================================*/

    int                         CHLObList::GetCount()

/*========================================================================*/
{
    return m_iCount;
}
/*========================================================================*/

    void                        *CHLObList::GetHead()

/*========================================================================*/
{
    if (m_pStart == NULL)
        return NULL;
    return m_pStart->Object();
}
/*========================================================================*/

    void                        *CHLObList::GetTail()

/*========================================================================*/
{
    if (m_pEnd == NULL)
        return NULL;
    return m_pEnd->Object();
}
/*========================================================================*/

    CHLPos                       *CHLObList::GetHeadPosition()

/*========================================================================*/
{
    return m_pStart;
}
/*========================================================================*/

    CHLPos                       *CHLObList::GetTailPosition()

/*========================================================================*/
{
    return m_pEnd;
}
/*========================================================================*/

    void                        *CHLObList::GetAt(
    CHLPos                       *pOP )

/*========================================================================*/
{
    if (pOP == NULL)
        return NULL;

    return pOP->Object();
}
/*========================================================================*/

    void                        *CHLObList::GetNext(
    CHLPos                      *&pOP )

/*========================================================================*/
{
    void *pR = pOP->Object();
    pOP = pOP->Next();
    return pR;
}
/*========================================================================*/

    void                        *CHLObList::GetPrev(
    CHLPos                      *&pOP )

/*========================================================================*/
{
    void *pR = pOP->Object();
    pOP = pOP->Prev();
    return pR;
}
/*========================================================================*/

    CHLPos                      *CHLObList::Find(
    void                        *pObj )

/*========================================================================*/
{
    CHLPos *pOP = m_pStart;
    while (pOP != NULL)
    {
        if (pOP->Object() == pObj)
            return pOP;
        pOP = pOP->Next();
    }

    return NULL;
}
/*========================================================================*/

    CHLPos                      *CHLObList::FindIndex(
    int                         iIndex )

/*========================================================================*/
{
    if (iIndex < (m_iCount / 2))
    {
        CHLPos *pOP = m_pStart;
        int iCount = 0;

        while (pOP != NULL && iCount < iIndex)
        {
            iCount++;
            pOP = pOP->Next();
        }

        return pOP;
    }
    else
    {
        CHLPos *pOP = m_pEnd;
        int iCount = m_iCount-1;

        if (iIndex >= m_iCount)
            return NULL;

        while (pOP != NULL && iCount > iIndex)
        {
            iCount--;
            pOP = pOP->Prev();
        }

        return pOP;
    }
}
/*========================================================================*/

    void                        CHLObList::AddHead(
    void                        *pObj )

/*========================================================================*/
{
    CHLPos *pOP = m_pStart;

    CHLPos *pNewStart = new CHLPos(pObj);
    m_pStart = pNewStart;
    pNewStart->Next(pOP);
    if (pOP != NULL)
        pOP->Prev(pNewStart);
    if (m_pEnd == NULL)
        m_pEnd = pNewStart;

    m_iCount++;
}
/*========================================================================*/

    void                        CHLObList::AddTail(
    void                        *pObj )

/*========================================================================*/
{
    CHLPos *pOP = m_pEnd;

    CHLPos *pNewEnd = new CHLPos(pObj);
    m_pEnd = pNewEnd;
    pNewEnd->Prev(pOP);
    if (pOP != NULL)
        pOP->Next(pNewEnd);
    if (m_pStart == NULL)
        m_pStart = pNewEnd;

    m_iCount++;
}
/*========================================================================*/

    void                        CHLObList::AddTail(
    void                        *pObj,
    int                         iSortIndex )

/*========================================================================*/
{
    AddTail(pObj);
}
/*========================================================================*/

    void                        CHLObList::InsertBefore(
    CHLPos                      *pOP,
    void                        *pObj )

/*========================================================================*/
{
    // If pOP is NULL, just add this to the back.
    if (pOP == NULL)
    {
        AddTail(pObj);
        return;
    }

    // Should not be here if list is empty.
    ASSERT(m_pStart != NULL);
    ASSERT(m_pEnd != NULL);
    ASSERT(m_iCount != 0);

    // Add this item: handle the previous item.
    CHLPos *pNew = new CHLPos(pObj);
    CHLPos *pOldPrev = pOP->Prev();
    if (pOldPrev == NULL)
    {
        ASSERT(m_pStart == pOP);
        m_pStart = pNew;
        pNew->Prev(NULL);
    }
    else
    {
        pOldPrev->Next(pNew);
        pNew->Prev(pOldPrev);
    }

    // Then handle the next item.
    pNew->Next(pOP);
    pOP->Prev(pNew);

    m_iCount++;
}
/*========================================================================*/

    void                        CHLObList::InsertAfterObject(
    CHLObList                   *pNewItems,
    void                        *pObj )

/*========================================================================*/
{
    CHLPos *pPosNew = pNewItems->GetHeadPosition();

    CHLPos *pPos = Find(pObj);
    if (pPos == NULL)
    {
        // Couldn't find the guy, just add to the end
        while (pPosNew != NULL)
        {
            void *pObj = pPosNew->Object();
            pPosNew = pPosNew->Next();
            AddTail(pObj);
        }
    
        m_iCount += pNewItems->GetCount();

        return;
    }

    CHLPos *pPrev = pPos;
    while (pPosNew != NULL)
    {
        void *pObj = pPosNew->Object();
        pPosNew = pPosNew->Next();

        CHLPos *pInsert = new CHLPos(pObj);
        pInsert->Prev(pPrev);
        pInsert->Next(pPrev->Next());
        pPrev->Next(pInsert);
        CHLPos *pNext = pInsert->Next();
        if (pNext != NULL)
            pNext->Prev(pInsert);
        else
            m_pEnd = pInsert;
        pPrev = pInsert;
    }

    m_iCount += pNewItems->GetCount();
}
/*========================================================================*/

    BOOL                        CHLObList::Contains(
    void                        *pObj )

/*========================================================================*/
{
    CHLPos *pOP = m_pStart;
    while (pOP != NULL)
    {
        if (pOP->Object() == pObj)
            return TRUE;
        pOP = pOP->Next();
    }

    return FALSE;
}
/*========================================================================*/

    void                        CHLObList::Remove(
    void                        *pObj )

/*========================================================================*/
{
    CHLPos *pOP = m_pStart;
    while (pOP != NULL)
    {
        if (pOP->Object() == pObj)
        {
            CHLPos *pA = pOP->Prev();
            CHLPos *pB = pOP->Next();

            if (pOP == m_pStart)
                m_pStart = pB;
            if (pOP == m_pEnd)
                m_pEnd = pA;

            if (pA != NULL)
                pA->Next(pB);
            if (pB != NULL)
                pB->Prev(pA);

            delete pOP;
            m_iCount--;

            return;
        }

        pOP = pOP->Next();
    }
}
/*========================================================================*/

    void                        *CHLObList::RemoveHead()

/*========================================================================*/
{
    if (m_pStart == NULL)
        return NULL;

    CHLPos *pOP = m_pStart;
    void *pReturn = pOP->Object();
    m_pStart = pOP->Next();
    if (m_pStart != NULL)
        m_pStart->Prev(NULL);
    if (m_pEnd == pOP)
        m_pEnd = NULL;

    delete pOP;
    m_iCount--;
    return pReturn;
}
/*========================================================================*/

    void                        *CHLObList::RemoveTail()

/*========================================================================*/
{
    if (m_pEnd == NULL)
        return NULL;

    CHLPos *pOP = m_pEnd;
    void *pReturn = pOP->Object();
    m_pEnd = pOP->Prev();
    if (m_pEnd != NULL)
        m_pEnd->Next(NULL);
    if (m_pStart == pOP)
        m_pStart = NULL;

    delete pOP;
    m_iCount--;
    return pReturn;
}
/*========================================================================*/

    void                        CHLObList::RemoveAt(
    CHLPos                      *pPos )

/*========================================================================*/
{
    if (pPos == NULL)
        return;
    CHLPos *pPrev = pPos->Prev();
    CHLPos *pNext = pPos->Next();
    if (pPrev != NULL)
        pPrev->Next(pNext);
    if (pNext != NULL)
        pNext->Prev(pPrev);

    if (pPos == m_pStart)
        m_pStart = pNext;
    if (pPos == m_pEnd)
        m_pEnd = pPrev;

    delete pPos;
    m_iCount--;
}
/*========================================================================*/

    void                        CHLObList::RemoveAll()

/*========================================================================*/
{
    CHLPos *pPos = m_pStart;
    while (pPos != NULL)
    {
        CHLPos *pLast = pPos;
        pPos = pPos->Next();
        delete pLast;
    }

    m_iCount    = 0;
    m_pStart    = NULL;
    m_pEnd      = NULL;
}
/*========================================================================*/

    CHLObList                    *CHLObList::CreateSortedList()

/*========================================================================*/
{
    return new CHLObList(*this);
}
