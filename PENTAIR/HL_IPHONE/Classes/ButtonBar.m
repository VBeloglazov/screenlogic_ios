//
//  ButtonBar.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 7/12/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "ButtonBar.h"
#import "shader.h"
#import "AppDelegate.h"
#import "hlm.h"
#import "hlcomm.h"
#import "hlbutton.h"


@implementation CButtonBar

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        m_pButtons = [[NSMutableArray alloc] init];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pCheckedColor release];
    [m_pButtons release];
    [super dealloc];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    float fDXThis = self.frame.size.width;
    float fDYThis = self.frame.size.height;
    float fDXBtn  = fDXThis / [m_pButtons count];
    for (int i = 0; i < [m_pButtons count]; i++)
    {
        CButtonBarButton *pBtn = (CButtonBarButton*)[m_pButtons objectAtIndex:i];
        CGRect rFrame = CGRectMake(i*fDXBtn, 0, fDXBtn, fDYThis);
        //DL
        //rFrame.origin.y += 20;
        
        [pBtn setFrame:rFrame];

        float fX0 = (float)i / (float)[m_pButtons count];
        float fDX = 1.0 / (float)[m_pButtons count];
        CGRect rContent = CGRectMake(fX0, 0, fDX, 1.0);
        
        //DL
        //rContent.origin.y += 20;
        
        [[pBtn layer] setContentsRect:rContent];
    }
}
/*============================================================================*/

-(void)SetCheckedColor:(UIColor*)pColor

/*============================================================================*/
{
    [m_pCheckedColor release];
    m_pCheckedColor = pColor;
    [m_pCheckedColor retain];
}
/*============================================================================*/

-(CButtonBarButton*)AddButtonText:(NSString*)pText Icon:(NSString*)pIcon

/*============================================================================*/
{
    CButtonBarButton *pBtn = [[CButtonBarButton alloc] initWithFrame:CGRectZero Text:pText];
    [pBtn SetIconEmbedded:pIcon];
    [m_pButtons addObject:pBtn];
    [self addSubview:pBtn];
    [pBtn release];

    if (m_pCheckedColor != NULL)
    {
        [pBtn SetCheckedColor:m_pCheckedColor];
    }

    return pBtn;
}
/*============================================================================*/

-(int)NItems

/*============================================================================*/
{
    return [m_pButtons count];
}
@end


@implementation CButtonBarButton

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame Text:(NSString*)pText Icon:(NSString*)pIcon

/*============================================================================*/
{
    self = [super initWithFrame:frame Text:pText];
    if (self) 
    {
        [self SetIconEmbedded:pIcon];
    }
    return self;
}
/*============================================================================*/

- (void)displayLayer:(CALayer *)layer

/*============================================================================*/
{
    int iState = ELEMENT_STD;
    if (m_bChecked)
        iState = ELEMENT_CHECK;
    CGImageRef pRef = [self ImageRefForState:iState];
    [layer setContents:(id)pRef];
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(CGImageRef)ImageRefForState:(int)iState

/*============================================================================*/
{
    CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *pSuperView = self.superview;
    int iDX = pSuperView.bounds.size.width;
    int iDY = pSuperView.bounds.size.height;
    int iSHIN    = [self ShadeIn];
    int iSHOUT   = [self ShadeOut];
    int iRad     = [self ROutside];
    int iGradient = [CHLComm GetInt:INT_BUTTON_GRADIENT];

    unsigned int rgbT = 0;
    unsigned int rgbB = 0;
    
    switch (iState)
    {
    case ELEMENT_STD:
        {
            rgbT = [CShader MakeRGB:[self RGBFace]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
        }
        break;
    case ELEMENT_FLASH:
        {
            rgbT = [CShader MakeRGB:[self RGBFlash]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
        }
        break;
    case ELEMENT_CHECK:
        {
            UIColor *prgbCheck = [CHLComm GetRGB:RGB_SELECT_SUBTLE];
            unsigned int rgbSel = [CShader MakeRGB:prgbCheck];
            int iOA1 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_TOP] * 100 / 255;
            int iOA2 = [CHLComm GetInt:INT_ALPHA_SUBTLE_SELECT_BTM] * 100 / 255;
            rgbT = [CShader MakeRGB:[self RGBFace]];
            rgbB = rgbT;
            [CShader MakeSpread:iGradient Color1:&rgbT Color2:&rgbB];
            rgbT = [CShader BlendPCT:iOA1 RGB1:rgbSel RGB2:rgbT];
            rgbB = [CShader BlendPCT:iOA2 RGB1:rgbSel RGB2:rgbB];
        }
        break;
    }

    return [pDelegate GetButtonImage:iDX DY:iDY Rad:iRad SHIN:iSHIN SHOUT:iSHOUT ColorT:rgbT ColorB:rgbB];
}

@end
