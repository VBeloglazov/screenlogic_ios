//
//  XPAD_VIEWViewController.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/3/08.
//  Copyright HomeLogic 2008. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"
#import "Sink.h"
#import "hlview.h"

@class CConnectPage;
@class CMainTab;
@class CHLButton;
@class CSystemModeList;
@class CHLToolBar;

/*============================================================================*/

@interface XPAD_VIEWViewController : UIViewController <     UINavigationBarDelegate,
                                                            UITableViewDelegate, 
                                                            UITableViewDataSource,
                                                            UIActionSheetDelegate,
                                                            UIScrollViewDelegate,
                                                            CNSSocketSink,
                                                            CSink,
                                                            CHLCommandResponder >

/*============================================================================*/
{
    BOOL                        m_bInit;
	CConnectPage                *m_pConnectPage;

    NSTimer                     *m_pTimer;
    NSTimer                     *m_pLogoTimer;
	NSMutableArray              *m_pMainTabs;
	IBOutlet UITableView        *m_pTableView;
    int                         m_iMainTabHeight;
    
    UILabel                     *m_pInfoText;
    UIAlertView                 *m_pLockView;
    
    CSystemModeList             *m_pModeList;

    CMainTab                    *m_pExpandedTab;
    int                         m_iExpandedTabIndex;

    CHLToolBar                  *m_pToolBar;
    
    UITextView                  *m_pTitleView;
    UIImageView                 *m_pBackground;
}

-(void)CheckConnectTimer;
-(void)HideLoginInfo;

  @property (nonatomic, retain) NSMutableArray *m_pMainTabs;
  @property (nonatomic, retain) UITableView *m_pTableView;

-(void)ShowConnectPage;
-(void)HideConnectPage;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;

-(CMainTab*)SelectedMainTab;

-(void)CollapseAll;
-(void)SetExpandedMode:(BOOL)bNew;
-(void)SetExpanded:(CMainTab*)pMainTab Expanded:(BOOL)bExpanded MainTabIndex:(int)iMainIndex;
-(int)FindMainTabIndex:(CMainTab*)pMainTab;
-(void)SetBackgroundImage:(int)iImage;

-(void)ShowSystemPage:(CMainTab*)pMainTab SubTabIndex:(int)iSubTabIndex;
-(CSystemModeList*)SysModeList;

+(int)BackgroundStyle;

@end
