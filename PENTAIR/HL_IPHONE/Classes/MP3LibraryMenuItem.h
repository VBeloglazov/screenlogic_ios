//
//  MP3LibraryMenuItem.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/23/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//


#import "MP3Menu.h"

/*============================================================================*/

@interface CMP3LibraryMenuItem : CMP3MenuItem <CNSSocketSink>

/*============================================================================*/
{
    CMP3Menu                            *m_pMenu;
    int                                 m_iSection;
    int                                 m_iRow;
    int                                 m_iIndex;
    int                                 m_iDataState;
    
    int                                 m_iArtistIndex;
    int                                 m_iAlbumIndex;
    int                                 m_iTrackIndex;
    
}

-(id)initWithData:(NSString*)pName Data:(int)iData Section:(int)iSection Row:(int)iRow Index:(int)iIndex Menu:(CMP3Menu*)pMenu;

-(void)ConnectionChanged:(BOOL)bNowConnected;
-(void)SinkMessage:(CNSMSG*)pMSG;
-(BOOL)OnIdleVisible;
-(NSString*)Text;
@end
