//
//  PoolPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 11/5/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#define POOLPAGE_MAIN           0
#define POOLPAGE_LIGHTS         1
#define POOLPAGE_SCHEDULE       2
#define POOLPAGE_HISTORY        3
#define POOLPAGE_EQUIPMENT      4

#import <UIKit/UIKit.h>
#import "SubTab.h"

@class CNSPoolConfig;
@class CSubSystemPage;
@class CMainTabViewController;

/*============================================================================*/

@interface CPoolSubTab : CSubTab
                                     

/*============================================================================*/
{
    CNSPoolConfig                       *m_pConfig;
}

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;

-(CSubSystemPage*)CreateUserPage:(CGRect)rFrame MainTabPage:(CMainTabViewController*)pMain;

@end
