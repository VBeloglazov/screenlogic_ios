//
//  tplaylist.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/2/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "tplaylist.h"
#import "AudioZone.h"
#import "PlaylistItem.h"
#import "PlaylistItemCell.h"
#import "hlm.h"
#import "tmp3.h"
#import "TableTitleHeaderView.h"
#import "HLTableFrame.h"
#import "MainView.h"
#import "RoundRegion.h"
#import "CustomPage.h"

@implementation CPlaylistView

#define INSETS_TOP                  0
#define INSETS_BOTTOM               ([CMP3InfoPage BUTTON_SIZE] + 2*[CMP3InfoPage SPACING] + 20)
#define ROW_HEIGHT                  26
#define TEXT_HEIGHT                 18

/*============================================================================*/

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    self = [super initWithFrame:rect DataSource:NULL Delegate:NULL];
    if (self)
    {
        [m_pTableView setDelegate:self];
        [m_pTableView setDataSource:self];
        m_pZone = pZone;
    
        [m_pZone AddSink:self];

        if ([m_pZone NPlaylistItems] < 1)
        {
            [self Notify:0 Data:SINK_ITEMSCHANGED];
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [m_pMessage release];
    [m_pZone RemoveSink:self];
    [super dealloc];
}
/*============================================================================*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
}
/*============================================================================*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 

/*============================================================================*/
{
    return 1;
}
/*============================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 

/*============================================================================*/
{
    return [m_pZone NPlaylistItems];
}
/*============================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 

/*============================================================================*/
{
    static NSString *CellIdentifier = @"Cell";

    if (indexPath == NULL)
        return NULL;

    CPlaylistItemCell *cell = (CPlaylistItemCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[CPlaylistItemCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }

    CPlaylistItem *pItem = [m_pZone PlaylistItem:indexPath.row];
    if (pItem == NULL)
        return NULL;
    [cell.textLabel setText:pItem.m_pTrack];
    [CCustomPage InitStandardLabel:cell.textLabel Size:TEXT_HEIGHT];
    [cell.textLabel setTextAlignment:UITextAlignmentLeft];

    if (indexPath.row == [m_pZone ActivePlaylistItem])
        [cell SetPosition:[m_pZone ProgressPercent]];
    else
        [cell SetPosition:-1];

    // Configure the cell
    return cell;
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    if (iData & SINK_ITEMSCHANGED)
    {
        if ([m_pZone NPlaylistItems] > 0)
        {
            if (m_pMessage != NULL)
            {
                [m_pMessage removeFromSuperview];
                [m_pMessage release];
                m_pMessage = NULL;
            }
        }
        else
        {
            if (m_pMessage == NULL)
            {
                CGRect rMessage = m_pTableView.bounds;
                rMessage.origin.y -= [CMP3InfoPage BUTTON_SIZE];
                m_pMessage = [[UILabel alloc] initWithFrame:rMessage];	// layoutSubViews will decide the final frame
                [CCustomPage InitStandardLabel:m_pMessage Size:18];
                [m_pMessage setText:@"Playlist is Empty"];
                [m_pTableView addSubview:m_pMessage];
            }
        }


        [m_pTableView reloadData];
    }
    if (iData & SINK_STATECHANGED)
    {
        int iActiveItemIndex = [m_pZone ActivePlaylistItem];

        NSIndexPath *pPathSel = [NSIndexPath indexPathForRow:iActiveItemIndex inSection:0];
        if ([m_pTableView numberOfSections] < 1)
            return;
        if ((iActiveItemIndex < 0) || (iActiveItemIndex >= [m_pTableView numberOfRowsInSection:0]))
            return;

        if (iActiveItemIndex != m_iLastActiveItemIndex)
        {
            m_iLastActiveItemIndex = iActiveItemIndex;

            CGRect rect = [m_pTableView rectForRowAtIndexPath:pPathSel];
            CGRect rScreen = rect;
            rScreen.origin.y -= m_pTableView.contentOffset.y;
            int iYB = rScreen.origin.y + rScreen.size.height;
            int iYMAX = m_pTableView.bounds.size.height - INSETS_BOTTOM;
            int iYMIN = INSETS_TOP;
            if (iYB > iYMAX)
                [m_pTableView setContentOffset:CGPointMake(0, rect.origin.y + rect.size.height - iYMAX) animated:YES];
            else if (rScreen.origin.y < iYMIN)
                [m_pTableView setContentOffset:CGPointMake(0, rect.origin.y - iYMIN) animated:YES];
            [m_pTableView deselectRowAtIndexPath:pPathSel animated:YES];
        }

//        printf("Active Item Index %d NItems in sec0 %d NPlaylist %d\n", iActiveItemIndex, [m_pTableView numberOfRowsInSection:0], [m_pZone NPlaylistItems]);

        int iPCT = [m_pZone ProgressPercent];
        for (int i = 0; i < [m_pZone NPlaylistItems]; i++)
        {
            NSIndexPath *pPath = [NSIndexPath indexPathForRow:i inSection:0];
            CPlaylistItemCell *pCell = (CPlaylistItemCell*) [m_pTableView cellForRowAtIndexPath:pPath];
            if (pCell != NULL)
            {
                if (i == iActiveItemIndex)
                    [pCell SetPosition:iPCT];
                else
                    [pCell SetPosition:-1];
            }
        }

    }
}
/*============================================================================*/

-(int)SelectedIndex

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    if (pPath == NULL)
        return -1;
    return pPath.row;
}
/*============================================================================*/

-(void)Deselect

/*============================================================================*/
{
    NSIndexPath *pPath = [m_pTableView indexPathForSelectedRow];
    if (pPath == NULL)
        return;
    [m_pTableView deselectRowAtIndexPath:pPath animated:YES];
}
@end
