/*
--------------------------------------------------------------------------------

    HLCOMM.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/

#import <AVFoundation/AVFoundation.h>

#ifdef __cplusplus
    class CHLString;
    class CStatusSink;
#else
    @class CHLString;
    @class CStatusSink;
#endif

@class CNSStatusSink;
@class CNSSocketSink;

@interface CHLComm : NSObject
{
    NSTimer                         *m_pTimer;
}

- (void) InitComm;
+ (BOOL) IsInit;
- (void) ShutDownComm;
- (void) TimerEvent;
- (void) applicationWillTerminate:(NSNotification*)aNotification;

+ (void) Disconnect;
+ (BOOL) Connected;
+ (BOOL) IsConnecting;
+ (BOOL) CreateRemoteConnection:(NSObject*)pSink Name:(const char*)pUserName Password:(const char*)pPassword;
+ (BOOL) ConnectViaDiscoveryService:(int)iConType Server:(int)iServer Name:(const char*)pUserName Password:(const char*)pPassword Sink:(CStatusSink*)pSink;
+ (BOOL) GetServersoftData:(int)iServer Address:(CHLString*)psAddress Port:(unsigned short*)pwPort;
+ (void) AddSocketSink:(id) pSink;
+ (void) RemoveSocketSink:(id) pSink;
+ (void) RemoveAllSocketSinks;
+ (int)  SenderID:(id) pSink;
+ (void) SuspendIO:(BOOL)bSuspsend;
+ (void) RunIO:(int)iMSEC;
+ (BOOL) WirelessOK;

+ (void) SetVersion:(int)iMajor Sub:(int)iMinor;
+ (int) VersionMajor;
+ (int) VersionMinor;

+ (BOOL) LoadSettings;
+ (void) LoadGradientTo:(CGFloat*)pArray G1:(int)iIndex1 G2:(int)iIndex2;
+ (UIColor*) GetRGB:(int)iIndex;
+ (UIColor*) GetRGB:(int)iIndex Adjust:(float)fAdjust;
+ (void) GetRGB:(int)iIndex To:(float*)pResult;
+ (unsigned int) GetRGBWin32:(int)iIndex;
+ (int) GetInt:(int)iIndex;
+ (NSString*)TranslateString:(NSString*)pText Context:(NSString*)pContext;
#ifndef PENTAIR
+ (AVAudioPlayer*)CreateButtonSoundPlayer;
#endif
@end
