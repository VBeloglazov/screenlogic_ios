//
//  SubTab.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SubTab.h"
#import "SubSystemPage.h"

@class CCustomPage;

/*============================================================================*/

@interface CHomeSubTab : CSubTab

/*============================================================================*/
{
    NSMutableArray              *m_pDefs;
}

-(id)initWithName:(NSString*)pName ID:(int)iID Data1:(int)iData1 Data2:(int)iData2;
-(CSubSystemPage*)CreateUserPage:(CGRect)rFrame MainTabPage:(CMainTabViewController*)pMain;

@end

/*============================================================================*/

@interface CHomePage : CSubSystemPage

/*============================================================================*/
{
    CCustomPage                 *m_pPage;
}

-(id)initWithFrame:(CGRect)rFrame Controls:(NSMutableArray*)pList SubTab:(CSubTab*)pSub MainTabPage:(CMainTabViewController*)pMain;
-(CToolBarClientView*)CreateMenuView:(CGRect)rView ViewController:(IPAD_ViewController*)pCtlr Flags:(int)iFlags;

@end