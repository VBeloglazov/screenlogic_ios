/*
--------------------------------------------------------------------------------

    HLMSG.CPP

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#include "hlstd.h"

#include "helpers.h"
#include "hlstructs.h"
#include "hlerror.h"
#include "hltime.h"

#ifdef MAC_OS
    #include "stdlib.h"
#endif

#include "hlmsg.h"

#define MAX_STRING  10000
#define UNICODE_BIT 0x80000000

/*============================================================================*/

    CHLMSG::CHLMSG(
    BOOL                        bOwnData )

/*============================================================================*/
{
    // Generic init.
    m_shSenderID    = -1;
    m_shMessageID   = -1;
    m_iNBytes       = 0;
    m_bCritical     = FALSE;

    m_pBuffer       = NULL;
    m_iNBuffer      = 0;
    m_iIndex        = 0;
    m_bOwnData      = bOwnData;
    m_bReadError    = FALSE;
}
/*============================================================================*/

    CHLMSG::CHLMSG(
    HLT_HEADER                  *pHeader )

/*============================================================================*/
{
    // Generic init.
    m_shSenderID    = -1;
    m_shMessageID   = -1;
    m_iNBytes       = 0;
    m_bCritical     = TRUE;

    m_pBuffer       = NULL;
    m_iNBuffer      = 0;
    m_iIndex        = 0;
    m_bOwnData      = FALSE;

    // Check message in client.
    int iNBytes = pHeader->m_iNBytes;
    if ((iNBytes < 0) || (iNBytes > HL_MAX_MESSAGE_SIZE))
    {
        ASSERT(FALSE);
        return;
    }

    // Set our data based on client.
    m_shSenderID    = pHeader->m_shSenderID;
    m_shMessageID   = pHeader->m_shMessageID;

    m_pBuffer       = (BYTE*) pHeader;
    m_iNBuffer      = iNBytes + sizeof(HLT_HEADER);
    m_iNBytes       = iNBytes;
    m_bOwnData      = FALSE;
    m_bReadError    = FALSE;
}
/*============================================================================*/

    CHLMSG::CHLMSG(
    short                       shSenderID,
    short                       shMessageID,
    int                         iNBytes,
    BOOL                        bCritical )

/*============================================================================*/
{
    m_shSenderID    = shSenderID;
    m_shMessageID   = shMessageID;
    m_iNBytes       = 0;
    m_bCritical     = bCritical;

    m_pBuffer       = NULL;
    m_iNBuffer      = 0;
    m_iIndex        = 0;
    m_bOwnData      = FALSE;
    m_bReadError    = FALSE;

    // Check.
    if ((m_iNBytes < 0) || (iNBytes > HL_MAX_MESSAGE_SIZE))
    {
        ASSERT(FALSE);
        return;
    }

    // Init: store iNBytes as the real data size.
    m_bOwnData = TRUE;
    m_iNBytes = iNBytes;
    if (!AllocBuffer(iNBytes))
        return;
}
/*============================================================================*/

    CHLMSG::CHLMSG(
    CHLMSG                      *pCopyFrom )

/*============================================================================*/
{
    m_shSenderID    = pCopyFrom->SenderID();
    m_shMessageID   = pCopyFrom->MessageID();
    m_iNBytes       = 0;
    m_bCritical     = pCopyFrom->Critical();

    m_pBuffer       = NULL;
    m_iNBuffer      = 0;
    m_iIndex        = 0;
    m_bOwnData      = FALSE;
    m_bReadError    = FALSE;

    // Init: store iNBytes as the real data size.
    m_bOwnData = TRUE;
    m_iNBytes = pCopyFrom->DataSize();
    if (!AllocBuffer(m_iNBytes))
        return;

    memcpy(MSG(), pCopyFrom->MSG(), pCopyFrom->MSGSize());
}
/*============================================================================*/

    CHLMSG::~CHLMSG()

/*============================================================================*/
{
    if ((m_bOwnData) && (m_pBuffer != NULL))
        delete [] m_pBuffer;
}
/*============================================================================*/

    CHLMSG                      *CHLMSG::CreateCopy()

/*============================================================================*/
{
    // Generic init.
    CHLMSG *pNew = new CHLMSG(m_shSenderID, m_shMessageID, m_iNBytes, TRUE);
    memcpy(pNew->MSG(), MSG(), MSGSize());
    return pNew;
}
/*============================================================================*/

    BOOL                        CHLMSG::Reset(
    HLT_HEADER                  *pHeader )

/*============================================================================*/
{
    m_shSenderID    = pHeader->m_shSenderID;
    m_shMessageID   = pHeader->m_shMessageID;
    m_bOwnData = TRUE;
    if (AllocBuffer(m_iNBytes))
    {
        m_iNBytes = pHeader->m_iNBytes;
        m_iIndex = 0;
        memcpy(((BYTE*) pHeader) + sizeof(HLT_HEADER), m_pBuffer, m_iNBytes);
        return TRUE;
    }
    else
    {
        m_iNBytes = 0;
        m_iIndex = 0;
        return FALSE;
    }
}
/*============================================================================*/

    BOOL                        CHLMSG::Reset(
    int                         iNewMessageID,
    int                         iNBytes,
    BOOL                        bCritical )

/*============================================================================*/
{
    m_shMessageID = iNewMessageID;
    m_bCritical = bCritical;
    m_iNBytes = 0;
    m_iIndex = 0;

    return AllocBuffer(iNBytes);
}
/*============================================================================*/

    void                        CHLMSG::ResetRead()

/*============================================================================*/
{
    m_iIndex = 0;
}
/*============================================================================*/

    short                       CHLMSG::SenderID()

/*============================================================================*/
{
    return m_shSenderID;
}
/*============================================================================*/

    void                        CHLMSG::SenderID(
    short                       shNew )

/*============================================================================*/
{
    if (m_pBuffer == NULL)
        return;

    // Shouldn't be changing the value in the HLT_HEADER style of CHLMSG...
    ASSERT(m_bOwnData);

    m_shSenderID = shNew;
    HLT_HEADER *pHeader = (HLT_HEADER*) m_pBuffer;
    pHeader->m_shSenderID = m_shSenderID;
}
/*============================================================================*/

    short                       CHLMSG::MessageID()

/*============================================================================*/
{
    return m_shMessageID;
}
/*============================================================================*/

    void                        CHLMSG::MessageID(
    short                       shNew )

/*============================================================================*/
{
    if (m_pBuffer == NULL)
        return;

    m_shMessageID = shNew;
    HLT_HEADER *pHeader = (HLT_HEADER*) m_pBuffer;
    pHeader->m_shMessageID = m_shMessageID;
}
/*============================================================================*/

    BOOL                        CHLMSG::Critical()

/*============================================================================*/
{
    return m_bCritical;
}
/*============================================================================*/

    BOOL                        CHLMSG::IsSameMSG(
    CHLMSG                      *pOther )

/*============================================================================*/
{
    if (DataSize() != pOther->DataSize())
        return FALSE;
    if (MessageID() != pOther->MessageID())
        return FALSE;
    BYTE *pThisData = Data();
    BYTE *pAltData = pOther->Data();
    int iNData = DataSize();
    for (int i = 0; i < iNData; i++)
    {
        if (*pThisData != *pAltData)
            return FALSE;
        pThisData++;
        pAltData++;
    }

    return TRUE;
}
/*============================================================================*/

    void                        CHLMSG::LoadHeader(
    HLT_HEADER                  *pHeader )

/*============================================================================*/
{
    // Generic init.
    m_shSenderID    = pHeader->m_shSenderID;
    m_shMessageID   = pHeader->m_shMessageID;
    m_iNBytes       = pHeader->m_iNBytes;
    m_bCritical     = FALSE;

    m_pBuffer       = NULL;
    m_iNBuffer      = 0;
    m_iIndex        = 0;
    m_bOwnData      = TRUE;

    // This does all the work of setting up the buffer and copying memory.
    BYTE *pData = ((BYTE*) pHeader) + sizeof(HLT_HEADER);
    PutData(pData, pHeader->m_iNBytes);

    // Reset our index so caller can now read properly.
    m_iIndex        = 0;
}
/*============================================================================*/

    int                         CHLMSG::DataSize()

/*============================================================================*/
{
    return m_iNBytes;
}
/*============================================================================*/

    void                        CHLMSG::SetDataSize(
    int                         iSizeNew )

/*============================================================================*/
{
    m_iNBytes = iSizeNew;
    HLT_HEADER *pHeader = (HLT_HEADER*) m_pBuffer;
    pHeader->m_iNBytes = m_iNBytes;
}
/*============================================================================*/

    BYTE                        *CHLMSG::Data()

/*============================================================================*/
{
    if (m_iNBuffer == 0)
        return NULL;
    ASSERT(m_iNBuffer > sizeof(HLT_HEADER));

    return m_pBuffer + sizeof(HLT_HEADER);
}
/*============================================================================*/

    int                         CHLMSG::MSGSize()

/*============================================================================*/
{
    return m_iNBytes + sizeof(HLT_HEADER);
}
/*============================================================================*/

    BYTE                        *CHLMSG::MSG()

/*============================================================================*/
{
    // If a buffer has not been allocated, return us: just the header.
    if (m_iNBuffer == 0)
        return (BYTE*) &m_shSenderID;
    else
    {
        // Check that the sender ID and message ID got set: if this assert fires
        // on you, then the messageID or senderID are no good.
        ASSERT(m_pBuffer != NULL);
        HLT_HEADER *pHeader = (HLT_HEADER*) m_pBuffer;
        ASSERT(pHeader->m_shSenderID != -1);
        ASSERT(pHeader->m_shMessageID != -1);
        pHeader->m_iNBytes = DataSize();

        return m_pBuffer;
    }
}
/*============================================================================*/

    BOOL                        CHLMSG::GetData(
    BYTE                        *pResult,
    int                         iNBytes )

/*============================================================================*/
{
    if (m_iIndex > (m_iNBytes - iNBytes))
    {
        m_bReadError = TRUE;
        return FALSE;
    }

    memcpy(pResult, Data() + m_iIndex, iNBytes);

    // Advance the index to next DWORD boundary.
    m_iIndex += DWORDAlign(iNBytes);
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetChar(
    char                        *pResult )

/*============================================================================*/
{
    if (m_iIndex > (m_iNBytes - (int) sizeof(char)))
    {
        m_bReadError = TRUE;
        return FALSE;
    }

    char chResult = *((char*) (Data() + m_iIndex));
    m_iIndex += sizeof(char);

    ASSERT(pResult != NULL);
    *pResult = chResult;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetBYTE(
    BYTE                        *pResult )

/*============================================================================*/
{
    if (m_iIndex > (m_iNBytes - (int) sizeof(char)))
    {
        m_bReadError = TRUE;
        return FALSE;
    }

    BYTE chResult = *((BYTE*) (Data() + m_iIndex));
    m_iIndex += sizeof(BYTE);

    ASSERT(pResult != NULL);
    *pResult = chResult;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetShort(
    short                       *pResult )

/*============================================================================*/
{
    if (m_iIndex > (m_iNBytes - (int) sizeof(short)))
    {
        m_bReadError = TRUE;
        return FALSE;
    }

    short shResult = *((short*) (Data() + m_iIndex));
    m_iIndex += sizeof(short);

    ASSERT(pResult != NULL);
    *pResult = shResult;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetWORD(
    WORD                        *pResult )

/*============================================================================*/
{
    if (m_iIndex > (m_iNBytes - (int) sizeof(WORD)))
    {
        m_bReadError = TRUE;
        return FALSE;
    }

    if (pResult == NULL)
    {
        m_iIndex += sizeof(WORD);
        return TRUE;
    }

    if ((m_iIndex % 2) != 0)
    {
        memcpy(pResult, Data()+m_iIndex, sizeof(WORD));
    }
    else
    {
        WORD shResult = *((WORD*) (Data() + m_iIndex));
        *pResult = shResult;
    }

    m_iIndex += sizeof(WORD);
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetInt(
    int                         *pResult )

/*============================================================================*/
{
    if (m_iIndex > (m_iNBytes - (int) sizeof(int)))
    {
        m_bReadError = TRUE;
        return FALSE;
    }

    if (pResult == NULL)
    {
        m_iIndex += sizeof(int);
        return TRUE;
    }

    if ((m_iIndex % 4) != 0)
    {
        memcpy(pResult, Data()+m_iIndex, sizeof(int));
    }
    else
    {
        int iResult = *((int*) (Data() + m_iIndex));
        *pResult = iResult;
    }

    m_iIndex += sizeof(int);

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetString(
    CHLString                   *pResult )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    // Get the header int
    int iHeader;
    if (!GetInt(&iHeader))
        return FALSE;

    int iNChars = iHeader;
    int iNData  = iHeader;

#ifdef MFC_CSTRING
    // 
    // CHLString is a CString derivitive, input must be converted to fixed type
    //
    BOOL bUnicode = FALSE;
    if (iHeader & UNICODE_BIT)
    {
        bUnicode = TRUE;
        iNData = iHeader & ~UNICODE_BIT;
        iNChars = iNData / 2;
    }

    char *pSrc = (char*) (Data() + m_iIndex);
    void *pBuf = pResult->GetBuffer(iNChars);

    #ifdef UNICODE
        //
        // Need Unicode
        //
        if (bUnicode)
        {
            // Already unicode
            memcpy(pBuf, pSrc, iNData);            
        }
        else
        {
		    mbstowcs((wchar_t*) pBuf, (char*)pSrc, iNChars);
        }
    #else
        //
        // Need chars
        //
        if (bUnicode)
        {
            // Unicode, need to make chars
            wcstombs((char*) pBuf, (wchar_t*)pSrc, iNChars);
        }
        else
        {
            // Already chars
            memcpy(pBuf, pSrc, iNData);
        }
        // FROM MAC
    #endif

    pResult->ReleaseBuffer(iNChars);

    m_iIndex += DWORDAlign(iNData);
    m_bReadError = FALSE;
    return TRUE;
#else
    CHLString::CHLSTRING_TYPE Type = CHLString::HLSTRING_CHAR;
    if (iHeader & UNICODE_BIT)
    {
        Type = CHLString::HLSTRING_UNICODE;
        iNData = iHeader & ~UNICODE_BIT;
        iNChars = iNData / 2;
    }
    

    if (m_iIndex > (m_iNBytes - iNData))
        return FALSE;
    if (iNData < 0)
        return FALSE;
    if ((iNData == 0) || (iNData > MAX_STRING))
    {
        m_iIndex += DWORDAlign(iNData);
        pResult->Empty();
        m_bReadError = FALSE;
        return TRUE;
    }

    int iAllocSize = iNChars+1;
    if (Type == CHLString::HLSTRING_UNICODE)
        iAllocSize *= SIZEOF_WCHAR_T;

    pResult->Empty();
    pResult->Init(Type, iAllocSize);
    char *pSrc = (char*) (Data() + m_iIndex);
    void *pDst = pResult->GetBuffer(iNChars);

#ifdef IPHONE
    if (Type == CHLString::HLSTRING_UNICODE)
    {
        // wchar_t is 4 bytes in iPhone
        wchar_t *pDst32 = (wchar_t*)pDst;
        unsigned short *pSrc16 = (unsigned short*)pSrc;
        for (int i = 0; i < iNChars; i++)
        {
            pDst32[i] = pSrc16[i];
        }
    }
    else
    {
        memcpy(pDst, pSrc, iNData);
    }
#else
    memcpy(pDst, pSrc, iNData);
#endif

    pResult->ReleaseBuffer(iNChars);

    // Advance our index to the next DWORD boundary.
    m_iIndex += DWORDAlign(iNData);
    m_bReadError = FALSE;
    return TRUE;
#endif
}
#ifdef UNICODE
/*============================================================================*/

    BOOL                        CHLMSG::GetString(
    wchar_t                     **ppResult,
    int                         *piLen )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    // First pull out the length, in characters.
    int iHeader;
    if (!GetInt(&iHeader))
    {
        *piLen = 0;
        return FALSE;
    }

    int iNData = iHeader;
    int iNChars = iHeader;

    BOOL bUnicode = FALSE;
    if (iHeader & UNICODE_BIT)
    {
        bUnicode = TRUE;
        iNData = iHeader & ~UNICODE_BIT;
        iNChars = iNData / 2;
    }

    if (m_iIndex > (m_iNBytes - iNData))
    {
        *piLen = 0;
        return FALSE;
    }
    if (iNChars < 0)
    {
        *piLen = 0;
        return FALSE;
    }

    wchar_t *pOut = *ppResult;
    if (pOut != NULL)
        delete [] pOut;
    pOut = new wchar_t[iNChars+1];
    *ppResult = pOut;


    if ((iNChars == 0) || (iNChars > MAX_STRING))
    {
        *piLen = 0;
        pOut[0] = 0;
        m_bReadError = FALSE;
        return TRUE;
    }

    // Then pull out the chars: CHLMSG always stores char strings, not UNICODE.
    char *pSource = (char*) (Data() + m_iIndex);

    if (bUnicode)   
    {
        memcpy(pOut, pSource, iNData);
    }
    else
    {
	    mbstowcs((wchar_t*) pOut, pSource, iNChars);
    }

    pOut[iNChars] = 0;

    *piLen = iNChars;

    // Advance our index to the next DWORD boundary.
    m_iIndex += DWORDAlign(iNData);
    m_bReadError = FALSE;

    return TRUE;
}
#endif
#ifndef UNICODE
/*============================================================================*/

    BOOL                        CHLMSG::GetString(
    char                        **ppResult,
    int                         *piLen )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    // First pull out the length, in characters.
    int iHeader;
    if (!GetInt(&iHeader))
    {
        *piLen = 0;
        return FALSE;
    }

    int iNData = iHeader;
    int iNChars = iHeader;

    BOOL bUnicode = FALSE;
    if (iHeader & UNICODE_BIT)
    {
        bUnicode = TRUE;
        iNData = iHeader & ~UNICODE_BIT;
        iNChars = iNData / 2;
    }

    if (m_iIndex > (m_iNBytes - iNData))
    {
        *piLen = 0;
        return FALSE;
    }
    if (iNChars < 0)
    {
        *piLen = 0;
        return FALSE;
    }

    char *pOut = *ppResult;
    if (pOut != NULL)
        delete [] pOut;
    pOut = new char[iNChars+1];
    *ppResult = pOut;

    if ((iNChars == 0) || (iNChars > MAX_STRING))
    {
        *piLen = 0;
        pOut[0] = 0;
        m_bReadError = FALSE;
        return TRUE;
    }

    // Then pull out the chars: CHLMSG always stores char strings, not UNICODE.
    char *pSource = (char*) (Data() + m_iIndex);

    if (bUnicode)   
    {
        wcstombs((char*) pOut, (wchar_t*)pSource, iNChars);
    }
    else
    {
        memcpy(pOut, pSource, iNData);
    }

    pOut[iNChars] = 0;

    *piLen = iNChars;

    // Advance our index to the next DWORD boundary.
    m_iIndex += DWORDAlign(iNData);
    m_bReadError = FALSE;

    return TRUE;
}
#endif
/*============================================================================*/

    BOOL                        CHLMSG::GetTime(
    CHLTime                     *pResult )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    // We send down 8 shorts...
    int iSize = 16;
    if (m_iIndex > (m_iNBytes - iSize))
        return FALSE;

    ASSERT(pResult != NULL);
    pResult->Day(1);
    short shYear, shMonth, shDayOfWeek, shDay, shHour, shMin, shSec, shMSec;
    if (!GetShort(&shYear))
        return FALSE;
    if (!GetShort(&shMonth))
        return FALSE;
    if (!GetShort(&shDayOfWeek))
        return FALSE;
    if (!GetShort(&shDay))
        return FALSE;
    if (!GetShort(&shHour))
        return FALSE;
    if (!GetShort(&shMin))
        return FALSE;
    if (!GetShort(&shSec))
        return FALSE;
    if (!GetShort(&shMSec))
        return FALSE;
    pResult->Year(shYear);
    pResult->Month(shMonth);
    pResult->DayOfWeek(shDayOfWeek);
    pResult->Day(shDay);
    pResult->Hour(shHour);
    pResult->Minute(shMin);
    pResult->Second(shSec);
    pResult->MSecond(shMSec);

    #ifdef UNDER_CE
        pResult->DayOfWeek(shDayOfWeek);
    #endif


    m_bReadError = FALSE;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetTimeCompressed(
    CHLTime                     *pResult )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    // We send down 8 shorts...
    int iSize = 6;
    if (m_iIndex > (m_iNBytes - iSize))
        return FALSE;

    ASSERT(pResult != NULL);
    BYTE byYear, byMonth, byDay, byHour, byMin, bySec;

    if (!GetChar((char*)&byYear))
        return FALSE;
    if (!GetChar((char*)&byMonth))
        return FALSE;
    if (!GetChar((char*)&byDay))
        return FALSE;
    if (!GetChar((char*)&byHour))
        return FALSE;
    if (!GetChar((char*)&byMin))
        return FALSE;
    if (!GetChar((char*)&bySec))
        return FALSE;

    BYTE byDayOfWeek = ((byMonth & 0xF0) >> 4);
    byMonth = byMonth & 0x0F;

    pResult->Day(1);
    pResult->Year(byYear + 2000);
    pResult->Month(byMonth);
    pResult->Day(byDay);
    pResult->DayOfWeek(byDayOfWeek);
    pResult->Hour(byHour);
    pResult->Minute(byMin);
    pResult->Second(bySec);
    pResult->MSecond(0);

    m_bReadError = FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetTimeSpan(
    CHLTimeSpan                 *pResult )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    // We send down 6 shorts, not the 64 bit UTC span...
    int iSize = 12;
    if (m_iIndex > (m_iNBytes - iSize))
        return FALSE;

    short shNegative, shDays, shHours, shMins, shSecs, shMSecs;
    if (!GetShort(&shNegative))
        return FALSE;
    if (!GetShort(&shDays))
        return FALSE;
    if (!GetShort(&shHours))
        return FALSE;
    if (!GetShort(&shMins))
        return FALSE;
    if (!GetShort(&shSecs))
        return FALSE;
    if (!GetShort(&shMSecs))
        return FALSE;

    ASSERT(pResult != NULL);
    pResult->Set(shDays, shHours, shMins, shSecs, shMSecs);
    pResult->Negative(shNegative);

    m_bReadError = FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetColor(
    COLORREF                    *pResult )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;


    int iR = 0;
    int iG = 0;
    int iB = 0;
    if (!GetInt(&iR))
        return FALSE;
    if (!GetInt(&iG))
        return FALSE;
    if (!GetInt(&iB))
        return FALSE;

    *pResult = RGB(iR, iG, iB);
    m_bReadError = FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::GetRECT(
    RECT                        *pResult )

/*============================================================================*/
{
    if (m_bReadError)
        return FALSE;
    // Set in case we don't make it
    m_bReadError = TRUE;

    int iXLeft  = 0;
    int iYTop   = 0;
    int iDX     = 0;
    int iDY     = 0;

    if (!GetInt(&iXLeft))
        return FALSE;
    if (!GetInt(&iYTop))
        return FALSE;
    if (!GetInt(&iDX))
        return FALSE;
    if (!GetInt(&iDY))
        return FALSE;

    pResult->left   = iXLeft;
    pResult->top    = iYTop;
    pResult->right  = iXLeft + iDX - 1;
    pResult->bottom = iYTop + iDY - 1;

    m_bReadError = FALSE;

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutData(
    BYTE                        *pData,
    int                         iNBytes )

/*============================================================================*/
{
    if (!AllocBuffer(m_iNBytes + DWORDAlign(iNBytes)))
        return FALSE;

    BYTE *pOut = Data() + m_iIndex;
    memcpy(pOut, pData, iNBytes);

    // Advance to next DWORD boundary.
    m_iIndex += DWORDAlign(iNBytes);
    m_iNBytes = m_iIndex;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutChar(
    char                        cChar )

/*============================================================================*/
{
    if (!AllocBuffer(m_iNBytes + sizeof(char)))
        return FALSE;

    char *pOut = (char*) (Data() + m_iIndex);
    *pOut = cChar;

    m_iIndex += sizeof(char);
    m_iNBytes = m_iIndex;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutBYTE(
    BYTE                        byData )

/*============================================================================*/
{
    if (!AllocBuffer(m_iNBytes + sizeof(BYTE)))
        return FALSE;

    BYTE *pOut = (BYTE*) (Data() + m_iIndex);
    *pOut = byData;

    m_iIndex += sizeof(BYTE);
    m_iNBytes = m_iIndex;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutShort(
    short                       shShort )

/*============================================================================*/
{
    if (!AllocBuffer(m_iNBytes + sizeof(short)))
        return FALSE;

    short *pOut = (short*) (Data() + m_iIndex);
    *pOut = shShort;

    m_iIndex += sizeof(short);
    m_iNBytes = m_iIndex;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutWORD(
    WORD                        wData )

/*============================================================================*/
{
    if (!AllocBuffer(m_iNBytes + sizeof(WORD)))
        return FALSE;

    BYTE *pOut = (Data() + m_iIndex);
    memcpy(pOut, &wData, sizeof(WORD));

    m_iIndex += sizeof(WORD);
    m_iNBytes = m_iIndex;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutInt(
    int                         iInt )

/*============================================================================*/
{
    if (!AllocBuffer(m_iNBytes + sizeof(int)))
        return FALSE;

    BYTE *pOut = (Data() + m_iIndex);
    memcpy(pOut, &iInt, sizeof(int));

    m_iIndex += sizeof(int);
    m_iNBytes = m_iIndex;
    return TRUE;
}
#ifdef GATEWAY
/*============================================================================*/

    BOOL                        CHLMSG::PutStringCHAR(
    CHLString                   sString )

/*============================================================================*/
{
    sString.ConvertTochar();
    return PutString(sString);
}
#elif (defined HLCONFIG && defined PENTAIR)
/*============================================================================*/

    BOOL                        CHLMSG::PutStringCHAR(
    CHLString                   sString )

/*============================================================================*/
{
#ifdef MFC_CSTRING
    int iNChars = sString.GetLength();
    int iNData  = iNChars;
    int iHeader = iNChars;

    if (!AllocBuffer(m_iNBytes + DWORDAlign(iNData) + sizeof(int)))
        return FALSE;

    PutInt(iHeader);

    char *pTarget = (char*) (Data() + m_iIndex);
    char *pData = sString.GetBufferCHAR(iNChars);
    memcpy(pTarget, pData, iNData);
    sString.ReleaseBuffer(iNChars);

    // Pad out with NULLs
    int iPadding = DWORDAlign(iNData) - iNData;
    for (int i=0; i<iPadding; i++)
        pTarget[iNData+i] = 0;

    // Increment m_iIndex so it falls on a DWORD boundary.
    m_iIndex += DWORDAlign(iNData);
    m_iNBytes = m_iIndex;
    return TRUE;
#else
    ASSERT(0);   // not expected, but this should work
    sString.ConvertTochar();
    return PutString(sString);
#endif
}
#endif
/*============================================================================*/

    BOOL                        CHLMSG::PutString(
    CHLString                   sString )

/*============================================================================*/
{
    // Check buffer: need room for int plus chars.
    int iNChars = sString.GetLength();
    int iNData  = iNChars;
    int iHeader = iNChars;

#ifdef MFC_CSTRING
    #ifdef UNICODE
        // We're always unicode
        iNData  = iNChars * 2;//reversed order
        iHeader = iNData | UNICODE_BIT;
        //iNData  = iNChars * 2;
    #endif

    if (!AllocBuffer(m_iNBytes + DWORDAlign(iNData) + sizeof(int)))
        return FALSE;

    PutInt(iHeader);

    char *pTarget = (char*) (Data() + m_iIndex);
    void *pData = sString.GetBuffer(iNChars);
    memcpy(pTarget, pData, iNData);
    sString.ReleaseBuffer(iNChars);

    int iPadding = DWORDAlign(iNData) - iNData;
    for (int i=0; i<iPadding; i++)
        pTarget[iNData+i] = 0;

    m_iIndex += DWORDAlign(iNData);
    m_iNBytes = m_iIndex;

    return TRUE;
#else
    if (sString.Type() == CHLString::HLSTRING_UNICODE)
    {
        iNData *= 2;
        iHeader = iNData | UNICODE_BIT;
    }

    if (!AllocBuffer(m_iNBytes + DWORDAlign(iNData) + sizeof(int)))
        return FALSE;

    PutInt(iHeader);

    // Then put in the chars: CHLMSG always takes char strings, not UNICODE.
    char *pTarget = (char*) (Data() + m_iIndex);

    void *pData = sString.GetBuffer(iNChars);

    #ifdef IPHONE
        if (sString.Type() == CHLString::HLSTRING_UNICODE)
        {
            wchar_t *pSrc = (wchar_t*)pData;
            unsigned short *pTgt = (unsigned short*)pTarget;
            for (int i = 0; i < iNChars; i++)
            {
                *pTgt = (unsigned short)(*pSrc);
                pTgt++;
                pSrc++;
            }
        }
        else 
        {
            memcpy(pTarget, pData, iNData);
        }
    #else
        memcpy(pTarget, pData, iNData);
    #endif

    sString.ReleaseBuffer(iNChars);

    // Pad out with NULLs
    int iPadding = DWORDAlign(iNData) - iNData;
    for (int i=0; i<iPadding; i++)
        pTarget[iNData+i] = 0;

    // Increment m_iIndex so it falls on a DWORD boundary.
    m_iIndex += DWORDAlign(iNData);
    m_iNBytes = m_iIndex;
    return TRUE;
#endif
}
/*============================================================================*/

    BOOL                        CHLMSG::PutTime(
    CHLTime                     hltTime )

/*============================================================================*/
{
    // PutTime / GetTime sends 8 shorts...
    int iSize = 16;
    if (!AllocBuffer(m_iNBytes + iSize))
        return FALSE;

    // Copy over each member.
    PutShort(hltTime.Year());
    PutShort(hltTime.Month());
    PutShort(hltTime.DayOfWeek());
    PutShort(hltTime.Day());
    PutShort(hltTime.Hour());
    PutShort(hltTime.Minute());
    PutShort(hltTime.Second());
    PutShort(hltTime.MSecond());

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutTimeCompressed(
    CHLTime                     hltTime )

/*============================================================================*/
{
    // PutTime / GetTime sends 6 Bytes
    int iSize = 6;
    if (!AllocBuffer(m_iNBytes + iSize))
        return FALSE;

    BYTE byMonth = hltTime.Month();
    byMonth = byMonth | (hltTime.DayOfWeek() << 4);

    // Convert to seconds since 2000
    PutChar((char)(hltTime.Year()-2000));
    PutChar((char)byMonth);
    PutChar((char)hltTime.Day());
    PutChar((char)hltTime.Hour());
    PutChar((char)hltTime.Minute());
    PutChar((char)hltTime.Second());

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutTimeSpan(
    CHLTimeSpan                 hltTimeSpan )

/*============================================================================*/
{
    // We send down 6 shorts, not the 64 bit UTC span...
    int iSize = 12;
    if (!AllocBuffer(m_iNBytes + iSize))
        return FALSE;

    PutShort(hltTimeSpan.Negative());
    PutShort(hltTimeSpan.Days());
    PutShort(hltTimeSpan.Hours());
    PutShort(hltTimeSpan.Minutes());
    PutShort(hltTimeSpan.Seconds());
    PutShort(hltTimeSpan.MSeconds());

    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutColor(
    COLORREF                    rgbOut )

/*============================================================================*/
{
    int iSize = 12;
    if (!AllocBuffer(m_iNBytes + iSize))
        return FALSE;

    if (!PutInt(GetRValue(rgbOut)))
        return FALSE;
    if (!PutInt(GetGValue(rgbOut)))
        return FALSE;
    if (!PutInt(GetBValue(rgbOut)))
        return FALSE;
    return TRUE;
}
/*============================================================================*/

    BOOL                        CHLMSG::PutRECT(
    RECT                        rectOut )

/*============================================================================*/
{
    int iSize = 16;
    if (!AllocBuffer(m_iNBytes + iSize))
        return FALSE;

    if (!PutInt(rectOut.left))
        return FALSE;
    if (!PutInt(rectOut.top))
        return FALSE;
    if (!PutInt(rectOut.right - rectOut.left + 1))
        return FALSE;
    if (!PutInt(rectOut.bottom - rectOut.top + 1))
        return FALSE;

    return TRUE;
}
/*============================================================================*/

    BYTE                        *CHLMSG::GetReadPointer()

/*============================================================================*/
{
    return (Data() + m_iIndex);
}
#ifdef _CPPUNWIND
/*============================================================================*/

    void                        CHLMSG::X_Reset(
    HLT_HEADER                  *pHeader )

/*============================================================================*/
{
    if (!Reset(pHeader))
        throw CHLERROR(HLE_MEMORY);
}
/*============================================================================*/

    void                        CHLMSG::X_Reset(
    int                         iNewMessageID,
    int                         iNBytes,
    BOOL                        bCritical )

/*============================================================================*/
{
    if (!Reset(iNewMessageID, iNBytes, bCritical))
        throw CHLERROR(HLE_MEMORY);
}
/*============================================================================*/

    void                        CHLMSG::X_GetData(
    BYTE                        *pResult,
    int                         iNBytes )

/*============================================================================*/
{
    if (!GetData(pResult, iNBytes))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    char                        CHLMSG::X_GetChar()

/*============================================================================*/
{
    char cChar;
    if (!GetChar(&cChar))
        throw CHLERROR(HLE_UNKNOWN);
    return cChar;
}
/*============================================================================*/

    short                       CHLMSG::X_GetShort()

/*============================================================================*/
{
    short shShort;
    if (!GetShort(&shShort))
        throw CHLERROR(HLE_UNKNOWN);
    return shShort;
}
/*============================================================================*/

    int                         CHLMSG::X_GetInt()

/*============================================================================*/
{
    int iInt;
    if (!GetInt(&iInt))
        throw CHLERROR(HLE_UNKNOWN);
    return iInt;
}
/*============================================================================*/

    CHLString                   CHLMSG::X_GetString()

/*============================================================================*/
{
    CHLString sString;
    if (!GetString(&sString))
        throw CHLERROR(HLE_UNKNOWN);
    return sString;
}
/*============================================================================*/

    CHLTime                     CHLMSG::X_GetTime()

/*============================================================================*/
{
    CHLTime hltTime;
    if (!GetTime(&hltTime))
        throw CHLERROR(HLE_UNKNOWN);
    return hltTime;
}
/*============================================================================*/

    COLORREF                    CHLMSG::X_GetColor()

/*============================================================================*/
{
    COLORREF rgbColor;
    if (!GetColor(&rgbColor))
        throw CHLERROR(HLE_UNKNOWN);
    return rgbColor;
}
/*============================================================================*/

    RECT                        CHLMSG::X_GetRECT()

/*============================================================================*/
{
    RECT rect;
    if (!GetRECT(&rect))
        throw CHLERROR(HLE_UNKNOWN);
    return rect;
}
/*============================================================================*/

    CHLTimeSpan                 CHLMSG::X_GetTimeSpan()

/*============================================================================*/
{
    CHLTimeSpan hltTimeSpan;
    if (!GetTimeSpan(&hltTimeSpan))
        throw CHLERROR(HLE_UNKNOWN);
    return hltTimeSpan;
}
/*============================================================================*/

    void                        CHLMSG::X_PutData(
    BYTE                        *pData,
    int                         iNBytes )

/*============================================================================*/
{
    if (!PutData(pData, iNBytes))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutChar(
    char                        cChar )

/*============================================================================*/
{
    if (!PutChar(cChar))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutShort(
    short                       shShort )

/*============================================================================*/
{
    if (!PutShort(shShort))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutInt(
    int                         iInt )

/*============================================================================*/
{
    if (!PutInt(iInt))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutString(
    CHLString                   sString )

/*============================================================================*/
{
    if (!PutString(sString))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutTime(
    CHLTime                     hltTime )

/*============================================================================*/
{
    if (!PutTime(hltTime))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutTimeSpan(
    CHLTimeSpan                 hltTimeSpan )

/*============================================================================*/
{
    if (!PutTimeSpan(hltTimeSpan))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutColor(
    COLORREF                    rgbOut )

/*============================================================================*/
{
    if (!PutColor(rgbOut))
        throw CHLERROR(HLE_UNKNOWN);
}
/*============================================================================*/

    void                        CHLMSG::X_PutRECT(
    RECT                        rectOut )

/*============================================================================*/
{
    if (!PutRECT(rectOut))
        throw CHLERROR(HLE_UNKNOWN);
}
#endif // _CPPUNWIND
/*============================================================================*/

    BOOL                        CHLMSG::AllocBuffer(
    int                         iSize )

/*============================================================================*/
{
    LOGASSERT(m_bOwnData);

    // Add room for the header
    iSize += sizeof(HLT_HEADER);

    // Is it bigger?
    if (iSize <= m_iNBuffer)
        return TRUE;

    // Need a bigger buffer: expand ours now.
    int iOldSize    = m_iNBuffer;
    BYTE *pOld      = m_pBuffer;

    m_iNBuffer  = 64 + 64 * (iSize / 64);
    m_pBuffer   = new BYTE[m_iNBuffer];
    if (m_pBuffer == NULL)
    {
        m_pBuffer = pOld;
        return FALSE;
    }

    // Copy in old data, or set our msgs.
    if (pOld == NULL)
    {
        HLT_HEADER *pHeader = (HLT_HEADER*) m_pBuffer;
        pHeader->m_shSenderID = m_shSenderID;
        pHeader->m_shMessageID = m_shMessageID;
        pHeader->m_iNBytes = m_iNBytes;
    }
    else
    {
        memcpy(m_pBuffer, pOld, iOldSize);
        delete [] pOld;
    }
    return TRUE;
}
/*============================================================================*/

    CHLLogMSG::CHLLogMSG(
    int                         iTypeInfo )

    : CHLMSG(0, 0, 128, TRUE)

/*============================================================================*/
{
    m_hlTime.GetPresentTime();
    m_iTypeInfo = iTypeInfo;

    PutTime(m_hlTime);
    PutInt(m_iTypeInfo);
}
/*============================================================================*/

    CHLLogMSG::CHLLogMSG(
    HLT_LOG_HEADER              *pHeader  )

    : CHLMSG(pHeader->m_shSenderID, pHeader->m_shSenderID, pHeader->m_iNBytes, TRUE)

/*============================================================================*/
{
    // Our stuff
    m_hlTime.Day(1);
    m_hlTime.Year(pHeader->m_shYear);
    m_hlTime.Month(pHeader->m_shMonth);
    m_hlTime.DayOfWeek(pHeader->m_shDayOfWeek);
    m_hlTime.Day(pHeader->m_shDay);
    m_hlTime.Hour(pHeader->m_shHour);
    m_hlTime.Minute(pHeader->m_shMin);
    m_hlTime.Second(pHeader->m_shSec);
    m_hlTime.MSecond(pHeader->m_shMSec);
    m_iTypeInfo = pHeader->m_iTypeInfo;

    if (m_pBuffer == NULL)
    {
        LOGASSERT(FALSE);
        return;
    }

    int iNBytes = sizeof(HLT_LOG_HEADER) - sizeof(HLT_HEADER);
    BYTE *pCopy = (BYTE*) pHeader;
    memcpy(Data(), pCopy+ sizeof(HLT_HEADER), iNBytes);

    // For above stuff
    m_iIndex = iNBytes;
}
/*============================================================================*/

    CHLTime                     CHLLogMSG::Time()

/*============================================================================*/
{
    return m_hlTime;
}
/*============================================================================*/

    void                        CHLLogMSG::Time(
    CHLTime                     tmNew )

/*============================================================================*/
{
    m_hlTime = tmNew;

    int iIndex = m_iIndex;
    m_iIndex = 0;
    PutTime(m_hlTime);
    m_iIndex = iIndex;
}
/*============================================================================*/

    int                         CHLLogMSG::TypeInfo()

/*============================================================================*/
{
    return m_iTypeInfo;
}
