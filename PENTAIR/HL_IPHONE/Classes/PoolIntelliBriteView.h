//
//  PoolIntelliBriteView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/9/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "HLView.h"
#import "NSSocketSink.h"


@class CNSPoolConfig;
@class CHLButton;


#define COLORREF                int
#define RGB(r,g,b)              (r<<16|g<<8|b)

/*============================================================================*/

@interface CPoolIntelliBriteView : UIView < CSink, CHLCommandResponder, CNSSocketSink >

/*============================================================================*/
{
    CNSPoolConfig               *m_pConfig;
    CHLButton                   *m_pIBriteButtons[13];
    CHLButton                   *m_pColorButtons[5];

    UILabel                     *m_pLabel1;
    UILabel                     *m_pLabel2;
}

- (id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig;

-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID;
-(CHLButton*)AddButton:(NSString*)pText CommandID:(int)iID Color:(COLORREF)rgb;
-(UILabel*)AddLabel:(NSString*)pText;

@end
