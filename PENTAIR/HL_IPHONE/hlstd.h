/*
--------------------------------------------------------------------------------

    HLSTD.H : CRYSTALPAD

    Copyright 2002 HomeLogic, Marblehead, MA 01945

This is the include file used to create the pre-compiled header for all WinCE projects.
--------------------------------------------------------------------------------
*/
#ifndef HLSTD_H
#define HLSTD_H

//#define GDI_TIMER

//
// Avoid the _DEBUG / DEBUG and _UNICODE / UNICODE problem.
// All CRYSTALPAD builds are UNICODE.
//
#if defined(_DEBUG) && (!defined(DEBUG))
    #define DEBUG
#endif
#if defined(DEBUG) && (!defined(_DEBUG))
    #define _DEBUG
#endif

    class CHLServer;
    class CSetServer;
    class CSpeedServer;

    extern CHLServer                *g_pHLServer;
    extern CSetServer               *g_pSetServer;
    extern CSpeedServer             *g_pSpeedServer;


#ifdef MAC_OS
    #include <wchar.h>
    #include <string.h>
    #include "stdlib.h"
    #include "stdint.h"
    #include "unistd.h"
    #include <netdb.h>
    #include <fcntl.h>
    #include <sys/select.h>
    #include <sys/ioctl.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <sys/errno.h>
    #include <netinet/in.h>
#endif

//
// Main headers: note no STL.
//
#undef _CPPRTTI
#define _CPPRTTI

#define BOOL            int
#define TCHAR           char
#define DWORD           unsigned int
#define TRUE            1
#define FALSE           0
#define COLORREF        unsigned int
#define BYTE            unsigned char
#define BOOLEAN         BYTE
#define WORD            unsigned short
#define UINT            unsigned int
#define __int64         uint64_t
#define _T(x)           x
#define _HLTEXT(x)      _T(x)
#define HLTCHAR         TCHAR
#define SOCKET          int
#define SOCKADDR_IN     sockaddr_in
#define SOCKADDR        sockaddr
#define RGBA(r,g,b, a)  ((b<<24)|(g<<16)|(r<<8)|a)
#define RGB(r,g,b)      ((r<<16)|(g<<8)|b)
#define GetRValue(x)    ((x >> 16) & 0xFF)
#define GetGValue(x)    ((x >> 8) & 0xFF)
#define GetBValue(x)    (x & 0xFF)
#define INVALID_SOCKET  -1
#define ZeroMemory(x,y) memset(x, 0, y)
#define __min(a,b)      MIN(a,b)
#define __max(a,b)      MAX(a,b)
#define closesocket(x)  close(x)
#define GetLastError()  (errno)
#define ULONG           unsigned long
#define ioctlsocket     ioctl
#define SOCKET_ERROR    -1
#define HOSTENT         hostent
#define HL_PIXEL        unsigned int
#define START_TIMER(x)  ;;
#define END_TIMER(x)    ;;
#define GDI_SHADEPOLY   0
#define HDC             void*
#define HWND            void*
#define HPEN            void*
#define HBITMAP         void*
#define HFONT           void*
#define HBRUSH          void*
#define SelectObject(x, y)  ;;
#define ReleaseDC(x, y)     ;;
#define TRANSPARENT     0


typedef struct
{
    int                 left;
    int                 top;
    int                 right;
    int                 bottom;
} RECT;

typedef struct
{
    int                 x;
    int                 y;
} POINT;

typedef struct
{
    int                 cx;
    int                 cy;
} SIZE;

//
// TRACE, ASSERT, VERIFY
//
#include "asserttrace.h"

#ifdef TRACE
    #undef TRACE
#endif
#ifdef TRACE1
    #undef TRACE1
#endif
#ifdef TRACE2
    #undef TRACE2
#endif
#ifdef TRACE3
    #undef TRACE3
#endif
#ifdef TRACE4
    #undef TRACE4
#endif
#ifdef TRACE5
    #undef TRACE5
#endif
#ifdef ASSERT
    #undef ASSERT
#endif
#ifdef _ASSERT
    #undef _ASSERT
#endif
#ifdef VERIFY
    #undef VERIFY
#endif

#ifdef _DEBUG
    #define TRACE(S)                          { TraceWrap(S); }
    #define TRACE1(F, P)                      { wchar_t Buffer[256]; _snwprintf(Buffer, 256, F, P); TraceWrap(Buffer); }
    #define TRACE2(F, P1,P2)                  { wchar_t Buffer[256]; _snwprintf(Buffer, 256, F, P1,P2); TraceWrap(Buffer); }
    #define TRACE3(F, P1,P2,P3)               { wchar_t Buffer[256]; _snwprintf(Buffer, 256, F, P1,P2,P3); TraceWrap(Buffer); }
    #define TRACE4(F, P1,P2,P3,P4)            { wchar_t Buffer[256]; _snwprintf(Buffer, 256, F, P1,P2,P3,P4); TraceWrap(Buffer); }
    #define TRACE5(F, P1,P2,P3,P4,P5)         { wchar_t Buffer[256]; _snwprintf(Buffer, 256, F, P1,P2,P3,P4,P5); TraceWrap(Buffer); }
    #define TRACE6(F, P1,P2,P3,P4,P5,P6)      { char Buffer[256]; _snprintf(Buffer, 256, F, P1,P2,P3,P4,P5,P6); TraceWrap(Buffer); }
    #define TRACE7(F, P1,P2,P3,P4,P5,P6,P7)   { char Buffer[256]; _snprintf(Buffer, 256, F, P1,P2,P3,P4,P5,P6,P7); TraceWrap(Buffer); }
    #define TRACE8(F, P1,P2,P3,P4,P5,P6,P7,P8){ char Buffer[256]; _snprintf(Buffer, 256, F, P1,P2,P3,P4,P5,P6,P7,P8); TraceWrap(Buffer); }

    #define VERIFY(X)                         if (!(X)) { ASSERT(FALSE); }
#else
    #define TRACE(S)                          {(void)0;}
    #define TRACE1(F, P)                      {(void)0;}
    #define TRACE2(F, P1, P2)                 {(void)0;}
    #define TRACE3(F, P1,P2,P3)               {(void)0;}
    #define TRACE4(F, P1,P2,P3,P4)            {(void)0;}
    #define TRACE5(F, P1,P2,P3,P4,P5)         {(void)0;}
    #define TRACE6(F, P1,P2,P3,P4,P5,P6)      {(void)0;}
    #define TRACE7(F, P1,P2,P3,P4,P5,P6,P7)   {(void)0;}
    #define TRACE8(F, P1,P2,P3,P4,P5,P6,P7,P8){(void)0;}

    #define VERIFY(X)                         (X)
#endif


//
// ASSERT / LOGASSERT Definitions
//
#ifdef _DEBUG
    #define  ASSERT(X)                      if (!(X)) { AssertDlg(__FILE__,__LINE__,FALSE); }
    #define  LOGASSERT(X)                   if (!(X)) { AssertDlg(__FILE__,__LINE__,TRUE); }
#else
    #define  ASSERT(X)                      {(void)0;}
    #define  LOGASSERT(X)                   if (!(X)) { AssertDlg(__FILE__,__LINE__,TRUE); }
#endif

// For global object

#if defined(HL_ARM) | defined(HL_X86B) | defined(HL_ARMV4C) | defined(HL_WIN32) | defined(SMALL_FORMAT)
    #define KEY_NAV
#endif


// Our string and oblist
#include "hlstring.h"
#include "hloblist.h"
#include "helpers.h"


//
// Java / C++ helpers...
//
#undef true
#undef false
#undef boolean
#undef null

// For now - don't use this in C++.
#undef String

#define true                    TRUE
#define false                   FALSE
#define boolean                 BOOL
#define null                    NULL

#endif

