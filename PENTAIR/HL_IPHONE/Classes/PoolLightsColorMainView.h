//
//  PoolLightsColorMainView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/20/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabRoundRegion.h"

@class CNSPoolConfig;

#define SUBPAGE_COLOR_LIGHTS    0
#define SUBPAGE_INTELLIBRITE    1
#define SUBPAGE_MAGICSTREAM     2

/*============================================================================*/

@interface CPoolLightsColorMainView : CTabRoundRegion

/*============================================================================*/
{
    int                         m_iSubPages[3];
    CNSPoolConfig               *m_pConfig;
}

-(id)initWithFrame:(CGRect)frame Config:(CNSPoolConfig*)pConfig;
-(UIView*)CreatePageForIndex:(int)iIndex;

@end
