//
//  SubSystemPage.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/29/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainView.h"
#import "HLToolBar.h"

@class IPOD_ViewController;
@class IPAD_ViewController;
@class CSubTab;
@class CMainTabViewController;
@class CSubTabViewOverlay;
@class CSystemBarButton;
@class CToolBarButton;
@class CHLSegment;
@class CSubTabView;

#define SUBTAB_HASTABSLEFT      0x00000001
#define SUBTAB_HASTABSRIGHT     0x00000002

/*============================================================================*/

@interface CSubSystemPage : UIView

/*============================================================================*/
{
    CSubTab                             *m_pSubTab;
    CMainTabViewController              *m_pMainPage;
    CSubTabView                         *m_pSubTabView;
    BOOL                                m_bFullScaleView;
    UIView                              *_m_pActivePage;
    //UIViewController                    *m_pMainViewController;
}

-(id)initWithFrame:(CGRect) rFrame MainTabPage:(CMainTabViewController*)pMainTabPage SubTab:(CSubTab*)pSubTab;
-(void)Init:(CSubTab*)pSubTab;
-(void)SetVisible:(BOOL)bVisible;
-(BOOL)ShouldRotate:(UIInterfaceOrientation)interfaceOrientation;
-(void)PrepareRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo;
-(void)NotifyRotationFrom:(UIInterfaceOrientation)ioFrom To:(UIInterfaceOrientation)ioTo;
-(void)NotifyRotationComplete:(UIInterfaceOrientation)ioNow;
-(void)LockHorizontalScroll:(BOOL)bNew;
-(void)PrepareScaleView:(BOOL)bFullScale;
-(void)PrepareLoseModal;
-(void)PrepareForZoom:(BOOL)bIn;
-(BOOL)WantOverviewOverlay;
-(BOOL)VisibleInOverview;
-(BOOL)IsVisible;
-(UIViewController*)ViewController;
-(CMainTabViewController*)MainTabPage;
-(CSubTabViewOverlay*)Overlay;
-(void)SetSubTabView:(CSubTabView*)pSubTabView;
-(void)InitSubTabViewOverlay;
-(void)BeginUpdateSubTabViewOverlay:(int)iDuration Every:(int)iRate;
-(void)UpdateOverlay;
-(CHLSegment*)GetPageCtrl;
-(void)UIInit;
-(void)TranslateNewPage:(UIView*)pNewPage OldPage:(UIView*)pOldPage ThisIndex:(int)iThisIndex LastIndex:(int)iLastIndex;
-(void)AnimationDidStop:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context;

-(CToolBarClientView*)CreateMenuView:(CGRect)rView ViewController:(IPAD_ViewController*)pCtlr Flags:(int)iFlags;
-(CSubTab*)SubTab;
-(BOOL)WantSystemMenu;


@end


/*============================================================================*/

@interface CSubSystemHeaderBarView : CToolBarClientView

/*============================================================================*/
{
    CMainTabViewController      *m_pMainPage;
    UILabel                     *m_pLabel;
    CSystemBarButton            *m_pArrowL;
    CSystemBarButton            *m_pArrowR;
    CToolBarButton              *m_pHomeButton;
    CHLSegment                  *m_pSegment;

    UIView                      *m_pAccessoryView;
    int                         m_iAccessoryDX;
    UIViewController            *m_pMainViewController;
}

-(id)initWithFrame:(CGRect) rFrame Label:(NSString*)pLabel Flags:(int)iFlags PageCtrl:(CHLSegment*)pCtrl;
-(void)SetAccessoryView:(UIView*)pView Width:(int)iDX;
-(UIView*)InsetLeft;
-(void)longPress:(UILongPressGestureRecognizer*)gesture;
-(void)SetMainViewController:(UIViewController*)mainViewController;
@end

/*============================================================================*/

@interface CSubSystemContainerPage : UIView

/*============================================================================*/
{
    UIView                      *m_pClientView;
}

-(id)initWithFrame:(CGRect)rFrame ClientView:(UIView*)pClientView;

@end

