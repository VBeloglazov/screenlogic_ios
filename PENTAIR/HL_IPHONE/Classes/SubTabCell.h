//
//  SubTabCell.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/11/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#define SUBTABCELL_ACCESSORYONLYWHENSELELECTED  0x00000001
#define SUBTABCELL_ACCESSORYNEVER               0x00000002


#import <UIKit/UIKit.h>

@class CSubTab;
@class CAccesoryView;
@class CUserTabIconView;

/*============================================================================*/

@protocol CTableCellDelegate

/*============================================================================*/

-(BOOL)CanDeselect:(UITableViewCell*)pCell;

@end

/*============================================================================*/

@interface CSubTabCell : UITableViewCell 

/*============================================================================*/
{
    CUserTabIconView        *m_pUserIconView;
	CAccesoryView           *m_pAccesory;
	UILabel                 *m_pLabel;
	UILabel                 *m_pAuxLabel;
    int                     m_iOptions;
    id<CTableCellDelegate>  m_pDelegate;
    CSubTab                 *m_pSubTab;
}

-(void)SetDelegate:(id)pDelegate;
-(void)SetSubTab:(CSubTab*)pSubTab;
-(void)SetUserIcon:(NSString*)pIconName;
-(void)SetEmbeddedIcon:(NSString*)pIconName;
-(void)SetAuxText:(NSString*)pAuxText;

@property (nonatomic, retain) UILabel *m_pLabel;
@property (nonatomic) int m_iOptions;

@end
