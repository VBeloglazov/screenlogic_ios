/*
--------------------------------------------------------------------------------

    HLSTRING.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLSTRING_H
#define HLSTRING_H
#ifndef HLCONFIG
#ifndef POOLCONFIG
#ifndef MFC_CSTRING

#if defined(GATEWAY) | defined(CRYSTALPAD) | defined(HLOSD)
    #define STRINGLIB
#endif

#ifndef IPHONE
    #include "tchar.h"
#endif

#include "hlapi.h"


    //
    // These defines specify if CHLString is a char string or a wchar_t string.
    //
    // IMPORTANT:
    //      - CHLString("Test") always produces a character string.
    //      - CHLString(_T("Test")) produces a char string if not UNICODE, or
    //        a wchar_t string if UNICODE.
    //      - CHLString(L"Test") always produces a wchar_t string.
    //
    // You can use the various functions in CHLString to convert back and forth
    // between wchar and char strings, but all the functions will ASSERT if you
    // try to combine two different types (like adding "Test" and L("Test)).
    //

    // Need to be reasonable here
    #define HL_MAX_STRING_SIZE  20000

    #ifdef UNICODE
        #ifdef GATEWAY
            #define HLSTRING_DEFAULT    CHLString::HLSTRING_CHAR
        #else
            #define HLSTRING_DEFAULT    CHLString::HLSTRING_UNICODE
        #endif
    #else
        #define HLSTRING_DEFAULT    CHLString::HLSTRING_CHAR
    #endif

    #ifdef IPHONE
        #define SIZEOF_WCHAR_T      4
    #else
        #define SIZEOF_WCHAR_T      2
    #endif

/*============================================================================*/

    class HL_API CHLString

/*============================================================================*/
{
public:
    enum CHLSTRING_TYPE
    {
        HLSTRING_UNICODE,
        HLSTRING_CHAR
    };

protected:
    CHLSTRING_TYPE              m_iType;                // Our type...
    char                        *m_pBuffer;             // Buffer (could be bigger than string)
    int                         m_iNBuffer;             // Buffer size

    #ifdef DEBUG
        wchar_t                     *m_pWBuffer;        // To see the text in a debugger...
    #endif

    static char                 *m_pNULL;               // Empty string.
    static char                 *m_pWNULL;              // Empty string.

//#ifdef STRINGLIB
//  CStringLib                  *m_pStringLib;
//#endif

public:
    //
    // Constructors and copy contstructors.
    //
    CHLString();

    CHLString(
    CHLSTRING_TYPE              iType );

    CHLString(                                          // Normal copy constructor
    const CHLString             &rhs );

    CHLString(                                          // From single char string
    const char                  *prhs );

    CHLString(                                          // From wide char string
    const wchar_t               *prhs );


    //
    // Constructors for adding two strings in one step.
    //
    CHLString(
    const char                  *prhs1,
    const char                  *prhs2 );

    CHLString(
    const wchar_t               *prhs1,
    const wchar_t               *prhs2 );

    //
    // Our one destructor.
    //
    ~CHLString();


    //
    // Operator =
    //
    CHLString                   &operator=(             // Standard operator =
    const CHLString             &rhs );

    CHLString                   &operator=(             // From single char string
    const char                  *prhs );

    CHLString                   &operator=(             // From wide char string
    const wchar_t               *prhs );


/*============================================================================*/
    //
    // Operator ==
    //
    inline BOOL                 operator==(             // Standard operator ==,
    const CHLString             &rhs )                  // for a CHLString on the left
                                                        // side of the ==.
/*============================================================================*/
{
    if (m_iType != rhs.m_iType)
    {
        if (rhs.m_iType == HLSTRING_UNICODE)
            ConvertTowchar_t();
        else
            ConvertTochar();
    }

    if (m_iNBuffer != rhs.m_iNBuffer)
        return FALSE;
    if (m_iType == HLSTRING_CHAR)
        return (strcmp(m_pBuffer, rhs.m_pBuffer) == 0);
    else
        return (wcscmp((wchar_t*) m_pBuffer, (wchar_t*) rhs.m_pBuffer) == 0);
}
/*============================================================================*/

    inline BOOL                 operator==(             // char version of above
    const char                  *prhs )

/*============================================================================*/
{
    LOGASSERT(prhs != NULL);
    if (m_iType == HLSTRING_CHAR)
        ConvertTochar();
    return (strcmp(m_pBuffer, prhs) == 0);
}
/*============================================================================*/

    inline BOOL                 operator==(             // wchar_t version of above
    const wchar_t               *prhs )

/*============================================================================*/
{

    LOGASSERT(prhs != NULL);
    if (m_iType != HLSTRING_UNICODE)
        ConvertTowchar_t();
    return (wcscmp((wchar_t*) m_pBuffer, prhs) == 0);
}
/*============================================================================*/

    inline friend BOOL          operator==(             // operator == for the case
    const char                  *prhs1,                 // when CHLString is on the
    CHLString                   &rhs2 )                 // right side of the ==

/*============================================================================*/
{
    LOGASSERT(prhs1 != NULL);
    LOGASSERT(rhs2.m_iType == HLSTRING_CHAR);
    return rhs2.operator==(prhs1);
}
/*============================================================================*/

    inline friend BOOL          operator==(              // wchar_t version
    const wchar_t               *prhs1,
    CHLString                   &rhs2 )

/*============================================================================*/
{
    LOGASSERT(prhs1 != NULL);
    LOGASSERT(rhs2.m_iType == HLSTRING_UNICODE);
    return rhs2.operator==(prhs1);
}
/*============================================================================*/

    //
    // Operator !=: call above.
    //
    inline BOOL                 operator!=(             // Standard operator !=
    const CHLString             &rhs )

/*============================================================================*/
{
    return !operator==(rhs);
}
/*============================================================================*/

    inline BOOL                 operator!=(             // char version
    const char                  *prhs )

/*============================================================================*/
{
    return !operator==(prhs);
}
/*============================================================================*/

    inline BOOL                 operator!=(             // wchar_t version
    const wchar_t               *prhs )

/*============================================================================*/
{
    return !operator==(prhs);
}


    //
    // Cast / conversion operators: All of these will change the string, not
    // return a new copy.
    //
    CHLSTRING_TYPE              Type(){ return m_iType; }

    inline BOOL                 Type(                   // This one can fail
    CHLSTRING_TYPE              iNew )
    {
        LOGASSERT((iNew == HLSTRING_CHAR) || (iNew == HLSTRING_UNICODE));
        if (m_iType == iNew)
            return TRUE;
        if (iNew == HLSTRING_CHAR)
            return ConvertTochar();
        else
            return ConvertTowchar_t();
    }

    BOOL                        ConvertToTCHAR();       // Conversion functions, which
    BOOL                        ConvertTochar();        // can fail...
    BOOL                        ConvertTowchar_t();

#ifdef GATEWAY
    BOOL                        ConvertTocharCodePage(
    int                         iCodePage);
#endif

    operator                    char*();                // Casting functions: if they fail,

    operator                    wchar_t*();             // an empty string is returned.

    //
    // Access operators: the [] operator returns a reference, so it can be used
    // as an lvalue (on the left side of an equation). It is type TCHAR, so it
    // can only be used to access the corresponding character type: char in normal
    // builds, wchar_t in UNICODE builds.
    //
    // Use the GetAt functions to access specific types.
    //
    TCHAR                       &operator[](int iIndex);

/*============================================================================*/

    inline char                 &charAt(
    int                         iIndex )

/*============================================================================*/
{
    ConvertTochar();
    LOGASSERT((iIndex >= 0) && (iIndex < m_iNBuffer));
    return m_pBuffer[iIndex];
}
/*============================================================================*/

    inline wchar_t              &wchar_tAt(
    int                         iIndex )

/*============================================================================*/
{
    ConvertTowchar_t();
    LOGASSERT(m_iType == HLSTRING_UNICODE);
    LOGASSERT((iIndex >= 0) && (iIndex < (m_iNBuffer/SIZEOF_WCHAR_T)));
    return ((wchar_t*) m_pBuffer)[iIndex];
}
/*============================================================================*/

    inline wchar_t              CharacterAt(
    int                         iIndex )

/*============================================================================*/
{
    if (iIndex < 0)
        return 0;
    if (m_iType == HLSTRING_UNICODE)
    {
        if (iIndex >= m_iNBuffer / SIZEOF_WCHAR_T)
            return 0;
        return ((wchar_t*) m_pBuffer)[iIndex];
    }

    if (iIndex >= m_iNBuffer)
        return 0;
    return m_pBuffer[iIndex];
}
/*============================================================================*/

    //
    // Operator +: adding strings together.
    //
    // Note that you should not be adding strings of different types: convert the
    // strings to the same type first.
    //
    inline CHLString            operator+(              // sText1 + sText2, or
    CHLString                   &rhs )                  // sText1 + _T("..."), but not
                                                        // _T("...") + sText: see below
/*============================================================================*/
{
    if (m_iType != rhs.m_iType)
    {
        if (rhs.m_iType == HLSTRING_UNICODE)
            ConvertTowchar_t();
        else
            ConvertTochar();
    }

    if (m_iType == HLSTRING_CHAR)
        return CHLString(m_pBuffer, rhs.m_pBuffer);
    else
        return CHLString((wchar_t*) m_pBuffer, (wchar_t*) (rhs.m_pBuffer));
}
/*============================================================================*/

    inline CHLString            operator+(              // char version
    const char                  *prhs )

/*============================================================================*/
{
    if (m_iType != HLSTRING_CHAR)
        ConvertTochar();
    return CHLString(m_pBuffer, prhs);
}
/*============================================================================*/

    inline CHLString            operator+(              // wchar_t version
    const wchar_t               *prhs )

/*============================================================================*/
{
    if (m_iType == HLSTRING_UNICODE)
        ConvertTowchar_t();
    return CHLString((wchar_t*) m_pBuffer, prhs);
}
/*============================================================================*/

    inline friend CHLString     operator+(              // _T("...") + sText1
    const char                  *prhs1,
    CHLString                   &rhs2 )

/*============================================================================*/
{
    LOGASSERT(rhs2.m_iType == HLSTRING_CHAR);
    return CHLString(prhs1, rhs2.m_pBuffer);
}
/*============================================================================*/

    inline friend CHLString     operator+(              // wchar_t version
    const wchar_t               *prhs1,
    CHLString                   &rhs2 )

/*============================================================================*/
{
    LOGASSERT(rhs2.m_iType == HLSTRING_UNICODE);
    return CHLString(prhs1, (wchar_t*) rhs2.m_pBuffer);
}
/*============================================================================*/

    //
    // Operator +: adding a char to a string.
    //
    inline CHLString            operator+(              // char version
    const char                  crhs )

/*============================================================================*/
{
    if (m_iType != HLSTRING_CHAR)
        ConvertTochar();

    char temp[2];
    temp[0] = crhs;
    temp[1] = '\0';
    return CHLString(m_pBuffer, temp);
}
/*============================================================================*/

    inline CHLString            operator+(              // wchar_t version
    const wchar_t               crhs )

/*============================================================================*/
{
    if(m_iType != HLSTRING_UNICODE)
        ConvertTowchar_t();
    wchar_t temp[2];
    temp[0] = crhs;
    temp[1] = L'\0';
    return CHLString((wchar_t*) m_pBuffer, temp);
}
/*============================================================================*/

    //
    // Operator +=: concatenating strings. Again, don't mix types.
    //
    inline CHLString            &operator+=(            // += sText2, or
    CHLString                   &rhs )                  // += _T("...")

/*============================================================================*/
{
    if (m_iType != rhs.m_iType)
    {
        if (rhs.m_iType == HLSTRING_UNICODE)
            ConvertTowchar_t();
        else
            ConvertTochar();
    }

    if (m_iType == HLSTRING_CHAR)
        return operator+=(rhs.m_pBuffer);
    else
        return operator+=((wchar_t*) rhs.m_pBuffer);
}
/*============================================================================*/

    CHLString                   &operator+=(            // char version
    const char                  *prhs );

    CHLString                   &operator+=(            // wchar_t version
    const wchar_t               *prhs );

/*============================================================================*/

    //
    // Operator +=: concatenating a char to a string.
    //
    CHLString                   &operator+=(            // char version
    const char                  crhs )

/*============================================================================*/
{
    char temp[2];
    temp[0] = crhs;
    temp[1] = '\0';
    return operator+=(temp);
}
/*============================================================================*/

    CHLString                   &operator+=(            // wchar_t version
    const wchar_t               crhs )

/*============================================================================*/
{
    wchar_t temp[2];
    temp[0] = crhs;
    temp[1] = L'\0';
    return operator+=(temp);
}
/*============================================================================*/

    //
    // String length and related: the length is always the number of characters...
    //
    inline void                 Empty()

/*============================================================================*/
{
    LOGASSERT(m_iNBuffer != 0);
    if (m_iType == HLSTRING_CHAR)
    {
        if (!IsNULL(m_pBuffer))
        {
            delete [] m_pBuffer;
            m_pBuffer = NULL;
        }
    }
    else
    {
        if (!IsNULL(m_pBuffer))
        {
            delete [] m_pBuffer;
            m_pBuffer = NULL;
        }
    }
    InitNULL(m_iType);
}
/*============================================================================*/

    inline BOOL                 IsEmpty()

/*============================================================================*/
{
    if (m_iType == HLSTRING_CHAR)
        return (m_iNBuffer == 1);
    else
        return (m_iNBuffer == SIZEOF_WCHAR_T);
}
/*============================================================================*/

    inline int                  GetCharBufferLength()

/*============================================================================*/
{
    return GetLength();
}
/*============================================================================*/

    inline int                  length()

/*============================================================================*/
{
    return GetLength();
}
/*============================================================================*/

    inline int                  GetLength()

/*============================================================================*/
{
    LOGASSERT(m_pBuffer != NULL);
    LOGASSERT(m_iNBuffer != 0);
    if (m_iType == HLSTRING_CHAR)
        return m_iNBuffer - 1;
    else
        return (m_iNBuffer / SIZEOF_WCHAR_T) - 1;
}

    void                        *GetBuffer(         // Pointer to data or NULL if fails
    int                         iLength );          // Could be char or wchar_t

    virtual char                *GetBufferCHAR(
    int                         iLength );

    wchar_t                     *GetBufferWCHAR(
    int                         iLength );

    TCHAR                       *GetBufferTCHAR(
    int                         iLength );

    void                        ReleaseBuffer(
    int                         iLength );


    //
    // Other string manipulation routines
    //
    #ifdef IPHONE
        int                         Format(
        const char                  *pFormat,
        ... );
    #else
        int                         Format(
        char                        *pFormat,
        ... );
    #endif

#ifndef MAC_OS
    int                         Format(
    wchar_t                     *pFormat,
    ... );
#endif

    int                         Find(
    int                         iChar,                      // Assumed to be of same type
    int                         iStart = 0 );               // as current string

    int                         ReverseFind(
    int                         iChar );                    // Like above...

    CHLString                   Left(
    int                         iCount );

    CHLString                   Mid(
    int                         iFirst,
    int                         iCount );

    CHLString                   Right(
    int                         iCount );


    //
    // Trim left / trim right: pass in a list of chars to use for trimming.
    // The list is in a string: same type as the CHLString.
    //
    void                        TrimLeft(
    const char                  *prhs);

    void                        TrimLeft(
    const wchar_t               *prhs);

    void                        TrimRight(
    const char                  *prhs );

    void                        TrimRight(
    const wchar_t               *prhs );

    void                        Replace(
    char                        cOld,
    char                        cNew );

    void                        MakeUpper();
    void                        MakeLower();

#ifdef GATEWAY
    void                        UTF8ToASCII();
    void                        NCRToASCII();
#endif

    void                        CopyFrom(
    char                        *pStart,
    char                        *pEnd );

    //
    // Compares: 3 flavors depending on situation. Each will ASSERT if you pass
    // in the wrong type of string.
    //
    int                         Compare(
    const CHLString             &rhs );

    int                         Compare(
    const char                  *prhs );

    int                         Compare(
    const wchar_t               *prhs );

    int                         CompareNoCase(
    const CHLString             &rhs );

    int                         CompareNoCase(
    const char                  *prhs );

    int                         CompareNoCase(
    const wchar_t               *prhs );

    void                        Reverse();

    BOOL                        IsNumeric();
    static BOOL                 IsNumeric(wchar_t c); // 05/15/2012 VB
/*============================================================================*/

    inline BOOL                 Init(
    CHLSTRING_TYPE              iType,
    int                         iNBytes )

/*============================================================================*/
{
    //
    // Basic init: set the type of char, and try to get the memory. If it fails,
    // m_pBuffer will be NULL and m_iNBuffer will be 0.
    //
    LOGASSERT((iType == HLSTRING_CHAR) || (iType == HLSTRING_UNICODE));
    LOGASSERT(iNBytes != 0);
    m_iType = iType;

    if (iNBytes > HL_MAX_STRING_SIZE)
    {
        //LOGASSERT(FALSE);
        return FALSE;
    }

    m_pBuffer = new char[iNBytes];
    if (m_pBuffer == NULL)
    {
        InitNULL(iType);
        return FALSE;
    }

    #ifdef DEBUG
    m_pWBuffer = (wchar_t*) m_pBuffer;
    #endif
    m_iNBuffer = iNBytes;
    return TRUE;
}
protected:
/*============================================================================*/

    //
    // Helper functions: note that these do not check for current value of m_pBuffer.
    // The caller needs to take care of deleting any current memory.
    //
    inline void                 InitNULL(
    CHLSTRING_TYPE              iType )

/*============================================================================*/
{
    //m_pStringLib = NULL;
    #ifdef DEBUG
        // !***!
        //if (m_pBuffer != NULL)
        //{
        //    ASSERT(IsNULL(m_pBuffer)); or m_pWide buffer?
        //}
    #endif

    m_iType     = iType;
    if (m_iType == HLSTRING_CHAR)
    {
        m_pBuffer   = m_pNULL;
        m_iNBuffer  = 1;
    }
    else
    {
        m_pBuffer   = m_pWNULL;
        m_iNBuffer  = SIZEOF_WCHAR_T;
    }
    #ifdef DEBUG
        m_pWBuffer = (wchar_t*) m_pBuffer;
    #endif
}
/*============================================================================*/

    inline BOOL                 IsNULL(
    char                        *pBuffer )

/*============================================================================*/
{
    return ((pBuffer == m_pNULL) || (pBuffer == m_pWNULL));
}
/*============================================================================*/

    inline BOOL                 Init(
    CHLSTRING_TYPE              iType,
    int                         iNBytes,
    const char                  *pBytes )

/*============================================================================*/
{
    // Like above, but we also copy over the data.
    LOGASSERT((iType == HLSTRING_CHAR) || (iType == HLSTRING_UNICODE));
    LOGASSERT(iNBytes != 0);
    Empty();
    m_iType = iType;

    if (iNBytes > HL_MAX_STRING_SIZE || iNBytes <= 0)
    {
        //LOGASSERT(FALSE);
        return FALSE;
    }

    if (iType == HLSTRING_CHAR && iNBytes == 1)
    {
        InitNULL(iType);
        return TRUE;
    }
    if (iType == HLSTRING_UNICODE && iNBytes == SIZEOF_WCHAR_T)
    {
        InitNULL(iType);
        return TRUE;
    }

    m_pBuffer = new char[iNBytes];
    if (m_pBuffer == NULL)
    {
        InitNULL(iType);
        return FALSE;
    }

    #ifdef DEBUG
        m_pWBuffer = (wchar_t*) m_pBuffer;
    #endif
    m_iNBuffer = iNBytes;
    memcpy(m_pBuffer, pBytes, m_iNBuffer);
    return TRUE;
}
/*============================================================================*/
};


#endif
#endif
#endif
#endif