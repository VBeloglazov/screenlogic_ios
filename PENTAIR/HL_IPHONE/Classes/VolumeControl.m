//
//  VolumeControl.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/3/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "VolumeControl.h"
#import "VolumeButton.h"
#import "VolumeLabel.h"
#import "AudioZone.h"
#import "MainView.h"
#import "crect.h"
#import "HLToolBar.h"
#import "hlcomm.h"
#import "hlm.h"
#import "shader.h"
#import "CustomPage.h"

@implementation CVolumeControl


/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Zone:(CAudioZone*)pZone

/*============================================================================*/
{
    if (self == [super initWithFrame:rFrame])
    {
        m_pDn = [[CVolumeButton alloc] initWithZone:pZone ImageName:@"VOL_DN.png"];
        m_pUp = [[CVolumeButton alloc] initWithZone:pZone ImageName:@"VOL_UP.png"];
        [m_pDn addTarget:pZone action:@selector(VolDownEvent:) forControlEvents:UIControlEventTouchDown];
        [m_pUp addTarget:pZone action:@selector(VolUpEvent:) forControlEvents:UIControlEventTouchDown];

        rFrame = self.bounds;

        [m_pDn Orientation:TOOLBAR_BTM_LEFT];
        [m_pUp Orientation:TOOLBAR_BTM_RIGHT];

//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        {
            [m_pDn IconFormat:HL_ICONLEFTHALF];
            [m_pUp IconFormat:HL_ICONRIGHTHALF];
//        }

        [self addSubview:m_pDn];
        [self addSubview:m_pUp];
        [m_pDn release];
        [m_pUp release];

        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:[CMainView TEXT_SIZE_SIDEBAR]];
        [m_pLabel setText:@"VOLUME"];
        [self addSubview:m_pLabel];
        [m_pLabel release];
    }

    return self;
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rThis = self.bounds;
    CGRect rUp = m_pUp.frame;
    CGRect rDn = m_pDn.frame;
    int iDXBtn = rThis.size.width / 2;
    rDn = [CRect BreakOffLeft:&rThis DX:iDXBtn];
    rUp = rThis;
    [m_pUp setFrame:rUp];
    [m_pDn setFrame:rDn];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
        [m_pLabel setFrame:self.bounds];
//    }
//    else
//    {
//        CGRect rText = [m_pUp TextRect];
//        rText.origin.x = 0;
//        rText.size.width = self.bounds.size.width;
//        [m_pLabel setFrame:rText];
//    }
}
/*============================================================================*/

-(void)FrameStyle:(int)iNew

/*============================================================================*/
{
    [m_pUp FrameStyle:iNew];
    [m_pDn FrameStyle:iNew];
}
@end
