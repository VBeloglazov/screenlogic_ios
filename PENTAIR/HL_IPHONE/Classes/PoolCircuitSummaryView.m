//
//  PoolCircuitSummaryView.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 5/13/11.
//  Copyright 2011 HomeLogic. All rights reserved.
//

#import "PoolCircuitSummaryView.h"
#import "AppDelegate.h"
#import "NSPoolConfig.h"
#import "NSPoolCircuit.h"
#import "hlm.h"
#import "MainView.h"
#import "crect.h"
#import "CustomPage.h"

#define TEXT_SIZE                       16
#define DY_ITEM                         30
#define DY_ITEM_MAX                     40


@implementation CPoolCircuitIndicatorView

/*============================================================================*/

-(id)initWithFrame:(CGRect)frame Circuit:(CNSPoolCircuit*)pCircuit

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self)
    {
        m_pCircuit = pCircuit;
        [m_pCircuit retain];


        m_pLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [CCustomPage InitStandardLabel:m_pLabel Size:TEXT_SIZE];
        [m_pLabel setText:[pCircuit Name]];
        [self addSubview:m_pLabel];
        [m_pLabel release];

        m_iState = 0xFF;
        [self Update];
    }

    return self;
}
/*============================================================================*/

-(void)Update

/*============================================================================*/
{
    int iState = [m_pCircuit State];
    if (iState == m_iState)
        return;
    m_iState = iState;
    if (iState != 0)
    {
        [m_pLabel setTextColor:[UIColor greenColor]];
    }
    else
    {
        [m_pLabel setTextColor:[UIColor lightGrayColor]];
    }

    [self setNeedsDisplay];
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    [m_pLabel setFrame:self.bounds];
}
/*============================================================================*/

-(void)drawRect:(CGRect)rect

/*============================================================================*/
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    int iR = 4;
    int iX = rect.origin.x+2;
    int iY = rect.origin.y+2;
    int iDX = rect.size.width-4;
    int iDY = rect.size.height-4;

    UIColor *pColor1 = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0];
    UIColor *pColor2 = [UIColor grayColor];
    if (m_iState != 0)
    {
        pColor1 = [UIColor grayColor];
        pColor2 = [UIColor lightGrayColor];
    }
    
    CGContextBeginPath (context);
    CGContextMoveToPoint(context, iX+iR, iY);                                         // Start Point
    CGContextAddArcToPoint(context, iX+iDX, iY, iX+iDX, iY+iDY, iR);                  // Top Line-UR
    CGContextAddArcToPoint(context, iX+iDX, iY+iDY, iX+iDX-iR, iY+iDY, iR);         // Right Line-LR
    CGContextAddArcToPoint(context, iX,     iY+iDY, iX, iY+iDY-iR, iR);             // Bottom Line-LL
    CGContextAddArcToPoint(context, iX, iY, iX+iR, iY, iR);                         // Bottom Line-LL

    CGContextSetFillColorWithColor(context, pColor1.CGColor);
    CGContextFillPath(context);

    CGContextSetStrokeColorWithColor(context, pColor2.CGColor);
    CGContextStrokePath(context);
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    [m_pCircuit release];
    [super dealloc];
}
@end

@implementation CPoolCircuitSummaryView

/*============================================================================*/

- (id)initWithFrame:(CGRect)frame

/*============================================================================*/
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        #ifdef PENTAIR
            CAppDelegate *pDelegate = (CAppDelegate*)[UIApplication sharedApplication].delegate;
            m_pConfig = [pDelegate PoolConfig];
        #else
            m_pConfig = [[CNSPoolConfig alloc] init ];
        #endif

        m_pControls = [[NSMutableArray alloc] init];

        NSMutableArray *pList = [[[NSMutableArray alloc] init] autorelease];

        for (int i = POOLINT_POOL; i <= POOLINT_LIGHT; i++)
        {
            [m_pConfig LoadCircuitsByInterface:pList Interface:i];
        }

        [self MoveFirst:pList CircuitType:POOLCIRCUIT_CLEANER ];
        [self MoveFirst:pList CircuitType:POOLCIRCUIT_POOL ];
        [self MoveFirst:pList CircuitType:POOLCIRCUIT_SPA ];

        for (int i = 0; i < [pList count]; i++)
        {
            CNSPoolCircuit *pCircuit = (CNSPoolCircuit*)[pList objectAtIndex:i];
            CPoolCircuitIndicatorView *pView = [[CPoolCircuitIndicatorView alloc] initWithFrame:CGRectZero Circuit:pCircuit];
            [m_pControls addObject:pView];
            [self addSubview:pView];
            [pView release];
        }
        
        [m_pConfig AddSink:self];
    }
    return self;
}
/*============================================================================*/

- (void)dealloc

/*============================================================================*/
{
    [m_pConfig RemoveSink:self];
    [m_pControls release];

    #ifndef PENTAIR
        [m_pConfig release];
    #endif

    [super dealloc];
}
/*============================================================================*/

-(void)MoveFirst:(NSMutableArray*)pList CircuitType:(int)iType

/*============================================================================*/
{
    for (int i = 0; i < [pList count]; i++)
    {
        CNSPoolCircuit *pCircuit = (CNSPoolCircuit*)[pList objectAtIndex:i];
        if ([pCircuit Function] == iType)
        {
            [pList exchangeObjectAtIndex:i withObjectAtIndex:0];
            return;
        }
    }
}
/*============================================================================*/

-(void)layoutSubviews

/*============================================================================*/
{
    CGRect rBounds = [self ClientRect:RECT_STDINSET];

    int iNItems = [m_pControls count];

    int iNRows = MIN(iNItems, rBounds.size.height / DY_ITEM);
    iNRows = MAX(1, iNRows);
    int iNCols = 1;
    while (iNCols * iNRows < iNItems)
        iNCols++;
        
    iNRows = iNItems / iNCols;
    while (iNRows * iNCols < iNItems)
        iNRows++;

    [CRect CenterDownToY:&rBounds DY:iNRows * DY_ITEM_MAX];

    int iRow = 0;
    int iCol = 0;
    for (int i = 0; i < iNItems; i++)
    {
        CGRect rCol = [CRect CreateXSection:rBounds Index:iCol Of:iNCols];
        CGRect rCircuit = [CRect CreateYSection:rCol Index:iRow Of:iNRows];
        CPoolCircuitIndicatorView *pView = (CPoolCircuitIndicatorView*)[m_pControls objectAtIndex:i];
        [pView setFrame:rCircuit];
        iRow++;
        if (iRow >= iNRows)
        {
            iRow = 0;
            iCol++;
        }
    }
}
/*============================================================================*/

-(void)Notify:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iNItems = [m_pControls count];
    for (int i = 0; i < iNItems; i++)
    {
        CPoolCircuitIndicatorView *pView = (CPoolCircuitIndicatorView*)[m_pControls objectAtIndex:i];
        [pView Update];
    }
}
@end
