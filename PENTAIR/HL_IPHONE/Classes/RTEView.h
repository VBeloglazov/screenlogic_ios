//
//  CRTEView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNSMSG;
@class CTimeView;

/*============================================================================*/

@interface CRunTimeEvent : NSObject

/*============================================================================*/
{
    NSDate                              *m_pDateStart;
    NSDate                              *m_pDateEnd;
}

-(id)initWithDate:(NSDate*)pStart End:(NSDate*)pEnd;

-(NSDate*)StartDate;
-(NSDate*)EndDate;
-(int)OverlapWith:(NSDate*)pStart End:(NSDate*)pEnd;

@end


/*============================================================================*/

@interface CRTESeries : NSObject

/*============================================================================*/
{
    UIColor                             *m_pRGB;
    NSMutableArray                      *m_pPoints;
    
    NSDate                              *m_pStartDate;
}

-(id)initWithColor:(UIColor*)pColor;
-(void)ReadFrom:(CNSMSG*)pMSG;
-(void)ProcessState:(NSDate*)pDate True:(BOOL)bTrue;
-(void)AddRTE:(NSDate*)pStart End:(NSDate*)pEnd;

-(NSMutableArray*)PointList;
-(UIColor*)Color;

@end

/*============================================================================*/

@interface CRTEView : UIView 

/*============================================================================*/
{
    CRTESeries                          *m_pSeries;
    CTimeView                           *m_pTimeView;
    int                                 m_iXOrigin;
}

-(id)initWithFrame:(CGRect) rFrame XOrigin:(int)iXOrigin Series:(CRTESeries*)pSeries TimeView:(CTimeView*)pTimeView;
-(void)SetXOrigin:(int)iXOrigin;

@end
