//
//  MP3ItemCell.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/24/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainView.h"

@class CUserTabIconView;
@class CAccesoryView;
@class CHLButton;
@class CMP3Menu;

#define MP3ITEMCELL_NOACCESSORY     0x00000001

/*============================================================================*/

@interface CMP3ItemCell : UITableViewCell 

/*============================================================================*/
{
    int                     m_iOptions;
    int                     m_iPlayOptions;
    CUserTabIconView        *m_pUserIconView;
	CAccesoryView           *m_pAccesory;
	UILabel                 *m_pLabel;

    CHLButton               *m_pPlay;
    CHLButton               *m_pAdd;
    
    UILabel                 *m_pAltLabel;
    UIView                  *m_pImageView;
}

- (id)initWithFrame:(CGRect)aRect MP3Menu:(CMP3Menu*)pMenu reuseIdentifier:(NSString *)identifier Options:(int)iOptions PlayOptions:(int)iPlayOptions;
- (void)SetAltText:(NSString*)pAltText;
- (void)SetImage:(CGImageRef)pImage;

@property (nonatomic, retain) UILabel *m_pLabel;

@end
