//
//  AudioButton.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/14/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "AudioButton.h"
#import "NSQuery.h"

#define CMD_PRESS   1000
#define CMD_RELEASE 1001

@implementation CAudioButton

/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame 
                    IconFormat:(int)iIconFormat
                    Style:(int)iStyle 
                    Text:(NSString*)pText 
                    TextSize:(int)iTextSize 
                    TextColor:(UIColor*)RGBText 
                    Color:(UIColor*)RGBFace 
                    Icon:(NSString*)sIcon 
                    Data:(int)iData
                    Flags:(int)iFlags
                    Function:(int)iFunction
                    ZoneID:(int)iZoneID

/*============================================================================*/
{
    self = [super initWithFrame:rFrame IconFormat:iIconFormat Style:iStyle Text:pText TextSize:iTextSize TextColor:RGBText Color:RGBFace Icon:sIcon];
    if (self)
    {
        m_iData = iData;
        m_iFlags = iFlags;
        m_iFunction = iFunction;
        m_iZoneID = iZoneID;
        [self SetCommandID:CMD_PRESS ReleaseID:CMD_RELEASE Responder:self];
    }
    
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(void)OnCommand:(int)iCommandID

/*============================================================================*/
{
    if (m_iFlags & CONTROL_FLAG_DEFAULT_BEHAVIOUR)
    {
        int iType = KEY_DOWN;
        if (iCommandID == CMD_RELEASE)
        {
//            [self UnHighlight];
            iType = KEY_UP;
        }
        else 
        {
        }

        CNSQuery *pQ = NULL;
        if (m_iFlags & CONTROL_FLAG_HATBUTTON)
            pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_EXECHATQ];
        else
            pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_EXECUNIVERSALQ];

        [pQ autorelease];

        [pQ PutInt:m_iZoneID];
        [pQ PutInt:m_iFunction];
        [pQ PutInt:iType];
        [pQ SendMessage];
        
    }
    else 
    {
        if (iCommandID == CMD_PRESS)
        {
            CNSQuery *pQ = [[CNSQuery alloc] initWithQ:HLM_AUDIO_EXECUTEAUDIOBUTTONQ];
            [pQ autorelease];
            [pQ PutInt:m_iData];
            [pQ PutInt:m_iZoneID];
            [pQ SendMessage];
        }
        else 
        {
//            [self UnHighlight];
        }

    }
}

@end
