/*
--------------------------------------------------------------------------------

    HLSOCK.H

    Copyright 2002 HomeLogic, Marblehead, MA 01945

--------------------------------------------------------------------------------
*/
#ifndef HLSOCK_H
#define HLSOCK_H

#include "hlapi.h"

    //
    // Defines for ResetZeroConfig
    //
    #ifdef UNDER_CE
        #define INTF_ALL            0x000fffff
        #define INTF_ALL_OIDS       0x000fff00

        #define INTF_CTLFLAGS       0x00000010
        #define INTFCTL_CM_MASK     0x0007   // mask for the configuration mode (NDIS_802_11_NETWORK_INFRASTRUCTURE value)
        #define INTFCTL_ENABLED     0x8000   // zero conf enabled for this interface
        #define INTFCTL_FALLBACK    0x4000   // attempt to connect to visible non-preferred networks also
        #define INTFCTL_OIDSSUPP    0x2000   // 802.11 OIDs are supported by the driver/firmware

        // [P]RAW_DATA: generic description of a BLOB
        typedef struct
        {
            DWORD   dwDataLen;
        #if defined(MIDL_PASS)
            [unique, size_is(dwDataLen)] LPBYTE pData;
        #else
            LPBYTE  pData;
        #endif
        } RAW_DATA, *PRAW_DATA;

        typedef struct
        {    
        #if defined(MIDL_PASS)
            [string] LPWSTR wszGuid;
        #else
            LPWSTR          wszGuid;
        #endif
        #if defined(MIDL_PASS)
            [string] LPWSTR wszDescr;
        #else
            LPWSTR          wszDescr;
        #endif
            ULONG           ulMediaState;
            ULONG           ulMediaType;
            ULONG           ulPhysicalMediaType;
            INT             nInfraMode;
            INT             nAuthMode;
            INT             nWepStatus;
            DWORD           dwCtlFlags;     // control flags (see INTFCTL_* defines)
            RAW_DATA        rdSSID;         // encapsulates the SSID raw binary
            RAW_DATA        rdBSSID;        // encapsulates the BSSID raw binary
            RAW_DATA        rdBSSIDList;    // encapsulates one WZC_802_11_CONFIG_LIST structure
            RAW_DATA        rdStSSIDList;   // encapsulates one WZC_802_11_CONFIG_LIST structure
            RAW_DATA        rdCtrlData;     // data for various control actions on the interface

            BOOL            bInitialized;   //  To track caller that freeing
                                            //  the same structure more than one time..
        } INTF_ENTRY, *PINTF_ENTRY;

        typedef DWORD (*PFN_WZCQueryInterface)(LPWSTR pSrvAddr,DWORD dwInFlags,PINTF_ENTRY pIntf,LPDWORD pdwOutFlags);
        typedef DWORD (*PFN_WZCSetInterface)(LPWSTR pSrvAddr,DWORD dwInFlags,PINTF_ENTRY pIntf,LPDWORD pdwOutFlags);



    #endif


    //
    // Functions to help manage addresses etc.
    //
    #ifdef MAC_OS
        void                    SetIP(
        SOCKADDR_IN             *pAddr,
        BYTE                    by1,
        BYTE                    by2,
        BYTE                    by3,
        BYTE                    by4 );

        BYTE                    GetIP(
        SOCKADDR_IN             *pAddr,
        int                     iDigit );
    #endif

    //
    // Functions to help manage addresses etc.
    //
#ifndef MAC_OS
    int                         CountAdapters();
    
    HL_API BOOL                 HLLoadIPAddress(
    int                         iIndex,
    SOCKADDR_IN                 *pIP );
#endif

    HL_API BOOL                 HLLoadIPAddress(
    SOCKADDR_IN                 *pIP );


    #if defined(GATEWAY) | defined(MONITOR)
        HL_API BOOL                 HLLoadIPInfo(
        SOCKADDR_IN                 *pIPAddress,
        SOCKADDR_IN                 *pSubNet,
        SOCKADDR_IN                 *pGateway,
        SOCKADDR_IN                 *pDNS );

        BOOL                        HLSetIPInfo(
        SOCKADDR_IN                 *pIPAddress,
        SOCKADDR_IN                 *pSubNet,
        SOCKADDR_IN                 *pGateway,
        SOCKADDR_IN                 *pDNS );

        HKEY                        NetworkAdapterKey();

        HKEY                        IterateFindNetworkKey(
        HKEY                        keyStart,
        SOCKADDR_IN                 *pActiveIP );

        BOOL                        IsNetworkKey(
        HKEY                        keyTry,
        SOCKADDR_IN                 *pActiveIP );

        BOOL                        GetAddress(
        HKEY                        keyTry,
        CHLString                   sParam,
        SOCKADDR_IN                 *pAddress );

        BOOL                        SetAddress(
        HKEY                        keyTry,
        CHLString                   sParam,
        SOCKADDR_IN                 *pAddress,
        BOOL                        bMultiSz );
    #endif

    HL_API CHLString            HLFormatAddress(
    SOCKADDR_IN                 *pIP );

    HL_API CHLString            HLFormatAddress(
    DWORD                       dwIP );

#ifndef MAC_OS
    HL_API BOOL                 GetNetInfo(
    int                         iAdapter,
    DWORD                       *pdwMask,
    DWORD                       *pdwLocalIP );

    HL_API DWORD                GetPrimaryIP();
#endif

    HL_API BOOL                 ReadyToSend(
    SOCKET                      hSocket );

    HL_API BOOL                 ReadyToReceive(
    SOCKET                      hSocket );

    HL_API BOOL                 WaitSocketReadState(
    SOCKET                      hSocket,
    DWORD                       dwMSEC );

    HL_API BOOL                 WaitSocketWriteState(
    SOCKET                      hSocket,
    DWORD                       dwMSEC );

    #if defined(GATEWAY) | defined(RELAYAGENT) | defined(WSERVER) | defined(RADIOSERVER) | defined(HLUPDATE) | defined(HLSERVICE) | defined(HLCONFIG) | defined(HLSTART) | defined(HLSURVEY) | defined(CMXUPDATE)
        HL_API BOOL                 AsyncConnect(
        SOCKET                      hSocket,
        SOCKADDR_IN                 *pAddress,
        BOOL                        *pbAbort,
        DWORD                       dwMaxMSEC );
    #endif

    BOOL                        RestartZeroConfig();
    
    int                         GetWirelessStrength();
    int                         GetWirelessStrength(TCHAR *pDeviceName);

    void                        SetAddress(
    SOCKADDR_IN                 *pAddress,
    BYTE                        by1,
    BYTE                        by2, 
    BYTE                        by3,
    BYTE                        by4,
    WORD                        wPort );

    HL_API void                 GetAddress(
    SOCKADDR_IN                 *pAddress,
    BYTE                        *by1,
    BYTE                        *by2, 
    BYTE                        *by3,
    BYTE                        *by4,
    WORD                        *wPort );

    HL_API int                  SetNonBlocking(SOCKET hSocket);
    HL_API int                  SetBlocking(SOCKET hSocket);

    HL_API void                 CloseSocket(
    SOCKET                      hSocket );

    BOOL                        HLRecvFrom(
    SOCKET                      hSocket,
    char                        *pBuffer,
    int                         iNMaxRecv,
    SOCKADDR_IN                 *pAddressFrom );

    int                         HLRecvFromUpTo(
    SOCKET                      hSocket,
    char                        *pBuffer,
    int                         iNMaxRecv,
    SOCKADDR_IN                 *pAddressFrom );

    BOOL                        IsAddress(
    CHLString                   sTest );

    BOOL                        ParseAddress(
    char                        *pData,
    CHLString                   sKey,
    BYTE                        *pBy1,
    BYTE                        *pBy2,
    BYTE                        *pBy3,
    BYTE                        *pBy4,
    UINT                        *pPort );

    HL_API DWORD                StringToIP(
    char                        *pString );

#endif

