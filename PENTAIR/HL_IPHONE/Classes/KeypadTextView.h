//
//  KeypadTextView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 2/2/09.
//  Copyright 2009 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSSocketSink.h"

/*============================================================================*/

@interface CKeypadTextView : UIView < CNSSocketSink >

/*============================================================================*/
{
    int                             m_iID;
    int                             m_iTextSize;
    UIColor                         *m_pRGBText;
    UILabel                         *m_pLabels[5];
}

-(id)initWithFrame: (CGRect)rView ID:(int)iID TextSize:(int)iTextSize TextColor:(UIColor*)pRGBText;
-(void)InitComm;

@end
