//
//  SecurityHistory.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/31/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SecurityHistory.h"
#import "SecPartition.h"
#import "timedataview.h"
#import "timebar.h"
#import "LegendItem.h"
#import "SecurityHistoryDataView.h"
#import "SecZone.h"


@implementation CSecurityHistoryView
/*============================================================================*/

-(id)initWithFrame:(CGRect)rFrame Partition:(CSecPartition*)pPartition

/*============================================================================*/
{
    if (self = [super initWithFrame:rFrame]) 
    {
        // Initialization code
        m_pPartition = pPartition;
        m_bStaticScale = TRUE;

        CGRect rData = CGRectMake(-rFrame.size.width, 0, 3 * rFrame.size.width, [self DataViewDY]);
        m_pTimeDataView = [self CreateTimeDataView:rData LastView:NULL];
        [self insertSubview:m_pTimeDataView belowSubview:m_pTimeBar];

        CGRect rScale = CGRectMake(0, 0, 100, [self DataViewDY]);
        m_pScaleView = [[UIView alloc] initWithFrame:rScale];
        m_pScaleView.backgroundColor = [UIColor clearColor];
        [self insertSubview:m_pScaleView belowSubview:m_pTimeBar];

        int iDYItem = [CSecurityHistoryView DYItem];
        
        for (int i = 0; i < 4; i++)
        {
            NSString *pLabel = NULL;
            UIColor *pRGB = NULL;
            switch (i)
            {
            case 0: { pLabel = @"Armed";        pRGB = [UIColor colorWithRed:.25 green:.25 blue:1.0 alpha:1.0]; }   break;
            case 1: { pLabel = @"Not Ready";    pRGB = [UIColor colorWithRed:1   green:1   blue:0   alpha:1.0]; }   break;
            case 2: { pLabel = @"Alarm";        pRGB = [UIColor colorWithRed:1   green:0   blue:1.0 alpha:1.0]; }   break;
            case 3: { pLabel = @"Fire";         pRGB = [UIColor colorWithRed:1   green:0   blue:0   alpha:1.0]; }   break;
            }

            CGRect rItem = CGRectMake(0, i*iDYItem+SECURITY_ITEM_INSET+2, rScale.size.width, iDYItem-2*SECURITY_ITEM_INSET-4);
            CLegendItem *pItem = [[CLegendItem alloc] initWithFrame:rItem Color:pRGB Text:pLabel];
            [m_pScaleView addSubview:pItem];
            [pItem release];
        }

        UIColor *pColors[2];
        pColors[0] = [UIColor colorWithRed:0 green:0 blue:1 alpha:1.0];
        pColors[1] = [UIColor colorWithRed:0 green:.8 blue:0 alpha:1.0];
        int iRGBIndex = 0;
        for (int i = 0; i < [m_pPartition NZones]; i++)
        {
            CSecZone *pZone = [m_pPartition Zone:i];
            CGRect rItem = CGRectMake(0, (i+4)*iDYItem+SECURITY_ITEM_INSET+2, rScale.size.width, iDYItem-2*SECURITY_ITEM_INSET-4);
            CLegendItem *pItem = [[CLegendItem alloc] initWithFrame:rItem Color:pColors[iRGBIndex] Text:[pZone Name]];
            [m_pScaleView addSubview:pItem];
            [pItem release];
            iRGBIndex++;
            if (iRGBIndex >= 2)
                iRGBIndex = 0;
            
        }
    }
    return self;
}
/*============================================================================*/

- (void)dealloc 

/*============================================================================*/
{
    [super dealloc];
}
/*============================================================================*/

-(CTimeDataView*)CreateTimeDataView:(CGRect)rFrame LastView:(CTimeDataView*)pLastView

/*============================================================================*/
{
    CSecurityHistoryDataView *pLastSecurityView = (CSecurityHistoryDataView*)pLastView;
    CSecurityHistoryDataView *pView = [[CSecurityHistoryDataView alloc] initWithFrame:rFrame TimeView:self Partition:m_pPartition LastView:pLastSecurityView];
    return pView;
}
/*============================================================================*/

-(int)DataViewDY

/*============================================================================*/
{
    int iNItems = (4+[m_pPartition NZones]);
    int iDYItem = [CSecurityHistoryView DYItem];
    return MAX(self.frame.size.height - [CMainView DY_TIMEBAR], iNItems * iDYItem);
}
/*============================================================================*/

-(void)NotifyRotation

/*============================================================================*/
{
    [m_pTimeBar removeFromSuperview];
    [m_pTimeBar release];

    CGRect rFrame = self.frame;
    CGRect rTimeBar = CGRectMake(-rFrame.size.width, rFrame.size.height-[CMainView DY_TIMEBAR], 3*rFrame.size.width, [CMainView DY_TIMEBAR]);
    m_pTimeBar = [[CTimeBar alloc] initWithFrame:rTimeBar TimeView:self];
    [self addSubview:m_pTimeBar];

    CGRect rData = m_pTimeDataView.frame;
    rData.origin.x = -self.frame.size.width;
    rData.size.width = 3 * self.frame.size.width;
    rData.size.height = MAX([self DataViewDY], self.frame.size.height - [CMainView DY_TIMEBAR]);
    [m_pTimeDataView setFrame:rData];

    [m_pTimeDataView SetXOrigin];
    
    CGRect rLegend = m_pLegendView.frame;
    rLegend.origin.x = self.frame.size.width - rLegend.size.width - 10;
    rLegend.origin.y = 2;
    [m_pLegendView setFrame:rLegend];
    
    [m_pTimeDataView setNeedsDisplay];
}
/*============================================================================*/

+(int)DYItem

/*============================================================================*/
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return SECURITY_DY_ITEM_IPAD;
    return SECURITY_DY_ITEM_IPOD;
}
@end
