//
//  TableTitleHeaderView.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/10/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DY_HEADER_IPOD          20
#define DY_HEADER_IPAD          40
#define DY_HEADER_SPACE         0

/*============================================================================*/

@interface CTableTitleHeaderView : UIView 

/*============================================================================*/
{
    UILabel                             *m_pLabel;
 	CGGradientRef                       m_Gradient;
    int                                 m_iRad;
}

-(id)initWithFrame:(CGRect)rFrame Title:(NSString*)pTitle;
-(id)initWithFrame:(CGRect)rFrame Title:(NSString*)pTitle Radius:(int)iRad;
-(void)SetTitle:(NSString*)psTitle;

@end
