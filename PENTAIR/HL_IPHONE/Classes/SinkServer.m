//
//  SinkServer.m
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 9/22/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import "SinkServer.h"
#import "Sink.h"


@implementation CSinkServer

/*============================================================================*/

-(id)init

/*============================================================================*/
{
    self = [super init];
    if (self)
    {
        m_pSinks = CFArrayCreateMutable(NULL, 0, NULL);
    }
    return self;
}
/*============================================================================*/

-(void)dealloc

/*============================================================================*/
{
    CFRelease(m_pSinks);
    [super dealloc];
}
/*============================================================================*/

-(void)AddSink:(id)pSink

/*============================================================================*/
{
    CFArrayAppendValue(m_pSinks, pSink);
}
/*============================================================================*/

-(void)RemoveSink:(id)pSink

/*============================================================================*/
{
    BOOL bRemove = TRUE;
    while (bRemove)
    {
        bRemove = FALSE;
        for (int iIndex = 0; iIndex < CFArrayGetCount(m_pSinks) && !bRemove; iIndex++)
        {
            if (CFArrayGetValueAtIndex(m_pSinks, iIndex) == pSink)
            {
                CFArrayRemoveValueAtIndex(m_pSinks, iIndex);
                bRemove = TRUE;
            }
        }
    }
}
/*============================================================================*/

-(void)NotifySinks:(int)iID Data:(int)iData

/*============================================================================*/
{
    int iN = CFArrayGetCount(m_pSinks);
    for (int i = 0; i < iN; i++)
    {
        CSink *pSink = (CSink*)CFArrayGetValueAtIndex(m_pSinks, i);
        [pSink Notify:iID Data:iData];
    }
}
/*============================================================================*/

-(int)NSinks

/*============================================================================*/
{
    int iN = CFArrayGetCount(m_pSinks);
    return iN;
}
@end
