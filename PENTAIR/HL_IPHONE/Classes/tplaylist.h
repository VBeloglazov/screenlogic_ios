//
//  tplaylist.h
//  XPAD_VIEW
//
//  Created by Andrew Watzke on 10/2/08.
//  Copyright 2008 HomeLogic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sink.h"
#import "HLTableFrame.h"

@class CAudioZone;
@class CMP3InfoPage;
@class CHLTableFrame;

/*============================================================================*/

@interface CPlaylistView : CHLTableFrame      <     UITableViewDelegate, 
                                                    UITableViewDataSource,
                                                    CSink >
                                                        
/*============================================================================*/
{
    CAudioZone                              *m_pZone;
    int                                     m_iLastActiveItemIndex;
    UILabel                                 *m_pMessage;
}

-(id)initWithFrame:(CGRect)rect Zone:(CAudioZone*)pZone;
-(int)SelectedIndex;
-(void)Deselect;

@end
